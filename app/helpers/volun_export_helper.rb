module VolunExportHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil,volun_export=nil)   
    return if items.blank?       
   
    columns = ""
    VolunExport.get_traslation_volunteer_csv.each do |t, v|      
      columns = columns + (columns.blank? ? "" : ",") + v if items.try(t).to_s == "true"
    end

    query = "COPY (Select distinct " + columns + "from volunteers) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
