module AuditsHelper 
    extend ActiveSupport::Concern
    
    def stream_query_rows(items=nil,params=nil)
        model = items.model
        filtrado = StreamingHelper.filter(model, items, params, "header_list_audits")
        begin
            filtrado[:header].each {|x| yield x if block_given? }
          rescue
          end
        
        return if items.blank?
        query = "COPY (
            Select distinct  COALESCE(CAST((CASE WHEN audits.user_id is null THEN ''
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Manager' THEN (Select CONCAT(managers.name, ' ', managers.last_name, ' ', managers.last_name_alt) from users, managers where audits.user_id = users.id AND users.loggable_type = 'Manager' AND users.loggable_id = managers.id)
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Volunteer' THEN (Select CONCAT(volunteers.name, ' ', volunteers.last_name, ' ', volunteers.last_name_alt) from users, volunteers where audits.user_id = users.id AND users.loggable_type = 'Volunteer' AND users.loggable_id = volunteers.id)
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Entity' THEN (Select entities.name from users, entities where audits.user_id = users.id AND users.loggable_type = 'Entity' AND users.loggable_id = entities.id)
                ELSE '-' END) as varchar),'-') as \"#{model.human_attribute_name(:user_name)}\",
            COALESCE(CAST((CASE WHEN audits.user_id is null THEN 'Ciudadano'
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Manager' THEN 'Gestor'
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Volunteer' THEN 'Voluntario'
                WHEN (Select users.loggable_type from users where audits.user_id = users.id) = 'Entity' THEN 'Entidad'
                ELSE '-' END) as varchar),'-') as \"#{model.human_attribute_name(:user_type)}\",
            COALESCE(CAST((Select resources.description from resources where resources.id=audits.resource_id) as varchar),'-') as \"#{model.human_attribute_name(:resource)}\",
            COALESCE(CAST(audits.created_at as varchar),'-') as \"#{model.human_attribute_name(:created_at)}\",
            COALESCE(CAST(audits.operation as varchar),'-') as \"#{model.human_attribute_name(:operation)}\",
            COALESCE(CAST(audits.operation_type as varchar),'-') as \"#{model.human_attribute_name(:operation_type)}\",
            COALESCE(CAST(audits.old_data as varchar),'-') as \"#{model.human_attribute_name(:old_data)}\",
            COALESCE(CAST(audits.new_data as varchar),'-') as \"#{model.human_attribute_name(:new_data)}\"
           
            FROM audits #{ "where audits.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
            TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
        StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    end
end
