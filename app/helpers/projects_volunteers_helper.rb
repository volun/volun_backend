module ProjectsVolunteersHelper
    extend ActiveSupport::Concern
  
    def stream_query_rows(items=nil,params=nil)
      model = items.model
      yield("#{I18n.t('csv_export.header_list_derivatie_projects')}\n\n")
      
      return if items.blank? || params[:id].blank?
      query = "COPY ( 
        SELECT DISTINCT 
          pv.project_id as \"#{I18n.t('derivatie_projects.list.project_id')}\",
          p.name as \"#{I18n.t("derivatie_projects.list.project_name")}\",
          CASE p.subtype_pt WHEN 'punctual' THEN 'Puntual'
            WHEN 'permanent' then 'Permanente' END as \"#{I18n.t("derivatie_projects.list.project_type")}\",
          TO_CHAR(p.execution_start_date::date, 'dd/mm/yyyy') as \"#{I18n.t("derivatie_projects.list.execution_start_date")}\",
          TO_CHAR(p.execution_end_date::date, 'dd/mm/yyyy') as \"#{I18n.t("derivatie_projects.list.execution_end_date")}\",
          CASE WHEN pv.volunteer_type = 'DERIVATIE' THEN 'No' 
            ELSE 'Si' END as \"#{I18n.t('derivatie_projects.list.link')}\"
        FROM projects_volunteers pv 
        INNER JOIN projects p on p.id = pv.project_id
        where pv.volunteer_id = #{params[:id]} 
        order by pv.project_id desc) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
        
      StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    end
  end