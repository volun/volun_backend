module VolunTrackingsHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_volun_tracking")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct 
      COALESCE(CAST((select projects.name FROM projects WHERE projects.id = volun_trackings.project_id) as varchar),'-') as \"#{Volun::Tracking.human_attribute_name(:project)}\",
      COALESCE(CAST((select tracking_types.name FROM tracking_types WHERE tracking_types.id = volun_trackings.tracking_type_id) as varchar),'-') as \"#{Volun::Tracking.human_attribute_name(:tracking_type)}\",
      COALESCE(CAST((select managers.name FROM managers WHERE managers.id = volun_trackings.manager_id) as varchar),'-') as \"#{Volun::Tracking.human_attribute_name(:manager)}\",
      COALESCE(CAST(volun_trackings.tracked_at as varchar),'-') as \"#{Volun::Tracking.human_attribute_name(:tracked_at)}\",
      CASE volun_trackings.automatic WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Sí' END as \"#{Volun::Tracking.human_attribute_name(:automatic)}\",
      COALESCE(CAST(volun_trackings.comments as varchar),'-') as \"#{Volun::Tracking.human_attribute_name(:comments)}\",
      COALESCE(CAST((select request_types.id FROM request_types, request_forms WHERE request_types.id=request_forms.request_type_id AND request_forms.id = volun_trackings.request_form_id) as varchar),'-') as \"#{I18n.t('activerecord.models.request_form.one')}\"
  
      FROM volun_trackings #{ "where volun_trackings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end