module RtOtherHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_rt_other")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select 
      COALESCE(TRIM(CAST((select entities.phone_number from entities,users 
        WHERE request_forms.user_id=users.id
        AND users.loggable_id= entities.id
        AND users.loggable_type='Entity') as varchar)),'-') as \"#{Project.human_attribute_name(:phone_number)}\",
      COALESCE(TRIM(CAST((select entities.email from entities,users 
        WHERE request_forms.user_id=users.id
        AND users.loggable_id= entities.id
        AND users.loggable_type='Entity') as varchar)),'-') as \"#{Project.human_attribute_name(:email)}\",
      COALESCE(CAST(rt_others.notes as varchar),'-') as \"#{Rt::Other.human_attribute_name(:notes)}\",
      COALESCE(CAST(rt_others.description as varchar),'-') as \"#{Rt::Other.human_attribute_name(:description)}\",
      COALESCE(CAST(DATE(rt_others.created_at)as varchar),'-') as \"#{Rt::Other.human_attribute_name(:created_at)}\",
      CASE 
        WHEN request_forms.req_status_id IN ('3','4','2')
        THEN DATE_PART('day', request_forms.status_date - request_forms.created_at)
        WHEN request_forms.req_status_id IN ('1')
        THEN DATE_PART('day', CURRENT_TIMESTAMP - request_forms.created_at)
      END as \"#{Rt::Other.human_attribute_name(:days_passed)}\",
      COALESCE(CAST((
        SELECT TRIM(CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt))
        FROM managers, request_forms 
        WHERE request_forms.request_type_id=12 
        AND request_forms.rt_extendable_id = rt_others.id  
        AND managers.id=request_forms.manager_id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:manager)}\",
      COALESCE(CAST((select DATE(request_forms.status_date) 
        FROM request_forms 
        WHERE request_forms.request_type_id=12 
        AND request_forms.rt_extendable_id = rt_others.id)as varchar),'-') as \"#{RequestForm.human_attribute_name(:status_date)}\",
      CASE (select req_statuses.description FROM req_statuses,request_forms WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=12 
          AND request_forms.rt_extendable_id = rt_others.id) 
        WHEN 'pending' THEN '#{I18n.t('rt_other_filter.search_pending')}'
        WHEN 'processing' THEN '#{I18n.t('rt_other_filter.search_processing')}'
        WHEN 'approved' THEN '#{I18n.t('rt_other_filter.search_approved')}'
        WHEN 'rejected' THEN '#{I18n.t('rt_other_filter.search_rejected')}'
        END as \"#{RequestForm.human_attribute_name(:status)}\"
      FROM rt_others, request_forms 
      WHERE request_forms.request_type_id=12 
      AND request_forms.rt_extendable_id = rt_others.id
      #{ "AND rt_others.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end