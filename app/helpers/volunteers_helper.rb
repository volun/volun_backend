module VolunteersHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil,derivatie=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, derivatie.blank? || !derivatie ? 'header_list_volunteer' : "header_list_derivaties")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    return if items.blank?

    derivatie_header=""
    if !derivatie.blank? && derivatie && !params[:project_id].blank?
      derivatie_header= "
        (Select projects_volunteers.subscribe_date from projects_volunteers where projects_volunteers.volunteer_id=volunteers.id AND projects_volunteers.project_id=#{params[:project_id]}) as \"#{ProjectsVolunteer.human_attribute_name(:subscribe_date)}\",
        (Select CONCAT(CAST(projects_volunteers.day as varchar),' - ', shift_definitions.name)  from projects_volunteers, shift_definitions where shift_definitions.id=projects_volunteers.shift_definition_id AND projects_volunteers.volunteer_id=volunteers.id AND projects_volunteers.project_id=#{params[:project_id]}) as \"#{ProjectsVolunteer.human_attribute_name(:day)}\",
        (Select 
          string_agg(CASE volun_proy_meetings.confirm 
          WHEN 'true' THEN CONCAT(meetings.name,' - Confirma')
          WHEN 'false' THEN CONCAT(meetings.name,' - No confirma')
          ELSE CONCAT(meetings.name,' - Sin confirmar') END,', ')
          from projects_volunteers, volun_proy_meetings,meetings where meetings.id = volun_proy_meetings.meeting_id AND volun_proy_meetings.projects_volunteer_id=projects_volunteers.id AND projects_volunteers.volunteer_id=volunteers.id AND projects_volunteers.project_id=#{params[:project_id]}) as \"#{VolunProyMeeting.human_attribute_name(:confirm)}\",
          (Select 
            string_agg(CASE volun_proy_meetings.asist 
            WHEN 'true' THEN CONCAT(meetings.name,' - Asiste')
            WHEN 'false' THEN CONCAT(meetings.name,' - No asiste')
            ELSE CONCAT(meetings.name,' - Sin confirmar') END,', ')
            from projects_volunteers, volun_proy_meetings,meetings where meetings.id = volun_proy_meetings.meeting_id AND  volun_proy_meetings.projects_volunteer_id=projects_volunteers.id AND projects_volunteers.volunteer_id=volunteers.id AND projects_volunteers.project_id=#{params[:project_id]}) as \"#{VolunProyMeeting.human_attribute_name(:asist)}\",
      "
    end
 
    query = "COPY (Select distinct volunteers.id_autonum as \"#{Volunteer.human_attribute_name(:id_autonum)}\",
    COALESCE(CAST(volunteers.id as varchar),'-') as \"#{Volunteer.human_attribute_name(:id)}\",
      COALESCE(CAST(volunteers.id_number as varchar),'-')  as \"#{Volunteer.human_attribute_name(:id_number)}\",
      COALESCE(volunteers.name,'-') as \"#{Volunteer.human_attribute_name(:name)}\",
      COALESCE(volunteers.last_name,'-') as \"#{Volunteer.human_attribute_name(:last_name)}\",
      COALESCE(volunteers.last_name_alt,'-') as \"#{Volunteer.human_attribute_name(:last_name_alt)}\",
      COALESCE(volunteers.email,'-') as \"#{Volunteer.human_attribute_name(:email)}\",
      COALESCE(volunteers.phone_number,'-') as \"#{Volunteer.human_attribute_name(:phone_number)}\",
      COALESCE(volunteers.phone_number_alt,'-') as \"#{Volunteer.human_attribute_name(:phone_number_alt)}\",
      COALESCE((select addresses.town from addresses where addresses.id = volunteers.address_id)
        ,'-') as \"#{Address.human_attribute_name(:town)}\",
      COALESCE((select addresses.postal_code from addresses where addresses.id = volunteers.address_id)
        ,'-')  as \"#{Address.human_attribute_name(:postal_code)}\",
      COALESCE((select d.name from districts d,addresses where d.id=addresses.district_id and addresses.id = volunteers.address_id)
      ,'OTROS') as \"#{Address.human_attribute_name(:district)}\",
      COALESCE((select d.name from boroughts d,addresses where d.id=addresses.borought_id and addresses.id = volunteers.address_id)
      ,'OTROS') as \"#{Address.human_attribute_name(:borought)}\",
      #{derivatie_header} 
      (select g.name from genders g where g.id=volunteers.gender_id) as \"#{Volunteer.human_attribute_name(:gender)}\",
      CASE WHEN volunteers.active = true AND volunteers.available = true 
        AND volunteers.availability_date IS NOT NULL AND volunteers.availability_date <= '#{(Date.today+7.day).to_s}'
        THEN 'Sí' ELSE '' END as \"#{Address.human_attribute_name(I18n.t('new_availability'))}\",
      COALESCE(CAST(volunteers.availability_date as varchar),'-') as \"Fecha reactivación\",
      CASE WHEN (date_part('year',age(volunteers.birth_date)) < 18 AND volunteers.birth_date IS NOT NULL) OR (
        volunteers.cession_of_personal_data = false OR volunteers.publication_image_ayto = false OR volunteers.publication_image_ooaa=false OR
        volunteers.publication_image_social_network=false OR volunteers.publication_image_media = false OR volunteers.vocne=true OR UPPER(COALESCE((SELECT statuses.name from statuses where statuses.id = volunteers.status_id),'')) = UPPER('no apto'))
        THEN 'Sí' ELSE 'No' END as \"#{I18n.t('not_autorized')}\",
      date_part('year',age(volunteers.birth_date)) as \"#{Volunteer.human_attribute_name(:age)}\",
      COALESCE((select nationalities.name from nationalities where nationalities.id = volunteers.nationality_id) 
        ,'-') as \"#{Volunteer.human_attribute_name(:nationality)}\",
      COALESCE((select p.name from addresses,provinces p where p.id=addresses.province_id and addresses.id = volunteers.address_id)
      ,'-') as \"#{Address.human_attribute_name(:province)}\",
      (Select count(*) from projects_volunteers where projects_volunteers.active=true AND projects_volunteers.volunteer_id=volunteers.id) as \"#{I18n.t('num_active_projects')}\",
      COALESCE(CAST(volunteers.subscribe_date as varchar),'-') as \"#{Volunteer.human_attribute_name(:subscribe_date)}\",
      COALESCE(CAST(volunteers.unsubscribe_date as varchar),'-') as \"#{Volunteer.human_attribute_name(:unsubscribe_date)}\",
      COALESCE((select statuses.name from statuses where statuses.id = volunteers.status_id)
        ,'-') as \"#{Volunteer.human_attribute_name(:status_id)}\",
      CASE volunteers.vocne WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Sí' END as \"#{Volunteer.human_attribute_name(:vocne)}\",
      CASE volunteers.accompany_volunteer WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Si' END as \"#{I18n.t('csv_export.diversity')}\",
      (select COALESCE(volun_trackings.comments,'-') from volun_trackings where volun_trackings.volunteer_id = volunteers.id order by tracked_at desc limit 1)
        as \"#{I18n.t('csv_export.last_tracking')}\",
      (SELECT string_agg(districts.name,', ') FROM districts, districts_volunteers WHERE districts.id=districts_volunteers.district_id AND districts_volunteers.volunteer_id=volunteers.id) as \"#{I18n.t('incluence_district')}\"
      from volunteers where #{ "volunteers.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end

  def stream_datosabiertos
    query = "COPY (
      select COALESCE((select g.\"name\"  from genders g where g.id=v.gender_id),'Otros') as \"Sexo\",
      CASE  WHEN v.birth_date IS NULL THEN '0-19'
      when date_part('year'::text, age(regexp_replace(v.birth_date::text, '(.*)/(.*)/(.*)'::text, '\3-\2-\1'::text)::date::timestamp with time zone)) > 90 then '>90'
      ELSE 
      CONCAT((date_part('year'::text, age(regexp_replace(v.birth_date::text, '(.*)/(.*)/(.*)'::text, '\3-\2-\1'::text)::date::timestamp with time zone))/10)*10,'-',(date_part('year'::text, age(regexp_replace(v.birth_date::text, '(.*)/(.*)/(.*)'::text, '\3-\2-\1'::text)::date::timestamp with time zone))/10)*10+9)
      END AS \"Edad\",
      COALESCE((select n.name from nationalities n where  v.nationality_id=n.id),'OTRA') as \"Nacionalidad\",
      COALESCE(to_char(v.subscribe_date, 'DD/MM/YYYY'::text),'-') as \"Fecha de alta\",
      COALESCE(to_char(v.unsubscribe_date, 'DD/MM/YYYY'::text),'-') as \"Fecha de baja\",
      COALESCE((select es.\"name\" from employment_statuses es where es.id=v.employment_status_id),'-') as \"Situación laboral\",
      COALESCE((select al.\"name\" from academic_levels al where al.id=v.academic_level_id),'-') as \"Nivel académico\",
      COALESCE((select d.\"name\" from addresses a, districts d  where a.id=v.address_id and d.id=a.district_id),'OTROS') as \"Distrito\",
      COALESCE((select b.\"name\" from addresses a, boroughts b  where a.id=v.address_id and b.id=a.borought_id),'OTROS') as \"Barrio\",
      cast(count(1) as varchar) as \"Número\"
      from volunteers v group by \"Sexo\",\"Edad\",\"Nacionalidad\",\"Fecha de alta\",\"Fecha de baja\",\"Situación laboral\",\"Nivel académico\",\"Distrito\",\"Barrio\")
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
