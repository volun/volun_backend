module CitizensHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=[],params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_citizens")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end  

    return if items.blank?
    query = "COPY (
      Select
        COALESCE(CAST(citizens.id as varchar),'-') as \"#{Citizen.human_attribute_name(:id)}\",
        COALESCE(CAST(citizens.name as varchar),'-') as \"#{Citizen.human_attribute_name(:name)}\",
        COALESCE(CAST(citizens.last_name_1 as varchar),'-') as \"#{Citizen.human_attribute_name(:last_name_1)}\",
        COALESCE(CAST(citizens.last_name_2 as varchar),'-') as \"#{Citizen.human_attribute_name(:last_name_2)}\", 
        CASE WHEN citizens.birthday is null THEN null
        WHEN date_part('year',age(TO_DATE(citizens.birthday, 'DD/MM/YYYY'))) < 150 then date_part('year',age(TO_DATE(citizens.birthday, 'DD/MM/YYYY')))
        WHEN date_part('year',age(TO_DATE(citizens.birthday, 'YYYY/MM/DD'))) < 150 then date_part('year',age(TO_DATE(citizens.birthday, 'YYYY/MM/DD')))
        ELSE null END as \"#{Citizen.human_attribute_name(:age)}\",
        COALESCE(CAST((SELECT string_agg(services.name,', ') FROM services WHERE services.id  in (SELECT citizen_services.service_id FROM citizen_services WHERE citizen_services.citizen_id = citizens.id )) as varchar),'-') as \"#{Citizen.human_attribute_name(:type_services)}\",
        (SELECT COUNT(id) FROM citizen_services WHERE citizen_services.citizen_id = citizens.id) as \"#{Citizen.human_attribute_name(:count_services)}\",
        (SELECT COUNT(id) FROM citizen_services WHERE citizen_services.citizen_id = citizens.id AND citizen_services.status='finished') as \"#{Citizen.human_attribute_name(:count_services_finish)}\",
        (SELECT COUNT(id) FROM citizen_services WHERE citizen_services.citizen_id = citizens.id AND citizen_services.status in ('active','prioritary')) as \"#{Citizen.human_attribute_name(:count_services_pending)}\",
        (SELECT COUNT(id) FROM citizen_services WHERE citizen_services.citizen_id = citizens.id AND citizen_services.status='accept') as \"#{Citizen.human_attribute_name(:count_services_accept)}\",
        COALESCE(cast(average_vote as varchar),'0') as \"#{Citizen.human_attribute_name(:average_vote)}\",
        COALESCE(CAST(citizens.document as varchar),'-') as \"#{Citizen.human_attribute_name(:document)}\",
        COALESCE((select document_types.name from document_types where document_types.id = citizens.document_type_id),'-') as \"#{Citizen.human_attribute_name(:document_type)}\",
        COALESCE(CAST(citizens.country as varchar),'-') as \"#{Citizen.human_attribute_name(:country)}\",
        COALESCE(CAST(citizens.academic_info as varchar),'-') as \"#{Citizen.human_attribute_name(:academic_info)}\",
        COALESCE(CAST((select g.name from genders g where g.id =citizens.gender_id) as varchar),'-') as \"#{Citizen.human_attribute_name(:gender)}\",
        COALESCE(CAST(citizens.people_at_home as varchar),'-') as \"#{Citizen.human_attribute_name(:people_at_home)}\",
        COALESCE(CAST(citizens.email as varchar),'-') as \"#{Citizen.human_attribute_name(:email)}\",
        COALESCE(CAST(citizens.phone as varchar),'-') as \"#{Citizen.human_attribute_name(:phone)}\",
        COALESCE(CAST(citizens.start_date as varchar),'-') as \"#{Citizen.human_attribute_name(:start_date)}\",
        COALESCE((select addresses.road_type from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:road_type)}\",
        COALESCE((select addresses.road_name from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:road_name)}\",
        COALESCE((select addresses.road_number_type from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:road_number_type)}\",
        COALESCE((select addresses.road_number from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:road_number)}\",
        COALESCE((select addresses.floor from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:floor)}\",
        COALESCE((select addresses.door from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:door)}\",
        COALESCE((select b.name from boroughts b, addresses where b.id=addresses.borought_id and addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:borough)}\",
        COALESCE((select d.name from districts d,addresses where d.id=addresses.district_id and addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:district)}\",
        COALESCE((select addresses.town from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:town)}\",
        COALESCE((select addresses.postal_code from addresses where addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:postal_code)}\",
        COALESCE((select p.name from addresses,provinces p where p.id=addresses.province_id and addresses.id = citizens.address_id),'-') as \"#{Address.human_attribute_name(:province)}\",
        CASE citizens.active WHEN true THEN 'OK' 
        WHEN false THEN 'ARCHIVADO'
        ELSE '-' END as \"#{Citizen.human_attribute_name(:active)}\",
        COALESCE(CAST(to_char(citizens.created_at, 'DD/MM/YYYY') as varchar),'-') as \"Fecha de registro\",
        COALESCE(CAST(to_char(citizens.created_at, 'HH24:MI') as varchar),'-') as \"Hora de registro\"
      FROM citizens where citizens.market = false #{ "AND citizens.id IN (#{items.select(:id).ids.to_s.gsub(/\[|\]/,"")})" unless items.select(:id).blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end

  def color_background(citizen)
    if citizen.tester == true
      "background-color:#28ae13; color: white"
    elsif citizen.active == false && citizen.unsubscribed == true  
      "background-color:#7F7CA6; color: white" 
    elsif citizen.email_verified == false
      "background-color:#C1C1C2"
    elsif citizen.internal_management== true && citizen.enabled_app=="Mayor sin App móvil"
      "background-color:#5482a2; color: white"      
    end
  rescue
    ""
  end

  def color_background_service(citizen_service)
    if citizen_service.generated_gi == true && (citizen_service.citizen.blank? || citizen_service.citizen.enabled_app=="Mayor sin App móvil")
      "background-color:#5482a2; color: white" 
    else
      color_background(citizen_service.citizen)
    end
  rescue
    ""
  end
end
