module MeetingsHelper
  extend ActiveSupport::Concern

  
  def stream_query_rows(items=nil,params=nil)
    model = nil
    model = items.model if !items.try(:model).blank?
    model = items.class if !items.try(:class).blank? && model.blank?
    filtrado = StreamingHelper.filter(model, items, params, "header_list_meeting")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct 
      COALESCE((SELECT CAST(volunteers.id_autonum as varchar) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:id)}\",
      COALESCE((SELECT CAST(volunteers.id_number as varchar) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-')  as \"#{Volunteer.human_attribute_name(:id_number)}\",
      COALESCE((SELECT volunteers.name FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:name)}\",
      COALESCE((SELECT volunteers.last_name FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:last_name)}\",
      COALESCE((SELECT volunteers.last_name_alt FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:last_name_alt)}\",
      COALESCE((SELECT volunteers.email FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:email)}\",
      COALESCE((SELECT volunteers.phone_number FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:phone_number)}\",
      COALESCE((SELECT volunteers.phone_number_alt FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:phone_number_alt)}\",
      COALESCE((select addresses.town from addresses,volunteers where addresses.id = volunteers.address_id AND volunteers.id = projects_volunteers.volunteer_id)
        ,'-') as \"#{Address.human_attribute_name(:town)}\",
      COALESCE((select addresses.postal_code from addresses, volunteers where addresses.id = volunteers.address_id AND volunteers.id = projects_volunteers.volunteer_id)
        ,'-')  as \"#{Address.human_attribute_name(:postal_code)}\",
      COALESCE((select d.name from districts d,addresses, volunteers where addresses.district_id=d.id and addresses.id = volunteers.address_id AND volunteers.id = projects_volunteers.volunteer_id)
      ,'-') as \"#{Address.human_attribute_name(:district)}\",
      projects_volunteers.subscribe_date as \"#{ProjectsVolunteer.human_attribute_name(:subscribe_date)}\",
      (Select CONCAT(CAST(projects_volunteers.day as varchar),' - ', shift_definitions.name)  from shift_definitions where shift_definitions.id=projects_volunteers.shift_definition_id) as \"#{ProjectsVolunteer.human_attribute_name(:day)}\",
      CASE volun_proy_meetings.confirm 
        WHEN 'true' THEN 'Confirma'
        WHEN 'false' THEN 'No confirma'
        ELSE 'Sin confirmar' END  as \"#{VolunProyMeeting.human_attribute_name(:confirm)}\",
      CASE volun_proy_meetings.asist 
        WHEN 'true' THEN 'Asiste'
        WHEN 'false' THEN 'No asiste'
        ELSE 'Sin confirmar' END as \"#{VolunProyMeeting.human_attribute_name(:asist)}\",
      COALESCE((SELECT g.name from genders g, volunteers where g.id= volunteers.gender_id and volunteers.id = projects_volunteers.volunteer_id),'-')  as \"#{Volunteer.human_attribute_name(:gender)}\",
      COALESCE((SELECT CASE WHEN volunteers.active = true AND volunteers.available = true 
        AND volunteers.availability_date IS NOT NULL AND volunteers.availability_date <= '#{(Date.today+7.day).to_s}'
        THEN 'Sí' ELSE '' END FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-')  as \"#{Address.human_attribute_name(I18n.t('new_availability'))}\",
      COALESCE((SELECT CAST(volunteers.availability_date as varchar) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"Fecha reactivación\",
      (SELECT CASE WHEN (date_part('year',age(volunteers.birth_date)) < 18 AND volunteers.birth_date IS NOT NULL) OR (
        volunteers.cession_of_personal_data = false OR volunteers.publication_image_ayto = false OR volunteers.publication_image_ooaa=false OR
        volunteers.publication_image_social_network=false OR volunteers.publication_image_media = false OR volunteers.vocne=false)
        THEN 'Sí' ELSE 'No' END FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id) as \"#{I18n.t('not_autorized')}\",
      (SELECT date_part('year',age(volunteers.birth_date)) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id) as \"#{Volunteer.human_attribute_name(:age)}\",
      COALESCE((select nationalities.name from nationalities, volunteers where nationalities.id = volunteers.nationality_id AND volunteers.id = projects_volunteers.volunteer_id)
        ,'-') as \"#{Volunteer.human_attribute_name(:nationality)}\",
      COALESCE((select p.name from addresses,provinces p, volunteers where p.id=addresses.province_id and addresses.id = volunteers.address_id AND volunteers.id = projects_volunteers.volunteer_id)
      ,'-') as \"#{Address.human_attribute_name(:province)}\",
      COALESCE((SELECT CAST(volunteers.subscribe_date as varchar) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:subscribe_date)}\",
      COALESCE((SELECT CAST(volunteers.unsubscribe_date as varchar) FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id),'-') as \"#{Volunteer.human_attribute_name(:unsubscribe_date)}\",
      COALESCE((select statuses.name from statuses, volunteers where statuses.id = volunteers.status_id AND volunteers.id = projects_volunteers.volunteer_id)
        ,'-') as \"#{Volunteer.human_attribute_name(:status_id)}\",
      (SELECT CASE volunteers.vocne WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Sí' END FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id) as \"#{Volunteer.human_attribute_name(:vocne)}\",
      (SELECT CASE volunteers.accompany_volunteer WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Si' END FROM volunteers WHERE volunteers.id = projects_volunteers.volunteer_id) as \"#{I18n.t('csv_export.diversity')}\",
      (select COALESCE(volun_trackings.comments,'-') from volun_trackings, volunteers where volun_trackings.volunteer_id = volunteers.id AND volunteers.id = projects_volunteers.volunteer_id order by tracked_at desc limit 1)
        as \"#{I18n.t('csv_export.last_tracking')}\",
      (SELECT string_agg(districts.name,', ') FROM districts, districts_volunteers WHERE districts.id=districts_volunteers.district_id AND districts_volunteers.volunteer_id=projects_volunteers.volunteer_id) as \"#{I18n.t('incluence_district')}\"
      FROM projects_volunteers, volun_proy_meetings,meetings,volunteers 
      WHERE meetings.id = volun_proy_meetings.meeting_id
      AND volun_proy_meetings.projects_volunteer_id=projects_volunteers.id
      AND projects_volunteers.volunteer_id = volunteers.id
      AND volunteers.market = false
      #{ "AND meetings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
