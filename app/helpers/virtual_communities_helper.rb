module VirtualCommunitiesHelper

    def traduction_route(route = nil)
        return '' if route.blank?
        route = route.gsub(/[http|https]+:\/\//, '')
        params = route.split('?')
        route = route.gsub(/\?.*/,'')
        aux = route.split('/')

        route = aux.blank? && !route.blank? ? 'Página de inicio' : ''
        host = request.host
        port = request.port
        local_url_port = "#{host}:#{port}"
        local_url = host.to_s

        aux.each_with_index do |x, i|
            if i==0 && (x.to_s == local_url_port || x.to_s == local_url)
                x = 'Página de inicio'
            elsif i== 0
                x = 'Otro origen'
            elsif x.match(/^[0-9]+/)
                x = 'Identificador: ' + x
            elsif x.include?('.')
                x = x.split('.')
                x= I18n.t('routes.' + x[0].to_s) + " / Exportación en formato #{x[1]}"
            else
                x = I18n.t('routes.' + x.to_s)
            end

            route = route + x + " / "
        end

        (!route.blank? ? route : "") + (params.length > 1 ?  params[1] : "")
    end

end