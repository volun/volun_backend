module ApplicationHelper

  def get_min_errors(error_name,model)
    return "" if error_name.blank?
    aux= error_name.to_s.split('.')

    return model.human_attribute_name(error_name.to_sym)  if aux.count <= 1
   
    #I18n.t("#{aux[aux.count-2]}.#{aux[aux.count-1]}")
    model.human_attribute_name(aux[aux.count-1].to_sym)
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

    def show_simple_list(ar_collection, attr = :name)
      ar_collection.pluck(attr).join(" <strong>-</strong> ").html_safe
    end
    
    def date_input_html_filter(date){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: (!date.blank? ? Date.parse(date).strftime('%d/%m/%Y') : nil),
        style: "width: 100px; margin:0px; padding:0px;"}
    end
  
    def date_input_html(date){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y') : nil}
    end
  
    def date_input_html_default_values(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y ') : Time.now.strftime('%d/%m/%Y'),
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def date_input_html_default_values_time(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y %H:%M') : Time.now.strftime('%d/%m/%Y %H:%M'),
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def date_input_html_no_default_values(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y') : nil,
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def pt_date_input_html_default_values(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y') : (!params[:pt_entity].blank? && params[:pt_entity] ? params[:pt_entity].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]} :
                                                   !params[:pt_subvention].blank? && params[:pt_subvention] ? params[:pt_subvention].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]} :
                                                   !params[:pt_centre].blank? && params[:pt_centre] ? params[:pt_centre].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}    :
                                                   !params[:pt_other].blank? && params[:pt_other] ? params[:pt_other].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}     :
                                                   !params[:pt_retired_volunteer].blank? && params[:pt_retired_volunteer] ? params[:pt_retired_volunteer].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}     :
                                                   !params[:pt_centre].blank? && params[:pt_centre] ? params[:pt_centre].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}    :
                                                   !params[:pt_permanent].blank? && params[:pt_permanent] ? params[:pt_permanent].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]} :
                                                   !params[:pt_punctual].blank? && params[:pt_punctual] ? params[:pt_punctual].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}  :
                                                   !params[:pt_social].blank? && params[:pt_social] ? params[:pt_social].try {|x| x[:project_attributes]}.try {|x| x[:execution_start_date]}    :
                                                   Date.today.strftime('%d/%m/%Y')),
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def pt_date_input_html_no_default_values(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y') : (!params[:pt_entity].blank? && params[:pt_entity] ? params[:pt_entity].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]} :
                                                   !params[:pt_subvention].blank? && params[:pt_subvention] ? params[:pt_subvention].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]} :
                                                   !params[:pt_centre].blank? && params[:pt_centre] ? params[:pt_centre].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}    :
                                                   !params[:pt_other].blank? && params[:pt_other] ? params[:pt_other].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}     :
                                                   !params[:pt_retired_volunteer].blank? && params[:pt_retired_volunteer] ? params[:pt_retired_volunteer].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}     :
                                                   !params[:pt_centre].blank? && params[:pt_centre] ? params[:pt_centre].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}    :
                                                   !params[:pt_permanent].blank? && params[:pt_permanent] ? params[:pt_permanent].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]} :
                                                   !params[:pt_punctual].blank? && params[:pt_punctual] ? params[:pt_punctual].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}  :
                                                   !params[:pt_social].blank? && params[:pt_social] ? params[:pt_social].try {|x| x[:project_attributes]}.try {|x| x[:execution_end_date]}    :
                                                   ""),
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def datetime_input_html_no_default_values(date, opts = {}){
        class: 'datepicker',
        placeholder: 'dd/mm/aaaa',
        value: date ? date.strftime('%d/%m/%Y %H:%M') : nil,
        data: {mask: '99/99/9999'}}.reverse_merge(opts)
    end
  
    def check_box_filter_for(param_name, js_selector = nil)
      check_box_tag "q[#{param_name}]",
                    params[param_name],
                    params[param_name],
                    onchange: "update_hidden_inputs(this, '#{js_selector || '#' + param_name}')"
    end
  
    def check_box_filter_other_for(param_name, params_search = params, js_selector = nil)
      if params_search[param_name].to_s=="true"
      check_box_tag param_name,
          params_search[param_name],
          params_search[param_name]
      else
        check_box_tag param_name
      end
    end
  
    def box_link_to(model, options = {})
      return unless can? (options[:action].presence || :read), model
  
      options[:count] ||= 1
      options[:html_input] ||= {}
      options[:html_input].merge!(data: { turbolinks: false })
      panel_type = options[:panel_type] || (model.exists? ? 'warning' : 'danger')
      path   = options[:path].presence
      path ||= public_send(options[:count] == 1 ? "new_#{model.model_name.singular}_path" : "#{model.model_name.plural}_path")
      link_to path, options[:html_input] || {} do
        content_tag :div, class: "panel panel-#{panel_type}" do
          ( content_tag :div, class: 'panel-heading' do
              content_tag :h3, model.model_name.human(count: options[:count]), class: 'panel-title'
            end  )+
          ( content_tag :div, model.model_name.human(count: options[:count]), class: 'panel-body' do
            ( content_tag :div, model.model_name.human(count: options[:count]) do
                options[:body_pendientes]
              end  )+
            ( content_tag :div, model.model_name.human(count: options[:count]) do
              options[:body_en_tramite]
            end  )+
            ( content_tag :div, model.model_name.human(count: options[:count]) do
              options[:body]
            end  )
          end  )
        end
      end
    end
  
  end
  
  
  
  module SearchFormModule
    def self.find_module(user,type)
      search = SearchForm.where(user_id: user.id,model_type:type).first
      if !search.blank?
        if !search.try(:updated_at).blank? && !user.try(:authentication_token_created_at).blank? && user.authentication_token_created_at > search.updated_at
          search.destroy
          nil
        else
          search
        end
      end
    end
    def self.generate(user_id,content_aux,type)
      search = SearchForm.find_by(user_id: user_id)
      search.destroy if !search.blank? && search.model_type != type
      SearchForm.create(user_id: user_id,content:content_aux,model_type:type)
    end
    def self.update(user,content_aux,type)
      c = SearchFormModule.find_module(user,type)
      c.update(content:content_aux)
    end
    def self.delete(user,type)
      c = SearchFormModule.find_module(user,type)
      c.destroy unless c.blank?
    end
  end
  
  module ActionView::Helpers::FormHelper
    def simple_nested_fields_for(record_or_name_or_array, *args, &block)
      options = args.extract_options!.reverse_merge(:builder => NestedForm::SimpleBuilder)
      simple_fields_for(record_or_name_or_array, *(args << options), &block)
    end
  end
  
  class DatepickerInput < SimpleForm::Inputs::Base
    def input(wrapper_options)
      input_html_options.merge!(placeholder: I18n.t('date_placeholder')) unless input_html_options[:placeholder]
      input_html_options[:class] << :"form-control"
      @builder.text_field(attribute_name, input_html_options) +
      @builder.hidden_field(attribute_name, { class: "#{attribute_name}-alt"})
    end
  end
 
  #**
  # CloneProject:
  #   @description CLONADO DE PROYECTOS
  #**
  module CloneProject
    extend ActiveSupport::Concern
      def self.clone_project(project,new_name,with_volunteers, current_user,matriz)

   
        
      results = []
      new_project = project.amoeba_dup 
      new_pt = project.pt_extendable.amoeba_dup 
  
      # PARAMETROS DE CLONADO
      new_project.name = new_name unless new_name.blank?
      new_project.cloned = true
      new_project.origin_id = project.id
      new_project.father_id = matriz if !matriz.blank?
      new_project.subtype_proyect = matriz.blank? ? "matriz" : "subproyect"
  
      # REFERENCIA DE PT, HAY QUE ASIGNAR EL PROYECTO CLONADO
      new_pt.project=new_project
      new_project.year = new_project.year.to_i+1
      new_project.publish = false
      new_project.volunteers_allowed = false
      new_project.insured = false
      new_project.urgent = false
      new_project.review_end_project = false
      new_project.publish_chart = false
      new_project.outstanding = false
  
      # REFERENCIA DE PROJECT, HAY QUE ASIGNAR EL PT CLONADO
      new_project.pt_extendable = new_pt
  
      if new_project.save
        begin
          if !matriz.blank? && new_project.subtype_proyect == "subproyect"
            finish = 0
            
            
            #track_custom(current_user.loggable_id, new_project.id, 'subproject', "Se ha asignado el subproyecto #{new_project.id} al proyecto matriz #{matriz}") if new_project.pt_order.blank?
           
            
              finish = Project.find_by(id: matriz).subprojects.where("id not in (?)", new_project.id).maximum(:pt_order).to_i  
                   
              finish= finish+1          
          else
            #track_custom(current_user.loggable_id, subproject.id, 'matriz')
            finish=nil
          end
          new_project.pt_order=finish
          new_project.save(validate: false)
        rescue =>e
          puts "="*50
          puts e
        end
        index = 0
        errors = []
        begin
          #INSERTAR LINKS E INSERTAR EN NUEVO PROYECTO
          project.links.each do |link|
            new_link=link.amoeba_dup
            new_link.linkable = new_project
            new_link.file = link.file
            unless new_link.save
              errors.push([1,I18n.t('clone_errors.error_save_links', name: link.file_file_name, id: link.id)]) # insertar I18n
              errors.push([1,link.errors.full_messages])
            else
              index = index+1
            end
          end
        rescue Exception => e 
          errors.push([1,I18n.t('clone_errors.error_links_cloned')]) # insertar I18n
          errors.push([1,e.message])
        end
        if project.links.count > 0
          results.push([0,I18n.t('clone_success.success_links_cloned',success: index, total: project.links.count)])
        end
        errors.each do |err|
          results.push(err)
        end
  
        #CLONAR EVENTOS E INSERTAR EN NUEVO PROYECTO
        index = 0
        errors = []
        begin
          project.events.each do |event|
            new_event=event.amoeba_dup
            new_event.eventable = new_project
            new_event.address = event.address.amoeba_dup
            unless new_event.save
              errors.push([1,I18n.t('clone_errors.error_save_events', id: event.id)]) # insertar I18n
              errors.push([1,event.errors.full_messages])
            else
              index = index + 1
            end
          end
        rescue Exception => e 
          errors.push([1,I18n.t('clone_errors.error_events_cloned')]) # insertar I18n
          errors.push([1,e.message])
        end
        if project.events.count > 0
          results.push([0,I18n.t('clone_success.success_events_cloned',success: index, total: project.events.count)])
        end
        errors.each do |err|
          results.push(err)
        end
  
        if !with_volunteers.blank? && with_volunteers.to_i == 1
          index = 0
          errors = []
          count_volunteers = ActiveRecord::Base.connection.exec_query("SELECT * FROM projects_volunteers as pv where pv.active=true and pv.project_id=#{project.id} and (volunteer_type != 'DERIVATIE' or volunteer_type is null)").count

          begin
            volunteers_projects=ActiveRecord::Base.connection.exec_query("SELECT * FROM projects_volunteers as pv where pv.active=true and pv.project_id=#{project.id}")
            
            volunteers_projects.each do |data_project| 
              if !data_project["subscribe_date"].blank?
                data_project["subscribe_date"]="\'"+data_project["subscribe_date"]+"\'"
              else  
                data_project["subscribe_date"]="NULL" 
              end
              if !data_project["unsubscribe_date"].blank?
                data_project["unsubscribe_date"]="\'"+data_project["unsubscribe_date"]+"\'"
              else  
                data_project["unsubscribe_date"]="NULL" 
              end
              if data_project["volunteer_type"].to_s != "DERIVATIE"
                ActiveRecord::Base.connection.exec_query("INSERT INTO projects_volunteers (project_id,volunteer_id,active,
                                                                                           subscribe_date,unsubscribe_date,comments,
                                                                                           volunteer_type, created_at, updated_at) VALUES 
                                                                                           (#{new_project.id},#{data_project["volunteer_id"]},#{data_project["active"]},
                                                                                            #{data_project["subscribe_date"]}, #{data_project["unsubscribe_date"]},'#{data_project["comments"]}',
                                                                                           'ASOCIATED','#{DateTime.now}','#{DateTime.now}' )")
                index = index + 1
              end              
              #volun_trackings              
              if data_project["volunteer_type"] == "DERIVATIE"
                tracking_type_vt = TrackingType.get_down_derivatie.id
                comments_vt = I18n.t('trackings.down_derivatie')
                project_aux = new_project.id
              else
                tracking_type_vt = TrackingType.get_project_subscribe.id
                comments_vt = I18n.t('trackings.subscribe_to_project')
                project_aux = project.id
              end
              ActiveRecord::Base.connection.exec_query("INSERT INTO volun_trackings (volunteer_id, tracking_type_id, project_id,
                                                                                     manager_id, request_form_id, tracked_at,
                                                                                     automatic, comments, created_at, updated_at) VALUES
                                                                                     (#{data_project["volunteer_id"]},#{tracking_type_vt},#{project_aux},
                                                                                      #{current_user.loggable_id}, NULL,'#{DateTime.now}', 
                                                                                      TRUE, '#{comments_vt}', '#{DateTime.now}', '#{DateTime.now}')")                        
            end
            if count_volunteers > 0
              results.push([0,I18n.t('clone_success.success_volunteers_cloned',success: index, total: count_volunteers)])
            end
            AuditGenerateHelper.audit_clone(project, current_user)
            #Clonado correcto
            return results
          rescue
            errors.push([6,I18n.t('clone_errors.error_save_project_cloned')]) # insertar I18n
            #No se ha guardado correctamente el clonado
            return results
          end
          if count_volunteers > 0
            results.push([0,I18n.t('clone_success.success_volunteers_cloned',success: index, total: count_volunteers)])
          end
          errors.each do |err|
            results.push(err)
          end
        end
       # AuditGenerateHelper.audit_clone(project, current_user)
        #Clonado correcto
        return results
      else
        results.push([1,I18n.t('clone_errors.error_save_project_cloned')]) # insertar I18n
        results.push([1,new_project.errors.full_messages])
        #No se ha guardado correctamente el clonado
        return results
      end
    end
  end
  
  #**
  # GlobalSearch:
  #   @description BUSCADOR AVANZADO CON PARÁMETROS DINÁMICOS
  #**
  module GlobalSearch 
    extend ActiveSupport::Concern
  
    #**
    # Módulo de búsqueda
    #**
    # @param {*} collection Modelo de datos a tratar
    # @param {*} params Parámetros de búsqueda
    # @description Función que buscará de forma dinámica
    # en cualquier tabla de la base de datos.
    #**
    def self.search_filter(collection,params=[])
      if params.any?
        collection_filtrer=collection.all
        # filter_collapse -> string que incluye la cadena que hay dentro del where de la query
        filter_collapse=""
        params.each do |p|
          case p[0]
            when "ilike"
              filter_collapse = self.ilike(p, filter_collapse)
            when "in"
              filter_collapse = self.in(p, filter_collapse)
  
            when "strict" 
              filter_collapse = self.strict(p, filter_collapse)
  
            when "f_between_between" 
              filter_collapse = self.f_between_between(p, filter_collapse) 
  
            when "f_between"
              filter_collapse = self.f_between(p, filter_collapse)
  
            when "f_between_year" 
              filter_collapse = self.f_between_year(p, filter_collapse)
  
            when "join" 
              filter_collapse = self.join(p, filter_collapse)
  
            when "collective"  
              filter_collapse = self.collective(p, filter_collapse)
            else
          end  
        end  
  
        collection_filtrer=collection_filtrer.where(filter_collapse)
        collection.where(filter_collapse)
      else
        collection.all
      end
    end
  
  ######################################################################################
  #CONJUNTO DE MÉTODOS DE BÚSQUEDA
  ######################################################################################
    def self.ilike(params, in_where, method=" AND ") 
    # Método para búsqueda de string parcial
    # Estructura 
    # params->[0: "ilike",1: campo,2: valor]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
     
      if !params[2].blank? && params[2].to_s != "null"
        in_where+=method unless in_where.blank?
        in_where+="(translate(UPPER(cast(#{params[1]} as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER(cast('%#{params[2].to_s}%' as varchar)), 'ÁÉÍÓÚ', 'AEIOU'))"
      end
      in_where
    end
  
    def self.in(params, in_where, method=" AND ") 
      # Método para búsqueda de string parcial
      # Estructura 
      # params->[0: "in",1: campo,2: valor]
      # in_where -> string que incluye la cadena que irá dentro del where de la query
       
        if !params[2].blank? && params[2].to_s != "null"
          aux = ""
          params[2].to_s.split(',').each {|x| aux= aux.blank? ? aux + "'#{x}'" : aux + ",'#{x}'"}
          in_where+=method unless in_where.blank?
          in_where+="(cast(#{params[1]} as varchar) IN (#{aux}))"
        end
        in_where
      end
  
    def self.strict (params, in_where, method=" AND ") 
    # Método para búsqueda de string total
    # Estructura 
    # params->[0: "strict",1: campo,2: valor]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
      
      if !params[2].blank? && params[2].to_s != "null"
        in_where+=method unless in_where.blank?
        in_where+="(translate(UPPER(cast(#{params[1]} as varchar)), 'ÁÉÍÓÚ', 'AEIOU') = translate(UPPER(cast('#{params[2].to_s}' as varchar)), 'ÁÉÍÓÚ', 'AEIOU'))"
      end
      in_where
    end 
  
    def self.f_between_between (params, in_where, method=" AND ") 
    # Método para búsqueda de fecha desde y fecha hasta
    # Estructura 
    # params ->[0: "f_between_between",1: campo1,2: valor1,3: campo2,4: valor2]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
  
      if !params[2].blank? && !params[4].blank? 
        in_where+=method unless in_where.blank?
        in_where+=" ( "
        in_where+="(cast(#{params[1]} as date)>= cast('#{params[2].to_date}' as date) AND cast(#{params[3]} as date) <= cast('#{params[4].to_date}' as date))"
        in_where+=" OR "
        in_where+="(cast(#{params[1]} as date) >= cast('#{params[2].to_date}' as date) AND #{params[3]} IS NULL)"
        in_where+=" ) "
      elsif !params[2].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) >= cast('#{params[2].to_date}' as date))"
      elsif !params[4].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[3]} as date) <= cast('#{params[4].to_date}' as date))"
      end
      in_where
    end
  
    def self.f_between (params, in_where, method=" AND ") 
    # Método para búsqueda de fecha entre dos fechas
    # Estructura 
    # params -> [0: "f_between",1: campo,2: valor1,3: valor2]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
      
      if !params[2].blank? && !params[3].blank? 
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) BETWEEN cast('#{params[2].to_date}' as date) AND cast('#{params[3].to_date}' as date))"
      elsif !params[2].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) >= cast('#{params[2].to_date}' as date))"
      elsif !params[3].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) <= cast('#{params[3].to_date}' as date))"
      end
      in_where
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      ""
    end
  
    def self.f_between_year(params, in_where, method=" AND ") 
    # Método para búsqueda de año entre dos años
    # Estructura 
    # params -> [0: "f_between_year",1: campo,2: valor1,3: valor2]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
  
      if !params[2].blank? && !params[3].blank?  
        in_where+=method unless in_where.blank?          
        in_where+="(cast(#{params[1]} as date) <= cast('#{Date.today-params[2].to_s.to_i.years}' as date) AND cast('#{params[1]}' as date) >= cast('#{Date.today-params[3].to_s.to_i.years}' as date))"
      elsif !params[2].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) <= cast('#{Date.today-params[2].to_s.to_i.years}' as date))"
      elsif !params[3].blank?
        in_where+=method unless in_where.blank?
        in_where+="(cast(#{params[1]} as date) >= cast('#{Date.today-params[3].to_s.to_i.years}' as date))"
      end
      in_where
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      ""
    end
  
    def self.join(params, in_where, method=" AND ", value=nil)
    # Método para búsqueda de un campo en varias tablas. Devuelve valores únicos
    # Estructura 
    # params -> [0: "join",1: <SQL join>,2: campo,3: <Tabla principal>,4*: valor]
    # value -> valor del string
    # in_where -> string que incluye la cadena que irá dentro del where de la query
      if value.blank?
        aux_val=params[4]
      else
        aux_val=value
      end
      
      unless aux_val.blank?
        aux_val = aux_val.split(',')
        sentence=""
        aux_val.each do |av|
          if sentence.blank?
            sentence="("
            sentence = sentence + "translate(UPPER(cast(#{params[2]} as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER('%#{av}%'), 'ÁÉÍÓÚ', 'AEIOU')"
          else
            sentence = sentence + " OR  translate(UPPER(cast(#{params[2]} as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER('%#{av}%'), 'ÁÉÍÓÚ', 'AEIOU')"
          end
        end
        sentence= sentence + ")" if !sentence.blank?
        if !sentence.blank?
          in_where+=method unless in_where.blank?
          in_where+="(#{params[3]}.id IN (#{params[1].where(sentence).select("#{params[3]}.id").distinct.to_sql}))"
        end
      end
      in_where
    end
    
    def tooltip(content, options = {}, html_options = {}, *parameters_for_method_reference)
      html_options[:title] = options[:tooltip]
      html_options[:class] = html_options[:class] || 'tooltip'
      content_tag("span", content, html_options)
    end
  
    def self.full(params, value, in_where, method=" AND ")
    # Método para búsqueda de un string en varios campos. Concatena los campos (CONCAT)
    # Estructura 
    # params -> ["full",campo1, campo2...]
    # value -> valor del string
    # in_where -> string que incluye la cadena que irá dentro del where de la query
      unless value.blank?
        in_where+=method unless in_where.blank?
        in_where+="("
        concatenacion=""
        (1..params.length).each do |i|
          unless params[i].blank?
            if i==1
              in_where+="(#{params[i].to_s} IS NOT NULL )"
              concatenacion+=params[i].to_s
            else
              in_where+= " AND "
              in_where+="(#{params[i].to_s} IS NOT NULL )"
              concatenacion+=",' ',"+params[i].to_s
            end
          end
        end
        in_where+=" AND translate(UPPER(cast(CONCAT(#{concatenacion}) as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER('%#{value}%'), 'ÁÉÍÓÚ', 'AEIOU')"
        in_where+=")"
      end
      in_where
    end
  
    def self.collective(params, in_where)
    # Método para búsqueda de un string en varios campos
    # Estructura 
    # params -> [0: "collective",1: [...],2: valor]
    # in_where -> string que incluye la cadena que irá dentro del where de la query
      unless params[2].blank?
        in_where+=" AND " unless in_where.blank?
        in_where+="("
        aux_in_where=""
        
        params[1].each do |aux|
          if aux.kind_of?(Array)
            case aux[0]
              when "full" 
                #Estructura ["collective",[...["full",campo1, campo2...]...],valor]
                aux_in_where=self.full(aux, params[2], aux_in_where," OR ")
              when "join"
                #Estructura ["collective",[...["join",<SQL join>,campo,<Tabla principal>]...],valor]
                aux_in_where=self.join(aux, aux_in_where," OR ", params[2])
              else
            end
          else  #Estructura ["collective",[...campo...],valor]
            aux_in_where=self.ilike(["ilike",aux,params[2]], aux_in_where," OR ")
           end
        end
        in_where+=aux_in_where
        in_where+=")"
      end
      in_where
    end #Fin collective
  end #Fin module
  