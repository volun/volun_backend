module RtProjectPublishingHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_project_publishing")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select 
      COALESCE(TRIM(CAST((select entities.name from entities,users 
        WHERE request_forms.user_id=users.id
        AND users.loggable_id= entities.id
        AND users.loggable_type='Entity') as varchar)),'-') as \"#{Project.human_attribute_name(:entity)}\",
      COALESCE(CAST(rt_project_publishings.notes as varchar),'-') as \"#{Rt::ProjectPublishing.human_attribute_name(:notes)}\",
      COALESCE(CAST(rt_project_publishings.description as varchar),'-') as \"#{Rt::ProjectPublishing.human_attribute_name(:description)}\",
      COALESCE(CAST(TRIM(CONCAT(
        rt_project_publishings.road_type,' ',
        rt_project_publishings.road_name,', ',
        rt_project_publishings.number_type,' ',
        rt_project_publishings.road_number,', ',
        rt_project_publishings.postal_code,', ',
        rt_project_publishings.borough,', ',
        rt_project_publishings.district,', ',
        rt_project_publishings.town,', ',
        (select p.name from provinces p where p.id=rt_project_publishings.province_id),', ')) as varchar),'-') as \"#{Rt::ProjectPublishing.human_attribute_name(:address)}\",
      COALESCE(TRIM(CAST((select entities.phone_number from entities,users 
        WHERE request_forms.user_id=users.id
        AND users.loggable_id= entities.id
        AND users.loggable_type='Entity') as varchar)),'-') as \"#{Project.human_attribute_name(:phone_number)}\",
      COALESCE(TRIM(CAST((select entities.email from entities,users 
        WHERE request_forms.user_id=users.id
        AND users.loggable_id= entities.id
        AND users.loggable_type='Entity') as varchar)),'-') as \"#{Project.human_attribute_name(:email)}\",
      COALESCE(CAST(DATE(rt_project_publishings.created_at)as varchar),'-') as \"#{Rt::ProjectPublishing.human_attribute_name(:created_at)}\",
      COALESCE(CAST((select DATE(projects.created_at) 
        FROM projects 
        WHERE request_forms.request_type_id=8 
        AND projects.id= rt_project_publishings.project_id
        AND request_forms.rt_extendable_id = rt_project_publishings.id) as varchar),'-') as \"#{Rt::ProjectPublishing.human_attribute_name(:publish_date_project)}\",
      CASE 
        WHEN request_forms.req_status_id IN ('3','4','2')
        THEN DATE_PART('day', request_forms.status_date - request_forms.created_at)
        WHEN request_forms.req_status_id IN ('1')
        THEN DATE_PART('day', CURRENT_TIMESTAMP - request_forms.created_at)
      END as \"#{Rt::ProjectPublishing.human_attribute_name(:days_passed)}\",
      COALESCE(CAST((
        SELECT TRIM(CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt))
        FROM managers, request_forms 
        WHERE request_forms.request_type_id=8 
        AND request_forms.rt_extendable_id = rt_project_publishings.id  
        AND managers.id=request_forms.manager_id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:manager)}\",
      COALESCE(CAST((select DATE(request_forms.status_date) 
        FROM request_forms 
        WHERE request_forms.request_type_id=8 
        AND request_forms.rt_extendable_id = rt_project_publishings.id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:status_date)}\",
      CASE (select req_statuses.description FROM req_statuses,request_forms WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=8
          AND request_forms.rt_extendable_id = rt_project_publishings.id) 
        WHEN 'pending' THEN '#{I18n.t('rt_proj_publish_filter.search_pending')}'
        WHEN 'processing' THEN '#{I18n.t('rt_proj_publish_filter.search_processing')}'
        WHEN 'approved' THEN '#{I18n.t('rt_proj_publish_filter.search_approved')}'
        WHEN 'rejected' THEN '#{I18n.t('rt_proj_publish_filter.search_rejected')}'
        END as \"#{RequestForm.human_attribute_name(:status)}\"
      FROM rt_project_publishings, request_forms 
        WHERE request_forms.request_type_id=8
        AND request_forms.rt_extendable_id = rt_project_publishings.id
        #{ "AND rt_project_publishings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end