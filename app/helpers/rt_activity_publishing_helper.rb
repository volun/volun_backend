module RtActivityPublishingHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_rt_activity_publish")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select 
      COALESCE(CAST((select entities.name from entities,request_forms,users 
        WHERE request_forms.request_type_id=10 
        AND request_forms.rt_extendable_id = rt_activity_publishings.id
        AND request_forms.user_id=users.id
        AND users.loggable_id= entities.id) as varchar),'-') as \"#{Project.human_attribute_name(:entity)}\",
      COALESCE(CAST((select entities.phone_number from entities,request_forms,users 
        WHERE request_forms.request_type_id=10 
        AND request_forms.rt_extendable_id = rt_activity_publishings.id
        AND request_forms.user_id=users.id
        AND users.loggable_id= entities.id) as varchar),'-') as \"#{Project.human_attribute_name(:phone_number)}\",
      COALESCE(CAST((select entities.email from entities,request_forms,users 
        WHERE request_forms.request_type_id=10 
        AND request_forms.rt_extendable_id = rt_activity_publishings.id
        AND request_forms.user_id=users.id
        AND users.loggable_id= entities.id) as varchar),'-') as \"#{Project.human_attribute_name(:email)}\",
      COALESCE(CAST(rt_activity_publishings.name as varchar),'-') as \"#{Rt::ActivityPublishing.human_attribute_name(:name)}\",
      COALESCE(CAST(rt_activity_publishings.description as varchar),'-') as \"#{Rt::ActivityPublishing.human_attribute_name(:description)}\",
      COALESCE(CAST(rt_activity_publishings.organizer as varchar),'-') as \"#{Rt::ActivityPublishing.human_attribute_name(:organizer)}\",
      COALESCE(CAST(DATE(rt_activity_publishings.created_at)as varchar),'-') as \"#{Rt::ActivityPublishing.human_attribute_name(:created_at)}\",
      COALESCE(CAST((select activities.created_at from activities where activities.id = rt_activity_publishings.activity_id) as varchar),'-') as \"#{Rt::ActivityPublishing.human_attribute_name(:publishing_date_activity)}\",
      CASE 
      WHEN ((select req_statuses.id FROM req_statuses,request_forms 
          WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id) ='3') 
        OR ((select req_statuses.id FROM req_statuses,request_forms 
          WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id)='4') 
        OR ((select req_statuses.id FROM req_statuses,request_forms 
          WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id)='2') 
        THEN DATE_PART('day', 
        (select request_forms.status_date from request_forms,req_statuses 
          WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id) - (select request_forms.created_at from req_statuses,request_forms 
            WHERE req_statuses.id=request_forms.req_status_id
            AND request_forms.request_type_id=10 
            AND request_forms.rt_extendable_id = rt_activity_publishings.id))
      WHEN ((select req_statuses.id FROM req_statuses,request_forms 
          WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id) ='1') 
        THEN DATE_PART('day', CURRENT_DATE - (select request_forms.created_at from req_statuses,request_forms 
            WHERE req_statuses.id=request_forms.req_status_id
            AND request_forms.request_type_id=10 
            AND request_forms.rt_extendable_id = rt_activity_publishings.id))
      END as \"#{Rt::ActivityPublishing.human_attribute_name(:days_passed)}\",
      COALESCE(CAST((
        SELECT TRIM(CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt))
        FROM managers, request_forms 
        WHERE request_forms.request_type_id=10 
        AND request_forms.rt_extendable_id = rt_activity_publishings.id  
        AND managers.id=request_forms.manager_id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:manager)}\",
      COALESCE(CAST((select DATE(request_forms.status_date) 
        FROM request_forms 
        WHERE request_forms.request_type_id=10 
        AND request_forms.rt_extendable_id = rt_activity_publishings.id)as varchar),'-') as \"#{RequestForm.human_attribute_name(:status_date)}\",
      CASE (select req_statuses.description FROM req_statuses,request_forms WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=10 
          AND request_forms.rt_extendable_id = rt_activity_publishings.id) 
        WHEN 'pending' THEN '#{I18n.t('rt_activity_filter.search_pending')}'
        WHEN 'processing' THEN '#{I18n.t('rt_activity_filter.search_processing')}'
        WHEN 'approved' THEN '#{I18n.t('rt_activity_filter.search_approved')}'
        WHEN 'rejected' THEN '#{I18n.t('rt_activity_filter.search_rejected')}'
        END as \"#{RequestForm.human_attribute_name(:status)}\"
      FROM rt_activity_publishings #{ "where rt_activity_publishings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end