module DerivatiesVolunteersHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_derivaties")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct activities.id as \"#{Activity.human_attribute_name(:id)}\",
        COALESCE(CAST(activities.name as varchar),'-') as \"#{Activity.human_attribute_name(:name)}\",
        COALESCE(CAST(activities.description as varchar),'-') as \"#{Activity.human_attribute_name(:description)}\",
        COALESCE(CAST(activities.start_date as varchar),'-') as \"#{Activity.human_attribute_name(:start_date)}\",
        COALESCE(CAST(activities.end_date as varchar),'-') as \"#{Activity.human_attribute_name(:end_date)}\",
        COALESCE(CAST(activities.transport as varchar),'-') as \"#{Activity.human_attribute_name(:transport)}\",
        COALESCE(CAST(activities.organizers as varchar),'-') as \"#{Activity.human_attribute_name(:organizers)}\",
        
        COALESCE(CAST((select activities.name from activities where activities.id = activities.project_id) as varchar),'-') as \"#{I18n.t('activerecord.models.activity.project')}\",
        CASE activities.share WHEN 'false' THEN 'No'
        WHEN 'true' THEN 'Sí' END as \"#{Activity.human_attribute_name(:share)}\" 
      FROM activities #{ "where activities.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end