module DistrictsHelper
  

  def getBoroughts(district=nil)
    return ["",""] if district.blank?

    district_code = Address.district_list.find_by(id: district).try(:code).to_s.rjust(2, '0')
    Address.borougth_list(district_code).select(:name,:id).map {|i| [i.name,i.id]}
  rescue
    ["",""]
  end

 
end
