module AuditGenerateHelper

    def self.input_parameters(service, resource, current_user)
        item = AuditGenerateHelper.get_ws_item(service, resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.generate(attr_options={})
        Audit.create(attr_options) if !attr_options.blank? && !attr_options[:resource_id].blank?
    rescue 
        false
    end

    def self.audit_create(resource, current_user)
        item = AuditGenerateHelper.get_item("register", resource, current_user)
        if resource.save
            item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
            AuditGenerateHelper.generate(item)
            true
        else
            false
        end
    rescue => e
        begin
            Rails.logger.error("ERROR-AUDIT: CREATE -> #{e.message.join("\n")}#{e.backtrace.join("\n")}")
        rescue
        end
        false
    end

    def self.audit_update(resource, strong_params, current_user, permit_strong = true, changed_status = false)
        item = AuditGenerateHelper.get_item("update", resource, current_user)
        
        item[:old_data] = JSON.parse(resource.class.find(resource.id).to_json)
        resource.assign_attributes(strong_params)
        if !resource.blank? && !resource.try(:project).blank?
            project = resource.project 
            changed_status = project.suspended_changed? 
        end


        if permit_strong && resource.valid? && resource.save
            project.update_dates if changed_status
            item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
            AuditGenerateHelper.generate(item)
            true
        elsif !permit_strong  && resource.valid? && resource.save
            project.update_dates if changed_status
            item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
            AuditGenerateHelper.generate(item)
            true
        else      
            false
        end

    
    rescue => e
        begin
            Rails.logger.error("ERROR-AUDIT: UPDATE -> #{e.message.join("\n")}#{e.backtrace.join("\n")}")
        rescue
        end
        false
    end

    def self.audit_destroy(resource, current_user, inside = false)
        item = AuditGenerateHelper.get_item("delete", resource, current_user)
        begin
            item[:old_data] = JSON.parse(resource.class.find(resource.id).to_json)
        rescue
        end
        if !inside
            if resource.destroy
                AuditGenerateHelper.generate(item)
                true
            else
                false
            end
        else
            if resource.destroy(item)
                AuditGenerateHelper.generate(item)
                true
            else
                false
            end
        end
    rescue 
        false
    end

    def self.audit_recover(resource, current_user, inside=false)
        item = AuditGenerateHelper.get_item("recover", resource, current_user)
        if !inside
            begin
                item[:old_data] = JSON.parse(resource.class.find(resource.id).to_json)
            rescue
            end
            if resource.recover
                item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
                AuditGenerateHelper.generate(item)
                true
            else
                false
            end
        else
            resource.recover(item)
        end
    rescue 
        false
    end


    def self.audit_archive(resource, current_user, inside = false)
        item = AuditGenerateHelper.get_item("archive", resource, current_user)
        if !inside
            begin
                item[:old_data] = JSON.parse(resource.class.find(resource.id).to_json)
            rescue
            end
            if resource.destroy
                item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
                AuditGenerateHelper.generate(item)
                true
            else
                resource.errors.full_messages
                false
            end
        else
            resource.destroy(item)
        end
    rescue 
        false
    end

    def self.audit_clone(resource, current_user)
        item = AuditGenerateHelper.get_item("clone", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_unlink_all(resource, current_user)
        item = AuditGenerateHelper.get_item("unlink_all", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_publish(resource, current_user)
        item = AuditGenerateHelper.get_item("publish", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item) 
    rescue 
        false
    end

    def self.audit_unpublish(resource, current_user)
        item = AuditGenerateHelper.get_item("unpublish", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_request_appointed(resource, current_user)
        item = AuditGenerateHelper.get_item("request_appointed", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_request_accept(resource, current_user)
        item = AuditGenerateHelper.get_item("request_accept", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_request_process(resource, current_user)
        item = AuditGenerateHelper.get_item("process", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_request_reject(resource, current_user)
        item = AuditGenerateHelper.get_item("request_reject", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_request_pending(resource, current_user)
        item = AuditGenerateHelper.get_item("request_pending", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end
    
    def self.audit_request_accept(resource, current_user)
        item = AuditGenerateHelper.get_item("request_accept", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end
    
    def self.audit_request_form_as_processing(resource, current_user)
        item = AuditGenerateHelper.get_item("request_form_as_processing", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_unlink(resource, current_user)
        item = AuditGenerateHelper.get_item("unlink", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_send_mail(resource, current_user)
        item = AuditGenerateHelper.get_item("email_send", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_send_sms(resource, current_user)
        item = AuditGenerateHelper.get_item("send_sms", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_unassignment(resource, current_user)
        item = AuditGenerateHelper.get_item("unassignment", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.audit_assignment(resource, current_user)
        item = AuditGenerateHelper.get_item("assignment", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end

    def self.reset_password(resource, current_user)
        item = AuditGenerateHelper.get_item("reset_password", resource, current_user)
        item[:new_data] = JSON.parse(resource.try {|x| x.attributes.to_json})
        AuditGenerateHelper.generate(item)
    rescue 
        false
    end


    def self.get_item(type, resource, current_user)
        {
            operation_type: "#{I18n.t("audit.#{type}")}", 
            user_id: current_user.try(:id),
            resource_id: Resource.find_by(name: resource.class.name).try(:id),
            operation: "#{I18n.t("audit_enum.#{type}", id: resource.try(:id))}",
            new_data: '',
            old_data: ''
        }
    rescue 
        false
    end

    def self.get_ws_item(type, resource, current_user)
        {
            operation_type: "#{I18n.t("audit.#{type}")}", 
            user_id: current_user.try(:id),
            resource_id: Resource.find_by(name: resource.class.name).try(:id),
            operation: "#{I18n.t("audit_enum.#{type}", id: resource.try(:id))}",
            new_data: '',
            old_data: ''
        }
    rescue 
        false
    end

end