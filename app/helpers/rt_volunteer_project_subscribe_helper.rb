module RtVolunteerProjectSubscribeHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_volun_proj_subscribe")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select  
      COALESCE(CAST(rt_volunteer_project_subscribes.name as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:name)}\",
      COALESCE(CAST(rt_volunteer_project_subscribes.last_name as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:last_name)}\", 
      COALESCE(CAST(rt_volunteer_project_subscribes.last_name_alt as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:last_name_alt)}\",
      COALESCE(CONCAT(CAST(rt_volunteer_project_subscribes.phone_number as varchar),'/',CAST(rt_volunteer_project_subscribes.phone_number_alt as varchar)),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:phone_numbers)}\",
      COALESCE(CAST(rt_volunteer_project_subscribes.email as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:email)}\",
      COALESCE(CAST((select d.name from districts d where d.id=rt_volunteer_project_subscribes.district_id) as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:district)}\",
      COALESCE(CAST(rt_volunteer_project_subscribes.postal_code as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:postal_code)}\",
      COALESCE(CAST(rt_volunteer_project_subscribes.created_at as varchar),'-') as \"#{Rt::VolunteerProjectSubscribe.human_attribute_name(:created_at)}\",      
      CASE 
        WHEN request_forms.req_status_id IN ('3','4','2')
        THEN DATE_PART('day', request_forms.status_date - request_forms.created_at)
        WHEN request_forms.req_status_id IN ('1')
        THEN DATE_PART('day', CURRENT_TIMESTAMP - request_forms.created_at)
      END as \"#{Rt::Other.human_attribute_name(:days_passed)}\",
      COALESCE(CAST((
        SELECT TRIM(CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt))
        FROM managers, request_forms 
        WHERE request_forms.request_type_id= 14
        AND request_forms.rt_extendable_id = rt_volunteer_project_subscribes.id  
        AND managers.id=request_forms.manager_id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:manager)}\",
      COALESCE(CAST((select DATE(request_forms.status_date) 
        FROM request_forms 
        WHERE request_forms.request_type_id=14
        AND request_forms.rt_extendable_id = rt_volunteer_project_subscribes.id)as varchar),'-') as \"#{RequestForm.human_attribute_name(:status_date)}\",
      CASE (select req_statuses.description FROM req_statuses,request_forms WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=14 
          AND request_forms.rt_extendable_id = rt_volunteer_project_subscribes.id) 
        WHEN 'pending' THEN '#{I18n.t('rt_volun_proj_subscribe_filter.search_pending')}'
        WHEN 'processing' THEN '#{I18n.t('rt_volun_proj_subscribe_filter.search_processing')}'
        WHEN 'approved' THEN '#{I18n.t('rt_volun_proj_subscribe_filter.search_approved')}'
        WHEN 'rejected' THEN '#{I18n.t('rt_volun_proj_subscribe_filter.search_rejected')}'
        END as \"#{RequestForm.human_attribute_name(:status)}\"
      FROM rt_volunteer_project_subscribes, request_forms 
        WHERE request_forms.request_type_id=14 
        AND request_forms.rt_extendable_id = rt_volunteer_project_subscribes.id
        #{ "AND rt_volunteer_project_subscribes.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end