module Digital::TopicHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_digital_topics")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct df.id as \"#{Digital::Topic.human_attribute_name(:id)}\",
        COALESCE(CAST(df.title as varchar),'-') as \"#{Digital::Topic.human_attribute_name(:title)}\",
        COALESCE(CAST(df.body as varchar),'-') as \"#{Digital::Topic.human_attribute_name(:description)}\",       
        case when df.active=true then 'Sí' 
            when  df.active=false then 'No'
            else '-' end as \"#{Digital::Topic.human_attribute_name(:active)}\",
        COALESCE(CAST((select projects.name from projects where projects.id = df.project_id) as varchar),'-') as \"#{Digital::Topic.human_attribute_name(:project)}\",
        (Select count(1) from digital_comments dc where dc.digital_topic_id=df.id) as \"#{Digital::Topic.human_attribute_name(:n_comments)}\",
        (Select count(1) from digital_follows dc where dc.digital_topic_id=df.id) as \"#{Digital::Topic.human_attribute_name(:n_follows)}\",
        (Select sum(cast(dc.value as int)) from digital_votes dc where dc.votable_id=df.id and dc.votable_type='Digital::Topic') as \"#{Digital::Topic.human_attribute_name(:votes)}\"
      FROM digital_topics as df #{ "where df.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end