module Digital::ModerateHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    # model = items.model
    # filtrado = StreamingHelper.filter(model, items, params, "header_list_digital_moderates")
    # begin
    #   filtrado[:header].each {|x| yield x if block_given? }
    # rescue
    # end
    
    return if items.blank?
    query = "COPY (
      Select distinct df.id as \"ID\",
      COALESCE(CAST((select p.name from projects p, digital_topics dt where p.id = dt.project_id and dt.id= df.digital_topic_id) as varchar),'-') as \"Proyecto\",
      'Comentario' as \"Tipo\",
      '-' as \"Título del tema\",       
      COALESCE(CAST(df.body as varchar),'-') as \"Descripción del tema/Comentario\",  
      COALESCE(CAST(ms.title as varchar),'-') as \"Estado\",   
      COALESCE(CAST((select Concat(m.name, ' ', m.last_name,' ',m.last_name_alt) from managers m, users u  where m.id= u.loggable_id and u.loggable_type ='Manager' and
      u.id=df.moderable_id and df.moderable_type='User') as varchar),'-') as \"Moderador\"
    FROM digital_comments as df,moderate_statuses ms where ms.id=df.moderate_status_id #{ " and ms.code = '#{params[:type]}'" if !params[:type].blank?}
    union
     Select distinct df.id as \"ID\",
      COALESCE(CAST((select p.name from projects p where p.id = df.project_id) as varchar),'-') as \"Proyecto\",
      'Tema de debate' as \"Tipo\",
      COALESCE(CAST(df.title as varchar),'-') as \"Título del tema\",       
      COALESCE(CAST(df.body as varchar),'-') as \"Descripción del tema/Comentario\",  
      COALESCE(CAST(ms.title as varchar),'-') as \"Estado\",   
      COALESCE(CAST((select Concat(m.name, ' ', m.last_name,' ',m.last_name_alt) from managers m, users u  where m.id= u.loggable_id and u.loggable_type ='Manager' and
      u.id=df.moderable_id and df.moderable_type='User') as varchar),'-') as \"Moderador\"
    FROM digital_topics as df,moderate_statuses ms where ms.id=df.moderate_status_id #{ " and ms.code = '#{params[:type]}'" if !params[:type].blank?})   
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end