module Digital::FaqHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_digital_faqs")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct df.id as \"#{Digital::Faq.human_attribute_name(:id)}\",
        COALESCE(CAST(df.order as varchar),'-') as \"#{Digital::Faq.human_attribute_name(:order)}\",
        COALESCE(CAST(df.title as varchar),'-') as \"#{Digital::Faq.human_attribute_name(:title)}\",
        COALESCE(CAST(df.description as varchar),'-') as \"#{Digital::Faq.human_attribute_name(:description)}\",       
        COALESCE(CAST((select projects.name from projects where projects.id = df.project_id) as varchar),'-') as \"#{Digital::Faq.human_attribute_name(:project)}\"
      FROM digital_faqs as df #{ "where df.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end