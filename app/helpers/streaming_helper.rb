module StreamingHelper
    extend ActiveSupport::Concern

    def self.filter(model, list, params, title)
        head_filter = ""
        begin
            model.get_filter(params).each { |key| head_filter=head_filter+"#{key}, " }
        rescue
        end
        begin
            aux = []

            aux.push(I18n.t('csv_export.header_filter').force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''})+"\n")
            aux.push(head_filter.gsub("null","").force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''})+"\n")
            aux.push("\n")
            aux.push("\n"+ I18n.t("csv_export.#{title}").force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''})+"\n")
            
            
            filter = {header: aux}
            return filter if list.blank?
            if model.to_s != "Citizen"
                if list.kind_of?(Array) || !list.try(:count).blank?
                    filter.merge!({filter: list.map(&:id).to_s.gsub(/\[|\]/,"")})
                else
                    filter.merge!({filter: list.id})
                end
            end
        rescue => e
            begin
                Rails.logger.error("ERROR-HELPER: #{e.message}] #{e.backtrace.join("\n")}")
              rescue
              end
            {header: []}
        end
    end
   
    def self.execute_stream_query(query)
        aux = []
        conn = ActiveRecord::Base.connection.raw_connection
        conn.copy_data "#{query}" do
            
            while row = conn.get_copy_data          
              aux.push(row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}))
            end
        end 

        aux
    rescue => e
        begin
            Rails.logger.error("ERROR-HELPER2: #{e.message}] #{e.backtrace.join("\n")}")
          rescue
          end
        []
    end
end