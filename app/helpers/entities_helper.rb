module EntitiesHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_entities")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct 
        COALESCE(CAST(entities.name as varchar),'-') as \"#{Project.human_attribute_name(:name)}\",
        COALESCE(CAST(entities.vat_number as varchar),'-') as \"#{I18n.t('activerecord.attributes.entity.vat_number')}\",
        COALESCE(CAST(entities.email as varchar),'-') as \"#{Event.human_attribute_name(:email)}\",
        COALESCE(CAST((select entity_types.name from entity_types where entity_types.id = entities.entity_type_id) as varchar),'-') as \"#{Project.human_attribute_name(:entity)}\"
      FROM entities #{ "where entities.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
