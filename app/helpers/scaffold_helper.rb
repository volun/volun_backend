module ScaffoldHelper

  def model_title(model_class, opts = {})
    main_title(model_class.model_name.human(count: 2), opts)
  end

  def main_title(title, opts = {})
    html_tag = opts.delete(:html_tag) || :h1
    content_tag(html_tag, title, { class: 'page-header' }.merge(opts))
  end

  def search_collection(search, search_condition, options = {})
    form =  search_form_for search, class: 'form-inline search-form', role: 'form', wrapper: :horizontal_form do |f|
              (get_hidden_fields options.delete(:hidden_fields) || {}) +
                (get_q_hidden_fields options.delete(:q_hidden_fields) || {}) +
                (hidden_field_tag :per_page, params[:per_page]) +
                (f.search_field search_condition, class: 'form-control', placeholder: t('type_text')) +
                (button_tag name: 'commit', class: 'btn btn-default' do
                  ((fa_icon 'search')).html_safe
                end)
            end
    form.html_safe
  end

  def search_collection_alt(search, search_condition, search_condition_alt, options = {})
    form =  search_form_for search, class: 'form-inline search-form', role: 'form', wrapper: :horizontal_form do |f|
              (get_hidden_fields options.delete(:hidden_fields) || {}) +
                (get_q_hidden_fields options.delete(:q_hidden_fields) || {}) +
                (hidden_field_tag :per_page, params[:per_page]) +
                (f.search_field search_condition_alt, class: 'form-control', placeholder: t('id')) +
                (f.search_field search_condition, class: 'form-control', placeholder: t('type_text')) +
                (button_tag name: 'commit', class: 'btn btn-default' do
                  ((fa_icon 'search')).html_safe
                end)
            end
    form.html_safe
  end

  def get_hidden_fields(hidden_fields)
    html_tag = ''
    grouped_hidden_fields = hidden_fields.select{ |_k, v| v.is_a? Hash }
    grouped_hidden_fields.each do |group_name, _hidden_fields|
      html_tag += content_tag(:div, id: group_name) do
                    build_hidden_fields(_hidden_fields)
                  end
    end
    independent_hidden_fields = hidden_fields.reject{ |_k, v| v.is_a? Hash }
    html_tag += build_hidden_fields independent_hidden_fields
    html_tag.html_safe
  end

  def build_hidden_fields(hidden_fields)
    hidden_fields.inject('') do |hf_tags, (name, value)|
      if value.is_a?(Array)
        value.each do |v|
          hf_tags += hidden_field_tag(build_name_attr(name), v, class: name.to_s.gsub(/\[|\]/, '_').sub(/_+\z/, ''))
        end
        hf_tags
      else
        hf_tags + hidden_field_tag(build_name_attr(name), value)
      end
    end.html_safe
  end

  def get_q_hidden_fields(q_hidden_fields)
    html_tag = ''
    grouped_q_hidden_fields = q_hidden_fields.select{ |_k, v| v.is_a? Hash }
    grouped_q_hidden_fields.each do |group_name, _q_hidden_fields|
      html_tag += content_tag(:div, id: group_name) do
                    build_q_hidden_fields(_q_hidden_fields)
                  end
    end
    independent_q_hidden_fields = q_hidden_fields.reject{ |_k, v| v.is_a? Hash }
    html_tag += build_q_hidden_fields independent_q_hidden_fields
    html_tag.html_safe
  end

  def build_q_hidden_fields(hidden_fields)
    hidden_fields.inject('') do |hf_tags, (name, value)|
      hf_tags + hidden_field_tag("q[#{name}]", value)
    end.html_safe
  end

  def convert_filters_to_params(filters, params)
    filters ||= {}
    filters.each do |k,v|
      params.merge(k.sub(/_[^_]+\z/,'') => v)
    end
    params = params.permit(:link_type_id, :linkable_id, :linkable_type, :per_page_list, :per_page, :q, :per_page_list, :q)
  end

  def build_name_attr(name)
    name = name.to_s
    /\Aq_(.*)\z/i === name ? "q[#{name.sub('q_', '')}]" : name
  end

  def show_simple_base_errors(form)
    return unless form.object.errors[:base].present?

    content_tag :div, class: 'has-error alert alert-danger alert-dismissable' do
      "<button name=\"button\" type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>" \
      "#{form.object.errors[:base].to_sentence}".html_safe
    end
  end

  def link_to_download_rt(model, opts = {})
    return unless can?(:read, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:   "#{content_tag(:span, t("action.download_csv"))}".html_safe,
        path:   "download_request_#{model.model_name.plural}_path",
        remote: false,
        class:  'btn btn-primary',
          id: "download_request"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_massive_email(model, opts = {})
    return unless can?(:mail, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:   "#{content_tag(:span, t("action.massive_email"))}".html_safe,
        path:   "massive_email_#{model.model_name.plural}_path",
        remote: false,
        style: "padding: 10px 10px;",
        class:  'btn btn-primary',
        #data: { turbolinks: false },
        id:     "massive_email_#{model.model_name.singular}_link"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_create_meeting(model, opts = {})
    return unless can?(:create, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:   "#{content_tag(:span, t("action.meetings"))}".html_safe,
        path:   "meeting_#{model.model_name.singular}_path",
        remote: false,
        style: "padding: 10px 10px;",
        class:  'btn btn-primary',
        data: { turbolinks: false },
        id:     "meeting_#{model.model_name.singular}_link"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_turn(model, opts = {})
    return unless can?(:create, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:  "#{content_tag(:span, model.model_name.human(count: 2))}".html_safe,
        path:   "#{model.model_name.plural}_path",
        remote: false,
        class:  'btn btn-primary',
        id:     "#{model.model_name.plural}_link"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_new(model, opts = {})
    return unless can?(:create, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:   "#{fa_icon('plus')} #{content_tag(:span, t("action.#{new_i18n_path}", model: model.model_name.human))}".html_safe,
        path:   opts[:path].blank? ? "new_#{model.model_name.singular}_path" : opts[:path],
        remote: false,
        class:  'btn btn-primary',
        id:     "new_#{model.model_name.singular}_link"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_new_citizen(model, type, opts = {})
    return unless can?(:create, model)
    new_i18n_path = opts.delete(:fem).present? ? 'new_fem' : 'new'
      options = {
        text:   "#{fa_icon('plus')} #{content_tag(:span, t("action.#{new_i18n_path}", model: model.model_name.human))}".html_safe,
        path:   "new_#{type}_#{model.model_name.singular}_path",
        remote: false,
        class:  'btn btn-primary',
        id:     "new_#{model.model_name.singular}_link"
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, options.delete(:path_params)||{}), options)
  end

  def link_to_show(record, opts = {})
    return unless can?(:read, record)
    options = {
        text: icon_show,
        path: opts[:path].blank? ? "#{record.class.model_name.singular}_path" : opts[:path],
        class: 'black-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_show_citizen(record,type, opts = {})
    return unless can?(:read, record)
    options = {
        text: icon_show,
        path: "#{type}_#{record.class.model_name.singular}_path",
        class: 'black-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_show_origin(record, opts = {})
    return unless can?(:read, VirtualCommunity)
    options = {
        text: icon_show,
        path: opts[:path].blank? ? "show_origin_virtual_communities_path" :  opts[:path],
        class: 'grey-color',
        remote: false
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_show_popular(record, opts = {})
    return unless can?(:read, VirtualCommunity)
    options = {
        text: icon_show,
        path: "show_popular_virtual_communities_path",
        class: 'grey-color',
        remote: false
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_question(record, opts = {})
    return unless can?(:read, record)
    options = {
        text: icon_question,
        path: "question_#{record.class.model_name.singular}_path",
        class: 'grey-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_services(record, opts = {})
    return unless can?(:read, record)
    options = {
        text: icon_services,
        path: "#{record.class.model_name.singular}_citizen_services_path",
        class: 'grey-color',
        remote: false,
        data: { turbolinks: false }
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_create_meeting(record, opts = {})
    return unless can?(:create, record)
    options = {
        text: icon_meeting,
        path: "create_meeting_#{record.class.model_name.singular}_path",
        class: 'grey-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_assign_turn(record, opts = {})
    return unless can?(:update, record)
    options = {
        text: icon_turn,
        path: "assign_turn_#{record.class.model_name.singular}_path",
        class: 'grey-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_csv(record, opts = {})
    return unless can?(:read, record)
    options = {
        id:     "#{dom_id(record)}_csv",
        text:   icon_download_csv,
        path:   "#{record.class.model_name.singular}_path",
        remote: false,
        class:  'black-color',
        path_params: {format: :csv}        
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end

  def link_to_csv_citizen(record,type, opts = {})
    return unless can?(:read, record)
    options = {
        id:     "#{dom_id(record)}_csv",
        text:   icon_download_csv,
        path:   "#{type}_#{record.class.model_name.singular}_path",
        remote: false,
        class:  'black-color',
        path_params: {format: :csv}        
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end

  def link_to_edit(record, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_edit",
        text:   icon_edit,
        path:   opts[:path].blank? ? "edit_#{record.class.model_name.singular}_path" : opts[:path],
        remote: false,
        class:  'black-color'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end

  def link_to_edit_citizen(record,type, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_edit",
        text:   icon_edit,
        path:   "edit_#{type}_#{record.class.model_name.singular}_path",
        remote: false,
        class:  'black-color'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end

  def link_to_edit_unsubscribe(record, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_edit",
        text:   icon_edit,
        path:   "edit_#{record.class.model_name.singular}_path",
        class:  'black-color',
        data:   {confirm_swal: t('messages.edit_unsubscribe',name: record.name, last_name: record.last_name)}
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end
  def link_to_edit_unsubscribe_derivatie(record, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_edit",
        text:   icon_edit,
        path:   "edit_#{record.class.model_name.singular}_path",
        class:  'black-color',
        data:   {confirm_swal: t('messages.edit_unsubscribe',name: record.name, last_name: record.last_name)}
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record,options[:path_params]||{}), options)
  end
  def link_to_public (campaing, opts = {})
    options = {
      id:     "#{dom_id(campaing)}_edit",
      text:   icon_public,
      path:   opts[:path].blank? ? "change_public_#{campaing.class.model_name.singular}_path" : opts[:path],
      remote: false,
      class:  'grey-color'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to( text,public_send(path, campaing.id,options[:path_params]||{}), options)
  end
  def link_to_un_public (campaing, opts = {})
    options = {
      id:     "#{dom_id(campaing)}_edit",
      text:   icon_unlink,
      path:   opts[:path].blank? ? "change_public_#{campaing.class.model_name.singular}_path" : opts[:path],
      remote: false,
      class:  'grey-color'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to( text,public_send(path, campaing.id,options[:path_params]||{}), options)
  end
  def link_to_download(volum_export, opts = {})
    options = {
      text:   volum_export.is_export ? icon_download_list : icon_download_no_export_list,
      path:   "download_#{volum_export.class.model_name.singular}_path",
      remote: false,
      path_params: {format: :csv},
      class:  'grey-color'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to( text,public_send(path, volum_export,options[:path_params]||{}), options)
  end
  def link_to_return_edit(record, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_edit",
        text:   "Volver",
        path:   "edit_#{record.class.model_name.singular}_path",
        path_params: params[:pt_extendable_id],
        remote: false,
        class:  'btn btn-default'
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_destroy(record, opts = {})
    return unless can?(:destroy, record)
    options = {
        id:     "#{dom_id(record)}_destroy",
        text:   icon_destroy,
        path:   opts[:path].blank? ? "#{record.class.model_name.singular}_path" : opts[:path],        
        data:   {confirm_swal: t('messages.are_you_sure'), method: :delete, remote: true, reload: true }
    }.merge(opts)
    #confirm_swal: t('messages.are_you_sure')
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_destroy_volunteer(record, opts = {})
    return unless can?(:destroy, record)
    options = {
      text: icon_destroy,
      path:  "show_unsubscribe_volunteer_path",
      class: 'grey-color',
      path_params: { volunteer: record },
      remote: true
      
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_clone_project(record, opts = {})
    return unless can?(:clone_project, record)
    options = {
        text:   icon_clone,
        path:   "pre_clone_#{record.class.model_name.singular}_path",
        remote: true         
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_password_ent(record, opts = {})
    return unless can?(:create, record)
    options = {
        text:   t('generate_password'),
        path:   "ent_password_#{record.class.model_name.singular}_path",
        class:  "btn btn-default",
        remote: true         
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_password_volun(record, opts = {})
    return unless can?(:create, record)
    options = {
        text:   t('generate_password'),
        path:   "volun_password_volunteer_path",
        class:  "btn btn-default",
        remote: true         
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_password_citizen(record, opts = {})
    return unless can?(:create, record)
    options = {
        text:   icon_password,
        path:   "citizen_password_citizen_path",
        class:  "grey-color",
        remote: true         
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_unlink_derivaties_volunteer(record, opts = {})
    return unless can?(:update, record)
    options = {
      id:     "#{dom_id(record)}_unlink",
      text:  icon_unlink,
      path:  "unlink_#{record.class.model_name.singular}_path",
      class: 'grey-color',
      path_params: { volunteer: record },
      remote: false,
      data:   {confirm_swal: t('messages.unlink_derivaties_volunteer',volunteer: "#{record.volunteer.name} #{record.volunteer.last_name} #{record.volunteer.last_name_alt}",project: record.project.name), reload: true} 
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_unlink(record, opts = {})
    return unless can?(:update, record)
    options = {
        path:   "volun_unlink_#{record.class.model_name.singular}_path",
        remote: true,
        data:   {confirm_swal: t('messages.unlink_volunteers',name: record.name), reload: true}
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end
  
  def button_to_unlink(record, opts = {})
    link_to_unlink(record, { text: t('unlink_volunteers'), class: "btn btn-primary", style: "
      padding: 10px 10px;" }.merge(opts))
  end

  def link_to_remove(record, opts = {})
    return unless can?(:destroy, record)
    options = {
        text:   icon_remove,
        path:   "remove_project_#{record.class.model_name.singular}_path",
        remote: false,
        class:  'grey-color',
        data:   {confirm_swal: t('messages.remove_project',name: record.name), reload: true}

         
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_recover(record, opts = {})
    return unless can?(:destroy, record)
    options = {
        id:     "#{dom_id(record)}_recover",
        text:   icon_recover,
        path:   opts[:path].blank? ? "recover_#{record.class.model_name.singular}_path" : opts[:path],
        data:   {confirm_swal: t('messages.are_you_sure'), method: :post, remote: true, reload: true}
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_recover_project(record,project_name, opts = {})
    return unless can?(:destroy, record)
    options = {
        id:     "#{dom_id(record)}_recover",
        text:   icon_recover,
        path:   "show_project_volunteer_#{record.class.model_name.singular}_path",
        path_params: { volunteer: record },
        remote: true
       
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)
    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_associate_derivatie(record,project_name, opts = {})
    return unless can?(:update, record)
    options = {
        text:   icon_check_circle,
        path:   "associate_derivatie_#{record.class.model_name.singular}_path",
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_recover_project_derivaty(record,project_name, opts = {})
    return unless can?(:destroy, record)
    options = {
        id:     "#{dom_id(record)}_recover",
        text:   icon_recover,
        path:   "show_project_derivaty_#{record.class.model_name.singular}_path",
        remote: false,
        data:   {confirm_swal: t('messages.are_you_sure_project_derivaty',volunteer: "#{record.name} #{record.last_name} #{record.last_name_alt}",project: project_name), reload: true} 
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_destroy_volunteer_project(record,project_name, opts = {})
    return unless can?(:destroy, record)
    options = {
      id:     "#{dom_id(record)}_destroy",
      text: icon_destroy,
      path:  "show_project_volunteer_#{record.class.model_name.singular}_path",
      class: 'grey-color',
      path_params: { volunteer: record },
      remote: true
      
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_destroy_project_derivaty(record,project_name, opts = {})
    return unless can?(:destroy, record)
    options = {
      id:     "#{dom_id(record)}_destroy",
      text: icon_destroy,
      path:  "show_project_derivaty_#{record.class.model_name.singular}_path",
      class: 'grey-color',
      path_params: { volunteer: record },
      remote: false,
      data:   {confirm_swal: t('messages.are_you_sure_unproject_derivaty',volunteer: "#{record.name} #{record.last_name} #{record.last_name_alt}",project: project_name), reload: true} 
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_unlink_volunteer_project(record,project_name, opts = {})
    
    return unless can?(:update, record)
    options = {
      id:     "#{dom_id(record)}_unlink",
      text: icon_unlink,
      path:  "unlink_project_volunteer_project_#{record.class.model_name.singular}_path",
      class: 'grey-color',
      path_params: { volunteer: record },
      remote: true,
      data:   {confirm_swal: t('messages.unlink_volunteer_project',volunteer: "#{record.name} #{record.last_name} #{record.last_name_alt}",project: project_name), reload: true} 
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def link_to_trackings(record, type, project = nil, opts = {})
    return unless can?(:read, "#{type.titleize}::Tracking".safe_constantize)
    options = {
        id:     "#{dom_id(record)}_trackings",
        text:   icon_tracking,
        path:   "#{type}_trackings_path",
        path_params: {q: {"#{record.class.model_name.singular}_id_eq": record}, project_id_assoc: project },
        class: 'black-color',
        remote: false,
        method: :get
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options[:path_params]||{}), options)
  end

  def link_to_trackings_citizen(record, type,  opts = {})
    return unless can?(:read, "#{type.titleize}::Tracking".safe_constantize)
    options = {
        text:   icon_tracking,
        path:   "#{type}_#{type}_trackings_path",
        
        class: 'black-color',
        remote: false,
        method: :get
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options[:path_params]||{}), options)
  end

  def link_to_sms(record, opts = {})
    return unless can?(:sms, record)
    options = {
        text: icon_sms,
        path: "show_sms_path",
        path_params: { volunteer: record },
        class: 'grey-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options[:path_params]||{}), options)
  end

  def link_to_mail(record, opts = {})
    return unless can?(:mail, record)
    options = {
        text: icon_mail,
        path: "show_mail_path",
        path_params: { volunteer: record },
        class: 'grey-color',
        remote: true
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options[:path_params]||{}), options)
  end

  def link_to_mail_questions
    options = {
        text: "Enviar cuestionario",
        path: "show_questions_mail_path",
        class: 'btn btn-primary',
        remote: true
    }
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, options[:path_params]||{}), options)
  end

  def link_to_linkable(record, opts = {})
    return unless can?(:update, record)
    options = {
        id:     "#{dom_id(record)}_recover",
        text:   icon_recover,
        path:   "#{record.linkable_type.underscore}_path",
        remote: false,
        method: :post,
        data:   {confirm_swal: t('messages.are_you_sure'), reload: true}
    }.merge(opts)
    path = options.delete(:path)
    text = options.delete(:text)

    link_to(text, public_send(path, record, options[:path_params]||{}), options)
  end

  def button_to_trackings(record, type, project = nil, opts = {}) 
     
    return unless can?(:read, "#{type.titleize}::Tracking".safe_constantize) 
    options = { 
        id:     "#{dom_id(record)}_trackings", 
        text:   t('trackings.name'), 
        path:   "#{type}_trackings_path", 
        path_params: {q: {"#{record.class.model_name.singular}_id_eq": record}, project_id_assoc: project }, 
        remote: false, 
        method: :get, 
        class: "btn btn-success pull-right" 
    }.merge(opts) 
    path = options.delete(:path) 
    text = options.delete(:text) 
 
    link_to(text, public_send(path, options[:path_params]||{}), options) 
  end 

  def button_to_back(path=:back)
    link_to(t('action.back'), path, class: "btn btn-default")
  end

  def button_to_edit(record, opts = {})
    link_to_edit(record, {text: icon_edit+' '+t('action.edit', model: ''), class: "btn btn-primary"}.merge(opts))
  end

  def default_form_options
    {
      remote:           request.xhr?,
      html:             { class: 'form-horizontal', multipart: true },
      wrapper:          :horizontal_form,
      wrapper_mappings: { check_boxes:   :horizontal_radio_and_checkboxes,
                          radio_buttons: :horizontal_radio_and_checkboxes,
                          file:          :horizontal_file_input,
                          boolean:       :horizontal_boolean }
     }
  end

 def special_form_options 
    { 
      remote:           request.xhr?, 
      html:             { class: 'form-horizontal' }, 
      wrapper:          :projects_box, 
      wrapper_mappings: { check_boxes:   :horizontal_radio_and_checkboxes, 
                          radio_buttons: :horizontal_radio_and_checkboxes, 
                          file:          :horizontal_file_input, 
                          boolean:       :horizontal_boolean } 
     } 
  end 
  
  def attachable_form_options
    @attachable_form_options = default_form_options
    @attachable_form_options[:html] = { class: 'form-horizontal', multipart: true }
    @attachable_form_options
  end

  def show_simple_date(date, options = {})
    return unless date

    format =  case date
              when Date
                '%d/%m/%Y'
              when DateTime, ActiveSupport::TimeWithZone
                '%d/%m/%Y %H:%M'
              else
                '%d/%m/%Y'
              end
    l(date, { format: format }.merge(options)) if format
  end

  def show_attr(record, attr_name = '', date_opts = {})
    return record.public_send "#{attr_name}_i18n" if record.respond_to? "#{attr_name}_i18n"
    return '' if attr_name.blank?


    attr_value = record.public_send attr_name
    case attr_value
    when TrueClass, FalseClass
      t("humanize.#{attr_value}")
    when Date, DateTime, ActiveSupport::TimeWithZone
      show_simple_date(attr_value, date_opts)
    else
      attr_value
    end
  end

end
