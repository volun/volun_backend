module PermissionsHelper

    def find_or_generate_permission(manager, section)
        return Permission.new(section: section) if manager.blank?
        per=Permission.find_by(manager_id: manager, section: section)

        per.blank? ? Permission.new(manager_id: manager,section: section) : per
    end

    def get_array_value(type=nil)
        return [] if type.blank?
        case type.to_s
        when "tracking"
            [
                ["volunteer",:volunteer_vol_track,"Volun::Tracking"], ["entity",:volunteer_ent_track,"Ent::Tracking"],
                ["project",:volunteer_pro_track,"Pro::Tracking"], ["citizen",:volunteer_citizen_track,"Citizen::Tracking"],
                ["citizen_service",:volunteer_citizen_service_track,"CitizenService::Tracking"]
            ]
        when "request"
            [
                ["volunteer",[
                    ["new_volun",:volunteer_rt_subscribe,"Rt::VolunteerSubscribe"],
                    ["new_provolun",:volunteer_rt_proj_subscribe,"Rt::VolunteerProjectSubscribe"],
                    ["edit_volun",:volunteer_rt_amends,"Rt::VolunteerAmendment"],
                    ["appo_volun",:volunteer_rt_appoint,"Rt::VolunteerAppointment"],
                    ["acc_volun",:volunteer_rt_solidarity,"Rt::SolidarityChain"],
                ]],
                ["entity",[
                    ["other",:volunteer_rt_other,"Rt::Other"],
                    ["agend",:volunteer_rt_act_publish,"Rt::ActivityPublishing"],
                    ["demand",:volunteer_rt_demand,"Rt::VolunteersDemand"],
                    ["publish",:volunteer_rt_publishing,"Rt::ProjectPublishing"]
                ]]
            ]
        when "generics"
            [["entity", :volunteer_entity, "Entity"], ["activity", :volunteer_activity,  "Activity"], ["citizen", :volunteer_citizen, "Citizen"], ["citizen_service", :volunteer_citizen_service,  "CitizenService"]]
        when "generics_reduce"
            []
        else
            []
        end
    end


    def permissions_new
        @permissions_new = {
            I18n.t('permissions.volun') => 
                [
                I18n.t('permissions.basics'),
                I18n.t('permissions.specifics')
                ],
            I18n.t('permissions.project') => 
                [
                    I18n.t('permissions.basics'),
                    I18n.t('permissions.specifics')
                ],
            I18n.t('permissions.entity') => "",
            I18n.t('permissions.activity') => "",
            I18n.t('permissions.citizen') => "",
            I18n.t('permissions.citizen_service') => "",
            I18n.t('permissions.track') => 
            [
                I18n.t('permissions.project'),
                I18n.t('permissions.volunteer'),
                I18n.t('permissions.entity'),
                I18n.t('permissions.citizen'),
                I18n.t('permissions.citizen_service')
            ],
            I18n.t('permissions.request') => 
            {
                I18n.t('permissions.volunteer') =>
                [
                    I18n.t('permissions.new_volun'),
                    I18n.t('permissions.new_provolun'),
                    I18n.t('permissions.edit_volun'),
                    I18n.t('permissions.appo_volun'),
                    I18n.t('permissions.acc_volun')
                ],
                I18n.t('permissions.entity') =>
                [
                    I18n.t('permissions.other'),
                    I18n.t('permissions.agend'),
                    I18n.t('permissions.demand'),
                    I18n.t('permissions.publish')
                ]
            },
            I18n.t('permissions.admin') => ""
        }

        return @permissions_new
    end 
end