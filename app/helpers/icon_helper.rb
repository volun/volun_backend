module IconHelper

  def icon_new(opts = {})
    build_icon(:new, opts.merge(icon_name: 'plus'))
  end

  def icon_edit(opts = {})
    build_icon(:edit, opts.merge(icon_name: 'pencil', title: t('action.edit', model: '')))
  end

  def icon_password(opts = {})
    build_icon(:password, opts.merge(icon_name: 'key', title: t('action.generate_password', model: 'Citizen')))
  end

  def icon_show(opts = {})
    build_icon(:show, opts.merge(icon_name: 'search', title: t('action.show', model: t('details').downcase)))
  end

  def icon_services(opts = {})
    build_icon(:show, opts.merge(icon_name: 'list', title: t('action.services', model: t('details').downcase)))
  end

  def icon_question(opts = {})
    build_icon(:show, opts.merge(icon_name: 'question-circle', title: t('action.question', model: t('details').downcase)))
  end

  def icon_clone(opts = {})
    build_icon(:clone, opts.merge(icon_name: 'copy', title: t('action.clone', model: ''))) 
  end

  def icon_remove(opts = {})
    build_icon(:remove, opts.merge(icon_name: 'times', title: t('action.delete', model: ''), class: 'delete')) 
  end

  def icon_unlink(opts = {})
    build_icon(:remove, opts.merge(icon_name: 'unlink', title: t('action.unlink', model: ''))) 
  end
  def icon_download_file(opts = {})
    build_icon(:download_file, opts.merge(icon_name: 'download', title: t('action.download_csv', model: ''))) 
  end

  def icon_download_csv(opts = {})
    build_icon(:download_file, opts.merge(icon_name: 'file-excel-o', title: "Descargar CSV")) 
  end

  def icon_at_digital(opts = {})
    build_icon(:at_digital, opts.merge(icon_name: 'at', title: "Digital")) 
  end
  def icon_download_list(opts = {})
    build_icon(:download_file, opts.merge(icon_name: 'download', title: t('action.download_list', model: ''))) 
  end
  def icon_download_no_export_list(opts = {})
    build_icon(:file_export, opts.merge(icon_name: 'cloud-download', title: t('action.download_list_no_export', model: ''))) 
  end
  def icon_destroy(opts = {})
    if 'recover'.in?(controller.action_methods)
      action    = :archive
      icon_name = :archive
    else
      action    = :delete
      icon_name = :times
    end
    build_icon(action, opts.merge(icon_name: icon_name, class: action))
  end

  def icon_recover(opts = {})
    build_icon(:recover, opts.merge(icon_name: 'bolt', class: :recover))
  end

  def icon_check_circle(opts = {})
    build_icon(:check_circle, opts.merge(icon_name: 'check-circle',title: t('action.icon_check_circle', model: ''), class: :check_circle))
  end

  def icon_tracking(opts = {})
    build_icon(:trackings, opts.merge(icon_name: 'road', title: t('action.man_trackings', model: '')))
  end

  def icon_sms(opts = {})
    build_icon(:show_sms, opts.merge(icon_name: 'mobile', title: t('send_sms')))
  end

  def icon_mail(opts = {})
    build_icon(:show_mail, opts.merge(icon_name: 'envelope-o', title: t('send_mail.title')))
  end
  def icon_public(opts = {})
    build_icon(:is_public, opts.merge(icon_name: 'link', title: t('isPublic')))
  end
  def icon_search(opts = {})
    build_icon(:search, opts)
  end
  def icon_meeting(opts = {})
    build_icon(:meeting, opts.merge(icon_name: 'comments', title: t('action.create_meeting'))) 
  end
  def icon_turn(opts = {})
    build_icon(:turn, opts.merge(icon_name: 'calendar-plus-o', title: t('action.assign_turn'))) 
  end
  def build_icon(action, opts = {})
    icon_name       = opts.delete(:icon_name) || action
    options         = {}
    options[:text]  = opts.delete(:text)
    options[:title] = opts.delete(:title) || t("action.#{action}", default: options[:text] || icon_name)
    options[:alt]   = opts.delete(:alt) || options[:title]
    fa_icon(icon_name, options.merge(opts))
  end
end
