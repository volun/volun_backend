module CitizenServicesHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_citizen_services")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct 
        COALESCE(CAST(citizen_services.id as varchar),'-') as \"#{CitizenService.human_attribute_name(:id)}\",
        COALESCE(CAST((SELECT citizens.id FROM citizens WHERE citizens.id = citizen_services.citizen_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:f_citizen_id)}\",
        COALESCE(CAST((SELECT citizens.name FROM citizens WHERE citizens.id = citizen_services.citizen_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:name_citizen)}\",
        COALESCE(CAST((SELECT citizens.last_name_1 FROM citizens WHERE citizens.id = citizen_services.citizen_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:last_name_citizen)}\",
        COALESCE(CAST((SELECT citizens.last_name_2 FROM citizens WHERE citizens.id = citizen_services.citizen_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:last_name_alt_citizen)}\",
        COALESCE(CAST((SELECT d.name  FROM districts d,citizens,addresses WHERE addresses.district_id = d.id and addresses.id=citizens.address_id AND citizens.id = citizen_services.citizen_id) as varchar),'OTROS') as \"#{CitizenService.human_attribute_name(:citizen_district)}\",
        COALESCE(CAST((SELECT d.name  FROM boroughts d,citizens,addresses WHERE addresses.borought_id = d.id and addresses.id=citizens.address_id AND citizens.id = citizen_services.citizen_id) as varchar),'OTROS') as \"#{CitizenService.human_attribute_name(:citizen_borought)}\",  
        COALESCE(CAST((SELECT COALESCE(volunteers.id_autonum, volunteers.id) FROM volunteers WHERE volunteers.id = citizen_services.volunteer_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:f_volunteer_id_autonum)}\",
        COALESCE(CAST((SELECT volunteers.name FROM volunteers WHERE volunteers.id = citizen_services.volunteer_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:name_volunteer)}\",
        COALESCE(CAST((SELECT volunteers.last_name FROM volunteers WHERE volunteers.id = citizen_services.volunteer_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:last_name_volunteer)}\",
        COALESCE(CAST((SELECT volunteers.last_name_alt FROM volunteers WHERE volunteers.id = citizen_services.volunteer_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:last_name_alt_volunteer)}\",
        case when volunteer_id is not null then COALESCE(CAST((SELECT d.name  FROM districts d,volunteers,addresses WHERE addresses.district_id = d.id and addresses.id=volunteers.address_id AND volunteers.id = citizen_services.volunteer_id) as varchar),'OTROS') else '-' end as \"#{CitizenService.human_attribute_name(:f_volunteer_district)}\",
        case when volunteer_id is not null then COALESCE(CAST((SELECT d.name  FROM boroughts d,volunteers,addresses WHERE addresses.borought_id = d.id and addresses.id=volunteers.address_id AND volunteers.id = citizen_services.volunteer_id) as varchar),'OTROS') else '-' end as \"#{CitizenService.human_attribute_name(:f_volunteer_borought)}\",
          COALESCE(CAST((SELECT services.name FROM services WHERE services.id = citizen_services.service_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:type_service)}\",
        COALESCE(CAST(CONCAT(citizen_services.date_accept_request,' ',citizen_services.hour_accept_request) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_accept)}\",
        COALESCE(CAST(CONCAT(citizen_services.date_request,' ',citizen_services.hour_request) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_request)}\",
        CASE citizen_services.status WHEN 'active' THEN 'Activo'
        WHEN 'accept' THEN 'Aceptado'
        WHEN 'canceled' THEN 'Cancelado'
        WHEN 'prioritary' THEN 'Urgente'
        WHEN 'finished' THEN 'Terminado'
        WHEN 'undone' THEN  'No realizado'
        WHEN 'expired' THEN 'Expirado'
        ELSE '-' END as \"#{CitizenService.human_attribute_name(:status_name)}\",
        COALESCE(CAST((SELECT DISTINCT service_votes.vote FROM service_votes WHERE service_votes.citizen_service_id = citizen_services.id AND service_votes.type_user = 'Citizen') as varchar),'-') as \"#{CitizenService.human_attribute_name(:citizen_vote)}\",
        COALESCE(CAST((SELECT DISTINCT service_votes.vote FROM service_votes WHERE service_votes.citizen_service_id = citizen_services.id AND service_votes.type_user = 'Volunteer') as varchar),'-') as \"#{CitizenService.human_attribute_name(:volunteer_vote)}\",
        COALESCE(CAST(citizen_services.canceled_at as varchar),'-') as \"#{CitizenService.human_attribute_name(:canceled_at)}\",
        COALESCE(CAST(citizen_services.citizen_reason as varchar),'-') as \"#{CitizenService.human_attribute_name(:citizen_reason)}\",
        COALESCE(CAST(citizen_services.rejected_at as varchar),'-') as \"#{CitizenService.human_attribute_name(:rejected_at)}\",
        COALESCE(CAST(citizen_services.volunteer_reason as varchar),'-') as \"#{CitizenService.human_attribute_name(:volunteer_reason)}\",
        COALESCE(CAST(citizen_services.description as varchar),'-') as \"#{CitizenService.human_attribute_name(:description)}\",
        COALESCE(CAST(citizen_services.place as varchar),'-') as \"#{CitizenService.human_attribute_name(:place)}\",
        COALESCE(CAST(citizen_services.additional_data as varchar),'-') as \"#{CitizenService.human_attribute_name(:additional_data)}\",
        COALESCE(CAST((SELECT boroughts.name FROM boroughts WHERE  boroughts.id= citizen_services.borought_id AND boroughts.district_id=citizen_services.district_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:borought_name)}\",
        COALESCE(CAST((SELECT districts.name FROM districts WHERE  districts.id=citizen_services.district_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:district_name)}\"
      FROM citizen_services, citizens WHERE citizens.id=citizen_services.citizen_id #{ " AND citizen_services.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end

  def questions_stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_questions_citizen_services")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end

    return if items.blank?
    query = "COPY (
      Select distinct 
        COALESCE(CAST((SELECT CONCAT(citizens.name,' ', citizens.last_name_1,' ',citizens.last_name_2) from citizens, citizen_services 
          where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"#{CitizenService.human_attribute_name(:full_name_citizen)}\",
        COALESCE(CAST((SELECT CONCAT(volunteers.name,' ', volunteers.last_name,' ',volunteers.last_name_alt) from volunteers, citizen_services 
          where volunteers.id = citizen_services.volunteer_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"#{CitizenService.human_attribute_name(:full_name_volunteer)}\",
        COALESCE(CAST((SELECT citizens.email from citizens, citizen_services 
          where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"Email ciudadano\",
        COALESCE(CAST((SELECT citizens.phone from citizens, citizen_services 
          where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"Teléfono ciudadano\",
        COALESCE(CAST((SELECT volunteers.email from volunteers, citizen_services 
          where volunteers.id = citizen_services.volunteer_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"Email voluntario\",
        COALESCE(CAST((SELECT CONCAT(volunteers.phone_number,'/',volunteers.phone_number_alt) from volunteers, citizen_services 
          where volunteers.id = citizen_services.volunteer_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"Teléfono voluntario\",
        COALESCE(CAST(service_questions.id as varchar),'-') as \"ID pregunta\",
        COALESCE(CAST(service_questions.description as varchar),'-') as \"#{CitizenService.human_attribute_name(:questions)}\",
        COALESCE(CAST(CASE WHEN service_questions.question_type = 'boolean' AND service_responses.response = 'true' 
          THEN 'Sí' WHEN service_questions.question_type = 'boolean' AND service_responses.response = 'false' 
          THEN 'No' ELSE service_responses.response END as varchar),'-') as \"#{CitizenService.human_attribute_name(:responses)}\",
        COALESCE(CAST(to_char(service_responses.created_at,'dd/MM/yyyy HH24:MI') as varchar),'-') as \"#{ServiceResponse.human_attribute_name(:created_at)} (Encuesta)\",
        COALESCE(CAST((SELECT CONCAT(to_char(citizen_services.date_accept_request,'dd/MM/yyyy'),' ',citizen_services.hour_accept_request) 
          FROM citizen_services WHERE citizen_services.id =service_responses.citizen_service_id ) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_accept)}\",
        COALESCE(CAST((SELECT CONCAT(to_char(citizen_services.date_request,'dd/MM/yyyy'),' ',citizen_services.hour_request) 
          FROM citizen_services WHERE citizen_services.id =service_responses.citizen_service_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_request)}\",
        COALESCE(CAST(service_responses.citizen_service_id as varchar),'-') as \"#{CitizenService.human_attribute_name(:id_service)}\",
        COALESCE(CAST((SELECT services.name from services, citizen_services 
          where services.id = citizen_services.service_id 
          AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
          as \"#{CitizenService.human_attribute_name(:type_service)}\"
      FROM service_responses, service_questions,citizen_services, citizens where citizens.id = citizen_services.citizen_id 
       AND citizen_services.id = service_responses.citizen_service_id AND service_responses.service_question_id = service_questions.id #{ "AND service_responses.citizen_service_id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} 
      ORDER BY \"#{ServiceResponse.human_attribute_name(:created_at)} (Encuesta)\" desc, \"ID pregunta\" asc)
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"

    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end

  def questions_stream_query_rows_export(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_questions_citizen_services")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end

    return if items.blank?
    query = "COPY (SELECT DISTINCT
      COALESCE(CAST((SELECT CONCAT(citizens.name,' ', citizens.last_name_1,' ',citizens.last_name_2) from citizens, citizen_services 
        where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
        as \"#{CitizenService.human_attribute_name(:full_name_citizen)}\",
      COALESCE(CAST((SELECT citizens.email from citizens, citizen_services 
        where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
        as \"Email ciudadano\",
      COALESCE(CAST((SELECT citizens.phone from citizens, citizen_services 
        where citizens.id = citizen_services.citizen_id AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
        as \"Teléfono ciudadano\",
      COALESCE(CAST(service_responses.service_question_id as varchar),'-') as \"ID pregunta\",
      COALESCE(CAST((SELECT service_questions.description FROM service_questions WHERE service_responses.service_question_id = service_questions.id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:questions)}\",
      COALESCE(CAST(
        (SELECT CASE WHEN service_questions.question_type = 'boolean' AND service_responses.response = 'true' 
          THEN 'Sí' WHEN service_questions.question_type = 'boolean' AND service_responses.response = 'false' 
          THEN 'No' ELSE service_responses.response END FROM service_questions WHERE service_responses.service_question_id = service_questions.id)
          as varchar),'-') as \"#{CitizenService.human_attribute_name(:responses)}\",
      COALESCE(CAST(to_char(service_responses.created_at,'dd/MM/yyyy HH24:MI') as varchar),'-') as \"#{ServiceResponse.human_attribute_name(:created_at)} (Encuesta)\",
      COALESCE(CAST((SELECT CONCAT(to_char(citizen_services.date_accept_request,'dd/MM/yyyy'),' ',citizen_services.hour_accept_request) 
        FROM citizen_services WHERE citizen_services.id =service_responses.citizen_service_id ) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_accept)}\",
      COALESCE(CAST((SELECT CONCAT(to_char(citizen_services.date_request,'dd/MM/yyyy'),' ',citizen_services.hour_request) 
        FROM citizen_services WHERE citizen_services.id =service_responses.citizen_service_id) as varchar),'-') as \"#{CitizenService.human_attribute_name(:full_date_request)}\",
      COALESCE(CAST(service_responses.citizen_service_id as varchar),'-') as \"#{CitizenService.human_attribute_name(:id_service)}\",
      COALESCE(CAST((SELECT services.name from services, citizen_services 
        where services.id = citizen_services.service_id 
        AND citizen_services.id = service_responses.citizen_service_id) as varchar),'-') 
        as \"#{CitizenService.human_attribute_name(:type_service)}\"
      FROM service_responses WHERE service_responses.citizen_service_id in 
      (SELECT citizen_services.id FROM citizen_services, citizens WHERE citizens.id=citizen_services.citizen_id AND citizen_services.date_request >= '#{(Time.zone.now - 7.days).strftime("%Y-%m-%d")}')
      #{ " AND service_responses.citizen_service_id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} 
      ORDER BY \"#{ServiceResponse.human_attribute_name(:created_at)} (Encuesta)\" desc, \"ID pregunta\" asc)
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"

    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
