module CitizenTrackingsHelper
    extend ActiveSupport::Concern
  
    def stream_query_rows(items=nil,params=nil)
      model = items.model
      filtrado = StreamingHelper.filter(model, items, params, "header_list_citizen_tracking")
      begin
        filtrado[:header].each {|x| yield x if block_given? }
      rescue
      end
      
      return if items.blank?
      query = "COPY (
        Select distinct 
        COALESCE(CAST((select citizens.name FROM citizens WHERE citizens.id = citizen_trackings.citizen_id) as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:citizen)}\",
        COALESCE(CAST((select tracking_types.name FROM tracking_types WHERE tracking_types.id = citizen_trackings.tracking_type_id) as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:tracking_type)}\",
        COALESCE(CAST((select managers.name FROM managers WHERE managers.id = citizen_trackings.manager_id) as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:manager)}\",
        COALESCE(CAST(citizen_trackings.tracked_at as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:tracked_at)}\",
        CASE citizen_trackings.automatic WHEN 'false' THEN 'No'
          WHEN 'true' THEN 'Sí' END as \"#{CitizenTracking.human_attribute_name(:automatic)}\",
        COALESCE(CAST(citizen_trackings.coments as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:comments)}\"    
        FROM citizen_trackings #{ "where citizen_trackings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
        TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
      StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    end
  end