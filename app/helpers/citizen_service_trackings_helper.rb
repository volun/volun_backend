module CitizenServiceTrackingsHelper
    extend ActiveSupport::Concern
  
    def stream_query_rows(items=nil,params=nil)
      model = items.model
      filtrado = StreamingHelper.filter(model, items, params, "header_list_citizen_service_tracking")
      begin
        filtrado[:header].each {|x| yield x if block_given? }
      rescue
      end
      
      return if items.blank?
      query = "COPY (
        Select distinct 
        COALESCE(CAST((select citizen_services.description FROM citizen_services WHERE citizen_services.id = citizen_service_trackings.citizen_service_id) as varchar),'-') as \"#{CitizenServiceTracking.human_attribute_name(:citizen_service)}\",
        COALESCE(CAST((select tracking_types.name FROM tracking_types WHERE tracking_types.id = citizen_service_trackings.tracking_type_id) as varchar),'-') as \"#{CitizenServiceTracking.human_attribute_name(:tracking_type)}\",
        COALESCE(CAST((select managers.name FROM managers WHERE managers.id = citizen_service_trackings.manager_id) as varchar),'-') as \"#{CitizenTracking.human_attribute_name(:manager)}\",
        COALESCE(CAST(citizen_service_trackings.tracked_at as varchar),'-') as \"#{CitizenServiceTracking.human_attribute_name(:tracked_at)}\",
        CASE citizen_service_trackings.automatic WHEN 'false' THEN 'No'
          WHEN 'true' THEN 'Sí' END as \"#{CitizenServiceTracking.human_attribute_name(:automatic)}\",
        COALESCE(CAST(citizen_service_trackings.coments as varchar),'-') as \"#{CitizenServiceTracking.human_attribute_name(:comments)}\"    
        FROM citizen_service_trackings #{ "where citizen_service_trackings.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
        TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
      StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
    end
  end