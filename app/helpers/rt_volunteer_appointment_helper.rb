module RtVolunteerAppointmentHelper
  extend ActiveSupport::Concern

  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_appointment")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select  
      COALESCE(CAST((select volunteers.name from volunteers 
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:name)}\",      
      COALESCE(CAST((select volunteers.last_name from volunteers 
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:last_name)}\",
      COALESCE(CAST((select volunteers.last_name_alt from volunteers
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:last_name_alt)}\",
      COALESCE(CAST((select volunteers.phone_number from volunteers
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:phone_number)}\",
      COALESCE(CAST((select volunteers.email from volunteers
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:email)}\",
      COALESCE(CAST((select districts.name from addresses, volunteers, districts
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        AND addresses.id=volunteers.address_id and districts.id=addresses.district_id
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:district)}\",
      COALESCE(CAST((select addresses.postal_code from addresses, volunteers
        WHERE volunteers.id= rt_volunteer_appointments.volunteer_id 
        AND addresses.id=volunteers.address_id
        )as varchar),'-') as \"#{Rt::VolunteerAppointment.human_attribute_name(:postal_code)}\",
      COALESCE(CAST(rt_volunteer_appointments.created_at as varchar),'-') as \"#{Rt::VolunteerAmendment.human_attribute_name(:created_at)}\",      
      CASE 
        WHEN request_forms.req_status_id IN ('3','4','2')
        THEN DATE_PART('day', request_forms.status_date - request_forms.created_at)
        WHEN request_forms.req_status_id IN ('1')
        THEN DATE_PART('day', CURRENT_TIMESTAMP - request_forms.created_at)
      END as \"#{Rt::VolunteerAppointment.human_attribute_name(:days_passed)}\",
      COALESCE(CAST((
        SELECT TRIM(CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt))
        FROM managers, request_forms 
        WHERE request_forms.request_type_id= 4
        AND request_forms.rt_extendable_id = rt_volunteer_appointments.id  
        AND managers.id=request_forms.manager_id) as varchar),'-') as \"#{RequestForm.human_attribute_name(:manager)}\",
      COALESCE(CAST((select DATE(request_forms.status_date) 
        FROM request_forms 
        WHERE request_forms.request_type_id=4 
        AND request_forms.rt_extendable_id = rt_volunteer_appointments.id)as varchar),'-') as \"#{RequestForm.human_attribute_name(:status_date)}\",
      CASE (select req_statuses.description FROM req_statuses,request_forms WHERE req_statuses.id=request_forms.req_status_id
          AND request_forms.request_type_id=4 
          AND request_forms.rt_extendable_id = rt_volunteer_appointments.id) 
        WHEN 'pending' THEN '#{I18n.t('rt_volun_amendment_filter.search_pending')}'
        WHEN 'processing' THEN '#{I18n.t('rt_volun_amendment_filter.search_processing')}'
        WHEN 'approved' THEN '#{I18n.t('rt_volun_amendment_filter.search_approved')}'
        WHEN 'rejected' THEN '#{I18n.t('rt_volun_amendment_filter.search_rejected')}'
        END as \"#{RequestForm.human_attribute_name(:status)}\"
      FROM rt_volunteer_appointments, request_forms 
        WHERE request_forms.request_type_id=4 
        AND request_forms.rt_extendable_id = rt_volunteer_appointments.id
        #{ "AND rt_volunteer_appointments.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end