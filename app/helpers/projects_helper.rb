module ProjectsHelper
  extend ActiveSupport::Concern

  
  def stream_query_rows(items=nil,params=nil)
    model = items.model
    filtrado = StreamingHelper.filter(model, items, params, "header_list_project")
    begin
      filtrado[:header].each {|x| yield x if block_given? }
    rescue
    end
    
    return if items.blank?
    query = "COPY (
      Select distinct projects.id as \"#{model.human_attribute_name(:id)}\",
        projects.pt_extendable_id as \"#{model.human_attribute_name(:pt_extendable_id)}\",
        CASE projects.subtype_pt WHEN 'permanent' THEN 'Permanente'
          WHEN 'punctual' THEN 'Puntual' 
          ELSE '-'
          END as \"#{model.human_attribute_name(:subtype_pt_compact)}\",
        CASE projects.subtype_proyect WHEN 'subproyect' THEN 'Subproyecto'
          WHEN 'matriz' THEN 'Matriz' 
          ELSE '-'
          END as \"#{model.human_attribute_name(:subtype_proyect_compact)}\",
        CASE subtype_proyect WHEN 'subproyect' THEN concat(projects.father_id,'.',projects.pt_order)
         ELSE '-' END as \"#{model.human_attribute_name(:pt_order)}\",
        projects.father_id as \"#{model.human_attribute_name(:father_id)}\",
        COALESCE(CAST(projects.name as varchar),'-') as \"#{model.human_attribute_name(:name)}\",
        CASE projects.pt_extendable_type 
          WHEN 'Pt::Social' THEN 'Distrito'
          WHEN 'Pt::Centre' THEN 'Centro de mayores'
          WHEN 'Pt::Entity' THEN 'Entidad'
          WHEN 'Pt::Subvention' THEN 'Subvencionado'
          WHEN 'Pt::Other' THEN 'Otros'
          WHEN 'Pt::RetiredVolunteer' THEN 'Jubilados'
          WHEN 'Pt::Volunteer' THEN 'Voluntari@s por Madrid'
          WHEN 'Pt::Municipal' THEN 'Municipales'
          END as \"#{model.human_attribute_name(:project_type)}\",
        COALESCE(CAST((select entities.name from entities where entities.id = projects.entity_id) as varchar),'-') as \"#{model.human_attribute_name(:entity)}\",
        CASE projects.suspended WHEN 'false' THEN 'No'
          WHEN 'true' THEN 'Sí' END as \"#{model.human_attribute_name(:suspended)}\",
        COALESCE(projects.execution_start_date) as \"#{model.human_attribute_name(:execution_start_date)}\",
        COALESCE(projects.execution_end_date) as \"#{model.human_attribute_name(:execution_end_date)}\",
        COALESCE(CAST((select count(*) from projects_volunteers, volunteers 
          WHERE projects_volunteers.project_id=projects.id 
          AND volunteers.id = projects_volunteers.volunteer_id 
          AND projects_volunteers.volunteer_type='DERIVATIE') as varchar),'-') as \"#{model.human_attribute_name(:derivaties_volunteers_count)}\",
        COALESCE(CAST((select count(*) from projects_volunteers 
          WHERE projects_volunteers.project_id=projects.id 
          AND (projects_volunteers.volunteer_type!='DERIVATIE' OR projects_volunteers.volunteer_type is null)) as varchar),'-') as \"#{model.human_attribute_name(:volunteers_project_count)}\",
        COALESCE(CAST((select count(*) from projects_volunteers, volunteers 
          WHERE projects_volunteers.project_id=projects.id 
          AND volunteers.id = projects_volunteers.volunteer_id 
          AND projects_volunteers.active=true 
          AND volunteers.active = true) as varchar),'-') as \"#{model.human_attribute_name(:volunteers_num_active)}\",
        COALESCE(CAST(projects.volunteers_num as varchar),'-') as \"#{model.human_attribute_name(:volunteers_num)}\",
        COALESCE(CAST(projects.beneficiaries_num as varchar),'-') as \"#{model.human_attribute_name(:beneficiaries_num)}\",
        CASE projects.review_end_project WHEN 'false' THEN 'No'
          WHEN 'true' THEN 'Sí' END as \"#{model.human_attribute_name(:review_end_project)}\",
        COALESCE(CAST((
          select string_agg(TRANSLATE(UPPER(CONCAT(
            areas.name)),'ÁÉÍÓÚ','AEIOU'),', ') from areas, areas_projects 
          WHERE areas.id=areas_projects.area_id 
          AND projects.id=areas_projects.project_id) as varchar),'-') as \"#{I18n.t('activerecord.models.area.other')}\"
      FROM projects #{ "where projects.id IN (#{filtrado[:filter]})" unless filtrado[:filter].blank?} )
      TO STDOUT (delimiter ';', FORMAT CSV,HEADER );" 
    StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? }
  end
end
