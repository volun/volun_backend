
$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert', function() {

    $('a[data-confirm-swal]').on('click', handleConfirm);    
    $('a[data-confirm]').on('click', handleConfirm);   
    
    const confirmed = (element, result) => {
        console.log(result)
        if (result) {
            if (!!element.getAttribute('data-remote')) {
                let reloadAfterSuccess = !!element.getAttribute('data-reload');
    
                $.ajax({
                    method: element.getAttribute('data-method') || 'GET',
                    url: element.getAttribute('href'),
                    dataType: 'json',
                    success: function(result2) {
                        swal('!Proceso realizado!', 'Se ha realizado correctamente el poceso', 'success')
                            .then((_result) => {
                                if (reloadAfterSuccess) {
                                    window.location.reload();
                                }
                            });
                    },
                    error: function(xhr) {
                        if (xhr.status == 200) {
                            swal('!Proceso realizado!', 'Se ha realizado correctamente el poceso', 'success')
                            .then((_result) => {
                                if (reloadAfterSuccess) {
                                    window.location.reload();
                                }
                            });
                        } else {
                            let title   = '¡Error!';
                            let message = 'Ha habido algun error. Por favor intentalo más tarde.';
                        
                            if (xhr.responseJSON && xhr.responseJSON.message) {
                                message = xhr.responseJSON.message;
                            }
        
                            swal(title, message,'error');
                        }
                    }
                });
            } else {
                element.removeAttribute('data-confirm-swal');
                // element.click();
                // eval(element.getAttribute('href'));
                $(location).attr('href',element.getAttribute('href'));
            }
        }
    };
    
    // Display the confirmation dialog
    const showConfirmationDialog = (element) => {
        const message = element.getAttribute('data-confirm-swal');
        const text    = element.getAttribute('data-text');
    
        swal({
            title:             message || 'Are you sure?',
            text:              text || '',
            showCancelButton:  true,
            confirmButtonText: 'Sí',
            cancelButtonText:  'Cancelar',
        }).then(result => confirmed(element, result));
    };
    
    const allowAction = (element) => {
        if (element.getAttribute('data-confirm-swal') === null) {
            return true;
        }
    
        showConfirmationDialog(element);
        return false;
    };
    
    function handleConfirm(element) {
        element.preventDefault();
        if (!allowAction(this)) {
            Rails.stopEverything(element);
        }
    }
       
});
   
    