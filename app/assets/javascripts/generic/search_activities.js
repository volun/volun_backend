
function search_js_activity() {
    search_generic([
        ['isSearch', true, 'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['search_id', $('#search_id'), 'i'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_denominacion', $('#search_denominacion'),'s'],
        ['search_tipo',$('#search_tipo-select option:selected'), 's'],
        ['search_organizador', $('#search_organizador-select option:selected'),'s'],
        ['search_inicio_f', $('#search_inicio_f'), 's'],
        ['search_status', $('#search_status-select option:selected'), 's'],
        ['search_fin_f',$('#search_fin_f'), 's'],
        ['search_rrss' ,$('#search_rrss'), 'check'],
        ['search_end_activity', $('#search_end_activity'), 'check']
        ],'#recarga-filtros');
}

function search_js_activities_clean() {
    search_clear([$('#search_id'),
        $('#enter_text'),
        $('#search_denominacion'),
        $('#search_status-select'),
        $('#search_inicio_f'),
        $('#search_fin_f'),
        $('#search_organizador-select'),
        $('#search_tipo-select'),
        $('#search_rrss'),
        $('#search_end_activity'),
        $('#buttonSearchActivity'),
        $('#warningLabelFechaFormat'),
        $('#warningLabelFechaFormat2'),
        $('#warningLabelFecha'),
        $('#warning_id_activity')], '#recarga-filtros');
}

