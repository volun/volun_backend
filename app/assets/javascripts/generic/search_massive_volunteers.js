
function search_js_massive_volunteers() {
    search_generic_turbo([
      ['isSearch',true,'b'],
      ['page',1,'b'],
      ['title',$('#title'),'s'],
      ['content','content','ck'],
      ['per_page',$('#pages-select option:selected'),'i'],
      ['search_vocne',$('#search_vocne'),'check'],
      ['search_postal_code',$('#search_postal_code'),'s'],
      ['search_district',$('#search_district-select'),'m'],
      ['search_situation',$('#search_situation-select'),'m']
        ],'#recarga-filtros');
}

function search_js_massive_volunteers_clean() {
    search_clear_turbo([$('#search_vocne'),
    $('#search_postal_code'),
    $('#search_district-select'),
    $('#search_situation-select'),
    $('#buttonSearchMassiveVolunteer')], '#recarga-filtros');
}



