
$(function() {

    $('#citizen_phone').on('change', function() {
        validation_telefono($(this),$('#phone'));
    });

    $('#citizen_email').on('change', function() {
        validation_email($(this),$('#email'));
    });

    $('#BDC').on('click', function() {
        var element = document.getElementById('BDC_Display');
        if (element) {
            var display = element.style.display;
        
            if (display == "none") {
                element.style.display = "block";
            } else {
                element.style.display = "none";
            }
        }
    });

    
    //validation_document_type($(this),$('#id_number'),$('#id_number2'),$('#volunteer_document_type_id option:selected'))
});