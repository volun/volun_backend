function ChangeBorought(action) {
  search_generic_specific([
        ['district_id',$('#citizen_service_district_id'),'s']
        ],'#recarga-borought', action);
}

function search_generic_specific(parametros,div, action){
  var url_params="";
  var i = 0;
  var action_aux = "";

  if (action == "update") {
    action_aux = "/edit";
  } else if(action == "create") {
    action_aux = "/new";
  }


  for(i=0;i<parametros.length;i++){
      var conversion="";
      var conca="";
      if(url_params!="") conca="&";
      if(parametros[i][1].length!=0){
          switch(parametros[i][2]){
              case 'i': //Conversión a numérico
                if (parametros[i][1].val() != null) {
                    conversion= parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[ ]/g,"").trim();
                    url_params=url_params+conca+parametros[i][0]+"="+conversion;
                }
                  break;
              case 'b': //Dato simple
                  conversion=parametros[i][1];
                  url_params=url_params+conca+parametros[i][0]+"="+conversion;
                  break;
              case 's': //Conversión a texto
                if (parametros[i][1].val() != null) {
                    conversion=parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[\'%();]/g,"").trim();
                    url_params=url_params+conca+parametros[i][0]+"="+conversion;
                }
                  break;
              case 'm':
                  conversion=parametros[i][1].val();
                  url_params=url_params+conca+parametros[i][0]+"="+conversion;
                  break;
              case 'check': //Obtención de valor booleano
                  if(parametros[i][1].prop('checked')){
                      conversion=parametros[i][1].prop('checked');
                      url_params=url_params+conca+parametros[i][0]+"="+conversion;
                  };
                  break;
          }
         
         
      }
  }
  var union="?";
  if(window.location.href.includes(union)) union="&";

  $(div).load(window.location.href+action_aux+union+encodeURI(url_params)+" "+div);
}


