
function search_js_volunteers() {
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_id',$('#search_id'),'i'],
        ['project_id',$('#project_id'),'s'],
        ['asociated',$('#asociated'),'s'],
        ['pt_extendable_id',$('#pt_extendable_id'),'s'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_nif',$('#search_nif'),'s'],
        ['search_name',$('#search_name'),'s'],
        ['search_location',$('#search_location-select option:selected'),'s'],
        ['search_volun_profile',$('#search_volun_profile-select'),'m'],
        ['search_info_source',$('#search_info_source-select'),'m'] ,
        ['search_last_name',$('#search_last_name'),'s'],
        ['search_last_name_alt',$('#search_last_name_alt'),'s'],
        ['search_postal_code',$('#search_postal_code'),'i'],
        ['search_movil',$('#search_movil'),'s'],
        ['search_availabilities_day',$('#search_availabilities_day-select'),'m'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['search_district',$('#search_district-select'),'m'] ,
        ['search_incluence_district',$('#search_incluence_district-select'),'m'] ,
        ['search_collective',$('#search_collective-select'),'m'],
        ['search_area',$('#search_area-select'),'m'],
        ['search_type',$('#search_type-select'),'m'],
        ['search_project_link_start_date',$('#search_project_link_start_date'),'s'],
        ['search_project_link_end_date',$('#search_project_link_end_date'),'s'],
        ['search_start_date',$('#search_start_date'),'s'],
        ['search_end_date',$('#search_end_date'),'s'],
        ['search_start_age',$('#search_start_age'),'s'],
        ['search_end_age',$('#search_end_age'),'s'],
        ['search_without_auth',$('#search_without_auth'),'check'],
        ['search_accompany_volunteer',$('#search_accompany_volunteer'),'check'],
        ['new_availability',$('#new_availability'),'check'],
        ['search_next_point',$('#search_next_point-select option:selected'),'i'],
        ['ec',$('#ec'),'check'],
        ['no_asign',$('#no_asign'),'check'],
        ['search_tester',$('#search_tester'),'check'],
        ['search_no_tester',$('#search_no_tester'),'check']
        ],'#recarga-filtros');
}

function search_js_volunteers_clean() {
    search_clear([$('#search_id'),
    $('#search_availabilities_day-select'),
    $('#search_volun_profile-select'),
    $('#search_location-select'),
    $('#search_status-select'),
    $('#search_info_source-select'),
    $('#search_id'),
    $('#search_district-select'),
    $('#search_incluence_district-select'),
    $('#enter_text'),
    $('#search_nif'),
    $('#search_name'),
    $('#search_last_name'),
    $('#search_last_name_alt'),
    $('#search_movil'),
    $('#search_postal_code'),
    $('#search_project_link_start_date'),
    $('#search_project_link_end_date'),
    $('#search_collective-select'),
    $('#search_area-select'),
    $('#search_type-select'),
    $('#search_start_date'),
    $('#search_end_date'),
    $('#search_without_auth'),
    $('#search_next_point-select'),
    $('#search_accompany_volunteer'),
    $('#new_availability'),
    $('#no_asign'),
    $('#ec'),
    $('#search_start_age'),
    $('#warningLabelFecha'),
    $('#warningLabelEdad'),
    $('#warning_id'),
    $('#warningLabelFechaFormat'),
    $('#warningLabelFechaFormat2'),
    $('#warning_phone'),
    $('#warning_postal_code'),
    $('#warning_age'),
    $('#search_end_age'),
    $('#buttonSearchVolunteer'),
    $('#search_tester'),$('#search_no_tester')], '#recarga-filtros');
}

// $(document).on('turbolinks:load', function(){
//     // Check date fields one time at rendering;
//     setAvailDate();
//     setAgrmtDate();
    
//     setVolunAge();

//     // Clean search values
//     resetButton();

//     // Search
//     searchButton();
//     keepSearch();

//     // Show others comment fields on traits
//     $.each($('[id^="volunteer_assessments_attributes"][id$="trait_id"]'), function( index, value ) {
//       showComments(value.id)
//     });
//     $.each($('[id^="volunteer_assessments_projects_attributes"][id$="trait_id"]'), function( index, value ) {
//       showComments(value.id)
//     });

//     $('#volunteer_birth_date').datepicker({
//       onSelect: function () {
//         setVolunAge();
//       }
//     });
//   });

 // Select jQuery for degrees filtering
 

  // Disable availability_date if available check is checked
  function setAvailDate() {
    if (!$("#volunteer_available").prop("checked")) {
      $("#volunteer_availability_date").attr("disabled", true);
      $("#volunteer_availability_date").val("");
    } else {
      $("#volunteer_availability_date").attr("disabled", false);
    }
  }

  // Disable agreement_date if available check is checked
  
  

  

  // Clean search values
  function resetButton() {
    $("#btn-reset").click(function(){
        $('#q_address_district_or_address_postal_code_or_name_or_last_name_or_last_name_alt_or_full_name_or_id_number_or_phone_number_or_phone_number_alt_or_email_or_other_academic_info_cont').prop('value', "");
        $('#q_id_eq').prop('value', "");
        $("#q_address_district_eq").removeAttr('value');
        $(".q_availabilities_day_in_all").removeAttr('value');
        $("#status").removeAttr('value');
        $('.search-form').submit()
    });
  }

  // Search
  function searchButton() {
    $("#btn-search").click(function(){
        $('.q_availabilities_day_in_all').prop('value',$('#availability-select option:selected').val());
        $('#q_address_district_eq').prop('value', $('#district-select option:selected').val());
        $('#status').prop('value', $('#status-select option:selected').val());
        $('.search-form').submit()
    });
  }

  function keepSearch() {
    if ($('#q_address_district_eq').val()) {
        var districtValue = $('#q_address_district_eq').val()
        $('#district-select option[value="' + districtValue + '"]').prop('selected', true)
    };

    if ($('#status').val()) {
        var statusValue = $('#status').val()
        $('#status-select option[value="' + statusValue + '"]').prop('selected', true)
    };

    if ($('.q_availabilities_day_in_all').val()) {
        var availabilityValue = $('.q_availabilities_day_in_all').val()
        $('#availability-select option[value="' + availabilityValue + '"]').prop('selected', true)
    };
  }

  function generarPassword() {
      $("#volunteer_initial_pass").val(generarCadenaAleatoria(10));
  }

  function generarCadenaAleatoria(longitud) {
      var strCaracteresPermitidos = 'a,b,c,d,e,f,g,h,i,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9';
      var strArrayCaracteres = new Array(strCaracteresPermitidos.length);
      strArrayCaracteres = strCaracteresPermitidos.split(',');
      var tmpstr = "";
      do {
          var randIndice = parseInt(Math.random() * strArrayCaracteres.length);
          tmpstr = tmpstr + strArrayCaracteres[randIndice];
      } while (tmpstr.length < longitud);
      return tmpstr.toString().toUpperCase();
  }
