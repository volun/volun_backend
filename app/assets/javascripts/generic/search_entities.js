
function search_js_entity(){
    search_generic([
        ['isSearch',true,'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['enter_text', $('#enter_text'), 's'],
        ['search_nombre', $('#search_nombre'), 's'],
        ['search_status', $('#search_status-select option:selected'), 's'],
        ['search_tipo', $('#search_tipo-select option:selected'), 's'],
        ['search_cif', $('#search_cif'), 's']
        ], '#recarga-filtros');
}

function search_js_entities_clean() {
    search_clear([$('#enter_text'),
        $('#search_nombre'),
        $('#search_cif'),
        $('#search_tipo-select'),
        $('#search_status-select'),
        $('#buttonSearchEntity')],'#recarga-filtros');
}


      
  function showOthersNoNested() {
    if ($("#entity_req_reason_id option:selected").html() == $("#others").val()) {
      $("#entity_other_subscribe_reason").parent().parent().show();
    } else {
      $("#entity_other_subscribe_reason").parent().parent().hide();
    }
  }
      
  function changeActive() {
    $("#entity_active").val(!$("#inactive:checkbox:checked").val());
    if($("#inactive:checkbox:checked").val()) {
      var d = new Date();
      var day = d.getDate();
      var month = d.getMonth() + 1;
      var year = d.getFullYear();
      var hours = d.getHours();
      var min = d.getMinutes();
      if (day < 10) {
        day = "0" + day;
      }
      if (month < 10) {
        month = "0" + month;
      }
      var strDate = day + "/" + month + "/" + year + " " + hours + ":" + min;
      $("#entity_unsubscribed_at").val(strDate);
    } else {
      $("#entity_unsubscribed_at").val("");
    }
  }

