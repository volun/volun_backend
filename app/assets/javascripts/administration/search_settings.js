function search_js_setting(){
    search_generic([
        ['isSearch',true,'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['search_key', $('#search_key'), 's'],
        ['search_value', $('#search_value'), 's'],
        ], '#recarga-filtros');
}

function search_js_settings_clean() {
    search_clear([$('#search_key'),
        $('#search_value'),
        $('#buttonSearchSetting')],'#recarga-filtros');
}

