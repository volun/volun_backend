
function search_js_manager(){
    search_generic([
        ['isSearch',true,'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['enter_text', $('#enter_text'), 's'],
        ['search_nombre', $('#search_nombre'), 's'],
        ['search_last_name', $('#search_last_name'), 's'],
        ['search_last_name_alt', $('#search_last_name_alt'), 's'],
        ['search_alias_name', $('#search_alias_name'), 's'],
        ], '#recarga-filtros');
}

function search_js_managers_clean() {
    search_clear([$('#enter_text'),
        $('#search_nombre'),
        $('#search_last_name'),
        $('#search_last_name_alt'),
        $('#search_alias_name'),
        $('#buttonSearchManager')],'#recarga-filtros');
}



