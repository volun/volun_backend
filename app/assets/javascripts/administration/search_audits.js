function search_js_audit() {
    search_generic([
        ['isSearch', true, 'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_type',$('#search_type-select option:selected'), 's'],
        ['search_manager', $('#search_manager'),'s'],
        ['search_resource', $('#search_resource'),'s'],
        ['search_operation', $('#search_operation-select option:selected'), 's'],
        ['search_start_date', $('#search_start_date'), 's'],
        ['search_end_date',$('#search_end_date'), 's'],
        ],'#recarga-filtros');
}

function search_js_audits_clean() {
    search_clear([$('#enter_text'),
        $('#search_type'),
        $('#search_manager'),
        $('#search_resource'),
        $('#search_operation'),
        $('#search_start_date'),
        $('#search_end_date'),
        $('#buttonSearch'),
        $('#warningLabelFechaFormat'),
        $('#warningLabelFechaFormat2'),
        $('#warningLabelFecha')], '#recarga-filtros');
}

