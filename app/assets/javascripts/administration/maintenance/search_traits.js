


function search_js_traits(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_nombre',$('#search_nombre'),'s']
        ],'#recarga-filtros');
}

function search_js_traits_clean() {
    search_clear([
        $('#enter_text'),
        $('#search_nombre'),
        $('#search_status-select')],
        '#recarga-filtros');
}



