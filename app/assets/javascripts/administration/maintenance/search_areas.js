


function search_js_area(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['search_id',$('#search_id'),'i'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_nombre',$('#search_nombre'),'s']
        ],'#recarga-filtros');
}
  
function search_js_area_clean() {
    search_clear([
        $('#search_id'),
        $('#search_nombre'),
        $('#enter_text'),
        $('#search_status-select')],
        '#recarga-filtros');
}

