


function search_js_service(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_nombre',$('#search_nombre'),'s'],
        ['search_description',$('#search_description'),'s'],
        ['search_code',$('#search_code'),'i']
        ],'#recarga-filtros');
}

function search_js_service_clean() {
    search_clear([
        $('#search_nombre'),
        $('#search_description'),
        $('#search_code'),
        $('#enter_text'),
        $('#search_status-select'),
        $('#warning_id')],
        '#recarga-filtros');
}


