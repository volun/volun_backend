

function search_js_service_question(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['search_type',$('#search_type-select option:selected'),'s'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_description',$('#search_description'),'s']
        ],'#recarga-filtros');
}

function search_js_service_question_clean() {
    search_clear([
        $('#search_description'),
        $('#search_type-select'),
        $('#enter_text'),
        $('#search_status-select')],
        '#recarga-filtros');
}



