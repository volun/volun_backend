
function search_js_resource(){
    search_generic([
        ['isSearch',true,'b'],
        ['page', 1, 'b'],
        
        ['per_page', $('#pages-select option:selected'), 'i'],
        ['search_description', $('#search_description'), 's'],
        ['enter_text', $('#enter_text'), 's'],
        ['search_nombre', $('#search_nombre'), 's'],
        ['search_status', $('#search_status-select option:selected'), 's'],
        ], '#recarga-filtros');
}

function search_js_resources_clean() {
    search_clear([$('#search_description'),
        $('#enter_text'),
        $('#search_nombre'),
        $('#search_status'),
        $('#buttonSearchResource')],'#recarga-filtros');
}

