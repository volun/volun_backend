

function search_js_properties(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_pages',$('#search_pages'),'s'],
        ['search_inicio_fech',$('#search_inicio_fech'),'s'],
        ['search_fin_fech',$('#search_fin_fech'),'s']
        ],'#recarga_apartados');
}

function search_js_properties_clean(){
    search_clear([
        $('#search_pages'),
        $('#search_inicio_fech'),
        $('#search_fin_fech')],
        '#recarga_apartados');
}


