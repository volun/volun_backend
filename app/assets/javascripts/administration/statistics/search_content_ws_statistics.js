

function search_js_ws_statistics() {
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_start_date',$('#search_start_date'),'s'],
        ['search_end_date',$('#search_end_date'),'s']
        ],'#recarga-filtros');
}


function search_js_ws_statistics_clean(){
    search_clear([
        $('#search_start_date'),
        $('#search_end_date')],
        '#recarga-filtros');
}


