
function search_js_project(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        ['search_pt_id',$('#search_pt_id'),'i'],
        ['search_subpt_id',$('#search_subpt_id'),'i'],
        ['per_page',$('#pages-select option:selected'),'i'],              
        ['search_ambito',$('#search_ambito-select option:selected'),'s'],
        ['search_entidad',$('#search_entidad-select option:selected'),'s'],
        ['search_colectivo',$('#search_colectivo-select option:selected'),'s'],
        ['search_district',$('#search_district-select option:selected'),'s'],
        ['search_status',$('#search_status-select option:selected'),'s'],
        ['search_subproyect_pt',$('#search_subproyect_pt-select option:selected'),'s'],
        ['search_subproyect_type',$('#search_subproyect_type-select option:selected'),'s'],
        ['search_id',$('#search_id'),'i'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_nombre',$('#search_nombre'),'s'],
        ['search_tipo',$('#search_tipo-select option:selected'),'s'],
        ['search_inicio_f',$('#search_inicio_f'),'s'],
        ['search_year',$('#search_year'),'i'],
        ['search_review',$('#search_review'),'check'],
        ['search_volun_allowed',$('#search_volun_allowed'),'check'],
        ['search_outstanding',$('#search_outstanding'),'check'],
        ['search_publish',$('#search_publish'),'check'],
        ['search_urgent',$('#search_urgent'),'check'],
        ['search_fin_f',$('#search_fin_f'),'s']
        ],'#recarga-filtros');
}
  
function search_js_projects_clean() {
    search_clear([$('#search_entidad-select'),
            $('#search_id'),
            $('#search_pt_id'),
            $('#search_subpt_id'),
            $('#search_nombre'),
            $('#search_volun_allowed'),
            $('#search_outstanding'),
            $('#search_publish'),
            $('#search_urgent'),
            $('#search_tipo-select'),
            $('#search_inicio_f'),
            $('#search_fin_f'),
            $('#enter_text'),
            $('#search_status-select'),
            $('#search_district-select'),
            $('#search_ambito-select'),
            $('#search_year'),
            $('#search_colectivo-select'),
            $('#search_subproyect_pt-select'),
            $('#search_subproyect_type-select'),
            $('#buttonSearchProject'),
            $('#warning_year_project'),
            $('#warningLabelFechaFormat'),
            $('#warningLabelFechaFormat2'),
            $('#warningLabelFecha'),
            $('#warning_id_project'),
          $('#search_review')],'#recarga-filtros');
}




$(document).on('turbolinks:load', function() {
    setInsurDate();
    $('.urls_block.js-files-block').on('nested:fieldAdded', function (event) {
        var $newAddedFile = $(this).find('.js-file-fields:last .js-file-link a.js-visit');
        $newAddedFile.hide();
    });
  });
  
  function checkElementNotInArray(elementToFind, array) {
    var params = {id: elementToFind, notFound: true};
    array.forEach(function(element) {
      if (element.id == this.id) {
        this.notFound = false;
      }
    }.bind(params));
    return params.notFound;
  }
  
  function filterImportance(checkId) {
    $.ajax({
      url: "/projects",
      dataType: 'json',
      success: function (data) {
        if (checkId == 'urgent') {
          if (data.urgent.length > 0) {
            data.urgent.forEach(function(element) {
              if (element.urgent && (element.id != $("#project_id").val())) {
                Swal.fire($("#message_title_urgent").val(), $("#message_body_urgent").val() + ': \'' + element.name + '\'', 'error');
                $('#urgent').prop('checked', false);
                return;
              }
            });
          }
        }
        if (checkId == 'outstanding') {
          if (data.outstanding.length > 2) {
            projects = "";
            data.outstanding.forEach(function(element) {
              if (element.id != $("#project_id").val()) {
                projects += element.name;
                if (data.outstanding.indexOf(element) != data.outstanding.length-1) {
                  projects += "', '";
                }
              }
            });
            if (projects.length > 2 && checkElementNotInArray($("#project_id").val(), data.outstanding)) {
              Swal.fire($("#message_title_outstanding").val(), $("#message_body_outstanding").val() + ': \'' + projects + '\'', 'error');
              $('#outstanding').prop('checked', false);
              return;
            }
          }
        }
        return {results: data};
      }
    });
  }
  
  // Disable insurance_date if available check is checked
  function setInsurDate() {
    if ($('[id$="_attributes_insured"]').prop("checked")) {
      $('[id$="_attributes_insurance_date"]').attr("disabled", false);
    } else {
      $('[id$="_attributes_insurance_date"]').attr("disabled", true);
      $('[id$="_attributes_insurance_date"]').val("");
    }
  }


