// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

//= require jquery
//= require rails-ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require cocoon
//= require jquery_nested_form
//= require sweetalert2
//= require sweet-alert2-rails
//= require select2
//= require turbolinks
//= require bootstrap
//= require ckeditor-jquery
//= require ckeditor/ckeditor
//= require_tree .


var confirm_submit = function (msg) {
    $('.js-confirmable').closest('form').submit(function() {
        var message = (msg === undefined) ? '¿Estás seguro?' : msg;
        var c = confirm(message);
        return c;
    });
};

var confirm_reload = function (msg) {
    $('.js-reloadable').on('click',function() {
        var message = (msg === undefined) ? '¿Estás seguro?' : msg;
        var c = confirm(message);
        if (c == true)
            return window.location.reload();
        else
            return false;
    });
};

var confirm_undo = function (msg) {
    confirm_reload(msg);
};
var update_hidden_inputs = function(elem, selector){
    if(elem.value)
        $(selector).prop('value', elem.checked);
    $('.search-form').submit();
}

function closeButton() {
    $('#btn-close').on('click',function() {
      window.close()
    });
}

function generarPassword() {
    $("#citizen_initial_pass").val(generarCadenaAleatoria(10));
}

function confirm_footer() {    
    confirm_submit(document.querySelector('meta[name="confirm_action_id"]').content);
    confirm_undo(document.querySelector('meta[name="confirm_undo_id"]').content);
}

$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert turbolinks:before-cache', function() {
    $('.ckeditor').each(function() { 
        if ($(this).attr('id')) {
            CKEDITOR.replace($(this).attr('id'));
        }
    });

    $('.js-example-basic-single').select2();
});






// Manage autocomplete for address blocks
$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert', function() {
    $.each($('.js-address'), function(i, address) {
        var $address = $(address);
        if ($address.find('.js-normalize').is(':checked')){
            setAddressAutocomplete($address);
        }//else{
        // Block rest of the fields
        $address.on('change', '.js-normalize', function () {
            //var blocked_fields = ['.js-road_number_type', '.js-road_type select', '.js-grader', '.js-postal_code'];

            var blocked_fields = ['.js-road_number_type', '.js-grader'];
            var $normalizeInput = $(this);
            var $_address = $normalizeInput.closest('.js-address');
            if ($normalizeInput.is(':checked')){

                // Prevent road type select from opening
               /* $address.find('.js-road_type select').on('mousedown', function(e) {
                    if ($(this).closest('.js-address').find('.js-normalize').is(':checked')){
                        e.preventDefault();
                        this.blur();
                        window.focus();
                    }
                });*/

                blocked_fields.forEach(function (field_selector, index) {
                    $_address.find(field_selector).attr('readonly', 'readonly');
                });
                $('.js-road_number-select-wrapper').removeAttr('hidden');
                $('.js-road_number-select').removeAttr('disabled');
                $('.js-road_number-input-wrapper').attr('hidden', 'hidden');
                $('.js-road_number-input').attr('disabled', 'disabled');
                addAddressAutocomplete($_address);
            }else{
                blocked_fields.forEach(function (field_selector, index) {
                    $_address.find(field_selector).removeAttr('readonly');
                });
                $('.js-road_number-select-wrapper').attr('hidden', 'hidden');
                $('.js-road_number-select').attr('disabled', 'disabled');
                $('.js-road_number-input-wrapper').removeAttr('hidden');
                $('.js-road_number-input').removeAttr('disabled');
                removeAddressAutocomplete($_address);
            }
        });
        //}
    });
});

$(document).on('click', '.add_fields', function(e) {
    $.each($('.js-address'), function(i, address) {
        var $address = $(address);
        if ($address.find('.js-normalize').is(':checked')){
            setAddressAutocomplete($address);
        }else{
        // Block rest of the fields
        $address.on('change', '.js-normalize', function () {
            //var blocked_fields = ['.js-road_number_type', '.js-road_type select', '.js-grader', '.js-postal_code'];

            var blocked_fields = ['.js-road_number_type', '.js-grader'];
            var $normalizeInput = $(this);
            var $_address = $normalizeInput.closest('.js-address');
            if ($normalizeInput.is(':checked')){

                // Prevent road type select from opening
               /* $address.find('.js-road_type select').on('mousedown', function(e) {
                    if ($(this).closest('.js-address').find('.js-normalize').is(':checked')){
                        e.preventDefault();
                        this.blur();
                        window.focus();
                    }
                });*/

                blocked_fields.forEach(function (field_selector, index) {
                    $_address.find(field_selector).attr('readonly', 'readonly');
                });
                $('.js-road_number-select-wrapper').removeAttr('hidden');
                $('.js-road_number-select').removeAttr('disabled');
                $('.js-road_number-input-wrapper').attr('hidden', 'hidden');
                $('.js-road_number-input').attr('disabled', 'disabled');
                addAddressAutocomplete($_address);
            }else{
                blocked_fields.forEach(function (field_selector, index) {
                    $_address.find(field_selector).removeAttr('readonly');
                });
                $('.js-road_number-select-wrapper').attr('hidden', 'hidden');
                $('.js-road_number-select').attr('disabled', 'disabled');
                $('.js-road_number-input-wrapper').removeAttr('hidden');
                $('.js-road_number-input').removeAttr('disabled');
                removeAddressAutocomplete($_address);
            }
        });
        }
    });
});




// Allow autocomple after adding a new address block
$(document).on('ready load beforeunload page:load turbolinks:load nested:fieldAdded cocoon:after-insert', function(event){  
    var $addNestedField = $($(this)[0].activeElement);
    var $newAddedAddress = $addNestedField.closest('.events-block').find('.event-fields:last .js-address');
    
    setAddressAutocomplete($newAddedAddress);
});

var setAddressAutocomplete = function ($address) {
    addAddressAutocomplete($address);
    blockBdcFields($address);
};
window.setAddressAutocomplete = setAddressAutocomplete;

// Block BDC fields
var blockBdcFields = function ($address) {

    // Prevent road type select from opening
   /* $address.find('.js-road_type select').on('mousedown', function(e) {
        if ($(this).closest('.js-address').find('.js-normalize').is(':checked')){
            e.preventDefault();
            this.blur();
            window.focus();
        }
    });*/

    // Block rest of the fields
    //var blocked_fields = ['.js-road_number_type', '.js-road_type select', '.js-grader', '.js-postal_code'];
    var blocked_fields = ['.js-road_number_type', '.js-grader'];
    $address.on('change', '.js-normalize', function () {
        var $normalizeInput = $(this);
        var $_address = $normalizeInput.closest('.js-address');
        if ($normalizeInput.is(':checked')){

            // Prevent road type select from opening
           /* $address.find('.js-road_type select').on('mousedown', function(e) {
                if ($(this).closest('.js-address').find('.js-normalize').is(':checked')){
                    e.preventDefault();
                    this.blur();
                    window.focus();
                }
            });*/

            blocked_fields.forEach(function (field_selector, index) {
                $_address.find(field_selector).attr('readonly', 'readonly');
            });
            $('.js-road_number-select-wrapper').removeAttr('hidden');
            $('.js-road_number-select').removeAttr('disabled');
            $('.js-road_number-input-wrapper').attr('hidden', 'hidden');
            $('.js-road_number-input').attr('disabled', 'disabled'); 
            addAddressAutocomplete($_address);
        }else{
            blocked_fields.forEach(function (field_selector, index) {
                $_address.find(field_selector).removeAttr('readonly');
            });
            $('.js-road_number-select-wrapper').attr('hidden', 'hidden');
            $('.js-road_number-select').attr('disabled', 'disabled');
            $('.js-road_number-input-wrapper').removeAttr('hidden');
            $('.js-road_number-input').removeAttr('disabled');
            removeAddressAutocomplete($_address);
        }
    });
};
window.blockBdcFields = blockBdcFields;

var removeAddressAutocomplete = function($address) {
    $address.find(".js-town").autocomplete({source: []});
    $address.find(".js-road_name").autocomplete({source: []});
};

window.removeAddressAutocomplete = removeAddressAutocomplete;

var addAddressAutocomplete = function($address) {
    function fillTown($townInput, message) {
        $townInput.scrollTop(0);
        $townInput.val(message);
    }

    // AUTOCOMPLETE TOWN
    var $input = $address.find(".js-town")
    var searchTownsUrl = $input.data('js-search-towns-url');
    $input.autocomplete({
        source: function(request, response) {

            var value = $.trim($input.val());
            if(value.length > 0){
                $.ajax({
                    url: searchTownsUrl,
                    dataType: "json",
                    data: {
                        address: {
                            country: $address.find(".js-country").val(),
                            province: $address.find(".js-province").val(),
                            town: $address.find(".js-town").val(),
                            road_type: $address.find(".js-road_type option:selected").val(),
                            road_name: $address.find(".js-road_name").val(),
                            road_number_type: $address.find(".js-road_number_type").val(),
                            road_number: $address.find(".js-road_number").val() ? $address.find(".js-road_number").val() : [0],
                            grader: $address.find(".js-grader").val(),
                            stairs: $address.find(".js-stairs").val(),
                            floor: $address.find(".js-floor").val(),
                            door: $address.find(".js-door").val()
                        },
                    },
                    success: function(data) {
                        response(data.towns);
                    }
                });
            }
        },
        minLength: 3,
        select: function(event, ui) {
            fillTown($(this), ui.item ? ui.item.label : "Nothing selected, input was " + this.value);
        },
        open: function() {
        },
        close: function() {
            $('.ui-autocomplete-loading').removeClass('ui-autocomplete-loading');
            $address = $(this).closest(".js-address");
            $address.find(".js-road_type select").prop('selectedIndex',0);
            $address.find(".js-road_name").val('');
            $address.find(".js-road_number_type").val('');
            $address.find(".js-road_number-select").prop('selectedIndex',0);
            $address.find(".js-grader").val('');
            $address.find(".js-postal_code").val('');
        }
    });

    // AUTOCOMPLETE ROAD NAME
    var roads;
    function fillRoadName($roadNameInput, message) {
        $roadNameInput.scrollTop(0);
        message = message || '';
        var roadName = message.split(" (")[0];
        var roadTypeName = message.split(" (")[1].replace(/\)/, "");
        var selectedRoad = roads.filter(function(road) {
            return ( road.nomVial.trim() == roadName && road.nomClase.trim() == roadTypeName );
        })[0];


        if (typeof selectedRoad !== 'undefined') {
            $roadNameInput.closest(".js-address").find(".js-road_type select option").filter(function() {
                return $(this).text().toLowerCase() == selectedRoad.nomClase.trim().toLowerCase();
            }).prop('selected', true);
        }
    }

    $address.find(".js-road_name").autocomplete({
        source: function(request, response) {
            var value = $.trim($address.find(".js-town").val());
            var searchRoadNamesUrl = $address.find('.js-road_name').data('js-search-road-names-url');
            if(value.length > 0) {
                $.ajax({
                    url: searchRoadNamesUrl,
                    dataType: "json",
                    data: {
                        address: {
                            country: $address.find(".js-country").val(),
                            province: $address.find(".js-province").val(),
                            town: $address.find(".js-town").val(),
                            road_type: '',
                            road_name: $address.find(".js-road_name").val(),
                            road_number_type: $address.find(".js-road_number_type").val(),
                            road_number: $address.find(".js-road_number-input").val() ? $address.find(".js-road_number-input").val() : 0,
                            grader: $address.find(".js-grader").val(),
                            stairs: $address.find(".js-stairs").val(),
                            floor: $address.find(".js-floor").val(),
                            door: $address.find(".js-door").val()
                        }
                    },
                    success: function (data) {
                        roads = data.roads;
                        if (roads.length > 0){
                            var roadList = [];
                            $.each(roads, function (index, road) {
                                roadList.push(road.nomVial.trim() + ' (' + road.nomClase.trim() + ')');
                            });
                            response(roadList);

                        }else{
                            $address.find(".js-road_number_type").val('');
                            $address.find(".js-grader").val('');
                            $address.find(".js-postal_code").val('');
                            $address.find(".js-road_number-input").val('');
                            response({})
                        }
                    }
                });
            }else{
                alert('El campo "Población" no puede estar vacío');
                response({});
            }
        },
        minLength: 3,
        select: function(event, ui) {
            fillRoadName($(this), ui.item ? ui.item.label : "Nothing selected, input was " + this.value);
        },
        open: function() {
        },
        close: function() {
            $('.ui-autocomplete-loading').removeClass('ui-autocomplete-loading');
            $input = $(this);
            value = $input.val() || '';
            $input.val(value.replace(/\s*\(\w*\)/, ""));
            $address = $input.closest(".js-address");
            $address.find(".js-road_number_type").val('');
            $address.find(".js-grader").val('');
            $address.find(".js-postal_code").val('');
            $address.find(".js-road_number-input").val('');

            // Fill ROAD NUMBER
            $("#volunteer_address_attributes_road_number").empty();
            $("#volunteer_address_attributes_road_number").append($("<option>").attr('value','').text(''));
            var value = $.trim($address.find(".js-road_name").val());
            var searchRoadNumbersUrl = $address.find('.js-road_number-input').data('js-search-road-numbers-url');
            if(value.length > 0) {
                $.ajax({
                    url: searchRoadNumbersUrl,
                    dataType: "json",
                    data: {
                        address: {
                            country: $address.find(".js-country").val(),
                            province: $address.find(".js-province").val(),
                            town: $address.find(".js-town").val(),
                            road_type: $address.find(".js-road_type option:selected").val(),
                            road_name: $address.find(".js-road_name").val(),
                        }
                    },
                    success: function (data) {
                        var numbers = data.numbers;
                        var numbersList = [];
                        if (numbers.length > 1) {
                            $.each(numbers, function (index, number) {
                                if (number.calificador == null)
                                    numbersList.push(number.numero)
                                else
                                    numbersList.push(number.numero + ' (' + number.calificador.trim() + ')')
                            });
                        } else {
                            if (numbers.calificador == null)
                                numbersList.push(numbers.numero)
                            else
                                numbersList.push(numbers.numero + ' (' + numbers.calificador.trim() + ')')
                        }
                        var selectArr = numbersList.map(function(elem){ return {val: elem, text: elem}});
                        var $roadNumberSelect = $('.js-road_number-select');
                        $(selectArr).each(function() {
                            $roadNumberSelect.append($("<option>").attr('value',this.val).text(this.text));
                        });
                        $roadNumberSelect.removeAttr('readonly');
                        $roadNumberSelect.change(function(){
                            var selectedNumberOption = $(this).prop('value') || '';
                            if (selectedNumberOption.indexOf("(") >= 0){
                                var numberName = selectedNumberOption.split(" (")[0];
                                var graderName = selectedNumberOption.split(" (")[1].replace(/\)/, "");
                            }else{
                                var numberName = selectedNumberOption;
                                var graderName = null;
                            }
                            $address = $(this).closest('.js-address');
                            $.ajax({
                                url: searchRoadNumbersUrl,
                                dataType: "json",
                                data: {
                                    address: {
                                        country: $address.find(".js-country").val(),
                                        province: $address.find(".js-province").val(),
                                        town: $address.find(".js-town").val(),
                                        road_type: $address.find(".js-road_type option:selected").val(),
                                        road_name: $address.find(".js-road_name").val(),
                                        road_number: numberName,
                                        grader: graderName,
                                    }
                                },
                                success: function (data) {
                                    if (data.numbers.length > 1) {
                                        var numbers = data.numbers;
                                    } else {
                                        var numbers = [];
                                        numbers.push(data.numbers);
                                    }
                                    var selectedNumber = numbers.filter(function(number) {
                                        return ( number.numero.toLowerCase() == numberName.toLowerCase() && number.calificador.trim() == graderName );
                                    })[0];

                                    if (typeof selectedNumber === 'undefined') {
                                        selectedNumber = numbers.filter(function (number) {
                                            return ( number.numero.toLowerCase() == numberName.toLowerCase());
                                        })[0];
                                    }

                                    $address.find(".js-road_number_type").val(selectedNumber.tipoNumeracion);
                                    var graderValue = (selectedNumber.calificador != null) ? selectedNumber.calificador.trim() : '  ';
                                    $address.find(".js-grader").val(graderValue);
                                    $address.find('.js-postal_code').val(selectedNumber.codPostal)
                                    

                                    $.ajax({
                                      url: "/volun/addresses/bdc_get_district_borought",
                                      dataType: "json",
                                      data: {
                                        dis_code: selectedNumber.codDis,
                                        bar_code: selectedNumber.codBar
                                      },
                                      success: function(dataintern) {
                                        $address.find('.js-district').val(dataintern.district_name)
                                        $address.find('.js-borought').val(dataintern.borought_name)
                                  }});
                                    console.log(selectedNumber)
                                    console.log(data)
                                }
                            });
                        });

                    },
                    error: function () {
                    }
                });
            }else{
                alert('El campo "Nombre de la vía" no puede estar vacío');
            }
        }
    });
};

window.addAddressAutocomplete = addAddressAutocomplete;


// var Inputmask = $.fn.inputmask.Constructor;

// // Prevent input field from content removing when uncompleted
// Inputmask.prototype.blurEvent = function() {
//     return
// };

// Show field others in assessments
function showComments(selId) {
    var otherId = selId.split("_id")[0] + "_other";
    if ($("#"+selId+" option:selected").html() == $("#others").val()) {
      $("#"+otherId).parent().parent().show();
    } else {
      $("#"+otherId).parent().parent().hide();
    }
  }

  window.showComments = showComments;

  function setAgrmtDate() {
    if (!$("#volunteer_agreement").prop("checked")) {
      $("#volunteer_agreement_date").attr("disabled", true);
      $("#volunteer_agreement_date").val("");
    } else {
      $("#volunteer_agreement_date").attr("disabled", false);
    }
  }
  window.setAgrmtDate= setAgrmtDate;

  function filterDegree(degreeTypeId) {
    let degreeId = degreeTypeId.split("_degree_type_id")[0] + "_name";
    let dt_id = $("#" + degreeTypeId + " option:selected").val();
    $.ajax({
      url: "/volunteers",
      dataType: 'json',
      data: {
          dt_id: dt_id
      },
      success: function (data) {
        $("#"+degreeId).empty();
        data.forEach(function(element) {
          $("#"+degreeId).append("<option value='" + element.name + "'>" + element.name + "</option>");
        });
        return {results: data};
      }
    });
  }

  window.filterDegree = filterDegree;

 




