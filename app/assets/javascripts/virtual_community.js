var x_sort_origin=0;
var x_sort_count_origin=0;
var x_sort_popular=0;
var x_sort_count_popular=0;
/*  VALORES: 
    1-> ASC
    2-> DESC
    0-> NADA
*/ 
function sort_elements(params){
    switch (params){

        case 'search_landing_page':
            x_sort_count_origin=0;
            if(x_sort_origin == 0) {
                x_sort_origin = 1;
            } else if(x_sort_origin == 1) {
                x_sort_origin = 2;
            } else {
                x_sort_origin = 1;
            }
            search_project([
                ['order_landing_page',x_sort_origin,'b'],
                ['isSearch',true,'b'],
                ['page',1,'b'],
                
                ['per_page',$('#pages-select option:selected'),'i'],
                ['search_origin',$('#search_origin'),'s'],
                ['search_inicio_f',$('#search_inicio_f'),'s'],
                ['search_fin_f',$('#search_fin_f'),'s']
            ],'#recarga');
        break;

        case 'count_origin':
            x_sort_origin = 0;
            
            if(x_sort_count_origin == 0) {
                x_sort_count_origin = 1;
            } else if(x_sort_count_origin == 1) {
                x_sort_count_origin = 2;
            } else {
                x_sort_count_origin = 1;
            }
            search_project([
                ['order_count_origin',x_sort_count_origin,'b'],
                ['isSearch',true,'b'],
                ['page',1,'b'],
                
                ['per_page',$('#pages-select option:selected'),'i'],
                ['search_origin',$('#search_origin'),'s'],
                ['search_inicio_f',$('#search_inicio_f'),'s'],
                ['search_fin_f',$('#search_fin_f'),'s']
            ],'#recarga');
        break;

        case 'search_properties':
            x_sort_count_popular=0;
            if(x_sort_popular == 0) {
                x_sort_popular = 1;
            } else if(x_sort_popular == 1) {
                x_sort_popular = 2;
            } else {
                x_sort_popular = 1;
            }
            search_project([
                ['order_properties',x_sort_popular,'b'],
                ['isSearch',true,'b'],
                ['page',1,'b'],
                
                ['per_page',$('#pages-select option:selected'),'i'],
                ['search_pages',$('#search_pages'),'s'],
                ['search_inicio_fech',$('#search_inicio_fech'),'s'],
                ['search_fin_fech',$('#search_fin_fech'),'s']
            ],'#recarga_apartados');
        break;
        case 'count_popular':
            x_sort_popular=0;
            if(x_sort_count_popular == 0) {
                x_sort_count_popular = 1;
            } else if(x_sort_count_popular == 1) {
                x_sort_count_popular = 2;
            } else {
                x_sort_count_popular = 1;
            }
            search_project([
                ['order_count_popular',x_sort_count_popular,'b'],
                ['isSearch',true,'b'],
                ['page',1,'b'],
                
                ['per_page',$('#pages-select option:selected'),'i'],
                ['search_pages',$('#search_pages'),'s'],
                ['search_inicio_fech',$('#search_inicio_fech'),'s'],
                ['search_fin_fech',$('#search_fin_fech'),'s']
            ],'#recarga_apartados');
        break;
    }
    
}