var isValid = true;
var errores_desplegados=new Array
/**
 * @param {*} parametros array de parámetros de la búsqueda
 * @param {*} div parte de la vista a recargar
 * @description Función encargada de transformar los datos 
 * de los campos en una url, para ello tratará los datos
 * según un parámetro adicional, ya que es necesario para
 * evitar fallos o conseguir un valor determinado.
 */

 function search_generic(parametros,div){
    var url_params="";
    var i = 0;
    for(i=0;i<parametros.length;i++){
        var conversion="";
        var conca="";
        if(url_params!="") conca="&";
        if(parametros[i][1].length!=0){
            switch(parametros[i][2]){
                case 'i': //Conversión a numérico
                    if (parametros[i][1].val()!='' && parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion= parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[ ]/g,"").trim();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'b': //Dato simple
                
                    if (parametros[i][1]!='' && parametros[i][1]!='undefined' && parametros[i][1] != null) {
                        conversion=parametros[i][1];
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 's': //Conversión a texto
       
                    if (parametros[i][1].val()!='' &&parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion=parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[\'%();]/g,"").trim();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'm':
                    if (parametros[i][1].val()!='' && parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion=parametros[i][1].val();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'check': //Obtención de valor booleano
                    if(parametros[i][1].prop('checked')){
                        conversion=parametros[i][1].prop('checked');
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
            }
        }
    }
    var union="?";
    //if(window.location.href.includes(union)) union="&";
    $(location).attr('href', window.location.pathname+union+encodeURI(url_params)+div);
    //$(div).load(window.location.href+union+encodeURI(url_params)+" "+div);
}

function search_generic_turbo(parametros,div){
    var url_params="";
    var i = 0;
    for(i=0;i<parametros.length;i++){
        var conversion="";
        var conca="";
        if(url_params!="") conca="&";
        if(parametros[i][1].length!=0){
            switch(parametros[i][2]){
                case 'i': //Conversión a numérico
                    if (parametros[i][1].val()!='' && parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion= parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[ ]/g,"").trim();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'b': //Dato simple
                
                    if (parametros[i][1]!='' && parametros[i][1]!='undefined' && parametros[i][1] != null) {
                        conversion=parametros[i][1];
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 's': //Conversión a texto
       
                    if (parametros[i][1].val()!='' &&parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion=parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[\'%();]/g,"").trim();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'm':
                    if (parametros[i][1].val()!='' && parametros[i][1].val()!='undefined' && parametros[i][1].val() != null) {
                        conversion=parametros[i][1].val();
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'check': //Obtención de valor booleano
                    if(parametros[i][1].prop('checked')){
                        conversion=parametros[i][1].prop('checked');
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
                case 'ck':
                    conversion= CKEDITOR.instances[parametros[i][1]].getData();
                    if (conversion != '' && conversion !='undefined' && conversion != null) {
                        url_params=url_params+conca+parametros[i][0]+"="+conversion;
                    }
                    break;
            }
        }
    }
    var union="?";
    if(window.location.href.includes(union)) union="&";
    //$(location).attr('href', window.location.pathname+union+encodeURI(url_params)+div);
    $(div).load(window.location.href+union+encodeURI(url_params)+" "+div);
}

/**
 * @param {*} parametros array de parámetros de la búsqueda
 * @param {*} url destino a recargar
 * @description Función encargada de transformar los datos 
 * de los campos en una url, para ello tratará los datos
 * según un parámetro adicional, ya que es necesario para
 * evitar fallos o conseguir un valor determinado.
 */
 function redirect_to_url(parametros,url){
    var url_params="";
    for(i=0;i<parametros.length;i++){
        var conversion="";
        switch(parametros[i][2]){
            case 'i': //Conversión a numérico
                conversion=parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[ ]/g,"").trim();
                break;
            case 'b': //Dato simple
                conversion=parametros[i][1];
                break;
            case 's': //Conversión a texto
                conversion=parametros[i][1].val() == null ? '' : parametros[i][1].val().replace(/[\'%();]/g,"").trim();
                break;
            case 'check': //Obtención de valor booleano
                conversion=parametros[i][1].prop('checked');
                break;
        }
        
        var conca="";
        if(url_params!="") conca="&";
        url_params=url_params+conca+parametros[i][0]+"="+conversion;
    }
    var union="?";
    $('#recarga-flash').load(url+union+encodeURI(url_params) + " #recarga-flash" );
}

/**
 * @param {*} parametros array de parametros del filtro
 * @param {*} div parte de la vista a recargar
 * @description Función para dejar el valor por defecto
 * en los filtros de búsqueda.
 */
 function search_clear(parametros,div) {
    var i = 0;
    for(i=0;i<parametros.length;i++){
        if(parametros[i].is(':checkbox')){
            parametros[i].prop('checked',false);
        }else if(parametros[i].is(':button')){
            parametros[i].prop('disabled',false);
        }else if(parametros[i].prop('tagName')==="LABEL"){
            parametros[i].hide();
        }else{
            parametros[i].val("");
            parametros[i].css('border', '1px solid LightGray');
        }
    }
    //errores_desplegados=new Array;
    errores_desplegados = new Array;
    var index=window.location.href.split('?');
    var union="?";
    // if(index.length>1) $(div).load(index[0]+union+"clean=true&"+index[1]+" "+div);
    // else $(div).load(index[0]+union+"clean=true "+div);

    $(location).attr('href', window.location.pathname+div);
} 

function search_clear_turbo(parametros,div) {
    var i = 0;
    for(i=0;i<parametros.length;i++){
        if(parametros[i].is(':checkbox')){
            parametros[i].prop('checked',false);
        }else if(parametros[i].is(':button')){
            parametros[i].prop('disabled',false);
        }else if(parametros[i].prop('tagName')==="LABEL"){
            parametros[i].hide();
        }else{
            parametros[i].val("");
            parametros[i].css('border', '1px solid LightGray');
        }
    }
    //errores_desplegados=new Array;
    errores_desplegados = new Array;
    var index=window.location.href.split('?');
    var union="?";

    console.log("pruebas")
    if(index.length>1) $(div).load(index[0]+union+"clean=true&"+index[1]+" "+div);
    else $(div).load(index[0]+union+"clean=true "+div);

    
} 

/**
 * 
 * @param {*} a 
 * @param {*} parametros 
 * @param {*} div 
 * @description Función que realiza busquedas cuando pulsamos la tecla enter
 * isValid , es un indicar para cmprobar si hay algun error de validación
 * En caso de estar correcto , se llama a la función buscar pasando todos los
 * parametros de busqueda y el div que vamos a actualizar
 */
 function search_enter(a,funcion){
    if (a.keyCode === 13 && isValid){
        for(var i=0;i<errores_desplegados.length;i++){
            if(errores_desplegados[i][1]==true){
               return;
            } 
        }
        funcion();
    }
}

 function changed_review(identificador,div,params){
    params.push(['id_check',identificador.attr("id"),'b']);
    search_generic(params,div);
}

 function changed_select(div,params){
    search_generic(params,div);
}

 function change_param_parametrize(param, section) {
    var data_send="{}"
    var data_url=""
  
    if (param.length !=0) {
      if (typeof param[1] === 'string') { 
        data_send = "{\""+param[0]+"\": \""+param[1]+"\" }";
      } else {
        data_send = "{\""+param[0]+"\": "+param[1]+" }";
      }
      if (section.includes("?")) {
        data_url = "&"+param[0]+"="+param[1];
      } else {
        section = section + '/'
        data_url = "?"+param[0]+"="+param[1];
      }
    }
    data_send=JSON.parse(data_send)
    $.ajax({
      type: 'get',
      url: '/'+section + data_url,
      data: data_send,
      success: function (data, status) {
        
        window.history.pushState(data, "Title",'/'+section+data_url);
      }});
}

 function correct_string(element){
    element.val(element.val().replace(/[\'%();]/g,""));
}


 function is_empty(element,buttonToDisable){
    if(element.val().length == 0){
        $('#'+buttonToDisable).prop("disabled", true);
    }else{
        $('#'+buttonToDisable).prop("disabled", false);
    }
}

 function validar_email(sender,recipent,message, buttonToDisable) {
    var regex = (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    var remitente = document.getElementById(sender).value;
    var destinatario =document.getElementById(recipent).value;
    var msg = document.getElementById(message).value;
    var remit_valid = regex.test(remitente);
    var destin_valid = regex.test(destinatario);
    var message_valid = msg.length != 0;

  if (remit_valid && destin_valid && message_valid){
    document.getElementById(buttonToDisable).disabled = false;
  }else{
    document.getElementById(buttonToDisable).disabled = true;
  }
    
}

 function is_number(numero,element,buttonToDisable){
    if(!/^([0-9])*$/.test(numero.val())){
        isValid = false;
        comprobarError(numero.prop("id")+"_n",false)
        numero.css('border', '1px solid red');
        showValidationText(element)
        abledOrdisabledElement(buttonToDisable,true);
        element.offset({top: element.offset().top, left: numero.offset().left}); 
    }else{
        comprobarError(numero.prop("id")+"_n",true)
        isValid = true;
        numero.css('border', '1px solid LightGray');
        hideValidationText(element)
        abledOrdisabledElement(buttonToDisable,false);
    }
}

 function valida_number_until(hasta,desde,element,elementValidFormatDesde,elementValidFormatHasta,buttonToDisable) {
    var aux_desde=parseInt(desde.val());
    var aux_hasta=parseInt(hasta.val());
    var aux_bool=true;
    var aux_css='1px solid LightGray';

    if((aux_desde > aux_hasta) && aux_hasta!='' && aux_desde!=''){
        aux_bool=false;
        aux_css='1px solid red';
        showValidationText(element)
    }else{
        hideValidationText(element)
    }

    isValid = aux_bool;
    comprobarError(hasta.prop("id")+"_d",aux_bool)
    comprobarError(desde.prop("id")+"_d",aux_bool)
    desde.css('border', aux_css);
    hasta.css('border', aux_css);
    abledOrdisabledElement(buttonToDisable,!aux_bool);
    element.offset({top: element.offset().top, left: desde.offset().left}); 
}

/**
 * @param {*} hasta id del campo hasta
 * @param {*} desde id del campo desde
 * @param {*} element id del texto de error
 * @param {*} buttonToDisable id del boton
 * @description Función para validar si el valor de un campo desde
 * es superior al de un campo hasta.
 * Primero se comprueba que los valores introducidos estan en el formato correcto,
 * en caso de no estar en el formato correcto se pondrá la caja de texto en color
 * rojo y se mostrará un mensaje de error.
 * Si el formato es correcto , se comprueba que la fecha desde no sea superior a la
 * fecha hasta.
 * Si el valor es superior , se muestra la validación ,se pone de rojo
 * la caja de texto y se deshabilita el boton.
 * Si no hay boton , no pasar ese campo
 */
 function valida_date_until(hasta,desde,element,elementValidFormatDesde,elementValidFormatHasta,buttonToDisable){
    var isValidHasta = valida_date(hasta,elementValidFormatHasta,buttonToDisable);
    var isValidDesde = valida_date(desde,elementValidFormatDesde,buttonToDisable);
    if(isValidDesde && isValidHasta){
        var aux_desde=convertArrayToDate(desde.val());
        var aux_hasta=convertArrayToDate(hasta.val());
        var aux_bool=true;
        var aux_css='1px solid LightGray';

        if((aux_desde.getTime() > aux_hasta.getTime()) && aux_hasta!='' && aux_desde!=''){
            aux_bool=false;
            aux_css='1px solid red';
            showValidationText(element)
        }else{
            hideValidationText(element)
        }

        isValid = aux_bool;
        comprobarError(hasta.prop("id")+"_d",aux_bool)
        comprobarError(desde.prop("id")+"_d",aux_bool)
        desde.css('border', aux_css);
        hasta.css('border', aux_css);
        abledOrdisabledElement(buttonToDisable,!aux_bool);
        element.offset({top: element.offset().top, left: desde.offset().left}); 
    }
}

 function showValidationText(element){
    element.show();
}
 function hideValidationText(element){
        element.hide();
}

 function valida_date(fecha,element,buttonToDisable){
    var isValidDate;
    var aux_bool=true;
    var aux_css='1px solid LightGray';
    var vregexNaix = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$ || (\d{4}-\d{2}-\d{2})/;
    if(!vregexNaix.test(fecha.val()) && (fecha.val()!="")){
        aux_bool=false;
        aux_css='1px solid red';
        isValidDate = false;
        showValidationText(element)
    }else{
        hideValidationText(element)
        isValidDate = true;
    }
    fecha.css('border', aux_css);
    comprobarError(fecha.prop("id")+"_format_d",aux_bool);
    abledOrdisabledElement(buttonToDisable,!aux_bool);
    return isValidDate;
}

 function valida_date_popup(fecha,buttonToDisable){
    var isValidDate;
    var aux_bool=true;
    var aux_css='1px solid LightGray';
    var vregexNaix = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
    if(!vregexNaix.test(fecha.val()) || (fecha.val()=="")){
        aux_bool=false;
        aux_css='1px solid red';
        isValidDate = false;
    }else{
        isValidDate = true;
    }
    fecha.css('border', aux_css);
    //comprobarError(fecha.prop("id")+"_format_d",aux_bool);
    abledOrdisabledElement(buttonToDisable,!aux_bool);
    return isValidDate;
}

 function review_hour(hora, buttonToDisable) {
    var vregexNaix = /^([0-2][0-9][:][0-5][0-9])/;
    var aux_css='1px solid LightGray';
    var aux_bool=true;
    if(!vregexNaix.test(hora.val())){
        aux_css='1px solid red';
        aux_bool=false;
    }
    hora.css('border', aux_css);
    abledOrdisabledElement(buttonToDisable,!aux_bool);
}

/**
 * 
 * @param {*} element Id del elemento que vamos a utilizar
 * @param {*} value Boolean para indicar si queremos habilitar o deshabilitar
 * @description Función para habilitar o deshabilitar un elemento 
 * del html(boton por ejemplo)
 */
 function abledOrdisabledElement(element,value){
    for(var i=0;i<errores_desplegados.length;i++){
        if(errores_desplegados[i][1]==true){
            value=true;
            break;
        } 
    }
    if (element) {
        document.getElementById(element).disabled = value;   
    } 
}

 function comprobarError(error,correcto){
    var indice=-2;
    for(var x=0;x<errores_desplegados.length;x++){
        if(errores_desplegados[x][0]==error){
            indice=x;
            break;
        } 
        indice=-1;
    }
    if(correcto && indice>=0) errores_desplegados[indice][1]=false;
    else if(!correcto && (indice==-1||errores_desplegados.length==0)) errores_desplegados[errores_desplegados.length]=[error,true];
}

/**
 * 
 * @param {*} aux_date valor de la caja de texto de fecha
 * @description Función para convertir los parametros que vemos en la caja de texto
 * a tipo Date.
 * Primero eliminamos las / del dato que recibimos.
 * Y luego devolvemos un tipo Date formado con los parametros del array
 */
 function convertArrayToDate(aux_date){
    aux_date = aux_date.split("/");
    return new Date(
        parseInt(aux_date[2], 10),
        parseInt(aux_date[1], 10) - 1,
        parseInt(aux_date[0], 10))
}


const css_valid = '1px solid Lightgrey';
const css_invalid = '1px solid #a94442';

function validation_pass(elementPhone, elementEmail, items_disable) {
    // items_disable [item1, ...]
    var valido = true;
    
        switch(tipo){
            case "telefono":  
            valido = validation_telefono(element);
            break;
            case "email":  
            valido = validation_email(element);
            break;
        }

        

    for(i = 0; i < item_disable.length; i++){
        item_disable[i].prop('disabled',valido);
    }


}

/**
 * VALIDACION STRING
 * @param {*} element 
 * @param {*} message 
 */
function validation_string(element, message) {
    var retired_volunteer = /[\'%();]/g;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }
    if (regex.test(element.val()) && element.val()!="") {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        if (message) message.hide();
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION NUMBER
 * @param {*} element 
 * @param {*} message 
 */
function validation_number(element, message) {
    var regex = /^([0-9])*$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }

    if (!regex.test(element.val()) && element.val()!="") {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        if (message) message.hide();
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION EMAIL
 * @param {*} element 
 * @param {*} message 
 */
 function validation_email(element, message) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        if (message) message.hide();
        return;
    }
    if (!regex.test(element.val()) && element.val()!="") {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        if (message) message.hide();
    }

}

window.validation_email = validation_email;

/**
 * VALIDACION EMPTY
 * @param {*} element 
 * @param {*} message 
 */
function validation_empty(element, message) {
    var aux_css = css_valid;
    if (!element) return;
    if (element.val() == null || element.val() == undefined || $.trim(element.val()) == '') {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        if (message) message.hide();
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION DATE
 * @param {*} element 
 * @param {*} message 
 */
function validation_date(element, message) {
    var regex = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }

    if (!regex.test(element.val()) && element.val()!="") {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        if (message) message.hide();
    }

    element.css('border', aux_css);
}

function validation_document_type(element, message,message2,type){
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        if (message2) message2.hide();
        return;
    }
    switch(type.val()){
        case '1': //DNI
        message2.hide();
        validation_dni(element,message);
        break;

        case '3': //NIE
        message.hide();
        validation_nie(element,message2);
        break;

        default:
        message2.hide();
        message.hide();
        element.css('border', aux_css);
    }
}
window.validation_document_type = validation_document_type;

function setVolunAge() {
    if ($("#volunteer_birth_date").length > 0) {
      var birth = $("#volunteer_birth_date").val().split("/");
      var today = new Date();
      var today_day = today.getDate();
      var today_month = today.getMonth() + 1;
      var today_year = today.getFullYear();
      var birth_day = parseInt(birth[0]);
      var birth_month = parseInt(birth[1]);
      var birth_year = parseInt(birth[2]);
      var age = 0;
      if (today_month > birth_month) {
            age = today_year - birth_year;
        } else if (today_month == birth_month) {
            if (today_day >= birth_day) {
                age = today_year - birth_year;
            } else {
                age = today_year - birth_year - 1;
            }
        } else {
            age = today_year - birth_year - 1;
        }
      if (isNaN(age) || age > 200 || age < 0) {
        $("#age").val("0");
      } else {
        $("#age").val(age);
      }
    } else {
      $("#age").val("0");
    }
  }

  window.setVolunAge= setVolunAge;
/**
 * VALIDACION DNI
 * @param {*} element 
 * @param {*} message 
 */
function validation_dni(element, message) {
    var regex = /^(\d{8})([A-Z])$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }

    if (!regex.test(element.val())) {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        var numero = element.val().substr(0, element.val().length - 1);
        var varraElement = element.val().substr(element.val().length - 1, 1).toUpperCase();
        var varra = 'TRWAGMYFPDXBNJZSQVHLCKET';

        numero = numero % 23;
        varra = varra.substring(numero, numero + 1);

        if (varra != varraElement) {
            if (message) message.show();
            aux_css = css_invalid;
        } else {
            if (message) message.hide();
        }
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION CIF
 * @param {*} element 
 * @param {*} message 
 */
function validation_cif(element, message) {
    var regex = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }
    if (!regex.test(element.val())) {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        var valueCif = element.val().substr(1, element.val().length - 2);
        var suma = 0;
        var suma2 = 0;

        for (i = 1; i < valueCif.length; i = i + 2) {
            suma = suma + parseInt(valueCif.substr(i, 1));
        }        

        for (i = 0; i < valueCif.length; i = i + 2) {
            result = parseInt(valueCif.substr(i, 1)) * 2;
            if (String(result).length == 1) {
                suma2 = suma2 + parseInt(result);
            } else {
                suma2 = suma2 + parseInt(String(result).substr(0, 1)) + parseInt(String(result).substr(1, 1));
            }
        }

        suma = suma + suma2;

        var unidad = String(suma).substr(1, 1)
        unidad = 10 - parseInt(unidad);

        var primerCaracter = element.val().substr(0, 1).toUpperCase();

        if (primerCaracter.match(/^[FJKNPQRSUVW]$/)) {
            if (String.fromCharCode(64 + unidad).toUpperCase() == element.val().substr(element.val().length - 1, 1).toUpperCase())
                if (message) message.hide();
        } else if (primerCaracter.match(/^[XYZ]$/)) {
            if (message) message.show();
            aux_css = css_invalid;
        } else if (primerCaracter.match(/^[ABCDEFGHLM]$/)) {
            if (unidad == 10)
                unidad = 0;
            if (element.val().substr(element.val().length - 1, 1) == String(unidad))
                if (message) message.hide();
        } else {
            if (message) message.show();
            aux_css = css_invalid;
        }
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION NIE
 * @param {*} element 
 * @param {*} message 
 */
function validation_nie(element, message) {
    var regex = /^[XYZ]\d{7,8}[A-Z]$/;
    var aux_css = css_valid;
    if (!element) return;

    if (element.val()==''){
        aux_css = css_valid;
        element.css('border', aux_css);
        if (message) message.hide();
        return;
    }
    if (!regex.test(element.val())) {
        if (message) message.show();
        aux_css = css_invalid;
    } else {
        var nie="";
        if(element.val().substr(0,1)=="X")
			nie=element.val().substr(1);
		else if(element.val().substr(0,1)=="Y")
			nie="1"+element.val().substr(1);
		else if(element.val().substr(0,1)=="Z")
			nie="2"+element.val().substr(1);

        var numero = nie.substr(0, element.val().length - 1);
        var varraElement = element.val().substr(element.val().length - 1, 1).toUpperCase();
        var varra = 'TRWAGMYFPDXBNJZSQVHLCKET';

        numero = numero % 23;
        varra = varra.substring(numero, numero + 1);

        if (varra != varraElement) {
            if (message) message.show();
            aux_css = css_invalid;
        } else {
            if (message) message.hide();
        }
    }

    element.css('border', aux_css);
}

/**
 * VALIDACION TELEFONO
 * @param {*} element 
 * @param {*} message 
 */
 function validation_telefono(element, message) {
    var regex = /^(\+34|0034|34)?[6|7|9][0-9]{8}$/;
    var aux_css = css_valid;
    if (!element) return false;

    if (element.val()==''){
        aux_css = css_valid;
        if (message) message.hide();
        return true;
    }
    if (!regex.test(element.val()) && element.val()!="") {
        if (message) message.show();
        aux_css = css_invalid;
        return false;
    } else {
        if (message) message.hide();
        return true;
    }
}

window.validation_telefono = validation_telefono;