

function search_js_links(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],        
        ['per_page',$('#pages-select option:selected'),'i'], 
        ['enter_text',$('#enter_text'),'s'],
        ['link_type_id',$('#search_tipo-select option:selected'),'s'],
        ['linkable_id',$('#linkable_id'),'s'],
        ['linkable_type',$('#linkable_type'),'s']
        ],'#recarga-filtros');
}

function search_js_links_clean(){
    search_clear([
        $('#enter_text'),
        $('#search_tipo-select')],
        '#recarga-filtros');
}

