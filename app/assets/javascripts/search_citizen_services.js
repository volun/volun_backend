
function search_js_citizen_service(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_id',$('#search_id'),'i'],
        ['search_nombre_citizen',$('#search_nombre_citizen'),'s'],
        ['search_apellido1_citizen',$('#search_apellido1_citizen'),'s'],
        ['search_apellido2_citizen',$('#search_apellido2_citizen'),'s'],
        ['search_nombre_volunteer',$('#search_nombre_volunteer'),'s'],
        ['search_apellido1_volunteer',$('#search_apellido1_volunteer'),'s'],
        ['search_apellido2_volunteer',$('#search_apellido2_volunteer'),'s'],
        ['search_services_start',$('#search_services_start'),'s'],
        ['search_services_end',$('#search_services_end'),'s'],       
        ['search_services_a_start',$('#search_services_a_start'),'s'],
        ['search_services_a_end',$('#search_services_a_end'),'s'],
        ['search_start_rate',$('#search_start_rate'),'i'],
        ['search_end_rate',$('#search_end_rate'),'i'],
        ['search_age_start',$('#search_age_start'),'i'],
        ['search_age_end',$('#search_age_end'),'i'],
        ['enter_text',$('#enter_text'),'s'],     
        ['search_service_type',$('#search_service_type-select'),'m'],
        ['search_service_status',$('#search_service_status-select'),'m'],
        ['search_other_option',$('#search_other_option-select'),'m'],
        ['search_gender',$('#search_gender-select'),'i'],
        ['search_district',$('#search_district-select'),'m']
        ],'#recarga-filtros');
}

function search_js_citizen_service_clean(){
    search_clear([
        $('#search_id'),
        $('#search_nombre_citizen'),
        $('#search_apellido1_citizen'),
        $('#search_apellido2_citizen'),
        $('#search_nombre_volunteer'),
        $('#search_apellido1_volunteer'),
        $('#search_apellido2_volunteer'),
        $('#search_services_start'),
        $('#search_services_end'),
        $('#search_services_a_start'),
        $('#search_services_a_end'),
        $('#search_start_rate'),
        $('#search_end_rate'),
        $('#enter_text'),
        $('#search_age_start'),
        $('#search_age_end'),
        $('#search_service_type'),
        $('#search_service_status'),
        $('#search_other_option-select'),
        $('#search_gender-select'),
        $('#search_district-select')],
        '#recarga-filtros');
}
