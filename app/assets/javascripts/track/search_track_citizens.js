

function search_js_track_citizens(){
    search_generic([
        ['search_enter_text',$('#search_enter_text'),'s'],
        
        ['search_tipo',$('#search_tipo-select option:selected'),'s'],
        ['search_manager',$('#search_manager'),'s']
        ],'#recarga-filtros');
}

function search_js_track_citizens_clean() {
    search_clear([
        $('#search_enter_text'),
        $('#search_tipo-select'),
        $('#search_manager'),
        $('#buttonSearch')],
        '#recarga-filtros');
}

