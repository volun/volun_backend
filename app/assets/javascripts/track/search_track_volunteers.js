

function search_js_track_volunteers(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        ['q[view_list_volunteer_id_eq]',$('#q_view_list_volunteer_id_eq'),'s'],
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_enter_text',$('#search_enter_text'),'s'],
        ['search_project',$('#search_project'),'s'],
        ['search_tipo',$('#search_tipo-select option:selected'),'s'],
        ['search_inicio_f',$('#search_inicio_f'),'s'],
        ['search_fin_f',$('#search_fin_f'),'s'],
        ['search_manager',$('#search_manager'),'s']
        ],'#recarga-filtros');
}

function search_js_track_volunteers_clean() {
    search_clear([
        $('#search_enter_text'),
        $('#search_tipo-select'),
        $('#search_project'),
        $('#search_inicio_f'),
        $('#search_fin_f'),
        $('#search_manager'),
        $('#warningLabelFechaFormat2'),
        $('#warningLabelFechaFormat'),
        $('#warningLabelFecha'),
        $('#buttonSearch')],
        '#recarga-filtros');
}

