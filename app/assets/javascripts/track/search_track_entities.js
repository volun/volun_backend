
function search_js_track_entities(){
    search_generic([
        ['search_enter_text',$('#search_enter_text'),'s'],
        ['q[entity_id_eq]',$('#q_entity_id_eq'),'m'],
        ['search_tipo',$('#search_tipo-select option:selected'),'s'],
        ['search_manager',$('#search_manager'),'s']
        ],'#recarga-filtros');
}

function search_js_track_entities_clean() {
    search_clear([
        $('#search_enter_text'),
        $('#search_tipo-select'),
        $('#search_manager'),
        $('#buttonSearch')],
        '#recarga-filtros');
}

