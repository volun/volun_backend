

function search_js_track_projects(){
    search_generic([
        ['search_enter_text',$('#search_enter_text'),'s'],
        ['q[project_id_eq]',$('#q_project_id_eq'),'m'],
        ['search_inicio_f',$('#search_inicio_f'),'s'],
        ['search_fin_f',$('#search_fin_f'),'s'],
        ['search_manager',$('#search_manager'),'s']
        ],'#recarga-filtros');
}
  
function search_js_track_projects_clean() {
    search_clear([
        $('#search_enter_text'),
        $('#search_inicio_f'),
        $('#search_fin_f'),
        $('#search_manager'),
        $('#warningLabelFechaFormat2'),
        $('#warningLabelFechaFormat'),
        $('#warningLabelFecha'),        
        $('#buttonSearchTrackProject')],
        '#recarga-filtros');
}

