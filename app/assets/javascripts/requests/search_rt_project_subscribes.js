
function search_js_rt_project_subscribed() {
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_district',$('#search_district-select option:selected'),'s'],
        ['search_project',$('#search_project'),'s'],
        ['search_start_date',$('#search_start_date'),'s'],
        ['search_end_date',$('#search_end_date'),'s'],
        ['search_pending',$('#search_pending'),'check'],
        ['search_processing',$('#search_processing'),'check'],
        ['search_approved',$('#search_approved'),'check'],
        ['search_rejected',$('#search_rejected'),'check'],
        ],'#recarga-filtros');
}

function search_js_rt_project_subscribes_clean() {
    search_clear([$(),
        $('#enter_text'),
        $('#search_district-select'),
        $('#search_project'),
        $('#search_start_date'),
        $('#search_end_date'),
        $('#search_pending'),
        $('#search_processing'),
        $('#search_approved'),
        $('#search_rejected'),
        $('#buttonSearch'),
        $('#warningLabelFecha'),
        $('#warningLabelFechaFormat'),
        $('#warningLabelFechaFormat2')],'#recarga-filtros');
}

