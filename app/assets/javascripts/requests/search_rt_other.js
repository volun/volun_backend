

function search_js_rt_other(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],
        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_start_date',$('#search_start_date'),'s'],
        ['search_end_date',$('#search_end_date'),'s'],
        ['enter_text',$('#enter_text'),'s'],
        ['search_pending',$('#search_pending'),'check'],
        ['search_processing',$('#search_processing'),'check'],
        ['search_notes',$('#search_notes'),'s'],
        ['search_description',$('#search_description'),'s'],
        ['search_approved',$('#search_approved'),'check'],
        ['search_rejected',$('#search_rejected'),'check'],
        ['search_entity',$('#search_entity-select option:selected'),'s']
        ],'#recarga-filtros');
}

function search_js_rt_other_clean(){
    search_clear([
        $('#search_start_date'),
        $('#search_end_date'),
        $('#enter_text'),
        $('#search_pending'),
        $('#search_processing'),
        $('#search_notes'),
        $('#search_description'),
        $('#search_approved'),
        $('#search_rejected'),
        $('#search_entity-select')],
        '#recarga-filtros');
}

