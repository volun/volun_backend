
function search_js_citizen(){
    search_generic([
        ['isSearch',true,'b'],
        ['page',1,'b'],        
        ['per_page',$('#pages-select option:selected'),'i'],
        ['search_id',$('#search_id'),'i'],
        ['search_nombre',$('#search_nombre'),'s'],
        ['search_apellido1',$('#search_apellido1'),'s'],
        ['search_apellido2',$('#search_apellido2'),'s'],
        ['search_services_start',$('#search_services_start'),'i'],
        ['search_services_end',$('#search_services_end'),'i'],
        ['search_services_f_start',$('#search_services_f_start'),'i'],
        ['search_services_f_end',$('#search_services_f_end'),'i'],
        ['search_services_a_start',$('#search_services_a_start'),'i'],
        ['search_services_a_end',$('#search_services_a_end'),'i'],
        ['search_services_p_start',$('#search_services_p_start'),'i'],
        ['search_services_p_end',$('#search_services_p_end'),'i'],
        ['search_age_start',$('#search_age_start'),'i'],
        ['search_age_end',$('#search_age_end'),'i'],
        ['enter_text',$('#enter_text'),'s'],        
        ['search_service_type',$('#search_service_type-select'),'m'],
        ['search_other_option',$('#search_other_option-select'),'m'],
        ['search_district',$('#search_district-select'),'m'],
        ['search_gender',$('#search_gender-select'),'i'],
        ],'#recarga-filtros');
}


function search_js_citizen_clean(){
    search_clear([
        $('#search_id'),
        $('#search_nombre'),
        $('#search_apellido1'),
        $('#search_apellido2'),
        $('#search_services_start'),
        $('#search_services_end'),
        $('#search_services_f_start'),
        $('#search_services_f_end'),
        $('#search_services_a_start'),
        $('#search_services_a_end'),
        $('#search_services_p_start'),
        $('#search_services_p_end'),
        $('#search_age_start'),
        $('#search_age_end'),
        $('#enter_text'),
        $('#search_service_type-select'),
        $('#search_other_option-select'),
        $('#search_gender-select'),
        $('#search_district-select')],
        '#recarga-filtros');
}
