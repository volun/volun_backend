class UnsubscribeVolunteer < Prawn::Document
  def initialize(volunteer, manager)

    super(top_margin: 70)
    @volunteer=volunteer
    @manager=Manager.find(manager.loggable_id)

    unless @volunteer.subscribe_date.blank?
      @subscribe_date=@volunteer.subscribe_date.strftime("%d/%m/%Y")
    else
      @subscribe_date="--/--/----"
    end
   
    unless @volunteer.unsubscribe_date.blank?
      @unsubscribe_date=@volunteer.unsubscribe_date.strftime("%d/%m/%Y")
    else
      @unsubscribe_date="--/--/----"
    end

    @current_date= Time.now.strftime("%d/%m/%Y")
    @current_date_complete= I18n.l Time.now, format: :format_date_complete
    
    unless @volunteer.unsubscribe_reason.blank?
      @reason  = @volunteer.unsubscribe_reason
    else
      @reason  = ""
    end
    
    header
    manager_info
  end
  
  def header
    #This inserts an image in the pdf file and sets the size of the image
    image "#{Rails.root}/app/assets/images/Voluntarios x Madrid.jpg",:at =>[35, 700],:width => 200
  end

  def manager_info
    formatted_text_box([:text => I18n.t('pdf.message_title', name: "#{@manager} #{@manager.last_name_alt}"),:size => 15,:align => :justify],:at =>[45, 550],:width => 460) 
    formatted_text_box([:text => I18n.t('pdf.message_body',name: "#{@volunteer.name} #{@volunteer.last_name}", id: @volunteer.id,subscribe_date: @subscribe_date,unsubscribe_date: @unsubscribe_date,reason: @reason),:size => 14],:align => :justify,:at =>[45, 400],:width => 460) 
    draw_text "Madrid, #{@current_date_complete} ",size: 14, :at => [350, 200] #[ancho, alto]
    draw_text "Fdo: ",size: 14, :at => [300, 150] #[ancho, alto]
  end
end
