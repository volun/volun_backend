class MassiveEmailsJob < ActiveJob::Base
  queue_as :default

  def perform(user,ids,title, content, upload, filters)
    count_total = ids.count
    count_volun = 0
    user_per = User.find_by(id: user)
    massive_historic = MassiveEmailHistoric.new(user_id: user, volun_ids: ids.join(';'),title: title, content: content, emails: "", filters: filters)
    massive_historic.log = I18n.t('massive_email.log_data',
      title: title,
      body: content,
      attach: upload.blank? ? "" : upload.map {|x| x[:original_filename]},
      filter: filters,
      emails: massive_historic.emails,
      total: "#{count_volun}/#{count_total}")
    massive_historic.save

    if !Rails.env.production?
      ["perezamv@madrid.es","sanchezcvic.ext@madrid.es","decastrocm.ext@madrid.es",
        "ajmorenoh@minsait.com","rybarczykpm@madrid.es","arodilla@fundacionasispa.org","abejarano@fundacionasispa.org"].each do |email|
        begin
          VolunteerMailer.send_massive_email(email, title: title, content: content, upload: upload).deliver_now
         
          count_volun = count_volun + 1
          massive_historic.emails = massive_historic.emails + "- Correo electrónico: #{email} se le ha enviado correctamente<br>"
        rescue => e
          massive_historic.emails = massive_historic.emails + "- ERROR: No se ha podido enviar el mensaje al correo #{email} -> [#{e.message}] #{e.backtrace.join("\n")}<br>"
        end

        massive_historic.log = I18n.t('massive_email.log_data',
          title: title,
          body: content,
          attach: upload.blank? ? "" : upload.map {|x| x[:original_filename]},
          filter: filters,
          emails: massive_historic.emails,
          total: "#{count_volun}/#{count_total}")
        massive_historic.save
      end
    else
      ids.each do |i|
        volunteer = Volunteer.find_by(id: i)
        begin
          VolunteerMailer.send_massive_email(volunteer.email, title: title, content: content, upload: upload).deliver_now
          begin
            volunteer_manager = VolunteerManager.new(volunteer: volunteer, manager_id: user_per.loggable_id)
            volunteer_manager.register_send_email(title: title)
          rescue
          end
          count_volun = count_volun + 1
          massive_historic.emails = massive_historic.emails + "- Correo electrónico: #{volunteer.email} se le ha enviado correctamente<br>"
        rescue
          massive_historic.emails = massive_historic.emails + "- ERROR: No se ha podido enviar el mensaje al correo #{volunteer.email}<br>"
        end

        massive_historic.log = I18n.t('massive_email.log_data',
          title: title,
          body: content,
          attach: upload.blank? ? "" : upload.map {|x| x[:original_filename]},
          filter: filters,
          emails: massive_historic.emails,
          total: "#{count_volun}/#{count_total}")
        massive_historic.save
      end
    end
  end
end
