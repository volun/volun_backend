class MassiveSmsJob < ActiveJob::Base
  queue_as :default

  def perform(user,ids,content, type)
    count_total = ids.count
    count_record = 0
    user_per = User.find_by(id: user)
    massive_historic = MassiveSmsHistoric.new(user_id: user, records_ids: ids.join(';'),content: content, sms: "", specific: type)

    if !Rails.env.production? 
      count_total = 5
      ["618844036","679966434", "605054436", "608249626","676431050"].each do |phone|
        begin
          SMSApi.new.sms_deliver(phone, content)     
          count_record = count_record + 1
          massive_historic.sms = massive_historic.sms + "- Teléfono móvil: #{phone} se le ha enviado correctamente<br>"         
        rescue
          massive_historic.sms = massive_historic.sms + "- ERROR: No se ha podido enviar el mensaje al teléfono #{phone}<br>"
        end
      end
    else
      ids.each do |i|
        if type=="citizen"
          record = Citizen.find_by(id: i)
        elsif type=="volunteer"
          record = Volunteer.find_by(id: i)
        end
        begin
          if !record.phone.to_s.blank?
            SMSApi.new.sms_deliver(record.phone, content)
            AuditGenerateHelper.audit_send_sms(record, user_per)        
            count_record = count_record + 1
            massive_historic.sms = massive_historic.sms + "- Teléfono móvil: #{record.phone} se le ha enviado correctamente<br>"
          else
            massive_historic.sms = massive_historic.sms + "- ERROR: No se ha podido enviar el mensaje ya que el teléfono está en blanco<br>"
          end
        rescue
          massive_historic.sms = massive_historic.sms + "- ERROR: No se ha podido enviar el mensaje al teléfono #{record.phone}<br>"
        end
      end
    end
   
    massive_historic.log = I18n.t('massive_sms.log_data',      
        body: content,
        sms: massive_historic.sms,
        total: "#{count_record}/#{count_total}")
    massive_historic.save(validate: false)
  end
end
