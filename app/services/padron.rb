class Padron
    attr_accessor  :citizen,:volunteer

    def initialize(citizen=nil, volunteer=nil)
        self.citizen = citizen
        self.volunteer = volunteer
    end

    def search_citizien
        response = client.call(:get_habita_datos, xml: habita_datos_msg)
        response_request = Hash.deep_strip!(response.body[:get_habita_datos_response][:get_habita_datos_return] || {})
        response_request
    rescue => e
        begin
            Rails.logger.error("ERROR: #{e}")
        rescue
        end
        {}
    end


    def get_baja 
        response = client.call(:get_habita_datos_baja, xml: habita_datos_baja_msg)
        response_request = Hash.deep_strip!(response.body[:get_habita_datos_baja_response][:get_habita_datos_baja_return] || {})
        response_request
    rescue => e
        begin
            Rails.logger.error("ERROR: #{e}")
        rescue
        end
        {}
    end
    

    private

        def client
            @client ||= Savon.client(wsdl: Rails.application.secrets.padron_wsdl, raise_errors: true)
        end

        def habita_datos_msg
            return  {} if self.citizen.blank? && self.volunteer.blank?
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:impl=\"http://impl.webservices.epob.tao.es\" xmlns:sch=\"http://schemas.webservices.epob.tao.es\">
                <soapenv:Header/>
                <soapenv:Body>
                <impl:getHabitaDatos>
                    <impl:request>
                        <sch:codigoIdioma>#{Rails.application.secrets.code_language_padron_wsdl}</sch:codigoIdioma>
                        <sch:codigoInstitucion>#{Rails.application.secrets.institution_padron_wsdl}</sch:codigoInstitucion>
                        <sch:codigoPortal>#{Rails.application.secrets.portal_code_padron_wsdl}</sch:codigoPortal>
                        <sch:codigoUsuario>#{Rails.application.secrets.user_padron_wsdl}</sch:codigoUsuario>
                        <sch:documento>#{self.citizen.blank? ? self.volunteer.id_number : self.citizen.document }</sch:documento>
                        <sch:tipoDocumento>#{self.citizen.blank? ? self.volunteer.document_type.code : self.citizen.document_type_ws}</sch:tipoDocumento>
                        <sch:nivel>3</sch:nivel>
                    </impl:request>
                </impl:getHabitaDatos>
                </soapenv:Body>
            </soapenv:Envelope>"
        end

        def habita_datos_baja_msg
            return  {} if self.citizen.blank? && self.volunteer.blank?
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:impl=\"http://impl.webservices.epob.tao.es\" xmlns:sch=\"http://schemas.webservices.epob.tao.es\">
                <soapenv:Header/>
                <soapenv:Body>
                <impl:getHabitaDatosBaja>
                    <impl:request>
                        <sch:codigoIdioma>#{Rails.application.secrets.code_language_padron_wsdl}</sch:codigoIdioma>
                        <sch:codigoInstitucion>#{Rails.application.secrets.institution_padron_wsdl}</sch:codigoInstitucion>
                        <sch:codigoPortal>#{Rails.application.secrets.portal_code_padron_wsdl}</sch:codigoPortal>
                        <sch:codigoUsuario>#{Rails.application.secrets.user_padron_wsdl}</sch:codigoUsuario>
                        <sch:documento>#{self.citizen.blank? ? self.volunteer.id_number : self.citizen.document }</sch:documento>
                        <sch:tipoDocumento>#{self.citizen.blank? ? self.volunteer.document_type.code : self.citizen.document_type_ws}</sch:tipoDocumento>
                        <sch:nivel>3</sch:nivel>
                    </impl:request>
                </impl:getHabitaDatosBaja>
                </soapenv:Body>
            </soapenv:Envelope>"
        end
        
        #DATOS PARA PRUEBAS
        # documento -> 5082111
        # tipo documento -> 1
end