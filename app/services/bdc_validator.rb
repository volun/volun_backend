class BdcValidator

  attr_accessor :bdc_fields, :response_data

  def initialize(bdc_fields = {})
    self.bdc_fields    = bdc_fields.with_indifferent_access
    self.response_data = {}
  end

  def validate_address(bdc_fields = self.bdc_fields)
    uri = URI("#{Rails.application.secrets.bdc_url}/getValidarDireccion") 
    uri.query = URI.encode_www_form(bdc_fields)
    res = Net::HTTP.get_response(uri)
    response_json = JSON.parse(res.body)
    self.response_data = response_json
  rescue  Exception  => e
    Rails.logger.error('BdcValidator#validate_address') do
    end
    {}
  end

  def address_normalized?
    bdc_fields_msg = {
      nomPais:       bdc_fields[:country],
      nomProvincia:  bdc_fields[:province],
      nomPueblo:     bdc_fields[:town],
      nomClase:      bdc_fields[:road_type],
      nomVial:       bdc_fields[:road_name],
      nomApp:        bdc_fields[:road_number_type],
      numApp:        bdc_fields[:road_number],
      calApp:        bdc_fields[:grader].present? ? bdc_fields[:grader] : '  ',
      escalera:       bdc_fields[:stairs],
      planta:         bdc_fields[:floor],
      puerta:         bdc_fields[:door],
      intercambioBDC: '',
      aplicacion:     Rails.application.secrets.bdc_app_name
    }

    validate_address(bdc_fields_msg)
    ndp_code.present?
  end

  def town_block
    response_data["poblaciones"] || {}
  end

  def road_block
    response_data["viales"] || {}
  end

  def number_block
    response_data["numeros"] || {}
  end

  def province_block
    response_data["provincias"] || {}
  end

  def country_block
    response_data["provincias"] || {}
  end

  def road_type
    nomClase = !road_block.blank? ? road_block["nomClase"].strip : response_data["nomClase"].strip
    exist = RoadType.where(name: nomClase).first
    RoadType.create(name: nomClase) if exist.blank? && road_block["nomClase"].present?
    nomClase
  rescue
    response_data["nomClase"].to_s.strip
  end

  def ndp_code
    if !response_data["codDireccion"].blank? && response_data["codDireccion"] != 0
      response_data["codDireccion"]
    else
      nil
    end
  end

  def province_code
    return !province_block.blank? ? province_block["codProvincia"] : response_data["codProvincia"]
  end

  def town_code
    return !town_block.blank? ? province_block["codPoblacion"] : response_data["codPueblo"]
  end

  def country
    return  response_data["nomPais"].blank? ? "" : response_data["nomPais"].strip 
  end

  def country_code
    return  response_data["codPais"].blank? ? "" : response_data["codPais"].to_s.strip 
  end

  def borough_code
    return !number_block.blank? ? number_block["codBar"] : response_data["codBarrio"]
  end

  def district_code
    return !number_block.blank? ? province_block["codDis"] : response_data["codDistrito"]
  end

  def local_code
    response_data["codLocal"]
  end

  def latitude
    return !number_block.blank? ? province_block["coordX"] : response_data["coordX"]
  end

  def longitude
    return !number_block.blank? ? province_block["coordY"] : response_data["coordY"]
  end

  def postal_code
    return !number_block.blank? ? province_block["codPostal"] : response_data["codPostal"]
  end

  def province
    return !province_block.blank? ? province_block["nomProvincia"].strip : response_data["nomProvincia"].strip
  end

  def town
    return !town_block.blank? ? province_block["nomPoblacion"].strip : response_data["nomPueblo"].strip
  end

  def district
    return !number_block.blank? ? number_block["distrito"] : response_data["distrito"] 
  end

  def borough
    return !number_block.blank? ? number_block["barrio"] : response_data["barrio"] 
  end

  def valid_bdc_fields?
    bdc_fields[:country].present? && bdc_fields[:province].present?
  end

  def search_towns
    return [] unless valid_bdc_fields? && bdc_fields[:town].present?
    validate_address(bdc_fields_msg)
    town_block.map { |town| town["nomPoblacion"] }
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    []
  end

  def search_roads
    return unless valid_bdc_fields?
    validate_address(bdc_fields_msg)
    _road_names = [road_block].flatten
    if bdc_fields[:nomClase].present?    
      _road_names.select! { |road| road["nomClase"] == bdc_fields[:road_type] }
    end
    _road_names.sort_by { |road| road["nomVial"] }
  end

  def search_road_numbers
    return unless valid_bdc_fields?
    validate_address(bdc_fields_msg.merge(nomClase: bdc_fields["road_type"] || ''))
    number_block
  end


  private

    def bdc_fields_msg
      {
        nomPais:       bdc_fields[:country],
        nomProvincia:  bdc_fields[:province],
        nomPueblo:     bdc_fields[:town],
        nomClase:      '',
        nomVial:       bdc_fields[:road_name].present? ? bdc_fields[:road_name] : 'X',
        nomApp:        '',
        numApp:        '0',
        calApp:        '',
        escalera:       '',
        planta:         '',
        puerta:         '',
        intercambioBDC: '',
        aplicacion:     Rails.application.secrets.bdc_app_name
      }
    end
end 
