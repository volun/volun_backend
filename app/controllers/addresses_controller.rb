class AddressesController < ApplicationController
    load_and_authorize_resource
    respond_to :html, :js, :json
    include AddressManager
  
    def index
      params[:q] ||= Address.ransack_default
      @search_q = @addresses.search(params[:q])
      @addresses = @search_q.result.paginate(page: params[:page], per_page: params[:per_page]||15)
  
      respond_with(@addresses)
    end
  
    def show
      respond_with(@address) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    end
  
    def new
      @address = Address.new
      respond_with(@address)
    end
  
    def edit
    end
  
    def create
      AuditGenerateHelper.audit_create(@address, current_user)
      respond_with(@address)
    end
  
    def update
      AuditGenerateHelper.audit_update(@address, address_params,current_user)
      respond_with(@address)
    end
  
    def destroy
      AuditGenerateHelper.audit_destroy(@address,current_user)
      respond_with(@address)
    end
    
    def bdc_search_towns
      @address = Address.new(address_params.except(:province))
      @address.province = Province.find_by(id: address_params[:province])
      respond_with(@address) do |format|
        format.json { render json: { towns: @address.bdc_validator.search_towns } }
      end
    end
  
    def bdc_search_roads
      @address = Address.new(address_params.except(:province))
      @address.province = Province.find_by(id: address_params[:province])
      respond_with(@address) do |format|
        format.json {
          render json: { roads: @address.bdc_validator.search_roads }
        }
      end
    end
  
    def bdc_search_road_numbers
      @address = Address.new(address_params.except(:province))
      @address.province = Province.find_by(id: address_params[:province])
      respond_with(@address) do |format|
        format.json {
          render json: { numbers: @address.bdc_validator.search_road_numbers }
        }
      end
    end

    def bdc_get_district_borought
      district      = District.find_by("cast(code as int)=?", params[:dis_code].to_i)
      borought       = Borought.find_by("cast(code as int) = ? and district_id=?",params[:bar_code].to_i, district.try(:id))
   
      respond_with do |format|
        format.json {
          render json: { district_name: district.try(:name).to_s, borought_name: borought.try(:name).to_s }
        }
      end
    end
  
    protected
    def address_params
      params
        .require(:address)
        .permit(address_attributes)
    end
  end