class Pt::VolunteersController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_volunteer

  def show
    respond_with(@pt_volunteer) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_volunteer = Pt::Volunteer.new
    respond_with(@pt_volunteer)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_volunteer = Pt::Volunteer.new(pt_volunteer_params)
    if AuditGenerateHelper.audit_create(@pt_volunteer, current_user)
      set_pt_order(@pt_volunteer.project.father, @pt_volunteer.project,current_user)
      track_custom(current_user.loggable_id, @pt_volunteer.project.id, 'subscribe')
    end
    respond_with(@pt_volunteer, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    old = @pt_volunteer.project.suspended
    old_father = @pt_volunteer.project.father
    AuditGenerateHelper.audit_update(@pt_volunteer, pt_volunteer_params, current_user)
    set_pt_order(@pt_volunteer.project.father, @pt_volunteer.project,current_user, old_father) 
    set_suspended_pt(old,@pt_volunteer.project)    
    respond_with(@pt_volunteer, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_volunteer.active
      AuditGenerateHelper.audit_destroy(@pt_volunteer, current_user)
      track_custom(current_user.loggable_id, @pt_volunteer.project.id, 'unsubscribe')
    end
    respond_with(@pt_volunteer)
  rescue => e
    log_error("COD-00006",e) 
  end

  protected

    def pt_volunteer_params
      params.require(:pt_volunteer).permit(
          :notes,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_volunteer_params
    
  private

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_volunteer.try(:project).try(:entity).try(:vat_number).to_s)      
    end
end
