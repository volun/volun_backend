class Pt::OthersController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_other
  before_action :set_class_style, only: [:new,:create] 

  def show
    respond_with(@pt_other) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_other = Pt::Other.new
    respond_with(@pt_other)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_other = Pt::Other.new(pt_other_params)
    if AuditGenerateHelper.audit_create(@pt_other, current_user)
      set_pt_order(@pt_other.project.father, @pt_other.project,current_user)
      track_custom(current_user.loggable_id, @pt_other.project.id, 'subscribe')

      unless @publish.blank?
        if AuditGenerateHelper.audit_update(@publish, {:project_id => @pt_other.id}, current_user)
          @publish.request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id, user_id: (@publish.request_form.user.id unless @publish.request_form.user.blank?))
          send_rt_project_publish(@publish.request_form.user.loggable) unless @publish.request_form.user.blank?
        end
      end
    end
    respond_with(@pt_other, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    old = @pt_other.project.suspended
    old_father = @pt_other.project.father
    AuditGenerateHelper.audit_update(@pt_other, pt_other_params, current_user)   
    set_pt_order(@pt_other.project.father, @pt_other.project,current_user,old_father)
    set_suspended_pt(old,@pt_other.project) 
    respond_with(@pt_other, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_other.active
      AuditGenerateHelper.audit_destroy(@pt_other, current_user)
      track_custom(current_user.loggable_id, @pt_other.project.id, 'unsubscribe')
    end
    respond_with(@pt_other)
  rescue => e
    log_error("COD-00006",e) 
  end
  
  protected

    def pt_other_params
      params.require(:pt_other).permit(
          :notes,
          project_attributes: project_attributes
        )
    end    

    alias_method :create_params, :pt_other_params

  private

    def send_rt_project_publish(entity)
      message=I18n.t('mailer.rt_others.message',notes: @publish.notes)
      subject=I18n.t('mailer.rt_others.subject')
      if entity.email == nil || entity.email == ''
        if entity.phone_number_alt == nil || entity.phone_number_alt == ''
          #No envía mensaje. No tiene ni dirección de correo electrónico ni móvil
        else
          begin #Si no tiene email, se envía sms
            movil = entity.mobile_number
            SMSApi.new.sms_deliver(movil, message)
          rescue Exception  => e
            Rails.logger.error('sendSms#project_publish') do
              "#{I18n.t('sms.error')}: \n#{e}"
            end
          end
        end
      else
        begin
          VolunteerMailer.send_email(entity.email, message: message, subject: subject).deliver_now
        rescue Exception  => e
          Rails.logger.error('sendEmail#project_publish') do
            "#{I18n.t('mailer.error')}: \n#{e}"
          end
        end
      end
    end 

    def set_class_style 
      @clase_estilo = @clase_estilo_other = ""

      unless params[:rt_project_publishing_id].blank? 
        @publish=Rt::ProjectPublishing.find(params[:rt_project_publishing_id])  
        @clase_estilo = @clase_estilo_other ="border: 1px solid green" 
      end 
    end 

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_other.try(:project).try(:entity).try(:vat_number).to_s)      
    end
end
