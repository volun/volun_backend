class Pt::MunicipalsController < ApplicationController
  include PtController  

  load_and_authorize_resource instance_name: :pt_municipal  

  def show
    respond_with(@pt_municipal) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @pt_municipal = Pt::Municipal.new
    respond_with(@pt_municipal)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @pt_municipal = Pt::Municipal.new(pt_municipal_params)
    if AuditGenerateHelper.audit_create(@pt_municipal, current_user)
      set_pt_order(@pt_municipal.project.father, @pt_municipal.project,current_user)
      track_custom(current_user.loggable_id, @pt_municipal.project.id, 'subscribe')      
    end
    respond_with(@pt_municipal, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    old = @pt_municipal.project.suspended
    old_father = @pt_municipal.project.father
    AuditGenerateHelper.audit_update(@pt_municipal, pt_municipal_params, current_user)
    set_pt_order(@pt_municipal.project.father, @pt_municipal.project, current_user,old_father)
    set_suspended_pt(old,@pt_municipal.project)
    respond_with(@pt_municipal, location: projects_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    if @pt_municipal.active
      AuditGenerateHelper.audit_destroy(@pt_municipal, current_user)
      track_custom(current_user.loggable_id, @pt_municipal.project.id, 'unsubscribe')
    end
    respond_with(@pt_municipal)
  rescue => e
    log_error("COD-00007",e) 
  end

  protected

    def pt_municipal_params
      params.require(:pt_municipal).permit(
          :notes,
          project_attributes: project_attributes
        )
    end
    
    alias_method :create_params, :pt_municipal_params

  private
    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_municipal.try(:project).try(:entity).try(:vat_number).to_s)      
    end
end
