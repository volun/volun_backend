class Pt::SocialsController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_social

  def show
    respond_with(@pt_social) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_social = Pt::Social.new
    respond_with(@pt_social)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_social = Pt::Social.new(pt_social_params)
    if AuditGenerateHelper.audit_create(@pt_social, current_user)
      set_pt_order(@pt_social.project.father, @pt_social.project, current_user)
      track_custom(current_user.loggable_id, @pt_social.project.id, 'subscribe')
    end
    respond_with(@pt_social, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update  
    old = @pt_social.project.suspended
    old_father = @pt_social.project.father
    AuditGenerateHelper.audit_update(@pt_social, pt_social_params, current_user)   
    set_pt_order(@pt_social.project.father, @pt_social.project,current_user, old_father)
    set_suspended_pt(old,@pt_social.project)   
    respond_with(@pt_social, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_social.active
      AuditGenerateHelper.audit_destroy(@pt_social, current_user)
      track_custom(current_user.loggable_id, @pt_social.project.id, 'unsubscribe')
    end
    respond_with(@pt_social)
  rescue => e
    log_error("COD-00006",e) 
  end

  protected

    def pt_social_params
      params.require(:pt_social).permit(
          :notes,
          :is_project_social,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_social_params

  private

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_social.try(:project).try(:entity).try(:vat_number).to_s)      
    end
end
