class Pt::SubventionsController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_subvention

  def show
    respond_with(@pt_subvention) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_subvention = Pt::Subvention.new
    respond_with(@pt_subvention)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_subvention = Pt::Subvention.new(pt_subvention_params)
    if AuditGenerateHelper.audit_create(@pt_subvention, current_user)
      set_pt_order(@pt_subvention.project.father, @pt_subvention.project, current_user)
      track_custom(current_user.loggable_id, @pt_subvention.project.id, 'subscribe')
    end
    respond_with(@pt_subvention, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    old = @pt_subvention.project.suspended
    old_father = @pt_subvention.project.father
    AuditGenerateHelper.audit_update(@pt_subvention, pt_subvention_params, current_user)  
    set_pt_order(@pt_subvention.project.father, @pt_subvention.project, current_user, old_father)
    set_suspended_pt(old,@pt_subvention.project)  
    respond_with(@pt_subvention, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_subvention.active
      AuditGenerateHelper.audit_destroy(@pt_subvention, current_user)
      track_custom(current_user.loggable_id, @pt_subvention.project.id, 'unsubscribe')
    end
    respond_with(@pt_subvention)
  rescue => e
    log_error("COD-00006",e) 
  end

  protected

    def pt_subvention_params
      params.require(:pt_subvention).permit(
          :representative_name,
          :representative_last_name,
          :representative_last_name_alt,
          :id_num,
          :vat_number,
          :cost,
          :requested_amount,
          :subsidized_amount,
          :initial_volunteers_num,
          :participants_num,
          :entity_registry,
          :suspended,
          :has_quality_evaluation,
          :proposal_id,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_subvention_params

  private

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_subvention.try(:project).try(:entity).try(:vat_number).to_s)
    end
end
