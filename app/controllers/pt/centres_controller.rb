class Pt::CentresController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_centre
  
  def show
    respond_with(@pt_centre) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_centre = Pt::Centre.new
    respond_with(@pt_centre)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_centre = Pt::Centre.new(pt_centre_params)
    if AuditGenerateHelper.audit_create(@pt_centre, current_user)      
      set_pt_order(@pt_centre.project.father, @pt_centre.project, current_user)
      track_custom(current_user.loggable_id, @pt_centre.project.id, 'subscribe')
    end
    respond_with(@pt_centre, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    old = @pt_centre.project.suspended
    old_father = @pt_centre.project.father
    AuditGenerateHelper.audit_update(@pt_centre, pt_centre_params, current_user)
    set_pt_order(@pt_centre.project.father, @pt_centre.project,current_user, old_father)
    set_suspended_pt(old,@pt_centre.project)    
    respond_with(@pt_centre, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_centre.active
      AuditGenerateHelper.audit_destroy(@pt_centre, current_user)
      track_custom(current_user.loggable_id, @pt_centre.project.id, 'unsubscribe')
    end
    respond_with(@pt_centre)
  rescue => e
    log_error("COD-00006",e) 
  end

  protected
    
    def pt_centre_params
      params.require(:pt_centre).permit(
          :notes,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_centre_params

  private

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_centre.try(:project).try(:entity).try(:vat_number).to_s)      
    end
end
