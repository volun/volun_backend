class Pt::RetiredVolunteersController < ApplicationController
  include PtController

  load_and_authorize_resource instance_name: :pt_retired_volunteer
  before_action :set_class_style, only: [:new,:create] 

  def show
    respond_with(@pt_retired_volunteer) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @pt_retired_volunteer = Pt::RetiredVolunteer.new
    respond_with(@pt_retired_volunteer)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_retired_volunteer = Pt::RetiredVolunteer.new(pt_retired_volunteer_params)
    if AuditGenerateHelper.audit_create(@pt_retired_volunteer, current_user)
      set_pt_order(@pt_retired_volunteer.project.father, @pt_retired_volunteer.project,current_user)
      track_custom(current_user.loggable_id, @pt_retired_volunteer.project.id, 'subscribe')

      unless @publish.blank?
        if AuditGenerateHelper.audit_update(@publish, {:project_id => @pt_retired_volunteer.id}, current_user)
          @publish.request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id, user_id: (@publish.request_form.user.id unless @publish.request_form.user.blank?))
          send_rt_project_publish(@publish.request_form.user.loggable) unless @publish.request_form.user.blank?
        end
      end
    end
    respond_with(@pt_retired_volunteer, location: projects_path)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    old = @pt_retired_volunteer.project.suspended
    old_father = @pt_retired_volunteer.project.father
    AuditGenerateHelper.audit_update(@pt_retired_volunteer, pt_retired_volunteer_params, current_user)
    set_pt_order(@pt_retired_volunteer.project.father, @pt_retired_volunteer.project, current_user,old_father)
    set_suspended_pt(old,@pt_retired_volunteer.project)      
    respond_with(@pt_retired_volunteer, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    if @pt_retired_volunteer.active
      AuditGenerateHelper.audit_destroy(@pt_retired_volunteer, current_user)
      track_custom(current_user.loggable_id, @pt_retired_volunteer.project.id, 'unsubscribe')
    end
    respond_with(@pt_retired_volunteer)
  rescue => e
    log_error("COD-00006",e) 
  end

  protected

    def pt_retired_volunteer_params
      params.require(:pt_retired_volunteer).permit(
          :notes,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_retired_volunteer_params
    
  private
    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_retired_volunteer.try(:project).try(:entity).try(:vat_number).to_s) 
    end

    def send_rt_project_publish(entity)
      message=I18n.t('mailer.pt_retired_volunteers.message',notes: @publish.notes)
      subject=I18n.t('mailer.pt_retired_volunteers.subject')
      if entity.email == nil || entity.email == ''
        if entity.phone_number_alt == nil || entity.phone_number_alt == ''
          #No envía mensaje. No tiene ni dirección de correo electrónico ni móvil
        else
          begin #Si no tiene email, se envía sms
            movil = entity.mobile_number
            SMSApi.new.sms_deliver(movil, message)
          rescue Exception  => e
            Rails.logger.error('sendSms#project_publish') do
              "#{I18n.t('sms.error')}: \n#{e}"
            end
          end
        end
      else
        begin
          VolunteerMailer.send_email(entity.email, message: message, subject: subject).deliver_now
        rescue Exception  => e
          Rails.logger.error('sendEmail#project_publish') do
            "#{I18n.t('mailer.error')}: \n#{e}"
          end
        end
      end
    end

    def set_class_style 
      @clase_estilo = @clase_estilo_other = ""

      unless params[:rt_project_publishing_id].blank? 
        @publish=Rt::ProjectPublishing.find(params[:rt_project_publishing_id])  
        @clase_estilo = @clase_estilo_other ="border: 1px solid green" 
      end 
    end 
end
