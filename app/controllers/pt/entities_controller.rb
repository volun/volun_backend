class Pt::EntitiesController < ApplicationController
  include PtController
  
  load_and_authorize_resource instance_name: :pt_entity
  before_action :set_num_addres, only: [:new,:create]

  def show
    respond_with(@pt_entity) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new   
    @pt_entity = Pt::Entity.new
    respond_with(@pt_entity)
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @pt_entity = Pt::Entity.new(pt_entity_params)
    if AuditGenerateHelper.audit_create(@pt_entity, current_user)
      set_pt_order(@pt_entity.project.father, @pt_entity.project, current_user)
      track_custom(current_user.loggable_id, @pt_entity.project.id, 'subscribe')
    end
    respond_with(@pt_entity, location: projects_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    # Modificación para los casos de revisión y ampliación de plazos
    update_project(@pt_entity) if @pt_entity.to_s.include? 'Pt'  

    old = @pt_entity.project.suspended
    old_father = @pt_entity.project.father
    AuditGenerateHelper.audit_update(@pt_entity, pt_entity_params, current_user)  
    set_pt_order(@pt_entity.project.father, @pt_entity.project, current_user, old_father)
    set_suspended_pt(old,@pt_entity.project)   
    respond_with(@pt_entity, location: projects_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    if @pt_entity.active
    AuditGenerateHelper.audit_destroy(@pt_entity, current_user)
    track_custom(current_user.loggable_id, @pt_entity.project.id, 'unsubscribe')
    end
    respond_with(@pt_entity)
  rescue => e
    log_error("COD-00007",e) 
  end

  protected
   
    def pt_entity_params
      params.require(:pt_entity).permit(
          :request_date,
          :request_description,
          :volunteers_profile,
          :activities,
          :sav_date,
          :derived_volunteers_num,
          :added_volunteers_num,
          :agreement_signed,
          :agreement_date,
          :prevailing,
          project_attributes: project_attributes
        )
    end

    alias_method :create_params, :pt_entity_params

  private    

    def set_num_addres
      @num_address = params[:events_count].to_i     
    end

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @pt_entity.try(:project).try(:entity).try(:vat_number).to_s)
    end
end
