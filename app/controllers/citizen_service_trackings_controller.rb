class CitizenServiceTrackingsController < ApplicationController  
  load_and_authorize_resource
  respond_to :html, :js, :json  
   
  before_action :tracking_types_active, only: [:new, :create, :update, :edit] 
  before_action :load_citizen_service

  def index
    @hash_tracking_types={}
    TrackingType.all_citizens_service.order(:name).each {|type| @hash_tracking_types.merge!({"#{type.name}" => type.id})}

    params[:q] ||= View::List::CitizenServiceTracking.ransack_default  
    params[:q][:s] ||= 'tracked_at desc'    
    @search_q = View::List::CitizenServiceTracking.search(params[:q])
    @unpaginated_citizen_service_trackings = @search_q.result.where(citizen_service_id: @citizen_service.id)

    # unless  params["search_manager"].blank?
    #   @citizen_service_trackings=@citizen_service_trackings.joins(:manager)
    # end
    @unpaginated_citizen_service_trackings=GlobalSearch.search_filter(@unpaginated_citizen_service_trackings,
      [ ["strict","tracking_type_id" ,  params["search_tipo"]],
        ["collective",[:manager,:tracking_type,:citizen_service,:coments], params["search_enter_text"]],
        ["ilike",:manager,  params["search_manager"]]
      ])

    # @unpaginated_citizen_service_trackings = @unpaginated_citizen_service_trackings.order(tracked_at: :desc)
    @citizen_service_trackings = @unpaginated_citizen_service_trackings.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_citizen_service_trackings,params)  }
    end
  end

  def show
    respond_with(@citizen_service_tracking) do |format|
      format.js 
      format.html
    end
  end

  def new
    respond_with(@citizen_service_tracking)
  end

  def edit
  end

  def create
    @citizen_service_tracking.tracked_at = Time.now
    if AuditGenerateHelper.audit_create(@citizen_service_tracking, current_user) 
      begin
        CitizenTracking.create(:citizen=> @citizen_service_tracking.citizen_service.citizen,
          :tracking_type => @citizen_service_tracking.tracking_type,
          :manager => @citizen_service_tracking.manager,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service_tracking.citizen_service.id}): #{@citizen_service_tracking.coments}")

        if !@citizen_service_tracking.citizen_service.volunteer.blank?
            Volun::Tracking.create(:volunteer=> @citizen_service_tracking.citizen_service.volunteer,
                :tracking_type => @citizen_service_tracking.tracking_type,
                :manager => @citizen_service_tracking.manager,
                :tracked_at => Time.zone.now,
                :comments => "Servicio (#{@citizen_service_tracking.citizen_service.id}): #{@citizen_service_tracking.coments}")
        end
      rescue => e
        begin
          Rails.logger.error("ERROR: create trackings manual -> #{e}")
        rescue
        end
      end
      redirect_to citizen_service_citizen_service_trackings_path(@citizen_service), notice: "Seguimiento del servicio del ciudadano se ha creado correctamente."
    else
      redirect_to new_citizen_service_citizen_service_tracking_path(@citizen_service), alert: "Se debe rellenar correctamente el formulario"
    end
  end

  def update
    @citizen_service_tracking.tracked_at = Time.now
    if AuditGenerateHelper.audit_update(@citizen_service_tracking, citizen_service_tracking_params, current_user)
      redirect_to citizen_service_citizen_service_trackings_path(@citizen_service_tracking.citizen_service), notice: "Seguimiento del servicio del ciudadano se ha actualizado correctamente."
    else
      redirect_to edit_citizen_service_citizen_service_tracking_path(@citizen_service, @citizen_service_tracking), alert: "Se debe rellenar correctamente el formulario"
    end
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@citizen_service_tracking, current_user)
    respond_with(@citizen_service_tracking)
  end

  protected
    def citizen_service_tracking_params
      params.require(:citizen_service_tracking).permit(
          :citizen_service_id,
          :tracking_type_id,
          :manager_id,
          :tracked_at,
          :coments
        )
    end
      
    alias_method :create_params, :citizen_service_tracking_params
  
    private 

    def load_citizen_service
      @citizen_service = CitizenService.find_by(id: params[:citizen_service_id])
    end

    
    def tracking_types_active 
      @tracking_types_active = TrackingType.all_citizens_service.order(:name)
    end 
  end
  