class CitizenServicesController < ApplicationController
    load_and_authorize_resource
    respond_to :html, :js, :json
    before_action :load_search , only: [:index]
    before_action :load_data, only: [:new,:edit,:update,:create,:search, :search_district]
    
    def index
      @citizen ||= nil
      params[:q] ||= View::List::CitizenService.ransack_default
      
      aux_s_param = params[:q][:s]
      @search_q = View::List::CitizenService.all.search(params[:q])
      @search_q.sorts ||= 'id asc'
      @unpaginated_citizen_services = @search_q.result
      if !params[:citizen_id].blank?
        @unpaginated_citizen_services=@unpaginated_citizen_services.where(citizen_id: params[:citizen_id])
        @citizen = Citizen.find(params[:citizen_id])
      end
      @unpaginated_citizen_services=@unpaginated_citizen_services.where("citizen_id in (?)", Citizen.not_market.select(:id))
      @unpaginated_citizen_services =GlobalSearch.search_filter(@unpaginated_citizen_services,
        [
         ["strict",:id,params["search_id"]],
         ["in",:service_id, params["search_service_type"]],
         ["in",:status, params["search_service_status"]],
         ["ilike",:name,params["search_nombre"]],

         ["ilike",:f_volunteer_name,params["search_nombre_volunteer"]],
         ["ilike",:f_volunteer_last_name_1,params["search_apellido1_volunteer"]],
         ["ilike",:f_volunteer_last_name_2,params["search_apellido2_volunteer"]],
         ["ilike",:f_citizen_name,params["search_nombre_citizen"]],
         ["ilike",:f_citizen_last_name_1,params["search_apellido1_citizen"]],
         ["ilike",:f_citizen_last_name_2,params["search_apellido2_citizen"]],

         ["collective",[:f_volunteer_email, :f_volunteer_name, :f_volunteer_last_name_1, :f_volunteer_last_name_2,
            :f_citizen_email, :f_citizen_last_name_1,:f_citizen_last_name_2, :f_citizen_name,:id
            ],params["enter_text"]
          ],
          ["f_between",:date_request,params["search_services_start"],params["search_services_end"]],
          ["f_between",:date_accept_request,params["search_services_a_start"],params["search_services_a_end"]]
        ])


        if !params["search_age_start"].blank? && !params["search_age_end"].blank?
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("age_citizen between ?  and ?",params["search_age_start"].to_i, params["search_age_end"].to_i)
        elsif !params["search_age_start"].blank? && params["search_age_end"].blank?
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("age_citizen >= ?",params["search_age_start"].to_i)
        elsif params["search_age_start"].blank? && !params["search_age_end"].blank?
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("age_citizen <= ?", params["search_age_end"].to_i)
        end

        if !params["search_gender"].blank? 
          case params["search_gender"].to_i
          when 1
            @unpaginated_citizen_services = @unpaginated_citizen_services.where("gender_name = 'Varón'")
          when 2
            @unpaginated_citizen_services = @unpaginated_citizen_services.where("gender_name = 'Mujer'")
          end     
        end

        if !params["search_start_rate"].blank? && !params["search_end_rate"].blank?
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("f_volunteer_vote between ? and  ?",params["search_start_rate"].to_i,params["search_end_rate"].to_i)
        elsif !params["search_start_rate"].blank? && params["search_end_rate"].blank?       
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("f_volunteer_vote >=  ?",params["search_start_rate"].to_i)
        elsif params["search_start_rate"].blank? && !params["search_end_rate"].blank?
          @unpaginated_citizen_services = @unpaginated_citizen_services.where("f_volunteer_vote <= ?",params["search_end_rate"].to_i)
         end
      
        if !params["search_district"].blank?  && params["search_district"].to_s!="null"
          @unpaginated_citizen_services= @unpaginated_citizen_services.where("district_id in (?)", params["search_district"].split(','))
        end

     
    
      if !params["search_other_option"].blank?
        case params["search_other_option"].to_i 
        when 1
          @unpaginated_citizen_services=@unpaginated_citizen_services.where(generated_gi: false)
        when 2
          @unpaginated_citizen_services=@unpaginated_citizen_services.where(generated_gi: true).where("enabled_app != 'Mayor con App móvil'")
        when 3
          @unpaginated_citizen_services=@unpaginated_citizen_services.where("email_verified= false")
        when 4
          @unpaginated_citizen_services=@unpaginated_citizen_services.where("tester=true")        
        when 5
          @unpaginated_citizen_services=@unpaginated_citizen_services.where("tester=false")
        end
      end

     

      
      #@unpaginated_citizen_services = CitizenService.for_ids_with_order(@unpaginated_citizen_services.map(&:id))
      @districts_names = District.where(active: true).all.select("name,id").order(:name)
      @hash_districts={}

      @districts_names.each {|n| @hash_districts.merge!({n.name=>n.id}) }
      
      @citizen_services = @unpaginated_citizen_services.paginate(page: params["page"], per_page: params["per_page"]||15)
      respond_to do |format|
        format.html
        format.csv { !params[:questions].blank? && params[:questions].to_s == "true" ? questions_stream_csv_report(@unpaginated_citizen_services,params) : stream_csv_report(@unpaginated_citizen_services,params)  }
      end
    end
    
    def new
      @citizen_service=CitizenService.new
    end

    def create
      @citizen_service.district=  Address.district_list.find_by(id: citizen_service_params[:district_id])
      @citizen_service.borought= Address.borougth_list( @citizen_service.district.try(:code)).find_by(id: citizen_service_params[:borought_id])
      @citizen_service.status ="active" if citizen_service_params[:status].blank?

      @citizen_service.status_service = StatusService.find_by(code:  citizen_service_params[:status])
      @citizen_service.generated_gi = true

      if AuditGenerateHelper.audit_create(@citizen_service,current_user)    
        @citizen_service.district=  Address.district_list.find_by(id: citizen_service_params[:district_id])
        @citizen_service.borought= Address.borougth_list( @citizen_service.district.try(:code)).find_by(id: citizen_service_params[:borought_id])
        permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service").try(:value).to_i
        days_difference = (Time.parse("#{@citizen_service.date_request} #{@citizen_service.hour_request}") - @citizen_service.created_at.to_datetime).abs
        if ["active","priority","expired"].include?(citizen_service_params[:status])
          @citizen_service.status = days_difference*24 > (permit_create.blank? ? 48 : permit_create ) ? "active" : "prioritary"    
          @citizen_service.status_service = StatusService.find_by(code:  @citizen_service.status)
          @citizen_service.save 
        end

        CitizenServiceTracking.create(:citizen_service=> @citizen_service,
          :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service.id}): Se ha generado el servicio en la aplicación de gestión interna")
        
        CitizenTracking.create(:citizen=> @citizen_service.citizen,
          :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service.id}): Se ha generado el servicio en la aplicación de gestión interna")

        if !@citizen_service.volunteer.blank?
          Volun::Tracking.create(:volunteer=> @citizen_service.volunteer,
            :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
            :manager => current_user.loggable,
            :tracked_at => Time.zone.now,
            :comments => "Servicio (#{@citizen_service.id}): Se ha generado el servicio en la aplicación de gestión interna")
        end
      end
      respond_with(@citizen_service)
    end

    def show
      respond_with(@citizen_service) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    end

    def edit
    end

    def update 
      aux_status = CitizenService.find_by(id: @citizen_service.id).try(:status)     
      @citizen_service.district=  Address.district_list.find_by(id: citizen_service_params[:district_id])
      @citizen_service.borought= Address.borougth_list( @citizen_service.district.try(:code)).find_by(id: citizen_service_params[:borought_id])

      @citizen_service.status_service = StatusService.find_by(code:  citizen_service_params[:status])

      puts "="*50
      puts  @citizen_service.district.id
      puts  @citizen_service.borought.id
             
      if AuditGenerateHelper.audit_update(@citizen_service, citizen_service_params, current_user)
        @citizen_service.district=  Address.district_list.find_by(id: citizen_service_params[:district_id])
        @citizen_service.borought= Address.borougth_list( @citizen_service.district.try(:code)).find_by(id: citizen_service_params[:borought_id])
        if ["active","priority","expired"].include?(citizen_service_params[:status])
          if !aux_status.blank? && ["canceled","finished"].include?(aux_status.to_s) 
            @citizen_service.volunteer = nil
            @citizen_service.date_accept_request = nil
            @citizen_service.hour_accept_request = nil
          end

          permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")
          fecha = Time.parse("#{@citizen_service.date_request} #{@citizen_service.hour_request}")
          horas = permit_create.blank? ? 48 :  permit_create.value.to_i

          if fecha < Time.zone.now
            @citizen_service.status = "expired"
          elsif fecha - (horas.to_i/2).hours >= Time.now 
            @citizen_service.status = "active"
          else 
            @citizen_service.status = "prioritary"
          end
          @citizen_service.status_service = StatusService.find_by(code:   @citizen_service.status)
          @citizen_service.save
        end

        if !["canceled","finished"].include?(aux_status.to_s) && ["canceled","finished"].include?(@citizen_service.status.to_s) 
          begin
            SMSApi.new.sms_deliver(@citizen_service.citizen.phone, "Se ha cancelado el servicio  
                #{@citizen_service.try(:type_service).to_s} de acompañamiento del día #{@citizen_service.try(:date_request).to_s} a la 
                hora #{@citizen_service.try(:hour_request).to_s}.")
          rescue =>error
              begin
                  Rails.logger.error("ERROR-GI: SMS -> #{error}")
              rescue
              end
          end   
        end        
       
      end
      respond_with(@citizen_service)
    end
  
    def destroy
      @citizen_service.reason_manage_canceled = 'Cancelado por el Departamento de voluntariado'
      @citizen_service.reason_manage_canceled_at = Time.now
      @citizen_service.status='canceled'
      if AuditGenerateHelper.audit_archive(@citizen_service, current_user)    
        begin
          SMSApi.new.sms_deliver(@citizen_service.citizen.phone, "Se ha cancelado el servicio  
              #{@citizen_service.try(:type_service).to_s} de acompañamiento del día #{@citizen_service.try(:date_request).to_s} a 
              la hora #{@citizen_service.try(:hour_request).to_s}.")
        rescue =>error          
            begin
                Rails.logger.error("ERROR-GI: SMS -> #{error}")
            rescue
            end
        end   
        # begin 
        #   SMSApi.new.sms_deliver(@citizen_service.citizen.phone, I18n.t("ws_accompany.sms.finished"))
        #   puts "Enviado SMS"
        # rescue Exception  => e
        #   Rails.logger.error('sendSms#canceledAccompany') do
        #     "Error sending sms: \n#{e}"
        #   end
        #   puts "===================================="
        #   puts "Error sending sms: \n#{e}"
        #   puts "===================================="
        # end
        CitizenServiceTracking.create(:citizen_service=> @citizen_service,
          :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service.id}): Se ha dado de baja el servicio en la aplicación de gestión interna")

          CitizenTracking.create(:citizen=> @citizen_service.citizen,
            :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
            :manager => current_user.loggable,
            :tracked_at => Time.zone.now,
            :coments => "Servicio (#{@citizen_service.id}): Se ha dado de baja el servicio en la aplicación de gestión interna")
  
          if !@citizen_service.volunteer.blank?
            Volun::Tracking.create(:volunteer=> @citizen_service.volunteer,
              :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
              :manager => current_user.loggable,
              :tracked_at => Time.zone.now,
              :comments => "Servicio (#{@citizen_service.id}): Se ha dado de baja el servicio en la aplicación de gestión interna")
          end
      end
      respond_with(@citizen_service)
    end
  
    def recover
      permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")

      fecha = Time.zone.parse("#{@citizen_service.date_request} #{@citizen_service.hour_request}")
      horas = permit_create.blank? ? 48 :  permit_create.value.to_i

      if fecha < Time.zone.now
        @citizen_service.status = "expired"
      elsif fecha - (horas.to_i/2).hours >= Time.zone.now 
        @citizen_service.status = "active"
      else 
        @citizen_service.status = "prioritary"
      end

      @citizen_service.volunteer = nil
      @citizen_service.date_accept_request = nil
      @citizen_service.hour_accept_request = nil

      @citizen_service.reason_manage_active_at = Time.now
      if AuditGenerateHelper.audit_recover(@citizen_service, current_user)
        CitizenServiceTracking.create(:citizen_service=> @citizen_service,
          :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service.id}): Se ha dado de alta el servicio en la aplicación de gestión interna")
        
          CitizenTracking.create(:citizen=> @citizen_service.citizen,
            :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Servicio (#{@citizen_service.id}): Se ha dado de alta el servicio en la aplicación de gestión interna")
  
          if !@citizen_service.volunteer.blank?
            Volun::Tracking.create(:volunteer=> @citizen_service.volunteer,
              :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
              :manager => current_user.loggable,
              :tracked_at => Time.zone.now,
              :comments => "Servicio (#{@citizen_service.id}): Se ha dado de alta el servicio en la aplicación de gestión interna")
          end
      end
      respond_with(@citizen_service)
    end

    def question
      @citizen_service = CitizenService.find(params[:id])
      respond_with(@citizen_service) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    end

    def show_mail
      @mail_address = Setting.find_by(key: "ListaDeCorreosEnvioCuestionario").try(:value)
      if !@mail_address.blank?
        respond_with(@citizen_services) do |format|
          format.js #{ render 'shared/popup' }
          format.html
        end
      else
        render js: "swal( '#{t('alert_title')}','#{t('alert_mail')}','error')"
      end
    end

    def send_mail
      begin
        if !Setting.find_by(key: 'ListaDeCorreosEnvioCuestionario').try(:value).blank?
          VolunteerMailer.send_email(Setting.find_by(key: 'ListaDeCorreosEnvioCuestionario').value, sender: params[:sender], subject: params[:subject], message: params[:message], upload: params[:upload]).deliver_now
          AuditGenerateHelper.audit_send_mail(params[:recipient], current_user)
          redirect_to citizen_services_path, notice: I18n.t('send_questions_success')
        else
          redirect_to citizen_services_path, alert: I18n.t('send_questions_no_mail') 
        end
      rescue Exception  => e
         Rails.logger.error('VolunteersController#send_mail') do
           "#{I18n.t('mailer.error')}: \n#{e}"
         end
         redirect_to citizen_services_path, alert: I18n.t('alert_message_sending')
      end
    end

    def search
      @citizens = Citizen.search_citizen(params[:search].split("=").last.gsub("%20","").gsub("%C3%A1", 'á').gsub("%C3%A9", 'é').gsub("%C3%AD", 'í').gsub("%C3%B3", 'ó').gsub("%C3%BA", 'ú'))
      respond_to :js
    end

    def search_district
      @address_data = Citizen.return_address_data(params[:citizen_id])
      params.merge!(district: @address_data[:district]) if !@address_data.blank? && !@address_data[:district].blank?
      respond_to :js
    end

    private

    def load_data
     
      @citizen_selector=Citizen.all_active.select(:name,:last_name_1,:last_name_2,:id).order(name: :asc, last_name_1: :asc, last_name_2: :asc).map {|i| ["#{i.name} #{i.last_name_1} #{i.last_name_2}",i.id]}
      @service_selector=Service.all_active.select(:name,:id).map {|i| [i.name,i.id]}
      @services=Service.all_active.select(:name,:id)
      @districts=District.where(active: true).select(:name,:id).map {|i| [i.name,i.id]}
      @boroughts_selector=Borought.where(active: true).select(:name,:id).map {|i| [i.name,i.id]}
      @status_selector=Service.status_type_list.map {|i| [ I18n.t("service.status.#{i}"),i]}
    end

    def stream_file(filename, extension)
      response.headers["Content-Type"] = "application/octet-stream"
      response.headers["Content-Disposition"] = "attachment; filename=#{filename}.#{extension}"
  
      yield response.stream
      ensure
      response.stream.close
    end

    def questions_stream_csv_report(items,params)
      stream_file("Cuestionario", "csv") do |stream|
        CitizenService.questions_stream_query_rows(items,params) do |row_from_db|
          stream.write row_from_db
        end
      end
    rescue 
    end

    def load_search
      @hash_service_type= {}
      Service.all_active.order(:name).select(:name,:id).each do |s|
        @hash_service_type.merge!({s.name => s.id})
      end

      @hash_service_status= {}
      Service.status_type_list.each do |s|
        @hash_service_status.merge!({I18n.t("service.status.#{s}") => s})
      end
      @hash_service_status = @hash_service_status.sort.to_h


      @hash_gender_type= {}
      @hash_gender_type.merge!({"Todos"=>"0", "Mujer" => "2","Varón" => "1"})

      @hash_other_option = {}
      @hash_other_option.merge!({"Todos" => "0"})
      @hash_other_option.merge!({"Generados en APP" => "1"})
      @hash_other_option.merge!({"Generados en GI sin APP móvil" => "2"})
      @hash_other_option.merge!({"Generados 'Sin verificar'" => "3"})
      @hash_other_option.merge!({"Servicio tester" => "4"})
      @hash_other_option.merge!({"Servicio no tester" => "5"})
    end

    protected

    def citizen_service_params
      params
        .require(:citizen_service)
        .permit(
          :id,
          :citizen_id,
          :service_id,
          :date_request,
          :hour_request,
          :date_accept_request,
          :hour_accept_request,
          :description,
          :place,
          :district,
          :borought,
          :district_id,
          :borought_id,
          :status,
          :indications,
          :additional_data
        )
    end
end