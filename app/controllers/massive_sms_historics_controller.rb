class MassiveSmsHistoricsController < ApplicationController
    load_and_authorize_resource
    respond_to :html, :js

   
    def show
        @massive_sms_historic=MassiveSmsHistoric.find_by(id: params[:id])
        respond_with(@massive_sms_historic) do |format|
        format.js #{ render 'shared/popup' }
        format.html
        end
    end
end