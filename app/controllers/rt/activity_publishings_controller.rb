class Rt::ActivityPublishingsController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_activity_publishing

  def index
    params[:q] ||= View::List::Rt::ActivityPublishing.ransack_default

    @search_q = View::List::Rt::ActivityPublishing.with_statuses(statuses.select{|status|  calculate_statuses[status].to_s=="true"}).search(params[:q])
    @unpaginated_activity_publishings=@search_q.result
    @unpaginated_activity_publishings=GlobalSearch.search_filter(@unpaginated_activity_publishings,
      [ ["f_between",:created_at, params["search_start_date"], params["search_end_date"]],
        ["collective",[:name,:description,:places_text_free], params["enter_text"]],
        ["ilike",:entity_id, params["search_entity"]]
      ])

    @rt_activity_publishings = @unpaginated_activity_publishings.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
      format.csv { stream_csv_report(@unpaginated_activity_publishings,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def process_request_form
    unless @rt_activity_publishing.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_activity_publishing, current_user)
    end
    status_manager = RtController::StatusManager.new(request_form: @rt_activity_publishing.request_form,
                                                     manager_id: current_user.loggable_id)
    @request_form = @rt_activity_publishing.request_form
    if params[:input_comments]
      @request_form.update_attribute(:comments, params[:input_comments])
    end
    unless status_manager.process_request_form
      redirect_to rt_activity_publishings_path, alert: status_manager.show_errors
    end
  rescue => e
    log_error("COD-00003",e) 
  end

  def pre_approve_request_form
    if @rt_activity_publishing.request_form.processing?
      redirect_to new_activity_path(rt_activity_publishing_id: @rt_activity_publishing.id)
    else
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      redirect_to process_request_form_rt_activity_publishing_path(@rt_activity_publishing)
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  protected
    
    def get_resource
      @rt_activity_publishing
    end

    def get_redirect_list
      rt_activity_publishings_path
    end

    def get_model_class
      @model_class = Rt::ActivityPublishing
    end
end
