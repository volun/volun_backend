class Rt::VolunteersDemandsController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_volunteers_demand
 
  def index
    params[:q] ||= View::List::Rt::VolunteersDemand.ransack_default

    @search_q = View::List::Rt::VolunteersDemand.with_statuses(statuses.select{|status| calculate_statuses[status].to_s == "true"}).search(params[:q])
    @unpaginated_volunteers_demands=@search_q.result
    @unpaginated_volunteers_demands=GlobalSearch.search_filter(@unpaginated_volunteers_demands,
      [ ["f_between",:created_at,params["search_start_date"],params["search_end_date"]],
        ["collective",[:notes,:description],params["enter_text"]],
        ["ilike",:entity_id,params["search_entity"]],
        ["ilike",:notes,params["search_notes"]],
        ["ilike",:description,params["search_description"]]
      ])

    @rt_volunteers_demands = @unpaginated_volunteers_demands.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
      format.csv { stream_csv_report(@unpaginated_volunteers_demands,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def process_request_form
    unless @rt_volunteers_demand.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_volunteers_demand, current_user)
    end
    status_manager = RtController::StatusManager.new(request_form: @rt_volunteers_demand.request_form, manager_id: current_user.loggable_id)  

    @request_form = @rt_volunteers_demand.request_form
    if params[:input_comments]
      @request_form.update_attribute(:comments, params[:input_comments])
    end
    unless status_manager.process_request_form
      redirect_to rt_volunteers_demands_path, alert: status_manager.show_errors
    end
  rescue => e
    log_error("COD-00003",e) 
  end

  def pre_approve_request_form
    if @rt_volunteers_demand.request_form.processing? && !@rt_volunteers_demand.entity.blank?
      approve_request_form
      respond_to do |format|
        format.html { redirect_to(rt_volunteers_demands_url, notice: I18n.t('messages.request_form_successfully_managed')) }
        format.js
      end
    elsif @rt_volunteers_demand.entity.blank?
      flash[:alert] = I18n.t('errors.not_found_volunteer')
      redirect_to process_request_form_rt_volunteers_demand_path(@rt_volunteers_demand)
    else
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      redirect_to process_request_form_rt_volunteers_demand_path(@rt_volunteers_demand)
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  protected

    def approve_request_form
      ActiveRecord::Base.transaction do
        default_attrs = {
          tracking_type: TrackingType.get_volunteers_demand,
          comments:     @rt_volunteers_demand.request_form.comments,
          entity:    @rt_volunteers_demand.entity,
          request_form: @rt_volunteers_demand.request_form,
          manager_id:   current_user.loggable_id,
          tracked_at:   DateTime.now,
          automatic:    true,
        }
        tracking = Ent::Tracking.new(default_attrs)
        @rt_volunteers_demand.copy_errors_from!(tracking) unless tracking.save
        AuditGenerateHelper.audit_request_accept(@rt_volunteers_demand, current_user)
      end

      unless @rt_volunteers_demand.request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id)
        @rt_volunteers_demand.copy_errors_from!(@rt_volunteers_demand.request_form)
      end
    end

    def get_resource
      @rt_volunteers_demand
    end

    def get_redirect_list
      rt_volunteers_demands_path
    end

    def get_model_class
      @model_class = Rt::VolunteersDemand
    end
end
