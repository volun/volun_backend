class Rt::VolunteerAmendmentsController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_volunteer_amendment

  def index
    params[:q] ||= View::List::Rt::VolunteerAmendment.ransack_default
    # unless  params["enter_text"].blank?
    #   @rt_volunteer_amendments = @rt_volunteer_amendments.joins(:volunteer,:district)
    # end
    @search_q =  View::List::Rt::VolunteerAmendment.with_statuses(statuses.select{|status|  calculate_statuses[status].to_s=="true"}).search(params[:q])
    @unpaginated_volunteer_amendments=@search_q.result
    @unpaginated_volunteer_amendments=GlobalSearch.search_filter(@unpaginated_volunteer_amendments,
      [ ["f_between",:created_at, params["search_start_date"], params["search_end_date"]],
        ["collective",[
          ["full",:name,:last_name],
          ["full",:name,:last_name,:last_name_alt],
          :name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code,:district
          ], params["enter_text"]
        ],
        ["ilike",:postal_code, params["search_postal_code"]]
      ])   
    unless params["search_district"].blank?
      if params["search_district"]=="-280"
        @unpaginated_volunteer_amendments=@unpaginated_volunteer_amendments.where("((cast(postal_code as varchar) NOT LIKE '280%' OR postal_code is null) AND (district='OTROS' ) OR district is null) OR (cast(postal_code as varchar) NOT LIKE '280%' AND postal_code is not null)")
      else
        @unpaginated_volunteer_amendments=@unpaginated_volunteer_amendments.where("UPPER(cast(district as varchar)) LIKE UPPER(?)","%#{params["search_district"].to_s}%").uniq    
      end
    end
    @rt_volunteer_amendments = @unpaginated_volunteer_amendments.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
      format.csv  { render text: Rt::VolunteerAmendment.to_csv(@rt_volunteer_amendments), filename: "#{Rt::VolunteerAmendment.model_name.human(count: 2)}.csv" }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def process_request_form
    unless @rt_volunteer_amendment.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_volunteer_amendment, current_user)
    end

    status_manager = RtController::StatusManager.new(request_form: @rt_volunteer_amendment.request_form,
                                                     manager_id: current_user.loggable_id)
    
    @request_form = @rt_volunteer_amendment.request_form
    if params[:input_comments]
      @request_form.update_attribute(:comments, params[:input_comments])
    end
    unless status_manager.process_request_form
      redirect_to rt_volunteer_amendments_path, alert: status_manager.show_errors
    end
  rescue => e
    log_error("COD-00003",e) 
  end

  def pre_approve_request_form
    if @rt_volunteer_amendment.request_form.processing? && !@rt_volunteer_amendment.volunteer.blank?
      redirect_to edit_volunteer_path(@rt_volunteer_amendment.volunteer,rt_volunteer_amendment_id: @rt_volunteer_amendment.id)
    elsif @rt_volunteer_amendment.request_form.user.blank? || @rt_volunteer_amendment.volunteer.blank?
      flash[:alert] = I18n.t('errors.not_found_volunteer')
      redirect_to process_request_form_rt_volunteer_amendment_path
    else
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      redirect_to process_request_form_rt_volunteer_amendment_path
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  def process_dowload_request
    array_ids=[]
    params[:district][:ids].each do |key, value|
      array_ids.push(key) unless key.blank?
    end

    if !array_ids.blank?
      list = Rt::VolunteerAmendment.all.where("district_id in (?)",array_ids)
      stream_csv_report(list,params)    
    else
      list = Rt::VolunteerAmendment.all
      stream_csv_report(list,params)    
    end
  rescue => e
    log_error("COD-00010",e) 
  end

  protected

    def get_resource
      @rt_volunteer_amendment
    end

    def get_redirect_list
      rt_volunteer_amendments_path
    end

    def get_model_class
      @model_class = Rt::VolunteerAmendment
    end
end
