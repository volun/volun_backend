class Rt::VolunteerAppointmentsController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_volunteer_appointment

  def index 
    params[:q] ||= View::List::Rt::VolunteerAppointment.ransack_default
    @search_q =View::List::Rt::VolunteerAppointment.with_statuses(statuses.select{|status| calculate_statuses[status].to_s == "true"}).search(params[:q])
    @unpaginated_volunteer_appointments=@search_q.result
    @unpaginated_volunteer_appointments=GlobalSearch.search_filter(@unpaginated_volunteer_appointments,
      [ ["f_between",:created_at,params["search_start_date"],params["search_end_date"]],
        ["collective",[
          ["full",:name,:last_name],
          ["full",:name,:last_name,:last_name_alt],
          :name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code,:district,
          ],params["enter_text"]
        ],
        ["ilike",:postal_code,params["search_postal_code"]]
      ])
    unless params["search_district"].blank?
      if params["search_district"]=="-280"
        @unpaginated_volunteer_appointments=@unpaginated_volunteer_appointments.where("((cast(postal_code as varchar) NOT LIKE '280%' OR postal_code is null) AND (district='OTROS'  OR district is null)) OR (cast(postal_code as varchar) NOT LIKE '280%' AND postal_code is not null)")
      else
        @unpaginated_volunteer_appointments=@unpaginated_volunteer_appointments.where("UPPER(cast(district as varchar)) LIKE UPPER(?)","%#{params["search_district"].to_s}%").uniq    
      end
    end

    @rt_volunteer_appointments = @unpaginated_volunteer_appointments.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def process_request_form
    unless @rt_volunteer_appointment.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_volunteer_appointment, current_user)
    end

    status_manager = RtController::StatusManager.new(request_form: @rt_volunteer_appointment.request_form,
                                                     manager_id: current_user.loggable_id)
    
    @request_form = @rt_volunteer_appointment.request_form
    if params[:input_comments]
      @request_form.update_attribute(:comments, params[:input_comments])
    end
    unless status_manager.process_request_form
      redirect_to rt_volunteer_appointments_path, alert: status_manager.show_errors
    end
  rescue => e
    log_error("COD-00003",e) 
  end

  def pre_approve_request_form
    if @rt_volunteer_appointment.request_form.processing? && !@rt_volunteer_appointment.volunteer.blank?
      approve_request_form
      AuditGenerateHelper.audit_request_accept(@rt_volunteer_appointment, current_user)
     
      respond_to do |format|
        format.html { redirect_to(rt_volunteer_appointments_url, notice: I18n.t('messages.request_form_successfully_managed')) }
        format.js
      end
    elsif @rt_volunteer_appointment.volunteer.blank?
      flash[:alert] = I18n.t('errors.not_found_volunteer')
      redirect_to process_request_form_rt_volunteer_appointment_path
    else
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      redirect_to process_request_form_rt_volunteer_appointment_path
    end
  rescue => e
    log_error("COD-00004",e) 
    flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
    redirect_to process_request_form_rt_volunteer_appointment_path
  end

  def process_dowload_request
    array_ids=[]
    params[:district][:ids].each do |key, value|
      array_ids.push(key) unless key.blank?
    end
    
    if !array_ids.blank?
      list = Rt::VolunteerAppointment.joins(:volunteer => [:address]).all.where("addresses.district_id in (?)", array_ids)
      stream_csv_report(list,params)
    else
      stream_csv_report(Rt::VolunteerAppointment.joins(:volunteer => [:address]).all,params)
    end
  rescue => e
    log_error("COD-00010",e) 
  end

  protected    

    def approve_request_form
      ActiveRecord::Base.transaction do
        default_attrs = {
          tracking_type: TrackingType.get_volunteer_appointment,
          comments:      I18n.t('trackings.volunteer_appointment'),
          volunteer:    @rt_volunteer_appointment.volunteer,
          request_form: @rt_volunteer_appointment.request_form,
          manager_id:   current_user.loggable_id,
          tracked_at:   DateTime.now,
          automatic:    true,
        }
        tracking = Volun::Tracking.new(default_attrs)
        copy_errors_from!(tracking) unless tracking.save
      end


      unless @rt_volunteer_appointment.request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id)
        copy_errors_from!(@rt_volunteer_appointment.request_form)
      end
    end

    def get_resource
      @rt_volunteer_appointment
    end

    def get_redirect_list
      rt_volunteer_appointments_path
    end

    def get_model_class
      @model_class = Rt::VolunteerAppointment
    end
end
