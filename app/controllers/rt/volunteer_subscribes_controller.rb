class Rt::VolunteerSubscribesController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_volunteer_subscribe

  before_action :load_data, only: [:index,:new,:create,:process_request_form,:edit,:update]
  
  def index
    params[:q] ||= View::List::Rt::VolunteerSubscribe.ransack_default
    @search_q = View::List::Rt::VolunteerSubscribe.with_statuses(statuses.select{|status| calculate_statuses[status].to_s == "true"}).search(params[:q])
    @unpaginated_volunteer_subscribes=@search_q.result
    @unpaginated_volunteer_subscribes=GlobalSearch.search_filter(@unpaginated_volunteer_subscribes,
      [ ["f_between",:created_at,params["search_start_date"],params["search_end_date"]],
        ["collective",[
          ["full",:name,:last_name],
          ["full",:name,:last_name,:last_name_alt],
          :name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code
          ],params["enter_text"]
        ],
        ["ilike",:postal_code,params["search_postal_code"]]
      ])
    unless params["search_district"].blank?
      if params["search_district"]=="-280"
        @unpaginated_volunteer_subscribes=@unpaginated_volunteer_subscribes.where("((cast(postal_code as varchar) NOT LIKE '280%' OR postal_code is null) AND district='OTROS' OR district_id is null) OR (cast(postal_code as varchar) NOT LIKE '280%' AND postal_code is not null)")
      else
        @unpaginated_volunteer_subscribes=@unpaginated_volunteer_subscribes.where("UPPER(cast(district as varchar)) LIKE UPPER(?)","%#{params["search_district"].to_s}%").uniq    
      end
    end
    @rt_volunteer_subscribes = @unpaginated_volunteer_subscribes.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  

  def process_dowload_request    
    array_ids=[]
    params[:district][:ids].each {|key, value| array_ids.push(key) unless key.blank?}

    if !array_ids.blank?      
      list = Rt::VolunteerSubscribe.all.where("district_id in (?)",array_ids)
      stream_csv_report(list,params)
    else
      stream_csv_report(Rt::VolunteerSubscribe.all, params)
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  def process_request_form
    unless @rt_volunteer_subscribe.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_volunteer_subscribe, current_user)     
    end

    status_manager = RtController::StatusManager.new(request_form: @rt_volunteer_subscribe.request_form, manager_id: current_user.loggable_id)
    @request_form = @rt_volunteer_subscribe.request_form
    if params[:appointment_at]
      @rt_volunteer_subscribe.appointment_at = Time.zone.parse("#{params[:appointment_at]['due_date(1i)']}-#{params[:appointment_at]['due_date(2i)']}-#{params[:appointment_at]['due_date(3i)']} #{params[:appointment_at]['due_date(4i)']}:#{params[:appointment_at]['due_date(5i)']}")
      @rt_volunteer_subscribe.save
    end

    if params[:channel]
      @rt_volunteer_subscribe.channel = Channel.find_by(id: params[:channel])
      @rt_volunteer_subscribe.save
    end

    [:contact_person].each do |input|
      if params[input]
        @rt_volunteer_subscribe.update_attribute(input,  params[input])
      end
    end
    [ :comments].each do |input|
      if params[input]
        @request_form.update_attribute(input, params[input])
      end
    end
    
    unless status_manager.process_request_form
      redirect_to rt_volunteer_subscribes_path, alert: status_manager.show_errors
    end
  rescue => e
    log_error("COD-00005",e) 
  end

  def pre_approve_request_form
    if @rt_volunteer_subscribe.request_form.processing?
      redirect_to new_volunteer_path(rt_volunteer_subscribe_id: @rt_volunteer_subscribe.id)
    else
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      redirect_to  process_request_form_rt_volunteer_subscribe_path
    end
  rescue => e
    log_error("COD-0006",e) 
  end

  def send_appointment
    status_manager = RtController::StatusManager.new(request_form: @rt_volunteer_subscribe.request_form,
      manager_id: current_user.loggable_id)
    if status_manager.mark_request_form_as_appointed
      AuditGenerateHelper.audit_request_appointed(@rt_volunteer_subscribe, current_user) 
      errormail = false
      errorsms = false  
      if !@rt_volunteer_subscribe.email.blank?
        message = I18n.t('rt_volunteer_subscribes.message_appointment', 
          date_at: @rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" : @rt_volunteer_subscribe.appointment_at.strftime("%d/%m/%y").to_s,
          hour_at: @rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" : @rt_volunteer_subscribe.appointment_at.strftime("%H:%M").to_s,
          contact_person: @rt_volunteer_subscribe.contact_person).html_safe
        subject = I18n.t('rt_volunteer_subscribes.subject_volunteer')
        begin
          VolunteerMailer.send_email(@rt_volunteer_subscribe.email, message: message, subject: subject).deliver_now
          AuditGenerateHelper.audit_send_mail(@rt_volunteer_subscribe, current_user)
        rescue Exception  => e
          errormail= true
          begin
            Rails.logger.error("ERROR-MAIL: #{e}")
          rescue
          end
        end
      end

      if !@rt_volunteer_subscribe.phone_number_alt.blank?
        message = I18n.t('rt_volunteer_subscribes.message_appointment_sms',
          date_at: @rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" : @rt_volunteer_subscribe.appointment_at.strftime("%d/%m/%y"),
          hour_at: @rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" : @rt_volunteer_subscribe.appointment_at.strftime("%H:%M"),
          contact_person: Nokogiri::HTML(@rt_volunteer_subscribe.contact_person).text.to_s).html_safe
        begin
          SMSApi.new.sms_deliver(@rt_volunteer_subscribe.phone_number_alt, message)
          AuditGenerateHelper.audit_send_sms(@rt_volunteer_subscribe, current_user)
        rescue Exception  => e
          errorsms = true
          begin
            Rails.logger.error("ERROR-SMS: #{e}")
          rescue
          end
        end
      end
      if errormail || errorsms
        flash[:alert]  = I18n.t("error_message_sending_rt_email") if errormail && !errorsms
        flash[:alert]  = I18n.t("error_message_sending_rt_phone") if !errormail && errorsms
        flash[:alert]  = I18n.t("error_message_sending_rt") if errormail && errorsms
        redirect_to process_request_form_rt_volunteer_subscribe_path(@rt_volunteer_subscribe)
      else
        redirect_to process_request_form_rt_volunteer_subscribe_path(@rt_volunteer_subscribe), notice: I18n.t('success_message_sending')
      end
    else
      flash[:alert] = status_manager.show_errors
      redirect_to process_request_form_rt_volunteer_subscribe_path(@rt_volunteer_subscribe)
    end
  rescue => e
    log_error("COD-00012",e) 
  end

  def new
    @rt_volunteer_subscribe= Rt::VolunteerSubscribe.new
    respond_with(@rt_volunteer_subscribe, :location => rt_volunteer_subscribes_path)
  rescue => e
    log_error("COD-00013",e) 
  end

  def create
    req_exist = Rt::VolunteerSubscribe.joins(:request_form).where("(rt_volunteer_subscribes.email = ? AND rt_volunteer_subscribes.email <> '' AND rt_volunteer_subscribes.email is not null) OR 
      (rt_volunteer_subscribes.phone_number_alt= ? AND rt_volunteer_subscribes.phone_number_alt <> '' AND rt_volunteer_subscribes.phone_number_alt is not null) AND (request_forms.req_status_id=1 
        OR request_forms.req_status_id=2 OR request_forms.req_status_id=3)",rt_volunteer_subscribe_params[:email],rt_volunteer_subscribe_params[:phone_number_alt]).count +
        Volunteer.where("(volunteers.email= ? AND volunteers.email <> '') OR (volunteers.phone_number_alt= ? AND volunteers.phone_number_alt <> '') ", rt_volunteer_subscribe_params[:email], rt_volunteer_subscribe_params[:phone_number_alt]).count
    if ((!rt_volunteer_subscribe_params[:email].blank? || !rt_volunteer_subscribe_params[:phone_number_alt].blank?) && req_exist>0) #Si ya tiene una solicitud -> ERROR  
      redirect_to new_rt_volunteer_subscribe_path, alert: I18n.t('request.duplicate')
    else #Si no, genera la nueva solicitud de alta de voluntario
      @rt_volunteer_subscribe = Rt::VolunteerSubscribe.new(rt_volunteer_subscribe_params) #Nueva solicitud
      # @rt_volunteer_subscribe.request_form.user = current_user
      @rt_volunteer_subscribe.appointment_at = Time.zone.parse("#{params["appointment_at(3i)"]}/#{params["appointment_at(2i)"]}/#{params["appointment_at(1i)"]} #{params["appointment_at(4i)"]}:#{params["appointment_at(5i)"]}")    

      AuditGenerateHelper.audit_create(@rt_volunteer_subscribe, current_user) #Si se crea con éxito
      respond_with(@rt_volunteer_subscribe, :location => rt_volunteer_subscribes_path)
    end   
  rescue => e
    log_error("COD-00014",e) 
  end

  def edit
  rescue => e
    log_error("COD-00015",e) 
  end

  def update
    req_exist = Rt::VolunteerSubscribe.joins(:request_form).where("((rt_volunteer_subscribes.email = ? AND rt_volunteer_subscribes.email <> '' AND rt_volunteer_subscribes.email is not null) OR 
      (rt_volunteer_subscribes.phone_number_alt= ? AND rt_volunteer_subscribes.phone_number_alt <> '' AND rt_volunteer_subscribes.phone_number_alt is not null) AND (request_forms.req_status_id=1 
        OR request_forms.req_status_id=2 OR request_forms.req_status_id=3)) and rt_volunteer_subscribes.id not in (?)",rt_volunteer_subscribe_params[:email],rt_volunteer_subscribe_params[:phone_number_alt],@rt_volunteer_subscribe.id).count +
        Volunteer.where("((volunteers.email= ? AND volunteers.email <> '') OR (volunteers.phone_number_alt= ? AND volunteers.phone_number_alt <> ''))", rt_volunteer_subscribe_params[:email], rt_volunteer_subscribe_params[:phone_number_alt]).count
    if ((!rt_volunteer_subscribe_params[:email].blank? || !rt_volunteer_subscribe_params[:phone_number_alt].blank?) && req_exist>0) #Si ya tiene una solicitud -> ERROR  
      redirect_to edit_rt_volunteer_subscribe_path(@rt_volunteer_subscribe), alert: I18n.t('request.duplicate')
    else #Si no, genera la nueva solicitud de alta de voluntario
      # @rt_volunteer_subscribe.request_form.user = current_user
      @rt_volunteer_subscribe.appointment_at = Time.zone.parse("#{params["appointment_at(3i)"]}/#{params["appointment_at(2i)"]}/#{params["appointment_at(1i)"]} #{params["appointment_at(4i)"]}:#{params["appointment_at(5i)"]}")
     
      AuditGenerateHelper.audit_update(@rt_volunteer_subscribe,rt_volunteer_subscribe_params, current_user) #Si se crea con éxito
      respond_with(@rt_volunteer_subscribe, :location => rt_volunteer_subscribes_path)   
    end
  rescue => e
    log_error("COD-00016",e) 
  end


  protected

  def rt_volunteer_subscribe_params
    params.require(:rt_volunteer_subscribe).permit(:name, :last_name, :last_name_alt, 
      :phone_number, :phone_number_alt, :email, :info_source_id, :channel_id, :channel,
      :info_source,:is_adult,:postal_code,:district_id,:district,:notes,:contact_person, request_form: [:user_id])
  end

  alias_method :create_params, :rt_volunteer_subscribe_params

    def get_resource
      @rt_volunteer_subscribe
    end

    def get_redirect_list
      rt_volunteer_subscribes_path
    end

    def get_model_class
      @model_class = Rt::VolunteerSubscribe
    end

    def load_data 
      @hash_districts={}
      District.where(active: true).where.not(name: "OTROS").all.select("name").order(:name).each {|n|  @hash_districts.merge!({n=>n})} 
      @hash_districts.merge!({I18n.t('others')=>"-280"})
      @hash_entities={}
      Entity.all.where("active=true").select("id,name").order(:name).each {|n| @hash_entities.merge!({n.name=>n.id})}
      set_channels
      set_districts_full
      set_info_sources
    end
end
