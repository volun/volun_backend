class Rt::OthersController < ApplicationController
  include RtController

  load_and_authorize_resource instance_name: :rt_other
  
  def index
    params[:q] ||= View::List::Rt::Other.ransack_default
    @search_q = View::List::Rt::Other.with_statuses(statuses.select{|status|  calculate_statuses[status].to_s=="true"}).search(params[:q])
    @unpaginated_others=@search_q.result
    @unpaginated_others=GlobalSearch.search_filter(@unpaginated_others,
      [ ["f_between",:created_at, params["search_start_date"], params["search_end_date"]],
        ["collective",[:notes,:description], params["enter_text"]],
        ["ilike",:entity_id, params["search_entity"]],
        ["ilike",:notes, params["search_notes"]],
        ["ilike",:description, params["search_description"]]
      ])
    @rt_others = @unpaginated_others.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.js
      format.csv { stream_csv_report(@unpaginated_others,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end
  
  def process_request_form
    unless @rt_other.request_form.processing?
      AuditGenerateHelper.audit_request_process(@rt_other, current_user)
    end

    status_manager = RtController::StatusManager.new(request_form: @rt_other.request_form,
                                                     manager_id: current_user.loggable_id)
    @request_form = @rt_other.request_form
    if params[:input_comments]
      @request_form.update_attribute(:comments, params[:input_comments])
    end
    unless status_manager.process_request_form
      redirect_to rt_others_path, alert: status_manager.show_errors
    end     
  rescue => e
    log_error("COD-00003",e)                                            
  end

  def pre_approve_request_form
    if request_form.processing?
      approve_request_form
      AuditGenerateHelper.audit_request_accept(@rt_other, current_user)   
      respond_to do |format|
        format.html { redirect_to(rt_others_url, notice: I18n.t('messages.request_form_successfully_managed')) }
        format.js
      end
    else
      @request_form = request_form
      flash[:alert] = I18n.t('errors.request_form_must_be_at_processing')
      render :process_request_form
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  protected

    def approve_request_form
      unless request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id, user_id: current_user.id)
        copy_errors_from!(request_form)
      end
    end

    def get_resource
      @rt_other
    end

    def get_redirect_list
      rt_others_path
    end

    def get_model_class
      @model_class = Rt::Other
    end

end
