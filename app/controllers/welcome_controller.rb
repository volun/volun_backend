class WelcomeController < ApplicationController
  def index
  rescue => e
    log_error("COD-00001",e) 
  end

  def administration
    unless (can?(:audit, WelcomeController) || can?(:order, WelcomeController) || can?(:config, WelcomeController) || can?(:catalogs, WelcomeController) || can?(:managers, WelcomeController) || can?(:analyze_cv, WelcomeController) || can?(:new_campaign, WelcomeController) || can?(:resource, WelcomeController))
      redirect_to root_path, alert: I18n.t('messages.access_denied')
    end
    #authorize! :administration, WelcomeController
  rescue => e
    log_error("COD-00002",e) 
  end

  def maintenance
    unless can?(:catalogs, WelcomeController)
      redirect_to root_path, alert: I18n.t('messages.access_denied')
    end
    #authorize! :maintenance, WelcomeController
  rescue => e
    log_error("COD-00003",e) 
  end

end
