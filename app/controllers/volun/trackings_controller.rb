class Volun::TrackingsController < ApplicationController
  load_and_authorize_resource instance_name: :volun_tracking
  respond_to :html, :js, :json
  before_action :tracking_types_active, only: [:new, :create, :update, :edit] 
  before_action :get_projects, only: [:new, :create, :update, :edit]

  def index
    @tracking_types_active = TrackingType.all_volunteers_with_system.order(:name)
    @hash_tracking_types={}
    @tracking_types_active.each do |type|
      @hash_tracking_types.merge!({"#{type.name}" => type.id})
    end

    params[:q] ||= View::List::VolunTracking.ransack_default   
    @volunteer =  params[:q][:view_list_volunteer_id_eq].blank? ? Volunteer.find_by(id: params[:q][:volunteer_id_eq]) : Volunteer.find_by(id: params[:q][:view_list_volunteer_id_eq])

    # unless params["search_manager"].blank?
    #   @volun_trackings=@volun_trackings.joins(:manager)
    # end
    # unless params["search_project"].blank?
    #   @volun_trackings=@volun_trackings.joins(:project)
    # end
    params[:q][:s] ||= 'tracked_at desc'
    @search_q = View::List::VolunTracking.search(params[:q])
   # @search_q.sorts ||= 'tracked_at desc'
    @unpaginated_volun_trackings = @search_q.result.where(volunteer_id: @volunteer.id)
    @unpaginated_volun_trackings=GlobalSearch.search_filter(@unpaginated_volun_trackings,
      [ ["strict",:tracking_type_id , params["search_tipo"]],
        ["collective",[:manager,:volunteer,:project,:tracking_type,:comments],params["search_enter_text"]],
        ["f_between",:tracked_at,params["search_inicio_f"],params["search_fin_f"]],
        ["ilike",:manager, params["search_manager"]],
        ["ilike",:project, params["search_project"]]
      ])

    if params["project_id_assoc"].present?
      @unpaginated_volun_trackings = @unpaginated_volun_trackings.where(project_id: params["project_id_assoc"])
    end
    #@unpaginated_volun_trackings = @unpaginated_volun_trackings.order(tracked_at: :desc)
    @volun_trackings = @unpaginated_volun_trackings.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_volun_trackings,params)  }
    end
  rescue => e
    log_error("COD-00001",e)
  end

  def show
    respond_with(@volun_tracking) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    #volunteer = Volunteer.find_by(id: params[:volunteer_id])
    #@volun_tracking = volunteer.trackings.build
    @volun_tracking=Volun::Tracking.new(volunteer_id: params[:volunteer_id],project_id: params[:project_id_assoc])
    #@volun_tracking.project_id = params[:project_id_assoc]
    respond_with(@volun_tracking)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    AuditGenerateHelper.audit_create(@volun_tracking, current_user)
    respond_with(@volun_tracking, location: volun_trackings_path({update_volun: params[:update_volun],no_project: params[:no_project],q: { view_list_volunteer_id_eq: @volun_tracking.volunteer_id}, project_id_assoc: params[:project_id_assoc]})) 
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@volun_tracking, volun_tracking_params, current_user)
    respond_with(@volun_tracking, location: volun_trackings_path({update_volun: params[:update_volun],no_project: params[:no_project],q: { view_list_volunteer_id_eq: @volun_tracking.volunteer_id}, project_id_assoc: params[:project_id_assoc]})) 
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@volun_tracking, current_user)
    respond_with(@volun_tracking, location: volun_trackings_path({update_volun: params[:update_volun],no_project: params[:no_project],q: { view_list_volunteer_id_eq: @volun_tracking.volunteer_id}, project_id_assoc: params[:project_id_assoc]})) 
  rescue => e
    log_error("COD-00007",e)
  end

  protected

    def volun_tracking_params
      params.require(:volun_tracking).permit(
          :volunteer_id,
          :tracking_type_id,
          :project_id,
          :manager_id,
          :tracked_at,
          :comments,
          :project_id_assoc
        )
    end

    alias_method :create_params, :volun_tracking_params
  
  private 


  def tracking_types_active 
    @tracking_types_active = TrackingType.all_volunteers.order(:name).select(:name,:id)
  end 

  def get_projects
    @projects=Volunteer.unassociated_projects(nil,params[:search],params[:search_id]).order(:name).select(:name,:id)
  end

end
