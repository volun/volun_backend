class Ent::TrackingsController < ApplicationController  
  load_and_authorize_resource instance_name: :ent_tracking
  respond_to :html, :js, :json
  before_action :tracking_types_active, only: [:new, :create, :update, :edit] 

  def index
    @tracking_types_active = TrackingType.all_entities.order(:name)
    @hash_tracking_types={}
    @tracking_types_active.each do |type|
      @hash_tracking_types.merge!({"#{type.name}" => type.id})
    end
    
    params[:q][:entity_id_eq] ||= params[:q][:view_list_entity_id_eq]
    params[:q] ||= View::List::EntTracking.ransack_default
    @entity = Entity.find_by(id:  params["q"].blank? ? params[:q][:entity_id_eq] : params["q"]["entity_id_eq"])

    # unless  params["search_manager"].blank?
    #   @ent_trackings=@ent_trackings.joins(:manager)
    # end
    params[:q][:s] ||= 'tracked_at desc'
    @search_q = View::List::EntTracking.search(params[:q])
    @unpaginated_ent_trackings = @search_q.result
    @unpaginated_ent_trackings=GlobalSearch.search_filter(@unpaginated_ent_trackings,
      [ ["strict",:tracking_type_id ,  params["search_tipo"]],
        ["collective",[:entity,:manager, :tracking_type, :comments], params["search_enter_text"]],
        ["ilike",:manager,  params["search_manager"]]
      ])

    #@unpaginated_ent_trackings = @unpaginated_ent_trackings.order(tracked_at: :desc)
    @ent_trackings = @unpaginated_ent_trackings.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_ent_trackings,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@ent_tracking) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    entity = Entity.find_by(id: params[:entity_id])
    @ent_tracking = entity.trackings.build
    respond_with(@ent_tracking)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @ent_tracking.tracked_at = Time.now
    AuditGenerateHelper.audit_create(@ent_tracking, current_user)
    respond_with(@ent_tracking, location: ent_trackings_path({q: { view_list_entity_id_eq: @ent_tracking.entity_id}})) 
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    @ent_tracking.tracked_at = Time.now
    AuditGenerateHelper.audit_update(@ent_tracking, ent_tracking_params, current_user)
    respond_with(@ent_tracking, location: ent_trackings_path({q: { view_list_entity_id_eq: @ent_tracking.entity_id}})) 
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@ent_tracking, current_user)
    respond_with(@ent_tracking)
  rescue => e
    log_error("COD-00007",e) 
  end

  protected
    def ent_tracking_params
      params.require(:ent_tracking).permit(
          :entity_id,
          :tracking_type_id,
          :manager_id,
          :tracked_at,
          :comments
        )
    end

    alias_method :create_params, :ent_tracking_params

  private 
  
  def tracking_types_active 
    @tracking_types_active = TrackingType.all_entities.order(:name)
  end 
end
