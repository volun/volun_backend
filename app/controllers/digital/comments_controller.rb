class Digital::CommentsController < ApplicationController
  load_and_authorize_resource class: '::Digital::Comment', except: [:create,:update]
  respond_to :html, :js, :json
  before_action :load_comment, except: [:new,:create]
  before_action :permit_access
  before_action :permit_access_topic

  def show
    respond_with(@digital_comment) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def new
    @digital_comment = Digital::Comment.new(digital_topic_id: params[:digital_topic_id])
    respond_with(@digital_comment) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def edit
    respond_with(@digital_comment) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00003",e) 
  end

  def create
    @digital_comment = Digital::Comment.new(digital_comment_params)
    @digital_comment.userable =current_user
    @digital_comment.moderate_status = ModerateStatus.find_by(code: 'accept')
    AuditGenerateHelper.audit_create(@digital_comment, current_user)
    respond_with(@digital_comment, :location => edit_digital_topic_path(@digital_comment.digital_topic_id))
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@digital_comment, digital_comment_params, current_user)
    respond_with(@digital_comment, :location =>edit_digital_topic_path(@digital_comment.digital_topic_id))
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@digital_comment, current_user)
    respond_with(@digital_comment, :location => edit_digital_topic_path(@digital_comment.digital_topic_id))
  rescue => e
    log_error("COD-00006",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@digital_comment, current_user)
    respond_with(@digital_comment, :location => edit_digital_topic_path(@digital_comment.digital_topic_id))
  rescue => e
    log_error("COD-00007",e) 
  end

  def comment_respond
    @rdigital_comment = Digital::Comment.new(digital_topic_id: @digital_comment.digital_topic_id, digital_comment_id: @digital_comment.id)
    respond_with(@rdigital_comment) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00008",e) 
  end

  def send_respond
    @rdigital_comment = Digital::Comment.new(digital_comment_params)
    @rdigital_comment.userable =current_user
    @rdigital_comment.moderate_status = ModerateStatus.find_by(code: 'accept')
    AuditGenerateHelper.audit_create(@rdigital_comment, current_user)
    respond_with(@rdigital_comment, :location => edit_digital_topic_path(@rdigital_comment.digital_topic_id))
  rescue => e
    log_error("COD-00009",e) 
  end
  
  protected

  def get_model_class
    @model_class = Digital::Comment
  end

  def digital_comment_params
    params.require(:digital_comment).permit(:id, :body, :digital_topic_id,:digital_comment_id)
  end

  def load_comment
    @digital_comment = Digital::Comment.find_by(id: params[:id])
  end

  def permit_access
    if !(can?(:read, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end

  def permit_access_topic
    if !(can?(:topic, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end
  
end
