class Digital::FollowsController < ApplicationController
  load_and_authorize_resource class: '::Digital::Follow', except: [:create,:update]
  respond_to :html, :js, :json
  before_action :load_follow, except: [:new,:create]
  before_action :load_volunteers, except: [:destroy,:recover]
  before_action :permit_access
  before_action :permit_access_topic
  
  def new
    @digital_follow = Digital::Follow.new(digital_topic_id: params[:digital_topic_id])
    respond_with(@digital_follow) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def edit
  rescue => e
    log_error("COD-00002",e) 
  end

  def create
    @digital_follow = Digital::Follow.new(digital_follow_params)
    @digital_follow.userable_type ="User"
    AuditGenerateHelper.audit_create(@digital_follow, current_user)
    respond_with(@digital_follow, :location => edit_digital_topic_path(@digital_follow.digital_topic_id))
  rescue => e
    log_error("COD-00003",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@digital_follow, digital_follow_params, current_user)
    respond_with(@digital_follow, :location => edit_digital_topic_path(@digital_follow.digital_topic_id))
  rescue => e
    log_error("COD-00004",e) 
  end

  def destroy
    aux = @digital_follow.digital_topic_id
    AuditGenerateHelper.audit_archive(@digital_follow, current_user)
    respond_with(@digital_follow, :location => edit_digital_topic_path(aux))
  rescue => e
    log_error("COD-00005",e) 
  end
  
  protected

  def get_model_class
    @model_class = Digital::Follow
  end

  def digital_follow_params
    params.require(:digital_follow).permit(:id, :userable_id, :digital_topic_id)
  end

  def load_follow
    @digital_follow = Digital::Follow.find_by(id: params[:id])
  end

  def load_volunteers
    @volunteers = Volunteer.joins("INNER JOIN projects_volunteers pv ON pv.volunteer_id=volunteers.id and pv.active=true and volunteers.active=true").where("pv.project_id in (?)",Project.where(permit_debate: true).select(:id)).order(:id_autonum).select(:id,:name,:last_name,:last_name_alt,:id_autonum)
  end

  def permit_access
    if !(can?(:read, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end

  def permit_access_topic
    if !(can?(:topic, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end
  
end
