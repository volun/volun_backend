class Digital::TopicsController < ApplicationController
  load_and_authorize_resource class: '::Digital::Topic', except: [:create,:update]
  respond_to :html, :js, :json
  before_action :load_project_permit
  before_action :load_digital_topic, except: [:index,:new,:create]
  before_action :permit_access
  before_action :permit_access_topic
  
  def index
    params[:q] ||= View::List::Digital::Topic.ransack_default
    @search_q = View::List::Digital::Topic.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_digital_topics = @search_q.result
    @unpaginated_digital_topics = GlobalSearch.search_filter(@unpaginated_digital_topics, [
        ["strict",:id,params["search_id"]],
        ["strict",:project_id,params["search_project"]],
        ["strict",:active,params["search_status"]],
        ["collective",[ :id,:title,:body ],params["search_text"]]
      ])     

    @digital_topics = @unpaginated_digital_topics.paginate(page: params["page"], per_page: params["per_page"]||15)
    
    respond_with(@digital_topics, :location => digital_topics_path) do |format|
      format.csv { stream_csv_report(@unpaginated_digital_topics,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@digital_topic) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @digital_topic = Digital::Topic.new(project_id: params[:project_id])
    @digital_topic.userable =current_user
    @digital_topic.moderate_status = ModerateStatus.find_by(code: 'accept')
    respond_with(@digital_topic, :location => digital_topics_path(search_project: @digital_topic.project_id))
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @digital_topic = Digital::Topic.new(digital_topic_params)
    AuditGenerateHelper.audit_create(@digital_topic, current_user)
    respond_with(@digital_topic, :location => digital_topics_path(search_project: @digital_topic.project_id))
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@digital_topic, digital_topic_params, current_user)
    respond_with(@digital_topic, :location => digital_topics_path(search_project: @digital_topic.project_id))
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@digital_topic, current_user)
    respond_with(@digital_topic, :location => digital_topics_path(search_project: @digital_topic.project_id))
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@digital_topic, current_user)
    respond_with(@digital_topic, :location => digital_topics_path(search_project: @digital_topic.project_id))
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = Digital::Topic
  end

  def digital_topic_params
    params.require(:digital_topic).permit(:id,:title, :body, :active,:order,:project_id)
  end

  def load_project_permit
    @projects_permited = Project.where(permit_debate: true).order(:id).select(:id,:name).map {|p| ["#{p.id}. #{p.name}",p.id]}
  end

  def load_digital_topic
    @digital_topic = Digital::Topic.find_by(id: params[:id])
  end

  def permit_access
    if !(can?(:read, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end

  def permit_access_topic
    if !(can?(:topic, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end
end
