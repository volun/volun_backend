class Digital::FaqsController < ApplicationController
  load_and_authorize_resource class: 'Digital::Faq', except: [:create,:update]
  respond_to :html, :js, :json
  before_action :load_project_permit
  before_action :load_digital_faq, except: [:index,:new,:create]
  before_action :permit_access
  before_action :permit_access_faq
  
  def index
    params[:q] ||= Digital::Faq.ransack_default
    @search_q = Digital::Faq.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_digital_faqs = @search_q.result
    @unpaginated_digital_faqs = GlobalSearch.search_filter(@unpaginated_digital_faqs, [
        ["strict",:id,params["search_id"]],
        ["strict",:project_id,params["search_project"]],
        ["strict",:active,params["search_status"]],
        ["collective",[ :id,:title,:description ],params["search_text"]]
      ])     

    @digital_faqs = @unpaginated_digital_faqs.paginate(page: params["page"], per_page: params["per_page"]||15)
    
    respond_with(@digital_faqs, :location => digital_faqs_path) do |format|
      format.csv { stream_csv_report(@unpaginated_digital_faqs,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@digital_faq) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @digital_faq = Digital::Faq.new(project_id: params[:project_id])
    respond_with(@digital_faq, :location => digital_faqs_path(search_project: @digital_faq.project_id))
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @digital_faq = Digital::Faq.new(digital_faq_params)
    AuditGenerateHelper.audit_create(@digital_faq, current_user)
    respond_with(@digital_faq, :location => digital_faqs_path(search_project: @digital_faq.project_id))
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@digital_faq, digital_faq_params, current_user)
    respond_with(@digital_faq, :location => digital_faqs_path(search_project: @digital_faq.project_id))
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@digital_faq, current_user)
    respond_with(@digital_faq, :location => digital_faqs_path(search_project: @digital_faq.project_id))
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@digital_faq, current_user)
    respond_with(@digital_faq, :location => digital_faqs_path(search_project: @digital_faq.project_id))
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = Digital::Faq
  end

  def digital_faq_params
    params.require(:digital_faq).permit(:id,:title, :description, :active,:order,:project_id)
  end

  def load_project_permit
    @projects_permited = Project.where(permit_faq: true).order(:id).select(:id,:name).map {|p| ["#{p.id}. #{p.name}",p.id]}
  end

  def load_digital_faq
    @digital_faq = Digital::Faq.find_by(id: params[:id])
  end

  def permit_access
    if !(can?(:read, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end

  def permit_access_faq
    if !(can?(:faq, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end
end
