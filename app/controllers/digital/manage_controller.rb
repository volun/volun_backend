class Digital::ManageController < ApplicationController
  before_action :load_permit_project, only: [:moderate]
  before_action :load_element, except: [:index, :moderate]
  before_action :permit_access
  before_action :permit_access_moderate, except: [:index]

  def index
  rescue => e
    log_error("COD-00001",e) 
  end
  
  def moderate
    @list_topics =[]
    @list_comments = []
    params[:type] ||= "pending"
    if !params[:search_moderate].blank?
      if params[:search_moderate]=="topics"
        @list_topics = Digital::Topic.where("#{"moderate_status_id is null or" if params[:type]=="pending"} moderate_status_id in (?)", ModerateStatus.find_by(code: params[:type]).try(:id))
      elsif params[:search_moderate]=="comments"          
        @list_comments = Digital::Comment.where("#{"moderate_status_id is null or" if params[:type]=="pending"} moderate_status_id in (?)", ModerateStatus.find_by(code: params[:type]).try(:id))
      end  
    else
      @list_topics = Digital::Topic.where("#{"moderate_status_id is null or" if params[:type]=="pending"} moderate_status_id in (?)", ModerateStatus.find_by(code: params[:type]).try(:id))
      @list_comments = Digital::Comment.where("#{"moderate_status_id is null or" if params[:type]=="pending"} moderate_status_id in (?)", ModerateStatus.find_by(code: params[:type]).try(:id))
    end

    if !params[:search_project].blank?
      if !@list_topics.blank?
        @list_topics = @list_topics.where(project_id: params[:search_project])
      end

      if !@list_comments.blank?
        @list_comments = @list_comments.where("digital_topic_id  in (?)", Digital::Topic.where(project_id: params[:search_project]).select(:id))
      end
    end

    if !params[:search_text].blank?
      if !@list_topics.blank?
        @list_topics = GlobalSearch.search_filter( @list_topics, [          
          ["collective",[ :title,:body ],params["search_text"]]
        ])   
      end

      if !@list_comments.blank?
        @list_comments = GlobalSearch.search_filter( @list_comments, [          
          ["collective",[ :body ],params["search_text"]]
        ]) 
      end
    end
    
    @list_moderates = @list_topics + @list_comments

    @list_moderates = @list_moderates.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_to do |format|
      format.csv { moderation_stream_csv_report(@list_moderates,params)  }
      format.html
    end
  # rescue => e
  #   log_error("COD-00002",e) 
  end

  def edit_moderate
  rescue => e
    log_error("COD-00003",e) 
  end

  def update_moderate
    @element.moderate_status = ModerateStatus.find_by(code: 'edit')
    @element.moderable = current_user
    if params[:type] == "topic"
      @element.assign_attributes(digital_topic_params)
    elsif params[:type]== "comment"
      @element.assign_attributes(digital_comment_params)
    end

    if @element.save
      redirect_to digital_moderate_path(type: params[:moderate_type]), notice: I18n.t("digital_manage.moderate.notices.update")
    else
      render :edit_moderate
    end
  rescue => e
    log_error("COD-00004",e) 
  end

  def accept_moderate
    @element.moderate_status = ModerateStatus.find_by(code: 'accept')
    @element.moderable = current_user
    if @element.save
      redirect_to digital_moderate_path(type: params[:moderate_type]), notice: I18n.t("digital_manage.moderate.notices.accept")
    else
      redirect_to digital_moderate_path(type: params[:moderate_type]), alert: I18n.t("digital_manage.moderate.alerts.accept")
    end
  rescue => e
    log_error("COD-00005",e) 
  end

  def reject_moderate
    @element.moderate_status = ModerateStatus.find_by(code: 'reject')
    @element.moderable = current_user
    if @element.save
      redirect_to digital_moderate_path(type: params[:moderate_type]), notice: I18n.t("digital_manage.moderate.notices.reject")
    else
      redirect_to digital_moderate_path(type: params[:moderate_type]), alert: I18n.t("digital_manage.moderate.alerts.reject")
    end
  rescue => e
    log_error("COD-00006",e) 
  end
  
  private

  def digital_topic_params
    params.require(:digital_topic).permit(:id,:title, :body)
  end

  def digital_comment_params
    params.require(:digital_comment).permit(:id, :body)
  end

  def load_permit_project
    @projects_permited= Project.where(permit_debate: true).order(:id).select(:id,:name).map {|p| ["#{p.id}. #{p.name}",p.id]}
  end

  def load_element
    if params[:type]=="topic"
      @element = Digital::Topic.find_by(id: params[:id])
    elsif params[:type]=="comment"
      @element = Digital::Comment.find_by(id: params[:id])
    else
      redirect_to digital_moderate_path, alert: I18n.t("digital_manage.moderate.alerts.reject")
    end
  end

  def permit_access
    if !(can?(:read, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end

  def permit_access_moderate
    if !(can?(:moderate, Digital::Manage))
      redirect_to root_path, alert: "No tienes acceso a esta sección"
    end
  end
end
  