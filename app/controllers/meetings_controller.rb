class MeetingsController < ApplicationController
    include MeetingManager
    load_and_authorize_resource
    respond_to :html, :js, :csv

    def index
        @project_id = params[:project_id]
        @project = Project.find(@project_id)
        @derivaties = params[:derivaties]
        params[:q] ||= Meeting.ransack_default
        params[:q].each do |key, value|
        params[:q].delete(key) if value.blank?
        end
        
        params[:q] = Meeting.ransack_default if params[:q].blank?
        @search_q = @meetings.search(params[:q])
        @search_q.sorts ||= 'name asc'

        @meetings = @search_q.result
        @meetings = @meetings.where(project_id: params[:project_id]) unless params[:project_id].blank?


        respond_with(@meetings)
    rescue => e 
        log_error("COD-00001",e)
        redirect_to projects_path, alert: "Se ha perdido el enlace al proyecto"
    end
    
    def show
        respond_with(@meeting) do |format|
            format.js
            format.html
            format.csv { stream_csv_report(@meeting, {}) }
        end
    end

    def new
        @meeting = Meeting.new
        respond_with(@meeting)
    end

    def edit
    end

    def create

        if AuditGenerateHelper.audit_create(@meeting, current_user)
            MeetingManager.generate_volun_proy_meeting(@meeting)
            redirect_to meetings_path(project_id: @meeting.project_id, derivaties: params[:derivaties],pt_extendable_id: params[:pt_extendable_id], asociated: params[:asociated]), notice: "Se ha creado correctamente la reunión"
        else
            respond_with(@meeting)
        end
    end

    def update
        if AuditGenerateHelper.audit_update(@meeting, meeting_params, current_user)
            MeetingManager.generate_volun_proy_meeting(@meeting)
            redirect_to meetings_path(project_id: @meeting.project_id, derivaties: params[:derivaties],pt_extendable_id: params[:pt_extendable_id], asociated: params[:asociated]), notice: "Se ha actualizado correctamente la reunión"
        else
            respond_with(@meeting)
        end
    end

    protected
        def meeting_params
            params.require(:meeting).permit(:project_id, :id, :name, :date, :hour, :place, :type_meeting_id)
        end
end
