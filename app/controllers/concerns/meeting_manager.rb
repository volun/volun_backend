module MeetingManager
    extend ActiveSupport::Concern
    
    def self.generate_volun_proy_meeting(meeting)
        voluns_proys= ProjectsVolunteer.where(project: meeting.project)
        voluns_proys.each do |vp|
            if vp.volun_proy_meetings.blank?
                volun_proy_m = VolunProyMeeting.create(projects_volunteer: vp, meeting: meeting)
                vp.volun_proy_meetings << volun_proy_m
                vp.save
            else
                aux = false
                vp.volun_proy_meetings.each do |vpm|
                    if vpm.meeting.id == meeting.id
                        aux=true 
                        break
                    end
                end
                unless aux
                    volun_proy_m = VolunProyMeeting.create(projects_volunteer: vp, meeting: meeting)
                    vp.volun_proy_meetings << volun_proy_m
                    vp.save
                end
            end
        end
    end
end