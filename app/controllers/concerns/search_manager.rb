class SearchManager

    def load_search(model, params, current_user, rt = false)
        params_search = nil
        aux_params = {}
        params.each do |key, value|
            aux_params = aux_params.merge!({key => value}) if key.to_s != "controller" && key.to_s != "action" && key.to_s != "per_page_list" && key.to_s != "per_page" && !(key.to_s === "search_appointed" || key.to_s === "search_pending" || key.to_s === "search_processing"|| key.to_s === "search_approved" || key.to_s === "search_rejected" || key.to_s === "pending" || key.to_s === "processing" || key.to_s === "approved" || key.to_s=== "rejected" || key.to_s === "appointed" )
        end
        search = SearchFormModule.find_module(current_user,model) 
        if aux_params.blank? && !search.blank?
            add_pagination(params).each do |key, value|
                search.content.merge!({key=>value})
            end
            if rt
                add_rt(params).each do |key, value|
                    search.content.merge!({key=>value})
                end
            end
            params_search = search.content 
        else
            add_pagination(params).each do |key, value|
               aux_params.merge!({key=>value})
            end
            if rt
                add_rt(params).each do |key, value|
                    aux_params.merge!({key=>value})
                end
            end
            if aux_params["clean"].blank?
                if search.blank?
                    SearchFormModule.generate(current_user.id,aux_params,model)
                else
                    SearchFormModule.update(current_user,aux_params,model)
                end
                params_search = aux_params
            else
               
                SearchFormModule.delete(current_user,model) 
                params_search = aux_params
            end
        end
        params_search
    end

    def add_pagination(params)
        add_pagination = {}
        params.each do |key, value|
            add_pagination.merge!({key => value}) if key.to_s === "per_page_list" || key.to_s === "per_page"|| key.to_s === "page"
        end
        add_pagination
    end


    def add_rt(params)
        add_rt = {}
        params.each do |key, value|
            add_rt.merge!({key => value}) if key.to_s === "search_appointed" || key.to_s === "search_pending" || key.to_s === "search_processing"|| key.to_s === "search_approved" || key.to_s === "search_rejected"
        end
        add_rt
    end

end