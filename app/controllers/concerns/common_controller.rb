module CommonController
    extend ActiveSupport::Concern   
  
    included do 
        respond_to :html, :js, :json, :csv

        protected

        
        def array_first_type_projects
            [Pt::Subvention,Pt::Centre,Pt::Entity,Pt::Social,Pt::Other,Pt::RetiredVolunteer,Pt::Volunteer,Pt::Municipal ]
        end

        def set_districts
            @hash_distritos={}
            District.where(active: true).select(:name,:id).order(:name).each {|n| @hash_distritos.merge!({n.name=>n.id})}      
        rescue
            {}
        end

        def set_districts_full
            @districts= District.where(active: true).select(:name,:id).order(:name)     
        rescue
            {}
        end

        def set_areas
            @ambitos_proyecto=Area.where(active: true).order(:name).select("areas.name")    
        rescue
            {}
        end

        def set_project_types(array)
            @hash_tipos={}
            array.each {|n|  @hash_tipos.merge!({n.model_name.human(count: 2) => n.to_s })}    
            @hash_tipos = @hash_tipos.sort.to_h   
        rescue
            {}
        end

        def set_collectives
            @colectivos_proyecto=Collective.where(active: true).order(:name).select("collectives.name")
        rescue
            {}
        end

        def set_channels
            @channels=Channel.where(active: true).order(:name).select(:id,:name)
        rescue
            {}
        end

        def set_entities
            @hash_entidades={}
            Entity.all.order(:name).distinct.select(:name,:id).each {|n|  @hash_entidades.merge!({n.name=>n.id})}
        rescue
            {}
        end

        def set_entities_organizations
            @EntityOrganizations = EntityOrganization.all.order(:name).select(:name,:id)
        rescue
            {}
        end    

        def set_subtipes_pt
            @hash_subtypes_pt = {}
            ["permanent", "punctual"].each {|n| @hash_subtypes_pt.merge!({t("pt_types.#{n}")=>n})}
        rescue
            {}
        end

        def set_subtipes_project
            @hash_subtypes_proyect = {}
            ["subproyect", "matriz"].each {|n| @hash_subtypes_proyect.merge!({t("pt_types.#{n}")=>n})}
        rescue
            {}
        end

        def set_projects_matriz
           @proyects_matriz= Project.where(subtype_proyect: "matriz").select(:name,:id).order(:name)
        rescue
            {}
        end

        def set_next_points
            @hash_next_points = {}
            NextPoint.all.order(:name).each {|n| @hash_next_points.merge!({n.name=>n.id})}
        rescue
            {}
        end

        def set_volun_profile
            @volun_profiles_names = VolunProfile.all.select(:name,:id).order(:name)
        rescue
            {}
        end

        def set_info_sources
            @info_sources =InfoSource.all.select(:name,:id).order(:name)
        rescue
            {}
        end

        def set_statuses
            @status = {}
            Status.all.order(:name).each {|n| @status.merge!({n.name=>n.id})} 
        rescue
            {}
        end 
        
        def get_cv_environment
            if Rails.env.production?
                "https://voluntariospormadrid.madrid.es/"
            elsif Rails.env.preproduction?
                "https://voluntariadopre.madrid.es/"
            else
                "#{root_url}"
            end
        end
    end
end