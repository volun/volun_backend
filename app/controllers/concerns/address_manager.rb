module AddressManager
    extend ActiveSupport::Concern   
  
    included do 
        protected

        def address_attributes
            [
                :id,
                :road_type,
                :road_name,
                :road_number_type,
                :road_number,
                :grader,
                :stairs,
                :floor,
                :door,
                :postal_code,
                # :borought,
                # :borought_id,
                # :district,
                # :district_id,
                :town,
                :province_id,
                :province,
                :country,
                :normalize,
                :_destroy
            ]
        end
    end
end