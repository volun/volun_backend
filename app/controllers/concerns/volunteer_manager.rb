class VolunteerManager
  include MeetingManager
  attr_accessor :errors, :rt_volunteer_subscribe,:rt_volunteer_amendment, :request_form, :volunteer, :volunteer_projects_relations

  def initialize(options = {})
    @rt_volunteer_subscribe_id   = options[:rt_volunteer_subscribe_id]
    @rt_volunteer_unsubscribe_id = options[:rt_volunteer_unsubscribe_id]
    @rt_volunteer_amendment_id   = options[:rt_volunteer_amendment_id]
    @volunteer_attributes        = options[:volunteer_attributes] || {}
    @volunteer                   = options[:volunteer]
    @manager_id                  = options[:manager_id]
    @errors                      = []
  end

  def valid?
    add_request_form_no_longer_exists_error if creation_through_request_form? && request_form.blank?
    errors.blank?
  end

  def build_volunteer(volunteer_attrs = nil)
    rt_v_attributes=rt_volunteer_subscribe.try(:volunteer_attributes)
    unless @rt_volunteer_subscribe_id.blank? 
      rt_v_attributes.each do |key,value| 
        if key.to_s=="district" || key.to_s=="postal_code" || key.to_s == "phone_number_specific"
          rt_v_attributes.delete(key)
        end
      end
    end

    Volunteer.new(volunteer_attrs || rt_v_attributes || {})
  end
  
  def upd_volunteer(volunteer, volunteer_attrs = nil)
    volunteer.update(volunteer_attrs || rt_volunteer_amendment.try(:volunteer_attributes) || rt_volunteer_unsubscribe.try(:volunteer_attributes) || {})
  end

  def creation_through_request_form?
    @rt_volunteer_subscribe_id.present?
  end
  
  def update_through_request_form?
    @rt_volunteer_unsubscribe_id.present? || @rt_volunteer_amendment_id.present?
  end

  def create_volunteer(current_user)
    return unless valid?
    self.volunteer = build_volunteer(@volunteer_attributes.merge manager_id: @manager_id, subscribe_date: Time.now.strftime("%d/%m/%Y"))
    if self.volunteer.age.to_i >=18
      self.volunteer.is_adult="true"    
    end

    ActiveRecord::Base.transaction do
      if AuditGenerateHelper.audit_create(volunteer, current_user)
        volunteer.update_attribute(:id_autonum, Volunteer.maximum("id_autonum")+1)
        create_derivaties_volunteers(volunteer)
        assign_user_to_volunteer!
        register_tracking!(
          tracking_type: TrackingType.get_volunteer_subscribe,
          comments:      I18n.t('trackings.volunteer_subscribe')
        )
        approve_request_form! if creation_through_request_form?
      else
        copy_errors_from!(volunteer)
      end
    end
    errors.blank?
  end
  
  def update_volunteer(current_user)
    return unless valid?
    ActiveRecord::Base.transaction do
      all_projects=[]
      input_projects_ids=[]
      unless @volunteer_attributes[:project_ids].blank?
        old_project_ids = volunteer.project_ids.map {|x| x.blank? ? nil : x.to_i}.compact
        new_projects_ids=@volunteer_attributes[:project_ids].map {|x| x.blank? ? nil : x.to_i}.compact
        all_projects = (old_project_ids+new_projects_ids).uniq
        input_projects_ids = all_projects-old_project_ids
        @volunteer_attributes[:project_ids]= all_projects
      end

      if self.volunteer.age.to_i >=18
        self.volunteer.is_adult="true"  
        @volunteer_attributes=@volunteer_attributes.except(:is_adult)  
      end

      @volunteer_attributes[:derivaties_volunteer_ids] = update_derivaties_volunteers(volunteer,@volunteer_attributes[:derivaties_volunteer_ids]) unless @volunteer_attributes[:derivaties_volunteer_ids].blank?
      if AuditGenerateHelper.audit_update(volunteer, @volunteer_attributes, current_user)
      
        usuario=User.find_by(loggable_id: @volunteer.id,loggable_type: "Volunteer")
        if !usuario.blank? && !volunteer.email.blank?
          copy_errors_from!(volunteer)   unless usuario.update!(email: volunteer.email.downcase, mobile_phone: volunteer.phone_number_alt)
        end
        input_projects_ids.each do |p|
          begin
            pvolunteer = ProjectsVolunteer.find_or_initialize_by(project_id: p, volunteer_id: volunteer.id)
            pvolunteer.active = true
            pvolunteer.subscribe_date = Time.now.strftime("%d/%m/%Y").to_date
            pvolunteer.volunteer_type = "DERIVATIE"
            if pvolunteer.save(validate: false)
              create_volun_proy_meetings(p)
              register_tracking_project_volunteer(project: p, proy_active: pvolunteer.active, volunteer_type: pvolunteer.volunteer_type)
            end
            begin
              Rails.logger.error("ERROR-PV: assing-errors -> #{pvolunteer.errors.full_messages}")
            rescue
            end
          rescue => e
            begin
              Rails.logger.error("ERROR-PV: assing -> #{e}")
            rescue
            end
          end
        end

        (all_projects-input_projects_ids).each do |p|
          begin
            pvolunteer = ProjectsVolunteer.find_by(project_id: p, volunteer_id: volunteer.id)
            activo = pvolunteer.try(:active).blank? || !pvolunteer.active ? false : true
            cambio = false

            if !activo && new_projects_ids.include?(p)
              pvolunteer.active = true
              pvolunteer.subscribe_date = Time.now.strftime("%d/%m/%Y").to_date
              cambio = true
            elsif activo && !new_projects_ids.include?(p)
              pvolunteer.active = false
              pvolunteer.unsubscribe_date = Time.now.strftime("%d/%m/%Y").to_date
              cambio = true
            end
            if cambio && pvolunteer.save(validate: false)
              create_volun_proy_meetings(p)
              register_tracking_project_volunteer(project: p, proy_active: pvolunteer.active, volunteer_type: pvolunteer.volunteer_type)
            end
            begin
              Rails.logger.error("ERROR-PV: assing-errors -> #{pvolunteer.errors.full_messages}")
            rescue
            end
          rescue => e
            begin
              Rails.logger.error("ERROR-PV: assing -> #{e}")
            rescue
            end
          end
        end
        if update_through_request_form?
          approve_request_form! 
          register_update_volunteer if  errors.blank?
        end
      else
        copy_errors_from!(volunteer)
      end
    end
    errors.blank?
  end

  def create_volun_proy_meetings(project_id)
    meetings = Meeting.where(project_id: project_id)
    meetings.each do |m|
      MeetingManager.generate_volun_proy_meeting(m)
    end
  end

  def register_tracking_project_volunteer(attributes={})
    if attributes[:volunteer_type].to_s == "DERIVATIE"
      if attributes[:proy_active]
        VolunteerProjectsRelation.create(project_id: attributes[:project], volunteer_id: volunteer.id, type_relation: attributes[:volunteer_type], start_date: Date.today, end_date: nil)
        register_tracking!(
          project_id:    attributes[:project],
          tracking_type: TrackingType.get_derivatie,
          comments:      attributes[:comments].blank? ? I18n.t('trackings.derivatie') : attributes[:comments]
        )
      elsif !attributes[:proy_active]
        derivatie_relation = VolunteerProjectsRelation.find_by(project_id: attributes[:project], volunteer_id: attributes[:volunteer_id], end_date: nil)
        unless derivatie_relation.blank?
          derivatie_relation.end_date = Date.today
          derivatie_relation.save
        end
        register_tracking!(
          project_id:    attributes[:project],
          tracking_type: TrackingType.get_down_derivatie,
          comments:      attributes[:comments].blank? ? I18n.t('trackings.down_derivatie') : attributes[:comments]
        )
      end
    elsif attributes[:proy_active]

      derivatie_relation = VolunteerProjectsRelation.find_by(project_id: attributes[:project], volunteer: volunteer, end_date: nil)
      unless derivatie_relation.blank?
        derivatie_relation.end_date = Date.today
        derivatie_relation.save
      end
      VolunteerProjectsRelation.create(project_id: attributes[:project], volunteer_id: volunteer.id, type_relation: attributes[:volunteer_type], start_date: Date.today, end_date: nil)
      register_tracking!(
        project_id:    attributes[:project],
        tracking_type: TrackingType.get_project_subscribe,
        comments:      attributes[:comments].blank? ? I18n.t('trackings.subscribe_to_project') : attributes[:comments]
      )
    elsif !attributes[:proy_active]
      derivatie_relation = VolunteerProjectsRelation.find_by(project_id: attributes[:project], volunteer: volunteer, end_date: nil)
      unless derivatie_relation.blank?
        derivatie_relation.end_date = Date.today
        derivatie_relation.save
      end
      register_tracking!(
        project_id:    attributes[:project],
        tracking_type: TrackingType.get_project_unsubscribe,
        comments:      attributes[:comments].blank? ? I18n.t('trackings.unsubscribe_from_project') : attributes[:comments]
      )
    end
  end

  def register_project_volunteer(attributes={})
    pv =ProjectsVolunteer.find_by(project_id: attributes[:project], volunteer_id: @volunteer.id)
    ActiveRecord::Base.transaction do 
      register_tracking_project_volunteer(project: pv.project_id, proy_active: pv.active, volunteer_type: pv.volunteer_type, comments: attributes[:input_comments])
    end 

    errors.blank? 
  end 

  def register_confirm_volunteer(project,vm)
    ActiveRecord::Base.transaction do
      register_tracking!(
        project_id:    project.id,
        tracking_type: TrackingType.get_confirm_derivaty,
        comments:      I18n.t('trackings.confirm_volunteer', volun_meeting: vm)
      )
    end
    errors.blank?
  end

  def register_not_confirm_volunteer(project,vm)
    ActiveRecord::Base.transaction do
      register_tracking!(
        project_id:    project.id,
        tracking_type: TrackingType.get_not_confirm_derivaty,
        comments:      I18n.t('trackings.not_confirm_volunteer', volun_meeting: vm)
      )
    end
    errors.blank?
  end

  def register_asist_volunteer(project,vm)
    ActiveRecord::Base.transaction do
      register_tracking!(
        project_id:    project.id,
        tracking_type: TrackingType.get_asist_derivaty,
        comments:      I18n.t('trackings.asist_volunteer', volun_meeting: vm)
      )
    end
    errors.blank?
  end

  def register_not_asist_volunteer(project,vm)
    ActiveRecord::Base.transaction do
      register_tracking!(
        project_id:    project.id,
        tracking_type: TrackingType.get_not_asist_derivaty,
        comments:      I18n.t('trackings.not_asist_volunteer', volun_meeting: vm)
      )
    end
    errors.blank?
  end

  def register_send_email(attributes={})
    ActiveRecord::Base.transaction do
      register_tracking!(
        tracking_type: TrackingType.get_volunteer_send_mail,
        comments:      "#{attributes[:title]} (#{Date.today})"
      )
    end

  end

  def register_generate_passwod_volunteer(attributes={})
    ActiveRecord::Base.transaction do
      register_tracking!(
        tracking_type: TrackingType.get_volunteer_generate_password,
        comments:      I18n.t('trackings.send_password', fecha: Date.today)
      )
    end

  end
  
  def register_destroy_volunteer(attributes={})
    if !volunteer.active
      ActiveRecord::Base.transaction do
        projects_unsubscribe=Project.joins("INNER JOIN projects_volunteers ON projects_volunteers.project_id=projects.id AND projects_volunteers.volunteer_id=#{volunteer.id} AND projects_volunteers.active = true").all.select(:id).uniq
        projects_unsubscribe.each do |p|

          register_tracking!(
            project_id:    p.id,
            tracking_type: TrackingType.get_project_unsubscribe,
            comments:      (attributes[:input_comments] unless attributes[:input_comments].blank?)||I18n.t('trackings.unsubscribe_from_project')
          )
        end
        

        register_tracking!(
          tracking_type: TrackingType.get_volunteer_unsubscribe,
          comments:      (attributes[:input_comments] unless attributes[:input_comments].blank?) || I18n.t('trackings.unsubscribe_from_project')
        )
      end
    end

    errors.blank?
  end

  def register_recover_volunteer(attributes={})
    if volunteer.active
      ActiveRecord::Base.transaction do
        projects_subscribe=Project.joins("INNER JOIN projects_volunteers ON projects_volunteers.project_id=projects.id AND projects_volunteers.volunteer_id=#{volunteer.id} AND projects_volunteers.active = false").all.select(:id).uniq
        projects_subscribe.each do |p|
          register_tracking!(
            project_id:    p.id,
            tracking_type: TrackingType.get_project_subscribe,
            comments:      (attributes[:input_comments] unless attributes[:input_comments].blank?)||I18n.t('trackings.subscribe_to_project')
          )
        end

        register_tracking!(
          tracking_type: TrackingType.get_volunteer_subscribe,
          comments:      (attributes[:input_comments] unless attributes[:input_comments].blank?)||I18n.t('trackings.volunteer_subscribe')
        )
      end
    end
    errors.blank?
  end

  def register_update_volunteer(attributes={})
    ActiveRecord::Base.transaction do
      register_tracking!(
        tracking_type: TrackingType.get_volunteer_amendment,
        comments:      I18n.t('trackings.volunteer_amendment')
      )
    end
    errors.blank?
  end
  
  def register_unsubscribe(attributes = {})
    ActiveRecord::Base.transaction do
      register_tracking!(attributes)
    end
  end

  def register_tracking!(attributes = {})
    default_attrs = {
      volunteer:    volunteer,
      request_form: request_form,
      manager_id:   @manager_id,
      tracked_at:   DateTime.now,
      automatic:    true
    }

    
    tracking = Volun::Tracking.new(default_attrs.merge attributes)
    copy_errors_from!(tracking) unless tracking.save
    tracking
  end

  def rt_volunteer_subscribe
    @rt_volunteer_subscribe ||= Rt::VolunteerSubscribe.find_by(id: @rt_volunteer_subscribe_id.to_i)
  end
  
  def rt_volunteer_unsubscribe
    #@rt_volunteer_unsubscribe ||= Rt::VolunteerUnsubscribe.find_by(id: @rt_volunteer_unsubscribe_id.to_i)
  end

  def rt_volunteer_amendment
    @rt_volunteer_amendment ||= Rt::VolunteerAmendment.find_by(id: @rt_volunteer_amendment_id.to_i)
  end

  def request_form
    @request_form ||= if rt_volunteer_subscribe.present?
                        rt_volunteer_subscribe.try(:request_form)
                      elsif rt_volunteer_unsubscribe.present?
                        rt_volunteer_unsubscribe.try(:request_form)
                      elsif rt_volunteer_amendment.present?
                        rt_volunteer_amendment.try(:request_form)
                      end
  end

  def assign_user_to_volunteer!
    user = User.new(login: "uservolunteer#{'%09d' % volunteer.id}", loggable: volunteer, notice_type: NoticeType.email.take)
    user.password = generate_new_password(user)
    user.password_confirmation = user.password

    # TODO Remove this line after removing email column from users
    # if @rt_volunteer_subscribe_id.present?
    #   user.email = volunteer.email Rt::VolunteerSubscribe.find(@rt_volunteer_subscribe_id).email
    # else
      if volunteer.email.blank?
        user.email = "uservolunteer#{'%09d' % volunteer.id}.volunteer@volun.es".downcase
      else
        user.email = volunteer.email.downcase
      end
    # end
    copy_errors_from!(user) unless user.save

    user
  end

  def approve_request_form!
    unless request_form.update_and_trace_status(:approved, manager_id: @manager_id, user_id: volunteer.user.id)
      copy_errors_from!(request_form)
    end
  end

  def respond_with_volunteer?
    return true if volunteer.errors.present?
    if rt_volunteer_subscribe
      volunteer.persisted?
    elsif rt_volunteer_subscribe
      volunteer.updated_at_changed?
    else
      true
    end
  end

  private

  def update_derivaties_volunteers(volunteer, derivaties)
    exist_derivates = []
    derivaties_aux = []
    derivaties.each do |id|
      unless id.blank?
        exist = volunteer.derivaties_volunteers.find_by(project_id: id, volunteer_id: volunteer.id)
        unless exist.blank?
          exist.active=true
          exist_derivates.push(exist.id)
        else
          project = Project.find(id)
          derivaty = DerivatiesVolunteer.new
          derivaty.volunteer = volunteer
          derivaty.project = project
          derivaty.volunteer_type = "DERIVATIE"
          volunteer.derivaties_volunteers << derivaty
        end
      end
    end
    volunteer.derivaties_volunteers.each do |der|
      derivaties_aux.push(der.id)
      dentro = false
      exist_derivates.each do |ex|
        if ex.to_i == der.id.to_i
          detro = true
          break
        end
      end
      unless dentro
        der.active = false
      end
    end
    derivaties_aux
  end

  def create_derivaties_volunteers(volunteer)
    ProjectsVolunteer.where(volunteer: volunteer).each do |derivatie|
      derivatie.active = true
      derivatie.subscribe_date = Time.now.strftime("%d/%m/%Y").to_date
      derivatie.volunteer_type = "DERIVATIE"
      if derivatie.save
        create_volun_proy_meetings(derivatie.project)
        register_tracking_project_volunteer(project: derivatie.project, proy_active: derivatie.active, volunteer_type: derivatie.volunteer_type)
      end
    end
  end

  def add_request_form_no_longer_exists_error
    errors << I18n.t('errors.request_form_no_longer_exists', request_type: Rt::VolunteerSubscribe.model_name.human)
    nil
  end

  def copy_errors_from(record)
    self.errors += record.errors.full_messages
    nil
  end

  def copy_errors_from!(record)
    copy_errors_from(record)
    raise ActiveRecord::Rollback
  end

  def generate_new_password(user)
    Digest::SHA1.hexdigest("#{volunteer.created_at.to_s}--#{user.login}")[0,8].upcase
  end

end