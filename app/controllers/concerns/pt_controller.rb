module PtController
  extend ActiveSupport::Concern

  included do   
    include CommonController
    include AddressManager

    before_action :load_data, only: [:new,:edit,:create,:update,:index]
    before_action :change_entity_vat_number, only: [:new,:create, :edit,:update]    

    protected


    def set_suspended_pt(old_sus_project,new_project)
      if old_sus_project != new_project.suspended
        if new_project.suspended.to_s=="true"
          AuditGenerateHelper.audit_archive(new_project, current_user, true)
        else
          AuditGenerateHelper.audit_recover(new_project, current_user, true)
        end
      end
    end

    def update_project(option_class)
      if !option_class.project.execution_end_date.blank? && option_class.project.execution_end_date>=Date.today && option_class.project.active
        option_class.project.update_attribute(:review_end_project, false)
      end
    end

    def change_entity_vat_number_process(params, vat_number)       
      unless params[:entity_vat_number].blank?
        return Entity.find(params[:entity_vat_number]).vat_number
      else
        return vat_number
      end
    end

    def track_custom(manager, pro_id, type, comment=nil) 
      create_project_tracking(type)
      if !pro_id.blank?
        Pro::Tracking.create!(tracking_type: TrackingType.all_projects.find_by(alias_name: type),manager_id: manager,project_id: pro_id, automatic: true, comments: comment.blank? ? I18n.t("#{type}_project_tracking") : comment, tracked_at: Time.now)
        project = Project.find_by(id: pro_id)        
        if project.subtype_proyect == "subproyect"
          Pro::Tracking.create!(tracking_type: TrackingType.all_projects.find_by(alias_name: type), manager_id: manager,project_id: project.father_id, automatic: true, comments: comment.blank? ? I18n.t("#{type}_project_tracking") : comment, tracked_at: Time.now)
        end
      end
    rescue => e
    end

    def create_project_tracking(type)
      TrackingType.find_or_create_by(name: I18n.t("tracking_type.#{type}"),alias_name: type, active: true, tracking_types_type: "Project")
    end

    def load_data
      set_districts
      set_subtipes_project
      set_projects_matriz
      set_subtipes_pt
    end

    def set_pt_order(matriz, subproject,current_user,old_father=nil)
      if !matriz.blank? && subproject.subtype_proyect == "subproyect"
        finish = 0
        if !old_father.blank? && matriz.id !=old_father.id
          # track_custom(current_user.loggable_id, matriz.id, 'subproject', "Se ha asignado el subproyecto #{subproject.id} al proyecto matriz #{matriz.id}")
          track_custom(current_user.loggable_id, old_father.id, 'unsubproject', "Se ha dado de baja el subproyecto #{subproject.id} en el proyecto matriz #{old_father.id}")
          track_custom(current_user.loggable_id, subproject.id, 'unsubproject', "Se ha dado de baja el subproyecto #{subproject.id} en el proyecto matriz #{old_father.id}")
          track_custom(current_user.loggable_id, subproject.id, 'subproject', "Se ha asignado el subproyecto #{subproject.id} al proyecto matriz #{matriz.id}")
          # VOLUN-511 Comentar siguientes 4 lineas
          # Project.find_by(id: old_father.id).subprojects.order(:pt_order).each_with_index do |p,i|
          #   p.pt_order = i+1
          #   p.save(validate: false)           
          # end
        end
       
        
        track_custom(current_user.loggable_id, subproject.id, 'subproject', "Se ha asignado el subproyecto #{subproject.id} al proyecto matriz #{matriz.id}") if subproject.pt_order.blank?
        # VOLUN-511 Comentar siguientes 5 lineas
        # Project.find_by(id: matriz.id).subprojects.where("id != ?",subproject.id).order(:pt_order).each_with_index do |p,i|
        #   p.pt_order = i+1
        #   p.save(validate: false)
        #   finish = i+1
        # end
        if old_father.blank? || matriz.id !=old_father.id || subproject.pt_order.blank?
          finish = Project.find_by(id: matriz.id).subprojects.where("id not in (?)", subproject.id).maximum(:pt_order).to_i  
               
          finish= finish+1
        else
          finish = subproject.pt_order
        end
      else
        track_custom(current_user.loggable_id, subproject.id, 'matriz')
        finish=nil
      end
      subproject.pt_order=finish
      subproject.save(validate: false)
    rescue
    end

    def project_attributes
      [
        :id,
          :name,
          :permit_debate,
          :permit_faq,
          :year,
          :description,
          :area_departament,
          :number_of_activities,
          :number_of_actions,
          :volunteers_allowed,
          :publish,
          :outstanding,
          :urgent,
          :district,
          :district_id,
          :suspended,
          :pt_subvention,
          :pt_extendable_id,
          :pt_extendable_type,
          :active,
          :publish_chart,
          :comments,
          :beneficiaries_num,
          :volunteers_num,
          :participants_num,
          :functions,
          :insured,
          :insurance_date,
          :contact_name,
          :contact_last_name,
          :contact_last_name_alt,
          :email,
          :phone_number,
          :contact_name1,
          :contact_last_name1,
          :contact_last_name_alt1,
          :email1,
          :phone_number1,
          :contact_name2,
          :contact_last_name2,
          :contact_last_name_alt2,
          :email2,
          :phone_number2,
          :entity_id,
          :review_end_project,
          :execution_start_date,
          :execution_end_date,
          :subtype_proyect,
          :father_id,
          :father,
          :subtype_pt,
          :pt_order,
          :accompany,
          { images_attributes: [ :link_type_id, :description,:_destroy,:id]},
          { area_ids:         [] },
          { collective_ids:   [] },
          { coordination_ids: [] },
          {
            entities_attributes: [
              :vat_number
            ]
          },
          {
            documents_attributes: [
              :id,
              :name,
              :_destroy
            ]
          },
          {
            logo_attributes: [
              :id,
              :link_type_id,
              :file,
              :_destroy
            ]
          },
          {
            videos_attributes: [
              :id,
              :path,
              :link_type_id,
              :file,
              :description,
              :_destroy
            ]
          },
          {
            entity_organizations_attributes: [
              :id,
              :name,
              :person_name,
              :phone,
              :_destroy
            ]
          },
          {
            events_attributes: [
              :id,
              {
                address_attributes: address_attributes
              },
              {
                timetables_attributes: [
                  :id,
                  :execution_date,
                  :start_hour,
                  :end_hour,
                  :_destroy
                ]
              },
              :_destroy
            ]
          },
          {
            albums_attributes: [
              :id,
              :description ,
              :year,
              :_destroy
            ]
          }
      ]
    end
      

  end



end