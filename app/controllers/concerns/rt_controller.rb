module RtController
  extend ActiveSupport::Concern

  included do
    include CommonController

    before_action :set_statuses, only: :index
    before_action :load_data, only: [:index]
    before_action :request_form, except: [:index]

    def show
      respond_with(get_resource) do |format|
        format.js
        format.html
      end
    rescue => e
      log_error("COD-00002",e) 
    end

    def download_request
      @hash_districts={}
      District.where(active: true).all.select("name,id,code").order(:code).each {|n|  @hash_districts.merge!({n.name=>n.id})} 
    rescue => e
      log_error("COD-00003",e) 
    end

    def pre_reject_request_form
      @request_form = get_resource.request_form
    rescue => e
      log_error("COD-0007",e) 
    end

    def reject_request_form
      status_manager = RtController::StatusManager.new((params[:request_form]).merge(manager_id: current_user.loggable_id))
      if status_manager.reject_request_form
        AuditGenerateHelper.audit_request_reject(get_resource, current_user)
        
        redirect_to get_redirect_list, notice: I18n.t('messages.request_form_successfully_rejected')
      else
        @request_form = status_manager.request_form
        flash[:alert] = status_manager.show_errors
        render :pre_reject_request_form
      end
    rescue => e
      log_error("COD-0008",e) 
    end

    def mark_request_form_as_pending
      status_manager = RtController::StatusManager.new(request_form: get_resource.request_form,
                                                       manager_id: current_user.loggable_id)

      if status_manager.mark_request_form_as_pending
        AuditGenerateHelper.audit_request_pending(get_resource, current_user)
        
        redirect_to get_redirect_list, notice: I18n.t('messages.request_form_successfully_marked_as_pending')
      else
        flash[:alert] = status_manager.show_errors
        render :process_request_form
      end
    rescue => e
      log_error("COD-0009",e) 
      redirect_to get_redirect_list, alert: "No se ha podido marcar como pendiente"
    end
  
    def mark_request_form_as_appointed
      status_manager = RtController::StatusManager.new(request_form: get_resource.request_form,
                                                       manager_id: current_user.loggable_id)
      if status_manager.mark_request_form_as_appointed
        AuditGenerateHelper.audit_request_appointed(get_resource, current_user)     
  
        redirect_to get_redirect_list, notice: I18n.t('messages.request_form_successfully_marked_as_appointed')
      else
        flash[:alert] = status_manager.show_errors
        render :process_request_form
      end
    rescue => e
      log_error("COD-00010",e) 
      redirect_to get_redirect_list, alert: "No se ha podido marcar como cita"
    end
  
    def mark_request_form_as_processing
      status_manager = RtController::StatusManager.new(request_form: get_resource.request_form,
                                                       manager_id: current_user.loggable_id)
      if status_manager.mark_request_form_as_processing
        AuditGenerateHelper.audit_request_form_as_processing(get_resource, current_user)
        redirect_to get_redirect_list, notice: I18n.t('messages.request_form_successfully_marked_as_processing')
      else
        flash[:alert] = status_manager.show_errors
        render :process_request_form
      end
    rescue => e
      log_error("COD-00011",e) 
      redirect_to get_redirect_list, alert: "No se ha podido marcar como en proceso"
    end

    protected

      def set_statuses
        params[:pending]    ||= 'true'
        params[:processing] ||= 'true'
        params[:appointed] ||= 'true'
        params[:approved] ||= 'true'
        params[:rejected] ||= 'true'

        statuses.each do |status|
          params[status] = cast_as_boolean params[status]
        end
      end

      def statuses
        @statuses ||= RequestForm.status_names
      end

      def load_data
        @hash_districts={}
        District.where(active: true).where.not(name: "OTROS").all.select("name").order(:name).each {|n|  @hash_districts.merge!({n=>n})} 
        @hash_districts.merge!({I18n.t('others')=>"-280"})
        @hash_entities={}
        Entity.all.where("active=true").select("id,name").order(:name).each {|n| @hash_entities.merge!({n.name=>n.id})}
      end

      def get_resource

      end

      def get_redirect_list

      end

      def request_form
        @request_form = get_resource.request_form    
      rescue
        @request_form = nil
      end

      def calculate_statuses
        whitout_status = true
        aux = {}
        ["search_pending","search_processing","search_approved","search_rejected","search_appointed"].each do |x|
          if params[x].to_s=="true"
            aux[x.gsub('search_','')] = true
            whitout_status = false
          elsif params[x].to_s=="false"
            aux[x.gsub('search_','')] = false
          end
        end
        if whitout_status
          ["search_pending","search_processing","search_approved","search_rejected","search_appointed"].each do |x|
            aux[x.gsub('search_','')] = true
          end
        end
        aux
      end
      
     
  end

  class StatusManager

    attr_accessor :errors, :rejection_type, :request_form

    def initialize(options = {})
      @req_rejection_type_id = options[:req_rejection_type_id]
      @request_form_id       = options[:id]
      @request_form_comments = options[:comments]
      @request_form          = options[:request_form]
      @manager_id            = options[:manager_id]
      @errors                = []
    end

    def rejection_type
      @rejection_type ||= Req::RejectionType.find_by(id: @req_rejection_type_id.to_i)
    end

    def request_form
      @request_form ||= RequestForm.find_by(id: @request_form_id.to_i)
    end

    def reject_request_form
      update_and_trace_status(:rejected, manager_id: @manager_id, req_rejection_type_id: rejection_type.try(:id), comments: @request_form_comments)
    end

    def process_request_form
      return if recently_processed_by_different_manager? 
      if !request_form.blank? && request_form.approved?
        true
      else
        update_and_trace_status(:processing, manager_id: @manager_id)
      end
    end

    def recently_processed_by_different_manager?
      if !request_form.blank? && (request_form.processing? || request_form.approved? )&& recently_processed? && different_manager?
        self.errors << I18n.t('messages.another_manager_is_processing_this_request_form', manager: request_form.manager)
      end
    end

    def different_manager?
      if request_form.try(:manager).blank?
        true
      else
        request_form.manager.id != @manager_id
      end
    end

    def recently_processed?
      time = request_form.try(:status_date).try {|x| x.to_time}
      time = time.blank? ? Time.now  : (Time.now - time.to_time)
      ((time) / 1.minutes).minutes < 5.minutes
    end

    def mark_request_form_as_pending
      update_and_trace_status(:pending, manager_id: @manager_id, req_rejection_type_id: nil)
    end

    def mark_request_form_as_processing
      update_and_trace_status(:processing, manager_id: @manager_id, req_rejection_type_id: nil)
    end

    def mark_request_form_as_appointed
      update_and_trace_status(:appointed, manager_id: @manager_id, req_rejection_type_id: nil)
    end

    def update_and_trace_status(status_name, options = {})
      if !request_form.blank? && !request_form.try {|x| x.update_and_trace_status(status_name, options)}
        self.errors += request_form.try(:errors).try(:full_messages)
      end
      self.errors.blank?
    end

    def show_errors
      "#{I18n.t('messages.errors_found_in_request_form')}\n #{errors.to_sentence}"
    end

  end

end