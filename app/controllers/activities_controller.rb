class ActivitiesController < ApplicationController
  load_and_authorize_resource
  include AddressManager
  respond_to :html, :js, :json
  before_action :set_class_style, only: [:new,:create] 

  def index
    
    if params[:id_check]
      actividad=Activity.find(params[:id_check])
      actividad.review_end_activity = true
      actividad.save(validate: false)
    end

    @hash_tipos={}
    @tipos_actividades=Area.all.order(:name)
    @tipos_actividades.each do |tipo|
      @hash_tipos.merge!({tipo.name => tipo.id })
    end

    @hash_organizadores={}
    @tipos_organizadores=Entity.all.order(:name)
    @tipos_organizadores.each do |tipo|
      @hash_organizadores.merge!({tipo.name => tipo.id })
    end
    @hash_organizadores = @hash_organizadores.sort.to_h
    params[:q] ||= View::List::Activity.ransack_default
    
    @search_q =View::List::Activity.all.search(params[:q])
    @search_q.sorts ||= 'id asc'
    @unpaginated_activities = @search_q.result

    @unpaginated_activities =GlobalSearch.search_filter(@unpaginated_activities,
      [
       ["strict", :id, params["search_id"]],
       ["collective",[
        :name,:description,:transport,:pdf_url
        ],params["enter_text"]
       ],
       ["ilike",:name,params["search_denominacion"]],
       ["strict",:entity_id,params["search_organizador"]],
       ["strict",:area_id,params["search_tipo"]],
       ["strict",:share,params["search_rrss"].to_s!="true" ? "" : params["search_rrss"]],
       ["f_between_between",:start_date,params["search_inicio_f"],:end_date,params["search_fin_f"]]
      ])
    @unpaginated_activities =  @unpaginated_activities.with_status(params["search_status"])
    if params["search_end_activity"].to_s=="true"
      @unpaginated_activities=@unpaginated_activities.all_not_review
    end
    @activities = @unpaginated_activities.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@activities) do |format|
      format.csv { stream_csv_report(@unpaginated_activities,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@activity) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @activity = Activity.new
    respond_with(@activity)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    if AuditGenerateHelper.audit_create(@activity, current_user)
      unless @publish.blank?
        if AuditGenerateHelper.audit_update(@activity, {:activity_id => @activity.id}, current_user)
          @publish.request_form.update_and_trace_status(:approved, manager_id: current_user.loggable_id, user_id: (@publish.request_form.user.id unless @publish.request_form.user.blank?))
          unless @publish.request_form.user.blank?
            send_rt_activity_publish(@publish.request_form.user.loggable) 
          end
        end
      end
    end

    respond_with(@activity)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@activity, activity_params, current_user)
    respond_with(@activity)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    if AuditGenerateHelper.audit_archive(@activity, current_user)
      @activity.update_attribute(:review_end_activity, false)
    end
    respond_with(@activity)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    if AuditGenerateHelper.audit_recover(@activity, current_user)
      @activity.update_attribute(:review_end_activity, false)
    end
    respond_with(@activity)
  rescue => e
    log_error("COD-00008",e) 
  end
  

  protected

  def activity_params
    params.require(:activity).permit(
        :name,
        :description,
        :start_date,
        :end_date,
        :transport,
        :link_url,
        :pdf_url,
        :entity_id,
        :review_end_activity,
        :organizers,
        :project_id,
        :share,
        { area_ids: [] },
        {
          events_attributes: [
            :id,
            {
              address_attributes: address_attributes
            },
            {
              timetables_attributes: [
                :id,
                :execution_date,
                :start_hour,
                :end_hour,
                :_destroy
              ]
            },
            :_destroy
          ]
        },
        {
          logo_attributes: [
            :id,
            :link_type_id,
            :file,
            :_destroy
          ]
        },
        {
          images_attributes: [
            :id,
            :link_type_id,
            :file,
            :description,
            :_destroy
          ]
        },
        {
          videos_attributes: [
            :id,
            :path,
            :link_type_id,
            :file,
            :description,
            :_destroy
          ]
        },
        {
          docs_attributes: [
            :id,
            :link_type_id,
            :file,
            :description,
            :_destroy
          ]
        },
        {
          urls_attributes: [
            :id,
            :path,
            :link_type_id,
            :file,
            :description,
            :_destroy
          ]
        }
    )
  end

  private

  def send_rt_activity_publish(entity)
    message=I18n.t('mailer.activities.message', name: @publish.name)
    subject=I18n.t('mailer.activities.subject')
    if entity.email == nil || entity.email == ''
      if entity.phone_number_alt == nil || entity.phone_number_alt == ''
        #No envía felicitación. No tiene ni dirección de correo electrónico ni móvil
      else
        begin #Si no tiene email, se envía sms
          movil = entity.mobile_number
          SMSApi.new.sms_deliver(movil, message)
        rescue Exception  => e
          Rails.logger.error('sendSms#activity_publish') do
            "#{I18n.t('sms.error')}: \n#{e}"
          end
        end
      end
    else
      begin
        VolunteerMailer.send_email(entity.email, message: message, subject: subject).deliver_now
      rescue Exception  => e
        Rails.logger.error('sendEmail#activity_publish') do
          "#{I18n.t('mailer.error')}: \n#{e}"
        end
      end
    end
  end

  def set_class_style 
    @clase_estilo="" 
    unless params[:rt_activity_publishing_id].blank? 
      @publish=Rt::ActivityPublishing.find(params[:rt_activity_publishing_id])  
      @clase_estilo="border: 1px solid green" 
    end 
  end 
end
