class RequestFormsController < ApplicationController

  load_and_authorize_resource
  respond_to :html, :js, :json

  def index
    params[:q] ||= RequestForm.ransack_default
    @search_q = @request_forms.includes(:request_type, :user, :reason, :status).search(params[:q])
    @request_forms = @search_q.result.paginate(page: params[:page], per_page: params[:per_page]||15)

    respond_with(@request_forms)
  end

  def show
    respond_with(@request_form) do |format|
      format.js #{ render 'shared/popup' }
      format.html
    end
  end

  def new
    @request_form = RequestForm.new
    respond_with(@request_form)
  end

  def edit
  end

  def create
    AuditGenerateHelper.audit_create(@request_form, current_user)
    respond_with(@request_form)
  end

  def update
    AuditGenerateHelper.audit_update(@request_form,request_form_params, current_user)
    respond_with(@request_form)
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@request_form, current_user)
    respond_with(@request_form)
  end

  protected

    def request_form_params
      params
        .require(:request_form)
        .permit(
          :request_type_id,
          :sent_at,
          :status,
          :status_date,
          :rejection_type_id,
          :comments,
          :rt_extendable_id,
          :req_reason_id,
          :rt_extendable_type,
          :req_status_id
        )
    end
end
