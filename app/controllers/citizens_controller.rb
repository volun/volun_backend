class CitizensController < ApplicationController
  require "net/http"
    require "uri"
    include AddressManager
    load_and_authorize_resource
    respond_to :html, :js, :json
    before_action :load_search , only: [:index]
    before_action :set_class_style, only: [:new,:create]
    after_action :create_user, only: [:create, :update]
    
    def index
      params[:q] ||= View::List::Citizen.ransack_default  
      @search_q = View::List::Citizen.search(params[:q])
      @search_q.sorts ||= 'id asc'
      @unpaginated_citizens = @search_q.result
      #@unpaginated_citizens=@unpaginated_citizens.where(email_verified: true)
     
      
      @unpaginated_citizens =GlobalSearch.search_filter(@unpaginated_citizens,
        [
         ["strict",:id,params["search_id"]],
         ["ilike",:name,params["search_nombre"]],
         ["ilike",:last_name_1,params["search_apellido1"]],
         ["ilike",:last_name_2,params["search_apellido2"]],
         ["ilike",:name,params["search_nombre"]],    
         ["collective",[:id,:name, :last_name_1, :last_name_2,:email,:phone],params["enter_text"]]
        ])
        if !params["search_service_type"].blank? 
          @unpaginated_citizens = @unpaginated_citizens.where("id in (?)", CitizenService.all.where("service_id in (?)",params["search_service_type"].split(',')).select(:citizen_id))
        end

      if !params["search_gender"].blank? 
        case params["search_gender"].to_i
        when 1
          @unpaginated_citizens = @unpaginated_citizens.where("gender = 'Varón'")
        when 2
          @unpaginated_citizens = @unpaginated_citizens.where("gender = 'Mujer'")
        end     
      end

    
      if !params["search_other_option"].blank?
        case params["search_other_option"].to_i 
        when 1
          @unpaginated_citizens=@unpaginated_citizens.where(internal_management: false)
        when 2
          @unpaginated_citizens=@unpaginated_citizens.where(internal_management: true, enabled_app: 'Mayor con App móvil')
        when 3
          @unpaginated_citizens=@unpaginated_citizens.where(internal_management: true).where("enabled_app != 'Mayor con App móvil'")
        when 4
          @unpaginated_citizens=@unpaginated_citizens.where(email_verified: false)
        when 5
          @unpaginated_citizens=@unpaginated_citizens.where("tester=true")        
        when 6
          @unpaginated_citizens=@unpaginated_citizens.where("tester=false")
        end
      end

      if !params["search_district"].blank?  && params["search_district"].to_s!="null"
        @unpaginated_citizens=@unpaginated_citizens.where("district_id in (?)", params["search_district"].split(','))
      end
      

      # aux_s = params[:q][:s].to_s.split(' ')
      # @unpaginated_citizens = @unpaginated_citizens.sort_by {|post| post.try(aux_s[0].to_s.to_sym).to_s }

      # begin
      #   @unpaginated_citizens= @unpaginated_citizens.reverse if aux_s[1].to_s == "desc"
      # rescue
      # end
      
      #@unpaginated_citizens = Citizen.for_ids_with_order(@unpaginated_citizens.map(&:id))

      #@unpaginated_citizens = @unpaginated_citizens.where(unsubscribed: false)
      if !params["search_age_start"].blank? && !params["search_age_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("age_name between ? and ?", params["search_age_start"].to_i,params["search_age_end"].to_i)
      elsif !params["search_age_start"].blank? && params["search_age_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("age_name >= ? ", params["search_age_start"].to_i)
      elsif params["search_age_start"].blank? && !params["search_age_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("age_name <= ? ", params["search_age_end"].to_i)
      end

      if !params["search_services_start"].blank? && !params["search_services_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services between ? and ?", params["search_services_start"].to_i,params["search_services_end"].to_i)
      elsif !params["search_services_start"].blank? && params["search_services_end"].blank?       
        @unpaginated_citizens = @unpaginated_citizens.where("count_services >= ?", params["search_services_start"].to_i)
      elsif params["search_services_start"].blank? && !params["search_services_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services <= ? ", params["search_services_end"].to_i)
      end

      if !params["search_services_f_start"].blank? && !params["search_services_f_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_finish between ? and ?", params["search_services_f_start"].to_i,params["search_services_f_end"].to_i)
      elsif !params["search_services_f_start"].blank? && params["search_services_f_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_finish >= ? ", params["search_services_f_start"].to_i)
      elsif params["search_services_f_start"].blank? && !params["search_services_f_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_finish <= ?", params["search_services_f_end"].to_i)
      end

      if !params["search_services_a_start"].blank? && !params["search_services_a_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_accept between ? and ?", params["search_services_a_start"].to_i,params["search_services_a_end"].to_i)
      elsif !params["search_services_a_start"].blank? && params["search_services_a_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_accept >= ?", params["search_services_a_start"].to_i)
      elsif params["search_services_a_start"].blank? && !params["search_services_a_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_accept <= ?", params["search_services_a_end"].to_i)
      end

      if !params["search_services_p_start"].blank? && !params["search_services_p_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_pending between ? and ?", params["search_services_p_start"].to_i,params["search_services_p_end"].to_i)
      elsif !params["search_services_p_start"].blank? && params["search_services_p_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_pending >= ?", params["search_services_p_start"].to_i)
      elsif params["search_services_p_start"].blank? && !params["search_services_p_end"].blank?
        @unpaginated_citizens = @unpaginated_citizens.where("count_services_pending <= ?",params["search_services_p_end"].to_i)
      end

      
      
      @citizens = @unpaginated_citizens.paginate(page: params["page"], per_page: params["per_page"]||15)
      respond_with(@citizens) do |format|
        format.csv { stream_csv_report(@unpaginated_citizens,params)  }
      end       
    end
  
    def show
      respond_with(@citizen) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    end

    def new    
      @citizen = Citizen.new
    end

    def create  
      @citizen.internal_management=true
      if AuditGenerateHelper.audit_create(@citizen,current_user) 

        begin
          url = URI("#{Rails.application.secrets.azure_mta_api}")          
          Net::HTTP.start(url.hostname, url.port, :use_ssl => url.scheme == 'https') do |http|
              request = Net::HTTP::Post.new(url,{'Content-type' => 'application/json; charset=UTF-8'})
              request.body={
                "email": "#{@citizen.email}",
                "document": "#{@citizen.document}",
                "documentType": @citizen.document_type_ws,
                "phone": "#{@citizen.phone}",
                "name": "#{@citizen.name}",
                "privacyTermsAccepted": true,
                "birthday": "#{@citizen.birthday.strftime("%d/%m/%Y") if !@citizen.try(:birthday).blank?}",
                "birthplace": 0
              }.to_json
              response = http.request(request)
              if !response.body.blank?
                begin
                  Rails.logger.error("ERROR-WS-AZURE: citizen -> No se ha podido crear el ciudadano en Azure")
                rescue
                end
              end
          end
        rescue => e
          begin
            Rails.logger.error("ERROR-WS-AZURE: citizen -> #{e}")
          rescue
          end
          puts "="*50
          puts e
        end
        CitizenTracking.create(:citizen=> @citizen,
          :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Se ha generado en la aplicación de gestión interna")
      end
      respond_with(@citizen)
    end

    def edit
    end

    def update
      if AuditGenerateHelper.audit_update(@citizen, citizen_params, current_user)
        if params[:button].to_s == "padron"
          @citizen.address = get_padron(@citizen)
          @citizen.save(validate: false)
        end
      end
      if params[:button].to_s == "padron"        
        if !@citizen.address.blank?
          redirect_to edit_citizen_path(@citizen), notice: "Se han actualizado los datos de padron"
        else
          redirect_to edit_citizen_path(@citizen), alert: "No se han actualizado los datos de padron"
        end
      else
        respond_with(@citizen)
      end
    end
  
    def destroy
      CitizenService.where(citizen_id: @citizen.id).each do |service|
        service.status = "canceled"
        if AuditGenerateHelper.audit_archive(service,current_user, true)
          CitizenServiceTracking.create(:citizen_service=> service,
            :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
            :manager => current_user.loggable,
            :tracked_at => Time.zone.now,
            :coments => "Se ha dado de baja en la aplicación de gestión interna")
        end
      end
      if AuditGenerateHelper.audit_archive(@citizen, current_user)
        CitizenTracking.create(:citizen=> @citizen,
          :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Se ha dado de baja en la aplicación de gestión interna")
      end
      respond_with(@citizen)
    end
  
    def recover
      if AuditGenerateHelper.audit_recover(@citizen, current_user)
        CitizenTracking.create(:citizen=> @citizen,
          :tracking_type => TrackingType.find_by(alias_name: "subscribe"),
          :manager => current_user.loggable,
          :tracked_at => Time.zone.now,
          :coments => "Se ha dado de alta en la aplicación de gestión interna")
      end
      respond_with(@citizen)
    end

    def new_password
      @citizen = Citizen.find(params[:id]) 
    end

    def citizen_password
      @citizen = Citizen.find(params[:id]) 
      if !@citizen.phone.blank? && @citizen.phone.to_s.match(/[9|8]\d{8}/).blank?
        @phone = @citizen.phone
      else
        @phone = ""
      end
      birthday = @citizen.try(:birthday).try(:to_datetime) 
      @pass = "#{@citizen.name.first.upcase}" + "#{@citizen.last_name_1.first.upcase}" + "#{birthday.blank? ? Time.zone.now.year : birthday.to_date.year}" + "#{SecureRandom.hex(1)}"
      respond_with(@citizen) do |format|
        format.js # { render 'shared/popup_password' }
        format.html
      end
    end

    def update_password
      email = params[:email]
      pass = params[:cadena_aleatoria]
      sms = params[:phone]    
      
      if pass.blank?
        redirect_to citizen_path, alert: I18n.t('change_password.alert_mail_cv')
      elsif (params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true") && (email.blank? || email.to_s.match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i).blank? )
        redirect_to citizen_path, alert: I18n.t('alert_mail')
      elsif (params[:send_sms].to_s == "1" || params[:send_sms].to_s == "true") && (sms.blank? || !sms.to_s.match(/[9|8]\d{8}/).blank? )
        redirect_to citizen_path, alert: I18n.t('alert_message')
      else
        citizen= Citizen.find(params[:id])
        subject = I18n.t('change_password.subject_citizen')
  
        if citizen.user.blank?
          user = User.new(login: "usercitizen#{'%09d' % citizen.id}", loggable: citizen, notice_type: NoticeType.email.take)
          citizen.user = user
        else 
          user = citizen.user
        end
  
        user.email = citizen.email
        user.mobile_phone = citizen.phone
        user.password = params[:cadena_aleatoria]
        user.password_confirmation = params[:cadena_aleatoria]
        
        if user.save(validate: false)
          citizen.save(validate: false)
          
          AuditGenerateHelper.reset_password(citizen, current_user)
        
  
          #Envio de mail si está 1
          if params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true"
            message = I18n.t('change_password_citizen.message', email: email, password: pass).html_safe
            begin
              VolunteerMailer.send_email(params[:recipient].presence || email, message: message, subject: subject).deliver_now
              AuditGenerateHelper.audit_send_mail(citizen, current_user)
            rescue Exception  => e
              redirect_to citizen_path, alert: I18n.t('alert_mail_sending')
              return
            end
          end
  
          #Envio de sms si está 1
          if params[:send_sms].to_s == "1" || params[:send_sms].to_s=="true"
            message = I18n.t('change_password_citizen.message_sms', password: pass).html_safe
            begin
              SMSApi.new.sms_deliver(sms, message)
              AuditGenerateHelper.audit_send_sms(citizen, current_user)
            rescue Exception  => e
              redirect_to citizen_path, alert: I18n.t('alert_sms_sending')
              return
            end
          end
          redirect_to citizen_path, notice: I18n.t('success_message_sending')
        else
          redirect_to citizen_path,alert: I18n.t('alert_not_user')
        end
      end
    end

    def send_massive_sms
     
    end

    def post_send_massive_sms     
      if params[:content].blank? && params[:content].strip.blank?
        redirect_to send_massive_sms_citizens_path, alert: "Falta introducir los campos obligatorios del cuerpo del mensaje"
      else
        citizens = Citizen.where("phone is not NULL AND phone != '' AND active=true AND email_verified=true").select(:id).distinct

        begin
          MassiveSmsJob.perform_later(current_user.id, citizens.map(&:id), params[:content], "citizen")  
       
          redirect_to send_massive_sms_citizens_path, notice: I18n.t('massive_sms.success') 
        rescue => e
          redirect_to send_massive_sms_citizens_path, alert: e 
        end
      end
    end

    def historic_massive_sms
      @massive_sms_historics = MassiveSmsHistoric.citizens_sms
      @massive_sms_historics = @massive_sms_historics.paginate(page: params["page"] || 1, per_page: params["per_page"]||10)

    end

    private

    def citizen_params
      params
        .require(:citizen)
        .permit(
          :id,
          :name,
          :last_name_1,
          :last_name_2,
          :document,
          :document_type_id,
          :gender_id,
          :email,
          :enabled_app,
          :phone,
          :tester,
          :BDC,
          :legal_msg,
          :email_verified,
          :padron,
          {
            address_attributes: address_attributes
          },
          {
            logo_attributes: [ :id, :link_type_id, :file, :_destroy ]
          },
        )
    end

    def create_user
      if @citizen.try(:user).blank?
        user = User.new(login: "usercitizen#{'%09d' % @citizen.id}", loggable: @citizen, notice_type: NoticeType.email.take)
        user.password = generate_new_password(@citizen,user)
        user.password_confirmation = user.password

        if @citizen.email.blank?
          user.email = "usercitizen#{'%09d' % @citizen.id}.citizen@volun.es".downcase
        else
          user.email = @citizen.email.downcase
        end
        user.save
      end
    rescue
    end

    def generate_new_password(citizen,user)
      Digest::SHA1.hexdigest("#{citizen.created_at.to_s}--#{user.login}")[0,8].upcase
    end

    def get_padron(citizen)
      padron = Padron.new(citizen).search_citizien

      if !padron.blank? && !padron[:datos_habitante].blank? && !padron[:datos_habitante][:item].blank?
          now = Time.zone.now.to_date
          date_bithday = Time.zone.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string].to_s)
          if now.year - date_bithday.year - ((now.month > date_bithday.month || (now.month == date_bithday.month && now.day >= date_bithday.day)) ? 0 : 1) >= 65
              citizen.name = padron[:datos_habitante][:item][:nombre]
              citizen.last_name_1 = padron[:datos_habitante][:item][:apellido1]
              citizen.last_name_2 = padron[:datos_habitante][:item][:apellido2]
              citizen.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
              citizen.country_code = padron[:datos_habitante][:item][:codigo_pais_nacimiento]
              citizen.birthday = padron[:datos_habitante][:item][:fecha_nacimiento_string].blank? ? '' : Time.zone.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string]).strftime("%d/%m/%Y")
              citizen.academic_info = padron[:datos_habitante][:item][:descripcion_nivel_instruccion]
              citizen.gender = Gender.where("upper(name) = ?", padron[:datos_habitante][:item][:descripcion_sexo].upcase).first
              citizen.people_at_home = padron[:datos_vivienda][:item][:num_personas]
              citizen.start_date = padron[:datos_vivienda][:item][:fecha_alta_inscripcion_string]
              citizen.phone = padron[:datos_vivienda][:item][:telefono].blank? ? citizen.phone : padron[:datos_vivienda][:item][:telefono]
  
              address = Address.new()
              
              address.normalize = false
              address.road_type = padron[:datos_vivienda][:item][:sigla_via]
              address.road_name = padron[:datos_vivienda][:item][:nombre_via]
              address.road_number = padron[:datos_vivienda][:item][:numero_via]
              address.road_number_type = padron[:datos_vivienda][:item][:nominal_via]
              address.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
              address.country_code = padron[:datos_habitante][:item][:codigo_pais_nacimiento]
              address.town = padron[:datos_habitante][:item][:desc_municipio_nacimiento].blank? ? "Madrid" : padron[:datos_habitante][:item][:desc_municipio_nacimiento]
              address.town_code = padron[:datos_habitante][:item][:codigo_municipio_nacimiento].blank? ? "" : padron[:datos_habitante][:item][:codigo_municipio_nacimiento]

              address.province = padron[:datos_habitante][:item][:codigo_provincia_nacimiento].blank? ? Province.find_by(code: 28) : Province.find_by(code: padron[:datos_habitante][:item][:codigo_provincia_nacimiento].to_i)
              address.floor = padron[:datos_vivienda][:item][:planta]
              address.door = padron[:datos_vivienda][:item][:puerta]
              address.borought = Borought.find_by("cast(code as int)=? and district_id=?",padron[:datos_vivienda][:item][:codigo_barrio].to_i,District.find_by("cast(code as int)=?",padron[:datos_vivienda][:item][:codigo_distrito].to_i))            
              address.district = District.find_by("cast(code as int)=?",padron[:datos_vivienda][:item][:codigo_distrito].to_i)
              address.postal_code = padron[:datos_vivienda][:item][:codigo_postal]

              
              address
          else
              nil
          end
      elsif padron.blank?
          nil
      else
          nil
      end
  rescue => e
      begin
          Rails.logger.error("ERROR-WS: #{e}")
      rescue
      end
      nil
  end

    def load_search
      @hash_service_type= {}
      Service.all_active.order(:name).select(:name,:id).each do |s|
        @hash_service_type.merge!({s.name => s.id})
      end

      @hash_gender_type= {}
      @hash_gender_type.merge!({"Todos"=>"0", "Mujer" => "2","Varón" => "1"})

      @hash_other_option = {}
      @hash_other_option.merge!({"Todos" => "0"})
      @hash_other_option.merge!({"Mayores creados en APP" => "1"})
      @hash_other_option.merge!({"Mayores creados en GI con APP móvil" => "2"})
      @hash_other_option.merge!({"Mayores creados en GI sin APP móvil" => "3"})
      @hash_other_option.merge!({"Mayores 'Sin verificar'" => "4"})
      @hash_other_option.merge!({"Mayores tester" => "5"})
      @hash_other_option.merge!({"Mayores no tester" => "6"})


      @hash_districts = {}
      District.where(active: true).order(:name).select(:name,:code).each do |d|
        @hash_districts.merge!({d.name => d.code})
      end
    end


    def set_class_style 
        @clase_selector = "margin-left:20px"
        @clase_estilo ="border: 1px solid green" 
        @clase_solicitud=@clase_estilo 
      end 
    
end
