class VolunExportsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js

  def index
    params[:q] ||= VolunExport.ransack_default
    @search_q = @volun_exports.search(params[:q])
    @search_q.sorts ||= 'id asc'
    @unpaginated_volun_exports = @search_q.result
    @unpaginated_volun_exports =GlobalSearch.search_filter(@unpaginated_volun_exports,
      [
        ["ilike",:id_field,params["search_id"]],
        ["ilike",:name_field,params["search_name_field"]],
        ["ilike",:first_last_name_field,params["search_first_last_name_field"]],
        ["ilike",:second_last_name_field,params["search_second_last_name_field"]],
        ["ilike",:phone_field,params["search_phone_field"]],
        ["ilike",:mobile_phone_field,params["search_mobile_phone_field"]],
        ["ilike",:mail_field,params["search_mail_field"]],
        ["ilike",:district_field,params["search_district_field"]],
        ["ilike",:subscribe_date_field,params["search_subscribe_date_field"]],
        ["ilike",:unsubscribe_date_field,params["search_unsubscribe_date_field"]],
        ["ilike",:sex_field,params["search_sex_field"]],
        ["ilike",:birth_date_field,params["search_birth_date_field"]],
        ["ilike",:age_field,params["search_age_field"]],
        ["ilike",:document_type_field,params["search_document_type_field"]],
        ["ilike",:document_number_field,params["search_document_number_field"]],
        ["ilike",:province_field,params["search_province_field"]],
        ["ilike",:town_field,params["search_town_field"]],
        ["ilike",:road_name_field,params["search_road_name_field"]],
        ["ilike",:postal_code_field,params["search_postal_code_field"]],
        ["ilike",:academic_level_field,params["search_academic_level_field"]],
        ["ilike",:languages_field,params["search_languages_field"]],
        ["ilike",:project_culities_field,params["search_project_culities_field"]],
        ["ilike",:vocne_field,params["search_vocne_field"]],
        ["ilike",:can_criminal_field,params["search_can_criminal_field"]],
        ["ilike",:has_diving_license_field,params["search_has_diving_license_field"]],
        ["ilike",:profession_field,params["search_profession_field"]],
        ["ilike",:how_know_cv_field,params["search_how_know_cv_field"]],
        ["ilike",:is_export,params["search_is_export"]],
        ["ilike",:retired,params["search_retired"]],
        ["ilike",:nationality,params["search_nationality"]],
        ["ilike",:country,params["search_country"]],
        ["ilike",:status,params["search_status"]],
        ["ilike",:academic_level,params["search_academic_level"]],
        ["ilike",:cat_situation,params["search_cat_situation"]],
        ["ilike",:language,params["search_language"]],
        ["ilike",:level,params["search_level"]],
        ["ilike",:skills_hobbies,params["search_skills_hobbies"]],
        ["ilike",:next_step,params["search_next_step"]]
       ])
    @volun_exports = @unpaginated_volun_exports.paginate(page: params[:page], per_page: params[:per_page]||15)

    respond_with(@volun_exports)
  end

  def show
    respond_with(@volun_export) do |format|
      format.js
      format.html
    end
  end

  def new
    respond_with(@volun_export)
  end

  def edit
  end

  def create
    AuditGenerateHelper.audit_create(@volun_export, current_user)
    respond_with(@volun_export)
  end

  def update
    @volun_export.is_export = false
    AuditGenerateHelper.audit_update(@volun_export, volun_export_params, current_user)
    respond_with(@volun_export)
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@volun_export, current_user)
    redirect_to volun_exports_path, notice: I18n.t('success_volun_export_success')
  end

  def delete_modal
    #AuditGenerateHelper.audit_destroy(@volun_export, current_user)
    redirect_to volun_exports_path, notice: I18n.t('success_volun_export_success')
  end

  def download 
    @volun_export = VolunExport.find_by(id: params[:id])
    params[:formats]="csv"

    respond_to do |format|
      format.html { redirect_to volun_exports_path, alert: I18n.t('alert_no_file')}
      format.csv  { stream_csv_report(@volun_export,params) }
    end   
  end 
  
  protected
    def volun_export_params
      params.require(:volun_export)
      .permit(:id_field, 
              :name_field, 
              :first_last_name_field,
              :second_last_name_field,
              :phone_field,
              :mobile_phone_field,
              :mail_field,
              :district_field,
              :subscribe_date_field,
              :unsubscribe_date_field,
              :sex_field,
              :birth_date_field,
              :age_field,
              :document_type_field,
              :document_number_field,
              :province_field,
              :town_field,
              :road_name_field,
              :postal_code_field,
              :academic_level_field,
              :languages_field,
              :project_culities_field,
              :vocne_field,
              :can_criminal_field,
              :has_diving_license_field,
              :profession_field,
              :how_know_cv_field,
              :is_export,
              :retired,
              :nationality,
              :country,
              :status,
              :academic_level,
              :degree_type,
              :language,
              :language_level,
              :skills,
              :next_point)
    end
end
