class Pro::TrackingsController < ApplicationController
  load_and_authorize_resource instance_name: :pro_tracking
  respond_to :html, :js, :json
  before_action :tracking_types_active, only: [:new, :create, :update, :edit] 

  def index
    params[:q] ||= View::List::ProTracking.ransack_default
    @project = Project.find_by(id: params[:q][:project_id_eq])
   
    # unless  params["search_manager"].blank?
    #   @pro_trackings=@pro_trackings.joins(:manager)
    # end
    params[:q][:s] ||= 'tracked_at desc'
    @search_q = View::List::ProTracking.search(params[:q])
    @unpaginated_pro_trackings = @search_q.result
    @unpaginated_pro_trackings=GlobalSearch.search_filter(@unpaginated_pro_trackings,
      [ ["strict",:tracking_type_id ,  params["search_tipo"]],
        ["collective",[:project,:manager,:comments], params["search_enter_text"]],
        ["f_between",:tracked_at, params["search_inicio_f"], params["search_fin_f"]],
        ["ilike",:manager,  params["search_manager"]]
      ])
   # @unpaginated_pro_trackings = @unpaginated_pro_trackings.order(tracked_at: :desc)
    @pro_trackings = @unpaginated_pro_trackings.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_pro_trackings,params)  }
    end
  rescue => e
    log_error("COD-00001",e)
  end

  def show
    respond_with(@pro_tracking) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    project = Project.find_by(id: params[:project_id])
    @pro_tracking = Pro::Tracking.new(project: project)
    respond_with(@pro_tracking)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    @pro_tracking=Pro::Tracking.new(pro_tracking_params)
    AuditGenerateHelper.audit_create(@pro_tracking, current_user)

    if @pro_tracking.project.subtype_proyect == "subproyect"
      pro_tracking_params[:project_id] = @pro_tracking.project.father_id
      @pro_tracking_father=Pro::Tracking.new(pro_tracking_params)
      @pro_tracking_father.project= @pro_tracking.project.father
      AuditGenerateHelper.audit_create(@pro_tracking_father, current_user)
    end
    respond_with(@pro_tracking, location: pro_trackings_path({project_id: params[:project_id], is_update: params[:is_update],q: { project_id_eq: @pro_tracking.project_id}})) 
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@pro_tracking, pro_tracking_params, current_user)
    respond_with(@pro_tracking, location: pro_trackings_path({project_id: params[:project_id], is_update: params[:is_update],q: { project_id_eq: @pro_tracking.project_id}})) 
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@pro_tracking, current_user)
    respond_with(@pro_tracking)
  rescue => e
    log_error("COD-00007",e)
  end

  private

  def tracking_types_active 
    @tracking_types_active = TrackingType.all_projects.order(:name)
  end 

  protected

    def pro_tracking_params
      params.require(:pro_tracking).permit(
          :project_id,
          :manager_id,
          :tracked_at,
          :tracking_type_id,
          :comments
        )
    end
    
    alias_method :create_params, :pro_tracking_params
end
