class CitizenTrackingsController < ApplicationController
  
  load_and_authorize_resource
  respond_to :html, :js, :json

  #before_action :load_search, only: [:index]
  before_action :tracking_types_active, only: [:new, :create, :update, :edit] 
  before_action :load_citizen

  def index
    @tracking_types_active = TrackingType.all_citizens.order(:name)
    @hash_tracking_types={}
    @tracking_types_active.each do |type|
      @hash_tracking_types.merge!({"#{type.name}" => type.id})
    end
    params[:q] ||= View::List::CitizenTracking.ransack_default 
    params[:q][:s] ||= 'tracked_at desc'
    @search_q = View::List::CitizenTracking.search(params[:q])
    @unpaginated_citizen_trackings = @search_q.result.where(citizen_id: @citizen.id)
    @unpaginated_citizen_trackings=GlobalSearch.search_filter(@unpaginated_citizen_trackings,
      [ ["strict",:tracking_type_id ,  params["search_tipo"]],
        ["collective",[:citizen,:manager,:tracking_type,:coments], params["search_enter_text"]],
        ["ilike",:manager,  params["search_manager"]]
      ])

    @citizen_trackings = @unpaginated_citizen_trackings.paginate(page:  params["page"], per_page:  params["per_page"]||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_citizen_trackings,params)  }
    end
  end

  def show
    respond_with(@citizen_tracking) do |format|
      format.js #{ render 'shared/popup' }
      format.html
    end
  end

  def new
    respond_with(@citizen_tracking)
  end

  def edit
  end

  def create
    @citizen_tracking.tracked_at = Time.now
    if AuditGenerateHelper.audit_create(@citizen_tracking, current_user)     
      redirect_to citizen_citizen_trackings_path(@citizen), notice: "Seguimiento de ciudadano se ha creado correctamente."
    else
      redirect_to new_citizen_citizen_tracking_path(@citizen), alert: "Se debe rellenar correctamente el formulario."
    end
  end  

  def update

    @citizen_tracking.tracked_at = Time.now
    if AuditGenerateHelper.audit_update(@citizen_tracking, citizen_tracking_params, current_user)
      redirect_to citizen_citizen_trackings_path(@citizen),notice: "Seguimiento de ciudadano se ha actualizado correctamente."
    else
      redirect_to edit_citizen_citizen_tracking_path(@citizen, @citizen_tracking), alert: "Se debe rellenar correctamente el formulario."

    end
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@citizen_tracking, current_user)
    respond_with(@citizen_tracking)
  end

  protected
    def load_citizen
      @citizen = Citizen.find_by(id: params[:citizen_id])
    end

    def citizen_tracking_params
      params
        .require(:citizen_tracking)
        .permit(
          :id,
          :citizen_id,
          :tracking_type_id,
          :manager_id,
          :tracked_at,
          :coments
        )
    end
    
  alias_method :create_params, :citizen_tracking_params

  private 
  
  def tracking_types_active 
    @tracking_types_active = TrackingType.all_citizens.order(:name)
  end 
end
