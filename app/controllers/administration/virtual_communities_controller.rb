class Administration::VirtualCommunitiesController < ApplicationController
  before_action :load_search_visit , only: [:cv_statistics, :show_origin]
  before_action :load_search_sections , only: [:cv_statistics, :show_popular]
  before_action :load_search_cv_statistics, only: [:cv_statistics]
  before_action :load_search_ws_statistics, only: [:ws_statistics]
  before_action :load_querys, only: [:cv_statistics]
  before_action :authorize
  before_action :load_data, only: [:ws_statistics,:cv_statistics, :show_origin, :show_popular]
  include VirtualCommunitiesHelper

  respond_to :html, :js, :csv

  def ws_statistics
    citizen_services = CitizenService.joins(:citizen).all.not_market.where("citizens.tester !=true")
    @services = Service.all
    @services_questions = ServiceQuestion.all
    citizens = Citizen.all.not_tester
    volunteer_proyect = ProjectsVolunteer.joins(:project,:volunteer).where("projects.accompany=true AND volunteers.tester !=true AND volunteers.market !=true AND projects_volunteers.volunteer_type != 'DERIVATIE'")

    citizen_services =GlobalSearch.search_filter(citizen_services, [["f_between","citizen_services.updated_at",@params_ws['search_start_date'],@params_ws['search_end_date']]])
    @services =GlobalSearch.search_filter(@services, [["f_between","services.updated_at",@params_ws['search_start_date'],@params_ws['search_end_date']]])
    @services_questions =GlobalSearch.search_filter(@services_questions, [["f_between","service_questions.updated_at",@params_ws['search_start_date'],@params_ws['search_end_date']]])
    citizens =GlobalSearch.search_filter(citizens, [["f_between","citizens.updated_at",@params_ws['search_start_date'],@params_ws['search_end_date']]])
    volunteer_proyect =GlobalSearch.search_filter(volunteer_proyect, [["f_between","projects_volunteers.subscribe_date",@params_ws['search_start_date'],@params_ws['search_end_date']]])

    total_rate = 0
    count = 0
    citizen_services.each do |x| 
      if !x.f_volunteer_vote.blank?
        total_rate = total_rate + x.f_volunteer_vote.to_i
        count = count + 1
      end
    end
    @data = {
      'total': volunteer_proyect.count,
      'total_actived': volunteer_proyect.select {|x| x.active}.count,
      'total_inactived': volunteer_proyect.select {|x| !x.active}.count,      
      'total_citizens': citizens.count,
      'total_actived_citizens': citizens.select {|x| x.active}.count,
      'total_inactived_citizens': citizens.select {|x| !x.active}.count,
      'total_services': citizen_services.count,
      'total_pending_services': citizen_services.select {|x| ["active","priority"].include?(x.status.to_s)}.count,
      'total_accepted_services': citizen_services.select {|x| x.status.to_s == "accept"}.count,
      'total_priority_services': citizen_services.select {|x| x.status.to_s == "priority"}.count,
      'total_undone_services': citizen_services.select {|x| x.status.to_s == "undone"}.count,
      'total_finished_services': citizen_services.select {|x| x.status.to_s == "finished"}.count,      
      'total_active_services': citizen_services.select {|x| x.status.to_s == "active"}.count,
      'total_expired_services': citizen_services.select {|x| x.status.to_s == "expired"}.count,
      'total_rate': total_rate == 0 ? 0 : count == 0 ? 0 : total_rate / count
    }
    respond_to do |format|
      format.html
      format.js
      format.csv  { send_data VirtualCommunity.to_csv(@data, @services_questions, @services), filename: "EstadisticasServiciosMayores.csv" }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def cv_statistics
    @count_popular = @popular_sections_unp.count
    @popular_sections_unp = @popular_sections_unp.to_a
    @popular_sections_unp= @popular_sections_unp
    @count_visit = @visit_origin_unp.count
    @visit_origin_unp = @visit_origin_unp.to_a
    @visit_origin_unp = @visit_origin_unp.paginate(page: @params_events["page"], per_page: 15)
  rescue => e
    log_error("COD-00002",e) 
  end

  def show_origin
    @key = params[:key]
    begin
      @key = URI.decode( @key) 
    end while( @key != URI.decode( @key)  )

    @list = []
    
    @date_start = @params_visit["search_inicio_f"].blank? ? "comienzo" : Time.zone.parse(@params_visit["search_inicio_f"]).strftime("%d/%m/%Y")
    @date_end = @params_visit["search_fin_f"].blank? ? "hoy" : Time.zone.parse(@params_visit["search_fin_f"]).strftime("%d/%m/%Y")

    if !@params_visit["search_inicio_f"].blank? && !@params_visit["search_fin_f"].blank?
      s_date_p = " AND (cast( ahoy_visits.started_at as date) BETWEEN cast('#{Time.zone.parse(@params_visit["search_inicio_f"]).to_s}' as date) AND cast('#{Time.zone.parse(@params_visit["search_fin_f"]).to_s}' as date))"
    elsif !@params_visit["search_inicio_f"].blank? 
      s_date_p = " AND (cast( ahoy_visits.started_at as date) >= cast('#{Time.zone.parse(@params_visit["search_inicio_f"]).to_s}' as date))"
    elsif !@params_visit["search_fin_f"].blank?
      s_date_p = " AND (cast( ahoy_visits.started_at as date) <= cast('#{Time.zone.parse(@params_visit["search_fin_f"]).to_s}' as date))"
    else 
      s_date_p = ""
    end

    s_origin = " AND (translate(UPPER(cast(ahoy_visits.landing_page as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER(cast('%#{@key}%' as varchar)), 'ÁÉÍÓÚ', 'AEIOU')) "
    query = "select CASE WHEN ahoy_visits.landing_page IS NULL THEN '' ELSE ahoy_visits.landing_page END  as origin, started_at as time from ahoy_visits 
    WHERE  (ahoy_visits.user_id IS NULL OR ahoy_visits.user_id IN (Select users.id from users where users.loggable_type != 'Manager' )) #{s_origin} #{s_date_p}
            order by time DESC"
    visit_origin_unp  = ActiveRecord::Base.connection.exec_query(query)
    visit_origin_unp.each {|x| @list.push({origin_translate: traduction_route(x['origin']), origin: x['origin'], time: Time.zone.parse(x['time'].to_s).to_date.strftime('%d/%m/%Y %H:%M')})}
    @list = @list.to_a.paginate(page: params[:page], per_page: params[:per_page] || 15)
  rescue => e
    log_error("COD-00003",e) 
  end

  def show_popular
    @key = params[:key]
    begin
      @key = URI.decode( @key) 
    end while( @key != URI.decode( @key)  )

    @list = []
    @date_start = @params_events["search_inicio_fech"].blank? ? "comienzo" : Time.zone.parse(@params_events["search_inicio_fech"]).strftime("%d/%m/%Y")
    @date_end = @params_events["search_fin_fech"].blank? ? "hoy" : Time.zone.parse(@params_events["search_fin_fech"]).strftime("%d/%m/%Y")

    if !@params_events["search_inicio_fech"].blank? && !@params_events["search_fin_fech"].blank?
      s_date = " AND (cast( ahoy_events.time as date) BETWEEN cast('#{Time.zone.parse(@params_events["search_inicio_fech"]).to_s}' as date) AND cast('#{Time.zone.parse(@params_events["search_fin_fech"]).to_s}' as date))"
    elsif !@params_events["search_inicio_fech"].blank? 
      s_date = " AND (cast( ahoy_events.time as date) >= cast('#{Time.zone.parse(@params_events["search_inicio_fech"]).to_s}' as date))"
    elsif !@params_events["search_fin_fech"].blank?
      s_date = " AND (cast( ahoy_events.time as date) <= cast('#{Time.zone.parse(@params_events["search_fin_fech"]).to_s}' as date))"
    else 
      s_date = ""
    end

    s_pages = " AND (translate(UPPER(cast(ahoy_events.properties as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER(cast('%#{@key}%' as varchar)), 'ÁÉÍÓÚ', 'AEIOU')) "
    query = "select properties -> 'title'  as origin, time from ahoy_events 
      WHERE properties IS NOT NULL #{s_pages} #{s_date} order by time DESC"
    popular_sections_unp  = ActiveRecord::Base.connection.exec_query(query) 
    popular_sections_unp.each {|x| @list.push({origin_translate: x['origin'].try {|x| x.to_s.gsub('"','')}, origin: x['origin'], time: Time.zone.parse(x['time'].to_s).to_date.strftime('%d/%m/%Y %H:%M')})}
    @list = @list.to_a.paginate(page: params[:page], per_page: params[:per_page] || 15)
  rescue => e
    log_error("COD-00004",e) 
  end

  private

  def authorize
    unless can? :analyze_cv, WelcomeController
      redirect_to root_path, alert: I18n.t('messages.access_denied')
    end
  end

  def load_search_visit
    search = SearchManager.new()
    @params_visit = search.load_search("Procedure", params, current_user)
  end

  def load_search_sections
    search = SearchManager.new()
    @params_events = search.load_search("PopularVisit", params, current_user)
  end

  def load_search_cv_statistics
    search = SearchManager.new()
    @params_cv = search.load_search("CVStatistics", params, current_user)
  end

  def load_search_ws_statistics
    search = SearchManager.new()
    @params_ws = search.load_search("WSStatistics", params, current_user)
  end

  def load_querys
    filter_visits = ""
    if !@params_cv['search_start_date'].blank? && !@params_cv['search_end_date'].blank?
      filter_visits = " WHERE time BETWEEN '#{@params_cv['search_start_date']}' AND '#{@params_cv['search_end_date']}'"
    elsif !@params_cv['search_start_date'].blank? && @params_cv['search_end_date'].blank?
      filter_visits = " WHERE time >= '#{@params_cv['search_start_date']}'"
    elsif @params_cv['search_start_date'].blank? && !@params_cv['search_end_date'].blank?
      filter_visits = " WHERE time <= '#{@params_cv['search_end_date']}'"
    end

    query = "select distinct count(visit_id) from ahoy_events #{filter_visits}"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_visits = result[0]["count"]

    query = "select distinct count(*) FROM ahoy_events, users where ahoy_events.name = 'Sign in' AND users.id = ahoy_events.user_id and loggable_type='Volunteer' #{filter_visits.gsub('WHERE','AND')}"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_users_volunteer = result[0]["count"]

    query = "select distinct count(*) FROM ahoy_events, users where ahoy_events.name = 'Sign in' AND users.id = ahoy_events.user_id and loggable_type='Entity' #{filter_visits.gsub('WHERE','AND')}"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_users_entity = result[0]["count"]

    query = "select distinct count(*) FROM ahoy_events, users where ahoy_events.name = 'Sign in' AND users.id = ahoy_events.user_id and loggable_type='Volunteer' AND time >= '#{Time.zone.now.strftime('%Y-%m-%d')}'"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_users_volunteer_now = result[0]["count"]

    query = "select distinct count(*) FROM ahoy_events, users where ahoy_events.name = 'Sign in' AND users.id = ahoy_events.user_id and loggable_type='Entity' AND time >= '#{Time.zone.now.strftime('%Y-%m-%d')}'"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_users_entity_now = result[0]["count"]
    
    query = "select distinct count(visit_id) from ahoy_events WHERE time >= '#{Time.zone.now.strftime('%Y-%m-%d')}'"
    result = ActiveRecord::Base.connection.exec_query(query)
    @num_visits_now = result[0]["count"] 

    unless @params_visit["search_origin"].blank?
      s_origin = " AND (translate(UPPER(cast(ahoy_visits.landing_page as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER(cast('%#{@params_visit["search_origin"]}%' as varchar)), 'ÁÉÍÓÚ', 'AEIOU')) "
    else
      s_origin = ""
    end

    if params[:order_landing_page].to_i == 2 && (params[:order_count_origin].to_i == 0)
      s_order = "origin DESC"
    elsif params[:order_landing_page].to_i == 0 && (params[:order_count_origin].to_i == 2)
      s_order ="visit DESC"
    elsif params[:order_landing_page].to_i == 1 && (params[:order_count_origin].to_i == 0)
      s_order ="origin ASC"
    elsif params[:order_landing_page].to_i == 0 && (params[:order_count_origin].to_i == 1)
      s_order ="visit ASC"
    elsif params[:order_landing_page].to_i == 0 && (params[:order_count_origin].to_i == 0)
      s_order ="visit DESC"
    else
      s_order = "visit DESC"
    end

    if !@params_visit["search_inicio_f"].blank? && !@params_visit["search_fin_f"].blank?
      s_date_p = " AND (cast( ahoy_visits.started_at as date) BETWEEN cast('#{Time.zone.parse(@params_visit["search_inicio_f"]).to_s}' as date) AND cast('#{Time.zone.parse(@params_visit["search_fin_f"]).to_s}' as date))"
    elsif !@params_visit["search_inicio_f"].blank? 
      s_date_p = " AND (cast( ahoy_visits.started_at as date) >= cast('#{Time.zone.parse(@params_visit["search_inicio_f"]).to_s}' as date))"
    elsif !@params_visit["search_fin_f"].blank?
      s_date_p = " AND (cast( ahoy_visits.started_at as date) <= cast('#{Time.zone.parse(@params_visit["search_fin_f"]).to_s}' as date))"
    else 
      s_date_p = ""
    end

    query = "select CASE WHEN ahoy_visits.landing_page IS NULL THEN '' ELSE ahoy_visits.landing_page END  as origin, count(ahoy_visits.id) as visit from ahoy_visits 
    WHERE  (ahoy_visits.user_id IS NULL OR ahoy_visits.user_id IN (Select users.id from users where users.loggable_type != 'Manager' )) #{s_origin} #{s_date_p}
              group by origin order by #{s_order} limit 30"
    @visit_origin_unp  = ActiveRecord::Base.connection.exec_query(query)

    unless @params_events["search_pages"].blank?
      s_pages = " AND (translate(UPPER(cast(ahoy_events.properties as varchar)), 'ÁÉÍÓÚ', 'AEIOU') ILIKE translate(UPPER(cast('%#{@params_events["search_pages"]}%' as varchar)), 'ÁÉÍÓÚ', 'AEIOU')) "
    else
      s_origin = ""
    end

    if params[:order_properties].to_i.to_i == 2 && (params[:order_count_popular].to_i == 0)
      s_order_p = "origin DESC"
    elsif params[:order_properties].to_i.to_i == 0 && (params[:order_count_popular].to_i == 2)
      s_order_p = "visit DESC"
    elsif params[:order_properties].to_i.to_i == 1 && (params[:order_count_popular].to_i == 0)
      s_order_p = "origin ASC"
    elsif params[:order_properties].to_i.to_i == 0 && (params[:order_count_popular].to_i == 1)
      s_order_p = "visit ASC"
    elsif params[:order_properties].to_i.to_i == 0 && (params[:order_count_popular].to_i == 0)
      s_order_p = "visit DESC"
    else
      s_order_p = "visit DESC"
    end

    if !@params_events["search_inicio_fech"].blank? && !@params_events["search_fin_fech"].blank?
      s_date = " AND (cast( ahoy_events.time as date) BETWEEN cast('#{Time.zone.parse(@params_events["search_inicio_fech"]).to_s}' as date) AND cast('#{Time.zone.parse(@params_events["search_fin_fech"]).to_s}' as date))"
    elsif !@params_events["search_inicio_fech"].blank? 
      s_date = " AND (cast( ahoy_events.time as date) >= cast('#{Time.zone.parse(@params_events["search_inicio_fech"]).to_s}' as date))"
    elsif !@params_events["search_fin_fech"].blank?
      s_date = " AND (cast( ahoy_events.time as date) <= cast('#{Time.zone.parse(@params_events["search_fin_fech"]).to_s}' as date))"
    else 
      s_date = ""
    end

    query = "select properties -> 'title'  as origin, count(id) as visit from ahoy_events WHERE properties IS NOT NULL #{s_pages} #{s_date} group by origin order by #{s_order_p}"
    @popular_sections_unp  = ActiveRecord::Base.connection.exec_query(query)      
  end

  def load_data
    @date_start = params[:search_start_date].blank? ? "comienzo" : Time.zone.parse(params[:search_start_date]).strftime("%d/%m/%Y")
    @date_end= params[:search_end_date].blank? ? "hoy" : Time.zone.parse(params[:search_end_date]).strftime("%d/%m/%Y")
  rescue
    @date_start = "comienzo"
    @date_end = "hoy" 
  end
end
