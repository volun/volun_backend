class Administration::AuditsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize
  
  def index
    params[:q] ||= Audit.includes(:user,:resource).preload(:user,:resource).ransack_default   

    if params[:q][:s].split(/ /)[0].to_s=="user_name"
      @search_q=@audits.includes(:user,:resource).preload(:user,:resource).joins("LEFT OUTER JOIN managers ON users.loggable_id = managers.id AND loggable_type='Manager'").joins("LEFT OUTER JOIN volunteers ON users.loggable_id = volunteers.id AND loggable_type='Volunteer'").joins("LEFT OUTER JOIN entities ON users.loggable_id = entities.id AND loggable_type='Entity'").select("audits.*,managers.name,managers.last_name,managers.last_name_alt,volunteers.name,entities.name,volunteers.last_name,volunteers.last_name_alt").search(params[:q])
      @unpaginated_audit = @search_q.result.order("CONCAT(CONCAT(managers.name,managers.last_name,managers.last_name_alt), entities.name, CONCAT(volunteers.name,volunteers.last_name,volunteers.last_name_alt)) #{params[:q][:s].split(/ /)[1]}")
    elsif params[:q][:s].split(/ /)[0].to_s=="resource"
      @search_q=@audits.includes(:user,:resource).preload(:user,:resource).joins(:resource).select("audits.*,resources.description").search(params[:q])
      @unpaginated_audit = @search_q.result.order("resources.description #{params[:q][:s].split(/ /)[1]}")
    elsif params[:q][:s].split(/ /)[0].to_s=="id"
      params[:q][:s] = "created_at desc"
      @search_q = @audits.includes(:user,:resource).preload(:user,:resource).search(params[:q])
      @search_q.sorts ||= 'created_at desc'
      @unpaginated_audit = @search_q.result.order("audits.created_at desc")
    else
      @search_q = @audits.includes(:user,:resource).preload(:user,:resource).search(params[:q])
      @search_q.sorts ||= 'created_at desc'
      @unpaginated_audit = @search_q.result.order("audits.created_at desc")
    end

    @unpaginated_audit=GlobalSearch.search_filter(@unpaginated_audit,
      [ ["ilike","audits.operation_type" , "#{params["search_operation"]}"],
        ["collective",[
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN entities ON users.loggable_id = entities.id AND loggable_type='Entity'"),"entities.name",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN managers ON users.loggable_id = managers.id AND loggable_type='Manager'"),"managers.name",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN managers ON users.loggable_id = managers.id AND loggable_type='Manager'"),"CONCAT(managers.name,' ',managers.last_name)",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN managers ON users.loggable_id = managers.id AND loggable_type='Manager'"),"CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt)",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN volunteers ON users.loggable_id = volunteers.id AND loggable_type='Volunteer'"),"volunteers.name",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN volunteers ON users.loggable_id = volunteers.id AND loggable_type='Volunteer'"),"CONCAT(volunteers.name,' ',volunteers.last_name)",:audits],
          ["join",Audit.joins(:user).joins("LEFT OUTER JOIN volunteers ON users.loggable_id = volunteers.id AND loggable_type='Volunteer'"),"CONCAT(volunteers.name,' ',volunteers.last_name,' ',volunteers.last_name_alt)",:audits],
          ["join",Audit.joins(:resource).all,"resources.description",:audits],
          ["join",Audit.joins(:resource).all,"audits.operation",:audits],
          "audits.operation"
          ],params["enter_text"]
        ],
        ["f_between",:created_at,params["search_start_date"],params["search_end_date"]],
        ["join",Audit.joins(:user).all,"users.loggable_type",:audits,(params["search_type"] unless params["search_type"].to_s=="null")],
        ["join",Audit.joins(:user).joins("LEFT OUTER JOIN entities ON users.loggable_id = entities.id AND loggable_type='Entity'"),"entities.name",:audits,params["search_manager"]],
        ["join",Audit.joins(:user).joins("LEFT OUTER JOIN managers ON users.loggable_id = managers.id AND loggable_type='Manager'"),"CONCAT(managers.name,' ',managers.last_name,' ',managers.last_name_alt)",:audits,params["search_manager"]],
        ["join",Audit.joins(:user).joins("LEFT OUTER JOIN volunteers ON users.loggable_id = volunteers.id AND loggable_type='Volunteer'"),"CONCAT(volunteers.name,' ',volunteers.last_name,' ',volunteers.last_name_alt)",:audits,params["search_manager"]],
        ["join",Audit.joins(:resource).all,"resources.description",:audits,params["search_resource"]]
      ])
    if params["search_type"].to_s == "null"
      @unpaginated_audit=@unpaginated_audit.where("audits.user_id IS NULL")
    end
    @audits = @unpaginated_audit.paginate(page: params["page"], per_page: params["per_page"]||15)
   
    respond_with(@audits) do |format|
      format.csv { stream_csv_report(@unpaginated_audit,params)  }
    end
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@audit) do |format|
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end
  
  protected

  def get_model_class
    @model_class = Audit
  end
end
