class Administration::SettingsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Setting.ransack_default   
    @search_q = View::List::Setting.all.search(params[:q])
    @search_q.sorts ||= 'key asc'

    @unpaginated_setting = @search_q.result
    @unpaginated_setting = GlobalSearch.search_filter(@unpaginated_setting, [
       ["ilike",:key,params["search_key"]],
       ["ilike",:value,params["search_value"]],
       ["collective",[ :key,:value ],params["enter_text"]]
      ])
    @settings = @unpaginated_setting.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@settings, :location => administration_settings_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@setting) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@setting, :location => administration_settings_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@setting, current_user)
    respond_with(@setting, :location => administration_settings_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@setting, setting_params, current_user)
    respond_with(@setting, :location => administration_settings_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@setting, current_user)
    respond_with(@setting, :location => administration_settings_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  protected
    def get_model_class
      @model_class = Setting
    end

    def setting_params
      params.require(:setting).permit(:key, :value)
    end
end
