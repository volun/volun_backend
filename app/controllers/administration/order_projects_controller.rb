class Administration::OrderProjectsController < ApplicationController
  before_action :authorize
  
  def index
    @main_table= ["id", "name", "date_start", "date_end", "type", "order"]
    @projects=Project.all_active_and_published.order(:order).order(:name)
  rescue => e
    log_error("COD-00001",e) 
  end

  def update
    projects_unsaved = params[:projects]
    unless projects_unsaved.blank?
      projects_unsaved.each do |key, value|
        project = Project.find(key.to_i)
        unless project.blank?
          AuditGenerateHelper.audit_update(project, {order: value.to_i} , current_user)
        end
      end
    end

    redirect_to administration_order_projects_path, notice: I18n.t('order_projects_succesfull')
  rescue => e
    log_error("COD-00002",e) 
  end
end
