class Administration::ResourcesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :csv
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Resource.ransack_default   
    @search_q = View::List::Resource.all.search(params[:q])
    @search_q.sorts ||= 'description asc'

    @unpaginated_resource = @search_q.result
    @unpaginated_resource = GlobalSearch.search_filter(@unpaginated_resource, [
        ["ilike",:description,params["search_description"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name,:description ],params["enter_text"]]
      ])
    @resources = @unpaginated_resource.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@resources, :location => administration_resources_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@resource) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@resource, :location => administration_resources_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    if AuditGenerateHelper.audit_create(@resource, current_user)
      # Manager.all.each do |manager|
      #   Permission.create(manager_id: manager.id, resource_id: @resource.id)
      # end
    end
    respond_with(@resource, :location => administration_resources_path) 
  rescue => e
    log_error("COD-00005",e)       
    flash[:alert] = I18n.t('flash.actions.create.duplicate_model')
    respond_with(@resource) 
  end

  def update
    AuditGenerateHelper.audit_update(@resource, resource_params, current_user)
    respond_with(@resource, :location => administration_resources_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@resource, current_user)
    respond_with(@resource, :location => administration_resources_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    if AuditGenerateHelper.audit_recover(@resource, current_user)
      # Manager.all.each do |manager|
      #   Permission.create(manager_id: manager.id, resource_id: @resource.id)
      # end
    end
    respond_with(@resource, :location => administration_resources_path)
  rescue => e
    log_error("COD-00008",e) 
  end
 
  protected
    def get_model_class
      @model_class = Resource
    end

    def resource_params
      params.require(:resource).permit(:description, :active, :main, :name)
    end
end
