class Administration::NewCampaingsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize

  def index
    params[:q] ||= View::List::NewCampaing.ransack_default   
    @search_q = View::List::NewCampaing.search(params[:q])
    @search_q.sorts ||= 'id asc'
    
    @unpaginated_new_campaings = @search_q.result
    @unpaginated_new_campaings =GlobalSearch.search_filter(@unpaginated_new_campaings, [
       ["strict",:id,params["search_id"]],
       ["ilike",:title,params["search_title"]],
       ["ilike",:body,params["search_body"]],
       ["ilike",:publicable,params["search_publicable"]]
      ])
    @new_campaings = @unpaginated_new_campaings.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@new_campaings, :location => administration_new_campaings_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@new_campaing) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@new_campaing, :location => administration_new_campaings_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@new_campaing, current_user)
    respond_with(@new_campaing, :location => administration_new_campaings_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@new_campaing,new_campaing_params, current_user)
    respond_with(@new_campaing, :location => administration_new_campaings_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_destroy(@new_campaing, current_user)
    respond_with(@new_campaing, :location => administration_new_campaings_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def change_public
    @new_campaing = NewCampaing.find(params[:id])
    unless @new_campaing.blank?
      @new_campaing.publicable = !@new_campaing.publicable
      if @new_campaing.save
        if @new_campaing.publicable == true
          AuditGenerateHelper.audit_publish(@new_campaing, current_user)
        else
          AuditGenerateHelper.audit_unpublish(@new_campaing, current_user)
        end
        redirect_to administration_new_campaings_path , notice: I18n.t('new_campaing.success_change')
      else
        redirect_to administration_new_campaings_path , alert: I18n.t('new_campaing.error_change')
      end
    end
  rescue => e
    log_error("COD-00008",e) 
  end

  protected
    def get_model_class
      @model_class = NewCampaing
    end

    def new_campaing_params
      params.require(:new_campaing).permit(:title, :body, :publicable)
    end
end
