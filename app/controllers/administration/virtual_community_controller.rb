class Administration::VirtualCommunityController < ApplicationController
  # load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize

  before_action :load_component, except: [:index]

  def index   
    @components = VirtualCommunity::Component.where(component_parent: nil).order(order: :asc)
  rescue => e
    log_error("COD-00001",e) 
  end

  def component
    @child_components = @component.child_components.order(order: :asc)

  rescue => e
    log_error("COD-00002",e)
  end

  def update_component
    @component.assign_attributes(virtual_community_component_strong_params)
    if @component.save
      if !@component.component_parent.blank?
        redirect_to virtual_community_component_path(id: @component.component_parent.id), notice: "Se ha actualizado correctamente el componente"
      else
        redirect_to virtual_community_path, notice: "Se ha actualizado correctamente el componente"
      end
    else
      render :component
    end
  rescue => e
    log_error("COD-00003",e)
  end


  protected
    
  def load_component
    @component=VirtualCommunity::Component.find_by(id: params[:id])
  end

  def virtual_community_component_strong_params
    params.require(:virtual_community_component).permit(
      :id,:title, :subtitle, :content, :button_active,
      :button_title,:button_alt_title,:order, :image_header)
  end

  def authorize
    if !(can? :virtual_community, WelcomeController)
      redirect_to root_path, alert: I18n.t('messages.access_denied')
    end
  end
    
end
