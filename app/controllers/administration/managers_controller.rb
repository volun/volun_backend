class Administration::ManagersController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json

  def index
    params[:q] ||= View::List::Manager.ransack_default   
    @search_q = View::List::Manager.search(params[:q])    
    @search_q.sorts ||= 'name asc'

    @unpaginated_manager = @search_q.result
    @unpaginated_manager =GlobalSearch.search_filter(@unpaginated_manager,
      [
       ["ilike",:name,params["search_nombre"]],
       ["ilike",:last_name,params["search_last_name"]],
       ["ilike",:last_name_alt,params["search_last_name_alt"]],
       ["ilike",:alias_name,params["search_alias_name"]],
       ["collective",[:name,:last_name,:last_name_alt,:alias_name],params["enter_text"]]
      ])
    @managers = @unpaginated_manager.paginate(page: params[:page], per_page: params[:per_page]||15)

    respond_with(@managers, :location => administration_managers_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@manager) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @manager = Manager.new
    respond_with(@manager, :location => administration_managers_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    if AuditGenerateHelper.audit_create(@manager, current_user)
      add_permissions
    end
    respond_with(@manager, :location => administration_managers_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update    
    if AuditGenerateHelper.audit_update(@manager,manager_params, current_user)
      add_permissions
    end

    respond_with(@manager, :location => administration_managers_path)
  # rescue => e
  #   log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@manager, current_user)
    respond_with(@manager, :location => administration_managers_path)
  rescue => e
    log_error("COD-00007",e) 
  end
  
  def recover
    AuditGenerateHelper.audit_recover(@manager, current_user)
    respond_with(@manager, :location => administration_managers_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  private  
    def get_model_class
      @model_class = Manager
    end

    def add_permissions
      Permission.generate_permissions(@manager.id)
    end

    def basic_params
      [:create,:update,:read,:destroy]
    end

    def tacking_params
      [:create,:update,:read]
    end

    def rt_params
      [:read,:reject, :approve, :process, :mark_pending, :mark_appointed, :mark_processing]
    end

    def manager_params
      params
        .require(:manager)
        .permit(
          :name,
          :last_name,
          :last_name_alt,
          :alias_name,
          :profile_id,
          :role_id,
          :phone_number,
          :active,
          {
            volunteer_pers_attributes: [
              :id,
              :section,
              data: [
                basics: basic_params,
                specifics: [
                  :sms,
                  :mail
                ]
              ]
            ]
          },
          {
            volunteer_project_attributes:[
              :id,
              :section,
              data:[
                basics: basic_params,
                specifics:[
                  :clone_project,
                  :volunteer_allowed,
                  :publish
                ]
              ]
            ]
          },
          {
            volunteer_entity_attributes:[
              :id,
              :section,
              data: basic_params
            ],
            volunteer_activity_attributes:[
              :id,
              :section,
              data: basic_params
            ],
            volunteer_citizen_attributes:[
              :id,
              :section,
              data: basic_params
            ],
            volunteer_citizen_service_attributes:[
              :id,
              :section,
              data: basic_params
            ],
            volunteer_citizen_track_attributes:[
              :id,
              :section,
              data:[
                tacking_params
              ]
            ],
            volunteer_citizen_service_track_attributes:[
              :id,
              :section,
              data:[
                tacking_params
              ]
            ],
            volunteer_ent_track_attributes:[
              :id,
              :section,
              data:[
                tacking_params
              ]
            ],
            volunteer_vol_track_attributes:[
              :id,
              :section,
              data:[
                tacking_params
              ]
            ],
            volunteer_pro_track_attributes:[
              :id,
              :section,
              data:[
                tacking_params
              ]
            ],
            volunteer_rt_publishing_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_other_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_demand_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_subscribe_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_appoint_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_act_publish_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ] 
            ],
            volunteer_rt_proj_subscribe_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_solidarity_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_rt_amends_attributes:[
              :id,
              :section,
              data:[
                rt_params
              ]
            ],
            volunteer_admin_attributes:[
              :id,
              :section,
              data:[
                :audit,
                :order,
                :config,
                :catalogs,
                :managers,
                :resource,
                :analyze_cv,
                :new_campaign,
                :virtual_community
              ]
            ],
            volunteer_digital_manage_attributes: [
              :id,
              :section,
              data:[
                :read,
                :faq,
                :topic,
                :moderate
              ]
            ]
          }
        )
    end
end
