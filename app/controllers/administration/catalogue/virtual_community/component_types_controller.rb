class Administration::Catalogue::VirtualCommunity::ComponentTypesController < ApplicationController
  #load_and_authorize_resource instance_name: :virtual_community_component_type
  respond_to :html, :js, :json
  before_action :authorize
  before_action :load_data, only: [:edit,:show,:update,:destroy,:recover]

  def index
    params[:q] ||= View::List::VirtualCommunity::ComponentType.ransack_default
    @search_q = View::List::VirtualCommunity::ComponentType.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_virtual_community_component_types = @search_q.result
    @unpaginated_virtual_community_component_types =GlobalSearch.search_filter(@unpaginated_virtual_community_component_types, [
        ["strict",:active,params["search_status"]],
        ["ilike",:title,params["search_nombre"]],
        ["ilike",:code,params["search_code"]],
        ["collective",[ :id,:title,:code ],params["enter_text"]]
      ])
    @virtual_community_component_types = @unpaginated_virtual_community_component_types.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@virtual_community_component_types, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@virtual_community_component_type) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @virtual_community_component_type = ::VirtualCommunity::ComponentType.new
    respond_with(@virtual_community_component_type, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @virtual_community_component_type = ::VirtualCommunity::ComponentType.new(virtual_community_component_type_params)
    AuditGenerateHelper.audit_create(@virtual_community_component_type, current_user)
    respond_with(@virtual_community_component_type, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@virtual_community_component_type, virtual_community_component_type_params, current_user)
    respond_with(@virtual_community_component_type, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@virtual_community_component_type, current_user)
    respond_with(@virtual_community_component_type, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@virtual_community_component_type, current_user)
    respond_with(@virtual_community_component_type, :location => administration_catalogue_virtual_community_component_types_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def load_data
    @virtual_community_component_type = ::VirtualCommunity::ComponentType.find_by(id: params[:id])
  end

  def get_model_class
    @model_class = ::VirtualCommunity::ComponentType
  end

  def virtual_community_component_type_params
    params.require(:virtual_community_component_type).permit(:id,:title, :code, :active)
  end

  alias_method :create_params, :virtual_community_component_type_params
end
