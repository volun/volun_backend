class Administration::Catalogue::CoordinationsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Coordination.ransack_default
    @search_q = View::List::Coordination.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_coordinations = @search_q.result
    @unpaginated_coordinations =GlobalSearch.search_filter(@unpaginated_coordinations, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @coordinations = @unpaginated_coordinations.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@coordinations, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00001",e)
  end

  def show
    respond_with(@coordination) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    @coordination = Coordination.new
    respond_with(@coordination, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    AuditGenerateHelper.audit_create(@coordination, current_user)
    respond_with(@coordination, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@coordination, coordination_params, current_user)
    respond_with(@coordination, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_archive(@coordination, current_user)
    respond_with(@coordination, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00007",e)
  end

  def recover
    AuditGenerateHelper.audit_recover(@coordination, current_user)
    respond_with(@coordination, :location => administration_catalogue_coordinations_path)
  rescue => e
    log_error("COD-00008",e)
  end

  protected

  def get_model_class
    @model_class = Coordination
  end

  def coordination_params
    params.require(:coordination).permit(:name, :description, :active)
  end
end
