class Administration::Catalogue::DegreesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::Degree.ransack_default
    @search_q = View::List::Degree.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_degrees = @search_q.result
    @unpaginated_degrees =GlobalSearch.search_filter(@unpaginated_degrees, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:degree_type,params["search_category"]],
        ["collective",[ :name,:degree_type ],params["enter_text"]]
      ])
    @degrees = @unpaginated_degrees.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@degrees, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@degree) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @degree = Degree.new
    respond_with(@degree, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@degree, current_user)
    respond_with(@degree, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@degree, degree_params, current_user)
    respond_with(@degree, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@degree, current_user)
    respond_with(@degree, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@degree, current_user)
    respond_with(@degree, :location => administration_catalogue_degrees_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = Degree
  end

  def degree_params
    params.require(:degree).permit(:degree_type_id, :name, :active)
  end
end
