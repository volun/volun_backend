class Administration::Catalogue::BoroughtsController < ApplicationController
    load_and_authorize_resource
    respond_to :html, :js, :json
    before_action :authorize
    
    def index
      params[:q] ||= View::List::Borought.ransack_default      
      @search_q = View::List::Borought.all.search(params[:q])
      @search_q.sorts ||= 'name asc'
  
      @unpaginated_boroughts = @search_q.result
      @unpaginated_boroughts =GlobalSearch.search_filter(@unpaginated_boroughts, [
          ["strict",:active,params["search_status"]],
          ["ilike",:name,params["search_nombre"]],
          ["collective",[ :id,:name ],params["enter_text"]]
        ])
      @boroughts = @unpaginated_boroughts.paginate(page: params["page"], per_page: params["per_page"]||15)
  
      respond_with(@boroughts, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00001",e) 
    end
  
    def show
      respond_with(@borought) do |format|
        format.js
        format.html
      end
    rescue => e
      log_error("COD-00002",e) 
    end
  
    def new
      @borought = Borought.new
      respond_with(@borought, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00003",e) 
    end
  
    def edit
    rescue => e
      log_error("COD-00004",e) 
    end
  
    def create
      AuditGenerateHelper.audit_create(@borought, current_user)
      respond_with(@borought, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00005",e) 
    end
  
    def update
      AuditGenerateHelper.audit_update(@borought, borought_params, current_user)
      respond_with(@borought, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00006",e) 
    end
  
    def destroy
      AuditGenerateHelper.audit_archive(@borought, current_user)
      respond_with(@borought, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00007",e) 
    end
  
    def recover
      AuditGenerateHelper.audit_recover(@borought, current_user)
      respond_with(@borought, :location => administration_catalogue_boroughts_path)
    rescue => e
      log_error("COD-00008",e) 
    end
  
    protected
  
    def get_model_class
      @model_class = Borought
    end

    def borought_params
      params.require(:borought).permit(:name, :code, :district_id)
    end
end