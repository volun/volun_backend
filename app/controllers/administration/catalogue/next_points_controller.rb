class Administration::Catalogue::NextPointsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::NextPoint.ransack_default    
    @search_q = View::List::NextPoint.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_next_point = @search_q.result
    @unpaginated_next_point = GlobalSearch.search_filter(@unpaginated_next_point, [
        ["strict",:id,params[:search_id]],
        ["strict",:active,params[:search_status]],
        ["ilike",:name,params[:search_nombre]],
        ["collective",[ :id,:name ],params[:enter_text]]
      ])
    @next_points = @unpaginated_next_point.paginate(page: params[:page], per_page: params[:per_page]||15)

    respond_with(@next_points, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@next_point) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@next_point, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@next_point, current_user)
    respond_with(@next_point, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@next_point, next_point_params, current_user)
    respond_with(@next_point, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@next_point, current_user)
    respond_with(@next_point, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@next_point, current_user)
    respond_with(@next_point, :location => administration_catalogue_next_points_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = NextPoint
  end

  def next_point_params
    params.require(:next_point).permit(:name, :active, :description)
  end
end
