class Administration::Catalogue::LanguagesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::Language.ransack_default    
    @search_q = View::List::Language.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_languages = @search_q.result
    @unpaginated_languages =GlobalSearch.search_filter(@unpaginated_languages, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @languages = @unpaginated_languages.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@languages, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@language) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @language = Language.new
    respond_with(@language, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@language, current_user)
    respond_with(@language, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@language, language_params, current_user)
    respond_with(@language, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@language, current_user)
    respond_with(@language, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00007",e) 
  end
  
  def recover
    AuditGenerateHelper.audit_recover(@language, current_user)
    respond_with(@language, :location => administration_catalogue_languages_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Language
  end

  def language_params
    params.require(:language).permit(:name, :active)
  end
end
