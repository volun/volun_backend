class Administration::Catalogue::ServiceQuestionsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::ServiceQuestion.ransack_default    
    @search_q = View::List::ServiceQuestion.all.search(params[:q])
    @search_q.sorts ||= 'code asc'

    @unpaginated_service_questions = @search_q.result
    @unpaginated_service_questions =GlobalSearch.search_filter(@unpaginated_service_questions, [
        ["ilike",:question_type,"#{params["search_type"]}"],
        ["strict",:active,params["search_status"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[:id,:description],params["enter_text"]]
      ])
    @service_questions = @unpaginated_service_questions.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@service_questions, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@service_question) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @service_question = ServiceQuestion.new
    respond_with(@service_question, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@service_question, current_user)
    respond_with(@service_question, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@service_question, service_question_params, current_user)
    respond_with(@service_question, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@service_question, current_user)
    respond_with(@service_question, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@service_question, current_user)
    respond_with(@service_question, :location => administration_catalogue_service_questions_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = ServiceQuestion
  end

  def service_question_params
    params.require(:service_question).permit(:description, :active, :question_type)
  end
end
