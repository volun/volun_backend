class Administration::Catalogue::ModerateStatusesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::ModerateStatus.ransack_default
    @search_q = View::List::ModerateStatus.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_moderate_statuses = @search_q.result
    @unpaginated_moderate_statuses = GlobalSearch.search_filter(@unpaginated_moderate_statuses, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:title,params["search_nombre"]],
        ["collective",[ :id,:title ],params["enter_text"]]
      ])
    @moderate_statuses = @unpaginated_moderate_statuses.paginate(page: params["page"], per_page: params["per_page"]||15)
    
    respond_with(@moderate_statuses, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@moderate_status) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @moderate_status = ModerateStatus.new
    respond_with(@moderate_status, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@moderate_status, current_user)
    respond_with(@moderate_status, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@moderate_status, moderate_status_params, current_user)
    respond_with(@moderate_status, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@moderate_status, current_user)
    respond_with(@moderate_status, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@moderate_status, current_user)
    respond_with(@moderate_status, :location => administration_catalogue_moderate_statuses_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = ModerateStatus
  end

  def moderate_status_params
    params.require(:moderate_status).permit(:title, :code, :active)
  end
end
