class Administration::Catalogue::ProfessionsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Profession.ransack_default    
    @search_q = View::List::Profession.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_profession = @search_q.result
    @unpaginated_profession = GlobalSearch.search_filter(@unpaginated_profession, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @professions = @unpaginated_profession.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@professions, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00001",e)
  end

  def show
    respond_with(@profession) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    @profession = Profession.new
    respond_with(@profession, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    AuditGenerateHelper.audit_create(@profession,  current_user)
    respond_with(@profession, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@profession, profession_params, current_user)
    respond_with(@profession, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_archive(@profession,  current_user)
    respond_with(@profession, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00007",e)
  end

  def recover
    AuditGenerateHelper.audit_recover(@profession,  current_user)
    respond_with(@profession, :location => administration_catalogue_professions_path)
  rescue => e
    log_error("COD-00008",e)
  end

  protected

  def get_model_class
    @model_class = Profession
  end

  def profession_params
    params.require(:profession).permit(:name, :active)
  end
end
