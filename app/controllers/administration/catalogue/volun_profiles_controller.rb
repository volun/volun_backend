class Administration::Catalogue::VolunProfilesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize

  def index
    params[:q] ||= View::List::VolunProfile.ransack_default    
    @search_q = View::List::VolunProfile.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_volun_profiles = @search_q.result
    @unpaginated_volun_profiles =GlobalSearch.search_filter(@unpaginated_volun_profiles, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @volun_profiles = @unpaginated_volun_profiles.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@volun_profiles, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@volun_profile) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @volun_profile = VolunProfile.new
    respond_with(@volun_profile, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@volun_profile, current_user)
    respond_with(@volun_profile, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@volun_profile, volun_profile_params, current_user)
    respond_with(@volun_profile, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@volun_profile, current_user)
    respond_with(@volun_profile, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@volun_profile, current_user)
    respond_with(@volun_profile, :location => administration_catalogue_volun_profiles_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = VolunProfile
  end

  def volun_profile_params
    params.require(:volun_profile).permit(:name, :active)
  end
end
