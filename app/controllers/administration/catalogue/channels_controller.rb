class Administration::Catalogue::ChannelsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Channel.ransack_default
    @search_q = View::List::Channel.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_channels = @search_q.result
    @unpaginated_channels = GlobalSearch.search_filter(@unpaginated_channels, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:code,params["search_code"]],
        ["collective",[ :id,:name,:code ],params["enter_text"]]
      ])
    @channels = @unpaginated_channels.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@channels, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@channel) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @channel = Channel.new
    respond_with(@channel, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@channel, current_user)
    respond_with(@channel, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@channel, channel_params, current_user)
    respond_with(@channel, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@channel, current_user)
    respond_with(@channel, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@channel, current_user)
    respond_with(@channel, :location => administration_catalogue_channels_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = Channel
  end

  def channel_params
    params.require(:channel).permit(:name, :code, :active)
  end
end
