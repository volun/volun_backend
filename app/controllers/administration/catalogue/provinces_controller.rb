class Administration::Catalogue::ProvincesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::Province.ransack_default      
    @search_q = View::List::Province.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_provinces = @search_q.result
    @unpaginated_provinces =GlobalSearch.search_filter(@unpaginated_provinces, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name,:code ],params["enter_text"]]
      ])
    @provinces = @unpaginated_provinces.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@provinces, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@province) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@province, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@province, current_user)
    respond_with(@province, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@province, province_params, current_user)
    respond_with(@province, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@province, current_user)
    respond_with(@province, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@province, current_user)
    respond_with(@province, :location => administration_catalogue_provinces_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Province
  end

  def province_params
    params.require(:province).permit(:name, :code, :active)
  end
end
