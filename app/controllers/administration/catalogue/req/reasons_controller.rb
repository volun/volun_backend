class Administration::Catalogue::Req::ReasonsController < ApplicationController
  #load_and_authorize_resource instance_name: :req_reason
  respond_to :html, :js, :json
  before_action :authorize
  before_action :load_data, only: [:edit,:show,:update,:destroy,:recover]

  def index
    params[:q] ||= View::List::Req::Reason.ransack_default
    @search_q = View::List::Req::Reason.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_req_reasons = @search_q.result
    @unpaginated_req_reasons =GlobalSearch.search_filter(@unpaginated_req_reasons, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @req_reasons = @unpaginated_req_reasons.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@req_reasons, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@req_reason) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @req_reason = ::Req::Reason.new
    respond_with(@req_reason, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @req_reason = ::Req::Reason.new(req_reason_params)
    AuditGenerateHelper.audit_create(@req_reason, current_user)
    respond_with(@req_reason, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@req_reason, req_reason_params, current_user)
    respond_with(@req_reason, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@req_reason, current_user)
    respond_with(@req_reason, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@req_reason, current_user)
    respond_with(@req_reason, :location => administration_catalogue_req_reasons_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def load_data
    @req_reason = ::Req::Reason.find_by(id: params[:id])
  end

  def get_model_class
    @model_class = ::Req::Reason
  end

  def req_reason_params
    params.require(:req_reason).permit(:name, :description, :active)
  end

  alias_method :create_params, :req_reason_params
end
