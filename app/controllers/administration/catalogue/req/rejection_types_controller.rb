class Administration::Catalogue::Req::RejectionTypesController < ApplicationController
 
  respond_to :html, :js, :json
  before_action :authorize
  before_action :load_data, only: [:edit,:show,:update,:destroy,:recover]

  def index
    params[:q] ||= View::List::Req::RejectionType.ransack_default 
    @search_q = View::List::Req::RejectionType.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_req_rejection_types = @search_q.result
    @unpaginated_req_rejection_types =GlobalSearch.search_filter(@unpaginated_req_rejection_types, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @req_rejection_types = @unpaginated_req_rejection_types.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@req_rejection_types, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show    
    respond_with(@req_rejection_type) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @req_rejection_type = ::Req::RejectionType.new
    respond_with(@req_rejection_type, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    @req_rejection_type = ::Req::RejectionType.new(req_rejection_type_params)
    AuditGenerateHelper.audit_create(@req_rejection_type, current_user)
    respond_with(@req_rejection_type, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@req_rejection_type, req_rejection_type_params, current_user)
    respond_with(@req_rejection_type, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@req_rejection_type, current_user)
    respond_with(@req_rejection_type, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00007",e) 
  end
  
  def recover
    AuditGenerateHelper.audit_recover(@req_rejection_type, current_user)
    respond_with(@req_rejection_type, :location => administration_catalogue_req_rejection_types_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def load_data
    @req_rejection_type = ::Req::RejectionType.find_by(id: params[:id])
  end

  def get_model_class
    @model_class = ::Req::RejectionType
  end
    
  def req_rejection_type_params
    params.require(:req_rejection_type).permit(:name, :description, :active)
  end

  alias_method :create_params, :req_rejection_type_params
end
