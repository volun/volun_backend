class Administration::Catalogue::ShiftDefinitionsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  before_action :authorize

  def index
    params[:q] ||= View::List::ShiftDefinition.ransack_default   
    @search_q = View::List::ShiftDefinition.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_shift_definitions = @search_q.result
    @unpaginated_shift_definitions = GlobalSearch.search_filter(@unpaginated_shift_definitions, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @shift_definitions = @unpaginated_shift_definitions.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@shift_definitions, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00001",e) 
  end
  
  def show
    respond_with(@shift_definitions) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @shift_definition = ShiftDefinition.new
    respond_with(@shift_definition, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@shift_definition, current_user)
    respond_with(@shift_definition, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@shift_definition, shift_definition_params, current_user)
    respond_with(@shift_definition, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@shift_definition, current_user)
    respond_with(@shift_definition, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@shift_definition, current_user)
    respond_with(@shift_definition, :location => administration_catalogue_shift_definitions_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = ShiftDefinition
  end

  def shift_definition_params
    params.require(:shift_definition).permit(:name, :description, :active)
  end
end
