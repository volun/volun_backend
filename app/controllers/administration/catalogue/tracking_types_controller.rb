class Administration::Catalogue::TrackingTypesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::TrackingType.ransack_default    
    @search_q = View::List::TrackingType.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_tracking_types = @search_q.result
    @unpaginated_tracking_types =GlobalSearch.search_filter(@unpaginated_tracking_types, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:alias_name,params["search_alias"]],
        ["collective",[ :id,:name,:alias_name ],params["enter_text"]]
      ])
    @tracking_types = @unpaginated_tracking_types.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@tracking_types, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@tracking_type) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @tracking_type = TrackingType.new
    respond_with(@tracking_type, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@tracking_type, current_user)
    respond_with(@tracking_type, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@tracking_type, tracking_type_params, current_user)
    respond_with(@tracking_type, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@tracking_type, current_user, true)
    respond_with(@tracking_type, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@tracking_type, current_user, true)
    respond_with(@tracking_type, :location => administration_catalogue_tracking_types_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = TrackingType
  end

  def tracking_type_params
    params.require(:tracking_type).permit(:name, :active,:tracking_types_type)
  end
end
