class Administration::Catalogue::NationalitiesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Nationality.ransack_default
    @search_q = View::List::Nationality.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_nationalities = @search_q.result
    @unpaginated_nationalities =GlobalSearch.search_filter(@unpaginated_nationalities, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @nationalities = @unpaginated_nationalities.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@nationalities,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@nationality) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @nationality = Nationality.new
    respond_with(@nationality,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@nationality, current_user)
    respond_with(@nationality,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@nationality, nationality_params, current_user)
    respond_with(@nationality,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@nationality, current_user)
    respond_with(@nationality,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@nationality, current_user)
    respond_with(@nationality,:location => administration_catalogue_nationalities_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Nationality
  end

  def nationality_params
    params.require(:nationality).permit(:name, :active)
  end
end
