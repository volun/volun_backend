class Administration::Catalogue::UnsubscribeReasonsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::UnsubscribeReason.ransack_default
    @search_q = View::List::UnsubscribeReason.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_unsubscribe_reasons = @search_q.result
    @unpaginated_unsubscribe_reasons =GlobalSearch.search_filter(@unpaginated_unsubscribe_reasons, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @unsubscribe_reasons = @unpaginated_unsubscribe_reasons.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@unsubscribe_reasons,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@unsubscribe_reason) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @unsubscribe_reason = UnsubscribeReason.new
    respond_with(@unsubscribe_reason,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@unsubscribe_reason, current_user)
    respond_with(@unsubscribe_reason,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@unsubscribe_reason, unsubscribe_reason_params, current_user)
    respond_with(@unsubscribe_reason,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@unsubscribe_reason, current_user)
    respond_with(@unsubscribe_reason,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@unsubscribe_reason, current_user)
    respond_with(@unsubscribe_reason,:location => administration_catalogue_unsubscribe_reasons_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = UnsubscribeReason
  end

  def unsubscribe_reason_params
    params.require(:unsubscribe_reason).permit(:name, :active)
  end
end
