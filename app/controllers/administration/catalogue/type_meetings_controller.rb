class Administration::Catalogue::TypeMeetingsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::TypeMeeting.ransack_default
    @search_q = View::List::TypeMeeting.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_type_meetings = @search_q.result
    @unpaginated_type_meetings = GlobalSearch.search_filter(@unpaginated_type_meetings, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[ :id,:name, :description ],params["enter_text"]]
      ])
    @type_meetings = @unpaginated_type_meetings.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@type_meetings, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00001",e)    
  end

  def show
    respond_with(@type_meeting) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    @type_meeting = TypeMeeting.new
    respond_with(@type_meeting, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    AuditGenerateHelper.audit_create(@type_meeting, current_user)
    respond_with(@type_meeting, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@type_meeting,type_meeting_params, current_user)
    respond_with(@type_meeting, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_archive(@type_meeting, current_user)
    respond_with(@type_meeting, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00007",e)
  end

  def recover
    AuditGenerateHelper.audit_recover(@type_meeting, current_user)
    respond_with(@type_meeting, :location => administration_catalogue_type_meetings_path)
  rescue => e
    log_error("COD-00008",e)
  end

  protected

  def get_model_class
    @model_class = TypeMeeting
  end

  def type_meeting_params
    params.require(:type_meeting).permit(:name, :description, :active)
  end
end
