class Administration::Catalogue::AreasController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Area.ransack_default
    @search_q = View::List::Area.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_areas = @search_q.result
    @unpaginated_areas = GlobalSearch.search_filter(@unpaginated_areas, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @areas = @unpaginated_areas.paginate(page: params["page"], per_page: params["per_page"]||15)
    
    respond_with(@areas, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@area) do |format|
      format.js 
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @area = Area.new
    respond_with(@area, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@area, current_user)
    respond_with(@area, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@area, area_params, current_user)
    respond_with(@area, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@area, current_user)
    respond_with(@area, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@area, current_user)
    respond_with(@area, :location => administration_catalogue_areas_path)
  rescue => e
    log_error("COD-00008",e) 
  end
  
  protected

  def get_model_class
    @model_class = Area
  end

  def area_params
    params.require(:area).permit(:name, :description, :active)
  end
end
