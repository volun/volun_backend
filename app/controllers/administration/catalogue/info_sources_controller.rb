class Administration::Catalogue::InfoSourcesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::InfoSource.ransack_default    
    @search_q = View::List::InfoSource.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_info_sources = @search_q.result
    @unpaginated_info_sources =GlobalSearch.search_filter(@unpaginated_info_sources, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @info_sources = @unpaginated_info_sources.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@info_sources, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@info_source) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @info_source = InfoSource.new
    respond_with(@info_source, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@info_source, current_user)
    respond_with(@info_source, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@info_source, info_source_params, current_user)
    respond_with(@info_source, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@info_source, current_user)
    respond_with(@info_source, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@info_source, current_user)
    respond_with(@info_source, :location => administration_catalogue_info_sources_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = InfoSource
  end

  def info_source_params
    params.require(:info_source).permit(:name, :active)
  end
end
