class Administration::Catalogue::ServicesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Service.ransack_default    
    @search_q = View::List::Service.all.search(params[:q])
    @search_q.sorts ||= 'code asc'

    @unpaginated_services = @search_q.result
    @unpaginated_services = GlobalSearch.search_filter(@unpaginated_services, [
        ["strict",:code,params["search_code"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["ilike",:description,params["search_description"]],
        ["collective",[ :id,:name, :description, :code ],params["enter_text"]]
      ])
    @services = @unpaginated_services.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@services, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@service) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @service = Service.new
    respond_with(@service, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@service, current_user)
    respond_with(@service, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@service, service_params, current_user)
    respond_with(@service, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@service, current_user)
    respond_with(@service, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@service, current_user)
    respond_with(@service, :location => administration_catalogue_services_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Service
  end

  def service_params
    params.require(:service).permit(:name,:code,:description, :active, :is_digital,:mta_order,:normal_flow)
  end
end
