class Administration::Catalogue::SkillsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize

  def index
    params[:q] ||= View::List::Skill.ransack_default   
    @search_q = View::List::Skill.all.search(params[:q])
    @search_q.sorts ||= 'id asc'

    @unpaginated_skill = @search_q.result
    @unpaginated_skill =GlobalSearch.search_filter(@unpaginated_skill, [
        ["strict",:id,params["search_id"]],
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"]]
      ])
    @skills = @unpaginated_skill.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@skills, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@skill) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @skill = Skill.new
    respond_with(@skill, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@skill, current_user)
    respond_with(@skill, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@skill, skill_params, current_user)
    respond_with(@skill, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@skill, current_user)
    respond_with(@skill, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@skill, current_user)
    respond_with(@skill, :location => administration_catalogue_skills_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Skill
  end

  def skill_params
    params.require(:skill).permit(:name, :active)
  end
end
