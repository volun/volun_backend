class Administration::Catalogue::GendersController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Gender.ransack_default
    @search_q = View::List::Gender.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_genders = @search_q.result
    @unpaginated_genders = GlobalSearch.search_filter(@unpaginated_genders, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[:id,:name],params["enter_text"]]
      ])
    @genders = @unpaginated_genders.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@genders,:location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00001",e) 
  end

  def show
    respond_with(@gender) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    @gender = Gender.new
    respond_with(@gender, :location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  rescue => e
    log_error("COD-00004",e) 
  end

  def create
    AuditGenerateHelper.audit_create(@gender, current_user)
    respond_with(@gender, :location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00005",e) 
  end

  def update
    AuditGenerateHelper.audit_update(@gender, gender_params, current_user)
    respond_with(@gender, :location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00006",e) 
  end

  def destroy
    AuditGenerateHelper.audit_archive(@gender, current_user)
    respond_with(@gender, :location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00007",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@gender, current_user)
    respond_with(@gender, :location => administration_catalogue_genders_path)
  rescue => e
    log_error("COD-00008",e) 
  end

  protected

  def get_model_class
    @model_class = Gender
  end

  def gender_params
    params.require(:gender).permit(:name, :active)
  end
end