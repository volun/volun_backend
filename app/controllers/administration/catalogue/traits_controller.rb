class Administration::Catalogue::TraitsController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js, :json
  before_action :authorize
  
  def index
    params[:q] ||= View::List::Trait.ransack_default    
    @search_q = View::List::Trait.all.search(params[:q])
    @search_q.sorts ||= 'name asc'

    @unpaginated_traits = @search_q.result
    @unpaginated_traits = GlobalSearch.search_filter(@unpaginated_traits, [
        ["strict",:active,params["search_status"]],
        ["ilike",:name,params["search_nombre"]],
        ["collective",[ :id,:name ],params["enter_text"] ]
      ])
    @traits = @unpaginated_traits.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@traits, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00001",e)
  end

  def show
    respond_with(@trait) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e)
  end

  def new
    @trait = Trait.new
    respond_with(@trait, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00003",e)
  end

  def edit
  rescue => e
    log_error("COD-00004",e)
  end

  def create
    AuditGenerateHelper.audit_create(@trait, current_user)
    respond_with(@trait, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00005",e)
  end

  def update
    AuditGenerateHelper.audit_update(@trait, trait_params, current_user)
    respond_with(@trait, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00006",e)
  end

  def destroy
    AuditGenerateHelper.audit_archive(@trait, current_user)
    respond_with(@trait, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00007",e)
  end

  def recover
    AuditGenerateHelper.audit_recover(@trait, current_user)
    respond_with(@trait, :location => administration_catalogue_traits_path)
  rescue => e
    log_error("COD-00008",e)
  end

  protected

  def get_model_class
    @model_class = Trait
  end

  def trait_params
    params.require(:trait).permit(:name, :active)
  end
end
