class MassiveEmailHistoricsController < ApplicationController
    load_and_authorize_resource
    respond_to :html, :js

   
    def show
        respond_with(@massive_email_historic) do |format|
        format.js #{ render 'shared/popup' }
        format.html
        end
    end
end