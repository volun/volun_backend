class ProjectsController < ApplicationController
  include PtController

  load_and_authorize_resource
  before_action :set_pt_extension, only: [:show, :new, :edit, :create, :update, :clone_project]

  def index
    if params[:id_check]
      proyecto=Project.find_by(id: params[:id_check])
      proyecto.review_end_project = true
      proyecto.save(validate: false)

      pvolunteers=ProjectsVolunteer.where(project_id: proyecto.id)     
      pvolunteers.each do |vol|          
        vol.active = false
        vol.unsubscribe_date = Time.now.strftime("%d/%m/%Y").to_date

        vol.save(validate: false)
      end
    end

    params[:q] ||= Project.ransack_default    
    @search_q = @projects.unscoped.search(params[:q])
    @search_q.sorts ||= 'id asc'
    @unpaginated_projects = @search_q.result.with_status(params["search_status"])
    @unpaginated_projects =GlobalSearch.search_filter(@unpaginated_projects, [
      ["strict","projects.id", params["search_id"]],
      ["strict","projects.pt_extendable_id", params["search_pt_id"]],
      ["ilike","concat(projects.father_id,'.',projects.pt_order)", params["search_subpt_id"]],
      ["collective",[
        "projects.name","projects.description","projects.pt_extendable_type",
        ["join",Project.joins(:addresses=>[:district]).all,"districts.name",:projects]
        ], params["enter_text"]
      ],
      ["strict","projects.volunteers_allowed", (true unless ( params["search_volun_allowed"].blank?|| params["search_volun_allowed"].to_s=="false"))],
      ["strict","projects.outstanding",(true unless ( params["search_outstanding"].blank?|| params["search_outstanding"].to_s=="false"))],
      ["strict","projects.publish",(true unless ( params["search_publish"].blank?|| params["search_publish"].to_s=="false"))],
      ["strict","projects.urgent",(true unless ( params["search_urgent"].blank?|| params["search_urgent"].to_s=="false"))],
      ["ilike","projects.name", params["search_nombre"]],
      ["ilike","projects.pt_extendable_type", params["search_tipo"]],
      ["ilike","projects.subtype_pt", params["search_subproyect_pt"]],
      ["ilike","projects.subtype_proyect", params["search_subproyect_type"]],
      ["strict","projects.year", params["search_year"]],
      ["f_between_between","projects.execution_start_date", params["search_inicio_f"],"projects.execution_end_date", params["search_fin_f"]],
      ["strict","projects.entity_id", params["search_entidad"]],
      ["join",Project.joins(:areas).all,"areas.name",:projects, params["search_ambito"]],
      ["join",Project.joins(:collectives).all,"collectives.name",:projects, params["search_colectivo"]]
      ])
    
    if params["search_review"].to_s=="true"
      @unpaginated_projects=@unpaginated_projects.all_not_review
    end
    unless  params["search_district"].blank?
      have_address = Project.joins(:addresses).all.distinct.select("projects.id")
      adrs_district = Project.joins(:addresses).all.where("addresses.district_id in (?)",params["search_district"]).distinct.select("projects.id")
      adrs_pc = Project.joins(:addresses).all.where("addresses.postal_code NOT LIKE (?)","280%").distinct.select("projects.id")
      if  params["search_district"]=="OTROS"
        @unpaginated_projects=@unpaginated_projects.where("projects.district_id = ? AND projects.id NOT IN (?) OR projects.id IN (?)",params["search_district"],have_address,adrs_pc)
      else
        @unpaginated_projects=@unpaginated_projects.where("projects.district_id = ?  AND projects.id NOT IN (?) OR projects.id IN (?)",params["search_district"],have_address,adrs_district)
      end
    end
    @projects = @unpaginated_projects.paginate(page:  params[:page] || params[:page], per_page:  params[:per_page] || params[:per_page] ||15)

    respond_to do |format|
      format.html
      format.csv { stream_csv_report(@unpaginated_projects,params)  }
      format.js
      format.json { render json: { urgent: Project.urgent_projects, outstanding: Project.outstanding_projects } }
    end    
  rescue => e
    log_error("COD-00001",e) 
    redirect_to root_path, alert: "Ha habido un error en la página"
  end

  def show
    respond_with(@project) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00002",e) 
  end

  def new
    respond_with(@project)
  rescue => e
    log_error("COD-00003",e) 
  end

  def edit
  end

  def create
    AuditGenerateHelper.audit_create(@project, current_user)
    track_custom(current_user.loggable_id, @project.id, 'subscribe')
    respond_with(@project)
  rescue => e
    log_error("COD-00004",e) 
  end

  def update
    #======================================================
    # Modificación para los casos de revisión y ampliación de plazos
    #======================================================
    if !@project.execution_end_date.blank? && @project.execution_end_date>=Date.today && @project.active
      @project.review_end_project = false
    end    
    #======================================================
    
    old = @project.suspended
    AuditGenerateHelper.audit_update(@project,project_params, current_user)
    set_suspended_pt(old,@project)
    respond_with(@project)
  rescue => e
    log_error("COD-00005",e) 
  end

  def destroy
    @project = Project.find(params[:id])
    if @project.active
    AuditGenerateHelper.audit_archive(@project, current_user, true)
    track_custom(current_user.loggable_id, @project.id, 'unsubscribe')
    end
    respond_with(@project)
  rescue => e
    log_error("COD-00006",e) 
  end

  def remove_project
    ActiveRecord::Base.connection.exec_query("DELETE from projects WHERE id= #{@project.id}")
    AuditGenerateHelper.audit_destroy(@project, current_user, true)
    redirect_to projects_path, notice: I18n.t('success_project_removed', name: @project.name)
  rescue => e
    log_error("COD-00007",e) 
    redirect_to projects_path, alert: "No se ha podido eliminar el proyecto"
  end

  def volun_unlink
    @project = Project.find_by(id: params[:id])
    ActiveRecord::Base.connection.exec_query("DELETE FROM projects_volunteers where projects_volunteers.project_id=#{@project.id}")
    AuditGenerateHelper.audit_unlink_all(@project, current_user)
    redirect_to projects_path, notice: I18n.t('success_volunteers_unlinked')
  rescue => e
    log_error("COD-00008",e) 
  end

  def recover
    AuditGenerateHelper.audit_recover(@project, current_user, true)
    respond_with(@project)
  rescue => e
    log_error("COD-00009",e) 
  end

  def pre_clone
    @proyect = Project.find_by(id: params[:proyect])
    set_projects_matriz
    respond_with(@project) do |format|
      format.js
      format.html
    end
  rescue => e
    log_error("COD-00010",e) 
  end

  def clone_project     
    @project=Project.find(params[:id])
    @project_clone=Project.find_by(name: params[:search_name], year: (@project.year.to_i+1))

    if @project_clone.blank? 
      @resultados = CloneProject.clone_project(@project, params[:search_name], params[:clone_with_volunteers], current_user, params[:matriz]) 
      @title = I18n.t('clone_success.success_project_cloned', name: params[:project])
      error = false
      success= false
      @resultados.each do |res|
        if res[0] == 1 
          error = true
        end
        if res[0]==0 
          success=true
        end
      end
      if error && success
        @title = I18n.t('clone_success.success_self_project_cloned', name: params[:project])
      elsif error && !success
        @title = I18n.t('clone_errors.error_project_cloned')   
      end
    else
      @error_other= I18n.t('clone_errors.other_project_cloned', {id_original: @project.id, id_clone: @project_clone.id})
      @title = I18n.t('clone_errors.error_project_cloned')   
    end
    respond_with(@project) do |format|
      format.js
      format.html
    end
  end

  def pt_extensions_menu
    @first_level_order = array_first_type_projects
  rescue => e
    log_error("COD-00012",e) 
  end
  
  protected

    def set_pt_extension
      @pt_extension = params[:pt_extension]
    end

    def get_model_class
      @model_class = Project
    end
   
    def project_params
      params.require(:project).permit(project_attributes)
    end

    def change_entity_vat_number
      @entity_vat_number=change_entity_vat_number_process(params, @project.try(:entity).try(:vat_number))
    end

    def load_data
      set_project_types(array_first_type_projects)
      set_districts
      set_areas
      set_collectives
      set_entities
      set_subtipes_project
      set_projects_matriz
      set_subtipes_pt
    end
    
end
