class EntitiesController < ApplicationController
  load_and_authorize_resource
  respond_to :html, :js
  include AddressManager
  
  def index
    @hash_tipos={}
    @tipos_entidades=EntityType.all.order(:name)
    @tipos_entidades.each do |tipo|
      @hash_tipos.merge!({tipo.name => tipo.id })
    end   
    params[:q] ||= View::List::Entity.ransack_default 
    @search_q = View::List::Entity.all.search(params[:q])

    @search_q.sorts ||= 'id asc'
    @unpaginated_entities = @search_q.result.with_status(params["search_status"])
    @unpaginated_entities =GlobalSearch.search_filter(@unpaginated_entities,
      [
       ["ilike",:name,params["search_nombre"]],
       ["collective",[:name,:vat_number,:email,:entity_type],params["enter_text"]],
       ["strict",:entity_type_id,params["search_tipo"]],
       ["ilike",:vat_number,params["search_cif"]]
    ])
    @entities = @unpaginated_entities.paginate(page: params["page"], per_page: params["per_page"]||15)

    respond_with(@unpaginated_entities) do |format|
    format.csv { stream_csv_report(@unpaginated_entities,params)  }    end
  end

  def show
    respond_with(@entity) do |format|
      format.js
      format.html
    end
  end

  def new
    @entity = Entity.new
    respond_with(@entity)
  end

  def edit
  end

  def create
    avoid_phone_numbers_mask
    if AuditGenerateHelper.audit_create(@entity, current_user)
      create_and_assign_user_to_entity!(@entity, params[:entity_notice_type])
      assign_subscribe_date!(@entity)
      register_ent_subs_tracking!(@entity)
    end
    respond_with(@entity)
  end

  def update
    previously_inactive = !@entity.active
    avoid_phone_numbers_mask
    if AuditGenerateHelper.audit_update(@entity, entity_params, current_user)
      create_and_assign_user_to_entity!(@entity, params[:entity_notice_type])
      if params[:entity][:unsubscribed_at].present?
        register_ent_unsubs_tracking!(@entity)
      elsif params[:entity][:unsubscribed_at].nil? && previously_inactive
        register_ent_subs_tracking!(@entity)
        assign_subscribe_date!(@entity)
        unassign_unsubscribe_date!(@entity)
      end
    end
    respond_with(@entity)
  end

  def destroy
    if AuditGenerateHelper.audit_archive(@entity, current_user)
      begin
        assign_unsubscribe_date!(@entity)
        register_ent_unsubs_tracking!(@entity)
      rescue
      end
    end
    redirect_to entities_path
  end

  def recover
    if AuditGenerateHelper.audit_recover(@entity, current_user)
      begin
        unassign_unsubscribe_date!(@entity)
        register_ent_subs_tracking!(@entity)
      rescue
      end
    end
    redirect_to entities_path, notice: t('messages.succesfully_recovered')
  end

  def new_password
    @entity = Entity.find(params[:id])
  end

  def ent_password
    @entity = Entity.find(params[:id])
    if !@entity.phone_number.blank? && @entity.phone_number.to_s.match(/[9|8]\d{8}/).blank?
      @phone_number = @entity.phone_number
    elsif !@entity.phone_number_alt.blank? && @entity.phone_number_alt.to_s.match(/[9|8]\d{8}/).blank?
      @phone_number = @entity.phone_number_alt
    else
      @phone_number = ""
    end
    respond_with(@entity) do |format|
      format.js #{ render 'shared/popup_password' }
      format.html
    end
  end

  def update_password
    if params[:cadena_aleatoria].blank?
      redirect_to "/entities/#{params[:id]}/edit", alert: "No se ha introducido la contraseña"
      return
    else

      subject = I18n.t('change_password.subject_entity')
      message = I18n.t('change_password.message', email: params[:email], password: params[:cadena_aleatoria]).html_safe
      message_sms = I18n.t('change_password.message_sms', password: params[:cadena_aleatoria]).html_safe


      if message.strip == ""
        redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('change_password.alert_mail_cv')
      elsif (params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true") &&  params[:email].blank?#Si está checked 'Enviar por correo'
        redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('alert_mail')
      elsif (params[:send_sms].to_s == "1" || params[:send_sms].to_s=="true") && (params[:phone_number].blank? || !params[:phone_number].to_s.match(/[9|8]\d{8}/).blank?) #Si está checked 'Enviar por sms'
        redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('alert_message')
      else
        @entity= Entity.find(params[:id])
        @entity.email = params[:email]
        @entity.phone_number_alt = params[:phone_number]

        @user= @entity.user
        if @user.blank?
          @user = User.new(login: "userentity#{'%09d' % @entity.id}", loggable: @entity, notice_type: NoticeType.email.take)
          @user.loggable = @entity
         # @entity.user = @user
        end

        @user.email = params[:email]
        @user.mobile_phone = params[:phone_number]
        @user.password = params[:cadena_aleatoria]
        @user.password_confirmation = params[:cadena_aleatoria]
        unless @user.save
          redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('alert_not_user')
        else
          AuditGenerateHelper.reset_password(@entity, current_user)
        

          unless @entity.save(validate: false)
            redirect_to "/entities/#{params[:id]}/edit",  alert: @entity.errors.full_messages#I18n.t('alert_not_entity')
          else
            AuditGenerateHelper.reset_password(@entity, current_user)
        
          
            #Envio de mail si está 1
            if params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true" 
              begin
                VolunteerMailer.send_email(params[:recipient].presence || @entity.email, message: message, subject:subject).deliver_now
                AuditGenerateHelper.audit_send_mail(@entity, current_user)
              rescue Exception  => e
                redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('alert_mail_sending')
                return
              end
            end

            #Envio de sms si está 1
            if params[:send_sms].to_s == "1" || params[:send_sms].to_s=="true" 
              sms_number =  params[:phone_number]
              begin
                SMSApi.new.sms_deliver(sms_number, message_sms)
                AuditGenerateHelper.audit_send_sms(@entity, current_user)        
              rescue Exception  => e
                redirect_to "/entities/#{params[:id]}/edit", alert: I18n.t('alert_sms_sending')
                return
              end
            end
            redirect_to "/entities/#{params[:id]}/edit", notice: I18n.t('success_message_sending') ##Pasa al formulario de edición de la entidad
          end
        end
      end
    end
  end
  
  protected

    def entity_params
      params
        .require(:entity)
        .permit(
          :name,
          :description,
          :vat_number,
          :email,
          :representative_name,
          :representative_last_name,
          :representative_last_name_alt,
          :contact_name,
          :contact_last_name,
          :contact_last_name_alt,
          :phone_number,
          :phone_number_alt,
          :publish_pictures,
          :annual_survey,
          :req_reason_id,
          :entity_type_id,
          :comments,
          :other_subscribe_reason,
          :active,
          :subscribed_at,
          :unsubscribed_at,
          {
            address_attributes: address_attributes
          },
          {
            projects_attributes: [
              :id,
              :active
            ]
          },
          {
            logo_attributes: [
              :id,
              :link_type_id,
              :file,
              :_destroy
            ]
          },
          {
            images_attributes: [
              :id,
              :link_type_id,
              :file,
              :description,
              :_destroy
            ]
          },
          {
            docs_attributes: [
              :id,
              :link_type_id,
              :file,
              :description,
              :_destroy
            ]
          },
          {
            urls_attributes: [
              :id,
              :path,
              :link_type_id,
              :file,
              :description,
              :_destroy
            ]
          },
        )
    end

    private

    def create_and_assign_user_to_entity!(entity, notice_type_id_param)
      ActiveRecord::Base.transaction do
        notice = NoticeType.kinds_i18n.key(notice_type_id_param)
        user =User.find_by(loggable_type: "Entity", loggable_id: entity.id) 
        if user.nil? 
          user = User.new(login: "userentity#{'%08d' % 0}#{entity.name}", loggable: entity)
          user.password = Digest::SHA1.hexdigest("#{entity.created_at.to_s}--#{user.login}")[0,8]
          user.password_confirmation = user.password
          user.email = entity.email#"#{user.login}.entity@volun.es"
          if notice.present?
            user.notice_type_id = NoticeType.find_by(description: notice).try(:id) if notice_type_id_param.present?
          else
            user.notice_type_id = nil
          end
          copy_errors_from!(entity, user) unless user.save

        else
          if notice.present?
            user.notice_type_id = NoticeType.find_by(description: notice).try(:id) if notice_type_id_param.present?
          else
            user.notice_type_id = nil
          end
          copy_errors_from!(entity, user) unless user.save
        end
        unless user.blank? 
          copy_errors_from!(entity, user) unless user.update(email: entity.email, mobile_phone: entity.phone_number_alt) 
        end 
      end
    end

    def register_ent_subs_tracking!(entity)
      default_attrs = {
        entity:        entity,
        manager_id:    current_user.loggable_id,
        tracking_type: TrackingType.get_volunteer_subscribe,
        comments:      I18n.t('trackings.entity_subscribe'),
        tracked_at:    DateTime.now,
        automatic:     true,
      }
      ActiveRecord::Base.transaction do
        tracking = Ent::Tracking.new(default_attrs)
        copy_errors_from!(entity, tracking) unless tracking.save
      end
    end

    def register_ent_unsubs_tracking!(entity)
      default_attrs = {
        entity:        entity,
        manager_id:    current_user.loggable_id,
        tracking_type: TrackingType.get_volunteer_unsubscribe,
        comments:      I18n.t('trackings.entity_unsubscribe'),
        tracked_at:    DateTime.now,
        automatic:     true,
      }
      ActiveRecord::Base.transaction do
        tracking = Ent::Tracking.new(default_attrs)
        copy_errors_from!(entity, tracking) unless tracking.save
      end
    end

    def assign_subscribe_date!(entity)
      entity.subscribed_at = Time.now
      entity.save
    end

    def assign_unsubscribe_date!(entity)
      entity.unsubscribed_at = Time.now
      entity.save
    end

    def unassign_unsubscribe_date!(entity)
      entity.unsubscribed_at = nil
      entity.save
    end

    def copy_errors_from(entity, record)
      entity.errors.add(:base, record.errors.full_messages.to_sentence)
      nil
    end

    def copy_errors_from!(entity, record)
      copy_errors_from(entity, record)
      raise ActiveRecord::Rollback
    end

    def avoid_phone_numbers_mask
      if params[:entity][:phone_number] == "_________"
        params[:entity][:phone_number] = nil
      end
      if params[:entity][:phone_number_alt] == "_________"
        params[:entity][:phone_number_alt] = nil
      end
    end

end
