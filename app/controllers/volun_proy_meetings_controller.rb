class VolunProyMeetingsController < ApplicationController

    load_and_authorize_resource
    respond_to :html, :js
    #before_action :load_search , only: [:index]

    def index
        @project_id = params[:project_id]
        @project = Project.find(@project_id)

        params[:q] ||= VolunProyMeeting.ransack_default
        params[:q].each do |key, value|
        params[:q].delete(key) if value.blank?
        end
        
        params[:q] = VolunProyMeeting.ransack_default if params[:q].blank?
        @search_q = @volun_proy_meetings.search(params[:q])
        @search_q.sorts ||= 'name asc'

        @volun_proy_meetings = @search_q.result
        @volun_proy_meetings = @volun_proy_meetings.where(project_id: params[:project_id]) unless params[:project_id].blank?


        respond_with(@volun_proy_meetings)
    end
    
    def show
        respond_with(@volun_proy_meetings) do |format|
        format.js #{ render 'shared/popup' }
        format.html
        end
    end

    def new
        @volun_proy_meeting = VolunProyMeeting.new
        respond_with(@volun_proy_meeting)
    end

    def edit
    end

    def create
        if !@volun_proy_meeting.blank?
            if AuditGenerateHelper.audit_create(@volun_proy_meeting, current_user)
                redirect_to meetings_path(project_id: @volun_proy_meeting.project_id), notice: I18n.t('success_message_sending')
            else
                respond_with(@volun_proy_meeting)
            end
        end
    end

    def update
        if AuditGenerateHelper.audit_update(@volun_proy_meeting, volun_proy_meeting_params, current_user)
            redirect_to meetings_path(project_id: @volun_proy_meeting.project_id), notice: I18n.t('success_message_sending')
        else
            respond_with(@volun_proy_meeting)
        end
    end
    
    protected
        def volun_proy_meeting_params
            params.require(:volun_proy_meeting).permit(:project_volunteer_id, :confirm, :asist)
        end
end
