class VolunteersController < ApplicationController
  include CommonController
  include AddressManager
  load_and_authorize_resource

  require 'csv'
  require 'stringio'

  attr_accessor :manager, :volunteer_projects_relation

  before_action :set_class_style, only: [:new,:create] 
  before_action :carga_projects, only: [:new,:create,:edit,:update] 
  before_action :set_class_other_style, only: [:edit,:update]
  before_action :load_data, only: [:index, :massive_email, :derivatie]
  before_action :load_mask, only: [:create,:update]

  def index
    @project = !params[:project_id].blank? ? Project.find_by(id: params[:project_id]) : nil
    params[:search_status] ||= (params[:clean] && params[:project_id].blank? || !params[:project_id].blank?) ? "all" : ""

   params[:q] ||= View::List::Volunteer.ransack_default   
    params[:q][:projects_id_eq] = nil
    params[:q][:pt_extendable_id] = nil
    params[:q][:s] ||='updated_at desc'

  

    @search_q = View::List::Volunteer.search(params[:q])
    
    @search_q.sorts ||= 'updated_at desc'
    @unpaginated_volunteers = @search_q.result 

    if params[:q][:s].blank?
      @unpaginated_volunteers = @unpaginated_volunteers.reorder(updated_at: :desc)
    end
    @unpaginated_volunteers=GlobalSearch.search_filter(@unpaginated_volunteers,
      [["strict",:next_point_id , params[:search_next_point]],
      ["ilike",:postal_code,params["search_postal_code"] ],
      ["strict",:id_autonum , params[:search_id]],      
        ["collective",[
          ["full",:name,:last_name],
          ["full",:name,:last_name,:last_name_alt],
          :district_custom,:name,:last_name,:last_name_alt,:id_number,:postal_code,:phone_number,
          :phone_number_alt,:email,:other_academic_info
          ],params["enter_text"]
        ],
        ["ilike",:id_number, params["search_nif"]],
        ["ilike",:name, params["search_name"]],
        ["ilike",:last_name, params["search_last_name"]],
        ["ilike",:last_name_alt, params["search_last_name_alt"]],
        ["ilike",:phone_number_alt, params["search_movil"]],
        ["f_between",:birth_date,params["search_start_date"],params["search_end_date"]],       
        ["join",Volunteer.joins(:info_source).all,"info_sources.name",:view_list_volunteers,(params["search_info_source"] if (!params["search_info_source"].blank? && params["search_info_source"].to_s!="null"))]
      ])
    
    if !params["search_district"].blank? && params["search_district"]!="null"      
      @unpaginated_volunteers=@unpaginated_volunteers.where("district_id::varchar IN (?)",params["search_district"].split(','))
    end
    if !params["search_incluence_district"].blank? && params["search_incluence_district"]!="null"      
      @unpaginated_volunteers=@unpaginated_volunteers.where("district_id::varchar IN (?)",params["search_incluence_district"] .split(','))
    end

    if !params["search_area"].blank? && params["search_area"]!="null"   
      @unpaginated_volunteers=@unpaginated_volunteers.where("id in (?)", Area.joins("INNER JOIN areas_volunteers on areas.id=areas_volunteers.area_id").where("areas.name in (?)",params["search_area"].split(',')).select("areas_volunteers.volunteer_id"))
    end

    if !params["search_collective"].blank? && params["search_collective"]!="null"   
      @unpaginated_volunteers=@unpaginated_volunteers.where("id in (?)", Collective.joins("INNER JOIN collectives_volunteers on collectives.id=collectives_volunteers.collective_id").where("collectives.name in (?)",params["search_collective"].split(',')).select("collectives_volunteers.volunteer_id"))
    end

    if !params["search_volun_profile"].blank? && params["search_volun_profile"]!="null"   
      @unpaginated_volunteers=@unpaginated_volunteers.where("id in (?)", VolunProfile.joins("INNER JOIN volun_profiles_volunteers on volun_profiles.id=volun_profiles_volunteers.volun_profile_id").where("volun_profiles.name in (?)",params["search_volun_profile"].split(',')).select("volun_profiles_volunteers.volunteer_id"))
    end

    if !params["search_project_link_start_date"].blank? && !params["search_project_link_end_date"].blank?
      @unpaginated_volunteers= @unpaginated_volunteers.trackings_projects_automatic_between(Volunteer.period_active(params["search_project_link_start_date"],params["search_project_link_end_date"]))
    elsif !params["search_project_link_start_date"].blank?
      @unpaginated_volunteers=@unpaginated_volunteers.trackings_projects_automatic_start(params["search_project_link_start_date"])
    elsif !params["search_project_link_end_date"].blank?
      @unpaginated_volunteers=@unpaginated_volunteers.trackings_projects_automatic_end(params["search_project_link_end_date"])
    end
    if !params["search_location"].blank? 
      @unpaginated_volunteers=@unpaginated_volunteers.where("postal_code #{params["search_location"].to_i>=0 ? "" : "NOT"} LIKE (?)","#{params[:search_location].to_i.abs.to_s.gsub("_"," ")}%")
    end
    
    @unpaginated_volunteers=@unpaginated_volunteers.where("birth_date > cast(? as date) AND birth_date IS NOT NULL OR cession_of_personal_data= false OR publication_image_media= false OR publication_image_ooaa= false OR publication_image_social_network=false OR publication_image_ayto=false",Date.today-18.years) if params["search_without_auth"].to_s=="true"
    @unpaginated_volunteers=@unpaginated_volunteers.where(accompany_volunteer: true) if params["search_accompany_volunteer"].to_s=="true"
    @unpaginated_volunteers=@unpaginated_volunteers.where(ec: true) if params["ec"].to_s=="true"
    @unpaginated_volunteers=@unpaginated_volunteers.where("0=(select count(*) from projects_volunteers where projects_volunteers.volunteer_id=view_list_volunteers.id)").distinct if params["no_asign"].to_s=="true"
    @unpaginated_volunteers=@unpaginated_volunteers.all_availabilities if params["new_availability"].to_s=="true"

    unless params["search_availabilities_day"].blank?
      sentence_search_availabilities_day=search_text_split(params["search_availabilities_day"], "day=")
      @unpaginated_volunteers=@unpaginated_volunteers.where("id IN (?)",Volun::Availability.all.where("#{sentence_search_availabilities_day}").distinct.select("volun_availabilities.volunteer_id")) if !sentence_search_availabilities_day.blank?
    end
    unless params["search_type"].blank?
      sentence_search_type=search_text_split(params["search_type"], "projects.pt_extendable_type=")
      @unpaginated_volunteers=@unpaginated_volunteers.where("0<(select count(*) from projects_volunteers,projects where projects_volunteers.volunteer_id=view_list_volunteers.id AND projects.id=projects_volunteers.project_id AND #{sentence_search_type})").distinct if !sentence_search_type.blank?
    end

    if !params['search_tester'].blank? && params['search_tester'].to_s == "true" && params['search_no_tester'].to_s != "true"
      @unpaginated_volunteers=@unpaginated_volunteers.where("tester=true")
    end

    if !params['search_no_tester'].blank? && params['search_no_tester'].to_s == "true" && params['search_tester'].to_s != "true"
      @unpaginated_volunteers=@unpaginated_volunteers.where("tester=false")
    end

    @unpaginated_volunteers=@unpaginated_volunteers.not_market

    if !params["search_start_age"].blank? && !params["search_end_age"].blank?
      @unpaginated_volunteers = @unpaginated_volunteers.where("age_name BETWEEN ? and ?",params["search_start_age"].to_i,params["search_end_age"].to_i)       
    elsif !params["search_start_age"].blank? && params["search_end_age"].blank?
      @unpaginated_volunteers = @unpaginated_volunteers.where("age_name >= ?",params["search_start_age"].to_i)        
    elsif params["search_start_age"].blank? && !params["search_end_age"].blank?
      @unpaginated_volunteers = @unpaginated_volunteers.where("age_name <= ?",params["search_end_age"].to_i)        
    end
    

    if !@project.blank? 
      @unpaginated_volunteers=@unpaginated_volunteers.joins("INNER JOIN projects_volunteers as pv ON pv.volunteer_id=view_list_volunteers.id")

      if !params["search_status"].blank? && params["search_status"].to_s != "all"
        @unpaginated_volunteers=@unpaginated_volunteers.where("pv.active=?",params["search_status"].to_s=="active" ) 
      end            
      if @project.subtype_proyect == "matriz"
        # if @project.active && @project.active_execution_dates
          # VOLUN-511 Descomentar distinct
          @unpaginated_volunteers=@unpaginated_volunteers.where("(pv.project_id = ? OR pv.project_id in (?)) and (pv.volunteer_type != 'DERIVATIE' or pv.volunteer_type is null)", @project.id,  @project.subprojects.ids)#.distinct
        # else 
        #   @unpaginated_volunteers=@unpaginated_volunteers.where("(pv.project_id = ? OR pv.active=true and project_id in (?)) and (pv.volunteer_type != 'DERIVATIE' or pv.volunteer_type is null)", @project.id,  @project.subprojects.where("case when execution_end_date is null then cast(? as date) >=cast(execution_start_date as date) ELSE cast(? as date) between cast(execution_start_date as date) and cast(execution_end_date as date) END", Time.zone.now,Time.zone.now).ids)
        # end
      else
        @unpaginated_volunteers=@unpaginated_volunteers.where("(project_id = ?) and (pv.volunteer_type!= 'DERIVATIE' or pv.volunteer_type is null)", @project.id)
      end
    else
      @unpaginated_volunteers=@unpaginated_volunteers.with_status(params[:search_status])
    end 

   
    @volunteers = @unpaginated_volunteers.paginate(page: params["page"], per_page: params["per_page"]||15)
    respond_to do |format|
      format.html
      format.js
      format.json { render json: Degree.filter_by_degree_type_id(params[:dt_id]).to_json }
      format.csv  { stream_csv_report(@unpaginated_volunteers,params) }
    end   
  end

  def derivatie
    @project = Project.find_by(id: params[:project_id])

    params[:q] ||= View::List::Derivatie.ransack_default
    @search_q = View::List::Derivatie.search(params[:q])
    @search_q.sorts ||= 'updated_at desc'
    @unpagined_projectv = @search_q.result
    if @project.subtype_proyect == "matriz"
      if @project.active && @project.active_execution_dates
        @unpagined_projectv = @unpagined_projectv.where("(project_id = ? OR project_id in (?)) and volunteer_type=?",@project.id,  @project.subprojects.ids ,"DERIVATIE")
      else
        @unpagined_projectv = @unpagined_projectv.where("(project_id = ? OR pv_active=true and project_id in (?)) and volunteer_type=?",@project.id,  @project.subprojects.where("case when execution_end_date is null then cast(? as date) >=cast(execution_start_date as date) ELSE cast(? as date) between cast(execution_start_date as date) and cast(execution_end_date as date) END", Time.zone.now,Time.zone.now).ids ,"DERIVATIE")
      end
    else
      @unpagined_projectv = @unpagined_projectv.where("project_id = ? and volunteer_type= 'DERIVATIE'",@project.id)
    end

    @unpagined_projectv=GlobalSearch.search_filter(@unpagined_projectv,
      [
        ["collective",[
          ["full","name","last_name"],
          ["full","name","last_name","last_name_alt"],
          "name","last_name","last_name_alt","id_number",
          "phone_number_alt","email"
          ],params["enter_text"]
        ],
        ["strict","id_autonum", params["search_id"]],
        ["ilike","id_number", params["search_nif"]],
        ["ilike","name", params["search_name"]],
        ["ilike","last_name", params["search_last_name"]],
        ["ilike","last_name_alt", params["search_last_name_alt"]],
        ["ilike","phone_number_alt", params["search_phone"]],
        ["ilike","email", params["search_email"]]
      ])

    if !params["search_status"].blank? && params["search_status"].to_s != "all"
      @unpagined_projectv=@unpagined_projectv.where("pv_active=?",params["search_status"].to_s=="active") 
    end 

    @projectv = @unpagined_projectv.distinct.paginate(page: params["page"], per_page: params["per_page"]||15)
    
    respond_to do |format|
      format.html
      format.js
      format.json { render json: @degreeSearch.to_json }
      format.csv  { stream_csv_report_derivatie(Volunteer.where('volunteers.id IN (?)', @unpagined_projectv.select(:volunteer_id)),params) }
    end
  end

  def show
    @manager = current_user
    @volunteer.unsubscribe_reason_id = params[:reason].to_i unless params[:reason].blank?
    @volunteer.unsubscribe_date= params[:data_end] unless params[:data_end].blank?
    respond_with(@volunteer) do |format|
      format.js
      format.html
      format.pdf do
        pdf = UnsubscribeVolunteer.new(@volunteer, @manager)
        send_data pdf.render, filename: 'unsubscribe_volunteer.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  def new    
    volunteer_manager = VolunteerManager.new(rt_volunteer_subscribe_id: params[:rt_volunteer_subscribe_id])
    if volunteer_manager.valid?
      @volunteer = volunteer_manager.build_volunteer
      respond_with( @volunteer)
    else
      redirect_to rt_volunteer_subscribes_path, alert: volunteer_manager.errors.to_sentence
    end
  end

  def edit   
    @path_before = request.referrer
  end

  def create
    exists_volunter=Volunteer.find_by("volunteers.id_number=? AND volunteers.active=false",params[:volunteer][:id_number])
   
    if !exists_volunter.blank?
      redirect_to action: 'new', recover: exists_volunter.id, rt_volunteer_subscribe_id: params[:rt_volunteer_subscribe_id]
    else
      volunteer_manager = VolunteerManager.new(rt_volunteer_subscribe_id: params[:rt_volunteer_subscribe_id],
        volunteer_attributes: volunteer_params,
        manager_id: current_user.loggable_id)
      volunteer_manager.create_volunteer(current_user)
      
      if volunteer_manager.respond_with_volunteer?
        @volunteer = volunteer_manager.volunteer
        AuditGenerateHelper.audit_request_accept(@volunteer,current_user) unless params[:rt_volunteer_subscribe_id].blank?
        respond_with(@volunteer)
      else
        redirect_to rt_volunteer_subscribes_path, alert: volunteer_manager.errors.to_sentence
      end
    end
  end

  def update    
    session[:return_to] ||= request.referer
    volunteer_manager = VolunteerManager.new(rt_volunteer_amendment_id: params[:rt_volunteer_amendment_id],
                                            rt_volunteer_unsubscribe_id: params[:rt_volunteer_unsubscribe_id],
                                            volunteer: @volunteer,
                                            volunteer_attributes: volunteer_params,
                                            manager_id: current_user.loggable_id)
    volunteer_manager.update_volunteer(current_user)
    if volunteer_manager.respond_with_volunteer?
      if volunteer_manager.errors.count.to_i>0
          @other_error = I18n.t('errors.address_error_normalice') if volunteer_manager.errors[0] == "Address base La dirección no está normalizada."
      else
        if @volunteer.unsubscribe_date == nil
          if !@volunteer.active? #si no está activo, pone active=true
            AuditGenerateHelper.audit_recover(@volunteer,current_user, true)
            volunteer_manager.register_recover_volunteer
          end
        else
          if @volunteer.active?  #si está activo, pone active=false
            AuditGenerateHelper.audit_archive(@volunteer, current_user, true)
            volunteer_manager.register_destroy_volunteer
          end
        end
      end

      #ActiveRecord::Base.connection.exec_query("DELETE from addresses WHERE id= #{@address_id_old}") unless @address_id_old.blank?
      unless params[:project].blank? 
        redirect_to project_volunteers_path(params[:project]) 
      else
         respond_with(@volunteer = volunteer_manager.volunteer)
      end
      AuditGenerateHelper.audit_request_accept(@volunteer, current_user) unless params[:rt_volunteer_amendment_id].blank?
      
    else
      redirect_to rt_volunteer_unsubscribes_path, alert: volunteer_manager.errors.to_sentence
    end
  end

  def destroy  
    @volunteer = Volunteer.find(@volunteer.id)  
    if @volunteer.active
    AuditGenerateHelper.audit_archive(@volunteer,current_user, true)
    volunteer_manager = VolunteerManager.new(volunteer: @volunteer, manager_id: current_user.loggable_id)
    volunteer_manager.register_destroy_volunteer
    end
    respond_with(@volunteer)
  end

  def recover
    @volunteer = Volunteer.find(@volunteer.id)
    if !@volunteer.active
    exists_volunter=Volunteer.where("volunteers.email=?",@volunteer.email).count
    if exists_volunter.to_i>0
      @volunteer.email=""
      @volunteer.save
    end
    AuditGenerateHelper.audit_recover(@volunteer,current_user, true)
    volunteer_manager = VolunteerManager.new(volunteer: @volunteer, manager_id: current_user.loggable_id)
    volunteer_manager.register_recover_volunteer
    flash[:notice]= I18n.t('volunteer_recover') if volunteer_manager
    end
    redirect_to volunteers_path
    #respond_with(@volunteer = volunteer_manager.volunteer)
  end  

  def unlink_project_volunteer
    type=""
    if !params[:project_id].blank? && !params[:id].blank? && !@volunteer.blank? &&  !@volunteer.id.blank?
      @pv = ProjectsVolunteer.find_by(project_id: params[:project_id], volunteer_id: @volunteer.id)
      type = @pv.try(:volunteer_type)
      derivatie_relation = VolunteerProjectsRelation.find_by(project_id: params[:project_id], volunteer: @volunteer, type_relation: type, end_date: nil)
      unless derivatie_relation.blank?
        derivatie_relation.end_date = Date.today
        derivatie_relation.save
      end
      ActiveRecord::Base.connection.exec_query("DELETE FROM projects_volunteers where projects_volunteers.project_id= #{params[:project_id]}
        AND projects_volunteers.volunteer_id = #{params[:id]}")
      @volunteer.trackings.where(project_id: params[:project_id]).each {|t| t.destroy} 
      flash[:notice]= I18n.t('volunteer_unlinked')
      AuditGenerateHelper.audit_unlink(@volunteer, current_user)
    end
    
    if type == "DERIVATIE"
      redirect_to derivatie_project_volunteers_path(params[:project_id] ||{},q:{projects_id_eq: params[:project_id]  },pt_extendable_id: params[:pt_extendable_id])
    else
      redirect_to project_volunteers_path(params[:project_id]||{},q:{projects_id_eq: params[:project_id] },pt_extendable_id:params[:pt_extendable_id], asociated: true)
    end
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to project_volunteers_path(params[:project_id]||{},q:{projects_id_eq: params[:project_id] },pt_extendable_id:params[:pt_extendable_id], asociated: true)
  end

  def new_password
    @volunteer = Volunteer.find(params[:id]) 
  end

  def volun_password
    @volunteer = Volunteer.find(params[:id]) 
    if !@volunteer.phone_number.blank? && @volunteer.phone_number.to_s.match(/[9|8]\d{8}/).blank?
      @phone_number = @volunteer.phone_number
    elsif !@volunteer.phone_number_alt.blank? && @volunteer.phone_number_alt.to_s.match(/[9|8]\d{8}/).blank?
      @phone_number = @volunteer.phone_number_alt
    else
      @phone_number = ""
    end

    respond_with(@volunteer) do |format|
      format.js# { render 'shared/popup_password' }
      format.html
    end
  end

  def update_password
    volunteer= Volunteer.find(params[:id])
    if params[:cadena_aleatoria].blank?
      redirect_to edit_volunteer_path(volunteer),alert: "No se ha introducido ninguna contraseña"
    else
      email = params[:email]
      pass = params[:cadena_aleatoria]
      sms = params[:phone_number]    

      if pass.blank?
        redirect_to edit_volunteer_path(volunteer), alert: I18n.t('change_password.alert_mail_cv')
      elsif (params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true") && (email.blank? || email.to_s.match(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i).blank? )
        redirect_to edit_volunteer_path(volunteer), alert: I18n.t('alert_mail')
      elsif (params[:send_sms].to_s == "1" || params[:send_sms].to_s == "true") && (sms.blank? || !sms.to_s.match(/[9|8]\d{8}/).blank? )
        redirect_to edit_volunteer_path(volunteer), alert: I18n.t('alert_message')
      else
       
        subject = I18n.t('change_password.subject_volunteer')

        if volunteer.user.blank?
          user = User.new(login: "uservolunteer#{'%09d' % volunteer.id}", loggable: volunteer, notice_type: NoticeType.email.take)
          begin
            volunteer.user = user
          rescue
          end          
        else 
          user = volunteer.user
        end

        user.email = volunteer.email
        user.mobile_phone = volunteer.phone_number_alt
        user.password = params[:cadena_aleatoria]
        user.password_confirmation = params[:cadena_aleatoria]       
        user.loggable = volunteer 

        if user.save(validate: false)
          volunteer.save(validate: false)

          AuditGenerateHelper.reset_password(volunteer, current_user)
          begin
            volunteer_manager = VolunteerManager.new(volunteer: volunteer, manager_id: current_user.loggable_id)
            volunteer_manager.register_generate_passwod_volunteer
          rescue
          end

          #Envio de mail si está 1
          if params[:send_mail].to_s == "1" || params[:send_mail].to_s=="true" 
            message = I18n.t('change_password.message', email: email, password: pass).html_safe
            begin
              VolunteerMailer.send_email(params[:recipient].presence || email, message: message, subject: subject).deliver_now
              AuditGenerateHelper.audit_send_mail(volunteer, current_user)
            rescue Exception  => e
              redirect_to edit_volunteer_path(volunteer), alert: I18n.t('alert_mail_sending')
              return
            end
          end

          #Envio de sms si está 1
          if params[:send_sms].to_s == "1" || params[:send_sms].to_s=="true"
            message = I18n.t('change_password.message_sms', password: pass).html_safe
            begin
              SMSApi.new.sms_deliver(sms, message)
              AuditGenerateHelper.audit_send_sms(volunteer, current_user)
            rescue Exception  => e
              redirect_to edit_volunteer_path(volunteer), alert: I18n.t('alert_sms_sending')
              return
            end
          end
          redirect_to edit_volunteer_path(volunteer), notice: I18n.t('success_message_sending')
        else
          redirect_to edit_volunteer_path(volunteer),alert: I18n.t('alert_not_user')
        end
      end
    end
  end

  def show_sms
    @volunteer = Volunteer.find_by(id: params[:volunteer])
    @sms_number = @volunteer.mobile_number
   
    if !@sms_number.blank?
      respond_with(@volunteer) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    else
      render js: "swal( '#{t('alert_title')}','#{t('alert_message')}','error')"
    end
  end

  def send_sms
    @volunteer = Volunteer.find_by(id: params[:volunteer])
    sms_number =  @volunteer.mobile_number
    begin
      SMSApi.new.sms_deliver(sms_number, params[:message])
      AuditGenerateHelper.audit_send_sms(@volunteer, current_user)
      redirect_to volunteers_path, notice: I18n.t('success_message_sending')
    rescue Exception  => e
      Rails.logger.error('VolunteersController#send_sms') do
        "#{I18n.t('sms.error')}: \n#{e}"
      end
      redirect_to volunteers_path, alert: I18n.t('alert_message_sending')
    end
  end

  def show_mail
    @volunteer = Volunteer.find_by(id: params[:volunteer])
    @mail_address = @volunteer.email
    if @mail_address
      respond_with(@volunteer) do |format|
        format.js #{ render 'shared/popup' }
        format.html
      end
    else
      render js: "swal( '#{t('alert_title')}','#{t('alert_mail')}','error')"
    end
  end

  def send_mail
    @volunteer = Volunteer.find_by(id: params[:volunteer])
    begin
      VolunteerMailer.send_email(params[:recipient].presence || @volunteer.email, message: params[:message], subject: params[:subject]).deliver_now
      AuditGenerateHelper.audit_send_mail(@volunteer, current_user)
      redirect_to volunteers_path, notice: I18n.t('success_message_sending')
    rescue Exception  => e
      Rails.logger.error('VolunteersController#send_mail') do
        "#{I18n.t('mailer.error')}: \n#{e}"
      end
      redirect_to volunteers_path, alert: I18n.t('alert_message_sending')
    end
  end

  def show_unsubscribe
    @unsubscribes_reasons=UnsubscribeReason.all
    @hash_reason={}
    @unsubscribes_reasons.each {|reason|@hash_reason.merge!({reason.name=>reason.id})} 
    @volunteer = Volunteer.find_by(id: params[:id])

    respond_with(@volunteer) do |format|
      format.js #{ render 'shared/popup_unsubscribe' }
      format.html
    end
  end

  def accept_unsubscribe
    request.format= :html
    @volunteer = Volunteer.find_by(id: params[:volunteer])
    if @volunteer.active
      @volunteer.unsubscribe_reason_id=params[:search_reason].to_i
      @volunteer.status_id=nil
      @volunteer.unsubscribe_date=params[:search_fecha_baja]

      @volunteer.save(validate: false)
      AuditGenerateHelper.audit_archive(@volunteer,current_user, true)
      volunteer_manager = VolunteerManager.new(volunteer: @volunteer, manager_id: current_user.loggable_id)
      volunteer_manager.register_destroy_volunteer(input_comments: params[:search_comments])
    end
    redirect_to volunteers_path,turbolinks: false, data: {:turbolinks => false}, notice: I18n.t('volunteer_archived')      
  end

  def show_project_volunteer
    @alta=params[:alta]
    @alta ||= false
     @volunteer = Volunteer.find(params[:id])
     @project= Project.find(params[:project_id])
     @update=params[:update_volun]
     @orden=params[:orden]
     @derivatie = ProjectsVolunteer.find_by(project_id: params[:project_id], volunteer_id: params[:id]).try(:volunteer_type) == "DERIVATIE"
  
    respond_with(@volunteer) do |format|
      format.js
      format.html
    end
  end

  def set_project_volunteer
    volunteer= Volunteer.find(params[:id]) 
    results = ProjectsVolunteer.where(project_id: params[:project_id], volunteer_id: params[:id])

    if results.count.to_i>0
      if params[:orden].to_i==0
        pvolunteer = ProjectsVolunteer.find_by(project_id: params[:project_id], volunteer_id: volunteer.id)
        pvolunteer.active = false
        pvolunteer.comments=params[:search_comments] || ""
        pvolunteer.unsubscribe_date=Date.today
        AuditGenerateHelper.audit_unassignment(@volunteer, current_user) if pvolunteer.save
      else
        pvolunteer = ProjectsVolunteer.find_by(project_id: params[:project_id], volunteer_id: volunteer.id)
        pvolunteer.active = true
        pvolunteer.comments=params[:search_comments] || ""
        pvolunteer.subscribe_date=Date.today
        AuditGenerateHelper.audit_assignment(@volunteer, current_user) if pvolunteer.save
      end  
    end
    volunteer_manager = VolunteerManager.new(volunteer: volunteer, manager_id: current_user.loggable_id) 
    volunteer_manager.register_project_volunteer(current_user: current_user.loggable_id,project: params[:project_id],
    input_comments: params[:search_comments], input_orden: params[:orden]) 
    if params[:update_volun].blank? 
      redirect_to project_volunteers_path(params[:project_id],q:{projects_id_eq: params[:project_id] })
    else
      redirect_to edit_volunteer_path(volunteer,project: params[:project_id],q:{projects_id_eq: params[:project_id] }) 
    end
  end

  def show_project_derivaty
    derivaty = DerivatiesVolunteer.find_by(volunteer_id: params[:volunteer_id],project_id:params[:project_id])
    derivaty.active = !derivaty.active
    derivaty.save
    if params[:update_volun].blank? 
      redirect_to project_volunteers_path(params[:project_id],q:{projects_id_eq: params[:project_id] }) 
    else 
      redirect_to edit_volunteer_path(derivaty.volunteer,project: params[:project_id],q:{projects_id_eq: params[:project_id] }) 
    end 
  end

  def set_project_derivaty
  end

  def massive_email
    params[:q] ||= {s: 'updated_at desc'}
    params[:q].each do |key, value|
      params[:q].delete(key) if value.blank?
    end

    @volunteers = Volunteer.all.not_market
    @massive_email_historics = MassiveEmailHistoric.all

    
    params[:q] = {s: 'updated_at desc'} if params[:q].blank?
    @search_q_volunteers = @volunteers.search(params[:q])
    @search_q_volunteers.sorts ||= 'id asc'
    @volunteers = @search_q_volunteers.result

    @search_q_massive = @massive_email_historics.search(params[:q])
    @search_q_massive.sorts ||= 'id asc'
    @massive_email_historics = @search_q_massive.result

    @volunteers = @volunteers.where("email is not NULL AND email != ''")

    @volunteers = GlobalSearch.search_filter(@volunteers,
      [ ["join",Address.joins(:volunteers).all,:postal_code,:volunteers,(params[:search_postal_code] if (!params[:search_postal_code].blank? && params[:search_postal_code].to_s!="null"))],])
    if (!params[:search_situation].blank? && params[:search_situation].to_s!="null")
      @volunteers=@volunteers.where("volunteers.status_id IS NULL OR volunteers.status_id NOT IN (#{params[:search_situation]})") 
    end

    @volunteers=@volunteers.where(vocne: true) if !params[:search_vocne].blank? && params[:search_vocne]
    if !params[:search_district].blank? && params[:search_district].to_s != "null"
      aux_district=params[:search_district]
      sentence=""
      aux_district = aux_district.split(',')
      aux_district.each do |ad|
        if !ad.blank? && ad.to_s != "null"
          if sentence.blank?
            sentence= "("
            ad=="-280" ? sentence= sentence + "addresses.postal_code NOT LIKE ('280%')" : sentence= sentence + "addresses.district_id in (#{ad.to_s.gsub("_"," ")})"       
          else
            ad=="-280" ? sentence= sentence + " OR addresses.postal_code NOT LIKE ('280%')" : sentence= sentence + " OR addresses.district_id in (#{ad.to_s.gsub("_"," ")})"       
          end
        end
      end
      sentence = sentence + ")" unless sentence.blank?
      @volunteers=@volunteers.where("volunteers.id IN (?)",Address.joins(:volunteers).all.where("#{sentence}").distinct.select("volunteers.id")) unless sentence.blank?
    end    


    @volunteers = @volunteers.paginate(page: !params[:new_email].blank? && params[:new_email] || params[:historic].blank? ? params["page"] : 1, per_page: params["per_page"]||10)

    @massive_email_historics = @massive_email_historics.paginate(page: !params[:historic].blank? && params[:historic] ? params["page"] : 1, per_page: params["per_page"]||10)

  end

  def send_massive_email
    limit_size_attach = 14
    aux = 0
    if !params[:upload].blank?
      params[:upload].each { |file| aux = aux + File.size(file.tempfile)}
    end

    if params[:title].blank? || params[:content].try{|x| x.strip}.blank?
      redirect_to massive_email_volunteers_path, alert: "Falta introducir los campos obligatorios del asunto y/o del cuerpo del mensaje"
    elsif  aux > limit_size_attach.megabyte
      redirect_to massive_email_volunteers_path, alert: "El tamaño de los archivos adjuntos es demasiado grande debe ser menor o igual a #{limit_size_attach} Mb"
    else
      aux_filter = ''

      volunteers = Volunteer.where("email is not NULL AND email != ''")

      volunteers = GlobalSearch.search_filter(volunteers,
        [ ["join",Address.joins(:volunteers).all,:postal_code,:volunteers,(params[:search_postal_code] if (!params[:search_postal_code].blank? && params[:search_postal_code].to_s!="null"))],])
      if (!params[:search_situation].blank? && params[:search_situation].to_s!="null")

        Status.where("id IN (?)",params[:search_situation]).each do |s|
          aux_filter = aux_filter + "- Situación #{s.name}<br>"
        end

        volunteers=volunteers.where("volunteers.status_id IS NULL OR volunteers.status_id NOT IN (?)",params[:search_situation]) 
      end
      volunteers=volunteers.where(vocne: true) if params[:search_vocne].to_s=="1"
      unless params[:search_district].blank?
        aux_district=params[:search_district]
        sentence=""
        aux_district.each do |ad|
          if !ad.blank? && ad.to_s != "null"          
            aux_filter = aux_filter + "- #{ad=="-280" ? "Distrito OTROS" :  "Distrito #{ad}"}<br>"
            if sentence.blank?
              sentence= "("
              ad=="-280" ? sentence= sentence + "addresses.postal_code NOT LIKE ('280%')" : sentence= sentence + " addresses.district_id = #{ad.to_s.gsub("_"," ")}"       
            else
              ad=="-280" ? sentence= sentence + " OR addresses.postal_code NOT LIKE ('280%')" : sentence= sentence + " OR addresses.district_id = #{ad.to_s.gsub("_"," ")}"       
            end
          end
        end
        sentence = sentence + ")" unless sentence.blank?
        volunteers=volunteers.where("volunteers.id IN (?)",Address.joins(:volunteers).all.where("#{sentence}").distinct.select("volunteers.id")) unless sentence.blank?
      end
      require 'base64'
      aux_filter = aux_filter + "- VOCNE<br>" if params[:search_vocne].to_s=="1"
      aux_filter = aux_filter + "- Código postal: #{params[:search_postal_code]}<br>" if (!params[:search_postal_code].blank? && params[:search_postal_code].to_s!="null")

      attach =[]   
      if !params[:upload].blank?
        params[:upload].each do |file|
          attach.push({
            original_filename: file.original_filename,
            tempfile: Base64.encode64(File.read(file.tempfile.path))
          })
        end
      end

      begin
        MassiveEmailsJob.perform_later(current_user.id, volunteers.map(&:id),params[:title], params[:content].gsub("src=\"/system/","src=\"#{get_cv_environment}system/"), attach, aux_filter)
        redirect_to massive_email_volunteers_path, notice: I18n.t('massive_email.success') 
      rescue => e
        redirect_to massive_email_volunteers_path, alert: e 
      end
    end
  rescue =>e
    log_error("COD-00001",e) 
    redirect_to massive_email_volunteers_path, alert: e 
  end

  def assign_turn 
    @asociated = params[:asociated]
    @project_id = params[:project_id]
    respond_with(@volunteer) do |format|
      format.js #{ render 'shared/popup' }
      format.html
    end
  end

  def process_assign_turn
    @project_id = params[:project_id]
    pvolunteer = ProjectsVolunteer.find_by(volunteer_id: params[:id], project_id: params[:project_id])
    pvolunteer.day = params[:day]
    pvolunteer.shift_definition_id = params[:turn]

    if params[:asociated].blank?
      if AuditGenerateHelper.audit_update(pvolunteer, pvolunteer.attributes,current_user)       
        redirect_to derivatie_project_volunteers_path(@project_id ||{},q:{projects_id_eq: @project_id  },pt_extendable_id: params[:pt_extendable_id]), notice: I18n.t('success_turn_delivery')
      else
        redirect_to derivatie_project_volunteers_path(@project_id ||{},q:{projects_id_eq: @project_id  },pt_extendable_id: params[:pt_extendable_id]), alert: I18n.t('alert_turn_delivery')
      end
    else
      if AuditGenerateHelper.audit_update(pvolunteer, pvolunteer.attributes,current_user)
        redirect_to project_volunteers_path(@project_id ||{},q:{projects_id_eq: @project_id  },pt_extendable_id: params[:pt_extendable_id]), notice: I18n.t('success_turn_delivery')
      else
        redirect_to project_volunteers_path(@project_id ||{},q:{projects_id_eq: @project_id  },pt_extendable_id: params[:pt_extendable_id]), alert: I18n.t('alert_turn_delivery')
      end
    end
  end

  def create_meeting
    @asociated = params[:asociated]
    @project_id = params[:project_id]
    @pt_extendable_id = params[:pt_extendable_id]
    @project_vol = ProjectsVolunteer.find_by(project: @project_id,volunteer: @volunteer.id)
    @meetings = Meeting.where("project_id = #{@project_id}").all
    respond_with(@volunteer) do |format|
      format.js #{ render 'shared/popup' }
      format.html
    end
  end

  def process_create_meeting
    projects_volunteer = ProjectsVolunteer.find(volun_proy_meeting_params[:id])
    old_data = []
    VolunProyMeeting.where(projects_volunteer_id: projects_volunteer.id).each {|o| old_data.push({o.id=>{confirm_swal: o.confirm, asist: o.asist}})} 
    volunteer_manager = VolunteerManager.new(volunteer: projects_volunteer.try(:volunteer), manager_id: current_user.loggable_id)
    project = projects_volunteer.try(:project)
    if AuditGenerateHelper.audit_update(projects_volunteer, volun_proy_meeting_params, current_user)
      change_data_meeting(projects_volunteer,params,old_data)
      if params[:projects_volunteer][:asociated].blank? 
        redirect_to derivatie_project_volunteers_path(project.try(:id) ||{},q:{projects_id_eq: project.try(:id)  },pt_extendable_id: params[:pt_extendable_id]), notice: I18n.t('success_meeting_delivery') 
      else
        redirect_to project_volunteers_path(project.try(:id) ||{},q:{projects_id_eq: project.try(:id)  },pt_extendable_id: params[:projects_volunteer][:pt_extendable_id], asociated: true), notice: I18n.t('success_meeting_delivery')
      end
    else
      if params[:projects_volunteer][:asociated].blank?
        redirect_to derivatie_project_volunteers_path(project.try(:id) ||{},q:{projects_id_eq: project.try(:id)  },pt_extendable_id: params[:pt_extendable_id]), alert: I18n.t('alert_meeting_delivery') 
      else
        redirect_to project_volunteers_path(project.try(:id) ||{},q:{projects_id_eq: project.try(:id)  },pt_extendable_id: params[:projects_volunteer][:pt_extendable_id], asociated: true), alert: I18n.t('alert_meeting_delivery')
      end
    end
  end

  def associate_derivatie
    @project = Project.find(params[:project_id])
    @volunteer =Volunteer.find(params[:id])
    
    respond_to do |format|
      format.js #{ render 'shared/popup' }
      format.html
    end
  end

  def post_associate_derivatie
    volun_to_associate = ProjectsVolunteer.find_by(volunteer_id: params[:volun_id], project_id: params[:project_id])
    aux_type = volun_to_associate.volunteer_type
    volun_to_associate.volunteer_type = "ASOCIATED"
   
    if  AuditGenerateHelper.audit_update(volun_to_associate, {}, current_user, false)   
      if aux_type!="ASOCIATED" 
        volunteer_manager = VolunteerManager.new(volunteer: volun_to_associate.volunteer, manager_id: current_user.loggable_id)
        volunteer_manager.register_project_volunteer(current_user: current_user.loggable_id,project: params[:project_id]) 
      end
      flash[:notice] = I18n.t('success_associated_volunteer')
    else
      flash[:alert] = I18n.t('alert_associated_volunteer')
    end

    redirect_to derivatie_project_volunteers_path(params[:project_id],q:{projects_id_eq: params[:project_id] },pt_extendable_id: Project.find(params[:project_id]).try(:pt_extendable_id))    
  end

  def derivatie_projects
    @volunteer = Volunteer.find(params[:id])
    @derivatie_projects = ProjectsVolunteer.where(volunteer_id: @volunteer.id).distinct.order("projects_volunteers.project_id desc")
    respond_to do |format|
      format.html
      format.csv { stream_csv_derivatie_projects(@derivatie_projects,params) }
    end
  rescue => e
    log_error("COD-00014",e) 
    flash[:error] = t("derivatie_projects.error")
    redirect_to volunteers_path
  end

  def send_massive_sms

  end

  def historic_massive_sms
    @massive_sms_historics = MassiveSmsHistoric.volunteers_sms
    @massive_sms_historics = @massive_sms_historics.paginate(page: params["page"] || 1, per_page: params["per_page"]||10)

  end

  def post_send_massive_sms
    if params[:content].blank? && params[:content].strip.blank?
      redirect_to send_massive_sms_volunteers_path, alert: "Falta introducir los campos obligatorios del cuerpo del mensaje"
    else
      volunteers = Volunteer.project_accompany.where("volunteers.active=true and (volunteers.phone_number is not null and volunteers.phone_number!='' or volunteers.phone_number_alt is not null and  volunteers.phone_number_alt!='')").select(:id).distinct

      begin
        MassiveSmsJob.perform_later(current_user.id, volunteers.map(&:id), params[:content], "volunteer")  
     
        redirect_to send_massive_sms_volunteers_path, notice: I18n.t('massive_sms.success') 
      rescue => e
        redirect_to send_massive_sms_volunteers_path, alert: e 
      end
    end
  end

  private
  
  def stream_csv_derivatie_projects(items,params)
    stream_file("proyectos_derivados_#{@volunteer.try(:full_name).gsub(" ","_")}", "csv") do |stream|
      items.model.stream_query_rows(items,params) do |row_from_db|
        stream.write row_from_db
      end
    end
  rescue
  end

  def stream_csv_report_derivatie(items,params)
    stream_file("#{items.name.gsub(" ", "_")}", "csv") do |stream|
      items.model.stream_query_rows(items,params,true) do |row_from_db|
        stream.write row_from_db
      end
    end
  rescue
  end

  def change_data_meeting(projects_volunteer,params,old_data)
    return if projects_volunteer.blank? || params[:projects_volunteer].blank?
    volunteer_manager = VolunteerManager.new(volunteer: projects_volunteer.volunteer, manager_id: current_user.loggable_id)

    old_data.each do |old|
      old.each do |k,v|
        params[:projects_volunteer][:volun_proy_meetings_attributes].each do |key, vm|
          met_vol = VolunProyMeeting.find(vm[:id])
          meeting = Meeting.find(met_vol[:meeting_id])
          if k == met_vol.id
            if v[:confirm].to_s != met_vol.confirm.to_s
              volunteer_manager.register_confirm_volunteer(projects_volunteer.project,meeting) if !met_vol.confirm.nil? && met_vol.confirm.to_s == "true"
              volunteer_manager.register_not_confirm_volunteer(projects_volunteer.project,meeting) if !met_vol.confirm.nil? && met_vol.confirm.to_s == "false"
            end
            if v[:asist].to_s != met_vol.asist.to_s
              volunteer_manager.register_asist_volunteer(projects_volunteer.project,meeting) if !met_vol.asist.nil? && met_vol.asist.to_s == "true"
              volunteer_manager.register_not_asist_volunteer(projects_volunteer.project,meeting) if !met_vol.asist.nil? && met_vol.asist.to_s == "false"
            end
          end
        end
      end
    end
  end

  protected

  def carga_projects
    @projects = Project.where(active: true).select("father_id,subtype_proyect,name,id,pt_order, case subtype_proyect WHEN 'subproyect' THEN concat(father_id,'.',COALESCE(pt_order,0)) ELSE cast(id as varchar) END as \"order_generic\"").order("\"order_generic\"")
    @volunteer ||=nil
    
    if !@volunteer.try(:projects).blank?
      @projects=@projects.where("id NOT IN (?)",@volunteer.projects.select('id'))
    end
    if !params[:search].blank?
      @projects = @projects.where("UPPER(projects.name) LIKE UPPER(?)","%#{params[:search]}%")
    end
    if !params[:search_id].blank?
      @projects=@projects.where("case subtype_proyect WHEN 'subproyect' THEN concat(father_id,'.',COALESCE(pt_order,0)) ELSE cast(id as varchar) END LIKE (?)","%#{params[:search_id]}%")
    end  
  end

  def load_mask
    avoid_phone_numbers_mask
    avoid_availability_date_mask
  end

 

  private

  def volun_proy_meeting_params
    params.require(:projects_volunteer).permit(
      :id,
      {
        volun_proy_meetings_attributes: [ :id, :asist, :confirm, :_destroy]
      }
    )
  end

  def volunteer_params
    params.require(:volunteer).permit(
        :id, :name, :last_name, :last_name_alt, :document_type_id,
        :id_number, :notes, :retired, :accompany_volunteer,
        :cession_of_personal_data, :publication_image_ayto,
        :publication_image_ooaa, :publication_image_social_network,
        :publication_image_media, :gender_id,
        :birth_date, :nationality_id, :phone_number,
        :phone_number_alt, :email, :is_adult, :initial_pass,
        :status_id, :employment_status_id, :vocne, :available,
        :availability_date, :academic_level_id, :subscribe_date,
        :unsubscribe_date, :unsubscribe_reason_id, :comments,
        :expectations, :agreement, :agreement_date, :search_authorization,
        :representative_statement, :has_driving_license, :criminal_certificate, 
        :publish_pictures, :ec, :accompany, :annual_survey, :manager_id,
        :info_source_id, :other_academic_info, :profession_id,
        :next_point_id, :review, :error_address, :error_other, :tester,
        {
          availabilities_attributes: [ :id, :day, :start_hour, :end_hour, :_destroy ]
        },
        {
          known_languages_attributes: [ :id, :volunteer_id, :language_id, :language_level_id, :_destroy ]
        },
        {
          assessments_attributes: [ :id, :volunteer_id, :trait_id, :trait_other, :_destroy ]
        },
        {
          assessments_projects_attributes: [
            :id, :volunteer_id, :trait_id, :project_id, :trait_other, :_destroy
          ]
        },
        { skill_ids: [] },
        {
          address_attributes: address_attributes
        },
        { district_ids: [] },
        { project_ids: [] },
        { trait_ids: [] },
        { area_ids: [] },
        { volun_profile_ids: [] },
        { collective_ids: [] },
        { derivaties_volunteer_ids: [] },
        {
          degrees_attributes: [
            :id, :name, :degree_type, :degree_type_id, :_destroy
          ]
        },
        {
          logo_attributes: [ :id, :link_type_id, :file, :_destroy ]
        },
        {
          images_attributes: [ :id, :link_type_id, :file, :description, :_destroy ]
        },
        {
          docs_attributes: [ :id, :link_type_id, :file, :description, :_destroy ]
        },
        {
          urls_attributes: [ :id, :path, :link_type_id, :file, :description, :_destroy ]
        }
      )
  end

  def set_class_style 
      @clase_estilo="" 
      unless params[:rt_volunteer_subscribe_id].blank? 
        @subscribe=Rt::VolunteerSubscribe.find(params[:rt_volunteer_subscribe_id])  
        @clase_estilo="border: 1px solid green" 
      end 
      @clase_solicitud=@clase_estilo 
  end 

  def get_model_class
    @model_class = Volunteer
  end
  
  def set_class_other_style 
    set_class_style 

    unless params[:rt_volunteer_amendment_id].blank? 
      @amendment=Rt::VolunteerAmendment.find(params[:rt_volunteer_amendment_id])  
      @clase_solicitud="border: 1px solid green" 
    end 
  end 

  def avoid_phone_numbers_mask
    params[:volunteer][:phone_number] = nil if params[:volunteer][:phone_number] == "_________"
    params[:volunteer][:phone_number_alt] = nil if params[:volunteer][:phone_number_alt] == "_________"
  end

  def avoid_availability_date_mask
    params[:volunteer][:availability_date] = nil if params[:volunteer][:availability_date] == "__/__/____"
  end

  def search_text_split(param, text)
    aux_search=param.split(',')
    sentence_search =""
    aux_search.each do |aux_st|
      if !aux_st.blank? && aux_st.to_s!="null"
        if sentence_search.blank?
          sentence_search= "(" 
          sentence_search = sentence_search + text +"'"+ aux_st +"'"
        else
          sentence_search = sentence_search + " OR " + text +"'"+ aux_st +"'"
        end
      end
    end
    sentence_search= sentence_search + ")" if !sentence_search.blank?
    sentence_search
  end

  def load_data
    set_districts
    set_next_points
    set_areas
    set_project_types(array_first_type_projects)
    set_collectives
    set_volun_profile
    set_info_sources
    set_statuses
  end
end
