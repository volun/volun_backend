class Meeting < ActiveRecord::Base
    extend MeetingsHelper

    ######## RELACIONES ########
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    belongs_to :type_meeting, class_name: "TypeMeeting", foreign_key: "type_meeting_id", optional: false
    has_many :volun_proy_meeting, class_name: "VolunProyMeeting", foreign_key: "meeting_id"


    ######## VALIDACIONES ########
    validates :name, uniqueness: true
    validates :type_meeting, presence: true
    validates :name, presence: true

    ######## MÉTODOS ########
    def self.main_columns
        [:id, :name, :date, :hour, :place]
    end

    def to_s
        name
    end

    private

        def self.get_filter(params)
            keys = []
            ParamsFilter.get_filter(params, keys, 'project_filter.')
        rescue
            []
        end
end
