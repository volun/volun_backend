class TypeMeeting < ApplicationRecord
    include Archivable

    ######## VALIDACIONES ########
    validates :name, uniqueness: true
    validates :name, presence: true
  
    ######## MÉTODOS ########
    def to_s
      name
    end

    def self.tooltip
      aux = ''
      [:id,:name,:description].each do |field|
        aux = Common.get_message_tooltip(aux, TypeMeeting,field)     
      end
      aux
    end
end
