class Volunteer < ActiveRecord::Base
  require 'csv'
  include ArchivableVolunteer
  include VirtualFullName
  extend VolunteersHelper
  include Constants
  include Common

  ######## ENUMS ########
  enum gender: [:male, :female, :other]
  
  belongs_to :academic_level, class_name: "AcademicLevel", foreign_key: "academic_level_id", optional: true
  belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
  belongs_to :employment_status, -> {order(:name)}, class_name: "EmploymentStatus", foreign_key: "employment_status_id", optional: true
  belongs_to :info_source, -> {order(:name)}, class_name: "InfoSource", foreign_key: "info_source_id", optional: true
  belongs_to :nationality, -> {order(:name)}, class_name: "Nationality", foreign_key: "nationality_id", optional: true
  belongs_to :profession, ->{ where(active: true).order('professions.name asc') }, class_name: "Profession", foreign_key: "profession_id", optional: true
  belongs_to :next_point, ->{ where(active: true).order('next_points.name asc') }, class_name: "NextPoint", foreign_key: "next_point_id", optional: true
  belongs_to :status, class_name: "Status", foreign_key: "status_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  belongs_to :unsubscribe_reason, -> {order(:name)}, class_name: "UnsubscribeReason", foreign_key: "unsubscribe_reason_id", optional: true
  belongs_to :gender, optional: true
  belongs_to :document_type, optional: true
  
  has_and_belongs_to_many :projects_volunteers, class_name: "ProjectsVolunteer"
  has_and_belongs_to_many :projects,        ->{ order('projects.name asc') }, :through => :projects_volunteers
  has_and_belongs_to_many :projects_derivaties,        ->{ where("projects_volunteers.volunteer_type='DERIVATIE'").order('projects.name asc') },class_name: 'Project', :through => :projects_volunteers
  has_and_belongs_to_many :projects_associated,        ->{ where("projects_volunteers.volunteer_type!='DERIVATIE' OR projects_volunteers.volunteer_type is null").order('projects.name asc') },class_name: 'Project', :through => :projects_volunteers
  has_and_belongs_to_many :projects_others, ->{ where(active: true).order('projects.name asc') }, class_name: 'Project', :through => :projects_volunteers
  has_and_belongs_to_many :projects_asign,  ->{ where("projects_volunteers.active=true") }, class_name: 'Project', :through => :projects_volunteers
  has_and_belongs_to_many :skills,          ->{ where(active: true).order('skills.name asc') }, class_name: "Skill"
  has_and_belongs_to_many :areas,           ->{ where(active: true).order('areas.name asc') }, class_name: "Area"
  has_and_belongs_to_many :collectives,     ->{ where(active: true).order('collectives.name asc') }, class_name: "Collective"
  has_and_belongs_to_many :volun_profiles,  ->{ where(active: true).order('volun_profiles.name asc') } , class_name: "VolunProfile"
  has_and_belongs_to_many :districts_volunteers, class_name: "DistrictsVolunteer"
  has_and_belongs_to_many :districts, :through => :districts_volunteers
  
  has_many :derivaties_volunteers, class_name: "DerivatiesVolunteer"
  has_many :shift_definitions, class_name: "ShiftDefinition"
  has_many :volun_proy_meetings, class_name: "VolunProyMeeting"
  has_many :degrees_volunteers, class_name: "DegreesVolunteer", dependent: :destroy
  has_many :volunteer_projects_relations, class_name: "VolunteerProjectsRelation"
  has_many :degrees, ->{ where(active: true).order('degrees.name asc') }, through: :degrees_volunteers
  has_many :availabilities, ->{ ordered }, class_name: 'Volun::Availability',foreign_key: 'volunteer_id', dependent: :destroy
  has_many :known_languages,      class_name: 'Volun::KnownLanguage',foreign_key: 'volunteer_id', dependent: :destroy
  has_many :assessments,          class_name: 'Volun::Assessment', foreign_key: 'volunteer_id', dependent: :destroy
  has_many :assessments_projects, class_name: 'Volun::AssessmentsProject',foreign_key: 'volunteer_id',dependent: :destroy
  has_many :contacts,             class_name: 'Volun::Contact'
  has_many :trackings,            class_name: 'Volun::Tracking'
  has_many :languages, :through => :known_languages
  has_many :traits,    :through => :assessments
  has_many :images, -> { volunteer_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { volunteer_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { volunteer_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_one  :logo,   -> { volunteer_logo   }, class_name: 'Link', foreign_key: 'linkable_id', required: false, validate: false
  has_many :links, as: :linkable
  has_one  :user, class_name: "User", foreign_key: 'loggable_id',  as: :loggable
  has_many :citizen_services, class_name: "CitizenService"
  

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :degrees,              reject_if:  :check_existing, allow_destroy: true
  accepts_nested_attributes_for :availabilities,       reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :known_languages,      reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :assessments,          reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :assessments_projects, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :projects,             reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :projects_derivaties,  reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :projects_associated,  reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :derivaties_volunteers,reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :images,               reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :docs,                 reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :urls,                 reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :logo,                 reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :citizen_services
  accepts_nested_attributes_for :districts,             reject_if:  :all_blank, allow_destroy: true
  

  validates :name, :last_name, :id_number, :is_adult, presence: true
  validates :nationality, :gender, :birth_date, presence: true
  validates :unsubscribe_reason, presence: true, unless: -> { unsubscribe_date_blank}
  validates :id_number, spanish_vat: true, unless: -> {document_type.try :is_other_type? }
  validates :id_number, uniqueness: {message: I18n.t('activerecord.errors.messages.duplicated_volun_id')}
  validates :phone_number, format: { with: /[8|9]\d{8}/ }, allow_blank: true, unless: ->{phone_number_blank_mask}
  validates :phone_number_alt, format: { with: /[6|7]\d{8}/ }, allow_blank: true, unless: ->{phone_number_alt_blank_mask}
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, allow_blank: true
  validates :email, uniqueness: true, unless: ->{email.blank?}
  validates :birth_date, date: { after:       Proc.new { 150.years.ago },
                                 before:      Proc.new { Date.yesterday },
                                 message:     I18n.t('activerecord.errors.messages.invalid_volun_birth_date'),
                                 allow_blank: true }
  validates :availability_date, date: { after:       Proc.new { Date.tomorrow },
                                        before:      Proc.new { 150.years.since },
                                        message:     I18n.t('activerecord.errors.messages.invalid_volun_availability_date'),
                                        allow_blank: true },
                                if: ->{available?},
                                unless: ->{availability_date_blank_mask}
  validates :agreement_date, presence: { message: I18n.t('activerecord.errors.messages.invalid_volun_agreement_date')},
                             if: ->{agreement?}
  validates :subscribe_date, date: { after:       Proc.new { 150.years.ago },
                                     before:      Proc.new { 150.years.since },
                                     message:     I18n.t('activerecord.errors.messages.invalid_volun_subscribe_dates'),
                                     allow_blank: true }
  validates :unsubscribe_date, date: { after:       Proc.new { 150.years.ago },
                                       before:      Proc.new { 150.years.since },
                                       message:     I18n.t('activerecord.errors.messages.invalid_volun_subscribe_dates'),
                                       allow_blank: true }
  validate :unsubscribe_date_higher_than_subscribe_date
  validate :trait_check
  validate :trait_project_check
  validate :uniq_email
  validate :validate_file_size 
  validate :email_downcase

  validates_associated :address

  scope :not_market, ->() { where(market: false)}
  scope :not_tester, ->() { not_market.where(tester: false)}
  scope :tester, ->() { not_market.where(tester: true)}
  scope :project_accompany, ->() { not_market.joins("INNER JOIN projects_volunteers ON volunteers.id = projects_volunteers.volunteer_id").where("projects_volunteers.project_id in (?)", Project.where(accompany: true).select(:id)) }
  scope :all_availabilities, ->(){ not_market.where(active: true).where(available: true).where("availability_date <= (?)",Date.today+Constants.n_days.day) }  
  scope :all_active,   ->(){ not_market.where(active: true) }
  scope :all_inactive, ->(){ not_market.where(active: false) }
  scope :last_tracking, ->(){ not_market.joins(:trackings).last}
  scope :trackings_projects_automatic_between, ->(ids) { not_market.where("id IN (?)",ids)}
  scope :trackings_projects_automatic_start, ->(date_init) { not_market.where("id IN 
    (select volun_trackings.volunteer_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) >= cast('#{date_init.to_date}' as date) AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='subscribe')")}
  scope :trackings_projects_automatic_end, ->(date_last) { not_market.where("id IN 
      (select volun_trackings.volunteer_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) <= cast('#{date_last.to_date}' as date) AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='unsubscribe')")}
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all.not_market
    end
  }

  ######## MÉTODOS ########
  ### Métodos SELF ###
  def self.period_active(start_date,end_date)    
    nums = []
    active_projects = []
    
    aux_subscribes = Volun::Tracking.where(:tracking_type => TrackingType.find_by(alias_name: "subscribe")).where.not(project_id: nil).sort_by { |t| t.tracked_at}
    aux_subscribes.each {|a| active_projects.push({a.project_id => {volun_id: a.volunteer_id,start: a.tracked_at}})}
    aux_unsubscribes = Volun::Tracking.where(:tracking_type => TrackingType.find_by(alias_name: "unsubscribe")).where.not(project_id: nil).sort_by { |t| t.tracked_at}

    aux_unsubscribes.each do |a|
        aux_ok = false
        active_projects.each do |ap|
            ap.each do |key, value|
                if key == a.project_id && !value[:start].blank? && value[:start] <= a.tracked_at && !value.has_key?(:final) && value[:volun_id] == a.volunteer_id
                    value.merge!(final: a.tracked_at)
                    aux_ok=true
                    break
                end
            end
        end

        unless aux_ok
            active_projects.push({a.project_id => {start: nil, final: a.tracked_at}})
        end
    end

    start_date = Time.zone.parse(start_date)
    end_date = Time.zone.parse(end_date)
    valid_volunteers = []

    active_projects.each do |ap|    
        ap.each { |key, value| valid_volunteers.push(value[:volun_id]) if !value[:volun_id].blank? && !(!value[:start].blank? && value[:start] > end_date || value.has_key?(:final) && value[:final] < start_date)} 
    end 

    valid_volunteers.uniq
  end

  def phone_number_all
    aux = phone_number.blank? ? phone_number_alt : phone_number_alt.blank? ? phone_number : phone_number + " / " + phone_number_alt
  end

  def phone
    self.mobile_number
  end

  def vocne_formated
    vocne ? 'Sí' : 'No'
  end

  def situation
    status.try :name
  end

  def gender_convert
    self.gender.try(:name)
  rescue
    ""
  end

  def alert_situation
    age.to_i < 18 && !birth_date.blank? || (!cession_of_personal_data || !publication_image_ayto || !publication_image_ooaa || !publication_image_social_network || !publication_image_media || vocne || !situation.blank? && situation.try(:downcase).to_s == 'no apto')
  rescue
    false
  end

  def self.tooltip_volunteer
    aux = ''
    [:name,:last_name,:last_name_alt,:id_number, :phone_number,:phone_number_alt, :email, :other_academic_info].each do |field|
      aux=Common.get_message_tooltip(aux, Volunteer,field)          
    end
    [:district,:postal_code].each do |field|     
      aux=Common.get_message_tooltip(aux, Address,field)   
    end
    aux
  end

  def self.main_columns(project_blank=true)
    if project_blank
      %i(id_autonum
        id_number
        age_name
        name
        last_name
        last_name_alt
        gender_convert
        email
        phone_number_alt        
        town
        postal_code
        district_custom
        borought
        notice_available_column
        not_autorized
        num_projects_active
      )
    else
      %i(id_autonum
        id_number
        age_name
        name
        last_name
        last_name_alt
        gender_convert
        email
        phone_number_alt
        district_custom
      )
    end
  end

 


  def self.main_columns_massive
    %i(id_autonum
       full_name
       gender_convert
       id_number
       email
       phone_number_all
       district_custom
       situation
       vocne_formated
       postal_code
      )
  end

  def self.main_columns_all
    %i(id_autonum
      id_number
      name
      last_name
      last_name_alt
      gender_convert
      phone_number
      phone_number_alt
      email
      )
  end

  def self.main_columns_all_short
    %i(id_autonum
      id_number)
  end

  def self.ransack_default
    {s: 'updated_at desc'}
  end

  def town
    self.address.try(:town).to_s
  rescue
    ""
  end

  def postal_code
    self.address.try(:postal_code).to_s
  rescue
    ""
  end

  def district_custom
    self.address.try(:district).blank? ? "OTROS" : self.address.try(:district).to_s
  rescue
    "OTROS"
  end

  def not_autorized
    (self.age.to_i < 18 && !self.birth_date.blank? || (!self.cession_of_personal_data || !self.publication_image_ayto || !self.publication_image_ooaa || !self.publication_image_social_network || !self.publication_image_media || self.vocne)).to_s
  rescue
    "true"
  end

  def num_active_projects
    self.num_projects_active
  rescue
    0
  end

  def self.to_csv(volunteers, params)
    CSV.generate(col_sep:';', encoding:'ISO-8859-1') do |csv|
      csv << [I18n.t('csv_export.header_filter')]
      csv << self.get_filter(params)
      csv << [""]
      csv << [I18n.t('csv_export.header_list')]
      csv << main_columns_all.map{ |column_name| 
        human_attribute_name(column_name) } +  
        [
          human_attribute_name(:gender), "Edad", 
          human_attribute_name(:birth_date), 
          Nationality.human_attribute_name(:name), 
          Address.human_attribute_name(:postal_code), 
          Address.human_attribute_name(:district), 
          Address.human_attribute_name(:town), 
          Address.human_attribute_name(:province), 
          human_attribute_name(:subscribe_date), 
          human_attribute_name(:unsubscribe_date), 
          human_attribute_name(:status), 
          human_attribute_name(:vocne),
          I18n.t('csv_export.diversity'),
          I18n.t('csv_export.last_tracking') 
        ]
      volunteers.find_each do |volunteer|
        begin
          csv << main_columns_all.map{ |column_name| volunteer.public_send column_name } + [volunteer.gender_i18n, volunteer.age, format_date(volunteer.birth_date), (volunteer.nationality.try :name),(volunteer.address.try :postal_code), (volunteer.address.try :district), (volunteer.address.try :town), (volunteer.address.try(:province).try(:name)), format_date(volunteer.subscribe_date), format_date(volunteer.unsubscribe_date), (volunteer.status.try :name), get_boolean(volunteer.vocne), get_boolean(volunteer.accompany_volunteer), (volunteer.trackings.last.try :comments).force_encoding("UTF-8") ]
        rescue
          csv << main_columns_all.map{ |column_name| volunteer.public_send column_name } + [volunteer.gender_i18n, volunteer.age, format_date(volunteer.birth_date), (volunteer.nationality.try :name),(volunteer.address.try :postal_code), (volunteer.address.try :district), (volunteer.address.try :town), (volunteer.address.try(:province).try(:name)), format_date(volunteer.subscribe_date), format_date(volunteer.unsubscribe_date), (volunteer.status.try :name), get_boolean(volunteer.vocne), get_boolean(volunteer.accompany_volunteer), "" ]
        end
      end
   end
  end

  def self.to_csv_open_data(volunteers)
    CSV.generate(col_sep:';', encoding:'ISO-8859-1') do |csv|
      csv <<  
      [ human_attribute_name(:gender), 
        human_attribute_name(:birth_date), 
        Nationality.human_attribute_name(:name), 
        Address.human_attribute_name(:district), 
        Address.human_attribute_name(:borough), 
        human_attribute_name(:subscribe_date), 
        human_attribute_name(:unsubscribe_date), 
        human_attribute_name(:employment_status), 
        human_attribute_name(:academic_level)  
      ]
      volunteers.each do |volunteer|
        csv << [volunteer.gender_i18n, format_date(volunteer.birth_date), (volunteer.nationality.try :name), (volunteer.address.try :district), (volunteer.address.try :borough), format_date(volunteer.subscribe_date), format_date(volunteer.unsubscribe_date), volunteer.employment_status, volunteer.academic_level]
      end
    end
  end

  def self.to_csv_all_short(volunteers)
    CSV.generate(col_sep:';', encoding:'ISO-8859-1') do |csv|
      csv << main_columns_all_short.map{ |column_name| human_attribute_name(column_name) } +  [human_attribute_name(:gender), 'Edad',  Address.human_attribute_name(:district), human_attribute_name(:subscribe_date), human_attribute_name(:unsubscribe_date) ]
      volunteers.each do |volunteer|
        csv << main_columns_all_short.map{ |column_name| volunteer.public_send column_name } + [volunteer.gender_i18n, volunteer.age, (volunteer.address.try :district), format_date(volunteer.subscribe_date), format_date(volunteer.unsubscribe_date)]
      end
    end
  end
  
  def self.to_csv_volunteers
    ###FICHERO PERÍODICO DE VOLUNTARIOS###
    volunteers=Volunteer.stream_datosabiertos

    Constants.arr_formats.each do |f|
      CSV.open("#{Setting.find_by(key: "RutaNasDatosAbiertos").value}fichero_voluntarios_#{DateTime.current().strftime("%Y%m%d_%H%M%S")}.#{f}", "wb",col_sep:';', encoding:'ISO-8859-1') do |csv|
        volunteers.each do |v|
          csv <<  v.split(';').map{|x| x.to_s.gsub(/[\"\n]+/,'')}
        end
      end
    end

    # Envio de email
    begin
      AdministratorMailer.send_email_dataset("transparenciaydatos@madrid.es", subject: I18n.t("mailer.subject_datosabiertos")).deliver_now
    rescue
    end
  end
  
  def self.to_csv_volunteers_by_districts
    ###FICHERO PERÍODICO DE POR DISTRITOS###

    volunteers=Volunteer.includes(:address).order("addresses.district_id,addresses.borought_id")
  
    contador = 0
    tabla_vol={}
    volunteers.each do |vol|

      if tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"].nil? 
        
        conjunto=[{},{},{},vol.address[:district],vol.try(:address).try(:borough_code),vol.try(:address).try(:borough),vol.try(:address).try(:district_code)]

        for i in 0..2
          conjunto[i].merge!("0-19"=>0)
          for j in 2..8
            conjunto[i].merge!("#{j}0-#{j}9"=>0)
          end
          conjunto[i].merge!(">90"=>0)
        end
        
        tabla_vol.merge!("#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"=>conjunto)
      end
     
      age_short=(vol.age.to_i/10)*10

      if vol.gender.to_s=="female"
        numx=1
      elsif vol.gender.to_s=="male"
        numx=0
      else
        numx=2
      end

      if vol.age.to_i<20
        tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx]["0-19"]=(tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx]["0-19"]).to_i+1
      elsif vol.age.to_i>=90
        tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx][">90"]=(tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx][">90"]).to_i+1
      else
        tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx]["#{age_short}-#{age_short+9}"]=(tabla_vol["#{vol.try(:address).try(:district)}-#{vol.try(:address).try(:borough)}"][numx]["#{age_short}-#{age_short+9}"]).to_i+1
      end
    end

    Constants.arr_formats.each do |f|
      CSV.open("#{Setting.find_by(key: "RutaNasDatosAbiertos").value}fichero_voluntarios_distrito_#{DateTime.current().strftime("%Y%m%d_%H%M%S")}.#{f}", "wb",col_sep:';', encoding:'ISO-8859-1') do |csv|
        csv <<  [I18n.t('file_volun_district.id_district'),I18n.t('file_volun_district.name_district'), I18n.t('file_volun_district.id_borough'),I18n.t('file_volun_district.name_borough'), I18n.t('file_volun_district.gender'),I18n.t('file_volun_district.middle_age'), I18n.t('file_volun_district.number')]
        tabla_vol.each do |key,value|

          if value[3].blank?
            distrito = "Sin Distrito"
          else
            distrito = value[3]
          end
          if value[5].blank?
            barrio = "Sin Barrio"
          else
            barrio=value[5]
          end
          if value[6].blank?
            id_distrito= "Sin ID Distrito"
          else
            id_distrito= value[6]
          end
          if value[4].blank?
            id_barrio="Sin ID Barrio"
          else
            id_barrio=value[4]
          end
          for j in 0..2
            if j==0
              csv << [
                id_distrito,
                distrito,
                id_barrio,
                barrio,
                "Mujer", "Menor de 20",value[1]["0-19"]||0]
              gen=1
              genero="Mujer"
            elsif j==1
              csv << [id_distrito, distrito, id_barrio, barrio,"Hombre", "Menor de 20",value[0]["0-19"]||0]
              gen=0
              genero="Hombre"
            else
              csv << [id_distrito, distrito, id_barrio, barrio,"Otro", "Menor de 20",value[2]["0-19"]||0]
              gen=2
              genero="Otro"
            end
            for i in 2..8
              csv << [id_distrito, distrito, id_barrio, barrio,genero, "#{i}0-#{i}9",value[gen]["#{i}0-#{i}9"]||0]
            end
            csv << [id_distrito, distrito, id_barrio, barrio,genero,"Mayor de 90",value[gen][">90"]||0]
          end
        end
      end
    end
  end

  def self.format_date(date)
    date.blank? ? "-" : date.strftime("%d-%m-%Y")
  end

  def self.get_boolean(valor)
    valor == true ? "Sí" : "No"
  end

  def self.number_max_length_id
    Project.all.order(id: :asc).last.id.to_s.length
  end

  def self.number_max_length_name
    numero=0
    Project.all.each {|v| numero=v.name.length if v.name.length > numero }
    numero
  end

  def self.seach_volunteers(palabra, identificador,busqueda)
    palabra||=""
    identificador||=""
    unless palabra.blank?
      palabra=palabra.gsub("_"," ")
      busqueda=busqueda.joins(:address).where('UPPER(volunteers.name) LIKE UPPER(?) OR UPPER(volunteers.last_name) LIKE UPPER(?) OR UPPER(volunteers.last_name_alt) LIKE UPPER(?) OR UPPER(volunteers.phone_number) LIKE UPPER(?) OR UPPER(volunteers.phone_number_alt) LIKE UPPER(?) OR UPPER(volunteers.email) LIKE UPPER(?) OR UPPER(volunteers.other_academic_info) LIKE UPPER(?) OR UPPER(select d.name from districts d where d.id=addresses.district_id) LIKE UPPER(?) OR UPPER(addresses.postal_code) LIKE UPPER(?)',"%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%","%#{palabra}%")
    end
  
    unless identificador.blank?
      busqueda=busqueda.where('CAST(volunteers.id as varchar) LIKE (?)',"%#{identificador}%")
    end
    busqueda
  end

  def get_list_skills
    cad = ""
    skills.each {|s| cad = cad + ( cad.blank? ? s.name : "/" + s.name)}
    cad
  end

  def self.unassociated_projects(voluntario,palabra,identificador)
    palabra||=""
    identificador||=""
    unless palabra.nil? or palabra.empty?
      palabra=palabra.gsub("_"," ")
    end
    unless voluntario.nil?
        self.like(palabra,identificador).where('id NOT IN (?)', voluntario.projects.select('id'))
    else
        self.like(palabra,identificador)
    end
  end

  def self.unassociated_derivaties_projects(voluntario,palabra,identificador)
    palabra||=""
    identificador||=""
    unless palabra.nil? or palabra.empty?
      palabra=palabra.gsub("_"," ")
    end
    unless voluntario.nil?
        self.like(palabra,identificador).where('id NOT IN (?)', voluntario.derivaties_volunteers.select('project_id'))
    else
        self.like(palabra,identificador)
    end
  end

  def self.like(palabra,identificador)
    Project.where('UPPER(projects.name) LIKE UPPER(?)',"%#{palabra}%").where('CAST(projects.id as varchar) LIKE (?)',"%#{identificador}%").where(active: true).order('projects.name asc')
  end

  def get_active_project(project)
    value=ActiveRecord::Base.connection.exec_query("SELECT projects_volunteers.active FROM projects_volunteers where projects_volunteers.project_id=#{project} AND projects_volunteers.volunteer_id=#{self.id}")
    if value.blank? || value[0].blank?
      return false
    else
      value = value[0]['active']
      return value.to_s=='true'
    end
  end

  def get_languages
    cad = ""
    known_languages.includes(:language).each {|language_know| cad = cad + (cad.blank? ? language_know.language.name : ' / ' + language_know.language.name)}
    cad
  end

  def get_collectives
    cad = ""
    collectives.each {|volunteer_collective| cad = cad + (cad.blank? ? volunteer_collective.name : ' / ' + volunteer_collective.name)}
    cad
  end

  def get_volun_profiles
    cad = ""
    volun_profiles.each {|volunteer_profiles| cad = cad + (cad.blank? ? volunteer_profiles.name : ' / ' + volunteer_profiles.name)}
    cad
  end

  def get_areas
    cad = ""
    areas.each {|volunteer_area| cad = cad + (cad.blank? ? volunteer_area.name : ' / ' + volunteer_area.name)}
    cad
  end

  def get_projects
    cad = ""
    projects.each {|volunteer_project| cad = cad + (cad.blank? ? volunteer_project.name : ' / ' + volunteer_project.name)}
    cad
  end

  def get_availabilities
    cad = ""
    availabilities.each {|volunteer_availability| cad = cad + (cad.blank? ? volunteer_availability.day_i18n.to_s + ' ' + volunteer_availability.start_hour.to_s + ' ' + volunteer_availability.end_hour.to_s : ' / ' + volunteer_availability.day_i18n.to_s + ' ' + volunteer_availability.start_hour.to_s + ' ' + volunteer_availability.end_hour.to_s)}
    cad
  end

  def get_employment_statuses
    employment_status.try(:name)
  end

  def get_statuses
    status.try(:name)
  end

  def project_comments(project_id) 
    result= ActiveRecord::Base.connection.exec_query("SELECT projects_volunteers.comments from volunteers,projects_volunteers WHERE volunteers.id=#{self.id} AND volunteers.id=projects_volunteers.volunteer_id AND projects_volunteers.project_id=#{project_id} AND projects_volunteers.active=false")
    return "" if result.blank? || result[0].blank?
    result[0]["comments"]
  end

  def num_projects_active
    self.projects_asign.count
  end

  def notice_available?
    self.active == true && self.available == true && !self.availability_date.blank? && self.availability_date <= (Date.today+Constants.n_days.day)
  end

  def notice_available_column
    self.active == true && self.available == true && !self.availability_date.blank? && self.availability_date <= (Date.today+Constants.n_days.day)
  end

  def age
    return "-" if self.birth_date.blank?
    now = Time.zone.now
    (now.year - birth_date.year - ((now.month > birth_date.month || (now.month == birth_date.month && now.day >= birth_date.day)) ? 0 : 1)).to_s
  end

  def age_name
    age
  end

  def to_s
    "#{self.name} #{self.last_name} #{self.last_name_alt}"
  end

  def country
    address.country
  end

  def degree_type
    cad = ""
    degrees.each {|degree| cad = cad + (cad.blank? ? degree.degree_type.name : ' / ' + degree.degree_type.name)}
    cad
  end

  def language
    cad = ""
    known_languages.includes(:language).each {|language_know| cad = cad + (cad.blank? ? language_know.language.name : ' / ' + language_know.language.name)}
    cad
  end

  def language_level
    cad = ""
    known_languages.includes(:language).each {|language_know| cad = cad + (cad.blank? ? language_know.language_level.name : ' / ' + language_know.language_level.name)}
    cad
  end

  def check_existing(degree_attr = nil)
    return false if degree_attr.blank?
     _degree = Degree.find_by(name: degree_attr[:name])
     if _degree && !self.degrees.exists?(_degree.id)
       self.degrees << _degree
     end
    return true
  rescue
    false
  end

  def check_existing_availability(availability_attr = nil)
#     return false if availability_attr.blank?
#     _availability = Availability.find_by(day: availability_attr[:day],start_hour:  availability_attr[:start_hour],end_hour: availability_attr[:end_hour])
#     if _availability && !self.availabilities.exists?(_availability.id)
#       self.availabilities << _availability
#     end
#    return true
#  rescue
   false
  end

  def check_existing_languages(language_attr = nil)
    return false if language_attr.blank?
    _language = Volun::KnownLanguage.new(language_attr.except(_destroy))
    if _language && !self.known_languages.exists?(_language.language_id)
      return false
    end
   return true
  rescue
   false 
  end

  def check_existing_assessments(assessment_attr = nil)
    return false if assessment_attr.blank?
    _assessment =Volun::Assessment.new(assessment_attr.except(_destroy))
    if _assessment && !self.assessments.exists?(_assessment.trait_id)
      return false
    end
     return true
  rescue => e
    puts "="*50
    puts e
   false 
  end

  def mobile_number
    if !phone_number.strip.blank? && !phone_number.strip.match(/[6|7]\d{8}/).blank?
      phone_number
    elsif !phone_number_alt.strip.blank? && !phone_number_alt.strip.match(/[6|7]\d{8}/).blank?
      phone_number_alt
    end 
  end

  alias_method :full_name, :to_s

  private

  def self.get_filter(params)
    keys = [
      'search_id', 'enter_text', 'search_nif',
      'search_name','search_last_name','search_last_name_alt',
      'search_location','search_volun_profile','search_postal_code',
      'search_movil','search_availabilities_day','search_status','search_district',
      'search_collective','search_area', 'search_info_source','search_type','search_project_link_start_date',
      'search_project_link_end_date','search_start_date','search_end_date',
      'search_start_age','search_end_age','search_without_auth',
      'search_accompany_volunteer','new_availability','ec','no_asign']
      ParamsFilter.get_filter(params, keys, 'volunteer_filter_params.')
  end

  def uniq_email
    if self.id.blank?
      exists= User.where("email='#{self.email}'").count
    else
      exists= User.where("email='#{self.email}' AND not (loggable_id=#{self.id} AND loggable_type='Volunteer')").count
    end
    errors.add(:email, I18n.t('user.exists')) if (exists.to_i>0 && (!self.email.blank?))
  end

  def trait_check
    return unless assessments.any?
    assessments.each do |first_assessment|
      trait_count = 0
      assessments.each do |assessment|
        if  first_assessment.trait_id == assessment.trait_id
          trait_count += 1
        end
      end
      if trait_count > 1
        errors.add(:base, :alert_trait_duplicity)
        return
      end
    end
  end

  def trait_project_check
    return unless assessments_projects.any?
    assessments_projects.each do |first_assessment|
      project_trait_count = 0
      assessments_projects.each do |assessment|
        if  first_assessment.trait_id == assessment.trait_id && first_assessment.project_id == assessment.project_id
          project_trait_count += 1
        end
      end
      if project_trait_count > 1
        errors.add(:base, :alert_project_trait_duplicity)
        return
      end
    end
  end

  def unsubscribe_date_blank
    unsubscribe_date.blank?
  end

  def unsubscribe_date_higher_than_subscribe_date
    return unless subscribe_date && unsubscribe_date

    unless subscribe_date <= unsubscribe_date
      errors.add(:unsubscribe_date, :unsubscribe_date_must_be_higher_than_subscribe_date)
    end
  end

  def phone_number_blank_mask
    avoid_phone_mask(phone_number)
  end

  def phone_number_alt_blank_mask
    avoid_phone_mask(phone_number_alt)
  end

  def avoid_phone_mask(phone)
    phone == "_________"
  end

  def availability_date_blank_mask
    volun = Volunteer.find(self.id)


    availability_date == "__/__/____" || volun.availability_date.to_s == self.availability_date.to_s
  end

  def validate_file_size
    link = Link.where(linkable_id: self.id, linkable_type: "Volunteer").where("path != '/images/missing.png'").count
    
    if link == 0  || self.id.blank? || !Volunteer.find(self.id).try(:logo).blank? && Volunteer.find(self.id).try(:logo).try(:file_file_name) != self.try(:logo).try(:file_file_name)
      if !Setting.find_by(key: "VolunBackend.file_size_kb").blank?
        value = Setting.find_by(key: "VolunBackend.file_size_kb").try(:value).to_i
        if !self.try(:logo).blank? && !self.try(:logo).try(:file_file_size).blank? && self.try(:logo).try(:file_file_size)/1024 > value
          value >= 1024 ? self.errors.add(:logo,:size_mb, value: value, value_mb: value/1024) : self.errors.add(:logo,:size, value: value)
          return false
        end
      else
        self.errors.add(:logo,:setting)
      end
    end
  rescue
    true
  end

  def email_downcase
    self.email = self.email.downcase
  end

  
end
