class Language < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common
  ######## RELACIONES ########
  has_many :known_languages, :class_name => 'Volun::KnownLanguage'
  has_many :volunteers, :through => :known_languages

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true
  
  ######## MÉTODOS ########
  def self.main_columns
    [:id, :name, :active]
  end

  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Language,field)
    end
    aux
  end
end
