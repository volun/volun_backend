class Skill < ActiveRecord::Base
  include Recordable
  include Archivable
  ######## RELACIONES ########
  has_and_belongs_to_many :volunteers, class_name: "Volunteer"

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Skill,field)
    end
    aux
  end
end
