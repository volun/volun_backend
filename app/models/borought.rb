class Borought < ActiveRecord::Base
  include Archivable

  belongs_to :district, class_name: "District", foreign_key: "district_id"

  def to_s
    name
  end

  def self.main_columns
    [:id, :name, :code, :district_code, :active]
  end

  def self.tooltip
      aux = ''
      [:name,:id, :code].each do |field|
          aux = Common.get_message_tooltip(aux, Borought,field)
      end
      aux
  end

  def district_code
    self.district.code
  rescue
    ""
  end
end
