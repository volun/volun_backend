class ProjectsVolunteer < ActiveRecord::Base
    extend ProjectsVolunteersHelper

    ######## RELACIONES ########
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    belongs_to :shift_definition, class_name: "ShiftDefinition", foreign_key: "shift_definition_id", optional: true
    has_many :volun_proy_meetings, dependent: :destroy , class_name: "VolunProyMeeting"

    ######## ATRIBUTOS ANIDADOS ########
    accepts_nested_attributes_for :volun_proy_meetings, reject_if:  :all_blank, allow_destroy: true

    ######## MÉTODOS ########
    def self.main_columns
        [:subscribe_date, :day]
    end

    def district
        self.try(:volunteer).try(:address).try(:district)
    end

    def description_confirm
        description = ""
        self.volun_proy_meetings.each do |v|
            if v.confirm.to_s == "true"
                description = description + I18n.t("meeting.confirmed", meeting: v.try(:meeting).try(:name)) + "<br>"
            elsif v.confirm.to_s == "false"
                description = description + I18n.t("meeting.notconfirm", meeting: v.try(:meeting).try(:name)) + "<br>"
            else
                description = description + I18n.t("meeting.unconfirmed", meeting: v.try(:meeting).try(:name)) + "<br>" 
            end
        end
        description
    end
    
    def description_asist
        description = ""
        self.volun_proy_meetings.each do |v|
            if v.asist.to_s == "true"
                description = description + I18n.t("meeting.assisted", meeting: v.try(:meeting).try(:name))  + "<br>"
            elsif v.asist.to_s=="false"
                description = description + I18n.t("meeting.notassisted", meeting: v.try(:meeting).try(:name))  + "<br>"
            else
                description = description + I18n.t("meeting.unassisted", meeting: v.try(:meeting).try(:name))  + "<br>"
            end
        end
        description
    end
    
    def description_turn
        if !self.day.blank? && !self.shift_definition.blank?
            self.try(:day) + " - " + self.shift_definition.try(:name)
        end
    end
end
