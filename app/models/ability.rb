class Ability
  include CanCan::Ability
  
  ######## MÉTODOS ########
  def initialize(user)
    return unless user

    case user.loggable
    when ->(manager){ manager.super_admin? }
      self.merge Abilities::SuperAdmin.new(user)

    when ->(manager){ manager.admin? }
      self.merge Abilities::Admin.new(user)

    when ->(manager){ manager.internal_staff? }
      self.merge Abilities::InternalStaff.new(user)

    when ->(manager){ manager.external_staff? }
      self.merge Abilities::ExternalStaff.new(user)

    when ->(manager){ manager.special_trust? }
      self.merge Abilities::SpecialTrust.new(user)
    when ->(manager){ manager.digital_manage? }
      self.merge Abilities::DigitalManage.new(user)
    else
      cannot :manage, :all
    end
  end
  
end
