class Resource < ActiveRecord::Base
  include Archivable
  attr_accessor :alias_name

  DEFAULT_RESOURCES = [
    Activity.name,
    Entity.name,
    Ent::Tracking.name,
    Link.name,
    Project.name,
    Pro::Tracking.name,
    Pt::Subvention.name,
    Pt::Centre.name,
    Pt::Entity.name,
    Pt::Other.name,
    Pt::RetiredVolunteer.name,
    Pt::Social.name,
    Pt::Municipal.name,
    Pt::Volunteer.name,
    RequestForm.name,
    Volunteer.name,
    Volun::Tracking.name,
  ]

  ALL_RESOURCES =[
    VolunProfile.name,
    AcademicLevel.name,
    Address.name,
    Area.name,
    Citizen.name,
    CitizenService.name,
    Collective.name,
    ContactResult.name,
    ContactType.name,
    Coordination.name,
    Degree.name,
    DegreeType.name,
    District.name,
    Document.name,
    EmploymentStatus.name,
    Event.name,
    EventType.name,
    FrontpageElement.name,
    FrontpagePosition.name,
    DocumentType.name,
    InfoSource.name,
    Language.name,
    LanguageLevel.name,
    LanguageVolunteer.name,
    Manager.name,
    Meeting.name,
    Motivation.name,
    Nationality.name,
    NoticeType.name,
    Permission.name,
    Pro::Issue.name,
    Profession.name,
    Profile.name,
    Proposal.name,
    Province.name,
    RecordHistory.name,
    Req::Reason.name,
    Req::RejectionType.name,
    Req::Status.name,
    Req::StatusTrace.name,
    RoadType.name,
    Role.name,
    Rt::ActivityPublishing.name,
    Rt::ProjectPublishing.name,
    Rt::VolunteerAmendment.name,
    Rt::VolunteerAppointment.name,
    Rt::VolunteersDemand.name,
    Rt::VolunteerProjectSubscribe.name,
    Rt::VolunteerSubscribe.name,
    Rt::Other.name,
    Sector.name,
    ShiftDefinition.name,
    Skill.name,
    Status.name,
    Timetable.name,
    TrackingType.name,
    Trait.name,
    UnsubscribeLevel.name,
    UnsubscribeReason.name,
    Volun::Assessment.name,
    Volun::Availability.name,
    Volun::Contact.name,
    Volun::KnownLanguage.name,
    Audit.name,
    Pt::RetiredVolunteer.name,
    NextPoint.name,
    EntityOrganization.name,
    NewCampaing.name,
    VolunExport.name,
    Album.name,
    DerivatiesVolunteer.name,
    Service.name,
    ServiceQuestion.name,
    ServiceResponse.name,
    TypeMeeting.name,
    Channel.name

  ] + DEFAULT_RESOURCES

  ######## RELACIONES ########
  has_many :managers, class_name: "Manager"
  has_many :audits, class_name: "Audit"

  ######## VALIDACIONE ########
  validates :name, inclusion: { in: ALL_RESOURCES }

  ######## SCOPES ########
  default_scope ->{ order('resources.description asc') }
  scope :main_resources, ->{ where(main: true) }

  ######## MÉTODOS ########
  def self.ransack_default
    {s: 'description asc'}
  end

  def self.admin_columns
    %i(description name active)
  end

  def self.internal_staff_columns
    self.admin_columns
  end

  def self.external_staff_columns
    self.admin_columns
  end

  def self.special_trust_columns
    self.admin_columns
  end

  def self.super_admin_columns
    admin_columns << :main
  end

  def alias_name
    class_name.model_name.human
  end

  def to_s
    name
  end

  def class_name
    name.to_s.safe_constantize
  end

  def self.tooltip
    aux = ''
    [:id,:name,:description].each do |field|
      aux = Common.get_message_tooltip(aux, Resource,field)     
    end
    aux
  end
end
