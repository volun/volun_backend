class FrontpageElement < ActiveRecord::Base
  include Archivable
  include Recordable

  ######## RELACIONES ########
  belongs_to :frontpage_position, required: true, class_name: "FrontpagePosition", foreign_key: "frontpage_position_id"

  ######## VALIDACIONES ########
  validates :frontpage_position_id, uniqueness: true
end
