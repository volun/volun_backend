class ShiftDefinition < ActiveRecord::Base
  include Recordable
  include Archivable
  ######## RELACIONES ########
  has_and_belongs_to_many :projects, class_name: "Project"
  has_and_belongs_to_many :volunteers, class_name: "Volunteer"
  has_and_belongs_to_many :projects_volunteers, class_name: "ProjectsVolunteer"

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true
    
  ######## SCOPES ########
  scope :id,   ->(){ where("id is not null") }

  ######## MÉTODOS ########
  def self.main_columns
      [:id, :name, :description, :active]
  end

  def to_s
      name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, ShiftDefinition,field)     
    end
    aux
  end
end
