class Link < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :linkable, polymorphic: true
  belongs_to :link_type, class_name: "LinkType", foreign_key: "link_type_id", optional: true
  belongs_to :project, -> { includes(:links).where(links: { linkable_type: 'Project' }) }, foreign_key: 'linkable_id', class_name: "Project", optional: true
  belongs_to :volunteer, -> { includes(:links).where(links: { linkable_type: 'Volunteer' }) }, foreign_key: 'linkable_id', class_name: "Volunteer",  optional: true
  belongs_to :citizen, -> { includes(:links).where(links: { linkable_type: 'Citizen' }) }, foreign_key: 'linkable_id', class_name: "Citizen",  optional: true
  belongs_to :activity, -> { includes(:links).where(links: { linkable_type: 'Activity'}) }, foreign_key: 'linkable_id', class_name: "Activity",  optional: true
  belongs_to :entity, -> { includes(:links).where(links: { linkable_type: 'Entity'}) }, foreign_key: 'linkable_id', class_name: "Entity", optional: true
  belongs_to :new_campaing, -> { includes(:links).where(links: { linkable_type: 'NewCampaing'}) }, foreign_key: 'linkable_id', class_name: "NewCampaing", optional: true
  belongs_to :album, -> { includes(:links).where(links: { linkable_type: 'Album'}) }, foreign_key: 'linkable_id', class_name: "Album", optional: true
  
  ######## VALIDACIONES ########
  belongs_to :settings, class_name: "Setting", foreign_key: "setting_id", optional: true

  has_attached_file :file,
                    styles: lambda{ |a|
                      return {} unless a.content_type.in? %w(image/jpeg image/png image/jpg image/gif)
                      { thumb:  '100x100#', small:  '200x200#', medium: '300x300>' }
                    },
                    default_url: '/images/missing.png',
                    url: '/system/:class/:belongs_to/:id/:attachment/:style/:filename'
  validates_attachment_content_type :file, content_type: /\Aimage\/.*\z/ , if: -> {logo? || image? }
  validates_attachment_content_type :file,
                                    content_type: %w(
                                      application/x-7z-compressed
                                      image/bmp
                                      txt/csv
                                      application/msword
                                      application/vnd.openxmlformats-officedocument.wordprocessingml.document
                                      application/vnd.openxmlformats-officedocument.wordprocessingml.template
                                      image/gif
                                      image/jpeg
                                      application/vnd.oasis.opendocument.graphics
                                      application/vnd.oasis.opendocument.presentation
                                      application/vnd.oasis.opendocument.spreadsheet
                                      application/vnd.oasis.opendocument.text
                                      application/pdf
                                      image/png
                                      application/vnd.ms-powerpoint
                                      application/vnd.openxmlformats-officedocument.presentationml.presentation
                                      application/rtf
                                      application/x-rar-compressed
                                      text/plain
                                      image/tiff
                                      application/x-tar
                                      application/vnd.ms-excel
                                      application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                                      text/xml
                                      application/zip
                                    ),
                                    if: -> { document? }

  validates :link_type_id, presence: true
  validates :linkable_id, :linkable_type, presence: true, unless: -> {logo?}
  validates :file, presence: { message: I18n.t('must_choose_a_file')}, unless: -> {url? || logo? || video?}
  validates :description, presence: { message: I18n.t('must_write_description')}, unless: -> {image? || logo?}
  validates :path, format: { with: /\A#{URI.regexp.to_s}\z/ }, if: -> { url? || video? }

  after_save :update_path, unless: -> {url? || video?}
  before_save :validate_file, unless: -> {url? || video?}
  before_save :validate_file_size   

  delegate :logo?, :image?, :url?, :video?, :document?, :kind_i18n, to: :link_type, allow_nil: true
  
  ######## SCOPES ########
  scope :project_images , ->{
    includes(:project, :link_type).where(linkable_type: 'Project', link_type_id: LinkType.image.take.id)
  }
  scope :project_videos , ->{
   includes(:project, :link_type).where(linkable_type: 'Project', link_type_id: LinkType.video.take.id)
  }
  scope :project_docs   , ->{
    includes(:project, :link_type).where(linkable_type: 'Project', link_type_id: LinkType.document.take.id)
  }
  scope :project_urls   , ->{
    includes(:project, :link_type).where(linkable_type: 'Project', link_type_id: LinkType.url.take.id)
  }
  scope :project_logo, ->{
    includes(:project, :link_type).where(linkable_type: 'Project', link_type_id: LinkType.logo.take.id)
  }
  scope :volunteer_images , ->{
    includes(:volunteer, :link_type).where(linkable_type: 'Volunteer', link_type_id: LinkType.image.take.id)
  }
  scope :volunteer_docs   , ->{
    includes(:volunteer, :link_type).where(linkable_type: 'Volunteer', link_type_id: LinkType.document.take.id)
  }
  scope :volunteer_urls   , ->{
    includes(:volunteer, :link_type).where(linkable_type: 'Volunteer', link_type_id: LinkType.url.take.id)
  }
  scope :volunteer_logo, ->{
    includes(:volunteer, :link_type).where(linkable_type: 'Volunteer', link_type_id: LinkType.logo.take.id)
  }
  scope :citizen_logo, ->{
    includes(:citizen, :link_type).where(linkable_type: 'Citizen', link_type_id: LinkType.logo.take.id)
  }
  scope :entity_images , ->{
    includes(:entity, :link_type).where(linkable_type: 'Entity', link_type_id: LinkType.image.take.id)
  }
  scope :entity_docs   , ->{
    includes(:entity, :link_type).where(linkable_type: 'Entity', link_type_id: LinkType.document.take.id)
  }
  scope :entity_urls   , ->{
    includes(:entity, :link_type).where(linkable_type: 'Entity', link_type_id: LinkType.url.take.id)
  }
  scope :entity_logo, ->{
    includes(:entity, :link_type).where(linkable_type: 'Entity', link_type_id: LinkType.logo.take.id)
  }
  scope :activity_images , ->{
    includes(:activity, :link_type).where(linkable_type: 'Activity', link_type_id: LinkType.image.take.id)
  }
  scope :activity_videos , ->{
   includes(:activity, :link_type).where(linkable_type: 'Activity', link_type_id: LinkType.video.take.id)
  }
  scope :activity_docs   , ->{
    includes(:activity, :link_type).where(linkable_type: 'Activity', link_type_id: LinkType.document.take.id)
  }
  scope :activity_urls   , ->{
    includes(:activity, :link_type).where(linkable_type: 'Activity', link_type_id: LinkType.url.take.id)
  }
  scope :activity_logo, ->{
    includes(:activity, :link_type).where(linkable_type: 'Activity', link_type_id: LinkType.logo.take.id)
  }
  scope :new_campaing_images , ->{
    includes(:new_campaing, :link_type).where(linkable_type: 'NewCampaing', link_type_id: LinkType.image.take.id)
  }
  scope :new_campaing_videos , ->{
    includes(:new_campaing, :link_type).where(linkable_type: 'NewCampaing', link_type_id: LinkType.video.take.id)
  }
  scope :album_images , ->{
    includes(:album, :link_type).where(linkable_type: 'Album', link_type_id: LinkType.image.take.id)
  }
  scope :album_videos , ->{
    includes(:album, :link_type).where(linkable_type: 'Album', link_type_id: LinkType.video.take.id)
  }
  amoeba do
    enable
    nullify :id
  end

  class << self
    delegate :logo, :file_kinds, :logo_kind, :kinds, :kinds_i18n, to: LinkType
  end

  ######## MÉTODOS ########
  def self.main_columns
    %i(file_file_name description linkable)
  end

  def file_extension
    file_name.sub(file_base_name, '')
  end

  def file_base_name
    file_name.match(/\A[^\.]+/).to_a.first.to_s
  end

  def file_name
    file_file_name || path.to_s.match(/[^\/]+\z/).to_a.first.to_s
  end

  Paperclip.interpolates :belongs_to  do |attachment, _style|
    link = attachment.instance
    "#{link.linkable_type}/#{link.linkable_id}/#{link.link_type.kind}"
  end

  private
  def update_path
    update_column :path, file.url unless url?
  end

  def validate_file
    if file.blank?
      self.errors.add(:file,:blank)
    end
  end

  def validate_file_size
    if !Setting.find_by(key: "VolunBackend.file_size_kb").blank?
      value = Setting.find_by(key: "VolunBackend.file_size_kb").try(:value).to_i
      if !self.file_file_size.blank? && self.file_file_size/1024 > value
        value >= 1024 ? self.errors.add(:file,:size_mb, value: value, value_mb: value/1024) : self.errors.add(:file,:size, value: value)
        return false
      else
        return true
      end
    else
      self.errors.add(:file,:setting)
    end
  rescue 
    true
  end

end
