class Degree < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common

  ######## RELACIONES ########
  belongs_to :degree_type, class_name: "DegreeType", foreign_key: "degree_type_id", optional: true
  has_many :degrees_volunteers, class_name: "DegreesVolunteer"
  has_many :volunteers, through: :degrees_volunteers

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, :degree_type, presence: true

  ######## SCOPES ########
  scope :filter_by_degree_type_id, ->(dt_id){ where(active: true).joins(:degree_type).where("degree_types.id = ?", dt_id) }
  scope :filter_by_degree_type_name, ->(dt_name){ where(active: true).joins(:degree_type).where("degree_types.name = ?", dt_name) }

  ######## MÉTODOS ########
  def self.main_columns
    %i(degree_type name active)
  end

  def self.tooltip
    aux = ''
    [:name, :degree_type].each do |field|
      aux = Common.get_message_tooltip(aux, Degree,field)
    end   
    aux
  end

  def to_s
    name
  end
end
