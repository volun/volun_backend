class ModerateStatus < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common

  self.table_name = "moderate_statuses"
  
  ######## VALIDACIONES ########
  validates :title,:code,presence: true 
  validates :title, uniqueness: true
  validates :code, uniqueness: true

   ######## MÉTODOS ########
   def self.main_columns
    [:id, :title,:code, :active]
  end

  def self.tooltip
    aux = ''
    [:id,:title].each do |field|
      aux = Common.get_message_tooltip(aux, ModerateStatus,field)      
    end
    aux
  end

  def to_s
    title
  end

end
