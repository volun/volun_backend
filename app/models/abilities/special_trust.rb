module Abilities
  class SpecialTrust
    include CanCan::Ability

    def initialize(user)
      can [:read,:update], Pt::Centre
      can [:read,:update], Pt::Entity
      can [:read,:update], Pt::Other
      can [:read,:update], Pt::RetiredVolunteer
      can [:read,:update], Pt::Social
      can [:read,:update], Pt::Subvention
      can [:read,:update], Pt::Municipal
      can [:read,:update], Pt::Volunteer
      can [:read,:update,:clone_project, :volunteers_allowed, :publish, :order], Project
      self.merge Abilities::InternalStaff.new(user)
    end

  end
end
