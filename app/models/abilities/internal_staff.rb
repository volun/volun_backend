module Abilities
  class InternalStaff
    include CanCan::Ability

    def initialize(user)
      can :manage, :all
      # Always performed
      can :access, :ckeditor   # needed to access Ckeditor filebrowser

      # Performed checks for actions:
      can [:read, :create, :destroy], Ckeditor::Picture
      can [:read, :create, :destroy], Ckeditor::AttachmentFile
      cannot :manage, User
      cannot :update_checks, Project
      cannot :destroy, TrackingType, system: true
      
      if !user.try(:loggable).try(:permissions).blank?
        user.loggable.permissions.each do |permission|
          puts "="*50
          
          section_class = permission.section.to_s
          if !["Rt::SolidarityChain"].include?(section_class.to_s)
            if ["Citizen::Tracking", "CitizenService::Tracking"].include?(section_class.to_s)
              section_class = section_class.gsub(":","")
            end
            if !permission.try(:data).blank?
              permission.data.to_h.each do |k, v|
                if v.kind_of?(Hash)
                  v.each do|nested_permission, permitted|
                    nested_permission = nested_permission.to_sym
                    if permitted.to_s =='0'
                      cannot nested_permission, section_class.constantize
                    else
                      can nested_permission, section_class.constantize
                    end
                  end
                else
                  k = k.to_sym
                  if section_class.to_s == "Administration" 
                    if v.to_s == '0'
                      cannot k, WelcomeController 
                    else
                      can k, WelcomeController
                    end
                  elsif section_class.to_s != "Administration" 
                    if v.to_s =='0'
                      cannot k, section_class.constantize
                    else
                      can k, section_class.constantize
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
