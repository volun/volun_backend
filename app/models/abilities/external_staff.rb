module Abilities
  class ExternalStaff
    include CanCan::Ability

    def initialize(user)
      cannot :manage, Pt::Subvention
      self.merge Abilities::InternalStaff.new(user)
    end

  end
end
