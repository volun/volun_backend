module Abilities
  class DigitalManage
    include CanCan::Ability

    def initialize(user)
      cannot :manage, Pt::Subvention
      #self.merge Abilities::InternalStaff.new(user)
      
      self.merge Abilities::InternalStaff.new(user)
      can :read, Digital::Manage
      can :moderate, Digital::Manage
    end
  end
end
