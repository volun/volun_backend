class VirtualCommunity < ActiveRecord::Base
    require 'csv'
    
    def self.to_csv(data, services_questions, services)
        CSV.generate(col_sep:';', encoding:'ISO-8859-1') do |csv|
            
            csv << ["Estadísticas Madrid te Acompaña"]
            
            csv << [I18n.t('ws_statistics.volunteers')]
            csv <<  
                [ 
                    I18n.t('ws_statistics.total'), 
                    I18n.t('ws_statistics.total_actived'), 
                    I18n.t('ws_statistics.total_inactived') 
                ]
        
            csv << [data[:total], data[:total_actived], data[:total_inactived]]              
            
            csv << [I18n.t('ws_statistics.citizens')]
            csv <<  
                [ 
                    I18n.t('ws_statistics.total'), 
                    I18n.t('ws_statistics.total_actived'), 
                    I18n.t('ws_statistics.total_inactived')                
                ]
        
            csv << [data[:total_citizens], data[:total_actived_citizens], data[:total_inactived_citizens]]
            
            csv << [I18n.t('ws_statistics.citizen_services')]
            csv <<  
                [ 
                    I18n.t('ws_statistics.total_services'),
                    I18n.t('ws_statistics.total_pending_services'), 
                    I18n.t('ws_statistics.total_active_services'), 
                    I18n.t('ws_statistics.total_accepted_services'), 
                    I18n.t('ws_statistics.total_priority_services'),
                    I18n.t('ws_statistics.total_undone_services'), 
                    I18n.t('ws_statistics.total_finished_services'), 
                    I18n.t('ws_statistics.total_expired_services')
                ]
        
            csv << [data[:total_services],data[:total_pending_services], data[:total_active_services], data[:total_accepted_services],
                data[:total_priority_services],data[:total_undone_services], data[:total_finished_services], data[:total_expired_services]]
            csv << [I18n.t('ws_statistics.rate_services')]
            csv << ["#{I18n.t('ws_statistics.total_rate')}:",data[:total_rate]]
            
            csv << [I18n.t('ws_statistics.services_questions')]
            csv << [
                    I18n.t('ws_statistics.question_title'), 
                    I18n.t('ws_statistics.question_response'), 
                    I18n.t('ws_statistics.question_response_true'),
                    I18n.t('ws_statistics.question_response_false'),
                    I18n.t('ws_statistics.question_response_other')
                ]
            services_questions.each do |sv|
                csv << [sv.description, sv.total_responses, sv.total_responses_true,sv.total_responses_false, sv.total_responses_other]
            end
            csv << [I18n.t('ws_statistics.service_types')]
            csv << [
                    I18n.t('ws_statistics.service_title'), 
                    I18n.t('ws_statistics.service_count'), 
                    I18n.t('ws_statistics.service_porcentaje')
                ]
            services.each do |s|
                csv << [s.name,s.total_uses,s.total_percentage]
            end
        end
    end

    private

    def start_date_less_than_end_date
        return unless start_date && end_date

        unless start_date <= end_date
            errors.add(:start_date,:execution_start_date_must_be_less_than_execution_end_date)
        end
    end
end
