class AcademicLevel < ActiveRecord::Base
  include Archivable

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, :educational_type, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end
end
