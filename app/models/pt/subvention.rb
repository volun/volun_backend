class Pt::Subvention < ActiveRecord::Base
  include PtModel

  ######## RELACIONES ########
  belongs_to :proposal, class_name: "Proposal", foreign_key: "proposal_id", optional: true

  ######## VALIDACIONES ########
  validates :representative_name, presence: true
  validates :cost, :requested_amount, :subsidized_amount, :initial_volunteers_num, numericality: { allow_blank: true }
  validates :id_num, spanish_vat: true
  validates :vat_number, spanish_vat: true

end
