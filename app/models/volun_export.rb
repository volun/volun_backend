class VolunExport < ActiveRecord::Base
    extend VolunExportHelper

    scope :sort_by_fields_volun_export_asc,  -> { joins("JOIN unnest('{#{VolunExport.all.sort_by(&:fields_volun_export).map{|x| x.id}.join(",")}}'::int[]) WITH ORDINALITY t(id, ord) USING (id)").order("t.ord") }
    scope :sort_by_fields_volun_export_desc, -> { joins("JOIN unnest('{#{VolunExport.all.sort_by(&:fields_volun_export).reverse.map{|x| x.id}.join(",")}}'::int[]) WITH ORDINALITY t(id, ord) USING (id)").order("t.ord")  }
    
   

    ######## MÉTODOS ########
    def self.main_columns
        %i(id 
            fields_volun_export
            )
    end
    

    def self.get_fields
        [
            :id_field, :name_field, :first_last_name_field, :second_last_name_field, :phone_field, :mobile_phone_field, :mail_field, :district_field, :subscribe_date_field,
            :unsubscribe_date_field, :sex_field, :birth_date_field, :age_field, :document_type_field, :document_number_field, :province_field,
            :town_field, :road_name_field, :postal_code_field, :academic_level_field, :languages_field, :profession_field, :vocne_field,
            :can_criminal_field,:has_diving_license_field,:project_culities_field, :how_know_cv_field, :retired, :nationality,
            :country, :status, :academic_level, :degree_type, :language, :language_level, :skills, :next_point
        ]
    end

    def self.get_traslation_volunteer
        {
            :id_field => :id_autonum, :name_field => :name, :first_last_name_field => :last_name, :second_last_name_field => :last_name_alt,
            :phone_field => :phone_number, :mobile_phone_field => :phone_number_alt, :mail_field => :email, :district_field => :district_id, :subscribe_date_field => :subscribe_date, :unsubscribe_date_field => :unsubscribe_date,
            :sex_field => :gender, :birth_date_field => :birth_date, :age_field => :age, :document_type_field => :document_type,
            :document_number_field => :id_number, :province_field => :province_id, :town_field => :town, :road_name_field => :road_name,
            :postal_code_field => :postal_code, :academic_level_field => :academic_level, :languages_field => :get_languages,
            :profession_field => :manager, :vocne_field => :vocne, :can_criminal_field => :criminal_certificate,
            :has_diving_license_field => :has_driving_license, :project_culities_field => :employment_status, :how_know_cv_field => :info_source,
            :retired => :retired, :nationality => :nationality, :country => :country, :status => :status,
            :academic_level => :academic_level, :degree_type => :degree_type, :language => :language, :language_level => :language_level,
            :skills => :get_list_skills, :next_point => :next_point
        }
    end

    def self.get_traslation_volunteer_csv
        {
            :id_field => "COALESCE(CAST(volunteers.id_autonum as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.id_field")}\"", 
            :name_field => "COALESCE(CAST(volunteers.name as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.name_field")}\"", 
            :first_last_name_field => "COALESCE(CAST(volunteers.last_name as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.first_last_name_field")}\"", 
            :second_last_name_field => "COALESCE(CAST(volunteers.last_name_alt as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.second_last_name_field")}\"",
            :phone_field => "COALESCE(CAST(volunteers.phone_number as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.phone_field")}\"",  
            :mobile_phone_field => "COALESCE(CAST(volunteers.phone_number_alt as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.mobile_phone_field")}\"", 
            :mail_field => "COALESCE(CAST(volunteers.email as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.mail_field")}\"",
            :district_field => "COALESCE(CAST((select d.name from districts d,addresses where  addresses.district_id=d.id and addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.district_field")}\"",
            :subscribe_date_field => "COALESCE(CAST(volunteers.subscribe_date as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.subscribe_date_field")}\"",
            :unsubscribe_date_field => "COALESCE(CAST(volunteers.unsubscribe_date as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.unsubscribe_date_field")}\"",
            :sex_field => "COALESCE(CAST( CASE volunteers.gender WHEN '0' THEN 'Hombre'
                            WHEN '1' THEN 'Mujer'
                            ELSE 'Otro' END as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.sex_field")}\"",
            :birth_date_field => "COALESCE(CAST(volunteers.birth_date as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.birth_date_field")}\"",
            :age_field => "COALESCE(CAST(CASE WHEN volunteers.birth_date is null THEN null
                ELSE date_part('year',age(cast(volunteers.birth_date as date))) END as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.age_field")}\"", 
            :document_type_field => "COALESCE(CAST((select document_types.name from document_types where document_types.id=volunteers.document_type_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.document_type_field")}\"",
            :document_number_field => "COALESCE(CAST(volunteers.id_number as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.document_number_field")}\"",
            :province_field => "COALESCE(CAST((select p.name from provinces p,addresses where p.id=addresses.province_id and addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.province_field")}\"",
            :town_field => "COALESCE(CAST((select addresses.town from addresses where addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.town_field")}\"",
            :road_name_field => "COALESCE(CAST((select addresses.road_name from addresses where addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.road_name_field")}\"",
            :postal_code_field => "COALESCE(CAST((select addresses.postal_code from addresses where addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.postal_code_field")}\"",
            :academic_level_field => "COALESCE(CAST((select academic_levels.name from academic_levels where academic_levels.id=volunteers.academic_level_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.academic_level_field")}\"", 
            :languages_field => "COALESCE(CAST((select string_agg(languages.name,'/') from languages,volun_known_languages where languages.id= volun_known_languages.language_id AND volun_known_languages.volunteer_id = volunteers.id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.languages_field")}\"",
            :profession_field => "COALESCE(CAST((select string_agg(professions.name,'/') from professions where professions.id =volunteers.profession_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.profession_field")}\"", 
            :vocne_field =>  "COALESCE(CAST(volunteers.vocne as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.vocne_field")}\"", 
            :can_criminal_field => "COALESCE(CAST(volunteers.criminal_certificate as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.can_criminal_field")}\"",
            :has_diving_license_field => "COALESCE(CAST(volunteers.has_driving_license as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.has_diving_license_field")}\"", 
            :project_culities_field => "COALESCE(CAST((select employment_statuses.name from employment_statuses where employment_statuses.id=volunteers.employment_status_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.project_culities_field")}\"",
            :how_know_cv_field =>  "COALESCE(CAST((Select info_sources.name from info_sources where info_sources.id=volunteers.info_source_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.how_know_cv_field")}\"",
            :retired => "COALESCE(CAST(volunteers.retired as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.retired")}\"", 
            :nationality => "COALESCE(CAST((select nationalities.name from nationalities where nationalities.id = volunteers.nationality_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.nationality")}\"",
            :country => "COALESCE(CAST((select addresses.country from addresses where addresses.id=volunteers.address_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.country")}\"", 
            :status => "COALESCE(CAST((select statuses.name from statuses where statuses.id =volunteers.status_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.status")}\"",
            :academic_level => "COALESCE(CAST((select academic_levels.name from academic_levels where academic_levels.id=volunteers.academic_level_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.academic_level")}\"",
            :degree_type =>  "COALESCE(CAST((select string_agg(degree_types.name, '/') from degrees,degree_types, degrees_volunteers where degree_types.id=degrees.degree_type_id AND degrees.id=degrees_volunteers.degree_id AND degrees_volunteers.volunteer_id=volunteers.id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.degree_type")}\"", 
            :language => "COALESCE(CAST((select string_agg(languages.name,'/') from languages,volun_known_languages where languages.id= volun_known_languages.language_id AND volun_known_languages.volunteer_id = volunteers.id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.language")}\"",
            :language_level => "COALESCE(CAST((select string_agg(language_levels.name,'/') from language_levels,volun_known_languages where language_levels.id=volun_known_languages.language_level_id AND volun_known_languages.volunteer_id=volunteers.id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.language_level")}\"",
            :skills => "COALESCE(CAST((select string_agg(skills.name,'/') from skills, skills_volunteers where skills.id=skills_volunteers.skill_id AND skills_volunteers.volunteer_id=volunteers.id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.skills")}\"",
            :next_point => "COALESCE(CAST((select next_points.name from next_points where next_points.id= volunteers.next_point_id) as varchar), '-') as \"#{I18n.t("activerecord.attributes.volun_export.next_point")}\""
        }
    end


    def fields_volun_export 
        listado = ""
        VolunExport.get_fields.each do |field|
            if self.try(field)
                if listado.blank?
                    listado = I18n.t("activerecord.attributes.volun_export.#{field}")
                else
                    listado = listado + "; " + I18n.t("activerecord.attributes.volun_export.#{field}") 
                end
            end
        end
        listado
    end
end
