class ContactResult < ActiveRecord::Base
  include Archivable

  ######## RELACIONES ########
  has_many :contacts, :class_name => 'Volun::Contact'
  has_many :volunteers, :through => :contacts

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end
end
