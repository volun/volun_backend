class Req::Status < ActiveRecord::Base
  ######## MÉTODOS ########
  enum kind: {
    pending:    1,
    processing: 2,
    approved:   3,
    rejected:   4,
    appointed:  5
  }

  def to_s
    kind_i18n
  end
end
