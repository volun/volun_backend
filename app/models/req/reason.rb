class Req::Reason < ActiveRecord::Base
  include Archivable

  ######## VALIDACIONES ########
  validates :name, :description, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    description
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Req::Reason,field)
    end
    aux
  end
end
