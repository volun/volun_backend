class Req::StatusTrace < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :status, :class_name => 'Req::Status', foreign_key: 'req_status_id'
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
end
