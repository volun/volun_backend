class MassiveSmsHistoric < ActiveRecord::Base
    belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true

    scope :volunteers_sms, -> {where(specific: "volunteer")}
    scope :citizens_sms, -> {where(specific: "citizen")}

    def self.main_columns
        %i(id manager_name log created_at)
    end


    def manager_name
        user.try(:loggable).try {|x| x.to_s}
    end

    def self.send_sms
        massive_historic = MassiveSmsHistoric.new(user_id: 1, title: "worker")
        massive_historic.save
    end

end
