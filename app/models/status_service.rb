class StatusService < ActiveRecord::Base  
    has_one :status_service, class_name: "StatusService", foreign_key: "status_service_id"
    
    validates :name, presence: true
    validates :code, uniqueness: true, presence: true

end