class CitizenService < ActiveRecord::Base
    include Archivable
    extend CitizenServicesHelper
    belongs_to :citizen, class_name: "Citizen", foreign_key: "citizen_id", optional: true
    belongs_to :service, class_name: "Service", foreign_key: "service_id", optional: true
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :status_service, class_name: "StatusService", foreign_key: "status_service_id", optional: true
    belongs_to :volunteer_vote_rel, class_name: "ServiceVote", foreign_key: "volunteer_vote_id", optional: true
    belongs_to :citizen_vote_rel, class_name: "ServiceVote", foreign_key: "citizen_vote_id", optional: true
    belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
    belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true

    has_many :service_responses, class_name: "ServiceResponse", foreign_key: "citizen_service_id"
    has_many :service_votes, class_name: "ServiceVote", foreign_key: "citizen_service_id"
    has_many :trackings, class_name: "CitizenServiceTracking", foreign_key: "citizen_service_id"


    accepts_nested_attributes_for :citizen
    accepts_nested_attributes_for :service
    accepts_nested_attributes_for :volunteer
    
    
    validates :citizen_id, presence: true
    validates :service_id, presence: true
    validates :date_request, presence: true
    validates :hour_request, presence: true
    validates :hour_request, format: { with: /\d{2}:\d{2}/ }
    validates :hour_accept_request, format: {  with: /\d{2}:\d{2}/  }, allow_blank: true

    validate :hour_request_valid
    validate :hour_accept_valid
    validate :unique_service_in_day

    attr_accessor :search


    scope :not_market, -> () { where("citizen_id in (select citizens.id from citizens where citizens.market = false)")}
    scope :not_tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = false)") }
    scope :tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = true)") }
    scope :all_prepared_active, -> () { where("status in ('active','accept','prioritary')")}
    scope :all_active,   ->(){ where(active: true) }
    scope :status,   ->(status){ where(status: status) }
    scope :for_ids_with_order, ->(ids) {
        # order = sanitize_sql_array(
        #   ["position(id::text in ?)", ids.join(',')]
        # )
        # where(:id => ids).order(order)

        where(:id => ids).order(id: :asc)
      }

    def self.main_columns
        %i(id
           f_citizen_id
           f_citizen_name
           f_citizen_last_name_1
           f_citizen_last_name_2
           f_volunteer_id_autonum
           f_volunteer_name
           f_volunteer_last_name_1
           f_volunteer_last_name_2  
           f_volunteer_district
           type_service
           is_digital
           full_date_accept
           full_date_request
           status_name
           f_volunteer_vote
           f_citizen_vote
           canceled_at
           citizen_reason
           rejected_at
           volunteer_reason
           place
           additional_data
           borought_name
           district_name
           )
    end

    def self.get_filter(params)
        keys = [
            'search_id',
            'enter_text',
            'search_nombre_citizen',
            'search_apellido1_citizen',
            'search_apellido2_citizen',
            'search_nombre_volunteer',
            'search_apellido1_volunteer',
            'search_apellido2_volunteer',
            'search_services_start',
            'search_services_end',         
            'search_services_a_start',
            'search_services_a_end',
            'search_service_type',
            'search_service_status',
            'search_start_rate',
            'search_end_rate'
        ]
        ParamsFilter.get_filter(params, keys, 'citizen_service_filter.')
    end 

    def description_content
        self.description.html_safe
    rescue
        ""
    end

    def f_citizen_id
        return '' if self.citizen.blank?
        self.citizen.id
    end

    def f_citizen_name
        return '' if self.citizen.blank?
        self.citizen.name
    end

    def f_citizen_last_name_1
        return '' if self.citizen.blank?
        self.citizen.last_name_1
    end

    def f_citizen_last_name_2
        return '' if self.citizen.blank?
        self.citizen.last_name_2
    end

    def f_volunteer_id
        return '' if self.volunteer.blank?
        self.volunteer.id_autonum
    end

    def full_name_citizen
        return '' if self.citizen.blank?
        "#{self.citizen.name} #{self.citizen.last_name_1} #{self.citizen.last_name_2}"
    end

    def f_volunteer_name
        return '' if self.volunteer.blank?
        self.volunteer.name
    end

    def f_volunteer_last_name_1
        return '' if self.volunteer.blank?
        self.volunteer.last_name
    end

    def f_volunteer_last_name_2
        return '' if self.volunteer.blank?
        self.volunteer.last_name_alt
    end

    def f_volunteer_district
        return '' if self.volunteer.try(:address).blank?
        self.volunteer.address.district.blank? ? "OTROS" :  self.volunteer.address.district
    end

    def full_name_volunteer
        return '' if self.volunteer.blank?
        "#{self.volunteer.name} #{self.volunteer.last_name} #{self.volunteer.last_name_alt}"
    end

    def type_service
        return '' if self.service.blank?
        self.service.name
    end

    def full_date_accept
        return '' if self.date_accept_request.blank? 
        Time.zone.parse("#{self.date_accept_request} #{self.hour_accept_request}").strftime('%d/%m/%Y %H:%M')
    end

    def full_date_request
        return '' if self.date_request.blank?
        Time.zone.parse("#{self.date_request} #{self.hour_request}").strftime('%d/%m/%Y %H:%M')
    end

    def status_name
        return "" if status.blank?
        I18n.t("service.status.#{status}")
    end

    def borought_name
        self.borought.try(:code).blank? || self.district.try(:code).blank? ? "" : Address.borougth_list(self.district.try(:code)).find_by(code: self.borought.try(:code)).try(:name)
    end

    def district_name
        self.district.try(:code).blank? ? "" : Address.district_list.find_by(code: self.district.try(:code)).try(:name)
    end
     
    def f_volunteer_vote
        return '' if self.service_votes.blank?
        self.service_votes.find_by(type_user: "Volunteer").try(:vote)
    end

    def f_citizen_vote
        return '' if self.service_votes.blank?
        self.service_votes.find_by(type_user: "Citizen").try(:vote)
    end

    # def to_s
    #     self.description.html_safe
    # rescue
    #     ""
    # end

    def hour_request_numeric
        self.hour_request.to_s.gsub(":","").to_i
    end

    def self.tooltip
        aux = ''
        [:id,:f_citizen_name,:f_citizen_last_name_1,:f_citizen_last_name_2,:f_citizen_email,
        :f_volunteer_name,:f_volunteer_last_name_1,:f_volunteer_last_name_2,:f_volunteer_email
        ].each do |field|
          aux = Common.get_message_tooltip(aux, CitizenService,field)     
        end
        aux
      end

    def to_s
        "Servicio (#{id}) - #{type_service}"
    end

    private

    def get_time(str)
        DateTime.strptime(str, '%H:%M')
    rescue
        "-"
    end

    def hour_request_valid
        if !self.hour_request.blank? && get_time(self.hour_request).to_s == '-'
            errors.add(:hour_request, "Hora incorrecta del servicio")
        end
    end

    def hour_accept_valid        
        if !self.hour_accept_request.blank? && get_time(self.hour_accept_request).to_s == '-'
            errors.add(:hour_accept_request, "Hora incorrecta de aceptación del servicio")
        end
    end
    

    def unique_service_in_day
        if self.id.blank?
            services = CitizenService.where(citizen_id: self.citizen_id).where("cast(date_request as date) = cast(? as date)",self.date_request).all_prepared_active
        else
            services = CitizenService.where(citizen_id: self.citizen_id).where("id != ? and cast(date_request as date) = cast(? as date)",self.id, self.date_request).all_prepared_active
        end
        if !services.blank?
            errors.add(:date_request, "Servicio mayor no se ha podido #{self.id.blank? ? "crear" : "modificar"}. Ya existen peticiones de este mayor para este día")
        end        
    end
end
