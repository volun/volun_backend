module ArchivableVolunteer
  extend ActiveSupport::Concern

  included do

    def archived?
      !active?
    end

    def destroy(attr_p)

      
      volunteer_old_attrs={id: self.id, active: self.active, phone_number: self.phone_number, phone_number_alt: self.phone_number_alt}
      projects=Project.joins("INNER JOIN projects_volunteers ON projects_volunteers.project_id=projects.id AND projects_volunteers.volunteer_id=#{self.id}").all
      

      pvolunteers_old_attrs=[]
      pvolunteers_new_attrs=[]
      if projects.count>0
        pvolunteers=ProjectsVolunteer.where(volunteer_id: self.id)
        pvolunteers.each do |vol|
          pvolunteers_old_attrs.push({id: vol.id, active: vol.active, unsubscribe_date: self.unsubscribe_date})

          vol.active = false
          vol.unsubscribe_date = Time.now.strftime("%d/%m/%Y").to_date
          if vol.save
            pvolunteers_new_attrs.push({id: vol.id, active: vol.active, unsubscribe_date: self.unsubscribe_date})
          end
        end
      end
      self.active = false
      self.email = ""
      self.phone_number = ""
      self.phone_number_alt = ""
      save(validate: false)
      #update(:active=> false,:email=>"",:phone_number=> "",:phone_number_alt=>"")
      volunteer_new_attrs={id: self.id, active: self.active, phone_number: self.phone_number, phone_number_alt: self.phone_number_alt}
      volunteer_old_attrs.merge({:projects_volunteers => pvolunteers_old_attrs})
      volunteer_new_attrs.merge({:projects_volunteers => pvolunteers_new_attrs})
      ##AUDITORIA
      attr_p[:old_data] = JSON.parse(volunteer_old_attrs.to_json)
      attr_p[:new_data] = JSON.parse(volunteer_new_attrs.to_json)
      attr_p[:operation_type] = "#{I18n.t('audit.archive')}" 
      attr_p[:operation] = "#{I18n.t('audit_enum.archive', id: self.id)}"
      AuditGenerateHelper.generate(attr_p)

      self
    end

    #alias_method :destroy, :archive

    def recover(attr_p)
      volunteer_old_attrs={id: self.id, active: self.active, unsubscribe_date: self.unsubscribe_date, subscribe_date: self.subscribe_date, unsubscribe_reason_id:self.unsubscribe_reason_id}
      projects=Project.joins("INNER JOIN projects_volunteers ON projects_volunteers.project_id=projects.id AND projects_volunteers.volunteer_id=#{self.id}").all
      pvolunteers_old_attrs=[]
      pvolunteers_new_attrs=[]
      if projects.count>0
        pvolunteers=ProjectsVolunteer.where(volunteer_id: self.id)
        pvolunteers.each do |vol|
          pvolunteers_old_attrs.push({id: vol.id, active: vol.active, unsubscribe_date: self.unsubscribe_date})
          vol.active = true
          vol.subscribe_date = Time.now.strftime("%d/%m/%Y").to_date
          if vol.save(validate: false)
            pvolunteers_new_attrs.push({id: vol.id, active: vol.active, subscribe_date: self.subscribe_date})
          end
        end
      end
      self.active = true
      self.unsubscribe_date=""
      self.subscribe_date=DateTime.now
      self.unsubscribe_reason_id=""
      save(validate: false)

      volunteer_new_attrs={id: self.id, active: self.active, unsubscribe_date: self.unsubscribe_date, subscribe_date: self.subscribe_date, unsubscribe_reason_id:self.unsubscribe_reason_id}
      volunteer_old_attrs.merge({:projects_volunteers => pvolunteers_old_attrs})
      volunteer_new_attrs.merge({:projects_volunteers => pvolunteers_new_attrs})
      ##AUDITORIA
      attr_p[:old_data] = JSON.parse(volunteer_old_attrs.to_json)
      attr_p[:new_data] = JSON.parse(volunteer_new_attrs.to_json)
      attr_p[:operation_type] = "#{I18n.t('audit.recover')}" 
      attr_p[:operation] = "#{I18n.t('audit_enum.recover', id: self.id)}"

      AuditGenerateHelper.generate(attr_p)
      self
    end

  end
end