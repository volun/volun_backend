module PtModel
  extend ActiveSupport::Concern

  included do

    has_one :project, class_name: 'Project', foreign_key: "pt_extendable_id", as: :pt_extendable, required: true
    accepts_nested_attributes_for :project

    after_initialize :build_new_project
    before_destroy :check_project_references
    validates_associated :project

    amoeba do
      enable
      nullify :id
    end

    private

   
    def build_new_project(attributes = {})
      return if persisted? || project
      build_project(attributes)
    end

    def check_project_references
      if has_project_references?
        errors.add :base, :unable_to_delete_due_to_project_references
        false
      end
    end

    def has_project_references?
      Project.where(pt_extendable_id: id, pt_extendable_type: self.class.name).exists?
    end

  end
end