module ParamsFilter
  def self.get_filter(params=nil, keys=nil, type_model_filter)
    cad_params = []
    
    keys.each do |key|
      unless params[key].blank?
        case key
        when "search_status"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t(params[key])}")
        when "search_rrss"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_end_activity"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_volun_allowed"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_urgent"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_publish"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_outstanding"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_review"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_rejected"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_processing"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_approved"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_pending"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_entity"
          id = params[key].to_i
          entidad = Entity.where(id: id).first
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{entidad.try(:name)}") unless entidad.blank?
        when "search_volun_profile"
          values_profile=[]
          params[key].split(',').each do |profile|
            if !profile.blank? && profile!="null"
              values_profile.push(profile)
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_profile.to_s}") unless values_profile.blank?
        when "search_availabilities_day"
          values_days=[]
          params[key].split(',').each do |day|
            if !day.blank? && day.to_i!=0 && day!="null"
              case day.to_i
              when 1
                values_days.push(I18n.t('enums.volun.availability.day.monday'))
              when 2
                values_days.push(I18n.t('enums.volun.availability.day.tuesday'))
              when 3
                values_days.push(I18n.t('enums.volun.availability.day.wednesday'))
              when 4
                values_days.push(I18n.t('enums.volun.availability.day.thursday'))
              when 5
                values_days.push(I18n.t('enums.volun.availability.day.friday'))
              when 6
                values_days.push(I18n.t('enums.volun.availability.day.saturday'))
              when 7
                values_days.push(I18n.t('enums.volun.availability.day.sunday'))
              else
              end
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_days.to_s}") unless values_days.blank?
        when "search_location"
          if params[key]=="-280"
            cad_params.push("#{I18n.t(type_model_filter+key)} = Fuera de Madrid")
          else
            cad_params.push("#{I18n.t(type_model_filter+key)} = Madrid")
          end
        when "search_district"
          values_district=[]          
          params[key.to_sym].split(',').each do |district|          
            if !district.blank? && district!="null"
              if district=="-280"
                values_district.push(I18n.t('others'))
              else
                values_district.push(district)
              end
            end
          end          
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_district.to_s}") unless values_district.blank?
        when "search_collective"
          values_collective=[]
          params[key].split(',').each do |collective|
            if !collective.blank? && collective.to_i!=0 && collective!="null"
              values_collective.push(collective)
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_collective.to_s}") unless values_collective.blank?
        when "search_info_source"
          values_info_source=[]
          params[key].split(',').each do |info|
            if !info.blank? && info.to_i!=0 && info!="null"
              values_info_source.push(info)
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_info_source.to_s}") unless values_info_source.blank?
        when "search_area"
          values_area=[]
          params[key].split(',').each do |area|
            if !area.blank? && area!="null"
              values_area.push(area)
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_area.to_s}") unless values_area.blank?
        when "search_type"
          values_type=[]
          params[key].split(',').each do |type|
            if !type.blank? && type.to_i!=0 && type!="null"
              pt=ProjectType.find(type.to_i)
              values_type.push(pt.to_s)
            end
          end
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{values_type.to_s}") unless values_type.blank?
        when "search_without_auth"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "search_accompany_volunteer"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "new_availability"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "ec"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        when "no_asign"
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{I18n.t('humanize.'+params[key])}")
        else
          cad_params.push("#{I18n.t(type_model_filter+key)} = #{params[key]}")
        end
      end
    end
    cad_params
  end
end