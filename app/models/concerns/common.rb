module Common
    def self.parse_to_days(date=nil)
        return 0 if date.blank?
        date / 86400
    end

    def self.get_message_tooltip(aux, model,field)
        if aux.blank?
            aux = 'Búsqueda por los campos: ' + model.human_attribute_name(field)
        else
            aux=aux +', ' + model.human_attribute_name(field) 
        end
    end
end