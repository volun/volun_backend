module Recordable
  extend ActiveSupport::Concern

  included do
    has_many :record_histories, as: :recordable

    def update_history(session_user_id)
      return unless session_user_id
      
      # attr_options={
      #   operation_type: "#{I18n.t('audit.register')}", 
      #   user_id: session_user_id,
      #   resource_id: Resource.find_by(name: RecordHistory.name).try(:id),
      #   operation: "#{I18n.t('audit_enum.register', id: self.id)}",
      #   new_data: '',
      #   old_data: ''
      # }
      record_history = RecordHistory.where(recordable_id: id, recordable_type: self.class.name)
                                    .first_or_initialize
      record_history.recordable_changed_at = updated_at unless record_history.new_record?
      record_history.user_id = session_user_id
      record_history.save! if record_history.recordable_changed_at != updated_at
      # attr_options[:new_data] = JSON.parse(record_history.attributes.to_json).except("created_at", "updated_at")

      # AuditGenerate.generate(attr_options)

    end
  end
end