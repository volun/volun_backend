module ArchivableTrackingType 
  extend ActiveSupport::Concern 
 
 included do 
 
    def archived? 
      !active? 
    end 

    def destroy(attr_p)
      tracking_old_attrs={id: self.id, active: self.active}
      exists_volun=Volun::Tracking.where("tracking_type_id=#{self.id}").count 
      exists_ent=Ent::Tracking.where("tracking_type_id=#{self.id}").count 
 
      if (exists_volun.to_i+exists_ent.to_i)>0 
        tracking_new_attrs={id: self.id, active: self.active}
        update_attribute(:active, false) 
        ##AUDITORIA
        attr_p[:old_data] = JSON.parse(tracking_old_attrs.to_json)
        attr_p[:new_data] = JSON.parse(tracking_new_attrs.to_json)
        attr_p[:operation_type] = "#{I18n.t('audit.archive')}" 
        attr_p[:operation] = "#{I18n.t('audit_enum.archive', id: self.id)}"
        AuditGenerateHelper.generate(attr_p)
      else 
        attr_p[:old_data] = JSON.parse(tracking_old_attrs.to_json)
        attr_p[:operation_type] = "#{I18n.t('audit.delete')}" 
        attr_p[:operation] = "#{I18n.t('audit_enum.delete', id: self.id)}"
        AuditGenerateHelper.generate(attr_p)
        ActiveRecord::Base.connection.execute("DELETE FROM tracking_types WHERE id=#{self.id}") 
      end 
      self 
    end 
 
    #alias_method :destroy, :archive 
 
    def recover(attr_p)
      tracking_old_attrs={id: self.id, active: self.active}
      update_attribute(:active, true)
      tracking_new_attrs={id: self.id, active: self.active}
      ##AUDITORIA
      attr_p[:old_data] = JSON.parse(tracking_old_attrs.to_json)
      attr_p[:new_data] = JSON.parse(tracking_new_attrs.to_json)
      attr_p[:operation_type] = "#{I18n.t('audit.recover')}" 
      attr_p[:operation] = "#{I18n.t('audit_enum.recover', id: self.id)}"
      AuditGenerateHelper.generate(attr_p)
      self 
    end 
 
  end 
end