module Archivable
  extend ActiveSupport::Concern

  included do

    def archived?
      !active?
    rescue
      false
    end

    def destroy
      update_attribute(:active, false)
      self
    end

    

    def recover
      update_attribute(:active, true)
      self
    end
    
  end
end