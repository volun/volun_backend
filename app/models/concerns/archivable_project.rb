module ArchivableProject
  extend ActiveSupport::Concern

  included do

    def archived?
      !active?
    end

    def destroy(attr_p)
      project_old_attrs={id: self.id, active: self.active,review_end_project: self.review_end_project, volunteers_allowed: self.volunteers_allowed, publish: self.publish, outstanding: self.outstanding, urgent: self.urgent}
      self.active = false
      self.review_end_project = false
      self.volunteers_allowed= false
      self.publish = false
      self.outstanding= false
      self.urgent= false
      self.save(validate: false)
      # update(active: false,review_end_project: false, volunteers_allowed: false, publish: false, outstanding: false, urgent: false)
      project_new_attrs={id: self.id, active: self.active,review_end_project: self.review_end_project, volunteers_allowed: self.volunteers_allowed, publish: self.publish, outstanding: self.outstanding, urgent: self.urgent}
      

      pvolunteers=ProjectsVolunteer.where(project_id: self.id)

      pvolunteers_old_attrs=[]
      pvolunteers_new_attrs=[]
      pvolunteers.each do |vol|
        pvolunteers_old_attrs.push({id: vol.id, active: vol.active, unsubscribe_date: vol.unsubscribe_date})
          
        # vol.active = false
        # vol.unsubscribe_date = Time.now.strftime("%d/%m/%Y").to_date

        # if vol.save
        #   pvolunteers_new_attrs.push({id: vol.id, active: vol.active, unsubscribe_date: vol.unsubscribe_date})
        # end
      end

      project_old_attrs.merge({:projects_volunteers => pvolunteers_old_attrs})
      project_new_attrs.merge({:projects_volunteers => pvolunteers_new_attrs})
    

      begin
        track_custom(User.find(attr_p[:user_id]).try(:loggable_id), self.id, 'unsubscribe')
      rescue        
      end
     
      
      #AUDITORIA
      begin
        attr_p[:old_data] = JSON.parse(project_old_attrs.to_json)
        attr_p[:new_data] = JSON.parse(project_new_attrs.to_json)

        
        attr_p[:operation_type] = "#{I18n.t('audit.archive')}" 
        attr_p[:operation] = "#{I18n.t('audit_enum.archive', id: self.id)}"
        AuditGenerateHelper.generate(attr_p)
      rescue
      end
      
      self
    end

    #alias_method :destroy, :archive

    def recover(attr_p)
      project_old_attrs={id: self.id, active: self.active,review_end_project: self.review_end_project}
      self.active = true
      self.review_end_project = false
      self.save(validate: false)
      # update(active: true, review_end_project: false)
      pvolunteers=ProjectsVolunteer.where(project_id: self.id)
      project_new_attrs={id: self.id, active: self.active,review_end_project: self.review_end_project}

      pvolunteers_old_attrs=[]
      pvolunteers_new_attrs=[]

      pvolunteers.each do |vol|
        pvolunteers_old_attrs.push({id: vol.id, active: vol.active, subscribe_date: vol.subscribe_date})

        vol.active = true
        vol.subscribe_date = Time.now.strftime("%d/%m/%Y").to_date
        if vol.save(validate: false)
          pvolunteers_new_attrs.push({id: vol.id, active: vol.active, subscribe_date: vol.subscribe_date})
        end
      end
      project_old_attrs.merge({:projects_volunteers => pvolunteers_old_attrs})
      project_new_attrs.merge({:projects_volunteers => pvolunteers_new_attrs})

      begin
        track_custom(User.find(attr_p[:user_id]).try(:loggable_id), self.id, 'subscribe')
      rescue
      end
      ##AUDITORIA
      begin
        attr_p[:old_data] = JSON.parse(project_old_attrs.to_json)
        attr_p[:new_data] = JSON.parse(project_new_attrs.to_json)
        attr_p[:operation_type] = "#{I18n.t('audit.recover')}" 
        attr_p[:operation] = "#{I18n.t('audit_enum.recover', id: self.id)}"
        AuditGenerateHelper.generate(attr_p)
      rescue
      end
      self
    end


    def track_custom(manager, pro_id, type, comment=nil) 
      if !pro_id.blank?
        Pro::Tracking.create!(tracking_type: TrackingType.all_projects.find_by(alias_name: type),manager_id: manager,project_id: pro_id, automatic: true, comments: comment.blank? ? I18n.t("#{type}_project_tracking") : comment, tracked_at: Time.now)
        project = Project.find_by(id: pro_id)        
        if project.subtype_proyect == "subproyect"
          Pro::Tracking.create!(tracking_type: TrackingType.all_projects.find_by(alias_name: type), manager_id: manager,project_id: project.father_id, automatic: true, comments: comment.blank? ? I18n.t("#{type}_project_tracking") : comment, tracked_at: Time.now)
        end
      end
    rescue
    end
  end
end