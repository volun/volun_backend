module RtModel
  extend ActiveSupport::Concern
  include Common

  included do

    has_one :request_form, class_name: 'RequestForm', foreign_key: "rt_extendable_id", as: :rt_extendable, required: true
    accepts_nested_attributes_for :request_form

    validate :check_request_type
    after_initialize :build_new_request_form

    default_scope -> { includes(request_form: [:status, :manager]).order('request_forms.status_date desc') }
    scope :with_statuses, ->(statuses = []){
      statuses = [statuses].flatten.compact.select{ |status_name| status_name.to_s.in? status_names } 
      return none unless statuses.any?

      where('request_forms.rt_extendable_type' => name,
            'request_forms.req_status_id' => statuses.map{ |status| get_status_id_by_kind(status) })
    }
    scope :with_status, ->(status){ with_statuses status }
    scope :pending,     ->(){ with_status(:pending) }
    scope :processing,  ->(){ with_status(:processing) }
    scope :approved,    ->(){ with_status(:approved) }
    scope :rejected,    ->(){ with_status(:rejected) }
    scope :appointed,   ->(){ with_status(:appointed) }


    class << self
      delegate :status_names, :get_status_id_by_kind, to: RequestForm
    end

    def self.to_csv(requests)
      CSV.generate(:col_sep =>';', encoding:'ISO-8859-1') do |csv|
        csv << main_columns.map{ |column_name| human_attribute_name(column_name)}
          requests.each do |r|
            unless r.request_form.blank? 
              csv << main_columns.map{ |column_name| (r.public_send column_name).to_s.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) } 
            else
              csv << main_columns.map{ |column_name| (r.public_send column_name).to_s.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}) }
            end
          end
      end
    end

    def days_passed
      return 0 if request_form.blank?
      final_date = request_form.approved? || request_form.rejected? || request_form.processing? ?
        (request_form.status_date - request_form.created_at).to_i :
        (Time.zone.now - Time.zone.parse(request_form.created_at.to_s)).to_i
      Common.parse_to_days(final_date)
    end

    def get_manager
      request_form.try :manager
    end

    def get_status
      request_form.try :status
    end

    def get_date_status
      request_form.try :status_date
    end

    def copy_errors_from(record)
      record.errors.each do |error|
        self.errors.add(error.attribute,error.message)
      end
      nil
    end
  
    def copy_errors_from!(record)
      copy_errors_from(record)
      raise ActiveRecord::Rollback
    end

    def address
      "#{self.road_type}/#{self.road_name}, #{self.number_type}#{self.road_number},<br>
      (#{self.province.try(:name)}) #{self.town}, #{self.borought.try(:name)}, #{self.district.try(:name)}<br>
      #{self.postal_code}".html_safe  
    rescue
        ""
    end

    def entity_name_table
      entity.name
    rescue
      ""
    end
  
    def entity_phone_number
      entity.phone_number
    rescue
      ""
    end
  
    def entity_email
      entity.email
    rescue
      ""
    end

    private

    
    def request_type_valid?
      self.class.model_name.singular == request_form&.request_type&.kind
    end

    def check_request_type
      errors.add(:base, :invalid_request_type) unless request_type_valid?
    end

    def build_new_request_form(attributes = {})       
      return if persisted?
      attributes.reverse_merge!(
        request_type: RequestType.where(kind: RequestType.kinds[self.class.model_name.singular]).take,
        req_status_id: 1,
        status_date: Time.now,
        req_reason_id:  self.request_form != nil ? self.request_form.req_reason_id : nil
      )
      build_request_form(attributes)
    end

    def check_request_form_references
      if has_request_form_references?
        errors.add :base, :unable_to_delete_due_to_request_form_references
        false
      end
    end

    def has_request_form_references?
      RequestForm.where(rt_extendable_id: id, rt_extendable_type: self.class.name).exists?
    end

  end
end