# == BdcCompatible
# The class that includes this module must be an ActiveRecord subclass
# and must implement the following methods:
#
# +country+
# +province+
# +town+
# +road_type+
# +road_name+
# +road_number_type+
# +road_number+
# +grader+
# +stairs+
# +floor+
# +door+
# +ndp_code+
# +province+
# +province_code+
# +town+
# +town_code+
# +district+
# +district_code+
# +borough+
# +local_code+
# +latitude+
# +longitude+
# +normalize+
#

module BdcCompatible
  extend ActiveSupport::Concern
  require 'net/http' 
  require 'json' 
  included do

    @@TABLE_BOROUGH = {
      "PALACIO" => 1,
      "EMBAJADORES" => 2,
      "CORTES" => 3,
      "JUSTICIA" => 4,
      "UNIVERSIDAD" => 5,
      "SOL" => 6,
      "IMPERIAL" => 1,
      "ACACIAS" => 2,
      "CHOPERA" => 3,
      "LEGAZPI" => 4,
      "DELICIAS" => 5,
      "PALOS DE MOGUER" => 6,
      "ATOCHA" => 7,
      "PACIFICO" => 1,
      "ADELFAS" => 2,
      "ESTRELLA" => 3,
      "IBIZA" => 4,
      "LOS JERONIMOS" => 5,
      "NIÑO JESUS" => 6,
      "RECOLETOS" => 1,
      "GOYA" => 2,
      "FUENTE DEL BERRO" => 3,
      "GUINDALERA" => 4,
      "LISTA" => 5,
      "CASTELLANA" => 6,
      "EL VISO" => 1,
      "PROSPERIDAD" => 2,
      "CIUDAD JARDIN" => 3,
      "HISPANOAMERICA" => 4,
      "NUEVA ESPAÑA" => 5,
      "CASTILLA" => 6,
      "BELLAS VISTAS" => 1,
      "CUATRO CAMINOS" => 2,
      "CASTILLEJOS" => 3,
      "ALMENARA" => 4,
      "VALDEACEDERAS" => 5,
      "BERRUGUETE" => 6,
      "GAZTAMBIDE" => 1,
      "ARAPILES" => 2,
      "TRAFALGAR" => 3,
      "ALMAGRO" => 4,
      "RIOS ROSAS" => 5,
      "VALLEHERMOSO" => 6,
      "EL PARDO" => 1,
      "FUENTELARREINA" => 2,
      "PEÑA GRANDE" => 3,
      "EL PILAR" => 4,
      "LA PAZ" => 5,
      "VALVERDE" => 6,
      "MIRASIERRA" => 7,
      "EL GOLOSO" => 8,
      "CASA DE CAMPO" => 1,
      "ARGUELLES" => 2,
      "CIUDAD UNIVERSITARIA" => 3,
      "VALDEZARZA" => 4,
      "VALDEMARIN" => 5,
      "EL PLANTIO" => 6,
      "ARAVACA" => 7,
      "LOS CARMENES" => 1,
      "PUERTA DEL ANGEL" => 2,
      "LUCERO" => 3,
      "ALUCHE" => 4,
      "CAMPAMENTO" => 5,
      "CUATRO VIENTOS" => 6,
      "LAS AGUILAS" => 7,
      "COMILLAS" => 1,
      "OPAÑEL" => 2,
      "SAN ISIDRO" => 3,
      "VISTA ALEGRE" => 4,
      "PUERTA BONITA" => 5,
      "BUENAVISTA" => 6,
      "ABRANTES" => 7,
      "ORCASITAS" => 1,
      "ORCASUR" => 2,
      "SAN FERMIN" => 3,
      "ALMENDRALES" => 4,
      "MOSCARDO" => 5,
      "ZOFIO" => 6,
      "PRADOLONGO" => 7,
      "ENTREVIAS" => 1,
      "SAN DIEGO" => 2,
      "PALOMERAS BAJAS" => 3,
      "PALOMERAS SURESTE" => 4,
      "PORTAZGO" => 5,
      "NUMANCIA" => 6,
      "PAVONES" => 1,
      "HORCAJO" => 2,
      "MARROQUINA" => 3,
      "MEDIA LEGUA" => 4,
      "FONTARRON" => 5,
      "VINATEROS" => 6,
      "VENTAS" => 1,
      "PUEBLO NUEVO" => 2,
      "QUINTANA" => 3,
      "CONCEPCION" => 4,
      "SAN PASCUAL" => 5,
      "SAN JUAN BAUTISTA" => 6,
      "COLINA" => 7,
      "ATALAYA" => 8,
      "COSTILLARES" => 9,
      "PALOMAS" => 1,
      "PIOVERA" => 2,
      "CANILLAS" => 3,
      "PINAR DEL REY" => 4,
      "APOSTOL SANTIAGO" => 5,
      "VALDEFUENTES" => 6,
      "VILLAVERDE ALTO C.H." => 1,
      "SAN CRISTOBAL" => 2,
      "BUTARQUE" => 3,
      "LOS ROSALES" => 4,
      "LOS ANGELES" => 5,
      "CASCO H.VALLECAS" => 1,
      "SANTA EUGENIA" => 2,
      "ENSANCHE DE VALLECAS" => 3,
      "CASCO H.VICALVARO" => 1,
      "VALDEBERNARDO" => 2,
      "VALDERRIVAS" => 3,
      "EL CAÑAVERAL" => 4,
      "SIMANCAS" => 1,
      "HELLIN" => 2,
      "AMPOSTA" => 3,
      "ARCOS" => 4,
      "ROSAS" => 5,
      "REJAS" => 6,
      "CANILLEJAS" => 7,
      "EL SALVADOR" => 8,
      "ALAMEDA DE OSUNA" => 1,
      "AEROPUERTO" => 2,
      "CASCO H.BARAJAS" => 3,
      "TIMON" => 4,
      "CORRALEJOS" => 5
  }
    attr_accessor :bdc_validator

    before_validation :check_normalization, if: -> { normalize? }
    after_validation :unnormalize_fields, on:[:create,:update], unless: -> { normalize? } 
    
    def normalized?
      ndp_code.present?
    end

    def bdc_validator
      @bdc_validator ||= BdcValidator.new(bdc_fields)
    end

    def bdc_address_result
      bdc_validator.search_towns
    end

    def reset_bdc_validator
      unnormalize_fields
      self.bdc_validator = nil
    end

    private

    # BDC service need two white spaces in the grader field in order to look for
    # an address without a grader, thus an empty string will be ignored.
    def normalize_grader
      self.grader = '  ' if grader.blank?
    end

    def normalize_road_number
      self.road_number = road_number.to_s.to_i
    end

    # =============================================================
    # VOLUN 180 AÑADIR FILTRO OTROS
    # =============================================================
    def district_fields(name,code)
      #if self.postal_code.match(/^280[0-9]+/) # Si empieza por 280XX
        self.district      = District.find_by("cast(code as int)=?", code.to_i)
        self.borought       = Borought.find_by("cast(code as int) = ? and district_id=?",bdc_validator.borough_code.to_i, self.district.try(:id))
     

     
        # else
      #   distrito=District.find_by(code: "99" ) # Si no empieza por 280XX
      #   self.district      = District.find_by(code: distrito.code)
      #   self.borought       = Borought.find_by(code: bdc_validator.borough_code,  district_id: self.district.try(:id))
      # end
    end
    # =============================================================

    def unnormalize_fields
      self.normalize     = false
      # self.ndp_code      = nil
      # self.province_code = nil
      # self.town_code     = nil
      # district_fields(nil,nil) # Tratamiento de distritos
      # self.borough       = nil
      # self.borough_code  = nil
      # self.local_code    = nil
      # self.latitude      = nil
      # self.longitude     = nil
      # self.xetrs89       = nil 
      # self.yetrs89       = nil
      #pruebas en local
      #lat_aux       =44479522/100.0 
      #long_aux      =448351309/100.0 
    end

    def normalize_fields
      self.postal_code   = bdc_validator.postal_code || self.postal_code
      self.ndp_code      = bdc_validator.ndp_code
      self.road_type     = bdc_validator.road_type      
      self.town          = bdc_validator.town
      self.town_code     = bdc_validator.town_code
      self.country       = bdc_validator.country
      self.country_code  = bdc_validator.country_code
      district_fields(bdc_validator.district,bdc_validator.district_code) # Tratamiento de distritos
      self.local_code    = bdc_validator.local_code
      self.latitude      = bdc_validator.latitude
      self.longitude     = bdc_validator.longitude      

      begin
        self.province      = Province.where(code: bdc_validator.province_code).first
      rescue
      end
      
      
      begin
        lat_aux       = self.latitude.to_i/100.0 
        long_aux      = self.longitude.to_i/100.0 

        url = "#{Rails.application.secrets.bdc_url}/conversionETRS89?coordX=#{lat_aux}&coordY=#{long_aux}&aplicacion=VOLUN" 

        uri = URI.parse(url) 
        response = Net::HTTP.get(uri) 
        hash_89 = JSON.parse(response) 
        
        self.xetrs89 = hash_89["coordX"] 
        self.yetrs89 = hash_89["coordY"] 

      rescue => exception
        self.xetrs89 = nil
        self.yetrs89 = nil
      end
    end

    def check_normalization
      normalize_grader
      normalize_road_number
      if bdc_validator.address_normalized?
        normalize_fields
      elsif normalize?
        errors.add :base, :address_is_not_normalized
      end
    end

    def bdc_fields
      {
        country:          country,
        province:         province,
        town:             town,
        road_type:        road_type.upcase,
        road_name:        road_name,
        road_number_type: road_number_type,
        road_number:      road_number,
        grader:           grader,
        stairs:           stairs,
        floor:            floor,
        door:             door,
        bdc_exchange:     '',
        aplication:       Rails.application.secrets.bdc_app_name
      }
    end
  end

end