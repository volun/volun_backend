class Proposal < ActiveRecord::Base
  include Archivable
  
  ######## RELACIONES ########
  has_many :pt_subventions, class_name: "Pt::Subvention"

  ######## VALIDACIONES ########
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end
end
