class Ahoy::Visit < ActiveRecord::Base
  self.table_name = "ahoy_visits"

  has_many :events, class_name: "Ahoy::Event"
  
  belongs_to :user

  scope :num_logs, -> {where("user_id IS NOT NULL")}
end
