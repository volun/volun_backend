class Ahoy::Event < ActiveRecord::Base
  # include Ahoy::QueryMethods

  self.table_name = "ahoy_events"

  belongs_to :visit, class_name: "Ahoy::Visit", foreign_key: "ahoy_visit_id", optional: true
  belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true

  scope :num_visit, -> {where(name: "Visit").count}
  
end
