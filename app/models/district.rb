class District < ActiveRecord::Base
  include Archivable

  has_and_belongs_to_many :volunteers, class_name: "Volunteer", foreign_key: "district_id"

  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.main_columns
    [:id, :name, :code, :active]
  end

  def self.tooltip
      aux = ''
      [:name,:code,:id].each do |field|
          aux = Common.get_message_tooltip(aux, District,field)
      end
      aux
  end
end
