class Permission < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id"

  ######## MÉTODOS ########
  def self.auth_permissions
    ["Volunteer","Project","Entity","Activity","Citizen", "CitizenService","CitizenService::Tracking", "Citizen::Tracking", "Ent::Tracking", "Volun::Tracking", "Pro::Tracking","Rt::ProjectPublishing", "Rt::Other", "Rt::VolunteersDemand", "Rt::VolunteerSubscribe", "Rt::VolunteerAppointment", "Rt::ActivityPublishing", "Rt::VolunteerProjectSubscribe", "Rt::SolidarityChain", "Rt::VolunteerAmendment", "Digital::Manage","Administration"]
  end

  def self.generate_default
    ["Volunteer" => {
      basics: {
          create: '0',
          update: '0',
          read:  '0',
          destroy: '0'
      },
      specifics: {
          mail: '0',
          sms: '0'
      }},
      "Project" => {
        basics: {
            create: '0',
            update: '0',
            read: '0',
            destroy: '0'
        },
        specifics: {
            clone_project: '0',
            volunteer_allowed: '0',
            publish: '0'
      }},
      "Entity" => {
        create:'0',
        update:'0',
        read: '0',
        destroy: '0'
      },
      "Activity" => {
        create:'0',
        update:'0',
        read: '0',
        destroy: '0'
      }, 
      "Citizen" => {
        create: '0',
        update: '0',
        read: '0',
        destroy: '0'
      },
      "CitizenService" => {
        create: '0',
        update: '0',
        read: '0',
        destroy: '0'
      },
      "CitizenService::Tracking"=>{
        create: '0',
        update: '0',
        read: '0'
      },
      "Citizen::Tracking"=>{
        create: '0',
        update: '0',
        read: '0'
      },
      "Ent::Tracking"=>{
        create: '0',
        update: '0',
        read: '0'
      },
      "Volun::Tracking" => {
        create: '0',
        update: '0',
        read: '0'
      }, 
      "Pro::Tracking" => {
        create: '0',
        update: '0',
        read: '0'
      },
      "Rt::ProjectPublishing" =>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, 
      "Rt::Other"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      },
      "Rt::VolunteersDemand"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, 
      "Rt::VolunteerSubscribe"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      },
      "Rt::VolunteerAppointment"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, 
      "Rt::ActivityPublishing"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, 
      "Rt::VolunteerProjectSubscribe"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, 
      "Rt::SolidarityChain"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      }, "Rt::VolunteerAmendment"=>{
        read: '0',
        process: '0',
        approve: '0',
        reject:'0',
        mark_processing: '0',
        mark_pending: '0',
        mark_appointed: '0'
      },
      "Administration" => {resource: '0',
        catalogs: '0',
        new_campaign: '0',
        managers: '0',
        config: '0',
        order: '0',
        analyze_cv: '0',
        virtual_community: '0'
      },
      "Digital::Manage" => {
        read: '0',
        faq: '0',
        topic: '0',
        moderate: '0'
      }
    ]
  end

  def self.generate_permissions(manager=nil)
    return if manager.blank?
    Permission.generate_default.each do |p|
      p.each do |k,v|
        if Permission.find_by(manager_id: manager, section: k).blank?
          Permission.create(manager_id: manager, section: k, data: v)
        end
      end
    end
  end
end
