class UnsubscribeReason < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common
  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true
  
  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, UnsubscribeReason,field)
    end
    aux
  end
end
