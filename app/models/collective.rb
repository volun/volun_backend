class Collective < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common

  ######## RELACIONES ########
  has_and_belongs_to_many :projects, class_name: "Project", foreign_key: "collective_id"
  has_and_belongs_to_many :volunteers, class_name: "Volunteer", foreign_key: "collective_id"

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  def self.main_columns
    [:id, :name, :active]
  end

  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Collective,field)     
    end
    aux
  end
end
