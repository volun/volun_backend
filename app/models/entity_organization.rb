class EntityOrganization < ActiveRecord::Base
    ######## RELACIONES ########
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    
    ######## RELAVALIDACIONESCIONES ########
    validates :name, presence: true
end
