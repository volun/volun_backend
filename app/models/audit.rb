class Audit < ActiveRecord::Base
    extend AuditsHelper
    ######## RELACIONES ########
    belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true
    belongs_to :resource, class_name: "Resource", foreign_key: "resource_id", optional: true

    ######## ATRIBUTOS ANIDADOS ########
    accepts_nested_attributes_for :user
    accepts_nested_attributes_for :resource

  ######## MÉTODOS ########
    def self.main_columns
        %i( 
           user_name
           user_type
           resource 
           created_at
           operation
           old_data
           new_data)
    end

    def self.main_attributes
        %i( 
           user_name
           user_type
           resource 
           created_at
           operation
           old_data
           new_data)
    end
    
    def self.getOperation
        hash = {
            "#{I18n.t('audit.register')}": "#{I18n.t('audit.register')}",
            "#{I18n.t('audit.unsubscribe')}": "#{I18n.t('audit.unsubscribe')}",
            "#{I18n.t('audit.update')}": "#{I18n.t('audit.update')}",
            "#{I18n.t('audit.process')}": "#{I18n.t('audit.process')}",
            "#{I18n.t('audit.delete')}": "#{I18n.t('audit.delete')}",
            "#{I18n.t('audit.assignment')}": "#{I18n.t('audit.assignment')}",
            "#{I18n.t('audit.unassignment')}": "#{I18n.t('audit.unassignment')}",
            "#{I18n.t('audit.request_reject')}": "#{I18n.t('audit.request_reject')}",
            "#{I18n.t('audit.request_accept')}": "#{I18n.t('audit.request_accept')}",
            "#{I18n.t('audit.request_pending')}": "#{I18n.t('audit.request_pending')}",
            "#{I18n.t('audit.recover')}": "#{I18n.t('audit.recover')}",
            "#{I18n.t('audit.archive')}": "#{I18n.t('audit.archive')}",
            "#{I18n.t('audit.email_send')}": "#{I18n.t('audit.email_send')}",
            "#{I18n.t('audit.sms_send')}": "#{I18n.t('audit.sms_send')}",
            "#{I18n.t('audit.clone')}": "#{I18n.t('audit.clone')}",
            "#{I18n.t('audit.unlink')}": "#{I18n.t('audit.unlink')}",
            "#{I18n.t('audit.unlink_all')}": "#{I18n.t('audit.unlink_all')}"
        }
        hash.sort.to_h
    end

    def self.getType
        hash = {
            "#{I18n.t('audit.volunteer')}": "volunteer",
            "#{I18n.t('audit.manager')}": "manager",
            "#{I18n.t('audit.entity')}": "entity",
            "#{I18n.t('audit.non_register')}": "null"
        }

        hash.sort.to_h
    end

    def self.get_filter(params)
        keys = [
          'enter_text',
          'search_manager',
          'search_resource',
          'search_operation',
          'search_type',
          'search_start_date',
          'search_end_date' ]
        ParamsFilter.get_filter(params, keys, 'audit_filter.')
      end

    def audit_attributes
    {
        user_name:      "#{user.try(:loggable).try(:full_name) unless user.blank?}",
        user_type:      (unless user.blank?
                            "#{I18n.t('audit.' + user.loggable_type) }"
                        else 
                            "#{I18n.t('audit.non_register')}"
                        end),
        resource:       "#{resource.description + " (#{format_data(self.old_data).blank? ? format_data(self.new_data).blank? ? 'N/A' : format_data(self.new_data)[:id] : format_data(self.old_data)[:id]})" unless resource.blank?}",
        created_at:     self.created_at.strftime("%d/%m/%Y"),
        operation:      "#{operation unless operation.blank?}",
        old_data:       "#{self.old_data.truncate(27) unless self.old_data.blank?}",
        new_data:       "#{self.new_data.truncate(27) unless self.new_data.blank?}"
    }
    end


    def old_data_format
        format_string(self.old_data)
    end

    def new_data_format
        format_string(self.new_data, self.old_data)
    end

    private

    def format_data(data = nil)
        return if data.blank?
        data.gsub(/[{}:]/,'').split(', ').map{|h| h1,h2 = h.split('=>'); {h1.parameterize.underscore.to_sym => h2}}.reduce(:merge)
    end

    def format_string(text, comp = nil)
        return "" if text.blank?
        aux = "<dl class=\"dl-horizontal\">"
        data = format_data(text) 
        data_com = format_data(comp)  unless comp.blank?

        if !data.blank?
            data.each {|k,v| aux = aux + "<dt>#{self.resource.name.constantize.human_attribute_name(k)}:</dt><dd #{"style='color: red'" if !comp.blank? && data_com[k].to_s != v.to_s}>#{v}</dd>" }
        end

        aux = aux + "</dl>"
        aux.html_safe
    end
end
