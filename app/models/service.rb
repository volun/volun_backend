class Service < ActiveRecord::Base
    include Archivable
    include Constants
    has_many :citizen_services, class_name: "CitizenService", foreign_key: "service_id"
    validates :code, uniqueness: true
    validates :name, uniqueness: true

    accepts_nested_attributes_for :citizen_services, allow_destroy: true


    scope :all_active,   ->(){ where(active: true) }
    scope :all_prepared_active, -> () { where("status in ('active','accept','prioritary')")}

    def self.status_type_list
        [:active,:accept,:canceled,:prioritary,:finished,:undone,:expired]
    end

    def self.status_key_list
        [:list_active,:list_inactive]
    end

    def to_s
        name.blank? ? description : name
    rescue
        ""
    end

    def total_uses
        return 0 if self.citizen_services.blank?
        self.citizen_services.count
    end

    def total_percentage
        return 0 if self.citizen_services.blank?
        services_c = self.citizen_services.count
        services_all = CitizenService.all.count
        return 0 if services_all.blank?

        (services_c * 100) / services_all
    end

    def self.tooltip
        aux = ''
        [:id,:name,:description,:code].each do |field|
          aux = Common.get_message_tooltip(aux, Service,field)     
        end
        aux
      end
end