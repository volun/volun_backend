class RequestForm < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :rt_extendable, polymorphic: true, optional: true
  belongs_to :request_type, required: true, class_name: "RequestType", foreign_key: "request_type_id"
  belongs_to :rejection_type, -> { where(active: true).order(:name) }, class_name: 'Req::RejectionType', foreign_key: 'req_rejection_type_id', optional: true
  belongs_to :inactive_rejection_type, -> { where(active: false) }, class_name: 'Req::RejectionType', foreign_key: 'req_rejection_type_id', optional: true
  belongs_to :reason, class_name: 'Req::Reason', foreign_key: 'req_reason_id', optional: true
  belongs_to :status, class_name: 'Req::Status', foreign_key: 'req_status_id', optional: true
  belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  has_many :events, as: :eventable
  has_many :status_traces, :class_name => 'Req::StatusTrace'
  has_many :volun_trackings, :class_name => 'Volun::Tracking'
  has_many :ent_trackings, :class_name => 'Ent::Tracking'
  has_many :pro_trackings, :class_name => 'Pro::Tracking'
  has_many :citizen_trackings, :class_name => 'CitizenTracking'

  ######## ATRIBUTOS ANIDADOS ########
  accepts_nested_attributes_for :rt_extendable

  ######## VALIDACIONES ########
  validates :req_rejection_type_id, presence: true, if: ->{req_status_id_changed? && rejected?}
  validate :req_rejection_type_no_presence
  validate :permitted_status_change_flow, if: ->{req_status_id_changed?}
  class << self
    delegate :kinds, :kinds_i18n, to: Req::Status
  end
  validates :req_rejection_type_id, presence: true, if: ->{req_status_id_changed? && rejected?}
  validate :req_rejection_type_no_presence
  validate :permitted_status_change_flow, if: ->{req_status_id_changed?}

  ######## SCOPES ########
  delegate :appointed?, :pending?, :processing?, :approved?, :rejected?, :kind_i18n, to: :status
  default_scope ->{ includes(:request_type) }
  scope :pending,    ->(){ includes(:status).where(req_statuses: { kind: kinds[:pending] }) }
  scope :processing, ->(){ includes(:status).where(req_statuses: { kind: kinds[:processing] }) }
  scope :approved,   ->(){ includes(:status).where(req_statuses: { kind: kinds[:approved] }) }
  scope :rejected,   ->(){ includes(:status).where(req_statuses: { kind: kinds[:rejected] }) }
  scope :appointed,  ->(){ includes(:status).where(req_statuses: { kind: kinds[:appointed] }) }

  ######## MÉTODOS ########
  def self.main_columns
    %i(request_type user status status_date reason)
  end
  
  def self.statuses_i18n
    kinds_i18n
  end

  def self.statuses
    kinds
  end

  def self.get_status_id_by_kind(status)
    Req::Status.send(status).take.try :id
  end

  def self.status_names
    statuses.keys
  end

  def pending!
    update_status!(:pending)
  end

  def processing!
    update_status!(:processing)
  end

  def approved!
    update_status!(:approved)
  end

  def rejected!
    update_status!(:rejected)
  end

  def appointed!
    update_status!(:appointed)
  end

  def update_status!(status_name)
    return unless status_name.to_s.in? self.class.status_names
    update!(status_date: DateTime.now, req_status_id: self.class.get_status_id_by_kind(status_name))
  end

  def status_i18n
    kind_i18n
  end

  alias_method :status_name_i18n, :status_i18n

  def status_name
    status.kind
  end

  def build_rt_extendable(attributes = {})
    return unless request_type.extendable?
    self.rt_extendable = rt_extendable_class.new(attributes.merge(request_form: self))
  end

  def update_and_trace_status(status_name, attributes = {})
    return true if status.kind == status_name.to_s && manager_id != attributes[:manager_id]

    if status_name.to_s.in? self.class.status_names
      attributes.merge!(
        status_date: DateTime.now,
        req_status_id: self.class.get_status_id_by_kind(status_name),
      )
      aux_attr = self.attributes
      self.assign_attributes(attributes)
      self.validate
      if errors.blank?
        if self.update(attributes)
          create_status_trace 
        end
      else
        self.assign_attributes(aux_attr)
      end
    else
      raise "#{I18n.t('errors.status_name_not_in')} #{self.class.status_names}"
    end
    errors.blank?
  end

  def create_status_trace(validate = false)
    status_trace = Req::StatusTrace.new(status: status, request_form: self, manager: manager)
    status_trace.save(validate: validate)
  end

  private
  def rt_extendable_class
    rt_extendable.try(:class) || request_type.kind.classify.sub(/\ARt/, 'Rt::').constantize
  end

  def req_rejection_type_no_presence
    if rejection_type.present? && !rejected?
      errors.add :req_rejection_type_id, :cannot_be_present_if_is_not_rejected
    end
  end

  def permitted_status_change_flow
    case self
    when ->(rf){ rf.pending? }
      add_status_error :cannot_change_to_pending    unless can_change_to_pending?
    when ->(rf){ rf.processing? }
      add_status_error :cannot_change_to_processing unless (status_was?(:pending) || status_was?(:appointed)) 
    when ->(rf){ rf.approved? }
      add_status_error :cannot_change_to_approved   unless status_was?(:processing)
    when ->(rf){ rf.rejected? }
      add_status_error :cannot_change_to_rejected   unless status_was?(:processing)
    when ->(rf){ rf.appointed? }
      add_status_error :cannot_change_to_appointed  unless status_was?(:processing)
    else
      nil
    end
  end

  def add_status_error(error_name)
    errors.add :req_status_id, error_name
    nil
  end

  def status_was?(status_name)
    req_status_id_was == self.class.get_status_id_by_kind(status_name)
  end

  def can_change_to_pending?
    status_was?(:processing) || status_was?(:rejected) || status_was?(:approved) || status_was?(:appointed) || req_status_id_was == nil
  end
end
