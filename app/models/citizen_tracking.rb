class CitizenTracking < ApplicationRecord
  extend CitizenTrackingsHelper
  ######## RELACIONES ########
  belongs_to :citizen ,class_name: "Citizen", foreign_key: "citizen_id", optional: true
  belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
  belongs_to :manager ,class_name: "Manager", foreign_key: "manager_id", optional: true

  accepts_nested_attributes_for :citizen

  ######## MÉTODOS ########
  def self.main_columns
    %i(citizen tracking_type manager tracked_at automatic coments)
  end
  
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_enter_text',
      'search_tipo',
      'search_manager']
    ParamsFilter.get_filter(params, keys, 'citizen_tracking_filter.')
  end

  def self.tooltip
    aux = ''
    [:coments, :citizen, :manager,:tracking_type].each do |field|
      aux = Common.get_message_tooltip(aux, CitizenTracking,field)     
    end   
    aux
  end
  
end
