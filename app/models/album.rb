class Album < ActiveRecord::Base
  ######## RELACIONES ########
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    has_many :links, as: :linkable, dependent: :destroy, class_name: "Link", foreign_key: "linkable_id"
    has_many :images, -> { album_images }, class_name: 'Link', foreign_key: 'linkable_id'
    has_many :videos, -> { album_videos }, class_name: 'Link', foreign_key: 'linkable_id'

  ######## ATRIBUTOS ANIDADOS ########
    accepts_nested_attributes_for :images, reject_if:  :all_blank, allow_destroy: true
    accepts_nested_attributes_for :videos, reject_if:  :all_blank, allow_destroy: true

  ######## VALIDACIONES ########
    validates :year, presence: true

end
