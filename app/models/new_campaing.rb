class NewCampaing < ActiveRecord::Base
    ######## RELACIONES ########
    has_many :links, as: :linkable
    has_many :videos, -> { new_campaing_videos }, class_name: 'Link', foreign_key: 'linkable_id'
    has_many :images, -> { new_campaing_images }, class_name: 'Link', foreign_key: 'linkable_id'
    
    ######## ATRIBUTOS ANIDADOS ########
    accepts_nested_attributes_for :images, reject_if:  :all_blank, allow_destroy: true
    accepts_nested_attributes_for :videos, reject_if:  :all_blank, allow_destroy: true
    
    ######## VALIDACIONES ########
    validates :title, presence: true, uniqueness: true
    
    ######## MÉTODOS ########
    def self.main_columns
        [:id, :title, :body, :publicable]
    end
    
    def to_s
        title
    end
end
