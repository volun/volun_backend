class Profile < ActiveRecord::Base
  include Archivable
  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true
  
  ######## MÉTODOS ########
  def to_s
    name
  end
end
