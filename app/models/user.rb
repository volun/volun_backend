class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable

  before_save :ensure_authentication_token
  ######## RELACIONES ########
  belongs_to :notice_type, class_name: "NoticeType", foreign_key: "notice_type_id", optional: true
  belongs_to :loggable, polymorphic: true, optional: true #required: true
  #belongs_to :manager, -> { where( loggable_type: Manager.name ) }, class_name: "Manager", foreign_key: 'loggable_id'
  has_many :audits, class_name: "Audit"
  has_many :digital_votes, as: :userable, class_name: "Digital::Vote"

  ######## VALIDACIONES ########
  validates :notice_type_id, presence: true, if: ->{volunteer?}
  validates :loggable_id, :loggable_type, :login, presence: true
  validates :email, presence: true, uniqueness: true

  ######## SCOPES ########
  default_scope ->{ includes(:loggable) }
  scope :loggable_type, -> {where(loggable_type: "Manager")}

  ######## MÉTODOS ########
  def to_s
    login || email
  end

  def volunteer?
    loggable_type == Volunteer.name
  end

  def entity?
    loggable_type == Entity.name
  end

  def citizen?
    loggable_type == Citizen.name
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def login_required?
    true
  end
end
