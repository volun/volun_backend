class Volun::Contact < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :contact_result, class_name: "ContactResult", foreign_key: "contact_result_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true

  ######## VALIDACIONES ########
  validates :volunteer_id, :contact_result_id, :project_id, :manager_id, :contact_date, presence: true

end
