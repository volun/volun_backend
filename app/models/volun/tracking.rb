class Volun::Tracking < ActiveRecord::Base
  extend VolunTrackingsHelper
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true

  ######## VALIDACIONES ########
  validates :volunteer_id, :tracking_type_id, :tracked_at, presence: true

  ######## MÉTODOS ########
  def self.main_columns
    %i(volunteer tracking_type project manager tracked_at automatic comments request_form)
  end

  private
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_enter_text',
      'search_project',
      'search_tipo',
      'search_inicio_f',
      'search_fin_f',
      'search_manager']
    ParamsFilter.get_filter(params, keys, 'volun_tracking_filter.')
  end

  def self.tooltip
    aux = ''
    [:manager,:project,:tracking_type,:volunteer].each do |field|
      aux = Common.get_message_tooltip(aux, Volun::Tracking,field)     
    end
    aux
  end
  
end
