class Volun::Assessment < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :trait, ->{ where(active: true).order(:name) }, class_name: "Trait", foreign_key: "trait_id", optional: true
  
  ######## VALIDACIONES ########
  validates :volunteer_id, :trait_id, presence: true, on: :update
  validates :trait_id, uniqueness: { scope: :volunteer_id }, on: :update

end
