class Volun::KnownLanguage < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :language, -> {order(:name)}, required: true, class_name: "Language", foreign_key: "language_id"
  belongs_to :language_level, required: true, class_name: "LanguageLevel", foreign_key: "language_level_id"
end
