class Volun::AssessmentsProject < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :trait, ->{ where(active: true).order(:name)  }, class_name: "Trait", foreign_key: "trait_id", optional: true
  belongs_to :project, ->{ where(active: true).order('projects.name asc') }, class_name: "Project", foreign_key: "project_id", optional: true

  ######## VALIDACIONES ########
  validates :volunteer_id, :trait_id, :project_id, presence: true
end