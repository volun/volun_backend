class LanguageVolunteer < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :language, class_name: "Language", foreign_key: "language_id", optional: true
  belongs_to :language_level, class_name: "LanguageLevel", foreign_key: "language_level_id", optional: true
end
