class Ent::Tracking < ActiveRecord::Base
  extend EntTrackingsHelper
  ######## RELACIONES ########
  belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id"
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id"
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true

  ######## VALIDACIONES ########
  validates :tracked_at, presence: true
  
  ######## MÉTODOS ########
  def self.main_columns
    %i(entity tracking_type manager tracked_at automatic comments request_form)
  end
  
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_enter_text',
      'search_tipo',
      'search_manager']
    ParamsFilter.get_filter(params, keys, 'ent_tracking_filter.')
  end

  def self.tooltip
    aux = ''
    [:comments,:entity,:manager,:tracking_type].each do |field|
      aux = Common.get_message_tooltip(aux, Ent::Tracking,field)     
    end
    aux
  end

end
