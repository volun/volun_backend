class InfoSource < ActiveRecord::Base
  include Archivable

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODO ########
  has_and_belongs_to_many :volunteers, class_name: "Volunteer", optional: true
  
  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, InfoSource,field)     
    end
    aux
  end
end
