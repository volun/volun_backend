class ServiceResponse < ActiveRecord::Base
    belongs_to :citizen_service, class_name: "CitizenService", foreign_key: "citizen_service_id", optional: true
    belongs_to :service_question, class_name: "ServiceQuestion", foreign_key: "service_question_id", optional: true

    def question
        return  '' if self.service_question.blank?
        self.service_question.description
    end

    def response_question
        return  '' if self.service_question.blank?
        if self.service_question.question_type == "boolean"
            I18n.t("humanize.#{self.response}")
        else
            self.response
        end
    end

end