class EntityType < ActiveRecord::Base
  include Archivable
  ######## VALIDACIONES ########
  validates :name, presence: true, uniqueness: true

  ######## MÉTODOS ########
  def to_s
    name
  end
end
