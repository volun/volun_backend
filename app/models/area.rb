class Area < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common

  ######## RELACIONES ########
  has_and_belongs_to_many :projects, class_name: "Project", foreign_key: "area_id", optional: true
  has_and_belongs_to_many :volunteers, class_name: "Volunteer", foreign_key: "area_id", optional: true
  has_many :activities, class_name: "Activity", foreign_key: "area_id"

  ######## VALIDACIONES ########
  validates :name, presence: true, uniqueness: true

  ######## MÉTODOS ########
  def self.main_columns
    [:id, :name, :active]
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Area,field)      
    end
    aux
  end

  def to_s
    name
  end
end
