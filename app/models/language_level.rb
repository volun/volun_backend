class LanguageLevel < ActiveRecord::Base
  include Archivable

  ######## RELACIONES ########
  has_many :known_languages, :class_name => 'Volun::KnownLanguage'
  has_many :volunteers, :through => :known_languages

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end
end
