class Channel < ActiveRecord::Base
    include Archivable  

    validates :name, presence: true, uniqueness: true
    validates :code, presence: true, uniqueness: true

  
    def to_s
      name
    end
  
    def self.main_columns
      [:id, :name, :code, :active]
    end
  
    def self.tooltip
        aux = ''
        [:name,:id, :code].each do |field|
            aux = Common.get_message_tooltip(aux, Channel,field)
        end
        aux
    end
end
  