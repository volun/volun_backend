class Trait < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common
  
  ######## RELACIONES ########
  has_many :assessments, :class_name => 'Volun::Assessment'
  has_many :volunteers, :through => :assessments

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Trait,field)     
    end
    aux
  end

end
