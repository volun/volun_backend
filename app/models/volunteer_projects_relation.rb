class VolunteerProjectsRelation < ActiveRecord::Base
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
    belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true


    def self.main_columns
        %i(project_id volunteer_id start_date end_date type_relation)
    end
    
end
