class Rt::Other < ActiveRecord::Base
  extend RtOtherHelper
  include RtModel

  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true

 ######## MÉTODOS ########
  def self.main_columns
    %i(
      description
      notes
      entity_phone_number
      entity_email
      created_at
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end

  private 
  
  
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_start_date',
      'search_end_date',
      'enter_text',
      'search_pending',
      'search_processing',
      'search_notes',
      'search_description',
      'search_approved',
      'search_rejected',
      'search_entity']
    ParamsFilter.get_filter(params, keys, 'rt_other_filter.')
  end

  def self.tooltip
    aux = ''
    [:description,:notes].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::Other,field)      
    end
    aux
  end
end
