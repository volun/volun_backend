class Rt::VolunteerProjectSubscribe < ActiveRecord::Base
  extend RtVolunteerProjectSubscribeHelper
  include RtModel
  ######## VALIDACIONES ########
  #  validates :name, :last_name, :phone_number_alt, :email, presence: true
  belongs_to :info_source, class_name: "InfoSource", foreign_key: "info_source_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true

  ######## MÉTODOS ########
  def self.main_columns
    %i(
      project
      name
      last_name
      last_name_alt
      phone_number_specific
      email
      district_name
      created_at
      volunteer_subscribe_date
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end

  def self.ransack_default
    {s: 'updated_at desc'}
  end

  #==============================================
  # Métodos de extracción de datos de PROYECTO
  #==============================================
  def volunteer_proj_subscribe
    self.project.try(:name)
  end

  #==============================================
  # Métodos de extracción de datos de VOLUNTARIO
  #==============================================
  def volunteer_subscribe_date
    request_form.user.created_at.strftime('%d/%m/%Y %H:%M') unless request_form.try(:user).blank?
  end

  def volunteer_attributes
    {
      name:          name,
      last_name:     last_name,
      last_name_alt: last_name_alt,
      phone_number_specific:  phone_number_specific,
      email:         email,
      district:      district,
      created_at:    created_at
    }
  end

  def phone_number_specific
    return self.phone_number if self.phone_number.to_s.gsub(' ','').match(/[6|7]\d{8}/)
    return self.phone_number_alt if self.phone_number_alt.to_s.gsub(' ','').match(/[6|7]\d{8}/)
    ''
  end

  def full_name
    "#{name} #{last_name} #{last_name_alt}"
  end

  alias_method :to_s, :full_name
  

  def self.tooltip
    aux = ''
    [:name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::VolunteerProjectSubscribe,field)      
    end
    aux
  end
end
