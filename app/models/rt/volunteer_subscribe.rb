class Rt::VolunteerSubscribe < ActiveRecord::Base
  extend RtVolunteerSubscribeHelper
  include RtModel

  ######## RELACIONES ########
  belongs_to :info_source, class_name: "InfoSource", foreign_key: "info_source_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :channel, class_name: "Channel", foreign_key: "channel_id", optional: true
 
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true
  validates :last_name, presence: true
  validates :phone_number_alt, format: { with: /[6|7]\d{8}/ }, allow_blank: true
  validates :phone_number, format: { with: /[8|9]\d{8}/ }, allow_blank: true
  validates :email, format: { with: VALID_EMAIL_REGEX }, allow_blank: true #, uniqueness: { scope: :project_id, case_sensitive: false }
  # validates :email, uniqueness: { case_sensitive: false }, unless: ->{email.blank?}
  # validates :phone_number_alt, uniqueness: true, unless: ->{phone_number_alt.blank?}
  
  ######## METODOS ########
  def self.main_columns
    %i(
      name
      last_name
      last_name_alt
      phone_number_specific
      email
      district
      postal_code
      created_at
      info_source
      volunteer_subscribe_date
      days_passed
      channel
      get_manager
      get_date_status
      get_status
    )
  end

  def volunteer_subscribe_date
    request_form.user.created_at.strftime('%d/%m/%Y %H:%M') if !request_form.try(:user).blank? && request_form.try(:user).try(:loggable_type) == "Volunteer"
  end

  def self.ransack_default
    {s: 'updated_at desc'}
  end

  def volunteer_attributes
    {
      name:          name,
      last_name:     last_name,
      last_name_alt: last_name_alt,
      phone_number_specific:  phone_number_specific,
      email:         email,
      district:      district,
      postal_code:   postal_code,
      created_at:    created_at,
      info_source_id: info_source
    }
  end

  def phone_number_specific
    return self.phone_number if self.phone_number.to_s.gsub(' ','').match(/[6|7]\d{8}/)
    return self.phone_number_alt if self.phone_number_alt.to_s.gsub(' ','').match(/[6|7]\d{8}/)
    ''
  end

  def full_name
    "#{name} #{last_name} #{last_name_alt}"
  end

  alias_method :to_s, :full_name

  def self.tooltip
    aux = ''
    [:name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::VolunteerSubscribe,field)      
    end
    aux
  end

end
