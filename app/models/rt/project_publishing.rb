class Rt::ProjectPublishing < ActiveRecord::Base
  extend RtProjectPublishingHelper
  include RtModel
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id",optional: true
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true
 ######## MÉTODOS ########
  def self.main_columns
    %i(
      entity
      notes
      description
      address
      entity_phone_number
      entity_email
      created_at
      publish_date_project
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end
  
  #==============================================
  # Métodos de extracción de datos de ACTIVIDAD
  #==============================================
  def activity_publishing_attributes
    {
      notes:        notes,
      description:  description,
      created_at:   created_at
    }
  end  

  def publish_date_project
    project.created_at.strftime('%d/%m/%Y %H:%M') 
  rescue
    ""
  end

  def self.tooltip
    aux = ''
    [:description,:notes,:road_name,:road_number,:town,:road_type,:number_type,:posta_code,:province].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::ProjectPublishing,field)      
    end
    aux
  end

  private 

  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_start_date',
      'search_end_date',
      'enter_text',
      'search_pending',
      'search_processing',
      'search_notes',
      'search_description',
      'search_approved',
      'search_rejected',
      'search_entity']
    ParamsFilter.get_filter(params, keys, 'rt_proj_publish_filter.')
  end
  

end 
