class Rt::ActivityPublishing < ActiveRecord::Base
  extend RtActivityPublishingHelper
  include RtModel
  ######## RELACIONES ########
  
  belongs_to :road_type, class_name: "RoadType", foreign_key: "road_type_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true
  belongs_to :activity, class_name: "Activity", foreign_key: "activity_id", optional: true
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true
  ######## VALIDACIONES ########

  ######## MÉTODOS ########
  def self.main_columns
    %i(
      entity
      entity_phone_number
      entity_email
      name
      description
      organizer
      created_at
      publishing_date_activity  
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end

  def to_s
    name
  end

  #==============================================
  # Métodos de extracción de datos de ACTIVIDAD
  #==============================================

  def activity_publishing_attributes
    {
      name:         name,
      description:  description,
      organizer:    organizer,
      created_at:   created_at
    }
  end

  def publishing_date_activity
    activity.created_at.strftime('%d/%m/%Y %H:%M') 
  rescue
    ""
  end
   
  def self.tooltip
    aux = ''
    [:name,:description,:places_text_fee].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::ActivityPublishing,field)      
    end
    aux
  end

  private 
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_start_date',
      'search_end_date',
      'enter_text',
      'search_pending',
      'search_processing',
      'search_approved',
      'search_rejected',
      'search_entity']
    ParamsFilter.get_filter(params, keys, 'rt_activity_filter.')
  end
end
