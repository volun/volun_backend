class Rt::VolunteerAmendment < ActiveRecord::Base
  extend RtVolunteerAmendmentHelper
  include RtModel

 ######## RELACIONES ########
  belongs_to :volunteer, foreign_key: 'volunteer_id', class_name: "Volunteer", optional: true
  belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
  
  
 ######## MÉTODOS ########
  def self.main_columns
    %i(
      id_autonum
      name
      last_name
      last_name_alt
      phone_number
      email
      district
      postal_code
      created_at
      volunteer_subscribe_date
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end

  #==============================================
  # Métodos de extracción de datos de VOLUNTARIO
  #==============================================
  def volunteer_subscribe_date
    volunteer.created_at.strftime('%d/%m/%Y %H:%M') unless volunteer.blank?
  end

  def volunteer_id
    volunteer.try(:id_autonum) || volunteer.try(:id)
  end

  def volunteer_name
    volunteer.try(:name)
  end

  def volunteer_last_name
    volunteer.try(:last_name)
  end

  def volunteer_last_name_alt
    volunteer.try(:last_name_alt)
  end

  def volunteer_attributes
    {
      phone_number:  phone_number,
      email:         email,
      district:      district,
      postal_code:   postal_code,
      created_at:    created_at
    }
  end

  def self.tooltip
    aux = ''
    [:name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code,:district].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::VolunteerAmendment,field)      
    end
    aux
  end
end
