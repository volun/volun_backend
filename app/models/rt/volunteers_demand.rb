class Rt::VolunteersDemand < ActiveRecord::Base
  include RtModel
  extend RtVolunteerDemandHelper

  belongs_to :province, class_name: "Province", foreign_key: "province_id",optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id",optional: true
  belongs_to :entity, class_name: "Entity", foreign_key: "entity_id", optional: true
  
  ######## MÉTODOS ########
  def self.main_columns 
    %i( 
      entity
      entity_phone_number
      entity_email
      notes 
      description 
      execution_start_date 
      execution_end_date 
      requested_volunteers_num 
      created_at 
      days_passed
      get_manager
      get_date_status
      get_status
    ) 
  end

  def self.tooltip
    aux = ''
    [:notes,:description].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::VolunteersDemand,field)      
    end
    aux
  end

  private 

  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_start_date',
      'search_end_date',
      'enter_text',
      'search_pending',
      'search_processing',
      'search_notes',
      'search_description',
      'search_approved',
      'search_rejected',
      'search_entity']
    ParamsFilter.get_filter(params, keys, 'rt_demand_filter.')
  end
end
