class Rt::VolunteerAppointment < ActiveRecord::Base
  extend RtVolunteerAppointmentHelper
  include RtModel

 ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true

 ######## MÉTODOS ########
  def self.main_columns
    %i(
      name
      last_name
      last_name_alt
      phone_number
      email
      district
      postal_code
      created_at
      volunteer_subscribe_date
      days_passed
      get_manager
      get_date_status
      get_status
    )
  end

  #==============================================
  # Métodos de extracción de datos de VOLUNTARIO
  #==============================================
  def volunteer_name
    volunteer.try(:name)
  end

  def volunteer_last_name
    volunteer.try(:last_name)
  end

  def volunteer_last_name_alt
    volunteer.try(:last_name_alt)
  end

  def volunteer_phone_number
    volunteer.try(:phone_number)
  end

  def volunteer_email
    volunteer.try(:email)
  end

  def volunteer_address_district
    volunteer.try(:address).try(:district).try(:name)
  end

  def volunteer_address_postal_code
    volunteer.try(:address).try(:postal_code)
  end


  def volunteer_subscribe_date
    volunteer.created_at.strftime('%d/%m/%Y %H:%M') unless volunteer.blank?
  end

  def volunteer_attributes
    {created_at:    created_at}
  end

  def self.tooltip
    aux = ''
    [:name,:last_name,:last_name_alt,:phone_number,:phone_number_alt,:email,:postal_code,:district].each do |field|
      aux = Common.get_message_tooltip(aux, Rt::VolunteerAppointment,field)      
    end
    aux
  end
 
end
