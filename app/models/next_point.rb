class NextPoint < ActiveRecord::Base
  include Recordable
  include Archivable
  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def self.main_columns
    [:id, :name, :description, :active]
  end

  def to_s
    name
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, NextPoint,field)     
    end
    aux
  end
end
