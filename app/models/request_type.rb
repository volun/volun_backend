class RequestType < ActiveRecord::Base
  enum kind: {
    rt_volunteer_subscribe:   1,    
    rt_volunteer_amendment:   3,
    rt_volunteer_appointment: 4,
    rt_volunteers_demand:     7,
    rt_project_publishing:    8,
    rt_activity_publishing:   10,
    rt_other:                 12,
    rt_volunteer_project_subscribe: 14
  }
  ######## VALIDACIONE ########
  validates :kind, presence: true

  ######## MÉTODOS ########
  def extendable?
    kind.classify.sub(/\ARt/, 'Rt::').safe_constantize.present?
  end

  def to_s
    kind_i18n
  end
end
