class Address < ActiveRecord::Base
  include BdcCompatible
  ######## RELACIONES ########
  has_many :entities, class_name: "Entity", foreign_key: "address_id"
  has_many :volunteers, class_name: "Volunteer", foreign_key: "address_id"
  has_many :citizens, class_name: "Citizen", foreign_key: "address_id"
  
  belongs_to :nationality, class_name: "Nationality", foreign_key: "nationality_id", optional: true
  belongs_to :province, class_name: "Province", foreign_key: "province_id", optional: true
  belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true
  belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
  ######## VALIDACIONES ########
  validates :road_type, :road_name, :road_number, :postal_code, :province, :country, :town, presence: true
  validates :postal_code, format: { with: /\d{5}/ }

  ## Clonado usando Amoeba ##
  amoeba do
    enable
    nullify :id
    exclude_association :entities
    exclude_association :volunteers
    exclude_association :citizens
  end

  ######## MÉTODOS ########
  def to_s
    "#{road_type}  #{road_name}, #{road_number}, #{postal_code}, #{district.try(:name)}, #{borought.try(:name)}, #{town}"
  end


  def self.district_list
    District.where(active: true)
  end

  def self.borougth_list(district)
    Borought.joins(:district).where("boroughts.active= true").where("cast(districts.code as int)=cast(? as int)", district)    
  end
end

