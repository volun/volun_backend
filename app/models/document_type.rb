class DocumentType < ActiveRecord::Base
    OTHER_TYPE_REGEX = /(?:other|otro)s?/i
    CIF_TYPE_REGEX = /(?:vat|cif)s?/i
    include Archivable

    validates :name, presence: true, uniqueness: true
    validates :code, presence: true
    validate  :check_immutability
    before_destroy :prevent_destroying_specific_types
    
    has_many :citizens
    has_many :volunteers

    ######## SCOPES ########
    scope :for_volunteers, ->{ where("upper(name) not in ('CIF', 'TRT', 'TRP', 'TRC')") }
    scope :for_citizens, ->{ where("upper(name) not in ('CIF', 'OTROS')") }

    ######## MÉTODOS ########
    def to_s
        name
    end

    def is_other_type?
        OTHER_TYPE_REGEX === to_s
    end

    def is_cif_type?
        CIF_TYPE_REGEX === to_s
    end

    private
    
    def prevent_destroying_specific_types
        return true unless is_other_type? || is_cif_type?

        errors.add :name, :cannot_be_deleted
        false
    end

    def check_immutability
        if OTHER_TYPE_REGEX === name_was && !(OTHER_TYPE_REGEX === name) ||
            CIF_TYPE_REGEX === name_was && !(CIF_TYPE_REGEX === name)
        errors.add :name, :cannot_be_modified
        end
    end

end