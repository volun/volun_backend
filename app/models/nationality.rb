class Nationality < ActiveRecord::Base
  require 'csv'
  include Recordable
  include Archivable
  include Constants
  include Common
  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true
  
  ######## MÉTODOS ########
  
  ###FICHERO PERÍODICO DE NACIONALIDADES###
  def self.to_csv_nationalities
    Constants.arr_formats.each do |f|
      nationalities=Nationality.where("nationalities.id IN (?)",Volunteer.all.select(:nationality_id).distinct).distinct.order(:name)
      CSV.open("#{Setting.find_by(key: "RutaNasDatosAbiertos").value}fichero_nacionalidades_#{DateTime.current().strftime("%Y%m%d_%H%M%S")}.#{f}", "wb",col_sep:';', encoding:'ISO-8859-1') do |csv|
        csv <<  [human_attribute_name(:name)]
        nationalities.each do |nationality|
          csv << [nationality.name]
        end
      end
    end
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Nationality,field)     
    end
    aux
  end

  def to_s
    name
  end
end