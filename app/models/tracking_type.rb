class TrackingType < ActiveRecord::Base
  include Common

  AUTOMATIC_TRACKINGS = {
    subscribe:   'subscribe',
    unsubscribe: 'unsubscribe',
    amendment: 'amendment', 
    appointment: 'appointment',
    demand: 'demand',
    send_mail: 'send_mail',
    generate_pass: 'generate_pass',
    confirm_derivaty: 'confirm_derivaty',
    not_confirm_derivaty: 'not_confirm_derivaty',
    asist_derivaty: 'asist_derivaty',
    not_asist_derivaty: 'not_asist_derivaty',
    derivatie: 'derivatie',
    down_derivatie: 'down_derivatie'
  }

  AUTOMATIC_NAMES = {
    demand: "Demanda",
    appointment: "Cita",
    send_mail: "Envio",
    amendment: "Modificación",
    generate_pass: "Generación",
    not_asist_derivaty: "No",
    asist_derivaty: "Asiste",
    not_confirm_derivaty: "No",
    confirm_derivaty: "Confirma",
    derivatie: "Derivación",
    down_derivatie: "Desderivación"
  }


  SQL_GENERATED = {
    demand: "('Demanda de voluntario','Entity','demand','true','true','#{Date.today}','#{Date.today}')",
    appointment: "('Cita de voluntario','Volunteer','appointment','true','true','#{Date.today}','#{Date.today}')",
    send_mail: "('Envio de correo','Volunteer','send_mail','true','true','#{Date.today}','#{Date.today}')",
    amendment: "('Modificación de voluntario','Volunteer','amendment','true','true','#{Date.today}','#{Date.today}')",
    generate_pass: "('Generación contraseña','Volunteer','generate_pass','true','true','#{Date.today}','#{Date.today}')",
    not_asist_derivaty: "('No asiste a la reunión','Volunteer','not_asist_derivaty','true','true','#{Date.today}','#{Date.today}')",
    asist_derivaty: "('Asiste a la reunión','Volunteer','asist_derivaty','true','true','#{Date.today}','#{Date.today}')",
    not_confirm_derivaty: "('No confirma la asistencia','Volunteer','not_confirm_derivaty','true','true','#{Date.today}','#{Date.today}')",
    confirm_derivaty: "('Confirma la asistencia','Volunteer','confirm_derivaty','true','true','#{Date.today}','#{Date.today}')",
    derivatie: "('Derivación','Volunteer','derivatie','true','true','#{Date.today}','#{Date.today}')",
    down_derivatie: "('Desderivación','Volunteer','down_derivatie','true','true','#{Date.today}','#{Date.today}')"
  }

  include Recordable
  include ArchivableTrackingType 
  ######## RELACIONES ########
  has_many :trackings, class_name: 'Volun::Tracking'
  has_many :citizen_tracking,class_name: "CitizenTracking",foreign_key: "tracking_type_id"
  has_many :citizen_service_tracking,class_name: "CitizenServiceTracking",foreign_key: "tracking_type_id"

  ######## VALIDATES ########
  validates :name, presence: true, uniqueness: true
  validates :alias_name, uniqueness: true, if: ->{alias_name.present?}
  validate  :check_immutability, on: :update

  ######## SCOPES ########
  scope :all_active,   ->(){ where(active: true) } 
  scope :all_volunteers_with_system, ->(){all_active.where("(tracking_types_type='Volunteer' OR tracking_types_type='' OR tracking_types_type IS NULL)")}
  scope :all_volunteers, ->(){all_active.where("system=false AND (tracking_types_type='Volunteer' OR tracking_types_type='' OR tracking_types_type IS NULL)")}
  scope :all_entities, ->(){all_active.where("system=false AND (tracking_types_type='Entity' OR tracking_types_type='' OR tracking_types_type IS NULL)")}
  scope :all_projects, ->(){all_active.where("system=false AND (tracking_types_type='Project' OR tracking_types_type='' OR tracking_types_type IS NULL)")}
  scope :all_citizens, ->(){all_active.where("system=false AND (tracking_types_type='Citizen' OR tracking_types_type='' OR tracking_types_type IS NULL)")}
  scope :all_citizens_service, ->(){all_active.where("system=false AND (tracking_types_type='CitizenService' OR tracking_types_type='' OR tracking_types_type IS NULL)")}


  ######## MÉTODOS ########
  def self.get_derivatie
    self.get_or_generate_tracking(:derivatie)
  end

  def self.get_down_derivatie
    self.get_or_generate_tracking(:down_derivatie)
  end

  def self.get_confirm_derivaty
    self.get_or_generate_tracking(:confirm_derivaty)
  end

  def self.get_not_confirm_derivaty
    self.get_or_generate_tracking(:not_confirm_derivaty)
  end

  def self.get_asist_derivaty
    self.get_or_generate_tracking(:asist_derivaty)
  end

  def self.get_not_asist_derivaty
    self.get_or_generate_tracking(:not_asist_derivaty)
  end

  def self.get_volunteer_generate_password
    self.get_or_generate_tracking(:generate_pass)
  end

  def self.get_volunteer_amendment
    self.get_or_generate_tracking(:amendment)
  end

  def self.get_volunteer_send_mail
    self.get_or_generate_tracking(:send_mail)
  end

  def self.get_volunteer_appointment
    self.get_or_generate_tracking(:appointment)
  end

  def self.get_volunteers_demand
    self.get_or_generate_tracking(:demand)
  end

  def self.get_volunteer_subscribe
    where(alias_name: AUTOMATIC_TRACKINGS[:subscribe]).take!
  end

  def self.get_project_subscribe
    where(alias_name: AUTOMATIC_TRACKINGS[:subscribe]).take!
  end

  def self.get_project_unsubscribe
    where(alias_name: AUTOMATIC_TRACKINGS[:unsubscribe]).take!
  end
  
  def self.get_volunteer_unsubscribe
    where(alias_name: AUTOMATIC_TRACKINGS[:unsubscribe]).take!
  end

  def self.tooltip
    aux = ''
    [:id,:name,:alias_name].each do |field|
      aux = Common.get_message_tooltip(aux, TrackingType,field)     
    end    
    aux
  end

  def to_s
    name
  end

  private
  def self.get_or_generate_tracking(tracking)
    exists=TrackingType.find_by(alias_name: tracking.to_s)
    if exists.blank? 
      exists=TrackingType.find_by(name: AUTOMATIC_NAMES[tracking]) 
      if exists.blank?
        ActiveRecord::Base.connection.execute("Insert into tracking_types (name,tracking_types_type,alias_name,active,system,updated_at,created_at) VALUES #{SQL_GENERATED[tracking]}")
      else
        exists.alias_name = tracking.to_s unless tracking.blank?
        exists.save
      end
    end
    TrackingType.find_by(alias_name: tracking.to_s)
  end

  def check_immutability
    errors.add :system, :cannot_be_modified if system_was && system_changed?
    errors.add :alias_name, :cannot_be_modified if system_was && alias_name_changed?
  end
end
