class Document < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :project, required: true, class_name: "Project", foreign_key: "project_id"

  ######## MÉTODOS ########
  def to_s
    name
  end
end
