class WsMessage < ActiveRecord::Base
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id"
  belongs_to :citizen, class_name: "Citizen", foreign_key: "citizen_id"

  has_attached_file :attach,
    styles: lambda{ |a|
      return {} 
    },
    url: '/system/:class/:id/:attachment/:style/:filename',
    validate_media_type: false
  
  do_not_validate_attachment_file_type  :attach

  def to_s
    body
  end  
end
