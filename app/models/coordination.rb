class Coordination < ActiveRecord::Base
  include Recordable
  include Archivable
  include Common
  ######## RELACIONES ########
  has_and_belongs_to_many :projects, class_name: "Project", foreign_key: "coordination_id"

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, presence: true

  ######## MÉTODOS ########
  def self.main_columns
    [:id, :name, :active]
  end

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Coordination,field)     
    end
    aux
  end

  def to_s
    name
  end
end
