class ServiceQuestion < ActiveRecord::Base
    include Archivable
    include Constants

    has_many :service_responses, class_name: "ServiceResponse", foreign_key: "service_question_id"

    def to_s
        description
    end

    def total_responses
        return 0 if self.service_responses.blank?
        self.service_responses.count
    end

    def total_responses_true
        return 0 if self.service_responses.blank? || self.question_type.to_s != "boolean"
        self.service_responses.where(response: "true").count
    end

    def total_responses_false
        return 0 if self.service_responses.blank? || self.question_type.to_s != "boolean"
        self.service_responses.where(response: "false").count
    end

    def total_responses_other
        return 0 if self.service_responses.blank? || self.question_type.to_s != "string"
        self.service_responses.where("response IS NOT NULL OR TRIM(response) != ''").count
    end

    def self.tooltip
        aux = ''
        [:id,:description].each do |field|
          aux = Common.get_message_tooltip(aux, ServiceQuestion,field)     
        end
        aux
      end
end