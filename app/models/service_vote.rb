class ServiceVote < ActiveRecord::Base
    belongs_to :citizen_service, class_name: "CitizenService", foreign_key: "citizen_service_id", optional: true
end