class Pro::Tracking < ActiveRecord::Base
  extend ProTrackingsHelper
  ######## RELACIONES ########
  belongs_to :project, class_name: "Project", foreign_key: "project_id"
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true
  belongs_to :manager, class_name: "Manager", foreign_key: "manager_id", optional: true
  belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id"

  ######## VALIDACIONES ########
  validates :tracked_at, presence: true

  ######## MÉTODOS ########
  def self.main_columns
    %i(project manager tracked_at automatic comments request_form)
  end
  
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'search_enter_text',
      'search_inicio_f',
      'search_fin_f',
      'search_manager']
    ParamsFilter.get_filter(params, keys, 'pro_tracking_filter.')
  end

  def self.tooltip
    aux = ''
    [:comments,:project,:manager].each do |field|
      aux = Common.get_message_tooltip(aux, Pro::Tracking,field)     
    end    
    aux
  end
end
