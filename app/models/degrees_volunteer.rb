class DegreesVolunteer < ActiveRecord::Base
  ######## RELACIONES ########
  belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
  belongs_to :degree, class_name: "Degree", foreign_key: "degree_id", optional: true
end
