class Digital::Follow < ActiveRecord::Base
  self.table_name = "digital_follows"
  
  ######## RELACIONES ########
  belongs_to :userable, polymorphic: true, optional: true
  belongs_to :digital_topic, class_name: "Digital::Topic", foreign_key: "digital_topic_id"

  def self.get_userables
    Volunteer.all.where(active: true).order(:name) 
  end

end
