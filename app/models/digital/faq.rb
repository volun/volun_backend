class Digital::Faq < ActiveRecord::Base  
  include Recordable
  include Archivable
  include Common
  extend ::Digital::FaqHelper
  self.table_name = "digital_faqs"
  
  ######## RELACIONES ########
  belongs_to :project, class_name: "Project", foreign_key: "project_id"

  ######## VALIDACIONES ########
  validates :title,:description,:order, presence: true
  validates :title, uniqueness: true
  validates :order, uniqueness: true


  ######## MÉTODOS ########
  def self.main_columns
    [:id, :order, :title,:description, :active]
  end

  def self.tooltip
    aux = ''
    [:id,:title,:description].each do |field|
      aux = Common.get_message_tooltip(aux, Digital::Faq,field)      
    end
    aux
  end

  def to_s
    title
  end

end
