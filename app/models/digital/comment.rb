class Digital::Comment < ActiveRecord::Base
  include Archivable
  include Recordable
  self.table_name = "digital_comments"
  
  ######## RELACIONES ########
  belongs_to :moderable, polymorphic: true, optional: true
  belongs_to :userable, polymorphic: true, optional: true
  belongs_to :digital_comment, class_name: "Digital::Comment", foreign_key: "digital_comment_id",  optional: true
  belongs_to :digital_topic, class_name: "Digital::Topic", foreign_key: "digital_topic_id", optional: true
  belongs_to :moderate_status, class_name: "ModerateStatus", foreign_key: "moderate_status_id", optional: true

  ######## VALIDACIONES ########
  validates :body, presence: true

  def type
    I18n.t("digital_manage.moderate.type_comment")
  end

  def project_text
    "#{self.digital_topic.project.id}. #{self.digital_topic.project}"
  end

  def moderate_status_text
    self.moderate_status.try(:title).blank? ? ModerateStatus.find_by(code: "pending").try(:title) : self.moderate_status.try(:title)
  rescue
    ModerateStatus.find_by(code: "pending").try(:title)
  end

  def moderate_user
    self.moderable.loggable
  rescue
    ""
  end

end
