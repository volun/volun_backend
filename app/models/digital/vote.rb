class Digital::Vote < ActiveRecord::Base
  self.table_name = "digital_votes"
  
  ######## RELACIONES ########
  belongs_to :votable, polymorphic: true, optional: true
  belongs_to :userable, polymorphic: true, optional: true

  ######## VALIDACIONES ########
  validates :value, presence: true
  validate :check_vote
  validate :check_value

  private 

  def check_vote
    exist = Digital::Vote.find_by(votable: votable,userable: userable)
    if !exist.blank? && (id.blank? || exist.id != id)
      add_error(:value,"Ya se ha realizado el voto")
    end
  end

  def check_value
    if ![-1,0,1].include?(value.to_i)
      add_error(:value, "El valor de voto está fuera de rango, solo se permiten votos positivos, negativos o neutros")
    end
  rescue
    add_error(:value, "El valor de voto no tiene el formato correcto")
  end

end
