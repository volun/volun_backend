class Digital::Topic < ActiveRecord::Base
  include Archivable
  include Recordable
  include Common
  extend ::Digital::TopicHelper
  self.table_name = "digital_topics"
  
  ######## RELACIONES ########
  belongs_to :moderable, polymorphic: true, optional: true
  belongs_to :userable, polymorphic: true, optional: true
  belongs_to :moderate_status, class_name: "ModerateStatus", foreign_key: "moderate_status_id", optional: true
  belongs_to :project, class_name: "Project", foreign_key: "project_id"

  has_many :digital_comments, class_name: "Digital::Comment", foreign_key: "digital_topic_id"
  has_many :digital_follows, class_name: "Digital::Follow", foreign_key: "digital_topic_id"
  has_many :digital_votes, as: :votable, class_name: "Digital::Vote"

  accepts_nested_attributes_for :digital_comments, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :digital_follows, reject_if: :all_blank, allow_destroy: true

  validates_associated :digital_comments
  validates_associated :digital_follows

  ######## VALIDACIONES ########
  validates :title,:body, presence: true
  validates :title, uniqueness: true

   ######## MÉTODOS ########
   def self.main_columns
    [:id, :title,:body, :n_comments, :n_follows, :votes, :active]
  end

  def self.tooltip
    aux = ''
    [:id,:title,:body].each do |field|
      aux = Common.get_message_tooltip(aux, Digital::Topic,field)      
    end
    aux
  end

  def to_s
    title
  end

  def type
    I18n.t("digital_manage.moderate.type_topic")
  end

  def n_comments
    self.digital_comments.count
  rescue
    0
  end

  def n_follows
    self.digital_follows.count
  rescue
    0
  end

  def votes
    self.digital_votes.sum(:value)
  rescue
    0
  end

  def project_text
    "#{self.project.id}. #{self.project}"
  end

  def moderate_status_text
    self.moderate_status.try(:title).blank? ? ModerateStatus.find_by(code: "pending").try(:title) : self.moderate_status.try(:title)
  rescue
    ModerateStatus.find_by(code: "pending").try(:title)
  end

  def moderate_user
    self.moderable.loggable
  rescue
    ""
  end

end
