module Digital::Manage
  extend ::Digital::ModerateHelper
  ######## MÉTODOS ########
  def self.table_name_prefix
    'digital_'
  end

  def self.main_columns 
    [:id,:project_text, :type, :title, :body, :moderate_status_text, :moderate_user]
  end

  def self.normal_columns
    [:id,:project_text,:type, :title, :body, :moderate_status_text,:moderate_user]
  end

  def self.human_attribute_name(attribute, options = {}) 
    I18n.translate("digital_manage.moderate.fields.#{attribute}")
  end
end
