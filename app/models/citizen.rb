class Citizen < ActiveRecord::Base  
    include Archivable 
    extend CitizensHelper
    belongs_to :address, class_name: "Address", foreign_key: "address_id", optional: true
    belongs_to :gender, optional: true
    belongs_to :province, optional: true
    belongs_to :nationality, optional: true
    belongs_to :document_type, optional: true
    
    has_many :citizen_services, class_name: "CitizenService", foreign_key: "citizen_id"
    has_many :trackings, class_name: "CitizenTracking", foreign_key: "citizen_id"
    has_one  :user, class_name: "User", foreign_key: 'loggable_id', as: :loggable 

    has_one  :logo,   -> { citizen_logo   }, class_name: 'Link', foreign_key: 'linkable_id', required: false, validate: false
    has_many :links, as: :linkable

    accepts_nested_attributes_for :user, allow_destroy: true
    accepts_nested_attributes_for :address, allow_destroy: true
    accepts_nested_attributes_for :citizen_services, allow_destroy: true
    accepts_nested_attributes_for :trackings, allow_destroy: true
    accepts_nested_attributes_for :logo, reject_if:  :all_blank, allow_destroy: true

    scope :not_market, ->() { where(market: false)}
    scope :not_tester, ->() { not_market.where(tester: false) }
    scope :tester, ->() { not_market.where(tester: true) }
    scope :all_active,   ->(){ not_market.where(active: true) }
    scope :for_ids_with_order, ->(ids) {
        # order = sanitize_sql_array(
        # # Esto no es compatible en rails 6
        #   ["position(id::text in ?)", ids.join(',')]
        # )
        # not_market.where(:id => ids).order(order)
        
        # SOLUCIÓN 1 
        not_market.where(:id => ids).order(id: :asc)
    
      }
    
    validates :name, presence: true

    def self.main_columns
        %i(id
           name
           last_name_1
           last_name_2
           age_name
           type_services
           count_services
           count_services_finish
           count_services_pending
           count_services_accept
           average_vote
        )
    end

    def self.get_filter(params)
        keys = [
          'search_id',
          'enter_text',
          'search_nombre',
          'search_apellido1',
          'search_apellido2',
          'search_services_start',
          'search_services_end',
          'search_services_f_start',
          'search_services_f_end',
          'search_services_a_start',
          'search_services_a_end',
          'search_services_p_start',
          'search_services_p_end',
          'search_age_start',
          'search_age_end',
          'search_service_type',
          'search_gender'
         ]
        ParamsFilter.get_filter(params, keys, 'citizen_filter.')
    end   

    def full_name
        "#{name} #{last_name_1} #{last_name_2}".strip
    rescue
        "#{name} #{last_name_1} #{last_name_2}"
    end

    def age
        return '' if self.birthday.blank?
        now = Time.zone.now.to_date
        date_bithday = Time.zone.parse(self.birthday.to_s)
        (now.year - date_bithday.year - ((now.month > date_bithday.month || (now.month == date_bithday.month && now.day >= date_bithday.day)) ? 0 : 1)).to_s
    end

    def type_services
        return '' if self.citizen_services.blank?
        aux = ''
        

        citizen_services.map(&:type_service).uniq.sort.each do |cs|
            if aux.blank?
                aux = aux + (cs.blank? ? '' : cs)
            else
                aux = aux + '<br>' + (cs.blank? ? '' : cs)
            end
        end
        aux.html_safe
    end

    def count_services
        return 0 if self.citizen_services.blank?
        self.citizen_services.all_active.count
    end
    
    def count_services_finish
        return 0 if self.citizen_services.blank?
        self.citizen_services.all_active.status(Service.status_type_list[4].to_s).count
    end

    def count_services_pending
        return 0 if self.citizen_services.blank?
        self.citizen_services.all_active.status(Service.status_type_list[0].to_s).count + self.citizen_services.all_active.status(Service.status_type_list[3].to_s).count
    end

    def count_services_accept
        return 0 if self.citizen_services.blank?
        self.citizen_services.status(Service.status_type_list[1].to_s).count
    end

    def document_type_ws
        case document_type.name.try(:upcase)
        when "DNI"
            1
        when "PASAPORTE"
            2
        when "TRT"
            3
        when "TRP"
            4
        when "TRC"
            5
        when "NIE"
            6
        end
    end

    def full_address
       return '' if self.address.blank?

        "#{address.road_type}/#{ address.road_name}, #{address.road_number_type} - #{ address.road_number}, #{address.floor} - #{address.door} <br> #{address.borought.try(:name)} - #{address.district.try(:name)}<br> #{address.town} (#{address.postal_code}), #{address.province.try(:name)}".html_safe
    end

    def email_activate
        self.email_confirmed = true
        self.confirm_token = nil
        save!(:validate => false)
    end

    def to_s
        self.full_name
    end

    def self.search_citizen(search)
        if search
            Citizen.where("translate(UPPER(name), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(:search), 'ÁÉÍÓÚ', 'AEIOU') OR  translate(UPPER(last_name_1), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(:search), 'ÁÉÍÓÚ', 'AEIOU') OR translate(UPPER(last_name_2), 'ÁÉÍÓÚ', 'AEIOU') LIKE 
                translate(UPPER(:search), 'ÁÉÍÓÚ', 'AEIOU') OR translate(UPPER(document), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(:search), 'ÁÉÍÓÚ', 'AEIOU') 
                OR translate(UPPER(CONCAT(name, last_name_1, last_name_2)), 'ÁÉÍÓÚ', 'AEIOU') LIKE translate(UPPER(:search), 'ÁÉÍÓÚ', 'AEIOU') OR CAST(id AS text) LIKE :search", search: "%#{search}%")
        else
            Citizen.all
        end
    end

    def self.return_address_data(citizen_id, district = nil, borought = nil, address_data = nil)
        if !citizen_id.blank?
            district = Citizen.find(citizen_id).try(:address).try(:district) 
            borough = Citizen.find(citizen_id).try(:address).try(:borough) 
            address_data = {district: district, borough: borough}
        end
        address_data
    end

    def select_text
        "#{self.id} - #{self.name} #{self.last_name_1} #{self.last_name_2} - #{self.document}"
    end 

    def self.tooltip
        aux = ''
        [:id,:name,:last_name_1,:last_name_2,:email,:phone].each do |field|
          aux = Common.get_message_tooltip(aux, Citizen,field)     
        end
        aux
    end

    def update_average_vote
        cs = self.citizen_services.where.not(citizen_vote: nil)
        if !cs.blank?
            self.update(average_vote: cs.average("citizen_vote::integer").to_f.round(2))
        end
    end
end
