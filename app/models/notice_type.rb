class NoticeType < ActiveRecord::Base

  enum kind: [:email, :sms, :paper]
  ######## RELACIONES ########
  has_many :users, class_name: "User", foreign_key: "notice_type_id"

  ######## VALIDACIONES ########
  validates :kind, presence: true

  ######## MÉTODOS ########
  def to_s
    kind_i18n
  end
end
