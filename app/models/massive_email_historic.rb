class MassiveEmailHistoric < ActiveRecord::Base
    belongs_to :user, class_name: "User", foreign_key: "user_id", optional: true


    def self.main_columns
        %i(id manager_name log_short created_at)
    end

    def log_short 
        self.log.truncate(100, separator: ' ')
    rescue
        self.log
    end


    def manager_name
        user.try(:loggable).try {|x| x.to_s}
    end

    def self.send_emails
        massive_historic = MassiveEmailHistoric.new(user_id: 1, title: "worker")
        massive_historic.save
    end

end
