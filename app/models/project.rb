class Project < ActiveRecord::Base
  include ArchivableProject
  include Constants
  extend ProjectsHelper

  ######## RELACIONES ########
  belongs_to :pt_extendable, polymorphic: true, optional: true
  belongs_to :father, optional: true, class_name: "Project", foreign_key: "father_id"
  belongs_to :entity, -> { where(active: true).order('entities.name asc') }, required: true, class_name: "Entity", foreign_key: "entity_id"
  belongs_to :district,class_name: "District", foreign_key: "district_id", optional: true
  
  has_and_belongs_to_many :projects_volunteers, class_name: "ProjectsVolunteer"
  has_many :volunteer_projects_relation, class_name: "VolunteerProjectsRelation"
  has_and_belongs_to_many :volunteers, :through => :projects_volunteers
  has_and_belongs_to_many :areas, -> { where(active: true).order('areas.name asc') }, class_name: "Area"
  has_and_belongs_to_many :inactive_areas, -> { where(active: false).order('areas.name asc') }, class_name: 'Area'
  has_and_belongs_to_many :collectives, -> { where(active: true).order('collectives.name asc') }, class_name: "Collective"
  has_and_belongs_to_many :inactive_collectives, -> { where(active: false).order('collectives.name asc') }, class_name: 'Collective'
  has_and_belongs_to_many :coordinations, -> { where(active: true).order('coordinations.name asc') }, class_name: "Coordination"
  has_and_belongs_to_many :inactive_coordinations, -> { where(active: false).order('coordinations.name asc') }, class_name: 'Coordination'
  
  has_many :derivaties_volunteers, class_name: "DerivatiesVolunteer"
  has_many :documents, class_name: "Document"
  has_many :albums, class_name: "Album"
  has_many :activities, class_name: "Activity"
  has_many :subprojects, class_name: "Project", foreign_key: "father_id"
  has_many :events, as: :eventable
  has_many :addresses, through: :events
  has_many :trackings,         :class_name => 'Pro::Tracking'#, foreign_key: "project_id"
  has_many :volun_trackings,   :class_name => 'Volun::Tracking'
  has_many :volun_contacts,    :class_name => 'Volun::Contact'
  has_many :volun_assessments, :class_name => 'Volun::Assessments'
  has_many :links, as: :linkable
  has_many :images, -> { project_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :videos, -> { project_videos }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { project_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { project_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :entity_organizations, class_name: "EntityOrganization"
  has_many :meetings, class_name: "Meeting"

  has_one  :logo,   -> { project_logo   }, class_name: 'Link', foreign_key: 'linkable_id',  required: false, validate: false
  ######## ATRIBUTOS ANIDADOS ########
  accepts_nested_attributes_for :documents,  allow_destroy: true
  accepts_nested_attributes_for :albums,  allow_destroy: true
  accepts_nested_attributes_for :pt_extendable
  accepts_nested_attributes_for :events, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :images, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :videos, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :docs,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :urls,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :logo,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :entity_organizations, reject_if:  :all_blank, allow_destroy: true

  ######## VALIDACIONES ########
  validates :name, :description, :contact_name, :contact_last_name, :subtype_pt, :subtype_proyect,
            :phone_number, :email, :entity_id, presence: true
  validates :father,:father_id, presence: true, if: -> {self.subtype_proyect.to_s=="subproyect"}
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validate  :execution_start_date_less_than_execution_end_date
  validates :execution_start_date, date: {date: true, message: I18n.t('activerecord.errors.messages.date_format_error')},
                                   presence: true
  validates :execution_end_date, date: {date: true, message: I18n.t('activerecord.errors.messages.date_format_error')}, allow_blank: true
  validates :insurance_date, date: {before:      Proc.new { 150.years.since },
                                     message:     I18n.t('activerecord.errors.messages.invalid_proj_insurance_date') },
                             if: -> {insured?}
  validates :volunteers_num, :beneficiaries_num, numericality: { allow_blank: true }
  validate  :check_valid_start_date
  validate  :validate_file_size
  validates :name, uniqueness: {scope: [:year]}

  validates_associated :documents
  validates_associated :albums
  validates_associated :activities
  validates_associated :events
  validates_associated :addresses
  validates_associated :links
  validates_associated :entity_organizations


  ######## SCOPES ########
  #==============================================
  # AVISO DE PROYECTOS DADOS DE BAJA O PROXIMOS A 
  # SU FECHA DE BAJA CON PERIODO DE N DIAS
  #==============================================
  scope :all_not_review,->(){ 
    where(review_end_project: false).where("execution_end_date <= cast(? as date) OR active=false",Date.today+Constants.n_days.day)
  }
  #==============================================
  scope :all_active,   ->(){ where(active: true) }
  scope :all_suspended,   ->(){ where(suspended: true) }
  scope :all_active_and_published,   ->(){ where(active: true, publish: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive suspended)
      public_send("all_#{status}")
    else
      all
    end
  }
  scope :urgent_projects, ->(){ where(urgent: true) }
  scope :outstanding_projects, ->(){ where(outstanding: true) }

  amoeba do
    enable
    nullify [:id, :pt_extendable_id,:pt_order,:father_id]  
    exclude_association :volunteers
    exclude_association :projects_volunteers
    exclude_association :trackings
    exclude_association :volun_trackings
    exclude_association :volun_contacts
    exclude_association :volun_assessments
    exclude_association :events
    exclude_association :links
    exclude_association :images
    exclude_association :videos
    exclude_association :docs
    exclude_association :logo
    exclude_association :urls
    exclude_association :activities
    exclude_association :meetings
    exclude_association :subprojects
    exclude_association :father
  end

   ######## MÉTODOS ########
   def self.main_columns
    %i(
      id
      pt_extendable_id
      subtype_pt_compact
      subtype_proyect_compact
      pt_order_compact
      father_id
      name
      project_type_compact
      entity
      suspended
      execution_start_date
      execution_end_date
      derivaties_volunteers_count
      volunteers_project_count
      volunteers_num
      beneficiaries_num
      review_end_project
      address_district
      areas_sequence
      collectives_sequence
    )
  end

  def self.normal_columns
    %i(
      subtype_pt_compact
      subtype_proyect_compact
      pt_order_compact
      project_type_compact
      entity
      derivaties_volunteers_count
      volunteers_project_count
      volunteers_num
      beneficiaries_num
      address_district
      areas_sequence
      collectives_sequence
    )
  end

  def self.subproyect_columns
    %i(
      pt_order_compact
      name
      execution_start_date
      execution_end_date
      derivaties_volunteers_count
      volunteers_project_count      
      address_district
    )
  end

  def self.matriz_columns
    %i(
      id
      name
      execution_start_date
      execution_end_date
      derivaties_volunteers_count
      volunteers_project_count      
      address_district
    )
  end

  def self.main_columns_all
    %i(id
         id_number
         name
         pt_extendable_type
         entity
         )
  end

  def self.ransack_default
    {s: 'updated_at desc'} 
  end

  def to_s
    name
  end

  def name_id
    "#{id} - #{name}"
  rescue
    name
  end

  def address_district
    if self.addresses.blank? 
      self.district.try(:name)
    else
      self.addresses.map {|a| a.district.try(:name)}.to_sentence
    end
  end

  def areas_sequence
    self.areas.uniq.to_sentence
  end

  def collectives_sequence
    self.collectives.uniq.to_sentence
  end

  def pt_order_compact    
    "#{self.father_id}.#{self.pt_order}" if !self.father_id.blank?
  rescue
    ""
  end

  def self.tooltip
    aux = ''
    [:name,:description,:project_type_compact].each do |field|
      aux = Common.get_message_tooltip(aux, Project,field)     
    end
    [:district].each do |field|
      aux = Common.get_message_tooltip(aux, Address,field)     
    end
    aux
  end

  def project_type_compact
    self.pt_extendable.model_name.human(count: 2)
  rescue
    ""
  end

  def subtype_pt_compact
    I18n.t("pt_types.#{self.subtype_pt}") if !self.subtype_pt.blank?
  rescue
    ""
  end

  def subtype_proyect_compact
    I18n.t("pt_types.#{self.subtype_proyect}") if !self.subtype_proyect.blank?
  rescue
    ""
  end

  #==============================================
  # COMPROBACIÓN DEL AVISO
  #==============================================
  def notice_end?
    self.active == false || !self.execution_end_date.blank? && self.execution_end_date <= (Date.today + Constants.n_days.day)
  end
  #==============================================

  def derivaties_volunteers_count
    volunteers = Volunteer.not_market.joins("INNER JOIN projects_volunteers as pv ON volunteers.id = pv.volunteer_id AND pv.volunteer_type='DERIVATIE'").distinct    
    if self.subtype_proyect == "matriz" 
      #if self.active && self.active_execution_dates
        volunteers = volunteers.where("(project_id = ? OR project_id in (?)) and volunteer_type=?",self.id,  self.subprojects.ids ,"DERIVATIE")
      #else
       # volunteers = volunteers.where("(project_id = ? OR pv.active=true and project_id in (?)) and volunteer_type=?",self.id,  self.subprojects.where("case when execution_end_date is null then cast(? as date) >=cast(execution_start_date as date) ELSE cast(? as date) between cast(execution_start_date as date) and cast(execution_end_date as date) END", Time.zone.now,Time.zone.now).ids ,"DERIVATIE")
      #end
    else 
      volunteers = volunteers.where("pv.project_id in (?)",  self.id)
    end

    volunteers.count
  end

  def volunteers_project_count  
    volunteers = Volunteer.not_market.joins("INNER JOIN projects_volunteers as pv ON volunteers.id = pv.volunteer_id AND (pv.volunteer_type!='DERIVATIE' OR pv.volunteer_type is null)").distinct    
    if self.subtype_proyect == "matriz" 
      #if self.active && self.active_execution_dates
        volunteers = volunteers.where("(project_id = ? OR project_id in (?)) and (pv.volunteer_type!= 'DERIVATIE' or pv.volunteer_type is null)", self.id,  self.subprojects.ids)
      #else 
      #  volunteers = volunteers.where("(project_id = ? OR pv.active=true and project_id in (?)) and (pv.volunteer_type= 'ASOCIATED' or pv.volunteer_type is null)", self.id,  self.subprojects.where("case when execution_end_date is null then cast(? as date) >=cast(execution_start_date as date) ELSE cast(? as date) between cast(execution_start_date as date) and cast(execution_end_date as date) END", Time.zone.now,Time.zone.now).ids)
      #end
    else 
      volunteers = volunteers.where("pv.project_id in (?)",  self.id)
    end

    volunteers.count
  end

  def derivaties_volunteers_all_count
    project_ids = [self.id]    
    project_ids = project_ids + self.subprojects.ids if self.subtype_proyect == "matriz"    
    derivaties_volunteers_count + Volunteer.joins(:trackings => [:tracking_type]).where("project_id in (?) AND tracking_types.alias_name = 'derivatie' AND volunteers.id not in (?)", project_ids,self.volunteers.where("projects_volunteers.volunteer_type='DERIVATIE'").uniq.select("volunteers.id")).uniq.count
  end

  def volunteers_project_all_count
    project_ids = [self.id]    
    project_ids = project_ids + self.subprojects.ids if self.subtype_proyect == "matriz"    
    volunteers_project_count + Volunteer.joins(:trackings => [:tracking_type]).where("project_id in (?) AND tracking_types.alias_name = 'subscribe' AND volunteers.id not in (?)", project_ids,self.volunteers.where("projects_volunteers.volunteer_type!='DERIVATIE' OR projects_volunteers.volunteer_type is null").uniq.select("volunteers.id")).uniq.count
  end

  def volunteers_num_active
    project_ids = [self.id]    
    project_ids = project_ids + self.subprojects.ids if self.subtype_proyect == "matriz"    
    Volunteer.joins(:projects).not_market.where("projects_volunteers.active = true AND projects.id in (?) AND volunteers.active = true", project_ids).count
  end

  def build_pt_extendable(attributes = {})
    self.pt_extendable = pt_extendable_class.new(attributes.merge(project: self))
  end

  def participants_num_calculated 
    unless self.id.blank?
      self.participants_num=self.volunteers.where("volunteers.active = true AND projects_volunteers.active=true").count
    end
  end 

  def update_dates
    self.change_status_at =  Time.current
    if self.suspended
      self.suspended_at = Time.current
    else
      self.reactivated_at = Time.current
    end
    self.save
  end

  def active_execution_dates
    (self.execution_end_date.blank? && Time.zone.now.to_date >= self.execution_start_date.to_date) || (!self.execution_end_date.blank? && Time.zone.now.to_date >= self.execution_start_date.to_date && Time.zone.now.to_date <= self.execution_end_date.to_date)
  end




  
  private
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = ['search_id',
      'enter_text',
      'search_pt_id',
      'search_subpt_id',
      'search_nombre',
      'search_ambito',
      'search_entidad',
      'search_colectivo',
      'search_district',
      'search_status',
      'search_tipo',
      'search_inicio_f',
      'search_year',
      'search_review',
      'search_volun_allowed',
      'search_outstanding',
      'search_publish',
      'search_urgent',
      'search_fin_f',
      'search_subproyect_pt',
      'search_subproyect_type'
    ]
    ParamsFilter.get_filter(params, keys, 'project_filter.')
  end

  def execution_start_date_less_than_execution_end_date
    return unless execution_start_date && execution_end_date

    unless execution_start_date <= execution_end_date
      errors.add(:execution_start_date, :execution_start_date_must_be_less_than_execution_end_date)
    end
  end
  
  def check_valid_start_date
    return unless events.any?
    validation = true
    self.events.each do |event|
      unless execution_start_date
        validation = false
      end
      if validation
        if execution_end_date?
          unless (execution_end_date >= execution_start_date)
            errors.add(:base, :timetable_must_be_between_execution_start_date_and_execution_end_date)
          end
        end
      else
        errors.add(:base, :invalid_start_date)
        return true
      end
    end
  end

  def pt_extendable_class
    pt_extendable.try(:class)
  end


  def validate_file_size
    link = Link.where(linkable_id: self.id, linkable_type: "Project").where("path != '/images/missing.png'").count
    if link == 0  || self.id.blank? || !Project.find(self.id).try(:logo).blank? && Project.find(self.id).try(:logo).try(:file_file_name) != self.try(:logo).try(:file_file_name)
      if !Setting.find_by(key: "VolunBackend.file_size_kb").blank?
        value = Setting.find_by(key: "VolunBackend.file_size_kb").try(:value).to_i
        if !self.try(:logo).blank? && !self.try(:logo).try(:file_file_size).blank? && self.try(:logo).try(:file_file_size)/1024 > value
          value >= 1024 ? self.errors.add(:logo,:size_mb, value: value, value_mb: value/1024) : self.errors.add(:logo,:size, value: value)
          return false
        end
      else
        self.errors.add(:logo,:setting)
      end
    end
  rescue
    true
  end
end
