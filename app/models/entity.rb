class Entity < ActiveRecord::Base
  include Archivable
  extend EntitiesHelper

  ######## RELACIONES ########
  belongs_to :reason, -> { where(active: true).order('req_reasons.name asc') }, class_name: 'Req::Reason', foreign_key: 'req_reason_id'
  belongs_to :entity_type, -> {order(:name)}, class_name: "EntityType", foreign_key: "entity_type_id", optional: true
  belongs_to :address, required: true, class_name: "Address", foreign_key: "address_id"
  has_one :user, class_name: 'User', foreign_key: 'loggable_id', as: :loggable
  has_one  :logo,   -> { entity_logo   }, class_name: 'Link', foreign_key: 'linkable_id',  required: false, validate: false
  has_many :images, -> { entity_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { entity_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { entity_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :projects, ->(){ order('created_at' => :asc) }, class_name: "Project", foreign_key: "entity_id"
  has_many :trackings, :class_name => 'Ent::Tracking'
  has_many :links, as: :linkable, class_name: "Link", foreign_key: "linkable_id"

  ######## ATRIBUTOS ANIDADOS ########
  accepts_nested_attributes_for :projects
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :images, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :docs,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :urls,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :logo,   reject_if:  :all_blank, allow_destroy: true

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, :vat_number, :email, :representative_name, :representative_last_name, :contact_name,
            :contact_last_name, :entity_type_id, presence: true
  validates :phone_number, format: { with: /[6|7|8|9]\d{8}/ }, allow_blank: true, unless: -> {phone_number_blank_mask}
  validates :phone_number_alt, format: { with: /[6|7|8|9]\d{8}/ }, allow_blank: true, unless: ->{phone_number_alt_blank_mask}
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, uniqueness: true
  validates :vat_number, spanish_vat: true, uniqueness: {message: I18n.t('activerecord.errors.messages.duplicated_ent_id')}
  validate :uniq_email
  validate :validate_file_size

  ######## SCOPES ########
  scope :all_active,   ->(){ where(active: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all
    end
  }

  ######## MÉTODOS ########
  def self.main_columns
    %i(name vat_number email entity_type)
  end

  def to_s
    name
  end

  alias_method :full_name, :to_s

  def self.tooltip
    aux = ''
    [:name,:vat_number,:email,:entity_type].each do |field|
      aux = Common.get_message_tooltip(aux, Entity,field)     
    end
    aux
  end

  private
  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = [
      'enter_text',
      'search_nombre',
      'search_status',
      'search_tipo',
      'search_cif']
    ParamsFilter.get_filter(params, keys, 'entity_filter.')
  end

  def uniq_email
    if self.id.blank?
      exists= User.where("email='#{self.email}'").count
    else
      exists= User.where("email='#{self.email}' AND not (loggable_id=#{self.id} AND loggable_type='Entity')").count
    end
    if exists.to_i>0
     errors.add(:email, I18n.t('user.exists'))
    end
  end

  def phone_number_blank_mask
    avoid_phone_mask(phone_number)
  end

  def phone_number_alt_blank_mask
    avoid_phone_mask(phone_number_alt)
  end

  def avoid_phone_mask(phone)
    phone == "_________"
  end
  def validate_file_size
    link = Link.where(linkable_id: self.id, linkable_type: "Entity").where("path != '/images/missing.png'").count
    if link == 0  || self.id.blank? || !Entity.find(self.id).try(:logo).blank? && Entity.find(self.id).try(:logo).try(:file_file_name) != self.try(:logo).try(:file_file_name)
      if !Setting.find_by(key: "VolunBackend.file_size_kb").blank?
        value = Setting.find_by(key: "VolunBackend.file_size_kb").value.to_i
        if !self.try(:logo).blank? && !self.try(:logo).try(:file_file_size).blank? && self.try(:logo).try(:file_file_size)/1024 > value
          value >= 1024 ? self.errors.add(:logo,:size_mb, value: value, value_mb: value/1024) : self.errors.add(:logo,:size, value: value)
          return false
        end
      else
        self.errors.add(:logo,:setting)
      end
    end
  rescue
    true
  end

   def self.tooltip
        aux = ''
        [:id,:name,:last_name_1,:last_name_2,:email,:phone].each do |field|
          aux = Common.get_message_tooltip(aux, Citizen,field)     
        end
        aux
      end
end
