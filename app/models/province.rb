class Province < ActiveRecord::Base
  include Archivable

  ######## MÉTODOS ########
  def to_s
    name
  end

  def self.main_columns
    [:id, :name, :code, :active]
  end

  def self.tooltip
      aux = ''
      [:name,:id, :code].each do |field|
          aux = Common.get_message_tooltip(aux, Province,field)
      end
      aux
  end
end
