class CitizenServiceTracking < ApplicationRecord
  extend CitizenServiceTrackingsHelper
  belongs_to :citizen_service, class_name: "CitizenService", foreign_key: "citizen_service_id", optional: true
  belongs_to :tracking_type, ->{order(:name)}, class_name: "TrackingType", foreign_key: "tracking_type_id", optional: true
  belongs_to :manager ,class_name: "Manager", foreign_key: "manager_id", optional: true, required: false

  accepts_nested_attributes_for :citizen_service

 ######## MÉTODOS ########
  def self.main_columns
    %i(citizen_service tracking_type manager tracked_at automatic coments)
  end

#==============================================
# Método que define los filtros en la descarga CSV
#==============================================
  def self.get_filter(params)
    keys = [
      'search_enter_text',
      'search_tipo',
      'search_manager']
    ParamsFilter.get_filter(params, keys, 'citizen_service_tracking_filter.')
  end

  def self.tooltip
    aux = ''
    [:coments, :citizen_service, :manager,:tracking_type].each do |field|
      aux = Common.get_message_tooltip(aux, CitizenServiceTracking,field)     
    end   
    aux
  end
end
