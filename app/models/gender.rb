class Gender < ActiveRecord::Base  
  include Archivable

  has_many :volunteers
  has_many :citizens

  def self.tooltip
    aux = ''
    [:id,:name].each do |field|
      aux = Common.get_message_tooltip(aux, Gender,field)     
    end
    aux
  end

  def to_s
    self.name
  end
end