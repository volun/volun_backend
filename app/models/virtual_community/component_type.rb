class VirtualCommunity::ComponentType < ActiveRecord::Base
  include Archivable
  include Recordable
  self.table_name = "virtual_community_component_types"
  
  validates :title, :code, presence: true
  validates :title, uniqueness: true
  validates :code, uniqueness: true

  def to_s
    title
  end

  def self.tooltip
    aux = ''
    [:id,:title,:code].each do |field|
      aux = Common.get_message_tooltip(aux, VirtualCommunity::ComponentType,field)
    end
    aux
  end
end
