class Activity < ActiveRecord::Base
  include Archivable
  include Constants
  extend ActivitiesHelper
  ######## RELACIONES ########
  has_many :events, class_name: "Event", foreign_key: 'eventable_id', as: :eventable
  has_many :links, class_name: "Link", foreign_key: 'linkable_id', as: :linkable
  has_many :images, -> { activity_images }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :videos, -> { activity_videos }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :docs,   -> { activity_docs   }, class_name: 'Link', foreign_key: 'linkable_id'
  has_many :urls,   -> { activity_urls   }, class_name: 'Link', foreign_key: 'linkable_id'
  
  has_one  :logo,   -> { activity_logo   }, class_name: 'Link', foreign_key: 'linkable_id', required: false, validate: false
  
  has_and_belongs_to_many :areas_activities, optional: true
  has_and_belongs_to_many :areas, ->{ where(active: true).order('areas.name asc') }, :through => :areas_activities
  
  belongs_to :entity, -> {order(:name)}, class_name: "Entity", foreign_key: "entity_id", optional: true
  belongs_to :project, ->{ where(active: true).order('projects.name asc') }, class_name: "Project", foreign_key: "project_id", optional: true

  ######## ATRIBUTOS ANIDADOS ########
  accepts_nested_attributes_for :events, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :images, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :videos, reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :docs,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :urls,   reject_if:  :all_blank, allow_destroy: true
  accepts_nested_attributes_for :logo,   reject_if:  :all_blank, allow_destroy: true

  ######## VALIDACIONES ########
  validates :name, uniqueness: true
  validates :name, :description, :start_date, presence: true
  validate :start_date_less_than_end_date
  validate :check_timetables_execution_date
  validates :link_url,format: { with: /[http|https]:\/\/*/ }, allow_blank: true
  validate :validate_file_size
  #validates_associated :logo
  ######## SCOPES ########
  scope :all_active,    ->(){ where(active: true) }
  scope :all_inactive,  ->(){ where(active: false) }

  #==============================================
  # AVISO DE ACTIVIDADES DADAS DE BAJA O PROXIMAS A 
  # SU FECHA DE BAJA CON PERIODO DE N DIAS
  #==============================================
  scope :all_not_review,->(){ 
    where(review_end_activity: false).where("end_date <= cast(? as date) OR active=false",Date.today + Constants.n_days.day)
  }
  #==============================================
  scope :all_active,   ->(){ where(active: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all
    end
  }
  ######## MÉTODOS ####### 
  def self.main_columns
    %i(id name description start_date end_date transport entity get_areas project share review_end_activity)
  end

  def self.project_columns
    %i(id name description start_date end_date transport entity get_areas share)
  end

  #==============================================
  # Método que define los filtros en la descarga CSV
  #==============================================
  def self.get_filter(params)
    keys = ['search_id',
      'enter_text',
      'search_denominacion',
      'search_tipo',
      'search_organizador',
      'search_inicio_f',
      'search_status',
      'search_fin_f',
      'search_rrss',
      'search_end_activity']
    ParamsFilter.get_filter(params, keys, 'activity_filter.')
  end

  #==============================================
  # COMPROBACIÓN DEL AVISO
  #==============================================
  def notice_end?
    self.active == false || !self.end_date.blank? && self.end_date <= (Date.today + Constants.n_days.day)
  end
  #==============================================

  def to_s 
    name 
  end

  def get_areas
    aux = ""
    self.areas.each {|a| aux = aux + a.name + " ; " }
    aux
  end

  def self.tooltip
    aux = ''
    [:name,:description,:transport,:pdf_url].each do |field|
      aux = Common.get_message_tooltip(aux, Activity,field)     
    end
    aux
  end

  private
  def start_date_less_than_end_date
    return unless start_date && end_date

    unless start_date <= end_date
      errors.add(:start_date, :execution_start_date_must_be_less_than_execution_end_date)
    end
  end

  def check_timetables_execution_date
    return unless events.any?
    validation = true
    self.events.each do |event|
      event.timetables.each do |timetable|
        if timetable.execution_date?
          if end_date?
            unless (timetable.execution_date >= start_date) && (timetable.execution_date <= end_date)
              validation = false
            end
          end
        end
        unless validation
          errors.add(:base, :timetable_must_be_between_execution_start_date_and_execution_end_date)
          return true
        end
      end
    end
  end
  
  def validate_file_size
    link = Link.where(linkable_id: self.id, linkable_type: "Activity").where("path != '/images/missing.png'").count
    if link == 0  || self.id.blank? || !Activity.find(self.id).try(:logo).blank? && Activity.find(self.id).try(:logo).try(:file_file_name) != self.try(:logo).try(:file_file_name)
      if !Setting.find_by(key: "VolunBackend.file_size_kb").blank?
        value = Setting.find_by(key: "VolunBackend.file_size_kb").try(:value).to_i
        if !self.try(:logo).blank? && !self.try(:logo).try(:file_file_size).blank? && self.try(:logo).try(:file_file_size)/1024 > value
          value >= 1024 ? self.errors.add(:logo,:size_mb, value: value, value_mb: value/1024) : self.errors.add(:logo,:size, value: value)
          return false
        end
      else
        self.errors.add(:logo,:setting)
      end
    end
  rescue
    true
  end
end
