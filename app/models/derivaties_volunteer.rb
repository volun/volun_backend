class DerivatiesVolunteer < ActiveRecord::Base
    extend DerivatiesVolunteersHelper
    include Recordable
    include Archivable

    ######## RELACIONES ########
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :project, class_name: "Project", foreign_key: "project_id", optional: true

    ######## MÉTODOS ########
    def self.main_columns
        %i(
           volunteer_id
           volunteer_name
           volunteer_last_name
           volunteer_last_name_alt
           volunteer_phone_number_alt
           volunteer_email
           meeting_data
           turn_data
        )
    end

    def volunteer_id
        self.volunteer.try(:id_autonum)
    end

    def volunteer_name
        self.volunteer.try(:name)
    end

    def volunteer_last_name
        self.volunteer.try(:last_name)
    end

    def volunteer_last_name_alt
        self.volunteer.try(:last_name_alt)
    end

    def volunteer_phone_number_alt
        self.volunteer.try(:phone_number_alt)
    end

    def volunteer_email
        self.volunteer.try(:email)
    end

    def meeting_data
        "#{date} | #{hour} | #{place}" unless date.blank?
    end

    def turn_data
        "#{turn} - #{day}" unless turn.blank?
    end

end
