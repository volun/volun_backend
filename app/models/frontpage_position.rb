class FrontpagePosition < ActiveRecord::Base
  include Archivable
  include Recordable

  ######## VALIDACIONES ########
  validates :position, presence: true, uniqueness: true

  ######## MÉTODOS ########
  def to_s
    position
  end
end
