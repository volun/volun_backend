class Manager < ActiveRecord::Base
  include Archivable
  include VirtualFullName

  ######## RELACIONES ########
  belongs_to :profile, class_name: "Profile", foreign_key: "profile_id", optional: true
  belongs_to :role, class_name: "Role", foreign_key: "role_id", optional: true

  has_many :contacts, :class_name => 'Volun::Contact'
  has_many :trackings, :class_name => 'Volun::Tracking'
  has_many :request_forms, class_name: "RequestForm", foreign_key: "manager_id"
  has_many :permissions, dependent: :destroy, class_name: "Permission", foreign_key: "manager_id"#, ->{ includes(:resource).where(resources: { active: true }).order('resources.description asc') }
  has_many :volunteers, class_name: "Volunteer"
  has_many :volunteer_projects_relations, class_name: "VolunteerProjectsRelation"
  has_many  :citizen_tracking, class_name: "CitizenTracking", foreign_key: "manager_id"
  has_many  :citizen_service_tracking, class_name: "CitizenServiceTracking", foreign_key: "manager_id"

  has_many :volunteer_pers,->{ where(section: "Volunteer") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_project,->{ where(section: "Project") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_entity,->{ where(section: "Entity") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_activity,->{ where(section: "Activity") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_citizen_track,->{ where(section: "Citizen::Tracking") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_citizen_service_track,->{ where(section: "CitizenService::Tracking") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_ent_track,->{ where(section: "Ent::Tracking") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_vol_track,->{ where(section: "Volun::Tracking") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_pro_track,->{ where(section: "Pro::Tracking") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_publishing,->{ where(section: "Rt::ProjectPublishing") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_other,->{ where(section: "Rt::Other") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_demand,->{ where(section: "Rt::VolunteersDemand") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_subscribe,->{ where(section: "Rt::VolunteerSubscribe") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_appoint,->{ where(section: "Rt::VolunteerAppointment") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_act_publish,->{ where(section: "Rt::ActivityPublishing") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_proj_subscribe,->{ where(section: "Rt::VolunteerProjectSubscribe") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_solidarity,->{ where(section: "Rt::SolidarityChain") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_rt_amends,->{ where(section: "Rt::VolunteerAmendment") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_admin,->{ where(section: "Administration") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_citizen,->{ where(section: "Citizen") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_citizen_service,->{ where(section: "CitizenService") }, :class_name => 'Permission', dependent: :destroy
  has_many :volunteer_digital_manage,->{ where(section: "Digital::Manage") }, :class_name => 'Permission', dependent: :destroy

  has_one  :user,  class_name: "User", foreign_key: "loggable_id", as: :loggable

  ######## ATRIBUTOS ANIDADOS ########
  accepts_nested_attributes_for :permissions,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_pers,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_project,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_entity,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_activity,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_citizen_track,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_citizen_service_track,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_ent_track,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_vol_track,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_pro_track,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_publishing,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_other,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_demand,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_subscribe,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_appoint,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_act_publish,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_proj_subscribe,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_solidarity,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_rt_amends,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_admin,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_citizen,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_citizen_service,:allow_destroy => true
  accepts_nested_attributes_for :volunteer_digital_manage,:allow_destroy => true

  ######## VALIDACIONES ########
  delegate :is_administrator?, :super_admin?, :admin?, :internal_staff?, :external_staff?, :special_trust?, :digital_manage?, to: :role, allow_nil: true

  validates :name, presence: true

  class << self
    delegate :kinds, :kinds_i18n, to: Role
  end

  ######## MÉTODOS ########
  def self.main_columns
    %i(name last_name last_name_alt login phone_number role)
  end

  def to_s
    "#{name} #{last_name} #{last_name_alt}"
  end

  alias_method :full_name, :to_s

  def self.tooltip
    aux = ''
    [:name,:last_name,:last_name_alt,:alias_name].each do |field|
      aux = Common.get_message_tooltip(aux, Manager,field)     
    end
    aux
  end
end
