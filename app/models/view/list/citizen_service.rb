class View::List::CitizenService < View::List 
  include Archivable
  extend CitizenServicesHelper
  self.table_name = "view_list_citizen_services"

  belongs_to :citizen, class_name: "Citizen", foreign_key: "citizen_id", optional: true
    belongs_to :service, class_name: "Service", foreign_key: "service_id", optional: true
    belongs_to :volunteer, class_name: "Volunteer", foreign_key: "volunteer_id", optional: true
    belongs_to :status_service, class_name: "StatusService", foreign_key: "status_service_id", optional: true
    belongs_to :volunteer_vote_rel, class_name: "ServiceVote", foreign_key: "volunteer_vote_id", optional: true
    belongs_to :citizen_vote_rel, class_name: "ServiceVote", foreign_key: "citizen_vote_id", optional: true
    belongs_to :borought, class_name: "Borought", foreign_key: "borought_id", optional: true
    belongs_to :district, class_name: "District", foreign_key: "district_id", optional: true

    has_many :service_responses, class_name: "ServiceResponse", foreign_key: "citizen_service_id"
    has_many :service_votes, class_name: "ServiceVote", foreign_key: "citizen_service_id"
  has_many :service_responses, class_name: "ServiceResponse", foreign_key: "citizen_service_id", primary_key: 'id'

  scope :not_market, -> () { where("citizen_id in (select citizens.id from citizens where citizens.market = false)")}
    scope :not_tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = false)") }
    scope :tester, ->() { not_market.where("citizen_id in (select citizens.id from citizens where citizens.tester = true)") }
    scope :all_prepared_active, -> () { where("status in ('active','accept','prioritary')")}
    scope :all_active,   ->(){ where(active: true) }
    scope :status,   ->(status){ where(status: status) }
    scope :for_ids_with_order, ->(ids) {
        # order = sanitize_sql_array(
        #   ["position(id::text in ?)", ids.join(',')]
        # )
        # where(:id => ids).order(order)

        where(:id => ids).order(id: :asc)
      }

end
