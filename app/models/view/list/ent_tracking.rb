class View::List::EntTracking < View::List 
  extend EntTrackingsHelper
  belongs_to :request_form, class_name: "RequestForm", foreign_key: "request_form_id", optional: true

  self.table_name = "view_list_ent_trackings"

  def self.ransack_default
    {s: 'tracked_at desc'}
  end
end
