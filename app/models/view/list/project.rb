class View::List::Project < View::List 
  include ArchivableProject
  self.table_name = "view_list_projects"

  scope :all_not_review,->(){ 
    where(review_end_project: false).where("execution_end_date <= cast(? as date) OR active=false",Date.today+Constants.n_days.day)
  }
  #==============================================
  scope :all_active,   ->(){ where(active: true) }
  scope :all_suspended,   ->(){ where(suspended: true) }
  scope :all_active_and_published,   ->(){ where(active: true, publish: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive suspended)
      public_send("all_#{status}")
    else
      all
    end
  }
  scope :urgent_projects, ->(){ where(urgent: true) }
  scope :outstanding_projects, ->(){ where(outstanding: true) }

end
