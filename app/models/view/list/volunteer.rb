class View::List::Volunteer < View::List 
  include ArchivableVolunteer
  extend VolunteersHelper
  self.table_name = "view_list_volunteers"

  scope :not_market, ->() { where(market: false)}
  scope :not_tester, ->() { not_market.where(tester: false)}
  scope :tester, ->() { not_market.where(tester: true)}
  scope :project_accompany, ->() { not_market.joins("INNER JOIN projects_volunteers ON volunteers.id = projects_volunteers.volunteer_id").where("projects_volunteers.project_id in (?)", Project.where(accompany: true).select(:id)) }
  scope :all_availabilities, ->(){ not_market.where(active: true).where(available: true).where("availability_date <= (?)",Date.today+Constants.n_days.day) }  
  scope :all_active,   ->(){ not_market.where(active: true) }
  scope :all_inactive, ->(){ not_market.where(active: false) }
  scope :last_tracking, ->(){ not_market.joins(:trackings).last}
  scope :trackings_projects_automatic_between, ->(ids) { not_market.where("id IN (?)",ids)}
  scope :trackings_projects_automatic_start, ->(date_init) { not_market.where("id IN 
    (select volun_trackings.volunteer_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) >= cast('#{date_init.to_date}' as date) AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='subscribe')")}
  scope :trackings_projects_automatic_end, ->(date_last) { not_market.where("id IN 
      (select volun_trackings.volunteer_id from volun_trackings, tracking_types where volun_trackings.project_id is not null AND cast(volun_trackings.tracked_at as date) <= cast('#{date_last.to_date}' as date) AND volun_trackings.tracking_type_id = tracking_types.id AND tracking_types.alias_name='unsubscribe')")}
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all.not_market
    end
  }


  def self.ransack_default
    {s: 'updated_at desc'}
  end

end
