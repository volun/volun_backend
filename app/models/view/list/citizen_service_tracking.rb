class View::List::CitizenServiceTracking < View::List 
  extend CitizenServiceTrackingsHelper
  self.table_name = "view_list_citizen_service_trackings"

  def self.ransack_default
    {s: 'tracked_at desc'}
  end
end
