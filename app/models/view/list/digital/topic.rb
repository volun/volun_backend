class View::List::Digital::Topic < View::List 
  include Archivable
  include Recordable
  extend ::Digital::TopicHelper
  self.table_name = "view_list_digital_topics"

end
