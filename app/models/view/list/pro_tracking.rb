class View::List::ProTracking < View::List 
  extend ProTrackingsHelper
  self.table_name = "view_list_pro_trackings"

  def self.ransack_default
    {s: 'tracked_at desc'}
  end

end
