class View::List::Rt < View::List 
     
    default_scope -> { order(status_date: :desc) }
    scope :with_statuses, ->(statuses = []){
      return none unless statuses.any?
  
      where(:req_status => statuses )
    }
    scope :with_status, ->(status){ with_statuses status }
    scope :pending,     ->(){ with_status(:pending) }
    scope :processing,  ->(){ with_status(:processing) }
    scope :approved,    ->(){ with_status(:approved) }
    scope :rejected,    ->(){ with_status(:rejected) }
    scope :appointed,   ->(){ with_status(:appointed) }
  
    def appointed?
      self.req_status=="appointed"
    end
  
    def pending?
      self.req_status=="pending"
    end
  
    def processing?
      self.req_status=="processing"
    end
  
    def approved?
      self.req_status=="approved"
    end
  
    def rejected?
      self.req_status=="rejected"
    end 
  
    class << self
      delegate :status_names, :get_status_id_by_kind, to: RequestForm
    end

  end
  