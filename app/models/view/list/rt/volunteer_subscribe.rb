class View::List::Rt::VolunteerSubscribe < View::List::Rt
  self.table_name = "view_list_rt_volunteer_subscribes"

  def self.ransack_default
    {s: 'updated_at desc'}
  end
end
