class View::List::Rt::VolunteerProjectSubscribe < View::List::Rt
  self.table_name = "view_list_rt_volunteer_project_subscribes"

  def self.ransack_default
    {s: 'updated_at desc'}
  end
end
