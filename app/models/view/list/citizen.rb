class View::List::Citizen < View::List 
  include Archivable
  extend CitizensHelper
  self.table_name = "view_list_citizens"

  scope :not_market, ->() { where(market: false)}
  scope :not_tester, ->() { not_market.where(tester: false) }
  scope :tester, ->() { not_market.where(tester: true) }
  scope :all_active,   ->(){ not_market.where(active: true) }
  scope :for_ids_with_order, ->(ids) {
      # order = sanitize_sql_array(
      # # Esto no es compatible en rails 6
      #   ["position(id::text in ?)", ids.join(',')]
      # )
      # not_market.where(:id => ids).order(order)
      
      # SOLUCIÓN 1 
      not_market.where(:id => ids).order(id: :asc)
  
    }


end
