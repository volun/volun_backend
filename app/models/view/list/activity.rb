class View::List::Activity < View::List 
  include Archivable
  extend ActivitiesHelper
  self.table_name = "view_list_activities"

  scope :all_active,    ->(){ where(active: true) }
  scope :all_inactive,  ->(){ where(active: false) }

  #==============================================
  # AVISO DE ACTIVIDADES DADAS DE BAJA O PROXIMAS A 
  # SU FECHA DE BAJA CON PERIODO DE N DIAS
  #==============================================
  scope :all_not_review,->(){ 
    where(review_end_activity: false).where("end_date <= cast(? as date) OR active=false",Date.today + Constants.n_days.day)
  }
  scope :all_active,   ->(){ where(active: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all
    end
  }

  def notice_end?
    self.active == false || !self.end_date.blank? && self.end_date <= (Date.today + Constants.n_days.day)
  end


end
