class View::List::VolunTracking < View::List 
  extend VolunTrackingsHelper
  self.table_name = "view_list_volun_trackings"

  def self.ransack_default
    {s: 'tracked_at desc'}
  end


end
