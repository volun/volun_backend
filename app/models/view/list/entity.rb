class View::List::Entity < View::List 
  include Archivable
  extend EntitiesHelper
  self.table_name = "view_list_entities"

  ######## SCOPES ########
  scope :all_active,   ->(){ where(active: true) }
  scope :all_inactive, ->(){ where(active: false) }
  scope :with_status, ->(status){
    if status.to_s.in? %w(active inactive)
      public_send("all_#{status}")
    else
      all
    end
  }

end
