class View::List::CitizenTracking < View::List 
  extend CitizenTrackingsHelper
  self.table_name = "view_list_citizen_trackings"

  def self.ransack_default
    {s: 'tracked_at desc'}
  end
end
