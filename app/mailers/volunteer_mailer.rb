class VolunteerMailer < ActionMailer::Base

  default from: 'from@example.com'#Rails.application.secrets.default_email_sender

  def send_email(mail_address, options = {})
    @message = options[:message]
    if !options[:upload].nil?
      options[:upload].each do |file|
        if !options[:type_upload].blank? && options[:type_upload].to_s == "csv"
          attachments["#{options[:name_upload]}"] = {mime_type: 'text/csv', content: file}
        else
          attachments["#{file.original_filename}"] = File.read(file.tempfile)
        end
      end
    end 
    mail(from: Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end

  def send_cuestionary(mail_address, options = {})
    @message = options[:message]
    if !options[:upload].nil?
      if !options[:type_upload].blank? && options[:type_upload].to_s == "csv"
        attachments["#{options[:name_upload]}"] = {mime_type: 'text/csv', content:  options[:upload]}       
      end
    end 
    mail(from: Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end

  def send_login(mail_address, options = {})
    @user = options[:user]
    @password = options [:password]
    mail(from: options[:sender].presence || Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end

  def send_massive_email(mail_address, options = {}) 
    require 'base64'
    @message = options[:content]
    if !Setting.find_by(key: "VolunBackend.mail_footer").nil?
      @message += "\r\n" * 3 + Setting.find_by(key: "VolunBackend.mail_footer").value
    end
    if !options[:upload].blank?
      options[:upload].each do |file|
        attachments["#{file[:original_filename]}"] =  Base64.decode64(file[:tempfile])
      end
    end    
    mail(from: options[:sender].presence || Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:title])
  end

  def send_acompany_project(mail_address, options = {}) 
    @message = options[:message]
    mail(from: options[:sender].presence || Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end

  def send_confirmation_email(mail_address, options = {})
    @message = options[:message]
    mail(from: options[:sender].presence || Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end
end
