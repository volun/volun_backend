class AdministratorMailer < ActionMailer::Base

  default from: Rails.application.secrets.default_email_sender,
          reply_to: -> {["crespodhe@madrid.es","fernandl@madrid.es"," rybarczykpm@madrid.es"]}

  def send_email_dataset(mail_address, options = {})
    mail(from: Rails.application.secrets.default_email_sender, to: mail_address, subject: options[:subject])
  end
end

