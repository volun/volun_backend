require 'imports/base_importer'

module Importers
  class CitizensImporter < BaseImporter
    def import!
      each_row do |row|
        citizen = Citizen.find_by(id: row[:id])       

        begin
          citizen.logo.file = File.new(Rails.root.join("app/assets", 'images/new_citizen/', row[:logo]).to_s, 'r')
        rescue
        end
        citizen.save(validate: false)       
      end
    end
  end
end
