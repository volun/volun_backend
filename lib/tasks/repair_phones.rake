namespace :repair_phones do
    desc "Reparar phones"
    task subscribes: :environment do
        change_phones_model(Rt::VolunteerSubscribe)
    end

    task subscribes_project: :environment do
        change_phones_model(Rt::VolunteerProjectSubscribe)
    end

    task volunteers: :environment do
        change_phones_model(Volunteer)
    end

    private

    def change_phones_model(model)
        model.all.find_each do |x|
            if x.phone_number.to_s.gsub(/[ -.]*/,'').match(/[6|7]\d{8}/) && x.phone_number_alt.to_s.gsub(/[ -.]*/,'').match(/[8|9]\d{8}/)
                aux = x.phone_number
                x.phone_number = x.phone_number_alt.gsub(/[ -.]*/,'')
                x.phone_number_alt  = aux.gsub(/[ -.]*/,'')
            elsif x.phone_number.to_s.gsub(/[ -.]*/,'').match(/[6|7]\d{8}/) && x.phone_number_alt.to_s.gsub(/[ -.]*/,'').match(/[6|7]\d{8}/)
                x.phone_number = ''
            elsif x.phone_number.to_s.gsub(/[ -.]*/,'').match(/[8|9]\d{8}/) && x.phone_number_alt.to_s.gsub(/[ -.]*/,'').match(/[8|9]\d{8}/)
                x.phone_number_alt  = ''
            elsif x.phone_number.to_s.gsub(/[ -.]*/,'').match(/[6|7]\d{8}/) && x.phone_number_alt.blank?
                x.phone_number_alt  = x.phone_number.gsub(/[ -.]*/,'')
                x.phone_number = ''
            elsif x.phone_number.blank? && x.phone_number_alt.to_s.gsub(/[ -.]*/,'').match(/[8|9]\d{8}/)
                x.phone_number = x.phone_number_alt.gsub(/[ -.]*/,'')
                x.phone_number_alt = ''
            end

            x.phone_number = x.phone_number.to_s.gsub(/[ -.]*/,'')
            x.phone_number_alt = x.phone_number_alt.to_s.gsub(/[ -.]*/,'')

            if x.save
                puts "Modelo #{model} se ha cambiado correctamente con id #{x.id} #{x.phone_number.to_s + " / " + x.phone_number_alt.to_s}"
            else
                puts "ERROR: Modelo #{model} no se ha cambiado con id #{x.id}: #{x.errors.full_messages}"
            end
        end

    end
end