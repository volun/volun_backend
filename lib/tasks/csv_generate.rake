namespace :csv_generate do 
    desc "Generación de csv" 
 
    task :volunteer => :environment do  
        Volunteer.to_csv_volunteers 
        puts "=====================================" 
        puts "Fichero autogenerado - Voluntarios" 
        puts "=====================================" 
    end 
 
    task :nationality => :environment do  
        Nationality.to_csv_nationalities 
        puts "=====================================" 
        puts "Fichero autogenerado - Nacionalidades" 
        puts "=====================================" 
    end 
 
    task :volunteer_by_district => :environment do  
        Volunteer.to_csv_volunteers_by_districts 
        puts "=====================================" 
        puts "Fichero autogenerado - Voluntarios por districto" 
        puts "=====================================" 
    end 
end 