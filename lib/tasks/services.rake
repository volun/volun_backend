namespace :services do
    task :order => :environment do
        services = {
            "1" => 1,
            "2" => 2,
            "3" => 3,
            "4" => 4, 
            "5" => 5, 
            "6" => 6, 
            "7" => 7, 
            "8" => 9,
            "9" => 8
        }
        services.each do |k,v|
            service = Service.find_by(code: k.to_s)
            service.mta_order= v.to_i
            service.save(validate: false)
        
        rescue => e 
            p "Error: #{e}"
        end
    end

    task :update_data => :environment do
        services = [
            [1,"Citas médicas","Podemos acompañarte a una de tus citas médicas el día, la hora y el lugar que tu indiques.", true],
            [2,"Paseo","Podemos acompañarte a dar un paseo el día, la hora y en el lugar que tú indiques.", true],
            [3,"Compras","Podemos acompañarte a hacer la compra.", true],
            [4,"Gestiones administrativas","Podemos acompañarte a realizar gestiones en organismos públicos y ayudarte a rellenar los impresos.", true],
            [5,"Mascotas (paseo o acompañamiento al veterinario)","Entre las ayudas con mascotas podemos afrecerte acompañarte al veterinario o acompañarte al veterinario o acompañarte a sacar a tu perro.", true],
            [6,"Actividades de ocio y tiempo libre","Podemos acompañarte a la biblioteca, al cine o al teatro o a otra actividad lúdica.", true],
            [7,"Acompañamiento en Hospital","Si estás ingresad@ en el hospital, podemos acompañarte y hacerte un poco más amena tu estancia.", true],
            [8,"Otros","Otros servicios.", false],
            [9,"Acompañamiento Digital","Podemos ayudarte en la gestión de trámites digitales. Para ello, una persona voluntaria de Voluntari@s por Madrid se pondrá en contacto contigo y te apoyará telefónicamente para que puedas aprender a realizar tus propios trámites digitales desde casa. Estos acompañamientos se realizarán de lunes a viernes solicitándolo con 72 h de antelación.", true]
        ]
        services.each do |s|
            service = Service.find_by(code: s[0].to_s)
            service.name= s[1]
            service.description = s[2]
            service.normal_flow = s[3]
            service.save(validate: false)
        
        rescue => e 
            p "Error: #{e}"
        end
    end

    task :update_services_names => :environment do
        CitizenService.all.each do |cs|

            cs.district= Address.district_list.find_by(code: cs.district_code).try(:name)
            cs.borought = Address.borougth_list(cs.district_code).find_by(code: cs.borought_code).try(:name)

            cs.status_service = StatusService.find_by(code:  cs.status)
            if cs.save(validate: false)
                puts "Se ha guardado el servicio"
            else
                puts "Error no se ha guardado el servicio id= #{cs.id}"
            end
        end
    end


    task :add_questions => :environment do
        questions = [
            {question_type: "boolean",id: 1, description: "¿La persona que ha solicitado el acompañamiento vive sola?"},
            {question_type: "boolean",id: 2, description: "¿Sale a la calle más de una vez por semana?"},
            {question_type: "boolean",id: 3, description: "¿Tiene dificultades para salir? (problemas de salud o barreras arquitectónicas) "},
            {question_type: "boolean",id: 4, description: "¿Tiene a alguien a quien acudir si necesita ayuda? (familiares, vecinos, amigos)"},
            {question_type: "boolean",id: 5, description: "¿Refiere sentirse solo a menudo?"},
            {question_type: "string",id: 6, description: "Observaciones"}
        ]

        questions.each do |s|
            ServiceQuestion.find_or_create_by(id: s[:id], description: s[:description], question_type: s[:question_type], active: true )
        end
    end


    task :add => :environment do
        services = {
            "1" => "Citas médicas",
            "2" => "Paseo",
            "3" => "Compras",
            "4" => "Gestiones administrativas", 
            "5" => "Mascotas (paseo o acompañamiento al veterinario)", 
            "6" => "Actividades de ocio y tiempo libre", 
            "7" => "Acompañamiento en hospital", 
            "8" => "Otros",
            "9" => "Acompañamiento Digital"
        }

        services.each do |k,v|
            if !Service.find_by(code: k.to_s,name: v.to_s)
                Service.create(code: k.to_s,name: v.to_s,active: true, description: v.to_s)
            end
        rescue => e 
            p "Error: #{e}"
        end
    end

    task :update => :environment do
        puts "=================== Actualizacion servicios ======================"
        Service.all.each do |service|
            if !service.description.blank?
                service.name = service.description
                service.description = ""
            end
            service.active = true
            if service.save
                puts "Se ha actualizado el serivicio: #{service.id}"
            else
                puts "No se ha podido actualizar el servicio: #{service.id}"
                puts service.errors.full_messages
            end
        end
        puts "============================= FIN ================================="
        puts "=================== Actualizacion preguntas ======================"
        ServiceQuestion.all.each do |q|
            q.active = true
            if q.save
                puts "Se ha actualizado la pregunta: #{q.id}"
            else
                puts "No se ha podido actualizar la pregunta: #{q.id}"
                puts q.errors.full_messages
            end
        end
        puts "============================= FIN ================================="
    end

    task :user => :environment do
        volun = Volunteer.find(11)
        user = volun.user
        user.password = "12345678"
        user.password_confirmation = "12345678"
        if user.save
            puts "Se ha guardado el usuario"
        end
    end

    task :update_status => :environment do
        permit_create = Setting.find_by(key: "VolunBackend.ws_permit_create_service")
        horas = permit_create.blank? ? 48 :  permit_create.value.to_i
        CitizenService.where("status in (?)",['active','prioritary','accept']).each do |sr|
            fecha = Time.zone.parse("#{sr.date_request} #{sr.hour_request}")
            if sr.status!="accept"
              if fecha < Time.zone.now
                  sr.status = "expired"
              elsif fecha - (horas.to_i/2).hours >= Time.zone.now 
                  sr.status = "active"
              else 
                  sr.status = "prioritary"
              end
            elsif fecha < Time.zone.now
              sr.status = "finished"
            end
            sr.status_service = StatusService.find_by(code:  sr.status)

            if sr.save(validate: false)
                puts "- Servicio guardado: #{sr.id} - #{sr.status}"
            else
                puts "- Servicio ERROR: #{sr.id} - #{sr.error.full_messages}"
            end
        end
    end

    task :fill_average_vote => :environment do 
        Citizen.all.each do |c|
            c.update_average_vote
        end
    end

    
end