namespace :volunteer do

    task :logo => :environment do  
        volun = Volunteer.find(16366)
        if !volun.blank?                    
            if !volun.try(:logo).try(:file).blank?
                data = File.open(volun.logo.file.path).read
                encoded = Base64.encode64(data)
                puts "==============================================================="
                puts "Voluntario -> #{volun.id}"
                puts "Path imagen -> #{volun.logo.file.path}"
                puts "Imagen -> #{encoded}"
                puts "==============================================================="
            else
                puts "================================================================="
                puts "Voluntario -> #{volun.id}"
                puts "No se encontró el logo"
                puts "================================================================="
            end
        else
            puts "================================================================="
            puts "No encuentra el voluntario 16366"
            puts "================================================================="
        end
    end

    task :autonum => :environment do
        id = 1
        Volunteer.all.order(id: :asc).each do |v|            
            if v.update_column :id_autonum, id
                puts "Voluntario (#{v.id}): #{v.id_autonum}"
            else
                puts "Error Voluntario (#{v.id})"
            end
            id = id + 1
        end
    end
end