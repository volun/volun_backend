namespace :new_permissions do
    
    desc "Reload permissions"
    task reload: :environment do
        Manager.all.each do |manager|
            manager.permissions.each {|p| p.destroy}
            Permission.generate_permissions(manager.id)
        end
    end

    desc "Add permissions"
    task add: :environment do
        Manager.all.each do |manager|
            Permission.generate_permissions(manager.id)
        end
    end
end

