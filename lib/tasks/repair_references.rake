namespace :repair_references do
    desc "Reparar referencias"
    task projects: :environment do
      Project.all.each do |project|
        pt_project = project.pt_extendable
        if !pt_project.blank?
            if  project.id.to_i!=pt_project.project.id.to_i
                puts "================================================================"
                puts "Proyecto incorrecto P1: #{project.id} - P2: #{pt_project.project.id}"
            
                new_pt = pt_project.class.new(pt_project.attributes)
                
                # REFERENCIA DE PT, HAY QUE ASIGNAR EL PROYECTO CLONADO
                new_pt.project=project
                new_pt.id=nil
            
                # REFERENCIA DE PROJECT, HAY QUE ASIGNAR EL PT CLONADO
                project.pt_extendable = new_pt

                if project.save
                    puts "Proyecto guardado correctamente #{project.id}"
                else
                    puts "Proyecto problematico #{project.id}"
                end
                puts "================================================================"
            else
                puts "================================================================"
                puts "Proyecto correcto P1: #{project.id} - P2: #{pt_project.project.id}"
                puts "================================================================"
            end
        else
            puts "================================================================"
            puts "Proyecto correcto P1: #{project.id}"
            puts "================================================================"
        end
      end
    end

    desc "Reparar nulos en email y teléfonos"
    task null_volunteers: :environment do
        Volunteer.all.each do |volunteer|
            puts "================================================================"
            puts "Voluntario (#{volunteer.id})"
            if volunteer.email.blank?
                ActiveRecord::Base.connection.execute("UPDATE volunteers SET email='' WHERE id=#{volunteer.id}")
                puts "- Email reparado"
            end

            if volunteer.phone_number.blank?
                ActiveRecord::Base.connection.execute("UPDATE volunteers SET phone_number='' WHERE id=#{volunteer.id}")
                puts "- Teléfono reparado"
            end

            if volunteer.phone_number_alt.blank?
                ActiveRecord::Base.connection.execute("UPDATE volunteers SET phone_number_alt='' WHERE id=#{volunteer.id}")
                puts "- Teléfono movil reparado"
            end
            puts "================================================================"
        end
    end

    desc "Repair users of volunteers, managers and entities "
    task add_user: :environment do 
        puts "*********************************************"
        puts "Reparando voluntarios..."
        repair_volunteers
        puts "*********************************************"
        puts "Reparando entidades..."
        repair_entities
        puts "*********************************************"
        puts "Reparando gestores..."
        repair_manager
        puts "*********************************************"
    end

    def repair_volunteers
        Volunteer.all.each do |volunteer|
            if volunteer.user.blank?
                puts "============================================"
                puts "Voluntario (#{volunteer.id}) sin usuario"
                user= volunteer.user
                user = User.new(login: "uservolunteer#{'%09d' % volunteer.id}", loggable: volunteer, notice_type: NoticeType.email.take)
                user.email = volunteer.email
                user.mobile_phone = volunteer.phone_number_alt
                @user_fin = volunteer.email
                @pass =  Digest::SHA1.hexdigest("#{Time.now.to_s}--#{user.login}")[0,8] #contraseña aleatoria
          
                user.password = @pass
                user.password_confirmation = @pass
                user.confirmed_at = Date.today
                unless user.save
                    puts "ERROR: No se ha guardado el usuario"
                    puts user.errors.full_messages
                    puts "============================================"
                else
                    volunteer.user = user
                    unless volunteer.save
                        puts "ERROR: No se ha guardado el voluntario con id(#{volunteer.id})"
                        puts volunteer.errors.full_messages
                        puts "============================================"
                    else
                        puts "Usuario asignado al voluntario"
                        puts "============================================"
                    end
                end
            end
        end
    end
    
    def repair_entities
        Entity.all.each do |entity|
            if entity.user.blank?
                puts "============================================"
                puts "Entidad (#{entity.id}) sin usuario"
                user= entity.user
                user = User.new(login: "userentity#{'%09d' % entity.id}", loggable: entity, notice_type: NoticeType.email.take)
                user.email = entity.email
                user.mobile_phone = entity.phone_number_alt
                @user_fin = entity.email
                @pass =  Digest::SHA1.hexdigest("#{Time.now.to_s}--#{user.login}")[0,8] #contraseña aleatoria
          
                user.password = @pass
                user.password_confirmation = @pass
                user.confirmed_at = Date.today
                unless user.save
                    puts "ERROR: No se ha guardado el usuario"
                    puts user.errors.full_messages
                    puts "============================================"
                else
                    entity.user = user
                    unless entity.save
                        puts "ERROR: No se ha guardado la entidad con id(#{entity.id})"
                        puts entity.errors.full_messages
                        puts "============================================"
                    else
                        puts "Usuario asignado a la entidad"
                        puts "============================================"
                    end
                end
            end
        end
    end

    def repair_manager
        Manager.all.each do |manager|
            if manager.user.blank?
                puts "============================================"
                puts "Gestor (#{manager.id}) sin usuario"
                user= manager.user
                user = User.new(login: "usermanager#{'%09d' % manager.id}", loggable: manager, notice_type: NoticeType.email.take)
                user.email = manager.email
                user.mobile_phone = manager.phone_number
                @user_fin = manager.email
                @pass =  Digest::SHA1.hexdigest("#{Time.now.to_s}--#{user.login}")[0,8] #contraseña aleatoria
          
                user.password = @pass
                user.password_confirmation = @pass
                user.confirmed_at = Date.today
                unless user.save
                    puts "ERROR: No se ha guardado el usuario"
                    puts user.errors.full_messages
                    puts "============================================"
                else
                    manager.user = user
                    unless manager.save
                        puts "ERROR: No se ha guardado el gestor con id(#{manager.id})"
                        puts manager.errors.full_messages
                        puts "============================================"
                    else
                        puts "Usuario asignado al gestor"
                        puts "============================================"
                    end
                end
            end
        end
    end
end