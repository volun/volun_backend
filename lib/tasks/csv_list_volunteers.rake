namespace :csv_list_volunteers do 
    desc "Generación de csv de listado de voluntarios" 
 
    task :list_volunteers => :environment do 
        # Metodo para obtener las cabeceras de cada tabla
        add_header_csv
    end 

    private
    
    def add_header_csv
        count_export = 0
        puts "======================================================="
        VolunExport.all.find_each do |h|
            listado = []
            VolunExport.get_fields.each do |field|
                listado = listado.push I18n.t("activerecord.attributes.volun_export.#{field}")  if h.try(field) 
            end
    
            #Una vez obtenidas las cabeceras, llamo al metodo para obtener los datos
            obtain_date_volunteer(listado,h)
            h.is_export = true
            if h.save
                count_export = count_export + 1
            end

            print "\rSe han exportado #{count_export}/#{VolunExport.all.count}"
        end
        puts ""
        puts "======================================================="
    end

    def obtain_date_volunteer(header,objeto)    
        objVolunter = []
        Volunteer.all.each do |volunteer|
            body = []
            VolunExport.get_fields.each do |field|
                if objeto.try(field)
                    if volunteer.try(VolunExport.get_traslation_volunteer[field]).in? [true,false]                        
                        body.push(I18n.t("humanize.#{volunteer.try(VolunExport.get_traslation_volunteer[field])}"))
                    elsif volunteer.try(:"#{VolunExport.get_traslation_volunteer[field]}")                        
                        body.push(volunteer.try(:"#{VolunExport.get_traslation_volunteer[field]}"))
                    else
                        body.push(volunteer.try(:address).try(VolunExport.get_traslation_volunteer[field]))
                    end
                end
            end
            objVolunter.push(body) 
        end
        # Cuando ya tengo los datos de cada documento , ya podemos generar el fichero csv
        generate_csv(objeto.id,header,objVolunter)
    end

    def generate_csv (id,header,body)
        CSV.open("#{Rails.root.to_s}/public/fichero_listas_#{id}.csv", 'w', col_sep: ';', force_quotes: true, encoding: "ISO-8859-1") do |csv|
            csv << header
            body.each  do |b| 
                aux = []

                b.each do |a|
                    begin
                        aux.push("#{a.blank? ? '' : a}".force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}))
                    rescue => e
                        puts "ERROR: #{e} ----- #{a}"
                        aux.push('')
                    end
                end
                csv << aux
            end           
        end
        
    end
end 
 