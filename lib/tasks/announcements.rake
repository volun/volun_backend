namespace :announcements do
	desc "Comprobación de fechas de baja de Proyectos y de Actividades"

	task :unsubscribe_projects => :environment do
        Project.all.each do |proyecto|
            if proyecto.active && (proyecto.try(:execution_end_date).try{|da| da.past?} || proyecto.try(:execution_end_date)== Date.today)
                proyecto.destroy(nil)
            end
        end
    end
    
    task :unsubscribe_activities => :environment do
        Activity.all.each do |actividad|
            if actividad.active && (actividad.try(:end_date).try{|da|da.past?} || actividad.try(:end_date) == Date.today)
                actividad.destroy(nil)
            end
        end
	end
end