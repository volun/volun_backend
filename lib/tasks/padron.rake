namespace :padron do

    task :get => :environment do  
        Volunteer.all.where(active: true).find_each do |v|
            puts "="*50
            if v.id_number.blank?
                puts "Voluntario #{v.id} no tiene datos"
            else
                puts "Voluntario #{v.id} procesando..."
                padron = Padron.new(nil,v).get_baja
                set_data_padron(padron, v)
            end
        end

        Citizen.all.where(active: true).find_each do |c|
            puts "="*50
            if c.document.blank?
                puts "Mayor #{c.id} no tiene datos"
            else
                puts "Mayor #{c.id} procesando..."
                padron = Padron.new(c).get_baja
                set_data_padron(padron, c)
            end
        end
    end

    private

    def set_data_padron(padron, resource)
        if padron.blank? 
            resource.padron_reason = "No se han encontrado datos en Padron."
        elsif !padron.blank? && padron.try{|x| x[:datos_habitante]}.try{|x| x[:situacion_padron]}.to_s == "B"
            if ["B-DE","B-BG"].include?(padron.try{|x| x[:datos_habitante]}.try{|x| x[:codigo_ultimo_mov]}.to_s)               
                resource.is_death = true
              
               
                if resource.class.name=="Volunteer"
                    resource.unsubscribe_reason=UnsubscribeReason.find_by(code: 'death')
                    resource.status_id=nil
                    resource.unsubscribe_date=Time.zone.parse(padron.try{|x| x[:datos_habitante]}.try{|x| x[:fecha_ultimo_mov]}  )                
                    
                    AuditGenerateHelper.audit_archive(resource,nil, true)
                    volunteer_manager = VolunteerManager.new(volunteer: resource, manager_id: nil)
                    volunteer_manager.register_destroy_volunteer(input_comments: UnsubscribeReason.find_by(code: 'death').name)
                else

                    CitizenService.where(citizen_id: resource.id).each do |service|
                        service.status = "canceled"
                        if AuditGenerateHelper.audit_archive(service,nil, true)
                          CitizenServiceTracking.create(:citizen_service=> service,
                            :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
                            :manager => nil,
                            :automatic=>true,
                            :tracked_at => Time.zone.now,
                            :coments => UnsubscribeReason.find_by(code: 'death').name)
                        end
                      end
                      if AuditGenerateHelper.audit_archive(resource, nil)
                        resource.unsubscribed = true
                        resource.unsubscribed_at = Time.zone.parse(padron.try{|x| x[:datos_habitante]}.try{|x| x[:fecha_ultimo_mov]})
                        CitizenTracking.create(:citizen=> resource,
                          :tracking_type => TrackingType.find_by(alias_name: "unsubscribe"),
                          :manager => nil,
                          :automatic=>true,
                          :tracked_at => Time.zone.now,
                          :coments => UnsubscribeReason.find_by(code: 'death').name)
                      end
                end 

            end
            resource.padron_reason = padron.try{|x| x[:datos_habitante]}.try{|x| x[:descripcion_ultimo_mov]}.to_s 
        end
        puts resource.padron_reason
        puts resource.is_death
        resource.save(validate: false)
    end

end