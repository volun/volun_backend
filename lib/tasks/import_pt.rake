namespace :import_pt do
    task change: :environment do 
        Project.where("pt_extendable_type in ('Pt::Punctual','Pt::Permanent')").each do |project|
            begin
                pt_extendable = project.pt_extendable

                    # case project.project_type.description.to_s
                    # when "pt_social"
                    #     #if project.pt_extendable_type != "Pt::Social"
                    #         aux = Pt::Social.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_centre"
                    #     #if project.pt_extendable_type != "Pt::Centre"
                    #         aux = Pt::Centre.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_entity"
                    #     #if project.pt_extendable_type != "Pt::Entity"
                    #         aux = Pt::Entity.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_subvention"
                    #     #if project.pt_extendable_type != "Pt::Subvention"
                    #         aux = Pt::Subvention.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_other"
                    #     #if project.pt_extendable_type != "Pt::Other"
                    #         aux = Pt::Other.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_retired_volunteer"
                    #     #if project.pt_extendable_type != "Pt::RetiredVolunteer"
                    #         aux = Pt::RetiredVolunteer.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # when "pt_pt_municipal"
                        #if project.pt_extendable_type != "Pt::pt_municipal"
                            aux = Pt::Municipal.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    # when "pt_volunteer"
                    #     #if project.pt_extendable_type != "Pt::Volunteer"
                    #         aux = Pt::Volunteer.new(project: project, notes: pt_extendable.try(:notes))
                    #         aux.save(validate: false)
                    #         project.pt_extendable = aux
                    #         pt_extendable.destroy if !pt_extendable.blank?
                    #     #end
                    # end
                    # project.save(validate: false)

                    puts "Proyecto actualizado con id #{project.id}"
            rescue
                puts "ERROR: Proyecto no se ha actualizado con id #{project.id}"
            end
        end

    end

    desc "Importar datos de proyectos"
    task load: :environment do
        [
            ["933","Primeros Auxilios Psicol�gicos","Matriz","","pt_volunteer","Puntual","pt_volunteer"],
            ["254","Apoyo Escolar Quedamos al salir de clase CEIP Guindalera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["98","Apoyo al estudio en Casa de Senegal 2017","Matriz","","pt_entity","Permanente","Madrid capital del Mito"],
            ["145","Te Contamos La Ciudad 2017","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["376","MADO Madrid Orgullo 2018 (a eliminar)","Matriz","","pt_entity","Puntual","AEGAL"],
            ["70","Todos a desayunar - Colegio M�ndez N��ez 2017","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["223","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - La Elipa 2018","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["589","Voluntarios Junior Colegio Jos� M� de Pereda","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["342","Formas Parte","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["74","Todos a desayunar - Colegio Eduardo Rojo 2017","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["224","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - Nuestra Se�ora de la Concepci�n 2018","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["477","Apoyo  en Campa�a de Fr�o - Centro Pinar de San Jos� 2019","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["69","Todos a desayunar - Colegio P�o XII 2017","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["73","Todos a desayunar - Colegio Antonio Machado","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["81","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - Nuestra Se�ora de la Concepci�n 2017","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["236","Apoyo Escolar Quedamos al salir de clase CEIP Agustina D�ez","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["176","Apoyo a la red de huertos escolares 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["773","Proyecto Valiente Bangla","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["353","Voluntarios Junior IES Lope de Vega","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["95","Apoyo a personas que sufren deterioro cognitivo 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["82","Programa Alpan 2017","Matriz","","pt_entity","Permanente","Club Rotary"],
            ["87","Voluntariado con animales 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["91","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital Gregorio Mara��n 2017","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["106","Apoyo  en Campa�a de Fr�o - Centro Villa de Vallecas","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["225","Programa Alpan 2018","Matriz","","pt_entity","Permanente","Club Rotary"],
            ["255","Apoyo Escolar Quedamos al salir de clase CEIP Jaime Vera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["246","Apoyo Escolar Quedamos al salir de clase CEIP Antonio Machado","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["248","Apoyo Escolar Quedamos al salir de clase CEIP Ciudad de Badajoz","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["261","Apoyo Escolar Quedamos al salir de claseCEIP Vic�lvaro","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["4","Apoyo a la iniciaci�n a la lectura 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["230","Apoyo a personas que sufren deterioro cognitivo 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["28","Los Mayores tambi�n cuentan 2017","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["10","Voluntarixs por Madrid Junior 2017","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["177","Apoyo Escolar 'Quedamos al salir de clase' 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["90","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital La Paz 2017","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["93","Acompa�amiento personas sin hogar en Centro Acogida San Isidro 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["234","Te Contamos La Ciudad 2018","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["361","Apoyo a la red de huertos escolares SAN BLAS ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["232","Apoyo  en Campa�a de Fr�o - Centro Villa de Vallecas 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["13","Hoy comemos juntos 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["71","Todos a desayunar - Colegio Joaqu�n Dicenta","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["76","Escuela deportiva para poblaci�n infantil El Gallinero 2017","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["79","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - La Elipa 2017","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["295","Voluntarios Junior 2018 CEIP P�O XII","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["108","Apoyo  en Campa�a de Fr�o - Centro Pinar de San Jos�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["227","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital La Paz 2018","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["253","Apoyo Escolar 'Quedamos al salir de clase' CEIP Fray Jun�pero Serra","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["257","Apoyo Escolar 'Quedamos al salir de clase' CEIP Loyola de Palacio","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["397","La Casita del Pescador","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["443","Programa ASPA 2018/20","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["249","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ciudad de los �ngeles","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["250","Apoyo Escolar 'Quedamos al salir de clase' CEIP Conde de Romanones","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["350","Voluntarios Junior CEIP Eduardo Rojo","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["264","Apoyo a la red de huertos escolares VILLAVERDE","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["228","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital Gregorio Mara��n 2018","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["355","Apoyo a la red de huertos escolares HORTALEZA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["269","Apoyo a la red de huertos escolares CENTRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["235","Apoyo a la red de huertos escolares MORATALAZ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["268","Apoyo a la red de huertos escolares CHAMBER�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["360","Apoyo a la red de huertos escolares FUENCARRAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["354","Apoyo a la red de huertos escolares RETIRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["491","Apoyo a la red de huertos escolares C. LINEAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["267","Apoyo a la red de huertos escolares USERA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["266","Apoyo a la red de huertos escolares CHAMART�N","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["265","Apoyo a la red de huertos escolares C. LINEAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["344","Apoyo a la red de huertos escolares VILLA VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["263","Apoyo a la red de huertos escolares LATINA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["483","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital Gregorio Mara��n","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["215","Hoy comemos juntos 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["260","Apoyo Escolar 'Quedamos al salir de clase' CEIP Rep�blica de Venezuela","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["748","Apoyo  en Campa�a de Fr�o - Centro Villa de Vallecas 2020","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["247","Apoyo Escolar 'Quedamos al salir de clase' CEIP Calder�n de la Barca","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["251","Apoyo Escolar 'Quedamos al salir de clase' CEIP Daniel Vazquez D�az","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["258","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ram�n M� del Valle Incl�n","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["214","Apoyo a la iniciaci�n a la lectura 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["217","Todos a desayunar - Colegio P�o XII 2018","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["193","Apoyo a los Equipos de Calle del Samur Social 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["218","Todos a desayunar - Colegio M�ndez N��ez 2018","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["345","Apoyo a la red de huertos escolares VIC�LVARO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["216","Los Mayores tambi�n cuentan 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["1029","Formas Parte - EXPODEPOR 2021","Matriz","","pt_volunteer","Puntual","pt_volunteer"],
            ["811","Apoyo a personas que sufren deterioro cognitivo ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["512","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ciudad de los �ngeles","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["591","Apoyo a la red de huertos escolares SAN BLAS ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["604","Voluntarios Junior CEIP Tirso de Molina","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["590","Voluntarios Junior Colegio Jos� M� de Pereda","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["478","Programa de acompa�amiento a menores y sus familias en hospitales - Hospital La Paz 2019","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["533","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - La Elipa","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["918","Programa Alpan","Matriz","","pt_entity","Permanente","Club Rotary"],
            ["771","La Casita del Pescador","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["505","Apoyo Escolar 'Quedamos al salir de clase' CEIP Loyola de Palacio","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["506","Apoyo Escolar 'Quedamos al salir de clase' CEIP Jaime Vera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["508","Apoyo Escolar 'Quedamos al salir de clase' CEIP Antonio Machado","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["596","Voluntariado con animales ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["958","Apoyo  en Campa�a de Fr�o - Centro Villa de Vallecas 2021","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["516","Apoyo Escolar 'Quedamos al salir de clase' CEIP Guindalera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["921","J�venes Voluntarios","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["735","Apoyo a la red de huertos escolares VILLA VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["487","Apoyo a la red de huertos escolares FUENCARRAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["501","Hoy comemos juntos","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["790","Apoyo a la red de huertos escolares CARABANCHEL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["713","Todos a desayunar - Colegio Antonio Machado","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["737","Apoyo a la red de huertos escolares HORTALEZA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["528","Voluntarios Junior CEIP Eduardo Rojo","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["736","Apoyo a la red de huertos escolares CENTRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["971","Programa Alpan","Matriz","","pt_entity","Permanente","Club Rotary"],
            ["493","Apoyo a la red de huertos escolares LATINA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["520","Te Contamos La Ciudad ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["809","Todos a desayunar - Colegio P�o XII ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["801","Apoyo a la red de huertos escolares C. LINEAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["812","Programa ASPA 2018/20","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["525","Todos a desayunar - Colegio Eduardo Rojo ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["808","Todos a desayunar - Colegio M�ndez N��ez ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["712","Ilustra tu ciudad","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["769","Apoyo a los Equipos de Calle del Samur Social ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["805","Te Contamos La Ciudad ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["499","Acompa�amiento personas sin hogar en Centro Acogida San Isidro","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["534","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - Nuestra Se�ora de la Concepci�n","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["714","Formas Parte","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["523","Apoyo a los Equipos de Calle del Samur Social ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["727","pt_social Mujer","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["810","Los Mayores tambi�n cuentan","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["966","Voluntariado con animales ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["1073","Apoyo a la red de huertos escolares TETU�N ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["796","Apoyo a la red de huertos escolares LATINA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["765","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - La Elipa","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["780","Apoyo Escolar 'Quedamos al salir de clase' CEIP Los Almendros","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["784","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ciudad de los �ngeles","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["791","Apoyo a la red de huertos escolares MONCLOA-ARAVACA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["937","Escuelas Deportivas Special Olympics","Matriz","","pt_entity","Permanente","Special Olympics Madrid"],
            ["774","Voluntariado con animales ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["787","Apoyo Escolar 'Quedamos al salir de clase' CEIP Fray Jun�pero Serra","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["988","Leemos para ti (TETUAN)","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["798","Apoyo a la red de huertos escolares BARAJAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["807","Todos a desayunar - Colegio Eduardo Rojo ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["980","Apoyo a la red de huertos escolares VIC�LVARO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["981","Apoyo a la red de huertos escolares FUENCARRAL- EL PARDO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["1042","Fase Piloto 'Madrid Te Acompa�a'","Subproyecto","961","pt_municipal","Permanente","pt_volunteer"],
            ["233","Apoyo  en Campa�a de Fr�o - Centro Pinar de San Jos� 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["957","Apoyo  en Campa�a de Fr�o - Centro Pinar de San Jos� 2021","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["782","Apoyo Escolar 'Quedamos al salir de clase' CEIP Agustina D�ez","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["793","Apoyo a la red de huertos escolares USERA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["987","Los Mayores tambi�n cuentan","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["789","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ram�n M� del Valle Incl�n","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["304","Voluntarios Junior CEIP Tirso de Molina","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["989","Leemos para ti (SAN BLAS-CANILLEJAS)","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["997","Apoyo a la red de huertos escolares USERA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["990","Leemos para ti (MORATALAZ)","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["991","Apoyo a la red de huertos escolares CARABANCHEL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["975","Apoyo a la red de huertos escolares PUENTE DE VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["983","Apoyo a la red de huertos escolares MONCLOA-ARAVACA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["1157","Apoyo a la red de huertos escolares - EI Las Amapolas","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["229","Acompa�amiento personas sin hogar en Centro Acogida San Isidro 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["1072","Tu historia de verdad importa","Matriz","","pt_entity","Permanente","Lo que de verdad importa"],
            ["231","Apoyo al estudio en Casa de Senegal 2018","Matriz","","pt_entity","Permanente","Madrid capital del Mito"],
            ["785","Apoyo Escolar 'Quedamos al salir de clase' CEIP Conde de Romanones","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["352","Voluntarios Junior Colegio Nuestra Se�ora de las Nieves ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["199","J�venes Voluntarios 2018","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["219","Todos a desayunar - Colegio Eduardo Rojo 2018","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["222","Escuela deportiva para poblaci�n infantil 'El Gallinero' 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["239","Voluntariado con animales 2018","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["262","Apoyo a la red de huertos escolares PUENTE DE VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["378","Castellano para inmigrantes en Valiente Bangla","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["379","Familias sin Hogar en Centro Pinar de San Jos�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["381","Acogida de migrantes en Madrid","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["398","CADENA SOLIDARIA","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["422","Apoyo en la ense�anza del castellano en las Escuelas de Adultos","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["427","Escuela sociodeportiva para la poblaci�n infantil","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["473","Apoyo a la red de huertos escolares BARAJAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["474","Apoyo a la red de huertos escolares MONCLOA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["476","Apoyo  en Campa�a de Fr�o - Centro Villa de Vallecas 2019","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["481","La Casita del Pescador","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["482","Programa ASPA 2018/20","Matriz","","pt_entity","Permanente","UTE ASPA. Asociaci�n Centro Trama y Fundaci�n Cruz de los �ngeles"],
            ["484","Apoyo a la red de huertos escolares BARAJAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["485","Apoyo a la red de huertos escolares MONCLOA-ARAVACA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["486","Apoyo a la red de huertos escolares CHAMBER�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["488","Apoyo a la red de huertos escolares RETIRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["489","Apoyo a la red de huertos escolares USERA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["490","Apoyo a la red de huertos escolares CHAMART�N","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["492","Apoyo a la red de huertos escolares VILLAVERDE","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["504","Apoyo Escolar 'Quedamos al salir de clase' CEIP Rep�blica de Venezuela","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["494","Apoyo a la red de huertos escolares PUENTE DE VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["497","Apoyo a la red de huertos escolares VIC�LVARO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["498","Escuela sociodeportiva para la poblaci�n infantil","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["500","Proyecto Valiente Bangla","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["502","Programa Alpan","Matriz","","pt_entity","Permanente","Club Rotary"],
            ["503","Apoyo a personas que sufren deterioro cognitivo ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["507","Apoyo Escolar 'Quedamos al salir de clase' CEIP Vic�lvaro","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["509","Apoyo Escolar 'Quedamos al salir de clase' CEIP Agustina D�ez","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["510","Apoyo Escolar 'Quedamos al salir de clase' CEIP Calder�n de la Barca","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["511","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ciudad de Badajoz","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["513","Apoyo Escolar 'Quedamos al salir de clase' CEIP Conde de Romanones","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["514","Apoyo Escolar 'Quedamos al salir de clase' CEIP Daniel Vazquez D�az","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["529","Voluntarios Junior IES Lope de Vega","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["515","Apoyo Escolar 'Quedamos al salir de clase' CEIP Fray Jun�pero Serra","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["518","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ram�n M� del Valle Incl�n","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["519","Apoyo a la iniciaci�n a la lectura ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["524","Todos a desayunar - Colegio P�o XII ","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["526","Todos a desayunar - Colegio M�ndez N��ez ","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["530","Voluntarios Junior Colegio Nuestra Se�ora de las Nieves ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["535","Los Mayores tambi�n cuentan","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["573","J�venes Voluntarios","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["601","Mujer, barrio y memoria","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["631","La Banda de pt_volunteer","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["738","Apoyo a la red de huertos escolares CARABANCHEL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["766","Apoyo en el Centro Integrado para Menores de Ciudad Lineal - Nuestra Se�ora de la Concepci�n","Matriz","","pt_social","Permanente","pt_volunteer"],
            ["767","Apoyo a la iniciaci�n a la lectura ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["768","Apoyo  en Campa�a de Fr�o - Centro Pinar de San Jos� 2020","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["770","Hoy comemos juntos","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["772","Acompa�amiento personas sin hogar en Centro Acogida San Isidro","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["775","pt_social Mujer","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["777","Apoyo Escolar 'Quedamos al salir de clase' CEIP Rep�blica de Venezuela","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["778","Apoyo Escolar 'Quedamos al salir de clase' CEIP Loyola de Palacio","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["779","Apoyo Escolar 'Quedamos al salir de clase' CEIP Jaime Vera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["781","Apoyo Escolar 'Quedamos al salir de clase' CEIP Antonio Machado","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["783","Apoyo Escolar 'Quedamos al salir de clase' CEIP Calder�n de la Barca","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["786","Apoyo Escolar 'Quedamos al salir de clase' CEIP Daniel Vazquez D�az","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["788","Apoyo Escolar 'Quedamos al salir de clase' CEIP Guindalera","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["792","Apoyo a la red de huertos escolares HORTALEZA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["794","Apoyo a la red de huertos escolares CENTRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["795","Apoyo a la red de huertos escolares PUENTE DE VALLECAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["797","Apoyo a la red de huertos escolares FUENCARRAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["799","Apoyo a la red de huertos escolares CHAMART�N","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["800","Apoyo a la red de huertos escolares CHAMBER�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["920","Apoyo Escolar 'Quedamos al salir de clase' CEIP Ciudad de Badajoz","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["802","Apoyo a la red de huertos escolares RETIRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["803","Apoyo a la red de huertos escolares TETU�N ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["804","Apoyo a la red de huertos escolares VIC�LVARO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["806","Todos a desayunar - Colegio Antonio Machado","Matriz","","pt_entity","Permanente","Valiente Bangla"],
            ["813","Apoyo en la ense�anza del castellano en las Escuelas de Adultos","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["968","Escuelas Deportivas Special Olympics - Aluche","Matriz","","pt_entity","Permanente","Special Olympics Madrid"],
            ["944","Gesti�n de demandas de necesidades b�sicas","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["967","Todos a desayunar - Colegio Eduardo Rojo ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["970","Apoyo a personas que sufren deterioro cognitivo ","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["972","Leemos para ti (CENTRO)","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["976","Apoyo a la red de huertos escolares BARAJAS","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["977","Apoyo a la red de huertos escolares C. LINEAL","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["978","Apoyo a la red de huertos escolares CHAMBER�","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["979","Apoyo a la red de huertos escolares CENTRO","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["982","Apoyo a la red de huertos escolares LATINA","Matriz","","pt_municipal","Permanente","pt_volunteer"],
            ["984","Escuelas Deportivas Special Olympics - Moratalaz","Matriz","","pt_entity","Permanente","Special Olympics Madrid"],
            ["986","Te Contamos La Ciudad ","Matriz","","pt_volunteer","Permanente","pt_volunteer"],
            ["1030","Todos a desayunar - Colegio Antonio Machado","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["1031","Todos a desayunar - Colegio M�ndez N��ez ","Matriz","","pt_entity","Permanente","pt_volunteer"],
            ["1088","Los Mayores tambi�n cuentan","Matriz","","pt_volunteer","Permanente","pt_volunteer"] 
        ].each do |data|
            begin 
                project = Project.find_by(id: data[0].to_i)

                if !project.blank?
                    project.subtype_pt = data[5].to_s== "Permanente" ? "permanent" : "punctual"
                    project.subtype_proyect = data[2].to_s=="Matriz" ? "matriz" : "subproyect"
                    if project.subtype_proyect == "subproyect" && !data[3].blank?
                        project.father_id = data[3].to_i
                    else
                        project.father_id = nil
                    end
                    project.project_type_id = ProjectType.find_by(description: data[4].to_s).try(:id)

                    pt_extendable = project.pt_extendable

                    case data[4].to_s
                    when "pt_social"
                        #if project.pt_extendable_type != "Pt::Social"
                            aux = Pt::Social.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_centre"
                        #if project.pt_extendable_type != "Pt::Centre"
                            aux = Pt::Centre.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_entity"
                        #if project.pt_extendable_type != "Pt::Entity"
                            aux = Pt::Entity.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_subvention"
                        #if project.pt_extendable_type != "Pt::Subvention"
                            aux = Pt::Subvention.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_other"
                        #if project.pt_extendable_type != "Pt::Other"
                            aux = Pt::Other.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_retired_volunteer"
                        #if project.pt_extendable_type != "Pt::RetiredVolunteer"
                            aux = Pt::RetiredVolunteer.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_pt_municipal"
                        #if project.pt_extendable_type != "Pt::pt_municipal"
                            aux = Pt::pt_municipal.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    when "pt_volunteer"
                        #if project.pt_extendable_type != "Pt::Volunteer"
                            aux = Pt::Volunteer.new(project: project, notes: pt_extendable.try(:notes))
                            aux.save(validate: false)
                            project.pt_extendable = aux
                            pt_extendable.destroy if !pt_extendable.blank?
                        #end
                    end

                    project.save(validate: false)

                    puts "Proyecto actualizado con id #{project.id}"

                end
            rescue
                puts "ERROR: Proyecto sin actualizar con id #{data[0]}"
            end
        end
    end
end