namespace :import_expedientes do
  desc "Importar expedientes de los voluntarios"
  task expedientes: :environment do
    puts "comienza la importación de los expedientes"
    i=0
    #Dir.glob('/home/iam/ExpedientesVoluntarios/PendientesDeImportar/*.pdf').each do |pathFileName|
     Dir.glob('/aytomad/app/volun/volun_backend/shared/private/system/links/ExpedientesVoluntarios/PendientesDeImportar/*.*').each do |pathFileName|

      file = File.new(pathFileName)

      identificator=  File.basename(pathFileName, ".pdf")
      identificator = identificator.to_i.to_s #Si el número tiene ceros delante lo convierto a int para quitarlos, y luego a string.
      puts identificator
      if identificator != "0"
        rutaFicheroOrigen =  pathFileName
        nombreFicheroOrigen = File.basename(pathFileName)
        start_nombreFichero = rutaFicheroOrigen.index(nombreFicheroOrigen)
        rutaSinFichero = rutaFicheroOrigen[0,start_nombreFichero]
        File.rename(rutaFicheroOrigen, rutaSinFichero + "expediente_" + identificator + ".pdf" )
        file = File.new(rutaSinFichero + "expediente_" + identificator + ".pdf")
        #File.rename(rutaFicheroOrigen, cadena + "expediente_" + identificator )

        #Compruebo si existe voluntario con ese id
        @volunteer = Volunteer.find_by(id: identificator)
        if @volunteer != nil
          puts "Voluntario encontrado"
          link = Link.new
          link.linkable_id = identificator
          link.link_type_id = 5
          link.description = "Expediente del voluntario"
          link.linkable_type = "Volunteer"
          link.file = file
          link.save
          #puts link.errors.messages
          #a = link.save
          #puts a

          #Muevo la imagen insertada a la carpeta de Importados, antes vuelvo a renombrar el fichero, para que quede con el nombre de inicio
          #File.rename(rutaSinFichero + "expediente_" + identificator + ".pdf", rutaFicheroOrigen)
          ##FileUtils.move(rutaFicheroOrigen, "/home/iam/ExpedientesVoluntarios/Importados")
          #FileUtils.move(pathFileName, "/aytomad/app/volun/volun_backend/shared/private/system/links/ExpedientesVoluntarios/Importados")

          #Como no hay espacio en el recurso, si todo ha ido bien, elimino el fichero en vez de moverlo
          FileUtils.rm(rutaSinFichero + "expediente_" + identificator + ".pdf")
        end
      end
    end
    puts "fin del proceso"

  end


end