namespace :associated do
	desc "Cambio de tipo de asociados"

    task :volunteers => :environment do
        puts "================================================="
        puts "Actualización de tipos de voluntarios"
        puts "================================================="
        
        ProjectsVolunteer.where(volunteer_type: nil).all.each do |pv|
            pv.volunteer_type="ASOCIATED"
            if pv.save  
                puts "Actualizado con id = #{pv.id}"
            else
                puts "ERROR: no se ha actualizado con id = #{pv.id}"
            end
        end
        puts "================================================="
    end

end