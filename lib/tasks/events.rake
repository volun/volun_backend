namespace :events do

    task :fix => :environment do
        events = Event.all
        arrayhash = Hash.new
        new_id = false
        puts "==================EVENTOS==============="
        events.each do |e|
            if !e.address_id.blank?
                if arrayhash.blank?
                    arrayhash.merge!({e.address_id => 1})
                elsif arrayhash[e.address_id].blank?
                    arrayhash.merge!({e.address_id => 1})
                else 
                    arrayhash[e.address_id]=  arrayhash[e.address_id]+1
                end
            end
           
        end
      
        events.each do |e|
            arrayhash.each do |k, v|
                if e.address_id == k && v > 1
                    e.address = e.address.amoeba_dup
                    if e.save
                        puts "Evento guardado correctamente #{e.id}"
                    end
                    break
                end
            end
        end
        puts "===================FIN=================="
    end
end