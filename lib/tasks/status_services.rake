namespace :status_services do
    desc "Rellenar los status_services"
    task fill: :environment do
        Service.status_type_list.map {|i| [ I18n.t("service.status.#{i}"),i]}.each do |ss|
            StatusService.create!(name: ss[0], code: ss[1].to_s)
        end
    end
    task update_services_statuses: :environment do
        CitizenService.where.not(status: nil).each do |cs|
            cs.update(status_service: StatusService.find_by(code: cs.status.to_s))
        end
    end
  
  end