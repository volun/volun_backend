require 'imports/citizens/citizens_importer'

namespace :citizens do
    require "net/http"
    require "uri"

    desc "Actualización de logo de ciudadanos"
    task :get_logo => :environment do
        Importers::CitizensImporter.new('./lib/imports/csv/citizens.csv').import!        
    end


    desc "Actualización de todos los voluntarios de base de datos"
    task :reset_password => :environment do
        
        emails = ["ciudadano_ios@yopmail.com", "juancgil@hotmail.com"]
        citizens = Citizen.where("email IN (?)", emails)
        if !citizens.blank?
            citizens.each do |citizen|
                if citizen.email_verified == true
                    if !citizen.try(:user).blank?
                        birthday = citizen.birthday.to_datetime          
                        pass = "#{citizen.name.first}" + "#{citizen.last_name_1.first}" + "#{birthday.to_date.year}" + "#{SecureRandom.hex(1)}"

                        citizen.user.password = pass
                        citizen.user.password_confirmation = pass
                        citizen.user.save
                        puts "="*20
                        puts "Usuario => #{citizen.email}"
                        puts "Contraseña => #{pass}"
                        puts "="*20
                        
                    else
                        puts "="*20
                        puts "No se ha encontrado el usuario para el ciudadano #{citizen.email}."
                        puts "="*20
                    end
                else
                    puts "="*20
                    puts "El ciudadano #{citizen.email} no ha verificado su cuenta."
                    puts "="*20
                end
            end
        else
            puts "="*20
            puts "No se han encontrado ciudadanos."
            puts "="*20
        end
    end

    desc "Envío de sms de advertencia"
    task :send_sms => :environment do
        CitizenService.all_active.where("status in ('active','prioritary') AND volunteer_id is null and send_sms_citizen= false 
            and  (date_request ||' '|| hour_request)::timestamp - '2 hr'::interval <  ?", Time.zone.now).each do |service|
            service.send_sms_citizen =  true
            message = "Lo sentimos. Su petición de acompañamiento no ha podido ser atendida por ninguna persona voluntaria. Gracias"
            begin 
                SMSApi.new.sms_deliver(service.citizen.phone, message)
                puts "Enviado SMS"
                service.save(validate: false)
            rescue Exception  => e
                Rails.logger.error('sendSms#congratulations') do
                    "Error sending sms: \n#{e}"
                end
                puts "Error sending sms: \n#{e}"
            end            
        end
    end

    desc "Carga de datos de padron"
    task :get_padron => :environment do
        Citizen.where("address_id is null OR birthday is null").each do |citizen|
            padron = Padron.new(citizen).search_citizien

            if !padron.blank? && !padron[:datos_habitante].blank? && !padron[:datos_habitante][:item].blank?
                now = Time.zone.now.to_date
                date_bithday = Time.zone.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string].to_s)
                citizen.name = padron[:datos_habitante][:item][:nombre]
                citizen.last_name_1 = padron[:datos_habitante][:item][:apellido1]
                citizen.last_name_2 = padron[:datos_habitante][:item][:apellido2]
                citizen.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
                citizen.birthday = padron[:datos_habitante][:item][:fecha_nacimiento_string].blank? ? '' : Time.zone.parse(padron[:datos_habitante][:item][:fecha_nacimiento_string]).strftime("%d/%m/%Y")
                citizen.academic_info = padron[:datos_habitante][:item][:descripcion_nivel_instruccion]
                citizen.gender = padron[:datos_habitante][:item][:descripcion_sexo]
                citizen.people_at_home = padron[:datos_vivienda][:item][:num_personas]
                citizen.start_date = padron[:datos_vivienda][:item][:fecha_alta_inscripcion_string]
                citizen.phone = padron[:datos_vivienda][:item][:telefono].blank? ? citizen.phone : padron[:datos_vivienda][:item][:telefono]
    
                address = Address.new()
                
                address.normalize = false
                address.road_type = padron[:datos_vivienda][:item][:sigla_via]
                address.road_name = padron[:datos_vivienda][:item][:nombre_via]
                address.road_number = padron[:datos_vivienda][:item][:numero_via]
                address.road_number_type = padron[:datos_vivienda][:item][:nominal_via]
                address.country = padron[:datos_habitante][:item][:desc_pais_nacimiento]
                address.town = padron[:datos_habitante][:item][:desc_municipio_nacimiento].blank? ? "Madrid" : padron[:datos_habitante][:item][:desc_municipio_nacimiento]
                address.town_code = padron[:datos_habitante][:item][:codigo_municipio_nacimiento].blank? ? "" : padron[:datos_habitante][:item][:codigo_municipio_nacimiento]

                address.province = padron[:datos_habitante][:item][:desc_provincia_nacimiento].blank? ? "Madrid" : padron[:datos_habitante][:item][:desc_provincia_nacimiento]
                address.floor = padron[:datos_vivienda][:item][:planta]
                address.door = padron[:datos_vivienda][:item][:puerta]
                address.borough = padron[:datos_vivienda][:item][:nombre_barrio]
                address.borough_code = padron[:datos_vivienda][:item][:codigo_barrio]
                address.district = padron[:datos_vivienda][:item][:nombre_distrito]
                address.district_code = padron[:datos_vivienda][:item][:codigo_distrito]
                address.postal_code = padron[:datos_vivienda][:item][:codigo_postal]
    
                    
                citizen.address=  address
                    puts "Success: El ciudadano con id #{citizen.id} se ha actualizado"
                if citizen.save
                else
                    puts "Error: El ciudadano con id #{citizen.id} no se ha podido actualizar"
                end
            else
                puts "Error: El ciudadano con id #{citizen.id} no tiene datos de padron"
            end
        end
    end


    desc "Carga de datos de padron"
    task :azure => :environment do
        url = URI("#{Rails.application.secrets.azure_mta_api}")          
        Net::HTTP.start(url.hostname, url.port, :use_ssl => url.scheme == 'https') do |http|
            request = Net::HTTP::Post.new(url,{'Content-type' => 'application/json; charset=UTF-8'})
            request.body={
            "email": "prueba@prueba.es",
            "document": "11709376F",
            "documentType": 1,
            "phone": "618844036",
            "name": "Pruebas Azure GI",
            "privacyTermsAccepted": true,
            "birthday": "28/10/1989",
            "birthplace": 0
            }.to_json
            response = http.request(request)
            if !response.body.blank?
            begin
                Rails.logger.error("ERROR-WS-AZURE: citizen -> No se ha podido crear el ciudadano en Azure")
            rescue
            end
            puts "="*50
            puts response.body
            end
        end
    end

    desc "Aviso a mayores de eliminación y posterior eliminación"
    task :citizens_removal => :environment do

        Citizen.where("EXTRACT(day from NOW()-cast(created_at as date)) > 30 and removal_notice = true and removal_notice_at is not null").where(internal_management: false).each do |cit|
            if (Date.today - cit.removal_notice_at).round >= 7
                cit.delete 
                p "#{cit.full_name} eliminado"
            end
        end

        Citizen.where("EXTRACT(day from NOW()-cast(created_at as date)) > 30").where(email_verified: false, removal_notice: false).where(internal_management: false).each do |cit|
            begin
                SMSApi.new.sms_deliver(cit.phone, "Hemos detectado que tu proceso de registro en la aplicación 'Madrid te acompaña' está incompleto. Te invitamos a que intentes registrarte de nuevo para poder acceder a la aplicación móvil. Si tienes cualquier duda, o simplemente la informática no es lo tuyo, el Equipo de Atención al Voluntariado te apoyará en lo que necesites. Puedes llamarnos al 900 777 888.")
                cit.update(removal_notice: true, removal_notice_at: Date.today)
                p "SMS de aviso enviado al mayor #{cit.full_name}"
            rescue =>error
                begin
                    Rails.logger.error("ERROR-SMS-citizens_removal -> #{error}")
                    next
                rescue
                end
            end 
        end
    end
end