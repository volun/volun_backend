namespace :project do

    task :set_year => :environment do  
        projects = Project.where(year: nil)

        if !projects.blank?
            projects.each do |project|
                project.year = project.execution_start_date.year
                if project.save
                    puts "="*50
                    puts "Proyecto: #{project.id} actualizado."
                    puts "="*50
                else
                    puts "="*50
                    puts "No se pudo guardar el año del proyecto #{project.id}: #{project.errors.full_messages}."
                    puts "="*50
                end
            end               
            
        else
            puts "="*50
            puts "No se encontraron proyectos con el campo 'year' vacío."
            puts "="*50
        end
    end
end