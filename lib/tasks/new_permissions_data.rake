namespace :new_permissions_data do
    desc "Add and remove data"
    task data: :environment do
        Manager.all.each do |manager|
            puts "=============================================="
            puts "PERMISOS DE MANAGER #{manager}"
            puts "=============================================="
            manager.permissions.each do |permission|
                Permission.auth_permissions.each do |resource|
                    if permission.section.blank? 
                        #eliminar el registro duplicado
                        if permission.destroy
                            puts "- PERMISO #{permission.id}(#{permission.resource_id}) - SIN SECTION ELIMINADO"
                        end
                        break
                    elsif is_permission_duplicated?(resource, manager)
                        aux=permissions_duplicated(resource, manager)
                        if !aux.blank? && permission.id != aux.id && permission.section == resource
                            if validate_fields(permission)
                                aux.data = permission.data
                                aux.resource_id=nil
                                if aux.save
                                    if permission.destroy
                                        puts "- PERMISO #{permission.id}(#{permission.resource_id}) - #{permission.section} - DUPLICADO -> ELIMINADO"
                                    end
                                    break
                                end
                            else
                                permission.data = aux.data
                                permission.resource_id=nil
                                if permission.save
                                    if aux.destroy
                                        puts "- PERMISO #{aux.id}(#{aux.resource_id}) - #{aux.section} - DUPLICADO -> ELIMINADO"
                                    end
                                    break
                                end 
                            end
                        end
                    end
                end
            end
        end
    end

    task data_info: :environment do
        Manager.all.each do |manager|
            puts "=============================================="
            puts "PERMISOS DE MANAGER #{manager}"
            puts "=============================================="
            manager.permissions.each do |permission|
                Permission.auth_permissions.each do |resource|
                    if permission.section.blank? 
                        puts "- PERMISO #{permission.id}(#{permission.resource_id}) - SIN SECTION ELIMINADO"
                        break
                    elsif is_permission_duplicated?(resource, manager)
                        aux=permissions_duplicated(resource, manager)
                        if !aux.blank? && permission.id != aux.id && permission.section == resource
                            if validate_fields(permission)
                                puts "- PERMISO #{permission.id}(#{permission.resource_id}) - #{permission.section} - DUPLICADO -> ELIMINADO"
                                break
                            else
                                puts "- PERMISO #{aux.id}(#{aux.resource_id}) - #{aux.section} - DUPLICADO -> ELIMINADO"
                                break
                            end
                        end
                    end
                end
            end
        end
    end

    private
    def all_false(p)
        !p.can_destroy && !p.can_create && !p.can_update && !p.can_read && !p.can_process_request_form && !p.can_pre_approve_request_form && !p.can_reject_request_form && !p.can_mark_request_form_as_processing && !p.can_mark_request_form_as_pending && !p.can_mark_request_form_as_appointed
    end

    def is_permission_duplicated?(resource, manager)
        Permission.where(section: resource, manager: manager).count > 1
    end

    def permissions_duplicated(resource, manager)
        Permission.where(section: resource, manager: manager).first
    end

    def validate_fields(p)
        resource = p.section
        case resource
        when "Volunteer"    
            #puts "======================Voluntarios #{resource}======================="
            ['create','update','read','destroy'].each do |aux|
               return true if !p.data['basics'].blank? && !p.data['basics'][aux].blank? && p.data['basics'][aux].to_s == "1"
            end

            ['mail','sms'].each do |aux|
                return true if !p.data['specifics'].blank? && !p.data['specifics'][aux].blank? && p.data['specifics'][aux].to_s == "1"
            end

        when "Project"    
            #puts "======================Proyectos #{resource}======================="
            ['create','update','read','destroy'].each do |aux|
                return true if !p.data['basics'].blank? && !p.data['basics'][aux].blank? && p.data['basics'][aux].to_s == "1"
             end
 
            ['clone_project','volunteer_allowed','publish'].each do |aux|
                return true if !p.data['specifics'].blank? && !p.data['specifics'][aux].blank? && p.data['specifics'][aux].to_s == "1"
            end
        
        when "Entity", "Activity", "Citizen", "CitizenService" 
            #puts "======================Entidades======================="
            #puts "======================Actividades=======================" 
            #puts "======================Ciudadanos======================="
            #puts "======================Servicios del ciudadano=======================" 
            ['create','update','read','destroy'].each do |aux|
                return true if !p.data[aux].blank? && p.data[aux].to_s == "1"
            end
        end

        if !resource.blank? && resource.split("::")[1] == "Tracking"
            #puts "======================Seguimientos======================="
            ['create','update','read'].each do |aux|
                return true if !p.data[aux].blank? && p.data[aux].to_s == "1"
            end
        elsif  !resource.blank? && resource.split("::")[0] == "Rt"
            #puts "======================Solicitudes======================="
            ['read','process','approve','reject','mark_processing','mark_pending','mark_appointed'].each do |aux|
                return true if !p.data[aux].blank? && p.data[aux].to_s == "1"
            end
        elsif !resource.blank? && resource == "Administration"
            ['audit','resource','catalogs','new_campaign','managers','config','order','analyze_cv'].each do |aux|
                return true if !p.data[aux].blank? && p.data[aux].to_s == "1"
            end
        end
        false
    end

end