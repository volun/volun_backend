namespace :import_data do
    desc "Carga de datos de sms masivo"
    task :add_sms_massive => :environment do 
        MassiveSmsHistoric.all.find_each do |hsms|
            hsms.specific="citizen"
            hsms.save(validate: false)
        end
    end

    task :vc_types => :environment do
      [
          ["Normal","normal"],
          ["Formulario","forms"],
          ["Acceso","access_login"],
          ["Principal","primary"],
          ["Subcomponente","subcomponent"]
      ].each do |c|
          VirtualCommunity::ComponentType.find_or_create_by(title: c[0], code: c[1], active: true)
      end
  end

  task :vc_components => :environment do    
    parent = VirtualCommunity::Component.find_or_create_by(denomination: "Homepage", component_type: VirtualCommunity::ComponentType.find_by(code: "primary"),title: 'Comunidad Virtual del Voluntariado y la solidaridad en la Ciudad de Madrid', button_active: false, order: 1)
    VirtualCommunity::Component.find_or_create_by(denomination: "¿Quiénes somos?",component_type: VirtualCommunity::ComponentType.find_by(code: "normal"),title: '¿Quiénes somos?',content: "<p class='text-justify'>Voluntari@s por Madrid es el programa municipal de voluntariado. El Departamento de Voluntariado pertenece al Área de Familias, Igualdad y Bienestar Social  del Ayuntamiento de Madrid.</p><p>
    Voluntari@s por Madrid cuenta con un cuerpo de voluntarios y voluntarias propio. Se puede participar cuándo y cómo  se quiera y en aquéllas actividades que más interesan. Además, realizamos numerosos proyectos y acciones solidarias en todos los ámbitos de la vida social de la ciudad.</p>
    <hr>
    <h3 class='dataGroup'>Puedes contactar con nosotros en: </h3>
    <h4>Departamento de Voluntariado</h4>
    <p class='data'>C/ Alcalá, 45, planta baja. 28045 Madrid</p>
    <p class='data'>Teléfono: +34 900 777 888</p>
    <p class='data'>Atención a Voluntarios y Voluntarias: De 9 a 18 horas de lunes a jueves y de 9 a 15 horas los viernes</p>
    <p class='data'>Correo electrónico: <a href='mailto: comunidadvoluntariad@madrid.es' alt='Correo de contacto'>comunidadvoluntariad@madrid.es</a></p>
    <h3 class='dataGroup text-justify'>También puedes seguir toda nuestra actividad a través de estas redes sociales:</h3>
    ",button_active: false, order: 2)
    VirtualCommunity::Component.find_or_create_by(denomination: "¿Qué puedes hacer tú?",component_type: VirtualCommunity::ComponentType.find_by(code: "access_login"), title: '¿Qué puedes hacer tú?', content: '<p>Voluntarios por Madrid es el instrumento de participaci&oacute;n ciudadana activa de los madrile&ntilde;os y madrile&ntilde;as en la ciudad.</p>

    <p>Ser voluntario/a por Madrid es participar en lo que m&aacute;s te gusta: programas sociales, culturales, medioambientales, deportivos, con animales&hellip; Como quieras y cuando quieras.</p>
    
    <p>Ser voluntario/a por Madrid es pertenecer a un equipo de personas solidarias y positivas donde crear v&iacute;nculos de amistad y compartir emociones.</p>
    <p>&nbsp;</p>    
    <h3 class="titlePage" style="text-align: center;"><strong>Hazte voluntario/a</strong></h3>    
    <p style="text-align: center;">Si te sientes una persona solidaria y te apetece hacer algo por los dem&aacute;s, puedes hacer mucho m&aacute;s de lo que crees por tu ciudad, por tus vecinos, por tu entorno. En Voluntarios por Madrid te indicamos c&oacute;mo.</p>
    ', button_active: true, button_title: 'Quiero ser voluntario/a', button_alt_title: 'Quiero ser voluntario/a', order: 3)
    VirtualCommunity::Component.find_or_create_by(denomination: "Entidades colaboradoras", component_type: VirtualCommunity::ComponentType.find_by(code: "access_login"),title:'Entidades colaboradoras', content: '<p>En el Programa Municipal de Voluntariado colaboramos con las entidades sociales sin &aacute;nimo de lucro en la realizaci&oacute;n de proyectos conjuntos y proporcionamos los voluntarios y las voluntarias que necesiten para desarrollar sus proyectos solidarios.</p>

    <div style="text-align: center; margin-top: 5rem">
        <h3 class="titlePage">¿Necesitas voluntarios/as?</h3>
            <h3 class="titlePage">¿Quieres publicar un proyecto?</h3>
            <h3 class="titlePage">¿Quieres difundir una actividad?</h3>
            <h3 class="titlePage">Otras</h3>
            <p>Si eres una entidad ponte en contacto con nosotros.</p>
     </div>
    
    ', button_active: true, button_title: 'Hazte entidad colaboradora', button_alt_title: 'Hazte entidad colaboradora', order: 4)
    VirtualCommunity::Component.find_or_create_by(denomination: "Solicitud de información sobre voluntariado", component_type: VirtualCommunity::ComponentType.find_by(code: "forms"),content: '<p style="margin-top: 3rem">Desde el Programa Municipal de Voluntariado del Ayuntamiento &ldquo;Voluntari@s por Madrid&rdquo;, te ofrecemos toda la informaci&oacute;n sobre voluntariado de la ciudad de Madrid, as&iacute; como sobre los proyectos de voluntariado en los que puede colaborar. Datos de contacto: 900 777 888 (tel&eacute;fono gratuito). Horario de atenci&oacute;n: lunes a jueves de 9:00 a 18:00 h y viernes de 9:00 a 15:00 h. Correo electr&oacute;nico:&nbsp; <a href="mailto:voluntariospormadrid@madrid.es">voluntariospormadrid@madrid.es</a> &nbsp;y&nbsp; <a href="mailto:comunidadvoluntariad@madrid.es">comunidadvoluntariad@madrid.es</a></p>

    <p style="margin-top: 3rem"><span style="font-size:10px;"><strong>* Aviso legal</strong></span></p>
    
    <p><span style="font-size:10px;">Los datos recabados ser&aacute;n incorporados y tratados en la actividad de tratamiento &ldquo;Gesti&oacute;n de voluntarios&rdquo;, responsabilidad de la Coordinaci&oacute;n General de Familias, Igualdad y Bienestar Social, con domicilio en el Paseo de la Chopera 41 (28045 Madrid), correo electr&oacute;nico<a href="mailto: cgbienestarsocial@madrid.es"> cgbienestarsocial@madrid.es</a> y tel&eacute;fono 91 588 89 24.</span></p>
    
    <p><span style="font-size:10px;">Los datos se recaban con la finalidad de organizaci&oacute;n y seguimiento de las personas voluntarias y de las actividades de voluntariado que se desarrollan. Los datos proporcionados se conservar&aacute;n con la finalidad descrita mientras no se oponga a ello o solicite la baja como voluntario/a y la cancelaci&oacute;n de sus datos. Una vez solicitada la cancelaci&oacute;n de sus datos, estos ser&aacute;n bloqueados durante el plazo de 5 a&ntilde;os, a disposici&oacute;n &uacute;nicamente de Administraciones P&uacute;blicas, Juzgados y Tribunales para atender a posibles reclamaciones, tras los cuales, dichos datos ser&aacute;n suprimidos. No ser&aacute;n utilizados para elaborar decisiones automatizadas respecto a la acci&oacute;n voluntaria.</span></p>
    
    <p><span style="font-size:10px;">El tratamiento de datos queda legitimado mediante el consentimiento de las personas interesadas. La base legal para el tratamiento de los datos se enmarca en el contexto de la Ley 45/2015, de 14 de octubre, del Voluntariado y la Ley 1/ 2015 de 24 de febrero, que establece como objeto el de promover y fortalecer el voluntariado, as&iacute; como crear el marco id&oacute;neo para que las relaciones que puedan establecerse entre los destinatarios de las actividades de voluntariado, los voluntarios, las organizaciones de voluntariado y las administraciones p&uacute;blicas contribuyan de manera &oacute;ptima al bienestar social.</span></p>
    
    <p><span style="font-size:10px;">Los datos no podr&aacute;n ser cedidos a terceros, excepto cuando hayan sido autorizados por el usuario, o la informaci&oacute;n sea requerida por la autoridad judicial, ministerio fiscal o la polic&iacute;a judicial, o la misma venga prevista en la ley.</span></p>
    
    <p><span style="font-size:10px;">Cualquier persona tiene derecho a obtener confirmaci&oacute;n sobre si en la Coordinaci&oacute;n General de Familias, Igualdad y Bienestar Social se est&aacute;n tratando datos personales que les conciernan, o no. Las personas interesadas tienen derecho a acceder a sus datos personales, as&iacute; como a solicitar la rectificaci&oacute;n de los datos inexactos o, en su caso, solicitar su supresi&oacute;n cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.</span></p>
    
    <p><span style="font-size:10px;">Para ello las solicitudes pueden dirigirse a la Coordinaci&oacute;n General de Familias, Igualdad y Bienestar Social (con domicilio en el Paseo de la Chopera 41, 28045 Madrid, correo electr&oacute;nico cgbienestarsocial@madrid.es).</span></p>
    
    <p><span style="font-size:10px;">En determinadas circunstancias, las personas interesadas podr&aacute;n solicitar la limitaci&oacute;n del tratamiento de sus datos, en cuyo caso &uacute;nicamente se conservar&aacute;n para el ejercicio o la defensa de reclamaciones. Tambi&eacute;n por motivos relacionados con su situaci&oacute;n particular, los interesados podr&aacute;n oponerse al tratamiento de sus datos. El responsable del tratamiento dejar&aacute; de tratar los datos, salvo por motivos leg&iacute;timos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.</span></p>
    
    <p><span style="font-size:10px;">Asimismo, tiene derecho a retirar el consentimiento otorgado, en cuyo caso ser&aacute; efectivo desde el momento en el que lo solicite, sin tener efectos retroactivos, y derecho, sin tener efectos retroactivos, y derecho a reclamar ante la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos.</span></p>
    
    ', button_active: false, order: 5)
    VirtualCommunity::Component.find_or_create_by(denomination: "Proyectos destacados", component_type: VirtualCommunity::ComponentType.find_by(code: "subcomponent"),title: 'Proyectos destacados', button_active: false, order: 6, component_parent: parent)
    VirtualCommunity::Component.find_or_create_by(denomination: "Estadísticas de la comunidad", component_type: VirtualCommunity::ComponentType.find_by(code: "subcomponent"),title: 'Estadísticas de la comunidad', button_active: false, order: 7, component_parent: parent)
    VirtualCommunity::Component.find_or_create_by(denomination: "Agenda de actividades solidarias", component_type: VirtualCommunity::ComponentType.find_by(code: "subcomponent"), title: 'Agenda de actividades solidarias', button_active: false, order: 8, component_parent: parent)
    VirtualCommunity::Component.find_or_create_by(denomination: "Redes sociales", component_type: VirtualCommunity::ComponentType.find_by(code: "subcomponent"), button_active: false, order: 9, component_parent: parent)
    VirtualCommunity::Component.find_or_create_by(denomination: "Mapa", component_type: VirtualCommunity::ComponentType.find_by(code: "subcomponent"), button_active: false, order: 10, component_parent: parent)
    VirtualCommunity::Component.find_or_create_by(denomination: "Condiciones de uso",component_type: VirtualCommunity::ComponentType.find_by(code: "normal"),title: 'Condiciones de uso', content: '<h2>AVISO LEGAL SOBRE LAS CONDICIONES DE USO, PRIVACIDAD Y PROTECCI&Oacute;N DE DATOS PERSONALES DEL PORTAL DE GOBIERNO ABIERTO DEL AYUNTAMIENTO DE MADRID</h2>

    <h3>1. &Aacute;mbito de aplicaci&oacute;n de las condiciones de uso</h3>
    
    <p>La participaci&oacute;n (entendi&eacute;ndola como participaci&oacute;n activa, m&aacute;s all&aacute; de la lectura del contenido presente) en la Comunidad Virtual de Voluntariado del Ayuntamiento de Madrid se regula por las presentes condiciones de uso que vinculan a todas las personas que participen en este sitio web. Por ello, cualquier persona que desee participar deber&aacute; registrarse, a cuyo fin se solicitar&aacute; la aceptaci&oacute;n de estas condiciones de uso.</p>
    
    <p>El Ayuntamiento de Madrid se reserva la facultad de modificar las presentes condiciones de uso para la participaci&oacute;n en el Portal de Gobierno Abierto del Ayuntamiento de Madrid, cuya &uacute;ltima versi&oacute;n se publicar&aacute; en este sitio web.</p>
    
    <p>El Ayuntamiento de Madrid se reserva la facultad de modificar las presentes condiciones de uso para la participaci&oacute;n en la Comunidad Virtual de Voluntariado del Ayuntamiento de Madrid, cuya &uacute;ltima versi&oacute;n se publicar&aacute; en este sitio web.</p>
    
    <h3>2. Objetivo de la iniciativa</h3>
    
    <p>A trav&eacute;s de la Comunidad Virtual de Voluntariado, el Ayuntamiento de Madrid quiere fomentar la participaci&oacute;n de los voluntarios facilitando una herramienta que ponga a su disposici&oacute;n toda la informaci&oacute;n necesaria para participar activamente en la vida de la ciudad. Apostamos por que cada voluntario tenga a su alcance la informaci&oacute;n sobre los proyectos y/o actividades que m&aacute;s se ajusten a sus inquietudes, expectativas e intereses.</p>
    
    <h3>3. Participaci&oacute;n de menores de edad en la Comunidad Virtual de Voluntariado del Ayuntamiento de Madrid</h3>
    
    <p>Podr&aacute; darse de alta v&aacute;lidamente cualquier persona f&iacute;sica a partir de los 16 a&ntilde;os. Mediante la aceptaci&oacute;n de estas condiciones de uso se declara tener 16 a&ntilde;os o m&aacute;s. Los menores de edad dados de alta en la comunidad virtual ser&aacute;n citados a una entrevista presencial en la sede de Voluntarios por Madrid. Para que puedan ser dados de alta como voluntarios ser&aacute; precisa la autorizaci&oacute;n del padre, madre o tutor/a en los t&eacute;rminos establecidos en los art&iacute;culos 8.2 a) de la Ley 45/2015, de 14 de octubre, de Voluntariado y en el art&iacute;culo 5.2 de la Ley 1/2015, de 24 de febrero, del Voluntariado en la Comunidad de Madrid.</p>
    
    <h3>4. Obligaciones de los usuarios de la Comunidad Virtual de Voluntariado</h3>
    
    <p>Al ser el Ayuntamiento de Madrid un punto de encuentro cuyo objetivo es fomentar e incrementar la participaci&oacute;n activa, los usuarios est&aacute;n obligados a hacer un uso diligente y acorde a dicho objetivo.</p>
    
    <p>El Ayuntamiento de Madrid no es responsable de los datos inexactos, incompletos o falsos que sean aportados por los usuarios, siendo &eacute;stos responsables de la veracidad de los mismos. En caso de litigio de cualquier clase o por cualquier motivo entre los participantes en el sitio web y/o un tercero, el Ayuntamiento de Madrid quedar&aacute; exento de cualquier responsabilidad por reclamaciones, demandas o da&ntilde;os de cualquier naturaleza relacionados o derivados del litigio.</p>
    
    <h3>5. Uso de la Comunidad Virtual de Voluntariado</h3>
    
    <p>Los participantes podr&aacute;n acceder y navegar por la Comunidad Virtual de Voluntariado libremente y de forma an&oacute;nima. Se puede acceder introduciendo credenciales a la comunidad como persona f&iacute;sica o como entidad social.</p>
    
    <p>S&oacute;lo se solicitar&aacute;n credenciales a personas f&iacute;sicas para las siguientes acciones:</p>
    
    <ul>
      <li>Cambiar contrase&ntilde;a</li>
      <li>Modificaci&oacute;n de perfil</li>
      <li>Solicitud de cita</li>
    </ul>
    
    <p>Para ello ser&aacute; necesario un registro previo en el que se solicitar&aacute;n nombre, apellidos, tel&eacute;fono y correo electr&oacute;nico.</p>
    
    <p>En el caso de las entidades sociales se solicitar&aacute;n credenciales, previo registro, para las siguientes acciones:</p>
    
    <ul>
      <li>Cambiar contrase&ntilde;a</li>
      <li>Difusi&oacute;n de actividad en agenda</li>
      <li>Demanda de voluntarios</li>
      <li>Publicaci&oacute;n de proyecto</li>
      <li>Otra solicitud</li>
    </ul>
    
    <h3>6. Finalidad del tratamiento de los datos proporcionados por los usuarios</h3>
    
    <p>1.- Respecto de las personas f&iacute;sicas:</p>
    
    <p>Los datos inicialmente facilitados por personas f&iacute;sicas tendr&aacute;n la finalidad de concertar una cita para una entrevista presencial, en la que se podr&aacute; tramitar&aacute; o no su alta en el Programa Municipal de Voluntariado.</p>
    
    <p>Los datos facilitados de manera sucesiva, una vez se ha efectuado el registro previo, tendr&aacute;n como finalidad modificar sus datos, concertar una cita personalizada y, con car&aacute;cter general, la organizaci&oacute;n y gesti&oacute;n de las personas pertenecientes al cuerpo de Voluntarios por Madrid.</p>
    
    <p>2.- Respecto de las entidades sociales:</p>
    
    <p>Los datos inicialmente facilitados por entidades sociales tendr&aacute;n como finalidad establecer un contacto con la entidad, bien por v&iacute;a telef&oacute;nica o correo electr&oacute;nico, o bien concertando una reuni&oacute;n para establecer los cauces de la colaboraci&oacute;n.</p>
    
    <p>Los datos facilitados de manera sucesiva, una vez se ha efectuado el registro previo, podr&aacute;n tener como finalidad modificar sus datos, recibir solicitudes de voluntarios, de difusi&oacute;n de una actividad en la agenda, de publicaci&oacute;n de proyecto u otras solicitudes.</p>
    
    <h3>7. Pol&iacute;tica de privacidad y protecci&oacute;n de datos</h3>
    
    <p>Los datos recabados ser&aacute;n incorporados y tratados en la actividad de tratamiento &ldquo;Gesti&oacute;n de voluntarios&rdquo;, responsabilidad de la Direcci&oacute;n General de Participaci&oacute;n Ciudadana, con domicilio en la calle Alcal&aacute; 45 (28014 Madrid), correo electr&oacute;nico &ldquo;dgpartciudadana@madrid.es&rdquo; y tel&eacute;fono 91 513 35 68.</p>
    
    <p>Los datos se recaban con la finalidad de organizaci&oacute;n y seguimiento de las personas voluntarias y de las actividades de voluntariado que se desarrollan. Los datos proporcionados se conservar&aacute;n con la finalidad descrita mientras no se oponga a ello o solicite la baja como voluntario/a y la cancelaci&oacute;n de sus datos. Una vez solicitada la cancelaci&oacute;n de sus datos, estos ser&aacute;n bloqueados durante el plazo de 5 a&ntilde;os, a disposici&oacute;n &uacute;nicamente de Administraciones P&uacute;blicas, Juzgados y Tribunales para atender a posibles reclamaciones, tras los cuales, dichos datos ser&aacute;n suprimidos. No ser&aacute;n utilizados para elaborar decisiones automatizadas respecto a la acci&oacute;n voluntaria.</p>
    
    <p>El tratamiento de datos queda legitimado mediante el consentimiento de las personas interesadas. La base legal para el tratamiento de los datos se enmarca en el contexto de la Ley 45/2015, de 14 de octubre, del Voluntariado y la Ley 1/ 2015 de 24 de febrero, que establece como objeto el de promover y fortalecer el voluntariado, as&iacute; como crear el marco id&oacute;neo para que las relaciones que puedan establecerse entre los destinatarios de las actividades de voluntariado, los voluntarios, las organizaciones de voluntariado y las administraciones p&uacute;blicas contribuyan de manera &oacute;ptima al bienestar social.</p>
    
    <p>Los datos no podr&aacute;n ser cedidos a terceros, excepto cuando hayan sido autorizados por el usuario, o la informaci&oacute;n sea requerida por la autoridad judicial, ministerio fiscal o la polic&iacute;a judicial, o la misma venga prevista en la ley.</p>
    
    <p>Cualquier persona tiene derecho a obtener confirmaci&oacute;n sobre si en la Direcci&oacute;n General de Participaci&oacute;n Ciudadana se est&aacute;n tratando datos personales que les conciernan, o no. Las personas interesadas tienen derecho a acceder a sus datos personales, as&iacute; como a solicitar la rectificaci&oacute;n de los datos inexactos o, en su caso, solicitar su supresi&oacute;n cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.</p>
    
    <p>Para ello las solicitudes pueden dirigirse a la Direcci&oacute;n General de Participaci&oacute;n Ciudadana (con domicilio en la calle Alcal&aacute; 45, 28014 Madrid, correo electr&oacute;nico dgpartciudadana@madrid.es).</p>
    
    <p>En determinadas circunstancias, las personas interesadas podr&aacute;n solicitar la limitaci&oacute;n del tratamiento de sus datos, en cuyo caso &uacute;nicamente se conservar&aacute;n para el ejercicio o la defensa de reclamaciones. Tambi&eacute;n por motivos relacionados con su situaci&oacute;n particular, los interesados podr&aacute;n oponerse al tratamiento de sus datos. El responsable del tratamiento dejar&aacute; de tratar los datos, salvo por motivos leg&iacute;timos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.</p>
    
    <p>Asimismo, tiene derecho a retirar el consentimiento otorgado, en cuyo caso ser&aacute; efectivo desde el momento en el que lo solicite, sin tener efectos retroactivos, y derecho a reclamar ante la Agencia Espa&ntilde;ola de Protecci&oacute;n de Datos.</p>
    
    <h3>8. Normativa aplicable</h3>
    
    <p>Las normas del ordenamiento jur&iacute;dico espa&ntilde;ol rigen de manera exclusiva estas condiciones de uso. Cualquier disputa, controversia o reclamaci&oacute;n derivada de estas condiciones de uso, o el incumplimiento, rescisi&oacute;n o invalidaci&oacute;n de estas, se resolver&aacute;n exclusivamente ante los juzgados competentes.</p>
    
    <h3>9. Revisi&oacute;n de las condiciones de uso</h3>
    
    <p>El Ayuntamiento de Madrid se reserva el derecho de revisar las presentes condiciones de uso y la pol&iacute;tica de privacidad en cualquier momento y por cualquier raz&oacute;n. En dicho caso, los usuarios registrados ser&aacute;n avisados a trav&eacute;s de este espacio en l&iacute;nea y, si contin&uacute;an utilizando el Portal de Gobierno Abierto, se entender&aacute;n aceptadas las modificaciones introducidas.</p>    
    ', button_active: false, order: 11)
    VirtualCommunity::Component.find_or_create_by(denomination: "Accesibilidad",component_type: VirtualCommunity::ComponentType.find_by(code: "normal"), title: 'Accesibilidad', content: '<p>La accesibilidad web se refiere a la posibilidad de acceso a la web y a sus contenidos por todas las personas, independientemente de las discapacidades (f&iacute;sicas, intelectuales o t&eacute;cnicas) que puedan presentar o de las que se deriven del contexto de uso (tecnol&oacute;gicas o ambientales).</p>

    <p>Cuando los sitios web est&aacute;n dise&ntilde;ados pensando en la accesibilidad, todos los usuarios pueden acceder en condiciones de igualdad a los contenidos, por ejemplo:</p>
    
    <ul>
      <li>Proporcionando un texto alternativo a las im&aacute;genes, los usuarios invidentes o con problemas de visi&oacute;n pueden utilizar lectores especiales para acceder a la informaci&oacute;n.</li>
      <li>Cuando los v&iacute;deos disponen de subt&iacute;tulos, los usuarios con dificultades auditivas pueden entenderlos plenamente.</li>
      <li>Si los contenidos est&aacute;n escritos en un lenguaje sencillo e ilustrados, los usuarios con problemas de aprendizaje est&aacute;n en mejores condiciones de entenderlos.</li>
      <li>Si el usuario tiene problemas de movilidad y le cuesta usar el rat&oacute;n, las alternativas con el teclado le ayudan en la navegaci&oacute;n.</li>
    </ul>
    
    <p>&nbsp;</p>
    
    <h2>&quot;Atajos&quot; de teclado</h2>
    
    <p>Para poder navegar por este sitio web de forma accesible, se han programado un grupo de teclas de acceso r&aacute;pido que recogen las principales secciones de inter&eacute;s general en los que est&aacute; organizado el sitio.</p>
    
    <h3 style="text-align: center;">Atajos de teclado para el men&uacute; de navegaci&oacute;n</h3>
    
    <table border="1" cellpadding="1" cellspacing="1" style="width:100%;">
      <tbody>
        <tr>
          <td><strong>Tecla</strong></td>
          <td><strong>P&aacute;gina</strong></td>
        </tr>
        <tr>
          <td>1</td>
          <td>Inicio</td>
        </tr>
        <tr>
          <td>2</td>
          <td>&iquest;Qui&eacute;nes somos?</td>
        </tr>
        <tr>
          <td>3</td>
          <td>&iquest;Qu&eacute; puedes hacer?</td>
        </tr>
        <tr>
          <td>4</td>
          <td>Proyectos</td>
        </tr>
        <tr>
          <td>5</td>
          <td>Entidades colaboradoras</td>
        </tr>
        <tr>
          <td>6</td>
          <td>Campa&ntilde;as</td>
        </tr>
        <tr>
          <td>7</td>
          <td>Agenda</td>
        </tr>
      </tbody>
    </table>
    
    <p>Dependiendo del sistema operativo y del navegador que se utilice, la combinaci&oacute;n de teclas ser&aacute; la siguiente:</p>
    
    <h3 style="text-align: center;">Combinaci&oacute;n de teclas dependiendo del sistema operativo y navegador</h3>
    
    <table border="1" cellpadding="1" cellspacing="1" style="width:100%">
      <tbody>
        <tr>
          <td><strong>Navegador</strong></td>
          <td><strong>Combinaci&oacute;n</strong></td>
        </tr>
        <tr>
          <td>Explorer</td>
          <td>Inicio</td>
        </tr>
        <tr>
          <td>Firefox</td>
          <td>ALT + atajo y luego ENTER</td>
        </tr>
        <tr>
          <td>Chrome</td>
          <td>ALT + atajo (si es un MAC, CTRL + ALT + atajo)</td>
        </tr>
        <tr>
          <td>Safari</td>
          <td>ALT + atajo (si es un MAC, CMD + atajo)</td>
        </tr>
      </tbody>
    </table>
    
    <p>Cuando los sitios web est&aacute;n dise&ntilde;ados pensando en la accesibilidad, todos los usuarios pueden acceder en condiciones de igualdad a los contenidos, por ejemplo:</p>
    
    <ul>
      <li>Proporcionando un texto alternativo a las im&aacute;genes, los usuarios invidentes o con problemas de visi&oacute;n pueden utilizar lectores especiales para acceder a la informaci&oacute;n.</li>
      <li>Cuando los v&iacute;deos disponen de subt&iacute;tulos, los usuarios con&nbsp;<strong>dificultades auditivas</strong>&nbsp;pueden entenderlos plenamente.</li>
      <li>Si los contenidos est&aacute;n escritos en un lenguaje sencillo e ilustrados, los usuarios con problemas de&nbsp;<strong>aprendizaje&nbsp;</strong>est&aacute;n en mejores condiciones de entenderlos.</li>
      <li>Si el usuario tiene problemas de&nbsp;<strong>movilidad&nbsp;</strong>y le cuesta usar el rat&oacute;n, las alternativas con el teclado le ayudan en la navegaci&oacute;n.</li>
    </ul>
    
    <h2>&nbsp;</h2>
    
    <h2>Tama&ntilde;o del texto</h2>
    
    <p>El dise&ntilde;o accesible de este sitio web permite que el usuario pueda elegir el tama&ntilde;o del texto que le convenga. Esta acci&oacute;n puede llevarse a cabo de diferentes maneras seg&uacute;n el navegador que se utilice.</p>
    
    <table border="1" cellpadding="1" cellspacing="1" style="width:100%;">
      <tbody>
        <tr>
          <td><strong>Navegador</strong></td>
          <td><strong>Acci&oacute;n a realizar</strong></td>
        </tr>
        <tr>
          <td>Explorer</td>
          <td>Ver &gt; Tama&ntilde;o del texto</td>
        </tr>
        <tr>
          <td>Firefox</td>
          <td>Ver &gt; Tama&ntilde;o</td>
        </tr>
        <tr>
          <td>Chrome</td>
          <td>Ajustes (icono) &gt; Opciones &gt; Avanzada &gt; Contenido web &gt; Tama&ntilde;o fuente</td>
        </tr>
        <tr>
          <td>Safari</td>
          <td>Visualizaci&oacute;n &gt; ampliar/reducir</td>
        </tr>
        <tr>
          <td>Opera</td>
          <td>Ver &gt; escala</td>
        </tr>
      </tbody>
    </table>
    
    <p>Otra forma de modificar el tama&ntilde;o de texto es utilizar los atajos de teclado definidos en los navegadores, en particular la combinaci&oacute;n de teclas:</p>
    
    <ul>
      <li><strong>CTRL y + (CMD y + en MAC)</strong>&nbsp;para aumentar el tama&ntilde;o del texto</li>
      <li><strong>CTRL y - (CMD y - en MAC)</strong>&nbsp;para reducir el tama&ntilde;o del texto</li>
    </ul>
    
    <h2>&nbsp;</h2>
    
    <h2>Compatibilidad con est&aacute;ndares y dise&ntilde;o visual</h2>
    
    <p>Todas las p&aacute;ginas de este sitio web cumplen con las&nbsp;<strong>Pautas de Accesibilidad</strong>&nbsp;o Principios Generales de Dise&ntilde;o Accesible establecidas por el Grupo de Trabajo WAI perteneciente al W3C.</p>
    
    <p>Continuamente trabajamos en mejorar la accesibilidad de este sitio web, no obstante, si detecta alg&uacute;n error o desea proponer alguna mejora en materia de accesibilidad, por favor contacte con nosotros.</p>
    
    <p><a href="http://www.w3.org/WAI/WCAG1AA-Conformance" title="Explanation of Level Double-A Conformance"><img alt="Level Double-A conformance icon, W3C-WAI Web Content Accessibility Guidelines 1.0" height="32" src="http://www.w3.org/WAI/wcag1AA" width="88" /></a></p>
    
    <h2>&nbsp;</h2>
    
    <h2>Sugerencias y reclamaciones sobre accesibilidad</h2>
    
    <p>Si detecta que alg&uacute;n documento de este sitio web no es accesible o si tiene alguna otra duda en relaci&oacute;n a la accesibilidad de este sitio web, puede utilizar el formulario de sugerencias que el Ayuntamiento de Madrid pone a su disposici&oacute;n.</p>
    
    ', button_active: false, order: 12)
    VirtualCommunity::Component.find_or_create_by(denomination: "Hazte entidad colaboradora", component_type: VirtualCommunity::ComponentType.find_by(code: "forms"),content: '<p>Desde el Programa Municipal de Voluntariado del Ayuntamiento &ldquo;Voluntari@s por Madrid&rdquo;, te ofrecemos toda la informaci&oacute;n sobre voluntariado de la ciudad de Madrid, as&iacute; como sobre los proyectos de voluntariado en los que puede colaborar. Datos de contacto: 900 777 888 (tel&eacute;fono gratuito). Horario de atenci&oacute;n: lunes a jueves de 9:00 a 18:00 h y viernes de 9:00 a 15:00 h. Correo electr&oacute;nico:&nbsp;<a href="mailto: voluntariospormadrid@madrid.es">voluntariospormadrid@madrid.es</a>&nbsp;y&nbsp;<a href="mailto: comunidadvoluntariad@madrid.es">comunidadvoluntariad@madrid.es</a></p>
    ', button_active: false, order: 13)
    VirtualCommunity::Component.find_or_create_by(denomination: "Política privacidad MTA",component_type: VirtualCommunity::ComponentType.find_by(code: "normal"),title: 'Política de privacidad y uso de datos de la aplicación Madrid Te Acompaña',content: '
    <p>El Departamento de Voluntariado de la DG. De Mayores del Ayuntamiento de Madrid suministra la aplicación Madrid Te Acompaña de forma completamente gratuita y sin publicidad.</p>
    <p>El propósito de este documento es informar a nuestros usuarios sobre nuestras políticas de recolección, uso y cesión de datos personales.</p>
    <p>Si usted utiliza nuestra aplicación está aceptando nuestra política de privacidad. Los datos que recibimos se utilizan solamente para proporcionar el servicio. No almacenamos esos datos ni los compartimos con terceros.</p>
  
    <h3>QUÉ DATOS RECOGEMOS</h3>
    <p>Los datos personales que se solicitan durante el registro son: nombre, correo electrónico, identificador, dirección postal y número de teléfono.</p>
    <p>La aplicación requiere permisos para acceso a los mensajes (SMS), accesos a la galería para fotos y videos.</p>
    <p>Los datos que la aplicación recoge durante su funcionamiento son: datos  para analíticas de rendimiento, información de la ejecución de la APP y la ID del dispositivo.</p>
    <p>Estos datos solo se recogen mientras la aplicación está en uso. No los almacenamos ni los utilizamos para ningún otro fin.</p>
    <p>Utilizamos los servicios de terceros de Google Play Services, cuya política de privacidad se puede consultar en <a href="https://www.google.com/policies/privacy/" target="_blank">https://www.google.com/policies/privacy/</a></p>
  
    <h3>REGISTROS DE ERRORES</h3>
    <p>Cuando se produce un error en la aplicación, se crea en su dispositivo un registro en forma de fichero .log que puede contener datos como su dirección IP, el nombre de su dispositivo, la versión del sistema operativo, la configuración de la aplicación y la fecha y hora del error, entre otros.</p>
  
    <h3>COOKIES</h3>
    <p>Las cookies son ficheros que contienen un código utilizado para identificar dispositivos de forma anónima. Estos ficheros los envían las páginas web que visita y se almacenan en la memoria interna de su dispositivo.</p>
    <p>Nuestra aplicación no utiliza cookies, pero es posible que algunos de los servicios de terceros que utilizamos sí lo hagan para recoger información y mejorar su funcionamiento. Si estos servicios le envían una cookie, deberán informárselo y darle la opción de aceptarla o rechazarla. Si las rechaza, es posible que no pueda utilizar algunas funciones de la aplicación.</p>
  
    <h3>PROVEEDORES DE SERVICIOS</h3>
    <p>Utilizamos los servicios de Google Play para posicionar su dispositivo en el mapa.</p>
    <p>Estos servicios de terceros pueden acceder a algunos de sus datos personales con el fin de realizar su función, pero no se les permite ceder esa información a otros ni utilizarla para ningún otro fin.</p>
  
    <h3>ENLACES A OTROS SITIOS WEB</h3>
    <p>La aplicación puede contener enlaces a otros sitios web no operados por nosotros. Si usted pulsa los enlaces, automáticamente se abrirán esas páginas. Le recomendamos que consulte las políticas de privacidad de esos sitios web. Nosotros no tenemos ningún control sobre sus contenidos ni sus políticas de privacidad, de modo que no podemos asumir ninguna responsabilidad si los visita.</p>
  
    <h3>CAMBIOS A ESTA POLÍTICA DE PRIVACIDAD</h3>
    <p>Esta política de privacidad entra en vigor el 1 de diciembre de 2022. Puede que la actualicemos ocasionalmente. Le recomendamos que visite esta página de vez en cuando si desea estar al tanto de los cambios.</p>
  
    <h3>CONTACTO</h3>
    <p>Si tiene alguna consulta o sugerencia sobre nuestra política de privacidad le invitamos a ponerse en contacto con nosotros en el correo electrónico <a href="mailto:voluntariado@madrid.es">voluntariado@madrid.es</a></p>
  
    ',button_active: false, order: 14)
  end

    task :moderate_status => :environment do
        [
            ["Pendiente","pending"],
            ["Rechazada","reject"],
            ["Modificada","edit"],
            ["Aceptada","accept"]
        ].each do |c|
            ModerateStatus.find_or_create_by(title: c[0], code: c[1], active: true)
        end
    end

    desc "Carga de datos de canal"
    task :channel => :environment do 
        [
            ["Comunidad virtual (CV)",1],
            ["Gestión interna (GI)",2],
            ["Correo electrónico (VXM@)",3],
            ["Solicitudes web (FW)",4]
        ].each do |c|
            Channel.find_or_create_by(name: c[0], code: c[1], active: true)
        end

        Rt::VolunteerSubscribe.where(channel: nil).each do |rt|
            rt.channel = Channel.find_by(code: 1)
            rt.save(validate: false)
        end
    end

    desc "Carga de datos de motivos de rechazo"
    task :reject_type => :environment do 
        [
            ["Baja por defunción","death"]           
        ].each do |c|
            Req::RejectionType.find_or_create_by(name: c[0], description: c[0], active: true, code: c[1])
        end       
    end

    desc "Carga de datos de motivos de baja"
    task :unsubscribe_reason => :environment do 
        [
            ["Baja por defunción","death"]           
        ].each do |c|
           UnsubscribeReason.find_or_create_by(name: c[0], active: true, code: c[1])
        end       
    end

    desc "Carga de datos de via de entrada"
    task :entry_way => :environment do 
        [
            ["Correo electrónico",1],
            ["Página web",2],
            ["Otros",3]
        ].each do |c|
            EntryWay.find_or_create_by(name: c[0], code: c[1], active: true)
        end
    end

    desc "Carga de datos de boroughts"
    task :load_boroughts => :environment do
        Address.all.find_each do |a|
            a.borought_id = Borought.where("upper(name) = upper('#{a.attributes["borough"]}')").try(:first).try(:id)
            a.save(validate: false)
            puts "ID: #{a.id}"
        end
        CitizenService.all.find_each do |cs|
            cs.borought_id = Borought.where("upper(name) = upper('#{cs.attributes["borought"]}')").try(:first).try(:id)
            cs.save(validate: false)
            puts "ID: #{cs.id}"
        end
        Rt::ProjectPublishing.all.find_each do |rt|
            rt.borought_id = Borought.where("upper(name) = upper('#{rt.attributes["borough"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteerAmendment.all.find_each do |rt|
            rt.borought_id = Borought.where("upper(name) = upper('#{rt.attributes["borough"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteersDemand.all.find_each do |rt|
            rt.borought_id = Borought.where("upper(name) = upper('#{rt.attributes["borough"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
    end

    desc "Carga de datos de districts"
    task :load_districts => :environment do
        Project.all.find_each do |pr|
            pr.district_id = District.where("upper(name) = upper('#{pr.attributes["district"]}')").try(:first).try(:id)
            pr.save(validate: false)
            puts "ID: #{pr.id}"
        end
        Address.all.find_each do |a|
            a.district_id = District.where("upper(name) = upper('#{a.attributes["district"]}')").try(:first).try(:id)
            a.save(validate: false)
            puts "ID: #{a.id}"
        end
        Borought.all.find_each do |b|
            b.district_id = District.where("upper(code) = upper('#{b.attributes["district_code"]}')").try(:first).try(:id)
            b.save(validate: false)
            puts "ID: #{b.id}"
        end
        CitizenService.all.find_each do |cs|
            cs.district_id = District.where("upper(name) = upper('#{cs.attributes["district"]}')").try(:first).try(:id)
            cs.save(validate: false)
            puts "ID: #{cs.id}"
        end
        Rt::VolunteerSubscribe.all.find_each do |rt|
            rt.district_id = District.where("upper(name) = upper('#{rt.attributes["district"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::ProjectPublishing.all.find_each do |rt|
            rt.district_id = District.where("upper(name) = upper('#{rt.attributes["district"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteerAmendment.all.find_each do |rt|
            rt.district_id = District.where("upper(name) = upper('#{rt.attributes["district"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteersDemand.all.find_each do |rt|
            rt.district_id =  District.where("upper(name) = upper('#{rt.attributes["district"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
    end


    desc "Carga de datos de genders"
    task :fill_genders => :environment do
        Gender.find_or_create_by(name: "Varón", active: true)
        Gender.find_or_create_by(name: "Mujer", active: true)
        Gender.find_or_create_by(name: "Otro", active: true)
    end
    
    desc "Actualización de genders en las distintas tablas"
    task :load_genders => :environment do
        Citizen.all.find_each do |c|
            c.gender = get_gender(c)
            c.save(validate: false)
            puts "ID: #{c.id}"
        end
        Volunteer.all.find_each do |v|
            v.gender = get_gender(c)
            v.save(validate: false)
            puts "ID: #{v.id}"
        end
    end

    desc "Asignar reuniones"
    task generate_meetings: :environment do
        Meeting.all.each do |meeting|
            voluns_proys= ProjectsVolunteer.where(project: meeting.project)
            voluns_proys.each do |vp|
                if vp.volun_proy_meetings.blank?
                    volun_proy_m = VolunProyMeeting.create(projects_volunteer: vp, meeting: meeting)
                    vp.volun_proy_meetings << volun_proy_m
                    vp.save
                else
                    aux = false
                    vp.volun_proy_meetings.each do |vpm|
                        if vpm.meeting.id == meeting.id
                            aux=true 
                            break
                        end
                    end
                    unless aux
                        volun_proy_m = VolunProyMeeting.create(projects_volunteer: vp, meeting: meeting)
                        vp.volun_proy_meetings << volun_proy_m
                        vp.save
                    end
                end
            end
        end
    end

    desc "Añade el código del borough"
    task boroughts: :environment do
        table_borough = {
            "PALACIO" => 1,
            "EMBAJADORES" => 2,
            "CORTES" => 3,
            "JUSTICIA" => 4,
            "UNIVERSIDAD" => 5,
            "SOL" => 6,
            "IMPERIAL" => 1,
            "ACACIAS" => 2,
            "CHOPERA" => 3,
            "LEGAZPI" => 4,
            "DELICIAS" => 5,
            "PALOS DE MOGUER" => 6,
            "ATOCHA" => 7,
            "PACIFICO" => 1,
            "ADELFAS" => 2,
            "ESTRELLA" => 3,
            "IBIZA" => 4,
            "LOS JERONIMOS" => 5,
            "NIÑO JESUS" => 6,
            "RECOLETOS" => 1,
            "GOYA" => 2,
            "FUENTE DEL BERRO" => 3,
            "GUINDALERA" => 4,
            "LISTA" => 5,
            "CASTELLANA" => 6,
            "EL VISO" => 1,
            "PROSPERIDAD" => 2,
            "CIUDAD JARDIN" => 3,
            "HISPANOAMERICA" => 4,
            "NUEVA ESPAÑA" => 5,
            "CASTILLA" => 6,
            "BELLAS VISTAS" => 1,
            "CUATRO CAMINOS" => 2,
            "CASTILLEJOS" => 3,
            "ALMENARA" => 4,
            "VALDEACEDERAS" => 5,
            "BERRUGUETE" => 6,
            "GAZTAMBIDE" => 1,
            "ARAPILES" => 2,
            "TRAFALGAR" => 3,
            "ALMAGRO" => 4,
            "RIOS ROSAS" => 5,
            "VALLEHERMOSO" => 6,
            "EL PARDO" => 1,
            "FUENTELARREINA" => 2,
            "PEÑA GRANDE" => 3,
            "EL PILAR" => 4,
            "LA PAZ" => 5,
            "VALVERDE" => 6,
            "MIRASIERRA" => 7,
            "EL GOLOSO" => 8,
            "CASA DE CAMPO" => 1,
            "ARGUELLES" => 2,
            "CIUDAD UNIVERSITARIA" => 3,
            "VALDEZARZA" => 4,
            "VALDEMARIN" => 5,
            "EL PLANTIO" => 6,
            "ARAVACA" => 7,
            "LOS CARMENES" => 1,
            "PUERTA DEL ANGEL" => 2,
            "LUCERO" => 3,
            "ALUCHE" => 4,
            "CAMPAMENTO" => 5,
            "CUATRO VIENTOS" => 6,
            "LAS AGUILAS" => 7,
            "COMILLAS" => 1,
            "OPAÑEL" => 2,
            "SAN ISIDRO" => 3,
            "VISTA ALEGRE" => 4,
            "PUERTA BONITA" => 5,
            "BUENAVISTA" => 6,
            "ABRANTES" => 7,
            "ORCASITAS" => 1,
            "ORCASUR" => 2,
            "SAN FERMIN" => 3,
            "ALMENDRALES" => 4,
            "MOSCARDO" => 5,
            "ZOFIO" => 6,
            "PRADOLONGO" => 7,
            "ENTREVIAS" => 1,
            "SAN DIEGO" => 2,
            "PALOMERAS BAJAS" => 3,
            "PALOMERAS SURESTE" => 4,
            "PORTAZGO" => 5,
            "NUMANCIA" => 6,
            "PAVONES" => 1,
            "HORCAJO" => 2,
            "MARROQUINA" => 3,
            "MEDIA LEGUA" => 4,
            "FONTARRON" => 5,
            "VINATEROS" => 6,
            "VENTAS" => 1,
            "PUEBLO NUEVO" => 2,
            "QUINTANA" => 3,
            "CONCEPCION" => 4,
            "SAN PASCUAL" => 5,
            "SAN JUAN BAUTISTA" => 6,
            "COLINA" => 7,
            "ATALAYA" => 8,
            "COSTILLARES" => 9,
            "PALOMAS" => 1,
            "PIOVERA" => 2,
            "CANILLAS" => 3,
            "PINAR DEL REY" => 4,
            "APOSTOL SANTIAGO" => 5,
            "VALDEFUENTES" => 6,
            "VILLAVERDE ALTO C.H." => 1,
            "SAN CRISTOBAL" => 2,
            "BUTARQUE" => 3,
            "LOS ROSALES" => 4,
            "LOS ANGELES" => 5,
            "CASCO H.VALLECAS" => 1,
            "SANTA EUGENIA" => 2,
            "ENSANCHE DE VALLECAS" => 3,
            "CASCO H.VICALVARO" => 1,
            "VALDEBERNARDO" => 2,
            "VALDERRIVAS" => 3,
            "EL CAÑAVERAL" => 4,
            "SIMANCAS" => 1,
            "HELLIN" => 2,
            "AMPOSTA" => 3,
            "ARCOS" => 4,
            "ROSAS" => 5,
            "REJAS" => 6,
            "CANILLEJAS" => 7,
            "EL SALVADOR" => 8,
            "ALAMEDA DE OSUNA" => 1,
            "AEROPUERTO" => 2,
            "CASCO H.BARAJAS" => 3,
            "TIMON" => 4,
            "CORRALEJOS" => 5
        }
        puts "============================"
        puts "Actualización de códigos de barrio"
        puts "============================"
        Address.all.find_each do |address|
            unless address.borough.blank?
                begin
                    address.borough_code = table_borough[address.borough.to_s.strip]

                    if address.update(borough_code: table_borough[address.borough.to_s.strip])
                        puts "Se ha actualizado la dirección con id: #{address.id}"
                    else
                        puts "ERROR! en la dirección con barrio: #{address.borough}, id: #{address.id}"
                    end
                rescue
                    puts "ERROR! en la dirección con barrio: #{address.borough}, id: #{address.id}"
                end
            end
        end
        puts "============================"

    end

    desc "Carga de datos de provinces"
    task :load_provinces => :environment do
        Address.all.find_each do |a|
            a.province_id = Province.where("upper(name) = upper('#{a.attributes["province"]}')").try(:first).try(:id)
            a.save(validate: false)
            puts "ID: #{a.id}"
        end
        Citizen.all.find_each do |c|
            c.province_id = Province.where("upper(name) = upper('#{c.attributes["province"]}')").try(:first).try(:id)
            c.save(validate: false)
            puts "ID: #{c.id}"
        end
        Rt::ProjectPublishing.all.find_each do |rt|
            rt.province_id = Province.where("upper(name) = upper('#{rt.attributes["province"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteerAmendment.all.find_each do |rt|
            rt.province_id = Province.where("upper(name) = upper('#{rt.attributes["province"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
        Rt::VolunteersDemand.all.find_each do |rt|
            rt.province_id =  Province.where("upper(name) = upper('#{rt.attributes["province"]}')").try(:first).try(:id)
            rt.save(validate: false)
            puts "ID: #{rt.id}"
        end
    end

    desc "Nuevos settings"  
    task :settings => :environment do  
        puts "-------------------------------------"
        puts "Añadiendo settings..."
        settings_list.each do |setting|
            exist = Setting.find_by(key: setting[:key])
            if exist.blank?
                Setting.create(key: setting[:key], value: setting[:value], active: true)
                puts "AÑADIDO: #{setting[:key]}"
            else
                puts "EXISTE: #{setting[:key]}"
            end
        end
        puts "FIN"
        puts "-------------------------------------"
    end 

    desc "Nuevos tipos de proyectos"  
    task :project_types => :environment do
        [
            'pt_social',
            'pt_centre',
            'pt_permanent',
            'pt_punctual',
            'pt_entity',
            'pt_subvention',
            'pt_other',
            'pt_retired_volunteer',
            'pt_municipal',
            'pt_volunteer'
        ].each_with_index do |type,i|
            ProjectType.find_or_create_by(id: i+1, kind: i+1, description: type)
        end
    end

    task change_district_borought: :environment do
        [
            ["02","01"],
["08","08"],
["01","09"],
["02","09"],
["03","09"],
["05","09"],
["06","09"],
["07","09"],
["01","10"],
["02","10"],
["03","10"],
["05","10"],
["06","10"],
["07","10"],
["01","11"],
["01","15"],
["02","15"],
["03","15"],
["04","15"],
["05","15"],
["07","15"],
["08","15"],
["09","15"],
["01","16"],
["02","16"],
["03","16"],
["04","16"],
["06","16"],
["01","01"],
["01","02"],
["07","02"],
["01","04"],
["02","05"],
["03","06"],
["04","07"],
["05","08"],
["04","09"],
["04","10"],
["04","11"],
["04","12"],
["04","13"],
["06","14"],
["06","15"],
["05","16"],
["01","17"],
["02","17"],
["03","17"],
["04","17"],
["05","17"],
["01","18"],
["02","18"],
["03","18"],
["01","19"],
["02","19"],
["03","19"],
["04","19"],
["01","20"],
["02","20"],
["03","20"],
["04","20"],
["05","20"],
["06","20"],
["07","20"],
["08","20"],
["01","21"],
["02","21"],
["03","21"],
["04","21"],
["05","21"],
["03","01"],
["04","01"],
["05","01"],
["06","01"],
["02","02"],
["03","02"],
["04","02"],
["05","02"],
["06","02"],
["01","03"],
["02","03"],
["03","03"],
["04","03"],
["05","03"],
["06","03"],
["02","04"],
["03","04"],
["04","04"],
["05","04"],
["06","04"],
["01","05"],
["03","05"],
["04","05"],
["05","05"],
["06","05"],
["01","06"],
["02","06"],
["04","06"],
["05","06"],
["06","06"],
["01","07"],
["02","07"],
["03","07"],
["05","07"],
["06","07"],
["01","08"],
["02","08"],
["03","08"],
["04","08"],
["06","08"],
["07","08"],
["02","11"],
["03","11"],
["05","11"],
["06","11"],
["07","11"],
["01","12"],
["02","12"],
["03","12"],
["05","12"],
["06","12"],
["07","12"],
["01","13"],
["02","13"],
["03","13"],
["05","13"],
["06","13"],
["01","14"],
["02","14"],
["03","14"],
["04","14"],
["05","14"]
        ].each do |codes|
            bor = Borought.find_by(code: codes[0])
            bor.district = District.find_by(code: codes[1])
            bor.save(validate: false)

        end
    end

    task create_request_form: :environment do
        Rt::VolunteerProjectSubscribe.all.each do |rt|
            if rt.request_form.blank?
                request = RequestForm.new(user_id: 1,rt_extendable: rt, request_type_id: 14, req_status_id: 1, status_date: Time.zone.now)
              
                if request.save
                    puts "Se ha creado"
                else
                    puts "ERROR: #{request.errors.full_messages}"
                end
            end

        end
    end

    private

    def settings_list
        [
            {key: "VolunFrontend.cifAytoMadrid",value: "P2807900B"},
            {key: "VolunBackend.file_size_kb",value: "1024"},
            {key: "VolunBackend.ws_priority_service",value: "12"},
            {key: "VolunBackend.ws_permit_create_service",value: "48"},
            {key: "VolunBackend.ws_limit_create_service",value: "365"},
            {key: "ListaDeCorreosEnvioCuestionario",value: ""},
            {key: "BlokedMTA",value: "false"},
            {key: "VolunBackend.mail_footer",value: ""},
            {key: "VolunFrontend.faq",value: "Aquí te presentamos una lista de las soluciones a los problemas más frecuentes en trámites digitales con la administración, uso de aplicaciones móviles y funcionalidad de los dispositivos, en el marco del proyecto \"VOLUNTARIADO DIGITAL: acompañamiento digital a personas mayores\""},
            {key: "RRSS.facebook",value: "https://www.facebook.com/voluntariospormadrid/"},
            {key: "RRSS.twitter",value: "https://twitter.com/voluntmadrid"},
            {key: "RRSS.instagram",value: "https://www.instagram.com/voluntariospormadrid/"},
            {key: "RRSS.youtube",value: "https://www.youtube.com/channel/UC8LQ25uGMGHE7wq5ihMx_7A"},
            {key: "CVContact.telefono",value: "+34 900 777 888"},
            {key: "CVContact.email",value: "comunidadvoluntariad@madrid.es"}
        ]
    end
    
    def get_gender(resource)
        case resource.attributes["gender"]
        when "Hombre","Varón",0,"male"
            return Gender.find_by(name: "Hombre")
        when "Mujer",1,"female"
            return Gender.find_by(name: "Mujer")
        else 
            return Gender.find_by(name: "Otro")
        end
    end

    

end