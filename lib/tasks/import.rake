namespace :import do
  desc "Importar fotos"
  task fotos: :environment do
    i=0
    #Dir.glob('/home/iam/FotosVoluntarios/PendientesDeImportar6/*.*').each do |pathFileName|
    Dir.glob('/aytomad/app/volun/volun_backend/shared/private/system/links/FotosVoluntarios/PendientesDeImportar/*.*').each do |pathFileName|

      file = File.new(pathFileName)
      #puts pathFileName

      name =  File.basename(pathFileName)
      size = name.length
      name = name[5, size]
      size = name.length
      identificator = name[0, size -4]
      #Compruebo si existe voluntario con ese id
      @volunteer = Volunteer.find_by(id: identificator)
      if @volunteer != nil

        @link = Link.find_by(linkable_id: identificator, link_type_id: 1, linkable_type: 'Volunteer')
        if @link != nil
          #puts "existe registro en link"
          link = Link.find_by(linkable_id: identificator, link_type_id: 1, linkable_type: 'Volunteer')
          link.file = file
          link.save
        else
          #puts "no existe registro en link"
          link = Link.new
          link.linkable_id = identificator
          link.link_type_id = 1
          link.linkable_type = "Volunteer"
          link.file = file
          link.save
        end

        #Como no hay espacio en el recurso, elimino el fichero en vez de moverlo
        FileUtils.rm(pathFileName)
      end
    end


  end

  desc "Add Resources"
  task resources: :environment do
    resources = {
      Citizen: 'Mayor', 
      CitizenService: 'Servicio a un Mayor'
    }
    puts "==========================================================="
    resources.each do |k,v|
    
      resource = Resource.new(name: k, description: v)
      if resource.save
        puts "Se ha añadido un nuevo recurso: #{resource.description}"
      else
        puts resource.errors.full_messages
      end
    end
    puts "==========================================================="
  end

  desc "Add Service Questions"
  task :service_questions => :environment do
    questions = {
        "1" => { "description" => "Pregunta de prueba", "type" => "true" },
        "2" => { "description" => "Pregunta de prueba 2", "type" => "false" }
    }

    questions.each do |k,v|
        question = ServiceQuestion.new(description: "Pregunta de prueba", question_type: true)
        question.save
        question = ServiceQuestion.new(description: "Pregunta de prueba 2", question_type: false)
        question.save
    end
  end
end