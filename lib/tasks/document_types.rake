namespace :document_types do

    desc "Actualizar el campo document_type_id de citizens y volunteers para que los valores ya existentes estén bien relacionados con los de la nueva tabla"
    task update: :environment do
        Citizen.where.not(document_type_id: nil).each do |citizen|
            citizen.document_type_id = document_type_for_citizens(citizen.document_type_id)
            if citizen.save(validate: false)
                p "Ciudadano con id #{citizen.id} actualizado con éxito"
            else
                p "**********ERROR al actualizar el ciudadano con id #{citizen.id} **********"
            end
        end
    end

    private

    def document_type_for_citizens(document_type_id)
        case document_type_id
        when 1
            DocumentType.find_by(name: "DNI").id
        when 2
            DocumentType.find_by(name: "PASAPORTE").id
        when 3
            DocumentType.find_by(name: "TRT").id
        when 4
            DocumentType.find_by(name: "TRP").id
        when 5
            DocumentType.find_by(name: "TRC").id
        when 6
            DocumentType.find_by(name: "NIE").id
        end
    end
end