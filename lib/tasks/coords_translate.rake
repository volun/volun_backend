namespace :coords_translate do
	desc "Traducción de las coordenadas de ET50 a ETRS89"

    task :translate89 => :environment do
        require 'net/http' 
        require 'json' 

        Address.all.each do |address|
            begin
                lat_aux       = address.latitude.to_i/100.0 
                long_aux      = address.longitude.to_i/100.0 
                if Rails.env.production?
                    environment = 'http://www-j.munimadrid.es';
                elsif Rails.env.preproduction?
                    environment = 'http://prejinter.munimadrid.es';
                else
                    environment = 'https://desajinter.munimadrid.es';
                end
        
                url = "#{environment}/BDCTR_RSGENERAL/restBDC/conversionETRS89?coordX=#{lat_aux}&coordY=#{long_aux}&aplicacion=VOLUN" 
        
                uri = URI.parse(url) 
                response = Net::HTTP.get(uri) 
                hash_89 = JSON.parse(response) 
                
                address.xetrs89=hash_89["coordX"] 
                address.yetrs89=hash_89["coordY"]
                if address.save    
                    puts "Conversión correcta de la dirección id: #{address.id}"
                else
                    puts "Error al guardar #{address.id}" 
                end
            rescue => exception
                puts "Error en la conversión de coordenadas"
            end     
        end
    end
end