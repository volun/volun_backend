namespace :unique_address do
  #rake unique_address:delete_duplicated_address [valor]
  desc "Eliminar duplicidades"
    task :delete_duplicated_address,[:address] => [:environment] do |task,args|
      if args[:address].blank?
        puts "Opciones:"
        puts "  [show] -> Ver coincidencias"
        puts "  [del]  -> Eliminar coincidencias"
        puts ""
      else
        addresses1 = Address.all
        addresses2 = Address.all
        addresses1.each.with_index(1) do |address1,index1|
          if (addresses1.count - index1) != 0 
            addresses2 = addresses1.last(addresses1.count - index1)
            addresses2.each.with_index(index1) do |address2,index2|
            unless address1.id == address2.id
              if (address1.road_type == address2.road_type &&
                  address1.road_name == address2.road_name &&
                  address1.road_number_type == address2.road_number_type &&
                  address1.road_number == address2.road_number &&
                  address1.grader == address2.grader &&
                  address1.stairs == address2.stairs &&
                  address1.floor == address2.floor &&
                  address1.door == address2.door &&
                  address1.postal_code == address2.postal_code &&
                  address1.borough == address2.borough &&
                  address1.district == address2.district &&
                  address1.town == address2.town &&
                  address1.province == address2.province &&
                  address1.country == address2.country &&
                  address1.ndp_code == address2.ndp_code &&
                  address1.local_code == address2.local_code &&
                  address1.province_code == address2.province_code &&
                  address1.town_code == address2.town_code &&
                  address1.district_code == address2.district_code &&
                  address1.class_name == address2.class_name) 

                  puts"========================================="
                  puts "COINCIDENCIA"
                  puts "---"
                  puts "Direcciones con ids #{address1.id} y #{address2.id}"
                  puts"========================================="
                  
                  del=false
                  if args[:address].to_s == "show"
                    del=false
                  elsif args[:address].to_s == "del"
                    del=true
                  end
                  update_entities(address1.id, address2.id,del)
                  update_events(address1.id, address2.id,del)
                  update_volunteers(address1.id, address2.id,del)
                  delete_address_duplicated(address2.id,del)

                end #FIN if (condiciones)
              end
            end
          end
        end
      end
    end #FIN TASK

    def update_entities(address_id_1,address_id_2,del)
      puts"========================================="
      puts "Buscando entidades con dirección duplicada..."
      puts"========================================="
      Entity.all.each do |ent|
        if (ent.address_id == address_id_2)
          puts"========================================="
          puts "Entidad #{ent.id}"
          puts"========================================="
          ActiveRecord::Base.connection.execute("UPDATE entities SET address_id=#{address_id_1} WHERE id=#{ent.id}") if del.to_s=="true"
        end
      end
    end

    def update_events(address_id_1,address_id_2,del)
      puts"========================================="
      puts "Buscando eventos con dirección duplicada..."
      puts"========================================="
      Event.all.each do |ev|
        if (ev.address_id == address_id_2)
          puts"========================================="
          puts "Evento #{ev.id}"
          puts"========================================="
          ActiveRecord::Base.connection.execute("UPDATE events SET address_id=#{address_id_1} WHERE id=#{ev.id}") if del.to_s=="true"
        end
      end
    end

    def update_volunteers(address_id_1,address_id_2,del)
      puts"========================================="
      puts "Buscando voluntarios con dirección duplicada..."
      puts"========================================="
      Volunteer.all.each do |volun|
        if (volun.address_id == address_id_2)
          puts"========================================="
          puts "Voluntario #{volun.id}"
          puts"========================================="
          ActiveRecord::Base.connection.execute("UPDATE volunteers SET address_id=#{address_id_1} WHERE id=#{volun.id}") if del.to_s=="true"
        end
      end
    end

    def delete_address_duplicated(address_id_2,del)
      # Si las direcciones coinciden, siempre se eliminará la correspondiente con el id2
      if del.to_s=="true"
        puts"========================================="
        puts "Dirección duplicada con id #{address_id_2}--ELIMINADA"
        puts"========================================="
        ActiveRecord::Base.connection.execute("DELETE FROM addresses WHERE id=#{address_id_2}")
      end 
    end

end #FIN NAMESPACE