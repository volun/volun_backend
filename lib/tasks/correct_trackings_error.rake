namespace :correct_trackings_error do
	desc "Elimina todos los seguimientos erroneos ocasionados"

  task :delete,[:volunteer] => [:environment] do |task,args|
    puts "=============================="
    puts "Eliminar duplicados"
    puts "=============================="
    unless args[:volunteer].blank?
      puts "Voluntario ID: #{args[:volunteer]}"
      puts "=============================="
      run_method(true,args[:volunteer])
    else
      puts "No se ha introducido un valor para el ID del voluntario (volunteer_id)"
      puts "ej: rake correct_trackings_error:delete[8]"
      puts ""
      puts "Opciones:"
      puts "  [x]   -> Donde x es un valor numérico, muestra y elimina los seguimientos de un único voluntario"
      puts "  [all] -> Muestra y elimina los seguimientos de todos los voluntarios."
      puts ""
    end
  end

  task :show,[:volunteer] => [:environment] do |task,args|
    puts "=============================="
    puts "Mostrar duplicados"
    puts "=============================="
    unless args[:volunteer].blank?
      puts "Voluntario ID: #{args[:volunteer]}"
      puts "=============================="
      run_method(false,args[:volunteer])
    else
      puts "No se ha introducido un valor para el ID del voluntario (volunteer_id)"
      puts "ej: rake correct_trackings_error:show[8]"
      puts ""
      puts "Opciones:"
      puts "  [x]   -> Donde x es un valor numérico, muestra un único voluntario"
      puts "  [all] -> Muestra todos los voluntarios."
      puts ""
    end
  end

  def run_method(delete,volunteer)
    if volunteer.to_s=="all"
      trackings=Volun::Tracking.find_by_sql("SELECT * FROM volun_trackings WHERE project_id is not null  AND (tracking_type_id=1 OR tracking_type_id=19) ORDER BY tracked_at DESC")
    else
      trackings=Volun::Tracking.find_by_sql("SELECT * FROM volun_trackings WHERE project_id is not null  AND (tracking_type_id=1 OR tracking_type_id=19) AND volunteer_id=#{volunteer} ORDER BY tracked_at DESC")
    end
    valid=[]
    trackings.each do |track|
      if !valid.any?
        valid[valid.count]="#{track.project_id}-#{track.volunteer_id}-#{track.tracking_type_id}"

        if track.tracking_type_id==1
          puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |V| -> Alta #{track.tracked_at}"
        else
          puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |V| -> Baja #{track.tracked_at}"
        end
      else
        if valid[valid.count-1]!="#{track.project_id}-#{track.volunteer_id}-#{track.tracking_type_id}"
          valid[valid.count]="#{track.project_id}-#{track.volunteer_id}-#{track.tracking_type_id}"

          if track.tracking_type_id==1
            puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |V| -> Alta #{track.tracked_at}"
          else
            puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |V| -> Baja #{track.tracked_at}"
          end
        else
          if delete.to_s=="true"
            ActiveRecord::Base.connection.execute("DELETE FROM volun_trackings WHERE id=#{track.id}")
          end

          if track.tracking_type_id==1
            puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |X| -> Alta Dup #{track.tracked_at}"
          else
            puts "Seguimineto:  project_id(#{track.project_id})-volunteer_id(#{track.volunteer_id}) |X| -> Baja Dup #{track.tracked_at}"
          end
        end
      end
    end
  end
end