namespace :destroy_search do
    desc "Limpiar busquedas"
    task clean: :environment do
        SearchForm.find_each do |s|
            if s.destroy
                puts "Se ha limpiado la busqueda"
            else
                puts "ERROR: No se ha borrado la busqueda con ID : #{s.id} "
            end
        end
    end
end