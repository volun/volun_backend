namespace :relations do
	desc "Cambio de tipo de asociados"

    task :trackings => :environment do
        trackings_derivaties = Volun::Tracking.all
        
        trackings_derivaties.each do |d|
            if d.comments == "Derivación a un proyecto"
                VolunteerProjectsRelation.create(project_id: d.project_id, volunteer_id: d.volunteer_id, type_relation: "DERIVATIE", start_date: d.tracked_at.strftime("%d/%m/%Y"), end_date: nil)
            elsif d.comments == "Desderivación a un proyecto"
                derivatie = VolunteerProjectsRelation.find_by(project_id: d.project_id, volunteer_id: d.volunteer_id, type_relation: "DERIVATIE", end_date: nil)
                if !derivatie.blank?
                    derivatie.end_date = d.tracked_at.strftime("%d/%m/%Y")
                    derivatie.save
                end
            elsif d.comments == "Alta en un proyecto"
                derivatie = VolunteerProjectsRelation.find_by(project_id: d.project_id, volunteer_id: d.volunteer_id, type_relation: "DERIVATIE", end_date: nil)
                if !derivatie.blank?
                    derivatie.end_date = d.tracked_at.strftime("%d/%m/%Y")
                    derivatie.save
                end
                VolunteerProjectsRelation.create(project_id: d.project_id, volunteer_id: d.volunteer_id, type_relation: "ASOCIATED", start_date: d.tracked_at.strftime("%d/%m/%Y"), end_date: nil)
            elsif d.comments == "Baja de un proyecto"
                derivatie = VolunteerProjectsRelation.find_by(project_id: d.project_id, volunteer_id: d.volunteer_id, type_relation: "ASOCIATED", end_date: nil)
                if !derivatie.blank?
                    derivatie.end_date = d.tracked_at.strftime("%d/%m/%Y")
                    derivatie.save
                end
            end
        end
    end
end