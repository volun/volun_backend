namespace :images do

    task :show => :environment do
        delete_image(false)        
    end

    task :delete => :environment do
        delete_image(true)        
    end

    private


    def delete_image(delete)

        projects = Project.all
        projects.each do |p| 

            imgs = Link.where(linkable_id: p.id).where(linkable_type: "Project").where(link_type_id: 1)

            imgs.each do |p| 
                if p.path.to_s != "/images/missing.png"
                    puts "====================================="
                    puts "Path bbdd: #{p.path}"
                    puts "Path file: #{p.file}"
                    puts "Existe: #{p.file.exists?}"
                    aux_path= p.path
                    if !p.file.exists? && delete && 
                        if p.destroy
                            puts "Se ha borrado el archivo: #{aux_path}"
                        end
                    end
                end
                
            end

        end
    end
end