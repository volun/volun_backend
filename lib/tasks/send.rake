namespace :send do
  desc "Enviar felicitación"
  task congratulations: :environment do
    message = "#{Setting.find_by(key: "BirthdayMessage").try(:value)}"
    subject = "Felicidades"
   
    @volunteers = Volunteer.where("extract(month from birth_date) = ? AND extract(day from birth_date) = ?", Time.zone.now.strftime('%m'), Time.zone.now.strftime('%d'))
    puts Time.zone.now.strftime('%m')
    puts Time.zone.now.strftime('%d')
    puts @volunteers.count
    @volunteers.each do |volunteer|
      if !volunteer.email.blank?
        begin
          VolunteerMailer.send_email(volunteer.email, message: message, subject: subject).deliver_now
          puts "Enviado EMAIL"
        rescue Exception  => e
          Rails.logger.error('sendEmail#congratulations') do
            "Error sending email: \n#{e}"
          end
          puts "Error sending email: \n#{e}"
        end
      elsif !volunteer.mobile_number.blank?
        begin 
          SMSApi.new.sms_deliver(volunteer.mobile_number, message)
          puts "Enviado SMS"
        rescue Exception  => e
          Rails.logger.error('sendSms#congratulations') do
            "Error sending sms: \n#{e}"
          end
          puts "Error sending sms: \n#{e}"
        end
      end
    end
  end

 #Tarea de envío masivo de correo con login y contraseña para la comunidad virtual a todos los usuarios
  desc "Enviar login y password"
  task :login,[:send] => [:environment] do |task,args|
    subject = "Bienvenido a la Comunidad Virtual"
    @volunteers=Volunteer.all.where("(email is not null OR email!='') AND active=true")
    
    puts "================================================================"
    puts "Comienzo de envío masivo a usuarios dados de alta, con email"
    puts "y sin usuario"
    puts "================================================================"
    puts "Modo de uso:"
    puts "    send:login[true] -> Crea los usuarios y envía los emails"
    puts "    send:login       -> Sólo muestra los voluntarios que no"
    puts "                        tienen usuario"
    puts "================================================================"

    @volunteers.each do |volunteer|
      if volunteer.user.blank? && !volunteer.email.blank?
        puts "================================================================"
        puts "Voluntario con id(#{volunteer.id}) y correo #{volunteer.email} sin usuario"
        puts "================================================================"

        if !args[:send].blank? && args[:send].to_s=="true"
          puts "================================================================"
          puts "Creando usuario y mandando el mail..."
          puts "================================================================"
          user= volunteer.user
          user = User.new(login: "uservolunteer#{'%09d' % volunteer.id}", loggable: volunteer, notice_type: NoticeType.email.take)
          user.email = volunteer.email
          user.mobile_phone = volunteer.phone_number_alt
          @user_fin = volunteer.email
          @pass =  Digest::SHA1.hexdigest("#{Time.now.to_s}--#{user.login}")[0,8] #contraseña aleatoria
          
          user.password = @pass
          user.password_confirmation = @pass
          user.confirmed_at = Date.today
          unless user.save
            puts "================================================================"
            puts "ERROR: No se ha guardado el usuario"
            puts user.errors.full_messages
            puts "================================================================"
          else
            volunteer.user = user
            unless volunteer.save
              puts "================================================================"
              puts "ERROR: No se ha guardado el voluntario con id(#{volunteer.id})"
              puts volunteer.errors.full_messages
              puts "================================================================"
            else
              begin
                puts "================================================================"
                puts "Correo enviado a #{volunteer.email} con usuario #{@user_fin}"
                puts "================================================================"
                VolunteerMailer.send_login(volunteer.email, user: @user_fin, password: @pass, subject: subject).deliver_now
                
              rescue Exception  => e
                Rails.logger.error('sendLogin#login') do
                    "Error sending email: \n#{e}"
                end
              end
            end
          end
        end
      end
    end
  end #fin task login


  desc "Enviar recordatorio el día antes"
  task rt_appointment: :environment do 
    puts "================================================================"
    puts "Día a recordar: #{(Time.zone.now + 1.day).strftime("%Y-%m-%d")}"
    Rt::VolunteerSubscribe.where("appointment_at is not null AND to_char(appointment_at, 'YYYY-MM-DD') = ?", (Time.zone.now + 1.day).strftime("%Y-%m-%d") ).each do |rt_volunteer_subscribe|
      puts "*********************************************************************"
      puts "ID: #{rt_volunteer_subscribe.id}"
      status_manager = RtController::StatusManager.new(request_form: rt_volunteer_subscribe.request_form,
        manager_id: nil)
      if status_manager.mark_request_form_as_appointed
        AuditGenerateHelper.audit_request_appointed(rt_volunteer_subscribe, nil) 
        errormail = false
        errorsms = false  
        if !rt_volunteer_subscribe.email.blank?
          message = I18n.t('rt_volunteer_subscribes.message_appointment', 
            date_at:rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" :rt_volunteer_subscribe.appointment_at.strftime("%d/%m/%y").to_s,
            hour_at:rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" :rt_volunteer_subscribe.appointment_at.strftime("%H:%M").to_s,
            contact_person:rt_volunteer_subscribe.contact_person).html_safe
          subject = I18n.t('rt_volunteer_subscribes.subject_volunteer')
          begin
            VolunteerMailer.send_email(rt_volunteer_subscribe.email, message: message, subject: subject).deliver_now
            AuditGenerateHelper.audit_send_mail(rt_volunteer_subscribe, nil)
          rescue Exception  => e
            errormail= true
            begin
              Rails.logger.error("ERROR-MAIL: #{e}")
            rescue
            end
            puts "ERROR-MAIL: #{e}"
          end
        end

        if !rt_volunteer_subscribe.phone_number_alt.blank?
          message = I18n.t('rt_volunteer_subscribes.message_appointment_sms',
            date_at:rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" :rt_volunteer_subscribe.appointment_at.strftime("%d/%m/%y"),
            hour_at:rt_volunteer_subscribe.try(:appointment_at).blank? ? "-" :rt_volunteer_subscribe.appointment_at.strftime("%H:%M"),
            contact_person: Nokogiri::HTML(rt_volunteer_subscribe.contact_person).text.to_s).html_safe
          begin
            SMSApi.new.sms_deliver(rt_volunteer_subscribe.phone_number_alt, message)
            AuditGenerateHelper.audit_send_sms(rt_volunteer_subscribe, nil)
          rescue Exception  => e
            errorsms = true
            begin
              Rails.logger.error("ERROR-SMS: #{e}")
            rescue
            end
            puts "ERROR-SMS: #{e}"
          end
        end
        if errormail || errorsms
          export  = I18n.t("error_message_sending_rt_email") if errormail && !errorsms
          export  = I18n.t("error_message_sending_rt_phone") if !errormail && errorsms
          export  = I18n.t("error_message_sending_rt") if errormail && errorsms
          puts export
        else
          puts I18n.t('success_message_sending')
        end
      else
        puts status_manager.show_errors
      end
      puts "*********************************************************************"
    end
    puts "================================================================"
  end

  desc "Enviar cuestionario de Madrid te acompaña"
  task citizen_services: :environment do 
   if Rails.env.production? || Rails.env.test?
      data = CitizenService.questions_stream_query_rows_export(CitizenService.not_market)

      destinatary= Setting.find_by(key: 'ListaDeCorreosEnvioCuestionario').blank? ? "vulnerabilidadm@madrid.es" : Setting.find_by(key: 'ListaDeCorreosEnvioCuestionario').try(:value)
      
      [destinatary, "madridvoluntariado@madrid.es", "ajmorenoh@minsait.com","rybarczykpm@madrid.es"].each do |toemail|
        begin
          if !toemail.blank?
            puts VolunteerMailer.send_cuestionary(toemail, sender: "", subject: "Envío automático de cuestionarios Madrid te acompaña", message: "", upload: data, type_upload: "csv", name_upload: "Cuestionarios.csv").deliver_now
            #AuditGenerateHelper.audit_send_mail(, nil)
            puts I18n.t('send_questions_success')
          else
            puts I18n.t('send_questions_no_mail') 
          end
        rescue Exception  => e
          Rails.logger.error('VolunteersController#send_mail') do
            "#{I18n.t('mailer.error')}: \n#{e}"
          end
          puts I18n.t('alert_message_sending')
          puts "#{I18n.t('mailer.error')}: \n#{e}"
        end
      end
    else
      puts "No es el entorno de producción"
    end
  end

end #fin namespace