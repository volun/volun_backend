require "rails_helper"

RSpec.describe VolunteersHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volunteer
  }

  it "stream_query_rows" do    
    expect(Volunteer.stream_query_rows(Volunteer.all)).not_to eq(nil)
  end     
end