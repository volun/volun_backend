require "rails_helper"

RSpec.describe RtVolunteerAppointmentHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volunteer_appointment
  }

  it "stream_query_rows" do
    item = Rt::VolunteerAppointment.create! valid_attributes
    
    expect(Rt::VolunteerAppointment.stream_query_rows(Rt::VolunteerAppointment.all)).not_to eq(nil)
  end     
end