require "rails_helper"

RSpec.describe PermissionsHelper, :type => :helper do

    it "get_array_value" do
      expect(helper.get_array_value("tracking")).not_to eq(nil)
      expect(helper.get_array_value("request")).not_to eq(nil)
      expect(helper.get_array_value("generics")).not_to eq(nil)
      expect(helper.get_array_value("other")).not_to eq(nil)
    end

    it "permissions_new" do
      expect(helper.permissions_new).not_to eq(nil)
    end

    it "find_or_generate_permission" do
      expect(helper.find_or_generate_permission(Manager.all.first, "Permiso")).not_to eq(nil)
    end
end