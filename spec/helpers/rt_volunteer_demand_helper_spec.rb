require "rails_helper"

RSpec.describe RtVolunteerDemandHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volunteers_demand
  }

  it "stream_query_rows" do
    item = Rt::VolunteersDemand.create! valid_attributes
    
    expect(Rt::VolunteersDemand.stream_query_rows(Rt::VolunteersDemand.all)).not_to eq(nil)
  end     
end