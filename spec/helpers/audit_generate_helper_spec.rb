require 'rails_helper'

RSpec.describe AuditGenerateHelper, :type => :helper do
    let(:user) { build(:user) }
       
    it "audit_clone" do
        expect(AuditGenerateHelper.audit_clone({id: 1, id_clon: 1}, user)).not_to eq(nil)
    end 
    
    it "audit_unlink_all" do
        expect(AuditGenerateHelper.audit_unlink_all(Project.all.first, user)).not_to eq(nil)
    end 

    it "audit_publish" do
        expect(AuditGenerateHelper.audit_publish(Project.all.first, user)).not_to eq(nil)
    end 

    it "audit_unpublish" do
        expect(AuditGenerateHelper.audit_unpublish(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_request_appointed" do
        expect(AuditGenerateHelper.audit_request_appointed(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_request_accept" do
        expect(AuditGenerateHelper.audit_request_accept(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_request_reject" do
        expect(AuditGenerateHelper.audit_request_reject(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_request_accept" do
        expect(AuditGenerateHelper.audit_request_accept(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_unlink" do
        expect(AuditGenerateHelper.audit_unlink({id: 1, id_volun: 1}, user)).not_to eq(nil)
    end

    it "audit_send_mail" do
        expect(AuditGenerateHelper.audit_send_mail(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_send_sms" do
        expect(AuditGenerateHelper.audit_send_sms(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_unassignment" do
        expect(AuditGenerateHelper.audit_unassignment(Project.all.first, user)).not_to eq(nil)
    end

    it "audit_assignment" do
        expect(AuditGenerateHelper.audit_assignment(Project.all.first, user)).not_to eq(nil)
    end
end