require 'rails_helper'

RSpec.describe ActivitiesHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :activity
    }

    it "stream_query_rows" do
        activity = Activity.create! valid_attributes
        
        expect(Activity.stream_query_rows(Activity.all)).not_to eq(nil)
    end   
end