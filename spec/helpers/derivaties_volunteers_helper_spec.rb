require 'rails_helper'

RSpec.describe DerivatiesVolunteersHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :derivaties_volunteer
    }

    it "stream_query_rows" do
        item = DerivatiesVolunteer.create! valid_attributes
        
        expect(DerivatiesVolunteer.stream_query_rows(DerivatiesVolunteer.all)).not_to eq(nil)
    end   
end