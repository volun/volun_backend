require "rails_helper"

RSpec.describe RtActivityPublishingHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :activity_publishing
  }

  it "stream_query_rows" do
    item = Rt::ActivityPublishing.create! valid_attributes
    
    expect(Rt::ActivityPublishing.stream_query_rows(Rt::ActivityPublishing.all)).not_to eq(nil)
  end     
end