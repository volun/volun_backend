require 'rails_helper'

RSpec.describe ProjectsHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :project
    }

    it "stream_query_rows" do
        item = Project.create! valid_attributes
        
        expect(Project.stream_query_rows(Project.all)).not_to eq(nil)
    end   
end