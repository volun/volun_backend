require "rails_helper"

RSpec.describe VolunTrackingsHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volun_tracking
  }

  it "stream_query_rows" do
    item = Volun::Tracking.create! valid_attributes
    
    expect(Volun::Tracking.stream_query_rows(Volun::Tracking.all)).not_to eq(nil)
  end     
end