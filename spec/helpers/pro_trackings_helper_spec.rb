require 'rails_helper'

RSpec.describe ProTrackingsHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :pro_tracking
    }

    it "stream_query_rows" do
        item = Pro::Tracking.create! valid_attributes
        
        expect(Pro::Tracking.stream_query_rows(Pro::Tracking.all)).not_to eq(nil)
    end   
end