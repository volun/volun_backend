require 'rails_helper'

RSpec.describe EntTrackingsHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :ent_tracking
    }

    it "stream_query_rows" do
        item = Ent::Tracking.create! valid_attributes
        
        expect(Ent::Tracking.stream_query_rows(Ent::Tracking.all)).not_to eq(nil)
    end   
end