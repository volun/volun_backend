require 'rails_helper'

RSpec.describe AuditsHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :audit
    }

    it "stream_query_rows" do
        audit = Audit.create! valid_attributes
        
        expect(Audit.stream_query_rows(Audit.all)).not_to eq(nil)
    end   
end