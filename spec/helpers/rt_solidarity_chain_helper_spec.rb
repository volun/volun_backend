require "rails_helper"

RSpec.describe RtSolidarityChainHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :rt_solidarity_chain
  }

  it "stream_query_rows" do
    item = Rt::SolidarityChain.create! valid_attributes
    
    expect(Rt::SolidarityChain.stream_query_rows(Rt::SolidarityChain.all)).not_to eq(nil)
  end     
end