require "rails_helper"

RSpec.describe RtProjectPublishingHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :project_publishing
  }

  it "stream_query_rows" do
    item = Rt::ProjectPublishing.create! valid_attributes
    
    expect(Rt::ProjectPublishing.stream_query_rows(Rt::ProjectPublishing.all)).not_to eq(nil)
  end     
end