require "rails_helper"

RSpec.describe RtVolunteerAmendmentHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volunteer_amendment
  }

  it "stream_query_rows" do
    item = Rt::VolunteerAmendment.create! valid_attributes
    
    expect(Rt::VolunteerAmendment.stream_query_rows(Rt::VolunteerAmendment.all)).not_to eq(nil)
  end     
end