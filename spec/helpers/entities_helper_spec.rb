require 'rails_helper'

RSpec.describe EntitiesHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :entity
    }

    it "stream_query_rows" do
        item = Entity.create! valid_attributes
        
        expect(Entity.stream_query_rows(Entity.all)).not_to eq(nil)
    end   
end