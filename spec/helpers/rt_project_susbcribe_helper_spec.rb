require "rails_helper"

RSpec.describe RtVolunteerSubscribeHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :volunteer_subscribe
  }

  it "stream_query_rows" do
    item = Rt::VolunteerSubscribe.create! valid_attributes
    
    expect(Rt::VolunteerSubscribe.stream_query_rows(Rt::VolunteerSubscribe.all)).not_to eq(nil)
  end     
end