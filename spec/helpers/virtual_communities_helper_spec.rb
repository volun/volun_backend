require "rails_helper"

RSpec.describe VirtualCommunitiesHelper, :type => :helper do

    it "traduction_route" do
      expect(helper.traduction_route).to eq("")
      expect(helper.traduction_route("localhost:3000")).not_to eq("")
      expect(helper.traduction_route("/15")).not_to eq("")
      expect(helper.traduction_route("/hola.csv")).not_to eq("")
      expect(helper.traduction_route("/hola")).not_to eq("")
    end
end