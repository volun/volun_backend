require "rails_helper"

RSpec.describe RtVolunteerSubscribeHelper, :type => :helper do

  let(:valid_attributes) {
    attributes_for :rt_volunteer_project_subscribe
  }

  it "stream_query_rows" do
    item = Rt::VolunteerProjectSubscribe.create! valid_attributes
    
    expect(Rt::VolunteerProjectSubscribe.stream_query_rows(Rt::VolunteerProjectSubscribe.all)).not_to eq(nil)
  end     
end