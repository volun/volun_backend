require 'rails_helper'

RSpec.describe RtOtherHelper, :type => :helper do
   
    let(:valid_attributes) {
        attributes_for :rt_other
    }

    it "stream_query_rows" do
        item = Rt::Other.create! valid_attributes
        
        expect(Rt::Other.stream_query_rows(Rt::Other.all)).not_to eq(nil)
    end   
end