require 'rails_helper'

RSpec.describe Pt::SocialsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :social
  }

  let(:invalid_attributes) {
    attributes_for :social, :invalid
  }

  describe "GET #index" do
    it 'assigns all pt_socials as @pt_socials' do
      social = Pt::Social.find(1)
      get :index
      expect(assigns(:pt_socials)).to eq([social])
    end
  end

  describe "GET #show" do
    it 'assigns the requested pt_social as @pt_social' do
      social = Pt::Social.find(1)
      get :show, id: social.to_param
      expect(assigns(:pt_social)).to eq(social)
    end
  end

  describe "GET #new" do
    it 'assigns a new pt_social as @pt_social' do
      get :new
      expect(assigns(:pt_social)).to be_a_new(Pt::Social)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested pt_social as @pt_social' do
      social = Pt::Social.find(1)
      get :edit, id: social.to_param
      expect(assigns(:pt_social)).to eq(social)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Pt::Social' do
        expect {
          post :create, pt_social: valid_attributes
        }.to change(Pt::Social, :count).by(0)
      end

      it 'assigns a newly created pt_social as @pt_social' do
        post :create, pt_social: valid_attributes
        expect(assigns(:pt_social)).to be_a(Pt::Social)
        expect(assigns(:pt_social)).not_to be_persisted
      end

      it 'redirects to the created pt_social' do
        post :create, pt_social: valid_attributes
        expect(response.status).to eq(200)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
     it 'assigns the requested pt_social as @pt_social' do
        social = Pt::Social.find(1)
        put :update, id: social.to_param, pt_social: valid_attributes
        expect(assigns(:pt_social)).to eq(social)
      end

      it 'redirects to pt_socials' do
        social = Pt::Social.find(1)
        put :update, id: social.to_param, pt_social: valid_attributes
        expect(response).to redirect_to(projects_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the pt_social as @pt_social' do
        social = Pt::Social.find(1)
        put :update, id: social.to_param, pt_social: invalid_attributes
        expect(assigns(:pt_social)).to eq(social)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested pt_social' do
      social = Pt::Social.find(1)
      expect {
        delete :destroy, id: social.to_param
      }.to change(Pt::Social, :count).by(social.respond_to?(:archived?) ? -1 : 0)
    end

    it 'redirects to the pt_socials list' do
      social = Pt::Social.find(1)
      delete :destroy, id: social.to_param
      expect(response).to redirect_to(pt_socials_url)
    end
  end

end
