require 'rails_helper'

RSpec.describe Pt::RetiredVolunteersController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :pt_retired_volunteer
  }

  let(:invalid_attributes) {
    attributes_for :pt_retired_volunteer, :invalid
  }

  describe "GET #index" do
    it 'assigns all pt_retired_volunteers as @pt_retired_volunteers' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
      get :index
      expect(assigns(:pt_retired_volunteers)).to eq([retired_volunteer])
    end
  end

  describe "GET #show" do
    it 'assigns the requested pt_retired_volunteer as @pt_retired_volunteer' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
      get :show, id: retired_volunteer.to_param
      expect(assigns(:pt_retired_volunteer)).to eq(retired_volunteer)
    end
  end

  describe "GET #new" do
    it 'assigns a new pt_retired_volunteer as @pt_retired_volunteer' do
      get :new
      expect(assigns(:pt_retired_volunteer)).to be_a_new(Pt::RetiredVolunteer)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested pt_retired_volunteer as @pt_retired_volunteer' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
      get :edit, id: retired_volunteer.to_param
      expect(assigns(:pt_retired_volunteer)).to eq(retired_volunteer)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Pt::RetiredVolunteer' do
        expect {
          post :create, pt_retired_volunteer: valid_attributes
        }.to change(Pt::RetiredVolunteer, :count).by(0)
      end

      it 'assigns a newly created pt_retired_volunteer as @pt_retired_volunteer' do
        post :create, pt_retired_volunteer: valid_attributes
        expect(assigns(:pt_retired_volunteer)).to be_a(Pt::RetiredVolunteer)
        expect(assigns(:pt_retired_volunteer)).not_to be_persisted
      end

      it 'redirects to the created pt_retired_volunteer' do
        post :create, pt_retired_volunteer: valid_attributes
        expect(response.status).to eq(200)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
     it 'assigns the requested pt_retired_volunteer as @pt_retired_volunteer' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
        put :update, id: retired_volunteer.to_param, pt_retired_volunteer: valid_attributes
        expect(assigns(:pt_retired_volunteer)).to eq(retired_volunteer)
      end

      it 'redirects to pt_retired_volunteers' do
        retired_volunteer = Pt::RetiredVolunteer.find(1)
        put :update, id: retired_volunteer.to_param, pt_retired_volunteer: valid_attributes
        expect(response).to redirect_to(projects_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the pt_retired_volunteer as @pt_retired_volunteer' do
        retired_volunteer = Pt::RetiredVolunteer.find(1)
        put :update, id: retired_volunteer.to_param, pt_retired_volunteer: invalid_attributes
        expect(assigns(:pt_retired_volunteer)).to eq(retired_volunteer)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested pt_retired_volunteer' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
      expect {
        delete :destroy, id: retired_volunteer.to_param
      }.to change(Pt::RetiredVolunteer, :count).by(retired_volunteer.respond_to?(:archived?) ? -1 : 0)
    end

    it 'redirects to the pt_retired_volunteers list' do
      retired_volunteer = Pt::RetiredVolunteer.find(1)
      delete :destroy, id: retired_volunteer.to_param
      expect(response).to redirect_to(pt_retired_volunteers_url)
    end
  end

end
