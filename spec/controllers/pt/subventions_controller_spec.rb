require 'rails_helper'

RSpec.describe Pt::SubventionsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :subvention
  }

  let(:invalid_attributes) {
    attributes_for :subvention, :invalid
  }

  let(:project_attributes){
    attributes_for :project
  }

  describe "GET #index" do
    it 'assigns all pt_subventions as @pt_subventions' do
      project_attributes[:project_type_id]= 6
      project_attributes[:pt_extendable_id]= 7
      project_attributes[:pt_extendable_type]= 'Pt::Subvention'
      project = Project.create! project_attributes
      subvention = Pt::Subvention.create valid_attributes
      get :index
      expect(assigns(:pt_subventions).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested pt_subvention as @pt_subvention' do
      subvention = Pt::Subvention.find(1)
      get :show, id: subvention.to_param
      expect(assigns(:pt_subvention)).to eq(subvention)
    end
  end

  describe "GET #new" do
    it 'assigns a new pt_subvention as @pt_subvention' do
      get :new
      expect(assigns(:pt_subvention)).to be_a_new(Pt::Subvention)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested pt_subvention as @pt_subvention' do
      subvention = Pt::Subvention.find(1)
      get :edit, id: subvention.to_param
      expect(assigns(:pt_subvention)).to eq(subvention)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Pt::Subvention' do
        expect {
          project_attributes[:project_type_id]= 6
          project_attributes[:pt_extendable_id]= 7
          project_attributes[:pt_extendable_type]= 'Pt::Subvention'
          project = Project.create! project_attributes
          post :create, pt_subvention: valid_attributes
        }.to change(Pt::Subvention, :count).by(0)
      end

      it 'assigns a newly created pt_subvention as @pt_subvention' do
        project_attributes[:project_type_id]= 6
        project_attributes[:pt_extendable_id]= 7
        project_attributes[:pt_extendable_type]= 'Pt::Subvention'
        project = Project.create! project_attributes
        post :create, pt_subvention: valid_attributes
        expect(assigns(:pt_subvention)).to be_a(Pt::Subvention)
        expect(assigns(:pt_subvention)).not_to be_persisted
      end

      it 'redirects to the created pt_subvention' do
        project_attributes[:project_type_id]= 6
        project_attributes[:pt_extendable_id]= 7
        project_attributes[:pt_extendable_type]= 'Pt::Subvention'
        project = Project.create! project_attributes
        post :create, pt_subvention: valid_attributes
        expect(response.status).to eq(200)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested pt_subvention as @pt_subvention' do
        subvention = Pt::Subvention.find(1)
        put :update, id: subvention.to_param, pt_subvention: valid_attributes
        expect(assigns(:pt_subvention)).to eq(subvention)
      end

      it 'redirects to pt_subventions' do
        subvention = Pt::Subvention.find(1)
        put :update, id: subvention.to_param, pt_subvention: valid_attributes
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid params' do
      it 'assigns the pt_subvention as @pt_subvention' do
        subvention = Pt::Subvention.find(1)
        put :update, id: subvention.to_param, pt_subvention: invalid_attributes
        expect(assigns(:pt_subvention)).to eq(subvention)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested pt_subvention' do
      subvention = Pt::Subvention.find(1)
      expect {
        delete :destroy, id: subvention.to_param
      }.to change(Pt::Subvention, :count).by(subvention.respond_to?(:archived?) ? -1 : 0)
    end

    it 'redirects to the pt_subventions list' do
      subvention = Pt::Subvention.find(1)
      delete :destroy, id: subvention.to_param
      expect(response).to redirect_to(pt_subventions_url)
    end
  end

end
