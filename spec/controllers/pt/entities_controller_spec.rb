require 'rails_helper'

RSpec.describe Pt::EntitiesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :pt_entity
  }

  let(:invalid_attributes) {
    attributes_for :pt_entity, :invalid
  }

  describe "GET #index" do
    it 'assigns all pt_entities as @pt_entities' do
      entity = Pt::Entity.find(1)
      get :index
      expect(assigns(:pt_entities)).to eq([entity])
    end
  end

  describe "GET #show" do
    it 'assigns the requested pt_entity as @pt_entity' do
      entity = Pt::Entity.find(1)
      get :show, id: entity.to_param
      expect(assigns(:pt_entity)).to eq(entity)
    end
  end

  describe "GET #new" do
    it 'assigns a new pt_entity as @pt_entity' do
      get :new
      expect(assigns(:pt_entity)).to be_a_new(Pt::Entity)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested pt_entity as @pt_entity' do
      entity = Pt::Entity.find(1)
      get :edit, id: entity.to_param
      expect(assigns(:pt_entity)).to eq(entity)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Pt::Entity' do
        expect {
          post :create, pt_entity: valid_attributes
        }.to change(Pt::Entity, :count).by(0)
      end

      it 'assigns a newly created pt_entity as @pt_entity' do
        post :create, pt_entity: valid_attributes
        expect(assigns(:pt_entity)).to be_a(Pt::Entity)
        expect(assigns(:pt_entity)).not_to be_persisted
      end

      it 'redirects to the created pt_entity' do
        post :create, pt_entity: valid_attributes
        expect(response.status).to eq(200)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
     it 'assigns the requested pt_entity as @pt_entity' do
        entity = Pt::Entity.find(1)
        put :update, id: entity.to_param, pt_entity: valid_attributes
        expect(assigns(:pt_entity)).to eq(entity)
      end

      it 'redirects to pt_entities' do
        entity = Pt::Entity.find(1)
        put :update, id: entity.to_param, pt_entity: valid_attributes
        expect(response).to redirect_to(projects_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the pt_entity as @pt_entity' do
        entity = Pt::Entity.find(1)
        put :update, id: entity.to_param, pt_entity: invalid_attributes
        expect(assigns(:pt_entity)).to eq(entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested pt_entity' do
      entity = Pt::Entity.find(1)
      expect {
        delete :destroy, id: entity.to_param
      }.to change(Pt::Entity, :count).by(entity.respond_to?(:archived?) ? -1 : 0)
    end

    it 'redirects to the pt_entities list' do
      entity = Pt::Entity.find(1)
      delete :destroy, id: entity.to_param
      expect(response).to redirect_to(pt_entities_url)
    end
  end

end
