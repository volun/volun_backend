require 'rails_helper'

RSpec.describe EntitiesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :entity
  }

  let(:invalid_attributes) {
    attributes_for :entity, :invalid
  }

  let(:user_atributes){
    attributes_for :user, :entity_log
  }

  describe "GET #index" do
    it 'assigns all entities as @entities' do
      entity = Entity.create! valid_attributes
      get :index
      expect(assigns(:entities).count).not_to eq(0)
    end

    it 'assigns all entities as @entities with params' do
      entity = Entity.create! valid_attributes
      get :index, search_status: "active"
      expect(assigns(:entities).count).not_to eq(0)
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'assigns the requested entity as @entity' do
      entity = Entity.create! valid_attributes
      get :show, id: entity.to_param
      expect(assigns(:entity)).to eq(entity)
    end
  end

  describe "GET #new" do
    it 'assigns a new entity as @entity' do
      get :new
      expect(assigns(:entity)).to be_a_new(Entity)
    end
  end

  describe "GET #new_password" do
    it 'assigns a new password as @entity' do
      entity = Entity.create! valid_attributes
      get :new_password, id: entity.to_param
      expect(response.status).to eq(200)
    end
  end

  describe "GET #ent_password" do
    it 'assigns a new password as @entity' do
      entity = Entity.create! valid_attributes
      get :ent_password, id: entity.to_param
      expect(response.status).to eq(200)
    end
  end

  describe "GET #update_password" do
    it 'assigns a update password as @entity' do
      
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "message",email: entity.email
      expect(response.status).to eq(302)
    end

    it 'assigns a update password as @entity without message'  do
      
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "",email: entity.email
      expect(response).to redirect_to edit_entity_path(entity.id)
    end

    it 'assigns a update password as @entity with mail' do
     
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "message",send_mail: 1,email: entity.email
      expect(response.status).to eq(302)
    end

    it 'assigns a update password as @entity without mail' do
     
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "message",send_mail: 1,email: ""
      expect(response).to redirect_to edit_entity_path(entity.id)
    end

    it 'assigns a update password as @entity with sms' do
      
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "message",send_sms: 1,mobile_phone: "678945656",email: entity.email
      expect(response.status).to eq(302)
    end

    it 'assigns a update password as @entity without sms' do
      
      entity = Entity.create! valid_attributes
      user=User.create! user_atributes
      get :update_password, entity: entity.to_param,message: "message",send_sms: 1,mobile_phone: "",email: entity.email
      expect(response).to redirect_to edit_entity_path(entity.id)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested entity as @entity' do
      entity = Entity.create! valid_attributes
      get :edit, id: entity.to_param
      expect(assigns(:entity)).to eq(entity)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Entity' do
        expect {
          post :create, entity: valid_attributes
        }.to change(Entity, :count).by(0)
      end

      it 'assigns a newly created entity as @entity' do
        post :create, entity: valid_attributes
        expect(assigns(:entity)).to be_a(Entity)
        expect(assigns(:entity)).not_to be_persisted
      end

      it 'redirects to the created entity' do
        post :create, entity: valid_attributes
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved entity as @entity' do
        post :create, entity: invalid_attributes
        expect(assigns(:entity)).to be_a_new(Entity)
      end

      it 're-renders the "new" template' do
        post :create, entity: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested entity as @entity' do
        entity = Entity.create! valid_attributes
        put :update, id: entity.to_param, entity: valid_attributes
        expect(assigns(:entity)).to eq(entity)
      end

      it 'assigns the requested entity as @entity unsubscribed' do
        entity = Entity.create! valid_attributes
        put :update, id: entity.to_param, entity: valid_attributes.merge({unsubscribed_at: "2018-02-12 15:19:49"})
        expect(assigns(:entity)).to eq(entity)
      end

      it 'assigns the requested entity as @entity with destroy' do
        entity = Entity.create! valid_attributes
        delete :destroy, id: entity.to_param
        put :update, id: entity.to_param, entity: valid_attributes
        expect(assigns(:entity)).to eq(entity)
      end

      it 'redirects to entities' do
        entity = Entity.create! valid_attributes
        put :update, id: entity.to_param, entity: valid_attributes
        expect(response).to redirect_to(entities_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the entity as @entity' do
        entity = Entity.create! valid_attributes
        put :update, id: entity.to_param, entity: invalid_attributes
        expect(assigns(:entity)).to eq(entity)
      end

      it 're-renders the "edit" template' do
        entity = Entity.create! valid_attributes
        put :update, id: entity.to_param, entity: invalid_attributes
        expect(response).to render_template('edit')
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested entity' do
      entity = Entity.create! valid_attributes
      expect {
        delete :destroy, id: entity.to_param
      }.to change(Entity, :count).by(entity.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the entities list' do
      entity = Entity.create! valid_attributes
      delete :destroy, id: entity.to_param
      expect(response).to redirect_to(entities_url)
    end
  end

  describe "POST #recover" do
    it 'recovers the requested entity' do
      entity = Entity.create! valid_attributes
      delete :destroy, id: entity.to_param
      post :recover, id: entity.to_param
      expect(entity.active).to eq(true)
    end

    it 'redirects to the entities list' do
      entity = Entity.create! valid_attributes
      post :recover, id: entity.to_param
      expect(response).to redirect_to(entities_url)
    end
  end
end
