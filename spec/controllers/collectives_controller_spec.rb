require 'rails_helper'

RSpec.describe CollectivesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :collective
  }

  let(:invalid_attributes) {
    attributes_for :collective, :invalid
  }

  describe "GET #index" do
    it 'assigns all collectives as @collectives' do
      collective = Collective.create! valid_attributes
      get :index
      expect(assigns(:collectives).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested collective as @collective' do
      collective = Collective.create! valid_attributes
      get :show, id: collective.to_param
      expect(assigns(:collective)).to eq(collective)
    end
  end

  describe "GET #new" do
    it 'assigns a new collective as @collective' do
      get :new
      expect(assigns(:collective)).to be_a_new(Collective)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested collective as @collective' do
      collective = Collective.create! valid_attributes
      get :edit, id: collective.to_param
      expect(assigns(:collective)).to eq(collective)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Collective' do
        expect {
          post :create, collective: valid_attributes
        }.to change(Collective, :count).by(1)
      end

      it 'assigns a newly created collective as @collective' do
        post :create, collective: valid_attributes
        expect(assigns(:collective)).to be_a(Collective)
        expect(assigns(:collective)).to be_persisted
      end

      it 'redirects to the created collective' do
        post :create, collective: valid_attributes
        expect(response).to redirect_to(collectives_url)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested collective as @collective' do
        collective = Collective.create! valid_attributes
        put :update, id: collective.to_param, collective: valid_attributes
        expect(assigns(:collective)).to eq(collective)
      end

      it 'redirects to collectives' do
        collective = Collective.create! valid_attributes
        put :update, id: collective.to_param, collective: valid_attributes
        expect(response).to redirect_to(collectives_url)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested collective' do
      collective = Collective.create! valid_attributes
      expect {
        delete :destroy, id: collective.to_param
      }.to change(Collective, :count).by(collective.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the collectives list' do
      collective = Collective.create! valid_attributes
      delete :destroy, id: collective.to_param
      expect(response).to redirect_to(collectives_url)
    end
  end
  
  describe "RECOVER #recover" do
    it 'recovers the requested collective' do
      collective = Collective.create! valid_attributes
      delete :destroy, id: collective.to_param
      post :recover, id: collective.to_param
      expect(collective.active).to eq(true)
    end

    it 'redirects to the collectives list' do
      collective = Collective.create! valid_attributes
      post :recover, id: collective.to_param
      expect(response).to redirect_to(collectives_url)
    end
  end

end
