require 'rails_helper'

RSpec.describe RolesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :role
  }

  let(:invalid_attributes) {
    attributes_for :role, :invalid
  }

  describe "GET #index" do
    it 'assigns all roles as @roles' do
      role = Role.find(0)
      get :index
      expect(assigns(:roles).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested role as @role' do
      role = Role.find(0)
      get :show, id: role.to_param
      expect(assigns(:role)).to eq(role)
    end
  end

  describe "GET #new" do
    it 'assigns a new role as @role' do
      get :new
      expect(assigns(:role)).to be_a_new(Role)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested role as @role' do
      role = Role.find(0)
      get :edit, id: role.to_param
      expect(assigns(:role)).to eq(role)
    end
  end

  # describe "POST #create" do
  #   context 'with valid params' do
  #     it 'creates a new role' do
  #       expect {
  #         post :create, role: valid_attributes
  #       }.to change(Role, :count).by(1)
  #     end

  #     it 'assigns a newly created role as @role' do
  #       post :create, role: valid_attributes
  #       expect(assigns(:role)).to be_a(Role)
  #       expect(assigns(:role)).to be_persisted
  #     end

  #     it 'redirects to the created role' do
  #       post :create, role: valid_attributes
  #       expect(response).to redirect_to(roles_url)
  #     end
  #   end
  # end

  # describe "PUT #update" do
  #   context 'with valid params' do
  #     it 'assigns the requested role as @role' do
  #       role = Role.find(0)
  #       put :update, id: role.to_param, role: valid_attributes
  #       expect(assigns(:role)).to eq(role)
  #     end

  #     it 'redirects to roles' do
  #       role = Role.find(0)
  #       put :update, id: role.to_param, role: valid_attributes
  #       expect(response).to redirect_to(roles_url)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns the role as @role' do
  #       role = Role.find(0)
  #       put :update, id: role.to_param, role: invalid_attributes
  #       expect(assigns(:role)).to eq(role)
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested role' do
  #     role = Role.find(0)
  #     expect {
  #       delete :destroy, id: role.to_param
  #     }.to change(Role, :count).by(role.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the roles list' do
  #     role = Role.find(0)
  #     delete :destroy, id: role.to_param
  #     expect(response).to redirect_to(roles_url)
  #   end
  # end
end
