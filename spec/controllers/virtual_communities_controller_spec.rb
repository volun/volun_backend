require 'rails_helper'

RSpec.describe VirtualCommunitiesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :virtual_community
  }

  let(:invalid_attributes) {
    attributes_for :virtual_community, :invalid
  }

  describe "GET #index" do
    it 'assigns all virtual_communities as @virtual_communities' do
      virtual_community = VirtualCommunity.create! valid_attributes
      get :index
      expect(assigns(:virtual_communities).count).not_to eq(0)
    end
  end

  

end
