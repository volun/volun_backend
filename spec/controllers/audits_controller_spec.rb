require 'rails_helper'

RSpec.describe AuditsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :audit
  }

  let(:invalid_attributes) {
    attributes_for :audit, :invalid
  }

  describe "GET #index" do
    it 'assigns all audits as @audits' do
      audit = Audit.create! valid_attributes
      get :index
      expect(assigns(:audits)).to eq([audit])
    end

    it 'assigns all audits as @audits with params :q' do
      audit = Audit.create! valid_attributes
      get :index, q:{sort: ""}
      expect(assigns(:audits)).to eq([audit])
    end
  end

  describe "GET #show" do
    it 'assigns the requested audit as @audit' do
      audit = Audit.create! valid_attributes
      get :show, id: audit.to_param
      expect(assigns(:audit)).to eq(audit)
    end
  end

end
