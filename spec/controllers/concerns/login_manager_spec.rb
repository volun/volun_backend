require 'rails_helper'

RSpec.describe LoginManager, type: :model do



  it 'valid?' do
    login_manager = LoginManager.new({login_data: {login: "XX", uweb_id: "XX", email: "xx@xx.es", document: "xxxxx"}})
    
    expect(login_manager.valid?).not_to eq(nil)

    login_manager = LoginManager.new({login_data: {uweb_id: "XX", email: "xx@xx.es", document: "xxxxx"}})
    
    expect(login_manager.valid?).not_to eq(nil)
  end

  it 'find_or_create_user' do
    login_manager = LoginManager.new({login_data: {login: "XX", uweb_id: "XX", email: "xx@xx.es", document: "xxxxx"}})
    
    expect(login_manager.find_or_create_user).to eq(nil)
  end

  it 'find_or_create_manager' do
    login_manager = LoginManager.new({login_data: {login: "XX", uweb_id: "XX", email: "xx@xx.es", document: "xxxxx"}})
    
    expect(login_manager.find_or_create_manager).not_to eq(nil)
  end


 

end
