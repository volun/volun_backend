require 'rails_helper'

RSpec.describe RtController, type: :model do

  describe "StatusManager" do

    it 'attributes' do
      status = RtController::StatusManager.new
      
      expect(status.rejection_type).to eq(nil)
      expect(status.request_form).to eq(nil)
      expect(status.reject_request_form).not_to eq(nil)
      expect(status.process_request_form).not_to eq(nil)
      expect(status.recently_processed_by_different_manager?).to eq(nil)
      expect(status.different_manager?).not_to eq(nil)
      #expect(status.recently_processed?).not_to eq(nil)
      expect(status.mark_request_form_as_pending).not_to eq(nil)
      expect(status.mark_request_form_as_processing).not_to eq(nil)
      expect(status.mark_request_form_as_appointed).not_to eq(nil)
      expect(status.show_errors).not_to eq(nil)
    end
  end
end
