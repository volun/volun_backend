require 'rails_helper'

RSpec.describe Req::StatusesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :req_status
  }

  let(:invalid_attributes) {
    attributes_for :req_status, :invalid
  }

  describe "GET #index" do
    it 'assigns all req_statuses as @req_statuses' do
      status = Req::Status.find(1)
      get :index
      expect(assigns(:req_statuses).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested req_status as @req_status' do
      status = Req::Status.find(1)
      get :show, id: status.to_param
      expect(assigns(:req_status)).to eq(status)
    end
  end

  describe "GET #new" do
    it 'assigns a new req_status as @req_status' do
      get :new
      expect(assigns(:req_status)).to be_a_new(Req::Status)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested req_status as @req_status' do
      status = Req::Status.find(1)
      get :edit, id: status.to_param
      expect(assigns(:req_status)).to eq(status)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Req::Status' do
        expect {
          post :create, req_status: valid_attributes
        }.to change(Req::Status, :count).by(1)
      end

      it 'assigns a newly created req_status as @req_status' do
        post :create, req_status: valid_attributes
        expect(assigns(:req_status)).to be_a(Req::Status)
        expect(assigns(:req_status)).to be_persisted
      end

      it 'redirects to the created req_status' do
        post :create, req_status: valid_attributes
        expect(response).to redirect_to(req_statuses_url)
      end
    end

    
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested req_status as @req_status' do
        status = Req::Status.find(1)
        put :update, id: status.to_param, req_status: valid_attributes
        expect(assigns(:req_status)).to eq(status)
      end

      it 'redirects to req_statuses' do
        status = Req::Status.find(1)
        put :update, id: status.to_param, req_status: valid_attributes
        expect(response).to redirect_to(req_statuses_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the req_status as @req_status' do
        status = Req::Status.find(1)
        put :update, id: status.to_param, req_status: invalid_attributes
        expect(assigns(:req_status)).to eq(status)
      end

      
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested req_status' do
      status = Req::Status.find(1)
      expect {
        delete :destroy, id: status.to_param
      }.to change(Req::Status, :count).by(status.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the req_statuses list' do
      status = Req::Status.find(1)
      delete :destroy, id: status.to_param
      expect(response).to redirect_to(req_statuses_url)
    end
  end
end
