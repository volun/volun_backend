require 'rails_helper'

RSpec.describe Req::StatusTracesController, type: :controller do
  let(:request_form_attributes){
    attributes_for :request_form
  }
  
  
  before(:each) do
    @user=create(:user)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)

    volunteers_demand = Rt::VolunteersDemand.create! (attributes_for :volunteers_demand)
    request_form_attributes[:req_reason_id]=@req_reason.id
    request_form_attributes[:rt_extendable_id]=volunteers_demand.id
    request_form_attributes[:rt_extendable_type]="Rt::VolunteersDemand"
    request_form_attributes[:request_type_id]=7
    request_form_attributes[:user_id]=@user.id
    @request_form = RequestForm.create! request_form_attributes
  end

  let(:valid_attributes) {
    attributes_for :status_trace
  }

  let(:invalid_attributes) {
    attributes_for :status_trace, :invalid
  }

  describe "GET #index" do
    it 'assigns all req_status_traces as @req_status_traces' do
      valid_attributes[:request_form_id]=@request_form.id
      status_trace = Req::StatusTrace.create! valid_attributes
      get :index
      expect(assigns(:req_status_traces).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested req_status_trace as @req_status_trace' do
      valid_attributes[:request_form_id]=@request_form.id
      status_trace = Req::StatusTrace.create! valid_attributes
      get :show, id: status_trace.to_param
      expect(assigns(:req_status_trace)).to eq(status_trace)
    end
  end

  describe "GET #new" do
    it 'assigns a new req_status_trace as @req_status_trace' do
      get :new
      expect(assigns(:req_status_trace)).to be_a_new(Req::StatusTrace)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested req_status_trace as @req_status_trace' do
      valid_attributes[:request_form_id]=@request_form.id
      status_trace = Req::StatusTrace.create! valid_attributes
      get :edit, id: status_trace.to_param
      expect(assigns(:req_status_trace)).to eq(status_trace)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Req::StatusTrace' do
        expect {
          valid_attributes[:request_form_id]=@request_form.id
          post :create, req_status_trace: valid_attributes
        }.to change(Req::StatusTrace, :count).by(1)
      end

      it 'assigns a newly created req_status_trace as @req_status_trace' do
        valid_attributes[:request_form_id]=@request_form.id
        post :create, req_status_trace: valid_attributes
        expect(assigns(:req_status_trace)).to be_a(Req::StatusTrace)
        expect(assigns(:req_status_trace)).to be_persisted
      end

      it 'redirects to the created req_status_trace' do
        valid_attributes[:request_form_id]=@request_form.id
        post :create, req_status_trace: valid_attributes
        expect(response).to redirect_to(req_status_traces_url)
      end
    end

   
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested req_status_trace as @req_status_trace' do
        valid_attributes[:request_form_id]=@request_form.id
        status_trace = Req::StatusTrace.create! valid_attributes
        put :update, id: status_trace.to_param, req_status_trace: valid_attributes
        expect(assigns(:req_status_trace)).to eq(status_trace)
      end

      it 'redirects to req_status_traces' do
        valid_attributes[:request_form_id]=@request_form.id
        status_trace = Req::StatusTrace.create! valid_attributes
        put :update, id: status_trace.to_param, req_status_trace: valid_attributes
        expect(response).to redirect_to(req_status_traces_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the req_status_trace as @req_status_trace' do
        valid_attributes[:request_form_id]=@request_form.id
        invalid_attributes[:request_form_id]=@request_form.id
        status_trace = Req::StatusTrace.create! valid_attributes
        put :update, id: status_trace.to_param, req_status_trace: invalid_attributes
        expect(assigns(:req_status_trace)).to eq(status_trace)
      end

      
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested req_status_trace' do
      valid_attributes[:request_form_id]=@request_form.id
      status_trace = Req::StatusTrace.create! valid_attributes
      expect {
        delete :destroy, id: status_trace.to_param
      }.to change(Req::StatusTrace, :count).by(status_trace.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the req_status_traces list' do
      valid_attributes[:request_form_id]=@request_form.id
      status_trace = Req::StatusTrace.create! valid_attributes
      delete :destroy, id: status_trace.to_param
      expect(response).to redirect_to(req_status_traces_url)
    end
  end

end
