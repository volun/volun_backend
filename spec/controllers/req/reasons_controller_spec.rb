require 'rails_helper'

RSpec.describe Req::ReasonsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :reason
  }

  let(:invalid_attributes) {
    attributes_for :reason, :invalid
  }

  describe "GET #index" do
    it 'assigns all req_reasons as @req_reasons' do
      reason = Req::Reason.create! valid_attributes
      get :index
      expect(assigns(:req_reasons).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested req_reason as @req_reason' do
      reason = Req::Reason.create! valid_attributes
      get :show, id: reason.to_param
      expect(assigns(:req_reason)).to eq(reason)
    end
  end

  describe "GET #new" do
    it 'assigns a new req_reason as @req_reason' do
      get :new
      expect(assigns(:req_reason)).to be_a_new(Req::Reason)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested req_reason as @req_reason' do
      reason = Req::Reason.create! valid_attributes
      get :edit, id: reason.to_param
      expect(assigns(:req_reason)).to eq(reason)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Req::Reason' do
        expect {
          post :create, req_reason: valid_attributes
        }.to change(Req::Reason, :count).by(1)
      end

      it 'assigns a newly created req_reason as @req_reason' do
        post :create, req_reason: valid_attributes
        expect(assigns(:req_reason)).to be_a(Req::Reason)
        expect(assigns(:req_reason)).to be_persisted
      end

      it 'redirects to the created req_reason' do
        post :create, req_reason: valid_attributes
        expect(response).to redirect_to(req_reasons_url)
      end
    end

   
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested req_reason as @req_reason' do
        reason = Req::Reason.create! valid_attributes
        put :update, id: reason.to_param, req_reason: valid_attributes
        expect(assigns(:req_reason)).to eq(reason)
      end

      it 'redirects to req_reasons' do
        reason = Req::Reason.create! valid_attributes
        put :update, id: reason.to_param, req_reason: valid_attributes
        expect(response).to redirect_to(req_reasons_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the req_reason as @req_reason' do
        reason = Req::Reason.create! valid_attributes
        put :update, id: reason.to_param, req_reason: invalid_attributes
        expect(assigns(:req_reason)).to eq(reason)
      end

    
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested req_reason' do
      reason = Req::Reason.create! valid_attributes
      expect {
        delete :destroy, id: reason.to_param
      }.to change(Req::Reason, :count).by(reason.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the req_reasons list' do
      reason = Req::Reason.create! valid_attributes
      delete :destroy, id: reason.to_param
      expect(response).to redirect_to(req_reasons_url)
    end
  end
  
  describe "RECOVER #recover" do
    it 'recovers the requested reason' do
      reason = Req::Reason.create! valid_attributes
      delete :destroy, id: reason.to_param
      post :recover, id: reason.to_param
      expect(reason.active).to eq(true)
    end

    it 'redirects to the reasons list' do
      reason = Req::Reason.create! valid_attributes
      post :recover, id: reason.to_param
      expect(response).to redirect_to(req_reasons_url)
    end
  end

end
