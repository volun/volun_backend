require 'rails_helper'

RSpec.describe ResourcesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :resource
  }

  let(:invalid_attributes) {
    attributes_for :resource, :invalid
  }

  describe "GET #index" do
    it 'assigns all resources as @resources' do
      resource = Resource.find_by(name: Volunteer.name)
      get :index
      expect(assigns(:resources).count).not_to eq 0
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
     it 'assigns the requested resource as @resource' do
       resource = Resource.find_by(name: Volunteer.name)
       get :show, id: resource.to_param
       expect(assigns(:resource)).to eq(resource)
     end

     it 'show a list of all resources' do
      resource = Resource.find_by(name: Volunteer.name)
      get :show, id: resource.to_param
      expect(response).to be_success
    end
   end

  describe "GET #new" do
     it 'assigns a new resource as @resource' do
       get :new
       expect(assigns(:resource)).to be_a_new(Resource)
     end
   end

   describe "POST #create" do
     context 'with valid params' do
        it 'creates a new Sector' do
          expect {
            post :create, resource: valid_attributes
          }.to change(Resource, :count).by(1)
        end

        it 'assigns a newly created resource as @resource' do
          post :create, resource: valid_attributes
          expect(assigns(:resource)).to be_a(Resource)
          expect(assigns(:resource)).to be_persisted
        end
     end

     context 'with invalid params' do
       it 'assigns a newly created but unsaved resource as @resource' do
         post :create, resource: invalid_attributes
         expect(assigns(:resource)).to be_a_new(Resource)
      end
     end
   end

   describe "PUT #update" do
     context 'with valid params' do
       

      it 'assigns the requested resource as @resource' do
        resource = Resource.find_by(name: Volunteer.name)
        valid_attributes[:name]=Volunteer.name
        put :update, id: resource.to_param, resource: valid_attributes
        expect(assigns(:resource)).to eq(resource)
      end

      it 'redirects to resources' do
        resource = Resource.find_by(name: Volunteer.name)
        valid_attributes[:name]=Volunteer.name
        put :update, id: resource.to_param, resource: valid_attributes
        expect(response).to redirect_to(resources_url)
      end
     end
   end

   describe "DELETE #destroy" do
     it 'destroys the resource' do
       resource = Resource.find_by(name: Volunteer.name)
       expect {
         delete :destroy, id: resource.to_param
       }.to change(Resource, :count).by(resource.respond_to?(:archived?) ? 0 : -1)
     end

     it 'redirects to the resource new path' do
      resource = Resource.find_by(name: Volunteer.name)
      delete :destroy, id: resource.to_param
      expect(response).to redirect_to(resources_url)
    end
   end
  
   describe "RECOVER #recover" do
     it 'recovers the resource' do
       resource = Resource.find_by(name: Volunteer.name)
       delete :destroy, id: resource.to_param
       post :recover, id: resource.to_param
       expect(resource.active).to eq(true)
     end

     it 'redirects to the resources list' do
       resource = Resource.find_by(name: Volunteer.name)
       post :recover, id: resource.to_param
       expect(response).to redirect_to(resources_url)
     end
   end

end
