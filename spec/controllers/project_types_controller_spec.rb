require 'rails_helper'

RSpec.describe ProjectTypesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :project_type
  }

  let(:invalid_attributes) {
    attributes_for :project_type, :invalid
  }

  describe "GET #index" do
    it 'assigns all project_types as @project_types' do
      get :index
      expect(assigns(:project_types).count).not_to eq(0)
    end
  end

  describe "GET #show" do
    it 'assigns the requested project_type as @project_type' do
      
      project_type = ProjectType.find(1)
      get :show, id: project_type.to_param
      expect(assigns(:project_type)).to eq(project_type)
    end
  end

  describe "GET #new" do
    it 'assigns a new project_type as @project_type' do
      get :new
      expect(assigns(:project_type)).to be_a_new(ProjectType)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested project_type as @project_type' do
      project_type = ProjectType.find(1)
      get :edit, id: project_type.to_param
      expect(assigns(:project_type)).to eq(project_type)
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested project_type as @project_type' do
        project_type = ProjectType.find(1)
        put :update, id: project_type.to_param, project_type: project_type.to_param
        expect(assigns(:project_type)).to eq(project_type)
      end

      it 'redirects to project_types' do
        project_type = ProjectType.find(1)
        put :update, id: project_type.to_param, project_type: project_type.to_param
        expect(response).to redirect_to(project_types_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the project_type as @project_type' do
        invalid_attributes[:kind]=1
        valid_attributes[:id]=1
        project_type = ProjectType.find(1)
        put :update, id: project_type.to_param, project_type: invalid_attributes
        expect(assigns(:project_type)).to eq(project_type)
      end

    
    end
  end

  describe "DELETE #destroy" do
    it 'redirects to the projects list' do
      project_type = ProjectType.find(1)
      delete :destroy, id: project_type.to_param
      expect(response).to redirect_to(project_types_url)
    end
  end

end
