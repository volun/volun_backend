require 'rails_helper'

RSpec.describe Ent::TrackingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :ent_tracking
  }

  let(:invalid_attributes) {
    attributes_for :ent_tracking, :invalid
  }

  describe "GET #index" do
    it 'assigns all ent_trackings as @ent_trackings' do
      tracking = Ent::Tracking.create! valid_attributes
      get :index,  q:{entity_id_eq:1}
      expect(assigns(:ent_trackings)).to eq([tracking])
    end

    it 'assigns all ent_trackings as @ent_trackings with params blank' do
      tracking = Ent::Tracking.create! valid_attributes
      get :index,q:{s:"",entity_id_eq:1}
      expect(assigns(:ent_trackings)).to eq([tracking])
    end

    it 'assigns all ent_trackings as @ent_trackings with manager' do
      tracking = Ent::Tracking.create! valid_attributes
      get :index,search_manager: "Andres",  q:{entity_id_eq:1}
      expect(assigns(:ent_trackings)).not_to eq([tracking])
    end
  end

  describe "GET #show" do
    it 'assigns the requested ent_tracking as @ent_tracking' do
      tracking = Ent::Tracking.create! valid_attributes
      get :show, id: tracking.to_param, q:{entity_id_eq:1}
      expect(assigns(:ent_tracking)).to eq(tracking)
    end
  end

  describe "GET #new" do
    it 'assigns a new ent_tracking as @ent_tracking' do
      get :new, entity_id: 1, q:{entity_id_eq:1}
      expect(assigns(:ent_tracking)).to be_a_new(Ent::Tracking)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested ent_tracking as @ent_tracking' do
      tracking = Ent::Tracking.create! valid_attributes
      get :edit, id: tracking.to_param,  q:{entity_id_eq:1}
      expect(assigns(:ent_tracking)).to eq(tracking)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Ent::Tracking' do
        expect {
          post :create, ent_tracking: valid_attributes,  q:{entity_id_eq:1}
        }.to change(Ent::Tracking, :count).by(1)
      end

      it 'assigns a newly created ent_tracking as @ent_tracking' do
        post :create, ent_tracking: valid_attributes,  q:{entity_id_eq:1}
        expect(assigns(:ent_tracking)).to be_a(Ent::Tracking)
        expect(assigns(:ent_tracking)).to be_persisted
      end

      it 'redirects to the created ent_tracking' do
        post :create, ent_tracking: valid_attributes,  q:{entity_id_eq:1}
        expect(response).to redirect_to(ent_trackings_url( q:{entity_id_eq:1}))
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested ent_tracking as @ent_tracking' do
        tracking = Ent::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, ent_tracking: valid_attributes,  q:{entity_id_eq:1}
        expect(assigns(:ent_tracking)).to eq(tracking)
      end

      it 'redirects to ent_trackings' do
        tracking = Ent::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, ent_tracking: valid_attributes,  q:{entity_id_eq:1}
        expect(response).to redirect_to(ent_trackings_url( q:{entity_id_eq:1}))
      end
    end

    context 'with invalid params' do
      it 'assigns the ent_tracking as @ent_tracking' do
        tracking = Ent::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, ent_tracking: invalid_attributes,  q:{entity_id_eq:1}
        expect(assigns(:ent_tracking)).to eq(tracking)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested ent_tracking' do
      tracking = Ent::Tracking.create! valid_attributes
      expect {
        delete :destroy, id: tracking.to_param,  q:{entity_id_eq:1}
      }.to change(Ent::Tracking, :count).by(tracking.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the ent_trackings list' do
      tracking = Ent::Tracking.create! valid_attributes
      delete :destroy, id: tracking.to_param,  q:{entity_id_eq:1}
      expect(response).to redirect_to(ent_trackings_url)
    end
  end

end
