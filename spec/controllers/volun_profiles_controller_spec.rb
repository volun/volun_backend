require 'rails_helper'

RSpec.describe VolunProfilesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :volun_profile
  }

  let(:invalid_attributes) {
    attributes_for :volun_profile, :invalid
  }

  describe "GET #index" do
    it 'assigns all VolunProfile as @VolunProfile' do
      volun_profile = VolunProfile.create! valid_attributes
      get :index
      expect(assigns(:volun_profile)).to eq([volun_profile])
    end
  end

  describe "GET #show" do
    it 'assigns the requested VolunProfile as @volun_profile' do
      volun_profile = VolunProfile.create! valid_attributes
      get :show, id: volun_profile.to_param
      expect(assigns(:volun_profile)).to eq(volun_profile)
    end
  end

  describe "GET #new" do
    it 'assigns a new volun_profile as @volun_profile' do
      get :new
      expect(assigns(:volun_profile)).to be_a_new(VolunProfile)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested volun_profile as @volun_profile' do
      volun_profile = VolunProfile.create! valid_attributes
      get :edit, id: volun_profile.to_param
      expect(assigns(:volun_profile)).to eq(volun_profile)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new VolunProfile' do
        expect {
          post :create, volun_profile: valid_attributes
        }.to change(VolunProfile, :count).by(1)
      end

      it 'assigns a newly created volun_profile as @volun_profile' do
        post :create, volun_profile: valid_attributes
        expect(assigns(:volun_profile)).to be_a(VolunProfile)
        expect(assigns(:volun_profile)).to be_persisted
      end

      it 'redirects to the created volun_profile' do
        post :create, volun_profile: valid_attributes
        expect(response).to redirect_to(volun_profiles_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved volun_profile as @volun_profile' do
        post :create, volun_profile: invalid_attributes
        expect(assigns(:volun_profile)).to be_a_new(VolunProfile)
      end

      it 're-renders the "new" template' do
        post :create, volun_profile: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it 'updates the requested volun_profile' do
        volun_profile = VolunProfile.create! valid_attributes
        put :update, id: volun_profile.to_param, volun_profile: new_attributes
        volun_profile.reload
        skip("Add assertions for updated state")
      end

      it 'assigns the requested volun_profile as @volun_profile' do
        volun_profile = VolunProfile.create! valid_attributes
        put :update, id: volun_profile.to_param, volun_profile: valid_attributes
        expect(assigns(:volun_profile)).to eq(volun_profile)
      end

      it 'redirects to volun_profiles' do
        volun_profile = VolunProfile.create! valid_attributes
        put :update, id: volun_profile.to_param, volun_profile: valid_attributes
        expect(response).to redirect_to(volun_profiles_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the volun_profile as @volun_profile' do
        volun_profile = VolunProfile.create! valid_attributes
        put :update, id: volun_profile.to_param, volun_profile: invalid_attributes
        expect(assigns(:volun_profile)).to eq(volun_profile)
      end

      it 're-renders the "edit" template' do
        volun_profile = VolunProfile.create! valid_attributes
        put :update, id: volun_profile.to_param, volun_profile: invalid_attributes
        expect(response).to render_template('edit')
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested volun_profile' do
      volun_profile = VolunProfile.create! valid_attributes
      expect {
        delete :destroy, id: volun_profile.to_param
      }.to change(VolunProfile, :count).by(volun_profile.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the volun_profiles list' do
      volun_profile = VolunProfile.create! valid_attributes
      delete :destroy, id: volun_profile.to_param
      expect(response).to redirect_to(volun_profiles_url)
    end
  end

end
