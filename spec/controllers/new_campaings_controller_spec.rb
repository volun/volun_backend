require 'rails_helper'

RSpec.describe NewCampaingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :new_campaing
  }

  let(:invalid_attributes) {
    attributes_for :new_campaing, :invalid
  }

  describe "GET #index" do
    it 'assigns all new_campaings as @new_campaings' do
      new_campaing = NewCampaing.create! valid_attributes
      get :index
      expect(assigns(:new_campaings)).to eq([new_campaing])
    end
  end

  describe "GET #show" do
    it 'assigns the requested new_campaing as @new_campaing' do
      new_campaing = NewCampaing.create! valid_attributes
      get :show, id: new_campaing.to_param
      expect(assigns(:new_campaing)).to eq(new_campaing)
    end
  end

  describe "GET #new" do
    it 'assigns a new new_campaing as @new_campaing' do
      get :new
      expect(assigns(:new_campaing)).to be_a_new(NewCampaing)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested new_campaing as @new_campaing' do
      new_campaing = NewCampaing.create! valid_attributes
      get :edit, id: new_campaing.to_param
      expect(assigns(:new_campaing)).to eq(new_campaing)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new NewCampaing' do
        expect {
          post :create, new_campaing: valid_attributes
        }.to change(NewCampaing, :count).by(1)
      end

      it 'assigns a newly created new_campaing as @new_campaing' do
        post :create, new_campaing: valid_attributes
        expect(assigns(:new_campaing)).to be_a(NewCampaing)
        expect(assigns(:new_campaing)).to be_persisted
      end

      it 'redirects to the created new_campaing' do
        post :create, new_campaing: valid_attributes
        expect(response).to redirect_to(new_campaings_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved new_campaing as @new_campaing' do
        post :create, new_campaing: invalid_attributes
        expect(assigns(:new_campaing)).to be_a_new(NewCampaing)
      end

      it 're-renders the "new" template' do
        post :create, new_campaing: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it 'updates the requested new_campaing' do
        new_campaing = NewCampaing.create! valid_attributes
        put :update, id: new_campaing.to_param, new_campaing: new_attributes
        new_campaing.reload
        skip("Add assertions for updated state")
      end

      it 'assigns the requested new_campaing as @new_campaing' do
        new_campaing = NewCampaing.create! valid_attributes
        put :update, id: new_campaing.to_param, new_campaing: valid_attributes
        expect(assigns(:new_campaing)).to eq(new_campaing)
      end

      it 'redirects to new_campaings' do
        new_campaing = NewCampaing.create! valid_attributes
        put :update, id: new_campaing.to_param, new_campaing: valid_attributes
        expect(response).to redirect_to(new_campaings_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the new_campaing as @new_campaing' do
        new_campaing = NewCampaing.create! valid_attributes
        put :update, id: new_campaing.to_param, new_campaing: invalid_attributes
        expect(assigns(:new_campaing)).to eq(new_campaing)
      end

      it 're-renders the "edit" template' do
        new_campaing = NewCampaing.create! valid_attributes
        put :update, id: new_campaing.to_param, new_campaing: invalid_attributes
        expect(response).to render_template('edit')
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested new_campaing' do
      new_campaing = NewCampaing.create! valid_attributes
      expect {
        delete :destroy, id: new_campaing.to_param
      }.to change(NewCampaing, :count).by(new_campaing.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the new_campaings list' do
      new_campaing = NewCampaing.create! valid_attributes
      delete :destroy, id: new_campaing.to_param
      expect(response).to redirect_to(new_campaings_url)
    end
  end

end
