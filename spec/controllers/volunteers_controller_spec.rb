require 'rails_helper'

RSpec.describe VolunteersController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :volunteer
  }

  let(:invalid_attributes) {
    attributes_for :volunteer, :invalid
  }

  let(:user_atributes){
    attributes_for :user, :volunteer_log
  }

  let(:valid_attributes_subscribe){
    attributes_for :volunteer_subscribe
  }

  let(:valid_attributes_amendment){
    attributes_for :volunteer_amendment
  }

  describe "GET #index" do
    it 'assigns all volunteers as @volunteers' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index
      expect(assigns(:volunteers)).to eq([volunteer])
    end

    it 'assigns all volunteers as @volunteers  with param q[:s] blank' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,q:{s: ""}
      expect(assigns(:volunteers)).to eq([volunteer])
    end

    it 'assigns all volunteers as @volunteers  with param project_id' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,project_id: 1
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param project_id, status' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      volunteer.projects.append(Project.find(1))
      get :index,project_id: 1, search_status: "active"
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param project_id, status' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,project_id: 1, search_status: ""
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param project_id, search_location >=0' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,project_id: 1, search_location: 0
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param project_id, search_location >=0' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,project_id: 1, search_location: "-280"
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param clean' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,clean: 1
      expect(assigns(:volunteers).count).not_to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param q[:s]' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,q:{s: "addresses#postal_code"}
      expect(assigns(:volunteers).count).not_to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param search_district -280' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,search_district: "-280"
      expect(assigns(:volunteers).count).not_to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param search_district' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,search_district: "28000"
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param search_without_auth' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,search_without_auth: "true"
      expect(assigns(:volunteers).count).to eq(1)
    end

    it 'assigns all volunteers as @volunteers  with param new_availability' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,new_availability: "true"
      expect(assigns(:volunteers).count).to eq(0)
    end

    it 'assigns all volunteers as @volunteers  with param search_availabilities_day' do
      @idNumerType = FactoryGirl.create(:document_type)
      valid_attributes[:address_id] = 3
      valid_attributes[:document_type_id] = @idNumerType.id
      volunteer = Volunteer.create! valid_attributes
      get :index,search_availabilities_day: "1"
      expect(assigns(:volunteers).count).to eq(0)
    end
  end
  
  # describe "GET #show" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     get :show, id: volunteer.to_param, format: :pdf
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #new" do
  #   it 'assigns a new volunteer as @volunteer' do
  #     get :new
  #     expect(assigns(:volunteer)).to be_a_new(Volunteer)
  #   end
  # end

  # describe "GET #edit" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     get :edit, id: volunteer.to_param
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "POST #create" do
  #   context 'with valid params' do
  #     it 'creates a new Volunteer' do
  #         @idNumerType = FactoryGirl.create(:document_type)
  #         valid_attributes[:address_id] = 3
  #         valid_attributes[:document_type_id] = @idNumerType.id
  #       expect {
  #         volunteer = Volunteer.create! valid_attributes
  #       }.to change(Volunteer, :count).by(1)
  #     end

  #     it 'assigns a newly created volunteer as @volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       expect(volunteer).to be_a(Volunteer)
  #       expect(volunteer).to be_persisted
  #     end

  #     it 'redirects to the created volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       post :create, volunteer: valid_attributes
  #       expect(response.status).to eq(200)
  #     end

  #     it 'redirects to the created volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       volunteer.active=false
  #       post :create, volunteer: valid_attributes
  #       expect(response.status).to eq(200)
  #     end

  #     it 'redirects to the created volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id

  #       volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes_subscribe
  #       post :create, volunteer: valid_attributes, rt_volunteer_subscribe_id: volunteer_subscribe.id
  #       expect(response.status).to eq(200)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns a newly created but unsaved volunteer as @volunteer' do
  #       post :create, volunteer: invalid_attributes
  #       expect(assigns(:volunteer)).to be_a_new(Volunteer)
  #     end

  #     it 're-renders the "new" template' do
  #       post :create, volunteer: invalid_attributes
  #       expect(response).to render_template('new')
  #     end
  #   end
  # end

  # describe "GET #show_sms" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     get :show_sms, volunteer: volunteer.id
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #show_mail" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
      
  #     get :show_mail, volunteer: volunteer.id
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #show_unsubscribe" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
      
  #     get :show_unsubscribe, volunteer: volunteer.id,xhr: true,format: :js
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #export_all" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
      
  #     get :export_all,format: :csv
  #     expect(assigns(:volunteer)).not_to eq(volunteer)
  #   end

  #   it 'assigns the requested volunteer as @volunteer with project_id' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
      
  #     get :export_all,project_id: 1,format: :csv
  #     expect(assigns(:volunteer)).not_to eq(volunteer)
  #   end
  # end

  # describe "GET #accept_unsubscribe" do
  #   it 'assigns the requested volunteer as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :accept_unsubscribe, volunteer: volunteer.id, search_reason: 1,xhr: true,format: :js
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #new_password" do
  #   it 'assigns a new password as @volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     get :new_password, id: volunteer.to_param
  #     expect(response.status).to eq(200)
  #   end
  # end

  # describe "GET #update_password" do
  #   it 'assigns a update password as @volunteer' do
      
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "message",email: volunteer.email
  #     expect(response.status).to eq(302)
  #   end
   

  #   it 'assigns a update password as @volunteer whit id exist' do
      
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     volunteer.id=1
  #     get :update_password, volunteer: volunteer.to_param,message: "message",email: volunteer.email
  #     expect(response.status).to eq(302)
  #   end
  #   it 'assigns a update password as @volunteer without message'  do
      
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "",email: volunteer.email
  #     expect(response).to redirect_to edit_volunteer_path(volunteer.id)
  #   end

  #   it 'assigns a update password as @volunteer with mail' do
     
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "message",send_mail: 1,email: volunteer.email
  #     expect(response.status).to eq(302)
  #   end

  #   it 'assigns a update password as @volunteer without mail' do
     
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "message",send_mail: 1,email: ""
  #     expect(response).to redirect_to edit_volunteer_path(volunteer.id)
  #   end

  #   it 'assigns a update password as @volunteer with sms' do
      
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "message",send_sms: 1,mobile_phone: "678945656",email: volunteer.email
  #     expect(response.status).to eq(302)
  #   end

  #   it 'assigns a update password as @volunteer without sms' do
      
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     user=User.create! user_atributes
  #     get :update_password, volunteer: volunteer.to_param,message: "message",send_sms: 1,mobile_phone: "",email: volunteer.email
  #     expect(response).to redirect_to edit_volunteer_path(volunteer.id)
  #   end
  # end

  # describe "GET #show_project_volunteer" do
  #   it 'redirect volunteer to edit volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :show_project_volunteer, id: volunteer,project_id: 1, update_volun: true,:format=>:js
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end

  #   it 'redirect volunteer to projects with volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :show_project_volunteer, id: volunteer.id,project_id: 1,:format=>:js
  #     expect(assigns(:volunteer)).to eq(volunteer)
  #   end
  # end

  # describe "GET #set_project_volunteer" do
  #   it 'redirect volunteer to edit volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :set_project_volunteer, id: volunteer,project: 1, update_volun: true,xhr: true
  #     expect(response).to redirect_to(edit_volunteer_path(volunteer,project: 1,q:{projects_id_eq: 1 }))
  #   end

  #   it 'redirect volunteer to projects with volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :set_project_volunteer, id: volunteer.id,project: 1,xhr: true
  #     expect(response).to redirect_to(project_volunteers_path(1,q:{projects_id_eq: 1 }))
  #   end
  # end

  # describe "GET #send_sms" do
  #   it 'redirect volunteer to volunteers' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     get :send_sms, volunteer: volunteer
  #     expect(response).to redirect_to(volunteers_path())
  #   end
  # end

  # describe "GET #send_mail" do
  #   it 'redirect volunteer to volunteers' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     volunteer.projects.append(Project.find(1))
  #     get :send_mail, id: volunteer
  #     expect(response).to redirect_to(volunteers_path())
  #   end
  # end

  # describe "PUT #update" do
  #   context 'with valid params' do

  #     it 'assigns the requested volunteer as @volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       put :update, id: volunteer.to_param, volunteer: valid_attributes
  #       expect(assigns(:volunteer)).to eq(volunteer)
  #     end

  #     it 'assigns the requested volunteer as @volunteer without unsubscribe date and active eq false' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:unsubscribe_date]=nil
  #       valid_attributes[:active]=false
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       put :update, id: volunteer.to_param, volunteer: valid_attributes
  #       expect(assigns(:volunteer).active).to eq(true)
  #     end
      

  #     it 'assigns the requested volunteer as @volunteer with amendment request' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       user=User.create! user_atributes
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes_amendment
  #       put :update, id: volunteer.to_param, volunteer: valid_attributes, rt_volunteer_amendment_id: volunteer_amendment.id
  #       expect(assigns(:volunteer).active).to eq(true)
  #     end

  #     it 'redirects to volunteers' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       put :update, id: volunteer.to_param, volunteer: valid_attributes
  #       expect(response).to redirect_to(volunteers_url)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns the volunteer as @volunteer' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       put :update, id: volunteer.to_param, volunteer: invalid_attributes
  #       expect(assigns(:volunteer)).to eq(volunteer)
  #     end

  #     it 're-renders the "edit" template' do
  #       @idNumerType = FactoryGirl.create(:document_type)
  #       valid_attributes[:address_id] = 3
  #       valid_attributes[:document_type_id] = @idNumerType.id
  #       volunteer = Volunteer.create! valid_attributes
  #       put :update, id: volunteer.to_param, volunteer: invalid_attributes
  #       expect(response).to render_template('edit')
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     expect {
  #       delete :destroy, id: volunteer.to_param
  #     }.to change(Volunteer, :count).by(volunteer.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the volunteers list' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     delete :destroy, id: volunteer.to_param
  #     expect(response).to redirect_to(volunteers_url)
  #   end
  # end
  
  # describe "RECOVER #recover" do
  #   it 'recovers the requested volunteer' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     delete :destroy, id: volunteer.to_param
  #     post :recover, id: volunteer.to_param
  #     expect(volunteer.active).to eq(true)
  #   end

  #   it 'redirects to the volunteers list' do
  #     @idNumerType = FactoryGirl.create(:document_type)
  #     valid_attributes[:address_id] = 3
  #     valid_attributes[:document_type_id] = @idNumerType.id
  #     volunteer = Volunteer.create! valid_attributes
  #     post :recover, id: volunteer.to_param
  #     expect(response).to redirect_to(volunteers_url)
  #   end
  # end

end
