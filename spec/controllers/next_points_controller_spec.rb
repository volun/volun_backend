require 'rails_helper'

RSpec.describe NextPointsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :next_point
  }

  let(:invalid_attributes) {
    attributes_for :next_point, :invalid
  }

  describe "GET #index" do
    it 'assigns all next_points as @next_points' do
      next_point = NextPoint.create! valid_attributes
      get :index
      expect(assigns(:next_points)).to eq([next_point])
    end
  end

  describe "GET #show" do
    it 'assigns the requested next_point as @next_point' do
      next_point = NextPoint.create! valid_attributes
      get :show, id: next_point.to_param
      expect(assigns(:next_point)).to eq(next_point)
    end
  end

  describe "GET #new" do
    it 'assigns a new next_point as @next_point' do
      get :new
      expect(assigns(:next_point)).to be_a_new(NextPoint)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested next_point as @next_point' do
      next_point = NextPoint.create! valid_attributes
      get :edit, id: next_point.to_param
      expect(assigns(:next_point)).to eq(next_point)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new NextPoint' do
        expect {
          post :create, next_point: valid_attributes
        }.to change(NextPoint, :count).by(1)
      end

      it 'assigns a newly created next_point as @next_point' do
        post :create, next_point: valid_attributes
        expect(assigns(:next_point)).to be_a(NextPoint)
        expect(assigns(:next_point)).to be_persisted
      end

      it 'redirects to the created next_point' do
        post :create, next_point: valid_attributes
        expect(response).to redirect_to(next_points_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved next_point as @next_point' do
        post :create, next_point: invalid_attributes
        expect(assigns(:next_point)).to be_a_new(NextPoint)
      end

      it 're-renders the "new" template' do
        post :create, next_point: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it 'updates the requested next_point' do
        next_point = NextPoint.create! valid_attributes
        put :update, id: next_point.to_param, next_point: new_attributes
        next_point.reload
        skip("Add assertions for updated state")
      end

      it 'assigns the requested next_point as @next_point' do
        next_point = NextPoint.create! valid_attributes
        put :update, id: next_point.to_param, next_point: valid_attributes
        expect(assigns(:next_point)).to eq(next_point)
      end

      it 'redirects to next_points' do
        next_point = NextPoint.create! valid_attributes
        put :update, id: next_point.to_param, next_point: valid_attributes
        expect(response).to redirect_to(next_points_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the next_point as @next_point' do
        next_point = NextPoint.create! valid_attributes
        put :update, id: next_point.to_param, next_point: invalid_attributes
        expect(assigns(:next_point)).to eq(next_point)
      end

      it 're-renders the "edit" template' do
        next_point = NextPoint.create! valid_attributes
        put :update, id: next_point.to_param, next_point: invalid_attributes
        expect(response).to render_template('edit')
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested next_point' do
      next_point = NextPoint.create! valid_attributes
      expect {
        delete :destroy, id: next_point.to_param
      }.to change(NextPoint, :count).by(next_point.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the next_points list' do
      next_point = NextPoint.create! valid_attributes
      delete :destroy, id: next_point.to_param
      expect(response).to redirect_to(next_points_url)
    end
  end

end
