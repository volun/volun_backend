require 'rails_helper'

RSpec.describe Rt::VolunteerUnsubscribesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :volunteer_unsubscribe
  }

  let(:invalid_attributes) {
    attributes_for :volunteer_unsubscribe, :invalid
  }

  describe "GET #index" do
    it 'assigns all rt_volunteer_unsubscribes as @rt_volunteer_unsubscribes' do
      volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
      get :index
      expect(assigns(:rt_volunteer_unsubscribes)).to eq([volunteer_unsubscribe])
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
      volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
      get :show, id: volunteer_unsubscribe.to_param
      expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
      get :new
      expect(assigns(:rt_volunteer_unsubscribe)).to be_a_new(Rt::VolunteerUnsubscribe)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
      volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
      get :edit, id: volunteer_unsubscribe.to_param
      expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::VolunteerUnsubscribe' do
        expect {
          post :create, rt_volunteer_unsubscribe: valid_attributes
        }.to change(Rt::VolunteerUnsubscribe, :count).by(1)
      end

      it 'assigns a newly created rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
        post :create, rt_volunteer_unsubscribe: valid_attributes
        expect(assigns(:rt_volunteer_unsubscribe)).to be_a(Rt::VolunteerUnsubscribe)
        expect(assigns(:rt_volunteer_unsubscribe)).to be_persisted
      end

      # it 'redirects to the created rt_volunteer_unsubscribe' do
      #   post :create, rt_volunteer_unsubscribe: valid_attributes
      #   expect(response).to redirect_to(rt_volunteer_unsubscribes_url)
      # end
    end

    # context 'with invalid params' do
    #   it 'assigns a newly created but unsaved rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
    #     post :create, rt_volunteer_unsubscribe: invalid_attributes
    #     expect(assigns(:rt_volunteer_unsubscribe)).to be_a_new(Rt::VolunteerUnsubscribe)
    #   end

    #   # it 're-renders the "new" template' do
    #   #   post :create, rt_volunteer_unsubscribe: invalid_attributes
    #   #   expect(response).to render_template('new')
    #   # end
    # end
  end

  # describe "GET #process_request_form" do
  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :process_request_form, id: volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_unsubscribe)
  #   end


  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe with comments' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     status_manager = RtController::StatusManager.new(request_form: @rt_volunteer_unsubscribe.request_form,
  #       manager_id: 1)
  #     status_manager.mark_request_form_as_processing
  #     get :process_request_form, id: @rt_volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(@rt_volunteer_unsubscribe)
  #   end
  # end

  # describe "GET #pre_approve_request_form" do
  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :pre_approve_request_form, id: volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end

  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe with status processing' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     status_manager = RtController::StatusManager.new(request_form: volunteer_unsubscribe.request_form,
  #       manager_id: 1)
  #     status_manager.mark_request_form_as_processing
  #     get :pre_approve_request_form, id: volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end

  # end

  # describe "GET #reject_request_form" do
  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :reject_request_form, id: volunteer_unsubscribe.to_param, request_form: {id: volunteer_unsubscribe.request_form.id}
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end

  #   it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe with status processing' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     status_manager = RtController::StatusManager.new(request_form: volunteer_unsubscribe.request_form,
  #       manager_id: 1)
  #     status_manager.mark_request_form_as_processing
  #     get :reject_request_form, id: volunteer_unsubscribe.to_param, request_form: {id: volunteer_unsubscribe.request_form.id}
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end

  # end

  # describe "GET #pre_reject_request_form" do
  #   it 'assigns the requested volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :pre_reject_request_form, id: volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end
  # end

  # describe "GET #mark_request_form_as_pending" do
  #   it 'assigns the requested volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :mark_request_form_as_pending, id: volunteer_unsubscribe.to_param
  #     expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
  #   end

  #   it 'redirects to rt_volunteer_subscribes' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     get :mark_request_form_as_pending, id: volunteer_unsubscribe.to_param      
  #     expect(response).to redirect_to(rt_volunteer_unsubscribes_url)
  #   end
  # end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
        volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
        put :update, id: volunteer_unsubscribe.to_param, rt_volunteer_unsubscribe: valid_attributes
        expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
      end

      # it 'redirects to rt_volunteer_unsubscribes' do
      #   volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
      #   put :update, id: volunteer_unsubscribe.to_param, rt_volunteer_unsubscribe: valid_attributes
      #   expect(response).to redirect_to(rt_volunteer_unsubscribes_url)
      # end
    end

    context 'with invalid params' do
      it 'assigns the rt_volunteer_unsubscribe as @rt_volunteer_unsubscribe' do
        volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
        put :update, id: volunteer_unsubscribe.to_param, rt_volunteer_unsubscribe: invalid_attributes
        expect(assigns(:rt_volunteer_unsubscribe)).to eq(volunteer_unsubscribe)
      end

      # it 're-renders the "edit" template' do
      #   volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
      #   put :update, id: volunteer_unsubscribe.to_param, rt_volunteer_unsubscribe: invalid_attributes
      #   expect(response).to render_template('edit')
      # end
    end
  end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested rt_volunteer_unsubscribe' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     expect {
  #       delete :destroy, id: volunteer_unsubscribe.to_param
  #     }.to change(Rt::VolunteerUnsubscribe, :count).by(volunteer_unsubscribe.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the rt_volunteer_unsubscribes list' do
  #     volunteer_unsubscribe = Rt::VolunteerUnsubscribe.create! valid_attributes
  #     delete :destroy, id: volunteer_unsubscribe.to_param
  #     expect(response).to redirect_to(rt_volunteer_unsubscribes_url)
  #   end
  # end

end
