require 'rails_helper'

RSpec.describe Rt::ActivityPublishingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :activity_publishing
  }

  let(:invalid_attributes) {
    attributes_for :activity_publishing, :invalid
  }

  describe "GET #index" do
    it 'csv' do
      @activity_publishing = Rt::ActivityPublishing.all
      get :index, format: :csv
      expect(assigns(:rt_activity_publishings)).to eq([@activity_publishing])
    end

    it 'assigns all rt_activity_publishings as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :index
      expect(assigns(:rt_activity_publishings)).to eq([activity_publishing])
    end

    it 'assigns all rt_activity_publishings as @rt_activity_publishings with filter' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :index, search_pending: 'true',search_processing: 'true',search_approved: 'false',search_rejected:'false'
      expect(assigns(:rt_activity_publishings)).to eq([activity_publishing])
    end

    it 'assigns all rt_activity_publishings as @rt_activity_publishings with filter' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :index, search_pending: 'false',search_processing: 'false',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_activity_publishings)).not_to eq([activity_publishing])
    end

    it 'assigns all rt_activity_publishings as @rt_activity_publishings with params q blank' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :index, q: {:sort => ""}
      expect(assigns(:rt_activity_publishings)).to eq([activity_publishing])
    end

    it 'assigns all rt_activity_publishings as @rt_activity_publishings with entity id' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :index, search_entity: 1
      expect(assigns(:rt_activity_publishings).count).to eq(0)
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :show, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_activity_publishing as @rt_activity_publishing' do
      get :new
      expect(assigns(:rt_activity_publishing)).to be_a_new(Rt::ActivityPublishing)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :edit, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::ActivityPublishing' do
        expect {
          post :create, rt_activity_publishing: valid_attributes
        }.to change(Rt::ActivityPublishing, :count).by(1)
      end

      it 'assigns a newly created rt_activity_publishing as @rt_activity_publishing' do
        post :create, rt_activity_publishing: valid_attributes
        expect(assigns(:rt_activity_publishing)).to be_a(Rt::ActivityPublishing)
        expect(assigns(:rt_activity_publishing)).to be_persisted
      end
    end
  end

  
  describe "GET #process_request_form" do
    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :process_request_form, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing with param comments' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :process_request_form, id: activity_publishing.to_param, input_comments: "MyString"
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing with comments' do
      @rt_activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: @rt_activity_publishing.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :process_request_form, id: @rt_activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(@rt_activity_publishing)
    end
  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :pre_approve_request_form, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing with status processing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: activity_publishing.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :pre_approve_request_form, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

  end

  describe "GET #reject_request_form" do
    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :reject_request_form, id: activity_publishing.to_param, request_form: {id: activity_publishing.request_form.id}
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'assigns the requested rt_activity_publishing as @rt_activity_publishing with status processing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: activity_publishing.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: activity_publishing.to_param, request_form: {id: activity_publishing.request_form.id}
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

  end

  describe "GET #pre_reject_request_form" do
    it 'assigns the requested activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :pre_reject_request_form, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested activity_publishing as @rt_activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :mark_request_form_as_pending, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'redirects to rt_volunteer_subscribes' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :mark_request_form_as_pending, id: activity_publishing.to_param      
      expect(response).to redirect_to(rt_activity_publishings_url)
    end
  end

  describe "GET #mark_request_form_as_processing" do
    it 'assigns the requested activity_publishing as @activity_publishing' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :mark_request_form_as_processing, id: activity_publishing.to_param
      expect(assigns(:rt_activity_publishing)).to eq(activity_publishing)
    end

    it 'redirects to rt_volunteer_subscribes' do
      activity_publishing = Rt::ActivityPublishing.create! valid_attributes
      get :mark_request_form_as_processing, id: activity_publishing.to_param      
      expect(response).to redirect_to(rt_activity_publishings_url)
    end
  end

end