require 'rails_helper'

RSpec.describe Rt::ActivityUnpublishingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :activity_unpublishing
  }

  let(:invalid_attributes) {
    attributes_for :activity_unpublishing, :invalid
  }

  describe "GET #index" do
    it 'assigns all rt_activity_unpublishings as @rt_activity_unpublishing' do
      activity_unpublishing = Rt::ActivityUnpublishing.create! valid_attributes
      get :index
      expect(assigns(:rt_activity_unpublishings)).not_to eq([activity_unpublishing])
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end

    it 'csv' do
      @activity_unpublishing = Rt::ActivityPublishing.all
      get :index, format: :csv
      expect(assigns(:rt_activity_unpublishings)).to eq([@activity_unpublishing])
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_activity_unpublishing as @rt_activity_unpublishing' do
      activity_unpublishing = Rt::ActivityUnpublishing.create! valid_attributes
      get :show, id: activity_unpublishing.to_param
      expect(assigns(:rt_activity_unpublishing)).to eq(activity_unpublishing)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_activity_unpublishing as @rt_activity_unpublishing' do
      get :new
      expect(assigns(:rt_activity_unpublishing)).to be_a_new(Rt::ActivityUnpublishing)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_activity_unpublishing as @rt_activity_unpublishing' do
      activity_unpublishing = Rt::ActivityUnpublishing.create! valid_attributes
      get :edit, id: activity_unpublishing.to_param
      expect(assigns(:rt_activity_unpublishing)).to eq(activity_unpublishing)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::ActivityUnpublishing' do
        expect {
          post :create, rt_activity_unpublishing: valid_attributes
        }.to change(Rt::ActivityUnpublishing, :count).by(1)
      end

      it 'assigns a newly created rt_activity_unpublishing as @rt_activity_unpublishing' do
        post :create, rt_activity_unpublishing: valid_attributes
        expect(assigns(:rt_activity_unpublishing)).to be_a(Rt::ActivityUnpublishing)
        expect(assigns(:rt_activity_unpublishing)).to be_persisted
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
     

      it 'assigns the requested rt_activity_unpublishing as @rt_activity_unpublishing' do
        activity_unpublishing = Rt::ActivityUnpublishing.create! valid_attributes
        put :update, id: activity_unpublishing.to_param, rt_activity_unpublishing: valid_attributes
        expect(assigns(:rt_activity_unpublishing)).to eq(activity_unpublishing)
      end

    
    end

    context 'with invalid params' do
      it 'assigns the rt_activity_unpublishing as @rt_activity_unpublishing' do
        activity_unpublishing = Rt::ActivityUnpublishing.create! valid_attributes
        put :update, id: activity_unpublishing.to_param, rt_activity_unpublishing: invalid_attributes
        expect(assigns(:rt_activity_unpublishing)).to eq(activity_unpublishing)
      end

    
    end
  end
end