require 'rails_helper'

RSpec.describe Rt::OthersController, type: :controller do
  before(:each) do
    @user=create(:user)
    @user_entity=User.create! (attributes_for :user, :entity_log)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)
  end

  let(:valid_attributes) {
    attributes_for :rt_other
  }

  let(:invalid_attributes) {
    attributes_for :rt_other, :invalid
  }
  let(:request_form_attributes){
    attributes_for :request_form
  }

  describe "GET #index" do
    it 'assigns all rt_others as @rt_others' do
      other = Rt::Other.create! valid_attributes
      get :index
      expect(assigns(:rt_others)).to eq([other])
    end

    it 'csv' do
      @other = Rt::Other.all
      get :index, format: :csv
      expect(assigns(:rt_others)).to eq([@other])
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :show, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_other as @rt_other' do
      get :new
      expect(assigns(:rt_other)).to be_a_new(Rt::Other)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :edit, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::Other' do
        expect {
          post :create, rt_other: valid_attributes
        }.to change(Rt::Other, :count).by(1)
      end

      it 'assigns a newly created rt_other as @rt_other' do
        post :create, rt_other: valid_attributes
        expect(assigns(:rt_other)).to be_a(Rt::Other)
        expect(assigns(:rt_other)).to be_persisted
      end

      it 'redirects to the created rt_other' do
        post :create, rt_other: valid_attributes
        expect(response).to redirect_to(request_forms_url)
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested rt_other as @rt_other' do
        other = Rt::Other.create! valid_attributes
        put :update, id: other.to_param, rt_other: valid_attributes
        expect(assigns(:rt_other)).to eq(other)
      end
    end

    context 'with invalid params' do
      it 'assigns the rt_other as @rt_other' do
        other = Rt::Other.create! valid_attributes
        put :update, id: other.to_param, rt_other: invalid_attributes
        expect(assigns(:rt_other)).to eq(other)
      end
     
    end
  end

  describe "GET #process_request_form" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :process_request_form, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end

    it 'assigns the requested rt_other as @rt_other with param comments' do
      other = Rt::Other.create! valid_attributes
      get :process_request_form, id: other.to_param, input_comments: "MyString"
      expect(assigns(:rt_other)).to eq(other)
    end
  end

  describe "GET #pre_reject_request_form" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :pre_reject_request_form, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end
  end

  describe "GET #reject_request_form" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :reject_request_form, id: other.to_param, request_form: {id: other.request_form.id}
      expect(assigns(:rt_other)).to eq(other)
    end

    it 'assigns the requested rt_other as @rt_other with status processing' do
      other = Rt::Other.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: other.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: other.to_param, request_form: {id: other.request_form.id}
      expect(assigns(:rt_other)).to eq(other)
    end

    it 'assigns the requested rt_other as @rt_other with reject_request_form' do
      other = Rt::Other.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: other.request_form,
        manager_id: 1)
      
      get :pre_reject_request_form, id: other.to_param
      get :reject_request_form,id: other.to_param, request_form: {id: other.request_form.id}
      
      expect(response.status).to eq(200)    
    end
  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :pre_approve_request_form, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end

    it 'assigns the requested rt_other as @rt_other with request' do
      other = Rt::Other.create! valid_attributes
      request_form_attributes[:req_reason_id]=@req_reason.id
      request_form_attributes[:rt_extendable_id]=other.id
      request_form_attributes[:rt_extendable_type]="Rt::Other"
      request_form_attributes[:request_type_id]=12
      request_form_attributes[:user_id]=@user_entity.id
      request_form = RequestForm.create! request_form_attributes
      status_manager = RtController::StatusManager.new(request_form: request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      
      get :pre_approve_request_form, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested rt_other as @rt_other' do
      other = Rt::Other.create! valid_attributes
      get :mark_request_form_as_pending, id: other.to_param
      expect(assigns(:rt_other)).to eq(other)
    end

    it 'redirects to rt_others' do
      other = Rt::Other.create! valid_attributes
      get :mark_request_form_as_pending, id: other.to_param      
      expect(response).to redirect_to(rt_others_url)
    end
  end

end
