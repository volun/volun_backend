require 'rails_helper'

RSpec.describe Rt::VolunteerSubscribesController, type: :controller do
  before(:each) do
    @user=create(:user)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)
  end

  let(:valid_attributes) {
    attributes_for :volunteer_subscribe
  }

  let(:invalid_attributes) {
    attributes_for :volunteer_subscribe, :invalid
  }
  let(:request_form_attributes){
    attributes_for :request_form
  }

  describe "GET #index" do
    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index
      expect(assigns(:rt_volunteer_subscribes)).not_to eq([volunteer_subscribe])
    end

    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes without params' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index, q:{s: ""}
      expect(assigns(:rt_volunteer_subscribes)).not_to eq([volunteer_subscribe])
    end

    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes with params district' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index,  search_district: "-280"
      expect(assigns(:rt_volunteer_subscribes)).not_to eq([volunteer_subscribe])
    end

    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes with params district other' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index,  search_district: "Centro"
      expect(assigns(:rt_volunteer_subscribes)).not_to eq([volunteer_subscribe])
    end
   
    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes with statuses' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index, search_pending: 'true',search_processing: 'true',search_approved: 'false',search_rejected:'false'
      expect(assigns(:rt_volunteer_subscribes)).to eq([volunteer_subscribe])
    end

    it 'assigns all rt_volunteer_subscribes as @rt_volunteer_subscribes with other statuses' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :index, search_pending: 'false',search_processing: 'false',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteer_subscribes)).not_to eq([volunteer_subscribe])
    end

  end

  describe "GET #show" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :show, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      get :new
      expect(assigns(:rt_volunteer_subscribe)).to be_a_new(Rt::VolunteerSubscribe)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :edit, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::VolunteerSubscribe' do
        expect {
          post :create, rt_volunteer_subscribe: valid_attributes
        }.to change(Rt::VolunteerSubscribe, :count).by(1)
      end

      it 'assigns a newly created rt_volunteer_subscribe as @rt_volunteer_subscribe' do
        post :create, rt_volunteer_subscribe: valid_attributes
        expect(assigns(:rt_volunteer_subscribe)).to be_a(Rt::VolunteerSubscribe)
        expect(assigns(:rt_volunteer_subscribe)).to be_persisted
      end

      it 'redirects to the created rt_volunteer_subscribe' do
        post :create, rt_volunteer_subscribe: valid_attributes
        expect(response).to redirect_to(rt_volunteer_subscribes_url)
      end
    end

    # context 'with invalid params' do
    #   it 'assigns a newly created but unsaved rt_volunteer_subscribe as @rt_volunteer_subscribe' do
    #     post :create, rt_volunteer_subscribe: invalid_attributes
    #     expect(assigns(:rt_volunteer_subscribe)).to be_a_new(Rt::VolunteerSubscribe)
    #   end

    #   it 're-renders the "new" template' do
    #     post :create, rt_volunteer_subscribe: invalid_attributes
    #     expect(response).to render_template('new')
    #   end
    # end
  end

  describe "PUT #update" do
    context 'with valid params' do

      it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
        volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
        put :update, id: volunteer_subscribe.to_param, rt_volunteer_subscribe: valid_attributes
        expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
      end

      it 'redirects to rt_volunteer_subscribes' do
        volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
        put :update, id: volunteer_subscribe.to_param, rt_volunteer_subscribe: valid_attributes
        expect(response).to redirect_to(rt_volunteer_subscribes_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the rt_volunteer_subscribe as @rt_volunteer_subscribe' do
        volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
        put :update, id: volunteer_subscribe.to_param, rt_volunteer_subscribe: invalid_attributes
        expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
      end

      # it 're-renders the "edit" template' do
      #   volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      #   put :update, id: volunteer_subscribe.to_param, rt_volunteer_subscribe: invalid_attributes
      #   expect(response).to render_template('edit')
      # end
    end
  end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested rt_volunteer_subscribe' do
  #     volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
  #     expect {
  #       delete :destroy, id: volunteer_subscribe.to_param
  #     }.to change(Rt::VolunteerSubscribe, :count).by(volunteer_subscribe.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the rt_volunteer_subscribes list' do
  #     volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
  #     delete :destroy, id: volunteer_subscribe.to_param
  #     expect(response).to redirect_to(rt_volunteer_subscribes_url)
  #   end
  # end

  describe "GET #process_request_form" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :process_request_form, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end
  end
  
  describe "GET #reject_request_form" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :reject_request_form, id: volunteer_subscribe.to_param, request_form: {id: volunteer_subscribe.request_form.id}
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end

    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe with status processing' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_subscribe.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: volunteer_subscribe.to_param, request_form: {id: volunteer_subscribe.request_form.id}
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end

    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe with reject_request_form' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_subscribe.request_form,
        manager_id: 1)
      
      get :pre_reject_request_form, id: volunteer_subscribe.to_param
      get :reject_request_form,id: volunteer_subscribe.to_param, request_form: {id: volunteer_subscribe.request_form.id}
      
      expect(response.status).to eq(200)    
    end

    it 'redirects to rt_volunteer_subscribes_url' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_subscribe.to_param      
      expect(response).to redirect_to(rt_volunteer_subscribes_url)
    end

  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :pre_approve_request_form, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end

    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe with request' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      request_form_attributes[:req_reason_id]=@req_reason.id
      request_form_attributes[:rt_extendable_id]=volunteer_subscribe.id
      request_form_attributes[:rt_extendable_type]="Rt::VolunteerSubscribe"
      request_form_attributes[:request_type_id]=1
      request_form_attributes[:user_id]=@user.id
      request_form = RequestForm.create! request_form_attributes
      status_manager = RtController::StatusManager.new(request_form: request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      
      get :pre_approve_request_form, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end
  end

  describe "GET #pre_reject_request_form" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :pre_reject_request_form, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end

    it 'redirects to rt_volunteer_subscribes' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_subscribe.to_param      
      expect(response).to redirect_to(rt_volunteer_subscribes_url)
    end
  end

  describe "GET #mark_request_form_as_processing" do
    it 'assigns the requested rt_volunteer_subscribe as @rt_volunteer_subscribe' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_subscribe.to_param
      expect(assigns(:rt_volunteer_subscribe)).to eq(volunteer_subscribe)
    end

    it 'redirects to rt_volunteer_subscribes' do
      volunteer_subscribe = Rt::VolunteerSubscribe.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_subscribe.to_param      
      expect(response).to redirect_to(rt_volunteer_subscribes_url)
    end
  end

end
