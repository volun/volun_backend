require 'rails_helper'

RSpec.describe Rt::VolunteersDemandsController, type: :controller do
  before(:each) do
    @user=create(:user)
    @user_entity=User.create! (attributes_for :user, :entity_log)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)

  end

  let(:valid_attributes) {
    attributes_for :volunteers_demand
  }

  let(:invalid_attributes) {
    attributes_for :volunteers_demand, :invalid
  }

  let(:request_form_attributes){
    attributes_for :request_form
  }

  describe "GET #index" do
    it 'assigns all rt_volunteers_demands as @rt_volunteers_demands' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index
      expect(assigns(:rt_volunteers_demands)).not_to eq(nil)
    end

    it 'csv' do
      @volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index, format: :csv
      expect(assigns(:rt_volunteers_demands)).not_to eq(nil)
    end
    

    it 'assigns all rt_volunteers_demands as @rt_volunteers_demands with filter' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index, search_pending: 'true',search_processing: 'true',search_approved: 'false',search_rejected:'false'
      expect(assigns(:rt_volunteers_demands)).not_to eq(nil)
    end

    it 'assigns all rt_volunteers_demands as @rt_volunteers_demands with filter' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index, search_pending: 'false',search_processing: 'false',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteers_demands)).not_to eq([volunteers_demand])
    end

    it 'assigns all rt_volunteers_demands as @rt_volunteers_demands with search_entity' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index, search_pending: 'true',search_entity: 1,search_processing: 'true',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteers_demands)).not_to eq([volunteers_demand])
    end

    it 'assigns all rt_volunteers_demands as @rt_volunteers_demands with q:{}' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :index, search_pending: 'true',q: {s:""},search_processing: 'true',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteers_demands)).not_to eq(nil)
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :show, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end
  end

  # describe "GET #new" do
  #   it 'assigns a new rt_volunteers_demand as @rt_volunteers_demand' do
  #     get :new
  #     expect(assigns(:rt_volunteers_demand)).to be_a_new(Rt::VolunteersDemand)
  #   end
  # end

  describe "GET #edit" do
    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :edit, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::VolunteersDemand' do
        expect {
          post :create, rt_volunteers_demand: valid_attributes
        }.to change(Rt::VolunteersDemand, :count).by(1)
      end

      it 'assigns a newly created rt_volunteers_demand as @rt_volunteers_demand' do
        post :create, rt_volunteers_demand: valid_attributes
        expect(assigns(:rt_volunteers_demand)).to be_a(Rt::VolunteersDemand)
        expect(assigns(:rt_volunteers_demand)).to be_persisted
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
        volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
        put :update, id: volunteers_demand.to_param, rt_volunteers_demand: valid_attributes
        expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
      end
    end
  end


  describe "GET #process_request_form" do
    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :process_request_form, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand with param comments' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :process_request_form, id: volunteers_demand.to_param, input_comments: "MyString"
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end
  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :pre_approve_request_form, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand with status processing' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      request_form_attributes[:req_reason_id]=@req_reason.id
      request_form_attributes[:rt_extendable_id]=volunteers_demand.id
      request_form_attributes[:rt_extendable_type]="Rt::VolunteersDemand"
      request_form_attributes[:request_type_id]=7
      request_form_attributes[:user_id]=@user.id
      request_form = RequestForm.create! request_form_attributes
      status_manager = RtController::StatusManager.new(request_form: request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing

      get :pre_approve_request_form, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

  end

  describe "GET #reject_request_form" do
    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :reject_request_form, id: volunteers_demand.to_param, request_form: {id: volunteers_demand.request_form.id}
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand with status processing' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteers_demand.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: volunteers_demand.to_param, request_form: {id: volunteers_demand.request_form.id}
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

    it 'assigns the requested rt_volunteers_demand as @rt_volunteers_demand with reject_request_form' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteers_demand.request_form,
        manager_id: 1)
      
      get :pre_reject_request_form, id: volunteers_demand.to_param
      get :reject_request_form,id: volunteers_demand.to_param, request_form: {id: volunteers_demand.request_form.id}
      
      expect(response.status).to eq(200)    
    end

    it 'redirects to rt_volunteers_demands_url' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteers_demand.to_param      
      expect(response).to redirect_to(rt_volunteers_demands_url)
    end

  end

  describe "GET #pre_reject_request_form" do
    it 'assigns the 
    requested volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :pre_reject_request_form, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).not_to eq(nil)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested volunteers_demand as @rt_volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).to eq(volunteers_demand)
    end

    it 'redirects to rt_volunteers_demands_url' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteers_demand.to_param      
      expect(response).to redirect_to(rt_volunteers_demands_url)
    end
  end

  describe "GET #mark_request_form_as_processing" do
    it 'assigns the requested volunteers_demand as @volunteers_demand' do
      volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteers_demand.to_param
      expect(assigns(:rt_volunteers_demand)).not_to eq(nil)
    end

    # it 'redirects to rt_volunteers_demands_url' do
    #   volunteers_demand = Rt::VolunteersDemand.create! valid_attributes
    #   get :mark_request_form_as_processing, id: volunteers_demand.to_param      
    #   expect(response).to redirect_to(rt_volunteers_demands_url)
    # end
  end
end
