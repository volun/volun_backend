require 'rails_helper'

RSpec.describe Rt::VolunteerAmendmentsController, type: :controller do
  before(:each) do
    @user=create(:user)
    @user_volunteer=User.create! (attributes_for :user, :volunteer_log)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)
  end

  let(:valid_attributes) {
    attributes_for :volunteer_amendment
  }

  let(:invalid_attributes) {
    attributes_for :volunteer_amendment, :invalid
  }
  let(:request_form_attributes){
    attributes_for :request_form
  }


  describe "GET #index" do
    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index
      expect(assigns(:rt_volunteer_amendments)).not_to eq([volunteer_amendment])
    end

    it 'csv' do
      @volunteer_amendment = Rt::VolunteerAmendment.all
      get :index, format: :csv
      expect(assigns(:rt_volunteer_amendments)).not_to eq([@volunteer_amendment])
    end

    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments with filter true' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index, search_pending: 'true',search_processing: 'true',search_approved: 'false',search_rejected:'false'
      expect(assigns(:rt_volunteer_amendments)).to eq([volunteer_amendment])
    end

    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments with filter false' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index, search_pending: 'false',search_processing: 'false',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteer_amendments)).not_to eq([volunteer_amendment])
    end

    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments with params q blank' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index, q: {:sort => ""}
      expect(assigns(:rt_volunteer_amendments)).not_to eq([volunteer_amendment])
    end

    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments without search_district blank' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index, :search_district =>"-280"
      expect(assigns(:rt_volunteer_amendments)).not_to eq([volunteer_amendment])
    end

    it 'assigns all rt_volunteer_amendments as @rt_volunteer_amendments without entertext' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :index, :enter_text =>"X"
      expect(assigns(:rt_volunteer_amendments)).not_to eq([volunteer_amendment])
    end

    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :show, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_volunteer_amendment as @rt_volunteer_amendment' do
      get :new
      expect(assigns(:rt_volunteer_amendment)).to be_a_new(Rt::VolunteerAmendment)
    end
  end
  # describe "GET #edit" do
  #   it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
  #     volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #     get :edit, id: volunteer_amendment.to_param
  #     expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
  #   end
  # end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::VolunteerAmendment' do
        expect {
          post :create, rt_volunteer_amendment: valid_attributes
        }.to change(Rt::VolunteerAmendment, :count).by(1)
      end

      it 'assigns a newly created rt_volunteer_amendment as @rt_volunteer_amendment' do
        post :create, rt_volunteer_amendment: valid_attributes
        expect(assigns(:rt_volunteer_amendment)).to be_a(Rt::VolunteerAmendment)
        expect(assigns(:rt_volunteer_amendment)).to be_persisted
      end
    end
  end

  # describe "PUT #update" do
  #   context 'with valid params' do
  #     let(:new_attributes) {
  #       skip("Add a hash of attributes valid for your model")
  #     }

  #     it 'updates the requested rt_volunteer_amendment' do
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #       put :update, id: volunteer_amendment.to_param, rt_volunteer_amendment: new_attributes
  #       volunteer_amendment.reload
  #       skip("Add assertions for updated state")
  #     end

  #     it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #       put :update, id: volunteer_amendment.to_param, rt_volunteer_amendment: valid_attributes
  #       expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
  #     end

  #     it 'redirects to rt_volunteer_amendments' do
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #       put :update, id: volunteer_amendment.to_param, rt_volunteer_amendment: valid_attributes
  #       expect(response).to redirect_to(rt_volunteer_amendments_url)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns the rt_volunteer_amendment as @rt_volunteer_amendment' do
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #       put :update, id: volunteer_amendment.to_param, rt_volunteer_amendment: invalid_attributes
  #       expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
  #     end

  #     it 're-renders the "edit" template' do
  #       volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #       put :update, id: volunteer_amendment.to_param, rt_volunteer_amendment: invalid_attributes
  #       expect(response).to render_template('edit')
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested rt_volunteer_amendment' do
  #     volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #     expect {
  #       delete :destroy, id: volunteer_amendment.to_param
  #     }.to change(Rt::VolunteerAmendment, :count).by(volunteer_amendment.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the rt_volunteer_amendments list' do
  #     volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #     delete :destroy, id: volunteer_amendment.to_param
  #     expect(response).to redirect_to(rt_volunteer_amendments_url)
  #   end
  # end

  describe "GET #process_request_form" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :process_request_form, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment with comments' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :process_request_form, id: volunteer_amendment.to_param, input_comments: "MyString"
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end
    
  end

  # describe "GET #pre_approve_request_form" do
  #   it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
  #     volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #     get :pre_approve_request_form, id: volunteer_amendment.to_param
  #     expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
  #   end

  #   it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
  #     volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
  #     status_manager = RtController::StatusManager.new(request_form: volunteer_amendment.request_form,
  #       manager_id: 1)
  #     status_manager.mark_request_form_as_processing
  #     get :pre_approve_request_form, id: volunteer_amendment.to_param
  #     expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
  #   end
  # end

  describe "GET #pre_reject_request_form" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :pre_reject_request_form, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end
  end

  describe "GET #reject_request_form" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :reject_request_form, id: volunteer_amendment.to_param, request_form: {id: volunteer_amendment.request_form.id}
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment with status processing' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_amendment.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: volunteer_amendment.to_param, request_form: {id: volunteer_amendment.request_form.id}
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment with reject_request_form' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_amendment.request_form,
        manager_id: 1)
      
      get :pre_reject_request_form, id: volunteer_amendment.to_param
      get :reject_request_form,id: volunteer_amendment.to_param, request_form: {id: volunteer_amendment.request_form.id}
      
      expect(response.status).to eq(200)    
    end

    it 'redirects to rt_volunteer_amendments_url' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_amendment.to_param      
      expect(response).to redirect_to(rt_volunteer_amendments_url)
    end

  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :pre_approve_request_form, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment with request' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      request_form_attributes[:req_reason_id]=@req_reason.id
      request_form_attributes[:rt_extendable_id]=volunteer_amendment.id
      request_form_attributes[:rt_extendable_type]="Rt::VolunteerAmendment"
      request_form_attributes[:request_type_id]=3
      request_form_attributes[:user_id]=@user_volunteer.id
      request_form = RequestForm.create! request_form_attributes
      status_manager = RtController::StatusManager.new(request_form: request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      
      get :pre_approve_request_form, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'redirects to rt_volunteer_amendments' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_amendment.to_param      
      expect(response).to redirect_to(rt_volunteer_amendments_url)
    end
  end

  describe "GET #mark_request_form_as_processing" do
    it 'assigns the requested rt_volunteer_amendment as @rt_volunteer_amendment' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_amendment.to_param
      expect(assigns(:rt_volunteer_amendment)).to eq(volunteer_amendment)
    end

    it 'redirects to rt_volunteer_amendments' do
      volunteer_amendment = Rt::VolunteerAmendment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_amendment.to_param      
      expect(response).to redirect_to(rt_volunteer_amendments_url)
    end
  end

end
