require 'rails_helper'

RSpec.describe Rt::VolunteerAppointmentsController, type: :controller do
  before(:each) do
    @user=create(:user)
    @user_volunteer=User.create! (attributes_for :user, :volunteer_log)
    sign_in @user
    @req_reason=Req::Reason.create! (attributes_for :req_reason)
  end

  let(:valid_attributes) {
    attributes_for :volunteer_appointment
  }

  let(:invalid_attributes) {
    attributes_for :volunteer_appointment, :invalid
  }

  let(:request_form_attributes){
    attributes_for :request_form
  }

  describe "GET #index" do
    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index
      expect(assigns(:rt_volunteer_appointments)).to eq([volunteer_appointment])
    end

    it 'csv' do
      @volunteer_appointment = Rt::VolunteerAppointment.all
      get :index, format: :csv
      expect(assigns(:rt_volunteer_appointments)).to eq([@volunteer_appointment])
    end

    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments without params' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index, q:{s: ""}
      expect(assigns(:rt_volunteer_appointments)).to eq([volunteer_appointment])
    end

    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments with params district' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index,  search_district: "-280"
      expect(assigns(:rt_volunteer_appointments)).not_to eq([volunteer_appointment])
    end

    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments with params district other' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index,  search_district: "Centro"
      expect(assigns(:rt_volunteer_appointments)).not_to eq([volunteer_appointment])
    end
   
    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments with statuses' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index, search_pending: 'true',search_processing: 'true',search_approved: 'false',search_rejected:'false'
      expect(assigns(:rt_volunteer_appointments)).to eq([volunteer_appointment])
    end

    it 'assigns all rt_volunteer_appointments as @rt_volunteer_appointments with other statuses' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :index, search_pending: 'false',search_processing: 'false',search_approved: 'true',search_rejected:'true'
      expect(assigns(:rt_volunteer_appointments)).not_to eq([volunteer_appointment])
    end
  end

  describe "GET #show" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :show, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end
  end

  describe "GET #new" do
    it 'assigns a new rt_volunteer_appointment as @rt_volunteer_appointment' do
      get :new
      expect(assigns(:rt_volunteer_appointment)).to be_a_new(Rt::VolunteerAppointment)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :edit, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Rt::VolunteerAppointment' do
        expect {
          post :create, rt_volunteer_appointment: valid_attributes
        }.to change(Rt::VolunteerAppointment, :count).by(1)
      end

      it 'assigns a newly created rt_volunteer_appointment as @rt_volunteer_appointment' do
        post :create, rt_volunteer_appointment: valid_attributes
        expect(assigns(:rt_volunteer_appointment)).to be_a(Rt::VolunteerAppointment)
        expect(assigns(:rt_volunteer_appointment)).to be_persisted
      end

      # it 'redirects to the created rt_volunteer_appointment' do
      #   post :create, rt_volunteer_appointment: valid_attributes
      #   expect(response).to redirect_to(rt_volunteer_appointments_url)
      # end
    end

    # context 'with invalid params' do
    #   it 'assigns a newly created but unsaved rt_volunteer_appointment as @rt_volunteer_appointment' do
    #     post :create, rt_volunteer_appointment: invalid_attributes
    #     expect(assigns(:rt_volunteer_appointment)).to be_a_new(Rt::VolunteerAppointment)
    #   end

    #   it 're-renders the "new" template' do
    #     post :create, rt_volunteer_appointment: invalid_attributes
    #     expect(response).to render_template('new')
    #   end
    # end
  end

  describe "PUT #update" do
    context 'with valid params' do
            it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
        volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
        put :update, id: volunteer_appointment.to_param, rt_volunteer_appointment: valid_attributes
        expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
      end

      # it 'redirects to rt_volunteer_appointments' do
      #   volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      #   put :update, id: volunteer_appointment.to_param, rt_volunteer_appointment: valid_attributes
      #   expect(response).to redirect_to(rt_volunteer_appointments_url)
      # end
    end

    context 'with invalid params' do
      it 'assigns the rt_volunteer_appointment as @rt_volunteer_appointment' do
        volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
        put :update, id: volunteer_appointment.to_param, rt_volunteer_appointment: invalid_attributes
        expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
      end

    
    end
  end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested rt_volunteer_appointment' do
  #     volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
  #     expect {
  #       delete :destroy, id: volunteer_appointment.to_param
  #     }.to change(Rt::VolunteerAppointment, :count).by(volunteer_appointment.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the rt_volunteer_appointments list' do
  #     volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
  #     delete :destroy, id: volunteer_appointment.to_param
  #     expect(response).to redirect_to(rt_volunteer_appointments_url)
  #   end
  # end

  describe "GET #process_request_form" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :process_request_form, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end
  end

  describe "GET #reject_request_form" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :reject_request_form, id: volunteer_appointment.to_param, request_form: {id: volunteer_appointment.request_form.id}
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end

    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment with status processing' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_appointment.request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      get :reject_request_form, id: volunteer_appointment.to_param, request_form: {id: volunteer_appointment.request_form.id}
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end

    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment with reject_request_form' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      status_manager = RtController::StatusManager.new(request_form: volunteer_appointment.request_form,
        manager_id: 1)
      
      get :pre_reject_request_form, id: volunteer_appointment.to_param
      get :reject_request_form,id: volunteer_appointment.to_param, request_form: {id: volunteer_appointment.request_form.id}
      
      expect(response.status).to eq(200)    
    end

    it 'redirects to rt_volunteer_appointments_url' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_appointment.to_param      
      expect(response).to redirect_to(rt_volunteer_appointments_url)
    end

  end

  describe "GET #pre_approve_request_form" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :pre_approve_request_form, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end

    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment with request' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      request_form_attributes[:req_reason_id]=@req_reason.id
      request_form_attributes[:rt_extendable_id]=volunteer_appointment.id
      request_form_attributes[:rt_extendable_type]="Rt::VolunteerAppointment"
      request_form_attributes[:request_type_id]=4
      request_form_attributes[:user_id]=@user_volunteer.id
      request_form = RequestForm.create! request_form_attributes
      status_manager = RtController::StatusManager.new(request_form: request_form,
        manager_id: 1)
      status_manager.mark_request_form_as_processing
      
      get :pre_approve_request_form, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end
  end

  describe "GET #pre_reject_request_form" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :pre_reject_request_form, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end
  end

  describe "GET #mark_request_form_as_pending" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end

    it 'redirects to rt_volunteer_appointments' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :mark_request_form_as_pending, id: volunteer_appointment.to_param      
      expect(response).to redirect_to(rt_volunteer_appointments_url)
    end
  end

  describe "GET #mark_request_form_as_processing" do
    it 'assigns the requested rt_volunteer_appointment as @rt_volunteer_appointment' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_appointment.to_param
      expect(assigns(:rt_volunteer_appointment)).to eq(volunteer_appointment)
    end

    it 'redirects to rt_volunteer_appointments' do
      volunteer_appointment = Rt::VolunteerAppointment.create! valid_attributes
      get :mark_request_form_as_processing, id: volunteer_appointment.to_param      
      expect(response).to redirect_to(rt_volunteer_appointments_url)
    end
  end

end
