require 'rails_helper'

RSpec.describe AddressesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :address
  }

  let(:invalid_attributes) {
    attributes_for :address, :invalid
  }

  describe "GET #index" do
    it 'assigns all addresses as @addresses' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :index
      expect(assigns(:addresses).count).not_to eq(0)
    end
  end

  describe "GET #bdc_search_towns" do
    it 'redirect status 200' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :bdc_search_towns, address: valid_attributes, format: :json
      expect(response.status).to eq(200)
    end
  end

  describe "GET #bdc_search_roads" do
    it 'redirect status 200' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :bdc_search_roads, address: valid_attributes, format: :json
      expect(response.status).to eq(200)
    end
  end
  
  describe "GET #bdc_search_road_numbers" do
    it 'redirect status 200' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :bdc_search_road_numbers, address: valid_attributes, format: :json
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    it 'assigns the requested address as @address' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :show, id: address.to_param
      expect(assigns(:address)).to eq(address)
    end
  end

  describe "GET #new" do
    it 'assigns a new address as @address' do
      get :new
      expect(assigns(:address)).to be_a_new(Address)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested address as @address' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      get :edit, id: address.to_param
      expect(assigns(:address)).to eq(address)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Address' do
        expect {
          valid_attributes[:id]=50
          post :create, address: valid_attributes
        }.to change(Address, :count).by(1)
      end

      it 'assigns a newly created address as @address' do
        valid_attributes[:id]=50
        post :create, address: valid_attributes
        expect(assigns(:address)).to be_a(Address)
        expect(assigns(:address)).to be_persisted
      end

      it 'redirects to the created address' do
        valid_attributes[:id]=50
        post :create, address: valid_attributes
        expect(response).to redirect_to(addresses_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved address as @address' do
        invalid_attributes[:id]=50
        post :create, address: invalid_attributes
        expect(assigns(:address)).to be_a_new(Address)
      end

      it 're-renders the "new" template' do
        invalid_attributes[:id]=50
        post :create, address: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      it 'assigns the requested address as @address' do
        valid_attributes[:id]=50
        address = Address.create! valid_attributes
        put :update, id: address.to_param, address: valid_attributes
        expect(assigns(:address)).to eq(address)
      end

      it 'redirects to addresses' do
        valid_attributes[:id]=50
        address = Address.create! valid_attributes
        put :update, id: address.to_param, address: valid_attributes
        expect(response).to redirect_to(addresses_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the address as @address' do
        valid_attributes[:id]=50
        address = Address.create! valid_attributes
        put :update, id: address.to_param, address: invalid_attributes
        expect(assigns(:address)).to eq(address)
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested address' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      expect {
        delete :destroy, id: address.to_param
      }.to change(Address, :count).by(address.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the addresses list' do
      valid_attributes[:id]=50
      address = Address.create! valid_attributes
      delete :destroy, id: address.to_param
      expect(response).to redirect_to(addresses_url)
    end
  end

end
