require 'rails_helper'

RSpec.describe Volun::AssessmentsProjectsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :assessments_project
  }

  let(:invalid_attributes) {
    attributes_for :assessments_project, :invalid
  }

  # describe "GET #index" do
  #   it 'assigns all volun_assessments_projects as @volun_assessments_projects' do
  #     assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #     get :index
  #     expect(assigns(:volun_assessments_projects)).to eq([assessments_project])
  #   end
  # end

  # describe "GET #show" do
  #   it 'assigns the requested volun_assessments_project as @volun_assessments_project' do
  #     assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #     get :show, id: assessments_project.to_param
  #     expect(assigns(:volun_assessments_project)).to eq(assessments_project)
  #   end
  # end

  # describe "GET #new" do
  #   it 'assigns a new volun_assessments_project as @volun_assessments_project' do
  #     get :new
  #     expect(assigns(:volun_assessments_project)).to be_a_new(Volun::AssessmentsProject)
  #   end
  # end

  # describe "GET #edit" do
  #   it 'assigns the requested volun_assessments_project as @volun_assessments_project' do
  #     assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #     get :edit, id: assessments_project.to_param
  #     expect(assigns(:volun_assessments_project)).to eq(assessments_project)
  #   end
  # end

  # describe "POST #create" do
  #   context 'with valid params' do
  #     it 'creates a new Volun::AssessmentsProject' do
  #       expect {
  #         post :create, volun_assessments_project: valid_attributes
  #       }.to change(Volun::AssessmentsProject, :count).by(1)
  #     end

  #     it 'assigns a newly created volun_assessments_project as @volun_assessments_project' do
  #       post :create, volun_assessments_project: valid_attributes
  #       expect(assigns(:volun_assessments_project)).to be_a(Volun::AssessmentsProject)
  #       expect(assigns(:volun_assessments_project)).to be_persisted
  #     end

  #     it 'redirects to the created volun_assessments_project' do
  #       post :create, volun_assessments_project: valid_attributes
  #       expect(response).to redirect_to(volun_assessments_projects_url)
  #     end
  #   end
  # end

  # describe "PUT #update" do
  #   context 'with valid params' do
  #     it 'assigns the requested volun_assessments_project as @volun_assessments_project' do
  #       assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #       put :update, id: assessments_project.to_param, volun_assessments_project: valid_attributes
  #       expect(assigns(:volun_assessments_project)).to eq(assessments_project)
  #     end

  #     it 'redirects to volun_assessments_projects' do
  #       assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #       put :update, id: assessments_project.to_param, volun_assessments_project: valid_attributes
  #       expect(response).to redirect_to(volun_assessments_projects_url)
  #     end
  #   end

  #   context 'with invalid params' do
  #     it 'assigns the volun_assessments_project as @volun_assessments_project' do
  #       assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #       put :update, id: assessments_project.to_param, volun_assessments_project: invalid_attributes
  #       expect(assigns(:volun_assessments_project)).to eq(assessments_project)
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   it 'destroys the requested volun_assessments_project' do
  #     assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #     expect {
  #       delete :destroy, id: assessments_project.to_param
  #     }.to change(Volun::AssessmentsProject, :count).by(assessments_project.respond_to?(:archived?) ? 0 : -1)
  #   end

  #   it 'redirects to the volun_assessments_projects list' do
  #     assessments_project = Volun::AssessmentsProject.create! valid_attributes
  #     delete :destroy, id: assessments_project.to_param
  #     expect(response).to redirect_to(volun_assessments_projects_url)
  #   end
  # end

end
