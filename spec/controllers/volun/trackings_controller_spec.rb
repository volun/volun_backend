require 'rails_helper'

RSpec.describe Volun::TrackingsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :volun_tracking
  }

  let(:invalid_attributes) {
    attributes_for :volun_tracking, :invalid
  }

  describe "GET #index" do
    it 'assigns all volun_trackings as @volun_trackings' do
      tracking = Volun::Tracking.create! valid_attributes
      get :index, q: {volunteer_id_eq: tracking.volunteer_id}
      expect(assigns(:volun_trackings)).to eq([tracking])
    end

    it 'assigns all volun_trackings as @volun_trackings without sort' do
      tracking = Volun::Tracking.create! valid_attributes
      get :index, q: {volunteer_id_eq: tracking.volunteer_id,s: ""}
      expect(assigns(:volun_trackings)).to eq([tracking])
    end

    it 'assigns all volun_trackings as @volun_trackings with params manager' do
      tracking = Volun::Tracking.create! valid_attributes
      get :index, q: {volunteer_id_eq: tracking.volunteer_id}, search_manager: 1
      expect(assigns(:volun_trackings).count).to eq(0)
    end

    it 'assigns all volun_trackings as @volun_trackings with params project' do
      tracking = Volun::Tracking.create! valid_attributes
      get :index, q: {volunteer_id_eq: tracking.volunteer_id}, search_project: 1
      expect(assigns(:volun_trackings).count).to eq(1)
    end

    it 'assigns all volun_trackings as @volun_trackings with params project assoc' do
      tracking = Volun::Tracking.create! valid_attributes
      get :index, q: {volunteer_id_eq: tracking.volunteer_id}, project_id_assoc: 1
      expect(assigns(:volun_trackings).count).to eq(1)
    end
  end

 

  describe "GET #show" do
    it 'assigns the requested volun_tracking as @volun_tracking' do
      tracking = Volun::Tracking.create! valid_attributes
      get :show, id: tracking.to_param
      expect(assigns(:volun_tracking)).to eq(tracking)
    end
  end

  describe "GET #new" do
    it 'assigns a new volun_tracking as @volun_tracking' do
      get :new, volunteer_id: 1
      expect(assigns(:volun_tracking)).to be_a_new(Volun::Tracking)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested volun_tracking as @volun_tracking' do
      tracking = Volun::Tracking.create! valid_attributes
      get :edit, id: tracking.to_param, volunteer_id: 1
      expect(assigns(:volun_tracking)).to eq(tracking)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new Volun::Tracking' do
        expect {
          post :create, volun_tracking: valid_attributes, volunteer_id: 1
        }.to change(Volun::Tracking, :count).by(1)
      end

      it 'assigns a newly created volun_tracking as @volun_tracking' do
        post :create, volun_tracking: valid_attributes, volunteer_id: 1
        expect(assigns(:volun_tracking)).to be_a(Volun::Tracking)
        expect(assigns(:volun_tracking)).to be_persisted
      end

      it 'redirects to the created volun_tracking' do
        tracking = Volun::Tracking.create! valid_attributes
        post :create, volun_tracking: valid_attributes, volunteer_id: 1
        expect(response).to redirect_to(volun_trackings_url(q: {volunteer_id_eq: tracking.volunteer_id}))

      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do

      it 'updates the requested volun_tracking' do
        tracking = Volun::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, volun_tracking: valid_attributes, volunteer_id: 1
        tracking.reload
        expect(response).to redirect_to(volun_trackings_url(q: {volunteer_id_eq: tracking.volunteer_id}))
      end

      it 'assigns the requested volun_tracking as @volun_tracking' do
        tracking = Volun::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, volun_tracking: valid_attributes, volunteer_id: 1
        expect(assigns(:volun_tracking)).to eq(tracking)
      end

      it 'redirects to volun_trackings' do
        tracking = Volun::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, volun_tracking: valid_attributes, volunteer_id: 1
        expect(response).to redirect_to(volun_trackings_url(q: {volunteer_id_eq: tracking.volunteer_id}))
      end
    end

    context 'with invalid params' do
      it 'assigns the volun_tracking as @volun_tracking' do
        tracking = Volun::Tracking.create! valid_attributes
        put :update, id: tracking.to_param, volun_tracking: invalid_attributes, volunteer_id: 1
        expect(assigns(:volun_tracking)).to eq(tracking)
      end
    end
  end

  describe "DELETE #destroy" do

    it 'redirects to the volun_trackings list' do
      tracking = Volun::Tracking.create! valid_attributes
      delete :destroy, id: tracking.to_param
      expect(response).to redirect_to(volun_trackings_url(q: {volunteer_id_eq: tracking.volunteer_id}))
    end
  end

end
