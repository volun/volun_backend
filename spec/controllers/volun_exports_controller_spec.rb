require 'rails_helper'

RSpec.describe VolunExportsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  let(:valid_attributes) {
    attributes_for :volun_export
  }

  let(:invalid_attributes) {
    attributes_for :volun_export, :invalid
  }

  describe "GET #index" do
    it 'assigns all volun_exports as @volun_exports' do
      volun_export = VolunExport.create! valid_attributes
      get :index
      expect(assigns(:volun_exports)).to eq([volun_export])
    end
  end

  describe "GET #show" do
    it 'assigns the requested volun_export as @volun_export' do
      volun_export = VolunExport.create! valid_attributes
      get :show, id: volun_export.to_param
      expect(assigns(:volun_export)).to eq(volun_export)
    end
  end

  describe "GET #new" do
    it 'assigns a new volun_export as @volun_export' do
      get :new
      expect(assigns(:volun_export)).to be_a_new(VolunExport)
    end
  end

  describe "GET #edit" do
    it 'assigns the requested volun_export as @volun_export' do
      volun_export = VolunExport.create! valid_attributes
      get :edit, id: volun_export.to_param
      expect(assigns(:volun_export)).to eq(volun_export)
    end
  end

  describe "POST #create" do
    context 'with valid params' do
      it 'creates a new VolunExport' do
        expect {
          post :create, volun_export: valid_attributes
        }.to change(VolunExport, :count).by(1)
      end

      it 'assigns a newly created volun_export as @volun_export' do
        post :create, volun_export: valid_attributes
        expect(assigns(:volun_export)).to be_a(VolunExport)
        expect(assigns(:volun_export)).to be_persisted
      end

      it 'redirects to the created volun_export' do
        post :create, volun_export: valid_attributes
        expect(response).to redirect_to(volun_exports_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved volun_export as @volun_export' do
        post :create, volun_export: invalid_attributes
        expect(assigns(:volun_export)).to be_a_new(VolunExport)
      end

      it 're-renders the "new" template' do
        post :create, volun_export: invalid_attributes
        expect(response).to render_template('new')
      end
    end
  end

  describe "PUT #update" do
    context 'with valid params' do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it 'updates the requested volun_export' do
        volun_export = VolunExport.create! valid_attributes
        put :update, id: volun_export.to_param, volun_export: new_attributes
        volun_export.reload
        skip("Add assertions for updated state")
      end

      it 'assigns the requested volun_export as @volun_export' do
        volun_export = VolunExport.create! valid_attributes
        put :update, id: volun_export.to_param, volun_export: valid_attributes
        expect(assigns(:volun_export)).to eq(volun_export)
      end

      it 'redirects to volun_exports' do
        volun_export = VolunExport.create! valid_attributes
        put :update, id: volun_export.to_param, volun_export: valid_attributes
        expect(response).to redirect_to(volun_exports_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the volun_export as @volun_export' do
        volun_export = VolunExport.create! valid_attributes
        put :update, id: volun_export.to_param, volun_export: invalid_attributes
        expect(assigns(:volun_export)).to eq(volun_export)
      end

      it 're-renders the "edit" template' do
        volun_export = VolunExport.create! valid_attributes
        put :update, id: volun_export.to_param, volun_export: invalid_attributes
        expect(response).to render_template('edit')
      end
    end
  end

  describe "DELETE #destroy" do
    it 'destroys the requested volun_export' do
      volun_export = VolunExport.create! valid_attributes
      expect {
        delete :destroy, id: volun_export.to_param
      }.to change(VolunExport, :count).by(volun_export.respond_to?(:archived?) ? 0 : -1)
    end

    it 'redirects to the volun_exports list' do
      volun_export = VolunExport.create! valid_attributes
      delete :destroy, id: volun_export.to_param
      expect(response).to redirect_to(volun_exports_url)
    end
  end

end
