require 'rails_helper'

RSpec.describe Ws::VolunteersController, type: :controller do

    let(:request_accept_params) do
        {
            id: "1", 
            fecha_aceptacion_servicio: "10/10/2020",
            hora_aceptacion_servicio: "10:10:10"
        }
    end

    let(:request_rejet_params) do
        {
            id: "1", 
            motivo: "prueba"
        }
    end

    describe "POST services" do
        context "with valid parameters" do
  
            it "check volunteer" do
                post "login", { cid360: "1" }
                expect(response).to have_http_status(:internal_server_error)
            end

            it "get volunteer" do
                post "get_volunteer", { cid360: "1" }
                expect(response).to have_http_status(:internal_server_error)
            end

            it "request accept" do
                post "request_accept", request_accept_params
                expect(response).to have_http_status(:internal_server_error)
            end

            it "request reject" do
                post "request_reject", request_rejet_params
                expect(response).to have_http_status(:internal_server_error)
            end

        end
    end


end
