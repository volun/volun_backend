require 'rails_helper'

RSpec.describe Ws::GenericApiController, type: :controller do

  describe "POST services" do
    context "with valid parameters" do
        let(:citizen_params) do
            {
                cid360: "1", 
                nombre: "Prueba",
                apellido1: "apellido1", 
                apellido2: "apellido2", 
                email: "prueba@madrid.es",
                nif: "5082111",
                tipo_nif: "1"
            }
        end

        let(:exist_citizen_response) do
            {
                cid360: "1", 
                nombre: "Prueba",
                apellido1: "apellido1", 
                apellido2: "apellido2", 
                email: "prueba@madrid.es",
                nif: "5082111",
                tipo_nif: "1",
                token: ""
            }
        end

        let(:register_request_params) do
            {
                codigo_servicio: 1, 
                fecha_del_servicio_solicitado: "05/08/2020", 
                hora_del_servicio_solicitado: "09:10:10", 
                codigo_distrito: "1"
            }
        end
  
  
        it "create citizen" do
           post "citizen", citizen_params

           expect(response.body).to eq "{\"error\":{\"mensaje\":\"No se pudo registrar el ciudadano.\",\"codigo\":\"COD-ERROR-02\"}}"
        end

        it "create request" do
            post "register_request", register_request_params


            expect(response.body).not_to eq(nil) 
         end
      

        it 'create new citizen' do
            post "citizen", {cid360: 1, nombre: "Prueba", apellido1: "apellido1", apellido2: "apellido2", email: "prueba@madrid.es"}
            expect(response).to have_http_status(:internal_server_error)
        end

        it 'check citizen exist' do
            post "citizen", {cid360: 1, nombre: "Prueba", apellido1: "apellido1", apellido2: "apellido2", email: "prueba@madrid.es"}
            expect(response).to have_http_status(:ok)
        end

        it 'register request' do
            post "register_request", {codigo_servicio: 1, fecha_del_servicio_solicitado: "05/08/2020", hora_del_servicio_solicitado: "09:10:10", codigo_distrito: "1"}
            expect(response).to have_http_status(:ok)
        end

        it 'check valid volunteer' do 
            post "check_volunteer", {cid360: 1}
            expect(response).to have_http_status(:ok)
        end

        it 'accept request' do
            post "accept_request", {id_servicio: 1, fecha_aceptacion_servicio: "05/08/2020", hora_aceptacion_servicio: "11:11:11"}
            expect(response).to have_http_status(:ok)
        end

    end
  end


end
