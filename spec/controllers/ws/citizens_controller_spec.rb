require 'rails_helper'

RSpec.describe Ws::CitizensController, type: :controller do

  describe "POST services" do
    context "with valid parameters" do
        let(:citizen_params) do
            {
                nombre: "Prueba",
                apellido1: "apellido1", 
                apellido2: "apellido2", 
                email: "prueba@madrid.es",
                nif: "5082111W",
                tipo_nif: "1"
            }
        end

        let(:exist_citizen_response) do
            {
                cid360: "1", 
                nombre: "Prueba",
                apellido1: "apellido1", 
                apellido2: "apellido2", 
                email: "prueba@madrid.es",
                nif: "5082111",
                tipo_nif: "1",
                token: ""
            }
        end

        let(:register_request_params) do
            {
                service_id: "1",
                description: "Prueba",
                date_request: "17/09/2020",
                hour_request: "09:00:00",
                place: "Prueba",
                additional_data: "Prueba",
                district_code: "1",
                borought_code: "1",
                status: "active"
            }
        end
  
  
        it "create citizen" do
           post "citizen", citizen_params

           expect(response).to have_http_status(:bad_request)
        end

        it "create request" do
            post "register_request", register_request_params

            expect(response).to have_http_status(:internal_server_error)
        end

        it 'citizen loging' do 
            post "login", {nombre_usuario: "prueba@prueba.com", password: "asdfTghghdsh10"}
            expect(response).to have_http_status(:bad_request)
        end

        it 'new password' do
            post "new_password", {nombre_usuario: "prueba@prueba.com"}
            expect(response).to have_http_status(:internal_server_error)
        end

        it 'change password' do
            post "change_password", {nombre_usuario: "prueba@prueba.com", password: "1234567Q"}
            expect(response).to have_http_status(:internal_server_error)
        end

        it 'send credentials' do
            post "send_credentials", {id: 1, email: "prueba@prueba.com"}
            expect(response).to have_http_status(:internal_server_error)
        end

        it 'request cancel' do
            post "request_cancel", {id: 1, motivo: "prueba"}
            expect(response).to have_http_status(:error)
        end

    end
  end


end
