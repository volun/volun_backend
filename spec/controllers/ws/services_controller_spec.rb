require 'rails_helper'

RSpec.describe Ws::ServicesController, type: :controller do

    let(:register_request_params) do
        {
            service_id: "1",
            description: "Prueba",
            date_request: "17/09/2020",
            hour_request: "09:00:00",
            place: "Prueba",
            additional_data: "Prueba",
            district_code: "1",
            borought_code: "1",
            status: "active"
        }
    end

    let(:request_search_params) do
        {
            cid360: "1", 
            nombre: "Prueba",
            apellido1: "apellido1", 
            apellido2: "apellido2", 
            email: "prueba@madrid.es",
            nif: "5082111",
            tipo_nif: "1",
            token: ""
        }
    end

    let(:update_request_params) do
        {
            id: "1",
            service_id: "1",
            description: "Prueba",
            date_request: "17/09/2020",
            hour_request: "09:00:00",
            place: "Prueba",
            additional_data: "Prueba",
            district_code: "1",
            borought_code: "1",
            status: "active"
        }
    end

    let(:request_vote_params) do
        {
            type: "Citizen", 
            valoracion: "5", 
            comments: "Prueba"
        }
    end

    let(:request_questions_params) do
        {
            token: "asdf"
        }
    end

    let(:request_rate_params) do
        {
            questions: [{"id": "1", "response": "prueba"},{"id": "2", "response": "true"}]
        }
    end

    describe "POST services" do
        context "with valid parameters" do 

            it "type request" do
                post "type_request"
                expect(response).to have_http_status(:internal_server_error)
            end

            it "request search" do
                post "request_search"

                expect(response).to have_http_status(:internal_server_error)
            end
    
            it "update request" do
                post "update_request", update_request_params

                expect(response).to have_http_status(:internal_server_error)
            end

            it "request vote" do
                post "request_vote", request_vote_params
    
                expect(response).to have_http_status(:internal_server_error)
            end

            it "request questions" do
                get "request_questions"
    
                expect(response).to have_http_status(:internal_server_error)
            end

            it "request rate" do
                post "request_rate", request_rate_params
    
                expect(response).to have_http_status(:internal_server_error)
            end

        end
    end


end
