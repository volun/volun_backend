require 'rails_helper'

RSpec.describe MeetingsController, type: :controller do
    before(:each) do
        sign_in create(:user)
      end
    
      let(:valid_attributes) {
        attributes_for :meeting
      }
    
      let(:invalid_attributes) {
        attributes_for :meeting, :invalid
      }
    
      describe "GET #index" do
        it 'assigns all meetings as @meetings' do
          meeting = Meeting.create! valid_attributes
          get :index
          expect(assigns(:meetings).count).not_to eq(0)
        end
      end
    
      describe "GET #show" do
        it 'assigns the requested meeting as @meeting' do
          meeting = Meeting.create! valid_attributes
          get :show, id: meeting.to_param
          expect(assigns(:meeting)).to eq(meeting)
        end
      end
    
      describe "GET #new" do
        it 'assigns a new meeting as @meeting' do
          get :new
          expect(assigns(:meeting)).to be_a_new(Meeting)
        end
      end
    
      describe "GET #edit" do
        it 'assigns the requested meeting as @meeting' do
          meeting = Meeting.create! valid_attributes
          get :edit, id: meeting.to_param
          expect(assigns(:meeting)).to eq(meeting)
        end
      end
    
      describe "POST #create" do
        context 'with valid params' do
          it 'creates a new Meeting' do
            expect {
              post :create, meeting: valid_attributes
            }.to change(Meeting, :count).by(1)
          end
    
          it 'assigns a newly created meeting as @meeting' do
            post :create, meeting: valid_attributes
            expect(assigns(:meeting)).to be_a(Meeting)
            expect(assigns(:meeting)).to be_persisted
          end
    
          it 'redirects to the created meeting' do
            post :create, meeting: valid_attributes
            expect(response).to redirect_to(meetings_url)
          end
        end
    
        context 'with invalid params' do
          it 'assigns a newly created but unsaved meeting as @meeting' do
            post :create, meeting: invalid_attributes
            expect(assigns(:meeting)).to be_a_new(Meeting)
          end
    
          it 're-renders the "new" template' do
            post :create, meeting: invalid_attributes
            expect(response).to render_template('new')
          end
        end
      end
    
      describe "PUT #update" do
        context 'with valid params' do
          it 'assigns the requested meeting as @meeting' do
            meeting = Meeting.create! valid_attributes
            put :update, id: meeting.to_param, meeting: valid_attributes
            expect(assigns(:meeting)).to eq(meeting)
          end
    
          it 'redirects to meetings' do
            meeting = Meeting.create! valid_attributes
            put :update, id: meeting.to_param, meeting: valid_attributes
            expect(response).to redirect_to(meetings_url)
          end
        end
    
        context 'with invalid params' do
          it 'assigns the meeting as @meeting' do
            meeting = Meeting.create! valid_attributes
            put :update, id: meeting.to_param, meeting: invalid_attributes
            expect(assigns(:meeting)).to eq(meeting)
          end
    
          it 're-renders the "edit" template' do
            meeting = Meeting.create! valid_attributes
            put :update, id: meeting.to_param, meeting: invalid_attributes
            expect(response).to render_template('edit')
          end
        end
      end
    
end
