require "rails_helper"

RSpec.describe VolunteerMailer, :type => :mailer do
  it "send_login" do
    expect(VolunteerMailer.send_login("example@example.es",{user: "xxx",password: "xxx",sender: "xxx@xxx.es"}).deliver_now).not_to eq(nil)
  end

  it "send_email" do
    expect(VolunteerMailer.send_email("example@example.es",{user: "xxx",password: "xxx",sender: "xxx@xxx.es"}).deliver_now).not_to eq(nil)
  end
  it "send_massive_email" do
    expect(VolunteerMailer.send_massive_email("example@example.es",{user: "xxx",password: "xxx",sender: "xxx@xxx.es", content: "<p>hola mundo</p>"}).deliver_now).not_to eq(nil)
  end

  it "send_massive_email_upload" do
    expect(VolunteerMailer.send_massive_email("example@example.es",{upload: [{original_filename: 'dato.txt', tempfile: "hola"}], user: "xxx",password: "xxx",sender: "xxx@xxx.es", content: "<p>hola mundo</p>"}).deliver_now).not_to eq(nil)
  end         
end
