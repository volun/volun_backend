FactoryGirl.define do
  factory :citizen_service_tracking do
    citizen_service nil
    tracking_type nil
    manager nil

    trait :invalid do
    end
  end
  factory :citizen_tracking do
    citizen nil
    tracking_type nil
    manager nil

    trait :invalid do
    end
  end


  factory :how_met do
    name "MyString"
    active false

    trait :invalid do
    end
  end

  

  factory :volunteer_turn do

    trait :invalid do
    end
  end

  factory :album do
    year 2018

    trait :invalid do
    end
  end

  factory :pt_retired_volunteer, class: 'Pt::RetiredVolunteer' do
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do
    end
  end
  factory :rt_volunteer_project_subscribe, class: 'Rt::VolunteerProjectSubscribe' do
    name "MyString"
    last_name "MyString"
    phone_number_alt "MyString"
    email "MyString"

    trait :invalid do
    end
  end
  

  factory :rt_solidarity_chain, class: 'Rt::SolidarityChain' do
    full_name "MyString"
    phone_number "915133368"
    email "perezljl@madrid.es"
    action_description "MyString"
    #url_image "MyString.jpg"

    trait :invalid do
      full_name ""
      phone_number "55"
      email "MyString"
      action_description ""
    end

    trait :no_phone do
      full_name ""
      phone_number ""
      email "MyString"
      action_description ""
    end

    trait :no_mail do
      full_name ""
      phone_number "666559969"
      email ""
      action_description ""
    end

    trait :no_data do
      full_name ""
      phone_number ""
      email ""
      action_description ""
    end

    # trait :no_url_format do
    #   url_image "MyString.pdf"
    #   end
  end

  factory :resource do
    name Project.name
    description Project.name
    active true
    main true

    trait :invalid do
    end
  end


  factory :volun_known_language, class: 'Volun::KnownLanguage' do
    volunteer_id 1
    language_id 1
    language_level_id 1

    trait :invalid do
    end
  end
  factory :rt_other, class: 'Rt::Other' do
    description "MyText"

    trait :invalid do
    end
  end
  factory :rt_activity_unpublishing, class: 'Rt::ActivityUnpublishing' do

    notes "MyString"
    created_at "2018-02-12 15:19:48"
    updated_at "2018-04-27 10:11:33"

    trait :invalid do
    end
  end
  factory :req_reason, class: 'Req::Reason' do
    name "MyString"
    description "MyString"
    active true
    created_at "2018-02-12 15:19:48"
    updated_at "2018-04-27 10:11:33"

    trait :invalid do

    end
  end
  factory :rt_activity_publishing, class: 'Rt::ActivityPublishing' do
    name "MyString"

    created_at "2018-02-12 15:19:48"
    updated_at "2018-04-27 10:11:33"

    trait :invalid do
    end
  end
  factory :rt_project_unsubscribe, class: 'Rt::ProjectUnsubscribe' do
    association :project 
    reason "MyText"

    trait :invalid do
    end
  end
  factory :rt_project_unpublishing, class: 'Rt::ProjectUnpublishing' do
    project_id 1
    notes "MyText"
    created_at "2018-02-12 15:19:48"
    updated_at "2018-04-27 10:11:33"

    trait :invalid do
    end
  end
  factory :rt_project_publishing, class: 'Rt::ProjectPublishing' do
    description "MyText"
    road_type "Calle"
    road_name "MyString"
    number_type "MyString"
    road_number "MyString"
    postal_code "MyString"
    borough "MyString"
    district "MyString"
    town "MyString"
    province "MADRID" 

    trait :invalid do
    end
  end
  factory :rt_volunteers_demand, class: 'Rt::VolunteersDemand' do
    description "MyText"
    execution_start_date "2017-01-27"
    execution_end_date "2017-01-28"
    road_type "MyString"
    road_name "MyString"
    number_type "MyString"
    road_number "MyString"
    postal_code "28045"
    borough "MyString"
    district "MyString"
    town "MyString"
    province "MyString"
    requested_volunteers_num "MyString"
    volunteers_profile "MyText"
    volunteer_functions_1 "MyText"
    volunteer_functions_2 "MyText"
    volunteer_functions_3 "MyText"
    volunteer_functions_4 "MyText"
    volunteer_functions_5 "MyText"

    trait :invalid do
    end
  end
  factory :rt_entity_unsubscribe, class: 'Rt::EntityUnsubscribe' do
    project_id 1
    notes "MyText"
    created_at "2018-05-02 13:52:27"
    updated_at "2018-05-02 13:52:27"

    trait :invalid do
    end
  end
  factory :rt_entity_subscribe, class: 'Rt::EntitySubscribe' do
    name "MyString"
    description "MyText"
    vat_number "MyString"
    email "MyString"
    representative_name "MyString"
    representative_last_name "MyString"
    representative_last_name_alt "MyString"
    contact_name "MyString"
    contact_last_name "MyString"
    contact_last_name_alt "MyString"
    phone_number "MyString"
    phone_number_alt "MyString"
    publish_pictures false
    annual_survey false
    entity_type_id 4
    comments "MyText"
    other_subscribe_reason "MyText"
    association :road_type
    road_name "MyString"
    road_number "MyString"
    postal_code "MyString"
    borough "MyString"
    association :district
    town "MyString"
    association :province

    trait :invalid do
    end
  end
  factory :rt_volunteer_appointment, class: 'Rt::VolunteerAppointment' do
    notes "MyString"
    created_at "2018-05-02 13:52:27"
    updated_at "2018-05-02 13:52:27"

    trait :invalid do
    end
  end
  factory :rt_volunteer_amendment, class: 'Rt::VolunteerAmendment' do
    road_type "Calle"
    road_name "MyString"
    number_type "MyString"
    road_number "MyString"
    postal_code "MyString"
    borough "MyString"
    district "Otros"
    town "MyString"
    province "MyString"
    phone_number "MyString"
    phone_number_alt "MyString"
    email "MyString"
    notes "MyString"
    project_id nil
    created_at "2018-05-02 13:52:27"
    updated_at "2018-05-02 13:52:27"

    trait :invalid do
    end
  end
  factory :rt_volunteer_unsubscribe, class: 'Rt::VolunteerUnsubscribe' do

    project_id 1
    notes "MyText"
    created_at "2018-05-02 13:52:27"
    updated_at "2018-05-02 13:52:27"

    trait :invalid do
    end
  end

  factory :rt_volunteer_subscribe, class: 'Rt::VolunteerSubscribe' do
    name "MyString"
    last_name "MyString"
    last_name_alt "MyString"
    phone_number "MyString"
    phone_number_alt "MyString"
    email "prueba@prueba.es"
    project_id nil
    notes "Pruebas"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    district "Otros"
    postal_code "37500"

    trait :invalid do
    end
  end

  factory :pt_social, class: 'Pt::Social' do 
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    is_project_social false

    trait :invalid do
    end
  end

  factory :pt_punctual, class: 'Pt::Punctual' do 

    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do
    end
  end

  factory :pt_permanent, class: 'Pt::Permanent' do 
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    trait :invalid do
    end
  end

  factory :pt_other, class: 'Pt::Other' do 
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    trait :invalid do
    end
  end

  factory :pt_entity, class: 'Pt::Entity' do 
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    trait :invalid do
    end
  end

  factory :pt_centre, class: 'Pt::Centre' do 
    notes "MyText"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    trait :invalid do
    end
  end

  factory :pt_subvention, class: 'Pt::Subvention' do
    id 7
    representative_name "MyString"
    representative_last_name "MyString"
    representative_last_name_alt "MyString"
    id_num "MyString"
    vat_number "MyString"
    entity_registry true
    cost 0
    requested_amount 0
    subsidized_amount 0
    initial_volunteers_num 0
    has_quality_evaluation false
    notes "MyString"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"
    trait :invalid do
    end
  end

  factory :req_status_trace, class: 'Req::StatusTrace' do
    req_status_id 1
    request_form_id 1
    manager_id 1
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do
    end
  end

  factory :req_status, class: 'Req::Status' do
    id 5
    kind 5
    description "pending"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do
    end
  end

  factory :req_rejection_type, class: 'Req::RejectionType' do
    name "MyRejectionType"
    description "MyText"
    active true

    trait :invalid do
      name nil 
    end
  end

  factory :volun_assessment, class: 'Volun::Assessment' do
    volunteer_id 1
    trait_id 1
    trait_other "MyString"
    assessment false
    comments "MyText"
    created_at "2017-01-27 10:42:03"
    updated_at "2017-01-27 10:42:03"

    trait :invalid do
      association :volunteer
    end
  end

  factory :volun_assessments_project, class: 'Volun::AssessmentsProject' do
    volunteer_id 1
    trait_id 1
    project_id 1
    trait_other "MyString"
    assessment false
    comments "MyText"
    created_at "2017-01-27 10:42:03"
    updated_at "2017-01-27 10:42:03"

    trait :invalid do
      association :volunteer
    end
  end

  factory :volun_tracking, class: 'Volun::Tracking' do
    volunteer_id 1
    tracking_type_id 1
    project_id 1
    manager_id 1
    request_form_id nil
    tracked_at "2017-01-27 10:41:50"
    automatic true

    comments "MyString"
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do

    end
  end

  factory :volun_contact, class: 'Volun::Contact' do
    volunteer_id 1
    contact_result_id 1
    project_id 1
    manager_id 1
    contact_type_id 1
    contact_date "2017-01-27 10:41:50"
    comments "MyString"
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do
    end
  end

  factory :volun_availability, class: 'Volun::Availability' do
    volunteer_id 1
    day 1
    start_hour "17:00"
    end_hour "20:00"
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do

    end
  end 
 

  factory :pro_issue, class: 'Pro::Issue' do
    comments "MyText"
    start_date "2017-01-27 10:41:05"
    project_id 1
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"

    trait :invalid do
    end
  end
  factory :pro_tracking, class: 'Pro::Tracking' do

    project_id 1
    tracked_at "2017-01-27 10:40:55"
    automatic false
    comments "MyText"
    created_at "2017-01-27 10:40:55"
    updated_at "2017-01-27 10:40:55"
    manager_id 1

    trait :invalid do
    end
  end

  factory :ent_tracking, class: 'Ent::Tracking' do
    tracking_type_id 1
    entity_id 1
    manager_id 1
    tracked_at "2017-01-27 10:40:50"
    comments "MyText"
    created_at "2018-02-12 15:19:51"
    updated_at "2018-02-12 15:19:51"

    trait :invalid do
    end
  end


end
