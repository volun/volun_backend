require 'rails_helper'

RSpec.describe Pt::Permanent, type: :model do
  let(:permanent) { Pt::Permanent.find(1) }

  it 'is valid' do
    expect(permanent).to be_valid
  end
end
