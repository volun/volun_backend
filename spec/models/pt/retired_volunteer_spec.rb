require 'rails_helper'

RSpec.describe Pt::RetiredVolunteer, type: :model do
  let(:pt_retired_volunteer) { Pt::RetiredVolunteer.all.first }
 
  it 'is valid' do
    expect(pt_retired_volunteer).to be_valid
  end
end
