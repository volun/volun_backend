require 'rails_helper'

RSpec.describe Volunteer, type: :model do
  before(:each) do
    @user=create(:user)
    @tipo=FactoryGirl.create(:document_type)
    @volunteer_subscribe=Volunteer.create(id: 50,name: 'Pepito2',last_name: 'Moran',id_number: '07711579R',document_type_id: @tipo.id,active: true,email: "example@example.es",phone_number: "618844036",phone_number_alt: "618844036") 
  end

  let(:volunteer_attributtes){
    attributes_for :volunteer
  }

  describe "Valid?" do
     

    it 'is valid' do
      volunteer=Volunteer.find(1)
      expect(volunteer).to be_valid
    end
  end

  describe "Methods" do
    it 'age' do
      volunteer=Volunteer.find(1)
      expect(volunteer.age).to eq("-")
    end
    
    it 'age with birth date' do
      volunteer=Volunteer.find(1)
      volunteer.birth_date="1989-10-28"
      expect(volunteer.age).not_to eq("-")
    end

    it 'notice available with birth date' do
      volunteer=Volunteer.find(1)
      volunteer.birth_date="1989-10-28"
      expect(volunteer.notice_available?).not_to eq(true)
    end
    it 'period_active' do
      expect(Volunteer.period_active("1989-10-28","1989-10-28")).not_to eq(nil)
    end

    it 'projects coments' do
      volunteer=Volunteer.find(1)
      expect(volunteer.project_comments(1)).to eq("")
    end

    it 'to_s' do
      volunteer=Volunteer.find(1)
      expect(volunteer.to_s).to eq("#{volunteer.name} #{volunteer.last_name}")
    end

    it 'get_languages' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_languages).to eq("")
    end

    it 'language_level' do
      volunteer=Volunteer.find(1)
      expect(volunteer.language_level).to eq("")
    end

    it 'language' do
      volunteer=Volunteer.find(1)
      expect(volunteer.language).to eq("")
    end

    it 'get_collectives' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_collectives).to eq("")
    end

    it 'get_areas' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_areas).to eq("")
    end

    it 'get_projects' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_projects).to eq("")
    end

    it 'get_availabilities' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_availabilities).to eq("")
    end

    it 'get_employment_statuses' do
     
      volunteer=Volunteer.find(1)
      volunteer.employment_status=EmploymentStatus.create!(attributes_for :employment_status)
      expect(volunteer.get_employment_statuses).not_to eq("")
    end

    it 'get_statuses' do
      
      volunteer=Volunteer.find(1)
      volunteer.status=Status.create!(attributes_for :status)
      expect(volunteer.get_statuses).not_to eq("")
    end

    it 'get_list_skills' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_list_skills).to eq("")
    end

    it 'get_volun_profiles' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_volun_profiles).to eq("")
    end

    it 'check_existing' do
      volunteer=Volunteer.find(1)
      expect(volunteer.check_existing('a')).to eq(nil)
    end

    it 'mobile_number' do
      volunteer=Volunteer.find(1)
      expect(volunteer.mobile_number).to eq(nil)
    end

    it 'degree_type' do
      volunteer=Volunteer.find(1)
      expect(volunteer.degree_type).not_to eq(nil)
    end

    it 'country' do
      volunteer=Volunteer.find(1)
      expect(volunteer.country).to eq("España")
    end

    it 'get_employment_statuses' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_employment_statuses).to eq(nil)
    end

    it 'get_active_project' do
      volunteer=Volunteer.find(1)
      expect(volunteer.get_active_project(1)).not_to eq(nil)
    end

    it 'main columns' do
      expect(Volunteer.main_columns).not_to eq([])
    end

    it 'main columns all' do
      expect(Volunteer.main_columns_all).not_to eq([])
    end

    it 'main columns all short' do
      expect(Volunteer.main_columns_all_short).not_to eq([])
    end
    
    it 'main columns open data' do
      expect(Volunteer.main_columns_open_data).to eq([])
    end

    it 'ransack default' do
      expect(Volunteer.ransack_default).not_to eq({})
    end

    it 'to csv' do
      expect(Volunteer.to_csv(Volunteer.all,{})).not_to eq("")
    end

    it 'to csv open data' do
      expect(Volunteer.to_csv_open_data(Volunteer.all)).not_to eq("")
    end

    it 'to csv all short' do
      expect(Volunteer.to_csv_all_short(Volunteer.all)).not_to eq("")
    end

    it 'to csv volunteers' do
      expect(Volunteer.to_csv_volunteers).not_to eq("")
    end

    it 'to csv volunteers in districts' do
      expect(Volunteer.to_csv_volunteers_by_districts).not_to eq("")
    end

    it 'get boolean' do
      expect(Volunteer.get_boolean(true)).to eq("Sí")
    end

    it 'get number_max_length_id' do
      expect(Volunteer.number_max_length_id).not_to eq(nil)
    end

    it 'get number_max_length_name' do
      expect(Volunteer.number_max_length_name).not_to eq(nil)
    end

    it 'get volunteer_asociated' do
      expect(Volunteer.volunteer_asociated(1)).not_to eq(nil)
    end
  end

  it 'phone_number_all' do
    volunteer=Volunteer.find(1)
    expect(volunteer.phone_number_all).not_to eq(nil)
  end

  it 'district' do
    volunteer=Volunteer.find(1)
    expect(volunteer.district).not_to eq(nil)
  end

  it 'postal_code' do
    volunteer=Volunteer.find(1)
    expect(volunteer.postal_code).not_to eq(nil)
  end

  it 'vocne_formated' do
    volunteer=Volunteer.find(1)
    expect(volunteer.vocne_formated).not_to eq(nil)
  end

  it 'situation' do
    volunteer=Volunteer.find(1)
    expect(volunteer.situation).not_to eq(nil)
  end

  it 'availability_date_blank_mask' do
    volunteer=Volunteer.find(1)
    expect(volunteer.availability_date_blank_mask).not_to eq(nil)
  end

  it 'num_projects_active' do
    volunteer=Volunteer.find(1)
    expect(volunteer.num_projects_active).not_to eq(nil)
  end

  it 'main_columns_massive' do
    expect(Volunteer.main_columns_massive).not_to eq(nil)
  end

  it 'main_columns_massive' do
    expect(Volunteer.seach_volunteers("","1","")).not_to eq(nil)
  end

  describe "#VOLUN-189_Destroy" do
    it 'voluntario dado de baja' do
      AuditGenerateHelper.audit_archive(@volunteer_subscribe,  @user, true)
      expect(@volunteer_subscribe.active).to be false
      expect(@volunteer_subscribe.email).to eq ""
      expect(@volunteer_subscribe.phone_number).to eq ""
      expect(@volunteer_subscribe.phone_number_alt).to eq ""
    end
  end
  
  describe "#VOLUN-189_Recover" do
    it 'voluntario dado de alta' do
      AuditGenerateHelper.audit_archive(@volunteer_subscribe,  @user, true)
      AuditGenerateHelper.audit_recover(@volunteer_subscribe,  @user, true)
      expect(@volunteer_subscribe.active).to be true
      expect(@volunteer_subscribe.email).to eq ""
      expect(@volunteer_subscribe.phone_number).to eq ""
      expect(@volunteer_subscribe.phone_number_alt).to eq ""
    end
  end
end






