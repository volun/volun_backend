require 'rails_helper'

RSpec.describe Ent::Tracking, type: :model do
  let(:tracking) { build(:ent_tracking) }

  it 'is valid' do
    expect(tracking).to be_valid
  end

  it 'to csv' do
    expect(Ent::Tracking.to_csv(Ent::Tracking.all)).not_to eq("")
  end
end
