require 'rails_helper'

RSpec.describe Status, type: :model do
  let(:status) { build(:status) }

  it 'is valid' do
    expect(status).to be_valid
  end

  it 'to_s' do
    expect(status.to_s).to eq(status.name)
  end
end
