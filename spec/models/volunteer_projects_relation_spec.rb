require 'rails_helper'

RSpec.describe VolunteerProjectsRelation, type: :model do
  let(:volunteer_projects_relation) { build(:volunteer_projects_relation) }

  it 'is valid' do
    expect(volunteer_projects_relation).to be_valid
  end
end
