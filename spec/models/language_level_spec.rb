require 'rails_helper'

RSpec.describe LanguageLevel, type: :model do
  let(:language_level) { build(:language_level) }

  it 'is valid' do
    expect(language_level).to be_valid
  end

  it 'to_s' do
    expect(language_level.to_s).to eq(language_level.name)
  end
end
