require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  it 'is valid' do
    expect(user).to be_valid
  end

  it 'entity?' do
    expect(user.entity?).to eq(false)
  end

  it 'login_required?' do
    expect(user.login_required?).to eq(true)
  end
end
