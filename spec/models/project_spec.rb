require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:project) { build(:project) }

  it 'is valid' do
    expect(project).to be_valid
  end

  it 'to csv' do
    expect(Project.to_csv(Project.all)).not_to eq("")
  end

  it 'build_pt_extendable' do
    expect(project.build_pt_extendable).not_to eq(nil)
  end
end
