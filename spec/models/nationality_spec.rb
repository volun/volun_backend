require 'rails_helper'

RSpec.describe Nationality, type: :model do
  let(:nationality) { build(:nationality) }

  it 'is valid' do
    expect(nationality).to be_valid
  end

  it 'to_s' do
    expect(nationality.to_s).to eq(nationality.name)
  end

  it 'to_csv_nationalities' do
    expect(Nationality.to_csv_nationalities).not_to eq("")
  end
end
