require 'rails_helper'

RSpec.describe Language, type: :model do
  let(:language) { build(:language) }

  it 'is valid' do
    expect(language).to be_valid
  end
  it 'to_s' do
    expect(language.to_s).to eq(language.name)
  end
end
