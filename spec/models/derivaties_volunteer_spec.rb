require 'rails_helper'

RSpec.describe DerivatiesVolunteer, type: :model do
  let(:derivaties_volunteer) { build(:derivaties_volunteer) }

  it 'is valid' do
    expect(derivaties_volunteer).to be_valid
  end

  it 'main_columns' do
    expect(DerivatiesVolunteer.main_columns).not_to eq(nil)
  end

  it 'volunteer_id' do
    expect(derivaties_volunteer.volunteer_id).to eq(nil)
  end

  it 'volunteer_name' do
    expect(derivaties_volunteer.volunteer_name).to eq(nil)
  end

  it 'volunteer_last_name' do
    expect(derivaties_volunteer.volunteer_last_name).to eq(nil)
  end

  it 'volunteer_last_name_alt' do
    expect(derivaties_volunteer.volunteer_last_name_alt).to eq(nil)
  end

  it 'volunteer_phone_number_alt' do
    expect(derivaties_volunteer.volunteer_phone_number_alt).to eq(nil)
  end

  it 'volunteer_email' do
    expect(derivaties_volunteer.volunteer_email).to eq(nil)
  end

  it 'meeting_data' do
    expect(derivaties_volunteer.meeting_data).to eq(nil)
  end

  it 'turn_data' do
    expect(derivaties_volunteer.turn_data).to eq(nil)
  end
end
