require 'rails_helper'

RSpec.describe EntityType, type: :model do
  let(:entity_type) { build(:entity_type) }

  it 'is valid' do
    expect(entity_type).to be_valid
  end

  it 'to_s' do
    expect(entity_type.to_s).to eq(entity_type.name)
  end
end
