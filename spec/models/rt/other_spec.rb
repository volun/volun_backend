require 'rails_helper'

RSpec.describe Rt::Other, type: :model do
  let(:other) { build(:rt_other) }

  it 'is valid' do
    expect(other).to be_valid
  end

  it 'self_columns' do
    expect(Rt::Other.main_columns).not_to eq(nil)
  end

  it 'get_filter' do
    expect(Rt::Other.get_filter({})).not_to eq(nil)
  end

  it 'entity_phone_number' do
    expect(other.entity_phone_number).not_to eq(nil)
  end

  it 'entity_email' do
    expect(other.entity_email).not_to eq(nil)
  end

end
