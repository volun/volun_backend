require 'rails_helper'

RSpec.describe Rt::ProjectPublishing, type: :model do
  let(:project_publishing) { build(:project_publishing) }

  it 'is valid' do
    expect(project_publishing).to be_valid
  end

  it 'attributes' do
    expect(project_publishing.activity_publishing_attributes).not_to eq({})
  end

  it 'main_columns' do
    expect(Rt::ProjectPublishing.main_columns).not_to eq(nil)
  end

  it 'address' do
    expect(project_publishing.address).not_to eq(nil)
  end

  it 'entity' do
    expect(project_publishing.entity).not_to eq(nil)
  end

  it 'publish_date_project' do
    expect(project_publishing.publish_date_project).to eq(nil)
  end

  it 'entity_phone_number' do
    expect(project_publishing.entity_phone_number).not_to eq(nil)
  end

  it 'entity_email' do
    expect(project_publishing.entity_email).not_to eq(nil)
  end
end
