require 'rails_helper'

RSpec.describe Rt::VolunteerProjectSubscribe, type: :model do
  let(:rt_volunteer_project_subscribe) { build(:rt_volunteer_project_subscribe) }

  it 'is valid' do
    expect(rt_volunteer_project_subscribe).to be_valid
  end

  it 'main_columns' do
    expect(Rt::VolunteerProjectSubscribe.main_columns).not_to eq(nil)
  end

  it 'ransack_default' do
    expect(Rt::VolunteerProjectSubscribe.ransack_default).not_to eq(nil)
  end

  it 'volunteer_proj_subscribe' do
    expect(rt_volunteer_project_subscribe.volunteer_proj_subscribe).not_to eq(nil)
  end

  it 'volunteer_subscribe_date' do
    expect(rt_volunteer_project_subscribe.volunteer_subscribe_date).to eq(nil)
  end

  it 'phone_number_specific' do
    expect(rt_volunteer_project_subscribe.phone_number_specific).not_to eq(nil)
  end

  it 'volunteer_attributes' do
    expect(rt_volunteer_project_subscribe.volunteer_attributes).not_to eq(nil)
  end
end
