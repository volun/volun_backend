require 'rails_helper'

RSpec.describe Rt::SolidarityChain, type: :model do
  let(:solidarity_chain) { build(:rt_solidarity_chain) }

  it 'is valid' do
    expect(solidarity_chain).to be_valid
  end
  
  it "should not be valid without an email" do
    solidarity_chain.email=nil
    expect(solidarity_chain).to_not be_valid
  end

  it "should not be valid without an email valid" do
    solidarity_chain.email="MyString"
    expect(solidarity_chain).to_not be_valid
  end

  it "should not be valid without an full_name" do
    solidarity_chain.full_name=nil
    expect(solidarity_chain).to_not be_valid
  end

  it "should not be valid without an phone_number" do
    solidarity_chain.phone_number=nil
    expect(solidarity_chain).to_not be_valid
  end

  it "should not be valid without an phone_number valid" do
    solidarity_chain.phone_number="55"
    expect(solidarity_chain).to_not be_valid
  end

  it "should not be valid without an action_description" do
    solidarity_chain.action_description=nil
    expect(solidarity_chain).to_not be_valid
  end

  it "should be valid without an alias" do
    solidarity_chain.alias=nil
    expect(solidarity_chain).to be_valid
  end

  it "should not be valid without a more 25 length alias" do
    solidarity_chain.alias="1111111111111111111111111111111111111111"
    expect(solidarity_chain).to_not be_valid
  end

  it "should be valid attributtes model in columns" do
    expect(Rt::SolidarityChain.main_columns.count).not_to eq 0
  end

  it "should be valid attributtes translate" do
    expect(solidarity_chain.solidarity_chain_attributes.count).not_to eq 0
  end

  it "should be valid csv" do
    create(:rt_solidarity_chain)

    expect(Rt::SolidarityChain.to_csv(Rt::SolidarityChain.all)).to match(/^[Nombre completo][a-zA-Z]*/)
  end
end