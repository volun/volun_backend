require 'rails_helper'

RSpec.describe Rt::VolunteerSubscribe, type: :model do
  let(:volunteer_subscribe) { build(:volunteer_subscribe) }

  it 'is valid' do
    expect(volunteer_subscribe).to be_valid
  end

  it 'full_name' do
    expect(volunteer_subscribe.full_name).to eq("#{volunteer_subscribe.name} #{volunteer_subscribe.last_name} #{volunteer_subscribe.last_name_alt}")
  end

  it 'volunteer_subscribe_date' do
    expect(volunteer_subscribe.volunteer_subscribe_date).to eq(nil)
  end

  it 'volunteer_attributes' do
    expect(volunteer_subscribe.volunteer_attributes).not_to eq(nil)
  end

  it 'phone_number_specific' do
    expect(volunteer_subscribe.phone_number_specific).not_to eq(nil)
  end
end
