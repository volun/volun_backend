require 'rails_helper'

RSpec.describe Rt::VolunteerAmendment, type: :model do
  let(:volunteer_amendment) { build(:volunteer_amendment) }

  it 'is valid' do
    expect(volunteer_amendment).to be_valid
  end

  it 'attributes' do
    expect(volunteer_amendment.volunteer_attributes).not_to eq({})
  end

  it 'volunteer_subscribe_date' do
    expect(volunteer_amendment.volunteer_subscribe_date).to eq(nil)
  end

  it 'volunteer_id' do
    expect(volunteer_amendment.volunteer_id).to eq(nil)
  end

  it 'volunteer_name' do
    expect(volunteer_amendment.volunteer_name).to eq(nil)
  end

  it 'volunteer_last_name' do
    expect(volunteer_amendment.volunteer_last_name).to eq(nil)
  end

  it 'volunteer_last_name_alt' do
    expect(volunteer_amendment.volunteer_last_name_alt).to eq(nil)
  end
end
