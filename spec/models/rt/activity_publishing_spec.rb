require 'rails_helper'

RSpec.describe Rt::ActivityPublishing, type: :model do
  let(:activity_publishing) { Rt::ActivityPublishing.create!(attributes_for :activity_publishing) }

  it 'is valid' do
    expect(activity_publishing).to be_valid
  end

  it 'attributes' do
    expect(activity_publishing.activity_publishing_attributes).not_to eq({})
  end

  it 'to_s' do
    expect(activity_publishing.to_s).to eq(activity_publishing.name)
  end

  it 'main_columns' do
    expect( Rt::ActivityPublishing.main_columns).not_to eq(nil)
  end

  it 'publishing_date_activity' do
    expect(activity_publishing.publishing_date_activity).to eq(nil)
  end

  it 'entity_name_table' do
    expect(activity_publishing.entity_name_table).not_to eq(nil)
  end

  it 'entity_phone_number' do
    expect(activity_publishing.entity_phone_number).not_to eq(nil)
  end

  it 'entity_email' do
    expect(activity_publishing.entity_email).not_to eq(nil)
  end
end
