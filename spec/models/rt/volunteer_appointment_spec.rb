require 'rails_helper'

RSpec.describe Rt::VolunteerAppointment, type: :model do
  let(:volunteer_appointment) { build(:volunteer_appointment) }

  it 'is valid' do
    expect(volunteer_appointment).to be_valid
  end

  it 'attributes' do
    expect(volunteer_appointment.volunteer_attributes).not_to eq({})
  end

  it 'main_columns' do
    expect(Rt::VolunteerAppointment.main_columns).not_to eq(nil)
  end
end
