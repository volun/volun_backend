require 'rails_helper'

RSpec.describe Rt::VolunteersDemand, type: :model do
  let(:volunteers_demand) { build(:volunteers_demand) }

  it 'is valid' do
    expect(volunteers_demand).to be_valid
  end

  it 'main_columns' do
    expect(Rt::VolunteersDemand.main_columns).not_to eq(nil)
  end

  it 'address' do
    expect(volunteers_demand.address).not_to eq(nil)
  end

  it 'entity_name_table' do
    expect(volunteers_demand.entity_name_table).not_to eq(nil)
  end

  it 'entity_phone_number' do
    expect(volunteers_demand.entity_phone_number).not_to eq(nil)
  end

  it 'entity_email' do
    expect(volunteers_demand.entity_email).not_to eq(nil)
  end
end
