require 'rails_helper'

RSpec.describe Entity, type: :model do
  let(:entity) { build(:entity) }

  it 'is valid' do
    expect(entity).to be_valid
  end

  it 'to_s' do
    expect(entity.to_s).to eq(entity.name)
  end

  it 'class main_columns' do
    expect(Entity.main_columns).not_to eq([])
  end
end
