require 'rails_helper'

RSpec.describe RequestForm, type: :model do
  let(:request_form) { build(:request_form) }

  it 'is valid' do
    expect(request_form).to be_valid
  end

  it 'pending!' do
    request_form.pending!
    expect(request_form.pending?).to eq(true)
  end

  it 'processing!' do
    request_form.pending!
    request_form.processing!
    expect(request_form.processing?).to eq(true)
  end

  it 'approved!' do
    request_form.pending!
    request_form.processing!
    request_form.approved!
    expect(request_form.approved?).to eq(true)
  end

  # it 'rejected!' do
  #   request_form.pending!
  #   request_form.processing!
  #   request_form.req_reason_id=1
  #   request_form.save
  #   request_form.rejected!
  #   expect(request_form.rejected?).to eq(true)
  # end

  it 'update_status!' do
    request_form.update_status!("pending")
    expect(request_form.pending?).to eq(true)
  end
end
