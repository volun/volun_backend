require 'rails_helper'

RSpec.describe ProjectsVolunteer, type: :model do
  let(:projects_volunteer) { build(:projects_volunteer) }

  it 'is valid' do
    expect(projects_volunteer).to be_valid
  end

  it 'district' do
    expect(projects_volunteer.district).to eq(nil)
  end

  it 'description_confirm' do
    expect(projects_volunteer.description_confirm).not_to eq(nil)
  end

  it 'description_asist' do
    expect(projects_volunteer.description_asist).not_to eq(nil)
  end

  it 'description_turn' do
    expect(projects_volunteer.description_turn).to eq(nil)
  end

  it 'self_columns' do
    expect(ProjectsVolunteer.main_columns).not_to eq(nil)
  end
  
end
