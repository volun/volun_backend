require 'rails_helper'

RSpec.describe Citizen, type: :model do
  let(:citizen) { build(:citizen) }

  it 'is valid' do
    expect(citizen).to be_valid
  end
end
