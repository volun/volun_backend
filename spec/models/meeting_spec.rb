require 'rails_helper'

RSpec.describe Meeting, type: :model do
  let(:meeting) { build(:meeting) }

  it 'is valid' do
    expect(meeting).to be_valid
  end

  it 'main_columns' do
    expect(Meeting.main_columns).not_to eq(nil)
  end

  it 'to_s' do
    expect(meeting.to_s).not_to eq(nil)
  end
end
