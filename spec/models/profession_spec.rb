require 'rails_helper'

RSpec.describe Profession, type: :model do
  let(:profession) { build(:profession) }

  it 'is valid' do
    expect(profession).to be_valid
  end

  it 'to_s' do
    expect(profession.to_s).to eq(profession.name)
  end
end
