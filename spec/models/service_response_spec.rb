require 'rails_helper'

RSpec.describe ServiceResponse, type: :model do
  let(:service_response) { build(:service_response) }

  it 'is valid' do
    expect(service_response).to be_valid
  end
end
