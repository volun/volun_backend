require 'rails_helper'

RSpec.describe LinkType, type: :model do
  let(:link_type) { LinkType.find(1) }

  it 'is valid' do
    expect(link_type).to be_valid
  end

  it 'file_kinds_i18n' do
    expect(LinkType.file_kinds_i18n).not_to eq({})
  end

  it 'visible_kinds_i18n' do
    expect(LinkType.visible_kinds_i18n).not_to eq({})
  end

  it 'logo_kind' do
    expect(LinkType.logo_kind).not_to eq({})
  end
end
