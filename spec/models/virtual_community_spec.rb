require 'rails_helper'

RSpec.describe VirtualCommunity, type: :model do
  let(:virtual_community) { build(:virtual_community) }

  it 'is valid' do
    expect(virtual_community).to be_valid
  end
end
