require 'rails_helper'

RSpec.describe VolunProfile, type: :model do
  let(:volun_profile) { build(:volun_profile) }

  it 'is valid' do
    expect(volun_profile).to be_valid
  end

  it 'main_columns' do
    expect(VolunProfile.main_columns).not_to eq(nil)
  end

  it 'to_s' do
    expect(volun_profile.to_s).not_to eq(nil)
  end
end
