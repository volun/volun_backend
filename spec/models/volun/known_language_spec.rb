require 'rails_helper'

RSpec.describe Volun::KnownLanguage, type: :model do
  let(:known_language) { Volun::KnownLanguage.create!(attributes_for :known_language) }

  it 'is valid' do
    expect(known_language).to be_valid
  end
end
