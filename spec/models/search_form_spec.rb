require 'rails_helper'

RSpec.describe SearchForm, type: :model do
  let(:search_form) { build(:search_form) }

  it 'is valid' do
    expect(search_form).to be_valid
  end
end
