require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:activity) { build(:activity) }
  let(:event_type) { EventType.create!(attributes_for :event_type) }
  let(:attributes_event){
    attributes_for :event
  }

  let(:event) { 
    attributes_event[:event_type_id]=event_type.id
    Event.create!(attributes_event) }
  

  let(:timetable_event){
    attributes_for :timetable
  }


  it 'is valid' do
    timetable_event[:event_id]=event.id
    timetable=Timetable.create! timetable_event
    expect(activity).to be_valid
  end

  it 'to_s' do
    expect(activity.to_s).to eq(activity.name)
  end
end
