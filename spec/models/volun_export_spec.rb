require 'rails_helper'

RSpec.describe VolunExport, type: :model do
  let(:volun_export) { build(:volun_export) }

  it 'is valid' do
    expect(volun_export).to be_valid
  end

  it 'fields_volun_export' do
    expect(volun_export.fields_volun_export).not_to eq(nil)
  end

  it 'main_columns' do
    expect(VolunExport.main_columns).not_to eq(nil)
  end

  it 'get_fields' do
    expect(VolunExport.get_fields).not_to eq(nil)
  end

  it 'get_traslation_volunteer' do
    expect(VolunExport.get_traslation_volunteer).not_to eq(nil)
  end
end
