require 'rails_helper'

RSpec.describe Pro::Tracking, type: :model do
  let(:tracking) { build(:pro_tracking) }

  it 'is valid' do
    expect(tracking).to be_valid
  end

  it 'to csv' do
    expect(Pro::Tracking.to_csv(Pro::Tracking.all)).not_to eq("")
  end
end
