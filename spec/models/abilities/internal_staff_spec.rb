require 'rails_helper'

RSpec.describe Abilities::InternalStaff, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  it 'is valid' do
    user = User.create! valid_attributes
    manager = user.loggable
    permiso = Permission.find(1)
    permiso2 = Permission.new(id: 2)

    permiso2.manager = manager
    permiso.manager = manager
    permiso2.section = "Volunteer"
    permiso.section = "Administration"
    permiso.data= {
                audit: "1",
                resource: "0",
                catalogs: "0",
                new_campaign: "1",
                managers: "0",
                config: "0",
                order: "0",
                analyze_cv: "0"}
    permiso2.data = {
      special: '0',
      no_special: '1',
      basics: {
          create:  "0",
          update:  "0",
          read:  "1",
          destroy:  "0"
      },
      specifics: {
          mail: "0",
          sms: "0"
      }
    }

    permiso2.save
    permiso.save
    manager.permissions << permiso
    manager.permissions << permiso2
    manager.save


    ability = Abilities::InternalStaff.new(user)

    expect(ability).not_to eq(nil)
  end
end
