require 'rails_helper'

RSpec.describe Abilities::Admin, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  it 'is valid' do
    user = User.create! valid_attributes
    ability = Abilities::Admin.new(user)

    expect(ability).not_to eq(nil)
  end
end
