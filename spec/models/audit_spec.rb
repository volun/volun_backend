require 'rails_helper'

RSpec.describe Audit, type: :model do
  let(:audit) { build(:audit) }

  it 'is valid' do
    expect(audit).to be_valid
  end

  it 'audit_attributes' do
    audit.created_at = Time.zone.now
    expect(audit.audit_attributes).not_to eq(nil)
    audit.user = nil
    expect(audit.audit_attributes).not_to eq(nil)
  end

  it 'old_data_format' do
    audit.old_data = {id: 1}
    expect(audit.old_data_format).not_to eq(nil)
  end

  it 'new_data_format' do
    audit.old_data = {id: 1}
    audit.new_data = {id: 2}
    expect(audit.new_data_format).not_to eq(nil)
  end

  it 'main_columns' do
    expect(Audit.main_columns).not_to eq(nil)
  end

  it 'getOperation' do
    expect(Audit.getOperation).not_to eq(nil)
  end

  it 'getType' do
    expect(Audit.getType).not_to eq(nil)
  end

  it 'get_filter' do
    expect(Audit.get_filter({})).not_to eq(nil)
  end
end
