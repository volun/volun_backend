require 'rails_helper'

RSpec.describe Motivation, type: :model do
  let(:motivation) { build(:motivation) }

  it 'is valid' do
    expect(motivation).to be_valid
  end
  it 'to_s' do
    expect(motivation.to_s).to eq(motivation.name)
  end
end
