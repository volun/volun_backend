require 'rails_helper'

RSpec.describe Album, type: :model do
  let(:album) { build(:album) }

  it 'is valid' do
    expect(album).to be_valid
  end
end
