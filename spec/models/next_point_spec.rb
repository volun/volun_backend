require 'rails_helper'

RSpec.describe NextPoint, type: :model do
  let(:next_point) { build(:next_point) }

  it 'is valid' do
    expect(next_point).to be_valid
  end

  it 'main_columns' do
    expect(NextPoint.main_columns).not_to eq(nil)
  end

  it 'to_s' do
    expect(next_point.to_s).not_to eq(nil)
  end
end
