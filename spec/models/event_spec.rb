require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:event_type) { EventType.create!(attributes_for :event_type) }
  let(:attributes_event){
    attributes_for :event
  }

  let(:event) { 
    attributes_event[:event_type_id]=event_type.id
    Event.create!(attributes_event) }
  

  let(:timetable_event){
    attributes_for :timetable
  }


  it 'is valid' do
    timetable_event[:event_id]=event.id
    timetable=Timetable.create! timetable_event

    expect(event).to be_valid
  end
end
