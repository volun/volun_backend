require 'rails_helper'

RSpec.describe Coordination, type: :model do
  let(:coordination) { build(:coordination) }

  it 'is valid' do
    expect(coordination).to be_valid
  end

  it 'to_s' do
    expect(coordination.to_s).to eq(coordination.name)
  end
end
