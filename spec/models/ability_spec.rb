require 'rails_helper'

RSpec.describe Ability, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  it 'is valid' do
    user = User.create! valid_attributes
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'superAdmin' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: 0,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)


    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'Admin' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: 1,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)


    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'InternalStaff' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: 2,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)

   
    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'externalStaff' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: 3,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)


    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'SpecialTrust' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: 4,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)


    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end

  it 'Other' do
    manager = Manager.create( name: "Indra",
    last_name: "Indra",
    last_name_alt: "Indra",
    alias_name: "YGG007",
    role_id: nil,
    profile_id: 1,
    phone_number: "",
    active: true,
    created_at: "2018-02-12 15:19:48",
    updated_at: "2018-04-27 10:11:33",
    login: "YGG007",
    uweb_id: nil,
    document: nil,
    email: "" ,
    official_position: nil,
    personal_number: nil)


    user = User.create! valid_attributes
    user.loggable = manager
    user.save
    ability = Ability.new(user)

    expect(ability).not_to eq(nil)
  end
end
