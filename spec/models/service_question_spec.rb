require 'rails_helper'

RSpec.describe ServiceQuestion, type: :model do
  let(:service_question) { build(:service_question) }

  it 'is valid' do
    expect(service_question).to be_valid
  end
end
