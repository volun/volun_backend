require 'rails_helper'

RSpec.describe TrackingType, type: :model do
  let(:tracking_type) { build(:tracking_type) }

  it 'is valid' do
    expect(tracking_type).to be_valid
  end

  it 'get unsubscribe tracking' do
    tracking=TrackingType.get_project_subscribe
    
    expect(tracking.alias_name).to eq "subscribe"
  end

  it 'get unsubscribe tracking' do
    tracking=TrackingType.get_volunteer_unsubscribe
    
    expect(tracking.alias_name).to eq "unsubscribe"
  end

  it 'get subscribe tracking' do
    tracking=TrackingType.get_volunteer_subscribe
    
    expect(tracking.alias_name).to eq "subscribe"
  end

  it 'get amendment tracking' do
    tracking=TrackingType.get_volunteer_amendment
    
    expect(tracking.alias_name).to eq "amendment"
  end

  it 'get appointment tracking' do
    tracking=TrackingType.get_volunteer_appointment
    
    expect(tracking.alias_name).to eq "appointment"
  end
end
