require 'rails_helper'

RSpec.describe Document, type: :model do
  let(:document) { build(:document) }

  it 'is valid' do
    expect(document).to be_valid
  end

  it 'to_s' do
    expect(document.to_s).to eq(document.name)
  end
end
