require 'rails_helper'

RSpec.describe Proposal, type: :model do
  let(:proposal) { build(:proposal) }

  it 'is valid' do
    expect(proposal).to be_valid
  end

  it 'to_s' do
    expect(proposal.to_s).to eq(proposal.name)
  end
end
