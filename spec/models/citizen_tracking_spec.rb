require 'rails_helper'

RSpec.describe CitizenTracking, type: :model do
  let(:citizen_tracking) { build(:citizen_tracking) }

  it 'is valid' do
    expect(citizen_tracking).to be_valid
  end
end
