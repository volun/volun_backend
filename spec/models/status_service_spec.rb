require 'rails_helper'

RSpec.describe StatusService, type: :model do

  
  it 'es válido' do
    status_service = StatusService.new(name: 'Activo', code: 'active')
    expect(status_service).to be_valid
  end

  it 'no es válido' do
    status_service = StatusService.new(code: 'active')
    expect(status_service).to_not be_valid
  end

  it 'code no puede ser duplicado' do
    status_service = StatusService.create(name: 'Activo', code: 'active')
    status_service2 = StatusService.new(name: 'Activo2', code: 'active')
    expect(status_service2).to_not be_valid
  end

  it 'code debe ser único' do
    status_service = StatusService.create(name: 'Activo', code: 'active')
    status_service2 = StatusService.new(name: 'Activo2', code: 'active2')
    expect(status_service2).to be_valid
  end
end
