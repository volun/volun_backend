require 'rails_helper'

RSpec.describe NewCampaing, type: :model do
  let(:new_campaing) { build(:new_campaing) }

  it 'is valid' do
    expect(new_campaing).to be_valid
  end

  it 'main_columns' do
    expect(NewCampaing.main_columns).not_to eq(nil)
  end

  it 'to_s' do
    expect(new_campaing.to_s).not_to eq(nil)
  end
end
