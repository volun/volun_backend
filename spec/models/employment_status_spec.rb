require 'rails_helper'

RSpec.describe EmploymentStatus, type: :model do
  let(:employment_status) { build(:employment_status) }

  it 'is valid' do
    expect(employment_status).to be_valid
  end

  it 'to_s' do
    expect(employment_status.to_s).to eq(employment_status.name)
  end
end
