require 'rails_helper'

RSpec.describe EntityOrganization, type: :model do
  let(:entity_organization) { build(:entity_organization) }

  it 'is valid' do
    expect(entity_organization).to be_valid
  end
end
