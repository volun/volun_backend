require 'rails_helper'

RSpec.describe ProjectType, type: :model do
  let(:project_type) { ProjectType.find(1) }

  it 'is valid' do
    expect(project_type).to be_valid
  end

  it 'extendable?' do
    expect(project_type.extendable?).to eq(true)
  end
end
