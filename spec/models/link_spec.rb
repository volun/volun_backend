require 'rails_helper'

RSpec.describe Link, type: :model do
  let(:link) { build(:link) }

  it 'is valid' do
    expect(link).to be_valid
  end

  it 'main_columns' do
    expect( Link).not_to eq(nil)
  end

  it 'file_extension' do
    expect(link.file_extension).not_to eq(nil)
  end

  it 'file_base_name' do
    expect(link.file_base_name).not_to eq(nil)
  end

  it 'file_name' do
    expect(link.file_name).not_to eq(nil)
  end
end
