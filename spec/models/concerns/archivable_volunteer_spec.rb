require 'rails_helper'

RSpec.describe ArchivableVolunteer, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  it 'archived?' do
    volunteer = Volunteer.all.first
    expect(volunteer.archived?).not_to eq(nil)
  end

  it 'archive' do
    volunteer = Volunteer.all.first
    expect(volunteer.archive({})).not_to eq(nil)
  end

  it 'recover' do
    volunteer = Volunteer.all.first
    expect(volunteer.recover({})).not_to eq(nil)
  end
end
