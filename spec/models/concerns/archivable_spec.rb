require 'rails_helper'

RSpec.describe Archivable, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:activity) { build(:activity) }

  it 'archived?' do
    expect(activity.archived?).not_to eq(nil)
  end

  it 'archive' do
    expect(activity.archive).not_to eq(nil)
  end

  it 'recover' do
    expect(activity.recover).not_to eq(nil)
  end
end
