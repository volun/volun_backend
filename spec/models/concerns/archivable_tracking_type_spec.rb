require 'rails_helper'

RSpec.describe ArchivableTrackingType, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:volun_tracking) { build(:volun_tracking) }

  it 'archived?' do
    track = TrackingType.all.first
    expect(track.archived?).not_to eq(nil)
  end

  it 'archive' do
    track = TrackingType.all.second
    expect(track.archive({})).not_to eq(nil)
  end

  it 'archive_track' do
    track = TrackingType.all.first
    
    volun_tracking.tracking_type = track
    volun_tracking.save
    expect(track.archive({})).not_to eq(nil)
  end

  it 'recover' do
    track = TrackingType.all.first
    expect(track.recover({})).not_to eq(nil)
  end
end
