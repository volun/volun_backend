require 'rails_helper'

RSpec.describe ArchivableProject, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:project) { build(:project) }

  it 'archived?' do
    expect(project.archived?).not_to eq(nil)
  end

  it 'archive' do
    expect(project.archive({})).not_to eq(nil)
  end

  it 'recover' do
    expect(project.recover({})).not_to eq(nil)
  end
end
