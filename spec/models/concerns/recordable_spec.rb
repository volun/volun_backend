require 'rails_helper'

RSpec.describe Recordable, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:volun_tracking) { build(:volun_tracking) }

  it 'update_history' do
    track = TrackingType.all.first
    user = User.create! valid_attributes
    expect(track.update_history(user.id)).not_to eq(nil)
  end


end
