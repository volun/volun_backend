require 'rails_helper'

RSpec.describe ParamsFilter, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:entity) { build(:entity) }

  it 'get_filter' do
       

    expect(ParamsFilter.get_filter({},[],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_status" => "2"},["search_status"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_rrss" => "2"},["search_rrss"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_end_activity" => "2"},["search_end_activity"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_volun_allowed" => "2"},["search_volun_allowed"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_urgent" => "2"},["search_urgent"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_publish" => "2"},["search_publish"],"volunteers")).not_to eq(nil)

    expect(ParamsFilter.get_filter({"search_outstanding" => "2"},["search_outstanding"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_review" => "2"},["search_review"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_rejected" => "2"},["search_rejected"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_processing" => "2"},["search_processing"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_approved" => "2"},["search_approved"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_pending" => "2"},["search_pending"],"volunteers")).not_to eq(nil)

    expect(ParamsFilter.get_filter({"search_entity" => entity.id},["search_entity"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_volun_profile" => 'P,U'},["search_volun_profile"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_availabilities_day" => '1,2,3,4,5,6,7'},["search_availabilities_day"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_location" => "-280"},["search_location"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_location" => "Arganzuela"},["search_location"],"volunteers")).not_to eq(nil)

    expect(ParamsFilter.get_filter({"search_district" => "-280"},["search_district"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_district" => "2"},["search_district"],"volunteers")).not_to eq(nil)

    expect(ParamsFilter.get_filter({"search_collective" => "2"},["search_collective"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_info_source" => "2"},["search_info_source"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_area" => "2"},["search_area"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_type" => "1"},["search_type"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_without_auth" => "hola"},["search_without_auth"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"search_accompany_volunteer" => "hola"},["search_accompany_volunteer"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"new_availability" => "hola"},["new_availability"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"ec" => "hola"},["ec"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"no_asign" => "hola"},["no_asign"],"volunteers")).not_to eq(nil)
    expect(ParamsFilter.get_filter({"inventado" => "hola"},["inventado"],"volunteers")).not_to eq(nil)
  end
end
