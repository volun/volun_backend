require 'rails_helper'

RSpec.describe Recordable, type: :model do
  let(:valid_attributes) {
    attributes_for :user
  }

  let(:address) { build(:address) }

  it 'normalized?' do
    
    expect(address.normalized?).not_to eq(nil)
  end

  it 'bdc_validator' do
    
    expect(address.bdc_validator).not_to eq(nil)
  end

  it 'bdc_address_result' do
    
    expect(address.bdc_address_result).not_to eq(nil)
  end

  it 'reset_bdc_validator' do
    
    expect(address.reset_bdc_validator).to eq(nil)
  end

 


end
