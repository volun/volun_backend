require 'rails_helper'

RSpec.describe Skill, type: :model do
  let(:skill) { build(:skill) }

  it 'is valid' do
    expect(skill).to be_valid
  end

  it 'to_s' do
    expect(skill.to_s).to eq(skill.name)
  end
end
