require 'rails_helper'

RSpec.describe DocumentType, type: :model do
  let(:document_type) { build(:document_type) }

  it 'is valid' do
    expect(document_type).to be_valid
  end

  it 'is_cif_type?' do
    expect(document_type.is_cif_type?).to eq(false)
  end
end
