require 'rails_helper'

RSpec.describe Setting, type: :model do
  let(:setting) { build(:setting) }

  it 'is valid' do
    expect(setting).to be_valid
  end
  
  it 'type' do
    expect(setting.type).to eq("common")
  end

  it 'enabled?' do
    expect(setting.enabled?).to eq(false)
  end

end
