require 'rails_helper'

RSpec.describe Permission, type: :model do
  let(:permission) { Permission.find(1) }
  let(:manager) { Manager.find(1) }

  it 'is valid' do
    expect(permission).to be_valid
  end

  it 'auth_permissions' do
    expect(Permission.auth_permissions).not_to eq(nil)
  end

  it 'generate_permissions' do
    Permission.where(manager: manager).each {|x| x.destroy}
    expect(Permission.generate_permissions(manager)).not_to eq(nil)
  end

  it 'generate_default' do
    expect(Permission.generate_default).not_to eq(nil)
  end
end
