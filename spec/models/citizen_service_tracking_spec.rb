require 'rails_helper'

RSpec.describe CitizenServiceTracking, type: :model do
  let(:citizen_service_tracking) { build(:citizen_service_tracking) }

  it 'is valid' do
    expect(citizen_service_tracking).to be_valid
  end
end
