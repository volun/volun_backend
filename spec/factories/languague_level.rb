FactoryGirl.define do 
factory :language_level do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end