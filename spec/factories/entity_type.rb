FactoryGirl.define do 
factory :entity_type do
    name "MyText"
    created_at "2018-02-12 15:19:49"
    updated_at "2018-02-12 15:19:49"
    description "MyText"
    active true

    trait :invalid do
    end
  end
end