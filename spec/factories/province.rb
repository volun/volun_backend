FactoryGirl.define do 
  factory :province do
    name "MyText"
    code "MyText"
    active true
    created_at "2017-01-27 10:40:33"
    updated_at "2017-01-27 10:40:33"
    trait :invalid do
    end
  end

end