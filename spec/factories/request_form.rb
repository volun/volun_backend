FactoryGirl.define do 
factory :request_form do

    request_type_id 1
    rt_extendable_id 1
    rt_extendable_type 'Rt::VolunteerSubscribe'
   
    req_status_id 1
    status_date "2017-01-27 10:42:03"

    req_reason_id 1
    manager_id 1
    comments "MyText"
    created_at "2017-01-27 10:42:03"
    updated_at "2017-01-27 10:42:03"

    trait :invalid do
      req_reason_id nil
      request_type_id nil
    end
  end
end