FactoryGirl.define do  
factory :coordination do
    name "MyString"
    description "MyText"
    active true
    created_at "2017-01-27 10:40:33"
    updated_at "2017-01-27 10:40:33"

    trait :invalid do
      name nil
    end
  end
end