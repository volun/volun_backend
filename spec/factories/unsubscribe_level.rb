
FactoryGirl.define do
factory :unsubscribe_level do
    id 1
    kind 1
    description "MyString"
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do

    end
  end
end