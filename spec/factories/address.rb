FactoryGirl.define do
    factory :address do
        road_type "MyString"
        road_name "MyString"
        road_number_type "MyString"
        road_number "MyString"
        grader "MyString"
        stairs "MyString"
        floor "MyString"
        door "MyString"
        postal_code "28047"
        borough "MyString"
        district "MyString"
        town "MyString"
        province "MyString"
        country "MyString"
        ndp_code "MyString"
        local_code "MyString"
        province_code "MyString"
        town_code "MyString"
        district_code "MyString"
        class_name "MyString"
        latitude "MyString"
        longitude "MyString"
        normalize false
        created_at "2017-01-27 10:40:33"
        updated_at "2017-01-27 10:40:33"
        notes "MyString"
        comments "MyString"
        xetrs89 "MyString"
        yetrs89 "MyString"
    
        trait :invalid do
          association :road_type
        end
      end
end