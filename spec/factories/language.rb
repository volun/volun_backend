FactoryGirl.define do 
factory :language do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end