FactoryGirl.define do  
  factory :document do
    project_id 1
    name "MyString"
    description "MyText"
    extension "MyText"
    csv "MyText"
    doc_class "MyText"
    documentum_id "MyString"
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"

    trait :invalid do
      name nil
    end
  end
end