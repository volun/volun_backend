FactoryGirl.define do
    factory :academic_level do
        name "MyString"
        educational_type "MyString"
        active true
        created_at "2017-01-27 10:41:05"
        updated_at "2017-01-27 10:41:05"
    
        trait :invalid do
          name nil
          educational_type nil
          active nil
          created_at nil
          updated_at nil
        end
      end
end