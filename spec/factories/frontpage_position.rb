FactoryGirl.define do 
factory :frontpage_position do
    position 1
    description "MyText"
    active true

    trait :invalid do
      position nil
    end
  end
end