FactoryGirl.define do 
factory :user do
    login  'admin'
    email Faker::Internet.email
    password '12345678'
    password_confirmation '12345678'
    notice_type_id 1
    confirmation_token 'MXF8fAef98iVGd5gfpHg'
    confirmed_at '2017-05-18 06:06:47.754827'
    confirmation_sent_at '2017-05-18 06:06:47.754827'
    terms_of_service true
    loggable_type "Manager"
    loggable_id 1

    trait :entity_log do
      login  'entity'
      email Faker::Internet.email
      password '12345678'
      password_confirmation '12345678'
      notice_type_id 1
      confirmation_token 'MXF8fAef98iVGd5gfpHg'
      confirmed_at '2017-05-18 06:06:47.754827'
      confirmation_sent_at '2017-05-18 06:06:47.754827'
      terms_of_service true
      loggable_type "Entity"
      loggable_id 11
    end

    trait :volunteer_log do
      login  'volunteer'
      email Faker::Internet.email
      password '12345678'
      password_confirmation '12345678'
      notice_type_id 1
      confirmation_token 'MXF8fAef98iVGd5gfpHg'
      confirmed_at '2017-05-18 06:06:47.754827'
      confirmation_sent_at '2017-05-18 06:06:47.754827'
      terms_of_service true
      loggable_type "Volunteer"
      loggable_id 20
    end
  end
end