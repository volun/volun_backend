FactoryGirl.define do 
factory :skill do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end