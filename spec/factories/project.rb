FactoryGirl.define do 
factory :project do
    id 20
    district "MyString"
    review_end_project false
    name "MyString"
    active true
    description "MyText"
    functions "MyString"
    execution_start_date "2017-01-27"
    execution_end_date "2017-01-27"
    contact_name "MyString"
    contact_last_name "MyString"
    contact_last_name_alt "MyString"
    phone_number "949-502-090"
    phone_number_alt "949-502-090"
    email "MyString@a.com"
    comments "MyText"
    beneficiaries_num 1
    volunteers_num 1
    insured false
    volunteers_allowed false
    publish false
    outstanding false
    insurance_date "2017-01-27"
    project_type_id 1
    pt_extendable_id 1
    pt_extendable_type 'Pt::Social'
    entity_id 1
    created_at "2018-02-12 15:19:51"
    updated_at "2018-02-12 15:19:51"
    urgent false

    trait :invalid do
      name nil
    end
  end
end