FactoryGirl.define do  
factory :degree do
    name "MyString"
    active true
    degree_type_id 1
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"

    trait :invalid do
      name nil
    end
  end
end