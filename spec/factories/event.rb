FactoryGirl.define do 
factory :event do
    publish false
    eventable_id 1
    eventable_type 'Activity'
    event_type_id 1
    address_id 3
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"
    notes "MyText"
    comments "MyText"

    trait :invalid do
      eventable_type 'Activity'
    end
  end
end