FactoryGirl.define do 
factory :role, class: 'Role' do
    id 1
    kind 1
    description "super_admin"
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do

    end
  end
end