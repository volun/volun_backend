FactoryGirl.define do 
factory :record_history do
    association :user
    recordable_id 1
    recordable_type 'Volunteer'
    recordable_changed_at "2017-01-27 10:40:33"

    trait :invalid do
      recordable_id 3
    end

    trait :manager do
      association :manager
      recordable_type 'Manager'
      
    end
  end
end