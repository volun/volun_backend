FactoryGirl.define do 
factory :manager do
    name "Indra"
    last_name "Indra"
    last_name_alt "Indra"
    alias_name "YGG007"
    role_id 0
    profile_id 1
    phone_number ""
    active true
    created_at "2018-02-12 15:19:48"
    updated_at "2018-04-27 10:11:33"
    login "YGG007"
    uweb_id nil
    document nil
    email "" 
    official_position nil 
    personal_number nil
    trait :invalid do
      name nil
    end
  end
end