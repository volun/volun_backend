FactoryGirl.define do 
factory :event_type do
    id 1
    kind 1
    description "MyText"
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"

    trait :invalid do
      
    end
  end
end