FactoryGirl.define do 
factory :entity do
    id 11
    name "Entidad 1578"
    description nil
    vat_number "U0764741E"
    email "chester34@baucheichmann.io"
    representative_name "Sta. Nicolás Dávila Tovar"
    representative_last_name "Márquez"
    representative_last_name_alt nil
    contact_name "Sta. Pilar Márquez Terán"
    contact_last_name "Corona"
    contact_last_name_alt nil
    phone_number nil
    phone_number_alt nil
    publish_pictures true
    annual_survey false
    req_reason_id nil
    entity_type_id 1
    comments nil 
    other_subscribe_reason nil
    address_id 3
    active true
    subscribed_at "2018-02-12 15:19:49"
    unsubscribed_at nil
    created_at "2018-02-12 15:19:49"
    updated_at "2018-02-12 15:19:49"


    trait :invalid do
      name nil
    end
  end
end