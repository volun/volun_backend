FactoryGirl.define do 
factory :document_type do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end