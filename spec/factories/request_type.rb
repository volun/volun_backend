FactoryGirl.define do 
factory :request_type do
    id 13
    kind 13
    description "MyText"
    created_at "2017-01-27 10:42:03"
    updated_at "2017-01-27 10:42:03"


    trait :invalid do
      association :kind
    end
  end
end