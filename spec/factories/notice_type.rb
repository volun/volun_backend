FactoryGirl.define do 
factory :notice_type do
    id 1
    kind 1
    description "sms"
    active true
    created_at "2018-02-12 15:19:45"
    updated_at "2018-02-12 15:19:45"

    trait :invalid do
      active true
    end
  end
end