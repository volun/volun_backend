FactoryGirl.define do 
factory :proposal do
    name "MyString"
    description "MyText"
    active true

    trait :invalid do
      name nil
    end
  end
end