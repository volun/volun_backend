FactoryGirl.define do 
factory :volunteer do
    id 20
    name "Muriel"
    last_name "MyString"
    last_name_alt "MyString"
    association :document_type
    id_number "90146503G"
    gender "female" #Volunteer.genders[:female]
    birth_date "2017-01-27"
    association :nationality
    phone_number "932147896"
    phone_number_alt "618854750"
    email "volunx@email.com"
    association :address
    association :status
    association :employment_status
    vocne false
    available false
    availability_date "2022-12-27"
    association :academic_level
    subscribe_date "2017-01-27"
    comments "MyText"
    expectations "MyText"
    agreement false
    agreement_date "2017-01-27 10:41:50"
    search_authorization false
    representative_statement false
    has_driving_license false
    publish_pictures false
    annual_survey false
    subscribed_at "2017-01-27 10:41:50"
    association :manager
    association :info_source
    other_academic_info "MyText"
    association :profession
    review 0
    active true

    trait :invalid do
      name nil
    end
  end
end