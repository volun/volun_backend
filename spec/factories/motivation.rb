FactoryGirl.define do 
factory :motivation do
    name "MyString"
    active "MyString"

    trait :invalid do
      name nil
    end
  end
end