FactoryGirl.define do 
factory :sector do
    name "MyString"
    active "MyString"

    trait :invalid do
      name nil
    end
  end
end