FactoryGirl.define do 
  factory :nationality do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end