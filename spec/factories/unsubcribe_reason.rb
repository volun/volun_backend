FactoryGirl.define do 
factory :unsubscribe_reason do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end