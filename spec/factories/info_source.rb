FactoryGirl.define do   
  factory :info_source do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end