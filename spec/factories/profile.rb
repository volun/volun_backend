FactoryGirl.define do 
factory :profile do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end