FactoryGirl.define do 
factory :tracking_type do
    
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end