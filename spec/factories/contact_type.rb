FactoryGirl.define do  
factory :contact_type do
    name "MyString"
    active "MyString"
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do
      name nil
    end
  end
end