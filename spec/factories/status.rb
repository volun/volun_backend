FactoryGirl.define do 
factory :status do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end