FactoryGirl.define do 
factory :employment_status do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end