FactoryGirl.define do
    factory :borough do
        name "MyString"
        active "MyString"
        association :district
    
        trait :invalid do
          name nil
        end
      end
end