FactoryGirl.define do
    factory :activity do
        name "MyString"
        description "MyText"
        start_date "2017-01-27 10:41:05"
        end_date "2017-01-27 10:41:05"
        transport "MyText"
        pdf_url "MyString"
        publish true
        active true
        association :entity
        association :project
        share false
        created_at "2017-01-27 10:41:05"
        updated_at "2017-01-27 10:41:05"
        review_end_activity false
    
        trait :invalid do
          name nil
        end
      end
end