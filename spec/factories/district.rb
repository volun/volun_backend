FactoryGirl.define do  
  factory :district, class: 'District' do
    name "MyString"
    code "MyString"
    active true
    created_at "2018-05-02 13:52:27"
    updated_at "2018-05-02 13:52:27"

    trait :invalid do
    end
  end
end