FactoryGirl.define do 
factory :frontpage_element do
    association :frontpage_position
    active true

    trait :invalid do
      frontpage_position_id nil
    end
  end
end