FactoryGirl.define do 
factory :project_type do
    created_at "2018-02-12 15:19:51"
    updated_at "2018-02-12 15:19:51"
    id 50
    kind 50
    description "MyText"
    
    trait :invalid do
      kind nil
      description nil
    end
  end
end