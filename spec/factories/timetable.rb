FactoryGirl.define do 
factory :timetable do
    execution_date "2020-02-27"
    start_hour "10:00"
    end_hour "20:00"

    trait :invalid do
      event_id nil
    end
  end
end