FactoryGirl.define do 
factory :permission do
    association :manager
    association :resource
    create true
    update true
    read true
    delete true

    trait :invalid do
    end
  end
end