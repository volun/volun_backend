FactoryGirl.define do 
factory :link do
   
    path "MyString"
    description "MyString"
    linkable_id 1
    linkable_type "Project"
    created_at "2017-01-27 10:41:05"
    updated_at "2017-01-27 10:41:05"
    link_type_id 1

    trait :invalid do
    end
  end
end