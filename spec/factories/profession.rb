FactoryGirl.define do 
factory :profession do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end