FactoryGirl.define do 
factory :trait do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end