FactoryGirl.define do  
factory :degree_type do
    name "MyString"
    educational_type "MyString"
    active true
    created_at "2017-01-27 10:41:50"
    updated_at "2017-01-27 10:41:50"

    trait :invalid do
      name nil
    end
  end
end