FactoryGirl.define do 
factory :language_volunteer do
    name "MyString"
    active true

    trait :invalid do
      name nil
    end
  end
end