source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.4.1'
# Use sqlite3 as the database for Active Record
#gem 'sqlite3', '~> 1.4'
gem 'pg', '~> 1.2', '>= 1.2.3'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
gem 'bootstrap-sass'
gem 'bootstrap-sass-extras'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
# gem 'webpacker', '~> 5.0'
gem 'sprockets', '~> 4'
gem 'sprockets-rails'#, :require => 'sprockets/railtie'
#gem 'yarn'
# gem 'devise-bootstrap-views'
gem 'will_paginate-bootstrap'
gem 'will_paginate'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
# gem 'bootstrap-generators'
gem 'rails-assets-sweetalert2', '~> 5.1.1', source: 'http://insecure.rails-assets.org'
gem 'sweet-alert2-rails'
gem 'rails-sweetalert2-confirm'
gem 'ransack'
gem 'cancancan'
gem 'devise'
gem 'simple_form'
gem 'nested_form'
gem "cocoon"
gem 'font-awesome-rails'
gem 'responders'
gem 'enum_help'
gem 'rails-i18n'
gem 'savon', '~> 2.13'
gem 'date_validator'
gem 'paperclip', '~> 5.0.0'
gem 'coffee-rails'
gem "select2-rails"

#Clonación de modelos
gem 'amoeba'
gem 'devise-token_authenticatable'
# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Use Unicorn as the app server
gem 'unicorn'
# Gema para automatizacion de tareas diarias
gem 'whenever', require: false
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'prawn'
#gem 'ckeditor', '~> 5.1' 
gem 'ckeditor_rails_6'
#gem 'ckeditor', github: 'galetahub/ckeditor'
#gem 'ckeditor_rails', github: 'tsechingho/ckeditor-rails'
#gem 'ahoy_matey', '2.2'
gem 'chartkick', '~> 3.2'
gem 'groupdate'
gem 'highcharts', '~> 0.0.3'
gem 'rails-bootstrap-tabs', '~> 0.2.2'
gem 'delayed_job_active_record'
gem "daemons"
gem 'capistrano3-delayed-job', '~> 1.0'
# Use Active Storage variant
# gem 'image_processing', '~> 1.2'
#gem 'sprockets', '~> 4.0'

group :staging, :preproduction, :production do
  gem 'elastic-apm'
end

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  
  gem 'bullet'
  gem 'faker'
  gem 'database_cleaner'
  #  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  gem 'fuubar'   # RSpec progress bar formatter
  gem 'i18n-tasks' #  helps you find and manage missing and unused translations.
  %w[rspec-core rspec-expectations rspec-mocks rspec-rails rspec-support].each do |lib|
      gem lib, git: "https://github.com/rspec/#{lib}.git", branch: 'main' # Previously '4-0-dev' or '4-0-maintenance' branch
  end
  #gem 'rspec-rails', '~> 4.0.0.beta2'
  gem 'factory_girl_rails'
end

group :development do
  #gem 'quiet_assets', '~> 1.1'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'poltergeist'
  gem 'coveralls', require: false
  gem 'email_spec'
  #  utility and framework for executing commands in parallel on multiple remote machines, via SSH.
  gem 'capistrano',         '~> 3.5.0', require: false
  gem 'capistrano-rails',   '~> 1.1.6', require: false
  gem 'capistrano-bundler', '~> 1.1.4', require: false
  gem 'capistrano-rvm',              require: false
  
  gem 'letter_opener_web'
   gem 'letter_opener'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]


eval_gemfile './Gemfile_custom'