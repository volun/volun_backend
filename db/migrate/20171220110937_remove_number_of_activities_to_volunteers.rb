class RemoveNumberOfActivitiesToVolunteers < ActiveRecord::Migration[5.1]
  def change
    remove_column :volunteers, :number_of_activities, :integer
  end
end
