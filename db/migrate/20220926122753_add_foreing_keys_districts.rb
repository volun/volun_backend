class AddForeingKeysDistricts < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :projects, :districts
    # add_foreign_key :addresses, :districts    
    add_foreign_key :boroughts, :districts
    add_foreign_key :citizen_services, :districts
    add_foreign_key :rt_volunteer_subscribes, :districts
    add_foreign_key :rt_project_publishings, :districts
    add_foreign_key :rt_volunteer_amendments, :districts
    add_foreign_key :rt_volunteers_demands, :districts
  end
end
