class CreateVolunExports < ActiveRecord::Migration[5.1]
  def change
    create_table :volun_exports do |t|
      t.boolean :id_field, :default => true
      t.boolean :name_field, :default => true
      t.boolean :first_last_name_field, :default => true
      t.boolean :second_last_name_field, :default => true
      t.boolean :phone_field, :default => true
      t.boolean :mail_field, :default => true
      t.boolean :district_field, :default => true
      t.boolean :subscribe_date_field, :default => false
      t.boolean :unsubscribe_date_field, :default => false
      t.boolean :sex_field, :default => false
      t.boolean :birth_date_field, :default => false
      t.boolean :age_field, :default => false
      t.boolean :document_type_field, :default => false
      t.boolean :document_number_field, :default => false
      t.boolean :province_field, :default => false
      t.boolean :town_field, :default => false
      t.boolean :road_name_field, :default => false
      t.boolean :postal_code_field, :default => false
      t.boolean :academic_level_field, :default => false
      t.boolean :languages_field, :default => false
      t.boolean :project_culities_field, :default => false
      t.boolean :vocne_field, :default => false
      t.boolean :can_criminal_field, :default => false
      t.boolean :criminal_certificate_field, :default => false
      t.boolean :has_diving_license_field, :default => false 
      t.boolean :profession_field, :default => false
      t.boolean :how_know_cv_field, :default => false
      t.boolean :is_export, :default => false
      t.timestamps null: false
    end
  end
end
