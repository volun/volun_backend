class AddDenominationComponent < ActiveRecord::Migration[6.1]
  def change
    add_column :virtual_community_components, :denomination,:string, null: false, index: { unique: true}
  end
end
