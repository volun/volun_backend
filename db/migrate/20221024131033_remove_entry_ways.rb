class RemoveEntryWays < ActiveRecord::Migration[6.1]
  def change
    remove_reference :rt_volunteer_subscribes, :entry_way, index: true, foreign_key: true
    drop_table :entry_ways
  end
end
