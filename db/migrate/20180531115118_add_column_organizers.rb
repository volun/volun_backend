class AddColumnOrganizers < ActiveRecord::Migration[5.1]
  def up
    add_column :activities,:organizers,:string
  end

  def down
    remove_column :activities,:organizers,:string
  end
end
