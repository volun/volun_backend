class AddNumberOfActivitiesToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :number_of_activities, :integer
  end
end
