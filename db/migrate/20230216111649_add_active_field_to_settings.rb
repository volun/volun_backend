class AddActiveFieldToSettings < ActiveRecord::Migration[6.1]
  def change
    add_column :settings, :active, :boolean, default: true
    
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_settings
      AS SELECT id, key, value, active from settings;
    }
  end
end
