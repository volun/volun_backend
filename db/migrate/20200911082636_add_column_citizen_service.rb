class AddColumnCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :canceled_at, :date
  end
end
