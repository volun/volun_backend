class AddReferencesMeetings < ActiveRecord::Migration[6.1]
  def up
    add_reference :meetings, :type_meeting, index: true, foreign_key: true
  end

  def down
    remove_reference :meetings, :type_meeting
  end
end
