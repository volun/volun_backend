class DropConstraintKind < ActiveRecord::Migration[6.1]
  def change
    execute %{ALTER TABLE project_types  DROP CONSTRAINT kind_and_id_must_be_equal}
  end
end
