class AddActiveColumn < ActiveRecord::Migration[5.1]
  def change
    add_column :service_questions, :active, :boolean, :default => false
  end
end
