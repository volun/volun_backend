class AddTimesQuestions < ActiveRecord::Migration[5.1]
  def up
    change_table :service_questions do |t|
      t.timestamps
    end
  end

  def down 
    remove_column :service_questions, :created_at
    remove_column :service_questions, :updated_at
  end
end
