class AddFieldsCitizens < ActiveRecord::Migration[6.1]
  def change
    add_column :citizens, :validate_code_sms, :text
    add_column :citizens, :validate_sms_intents, :integer, default: 0
    add_column :citizens, :validate_sms_at, :datetime
  end
end
