class AddTimestampToJoinTablePv < ActiveRecord::Migration[6.1]
  def change
    add_timestamps :projects_volunteers, default: DateTime.now
    change_column_default :projects_volunteers, :created_at, from: DateTime.now, to: nil
    change_column_default :projects_volunteers, :updated_at, from: DateTime.now, to: nil
  end
end
