class CreateServiceQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :service_questions do |t|
      t.string :description
      t.boolean :type
    end
  end
end
