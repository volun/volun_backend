class AddPermissionAhoy < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE ahoy_events_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE ahoy_events_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE ahoy_events_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE ahoy_events_id_seq TO volun_app;

        ALTER TABLE  ahoy_events
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  ahoy_events TO volun_owner;
        GRANT SELECT ON TABLE  ahoy_events TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  ahoy_events TO volun_app;

        ALTER TABLE ahoy_visits_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE ahoy_visits_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE ahoy_visits_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE ahoy_visits_id_seq TO volun_app;

        ALTER TABLE  ahoy_visits
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  ahoy_visits TO volun_owner;
        GRANT SELECT ON TABLE  ahoy_visits TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  ahoy_visits TO volun_app;

       }
    end
  end
end
