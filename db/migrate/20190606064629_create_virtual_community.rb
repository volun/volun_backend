class CreateVirtualCommunity < ActiveRecord::Migration[5.1]
  def change
    create_table :virtual_communities do |t|
      t.integer :visits_num
      t.integer :num_share_projects
      t.integer :num_login
      t.timestamps null: false
    end
  end
end
