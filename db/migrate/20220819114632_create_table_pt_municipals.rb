class CreateTablePtMunicipals < ActiveRecord::Migration[6.1]
  def change
    create_table :pt_municipals do |t|
      t.string :notes
      
      t.timestamps
    end
  end
end
