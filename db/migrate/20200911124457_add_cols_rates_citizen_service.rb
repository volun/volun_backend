class AddColsRatesCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :citizen_vote, :string
    add_column :citizen_services, :volunteer_vote, :string
  end
end
