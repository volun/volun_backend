class AddBoroughtReference < ActiveRecord::Migration[6.1]
  def change
    add_reference :addresses, :borought
    add_reference :citizen_services, :borought
    add_reference :rt_project_publishings, :borought
    add_reference :rt_volunteer_amendments, :borought
    add_reference :rt_volunteers_demands, :borought
  end
end
