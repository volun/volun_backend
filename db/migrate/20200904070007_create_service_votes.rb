class CreateServiceVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :service_votes do |t|
      t.integer :vote
      t.references :citizen_service, foreign_key: true
      t.string :type
      t.string :comments

      t.timestamps null: false
    end
  end
end
