class CreateVolunProyMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :volun_proy_meetings do |t|
      t.references :projects_volunteer, index: true, foreign_key: true
      t.references :meeting, index: true, foreign_key: true
      t.boolean :asist
      t.boolean :confirm

      t.timestamps null: false
    end
  end
end
