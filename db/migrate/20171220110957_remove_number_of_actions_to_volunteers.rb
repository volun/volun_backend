class RemoveNumberOfActionsToVolunteers < ActiveRecord::Migration[5.1]
  def change
    remove_column :volunteers, :number_of_actions, :integer
  end
end
