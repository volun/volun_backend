class AddColumnRetiredToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :retired, :boolean, default: false 
  end
end
