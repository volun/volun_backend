class CreateShiftDefinitions < ActiveRecord::Migration[5.1]
  def change
    create_table :shift_definitions do |t|
      t.string :name, null: false, unique: true
      t.string :description
	    t.boolean :active

      t.timestamps null: false
    end
  end
end
