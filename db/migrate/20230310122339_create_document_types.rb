class CreateDocumentTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :document_types do |t|
      t.string :name, null: false
      t.integer :code, null: false
      t.boolean :active, default: true
      t.timestamps
    end

    execute %{
      INSERT INTO document_types (name, code, active, created_at, updated_at) VALUES
        ('DNI', 1, true, NOW(), NOW()),
        ('CIF', 1, true, NOW(), NOW()),
        ('NIE', 3, true, NOW(), NOW()),
        ('PASAPORTE', 2, true, NOW(), NOW()),
        ('OTROS', 4, true, NOW(), NOW()),
        ('TRT', 4, true, NOW(), NOW()),
        ('TRP', 4, true, NOW(), NOW()),
        ('TRC', 4, true, NOW(), NOW())
    }
  end
end
