class AddColumnsVolunteerSubcribe < ActiveRecord::Migration[5.1]
  def up
    add_column :rt_volunteer_subscribes, :appointment_at, :datetime
    add_column :rt_volunteer_subscribes, :contact_person, :string
    add_column :rt_volunteer_project_subscribes, :appointment_at, :datetime
    add_column :rt_volunteer_project_subscribes, :contact_person, :string
  end

  def down
    remove_column :rt_volunteer_subscribes, :appointment_at
    remove_column :rt_volunteer_subscribes, :contact_person
    remove_column :rt_volunteer_project_subscribes, :appointment_at
    remove_column :rt_volunteer_project_subscribes, :contact_person
  end
end
