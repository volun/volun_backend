class AddResourceMeeting < ActiveRecord::Migration[5.1]
  def change
    exist = Resource.find_by(name: Meeting.name)
    if exist.blank?
      resource = Resource.new(name: Meeting.name , active: true, main: false)
      resource.description = resource.class_name.model_name.human
      resource.save!
    end
  end
end
