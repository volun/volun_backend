class CreateVolunteerProjectsRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :volunteer_projects_relations do |t|
      t.references :project, index: true, foreign_key: true
      t.references :volunteer, index: true, foreign_key: true
      t.string :type_relation
      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
