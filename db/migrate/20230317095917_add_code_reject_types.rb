class AddCodeRejectTypes < ActiveRecord::Migration[6.1]
  def up
    add_column :req_rejection_types,:code,:string
  end

  def down
    remove_column :req_rejection_types,:code
  end
end
