class AddDocumentTypeReferenceToVolunteersAndCitizens < ActiveRecord::Migration[6.1]
  def change
    execute %{
      DROP VIEW IF EXISTS view_list_citizens;
      DROP VIEW IF EXISTS volun.view_list_citizens;
    }
    

    rename_column :citizens, :document_type, :document_type_id
    change_column :citizens, :document_type_id, 'integer USING CAST(document_type_id AS integer)'
    add_foreign_key :citizens, :document_types, column: :document_type_id, index: true, foreign_key: true
    rename_column :volunteers, :id_number_type_id, :document_type_id
    add_foreign_key :volunteers, :document_types, column: :document_type_id, index: true, foreign_key: true


    if Rails.env.development?
      execute %{
        CREATE OR REPLACE VIEW view_list_citizens
          AS SELECT c.id,
              c.name,
              c.last_name_1,
              c.last_name_2,
              c.phone,
              c.email,
              c.document,
              c.document_type_id,
              c.birthday,
              c.country,
              c.academic_info,
              c.info_source_id,
              c.people_at_home,
              c.start_date,
              c.last_sign_in,
              c.code_ci360,
              c.created_at,
              c.updated_at,
              c.address_id,
              c.terms_of_service,
              c.access_token,
              c.authentication_token,
              c.authentication_token_created_at,
              c.code_cid360_autentication,
              c.validation_cid360,
              c.active,
              c.email_verified,
              c.unsubscribed,
              c.market,
              c.email_confirmed,
              c.confirm_token,
              c.tester,
              c.padron,
              c.legal_msg,
              c.validated_at,
              c.unsubscribed_at,
              c.internal_management,
              c.enabled_app,
              c.removal_notice,
              c.removal_notice_at,
              c.gender_id,
              c.province_id,
              c.nationality_id,
              c.average_vote,
              c.country_code,
              c.is_death,
              c.padron_reason,
              date_part('year'::text, age(c.birthday::date::timestamp with time zone))::integer AS age_name,
              ( SELECT string_agg(DISTINCT s.name::text, ' ; '::text) AS string_agg
                    FROM citizen_services cs,
                      services s
                    WHERE cs.service_id = s.id AND cs.citizen_id = c.id) AS type_services,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id) AS count_services,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = 'finished'::text))) AS count_services_finish,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = ANY (ARRAY['prioritary'::character varying, 'active'::character varying]::text[])))) AS count_services_pending,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = 'accept'::text))) AS count_services_accept,
              ( SELECT string_agg(DISTINCT cs.service_id::character varying::text, ','::text) AS string_agg
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id) AS service_id,
              ( SELECT g.name
                    FROM genders g
                    WHERE g.id = c.gender_id) AS gender,
              ( SELECT a.district_id
                    FROM addresses a
                    WHERE a.id = c.address_id) AS district_id
            FROM citizens c;
                }
    else 
      execute %{
        CREATE OR REPLACE VIEW volun.view_list_citizens
          AS SELECT c.id,
              c.name,
              c.last_name_1,
              c.last_name_2,
              c.phone,
              c.email,
              c.document,
              c.document_type_id,
              c.birthday,
              c.country,
              c.academic_info,
              c.info_source_id,
              c.people_at_home,
              c.start_date,
              c.last_sign_in,
              c.code_ci360,
              c.created_at,
              c.updated_at,
              c.address_id,
              c.terms_of_service,
              c.access_token,
              c.authentication_token,
              c.authentication_token_created_at,
              c.code_cid360_autentication,
              c.validation_cid360,
              c.active,
              c.email_verified,
              c.unsubscribed,
              c.market,
              c.email_confirmed,
              c.confirm_token,
              c.tester,
              c.padron,
              c.legal_msg,
              c.validated_at,
              c.unsubscribed_at,
              c.internal_management,
              c.enabled_app,
              c.removal_notice,
              c.removal_notice_at,
              c.gender_id,
              c.province_id,
              c.nationality_id,
              c.average_vote,
              c.country_code,
              c.is_death,
              c.padron_reason,
              date_part('year'::text, age(c.birthday::date::timestamp with time zone))::integer AS age_name,
              ( SELECT string_agg(DISTINCT s.name::text, ' ; '::text) AS string_agg
                    FROM citizen_services cs,
                      services s
                    WHERE cs.service_id = s.id AND cs.citizen_id = c.id) AS type_services,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id) AS count_services,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = 'finished'::text))) AS count_services_finish,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = ANY (ARRAY['prioritary'::character varying, 'active'::character varying]::text[])))) AS count_services_pending,
              ( SELECT count(1) AS count
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id AND (cs.status_service_id IN ( SELECT ss.id
                            FROM status_services ss
                            WHERE ss.code::text = 'accept'::text))) AS count_services_accept,
              ( SELECT string_agg(DISTINCT cs.service_id::character varying::text, ','::text) AS string_agg
                    FROM citizen_services cs
                    WHERE cs.citizen_id = c.id) AS service_id,
              ( SELECT g.name
                    FROM genders g
                    WHERE g.id = c.gender_id) AS gender,
              ( SELECT a.district_id
                    FROM addresses a
                    WHERE a.id = c.address_id) AS district_id
            FROM citizens c;
                }
    end
  end
end