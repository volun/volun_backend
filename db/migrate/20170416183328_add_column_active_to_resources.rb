class AddColumnActiveToResources < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :active, :boolean, default: true
  end
end
