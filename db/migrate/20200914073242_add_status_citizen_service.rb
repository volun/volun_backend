class AddStatusCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :status, :string
  end
end
