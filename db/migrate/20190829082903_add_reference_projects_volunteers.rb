class AddReferenceProjectsVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_reference :projects_volunteers, :shift_definition, foreign_key: true
  end
end
