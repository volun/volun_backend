class AddReferenceShiftDefinitions < ActiveRecord::Migration[5.1]
  def change
    add_reference :shift_definitions, :projects_volunteer, foreign_key: true
  end
end
