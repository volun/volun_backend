class AddForeingKeyGender < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :volunteers, :genders
    add_foreign_key :citizens, :genders
  end
end
