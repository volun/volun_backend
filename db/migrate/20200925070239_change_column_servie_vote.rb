class ChangeColumnServieVote < ActiveRecord::Migration[5.1]
  def change
    rename_column :service_votes, :type, :type_user
  end
end
