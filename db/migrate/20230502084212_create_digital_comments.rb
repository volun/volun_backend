class CreateDigitalComments < ActiveRecord::Migration[6.1]
  def change
    create_table :digital_comments do |t|
      t.references :digital_topic, index: true, foreign_key: true
      t.boolean :active, default: true
      t.string :body, null: false
      t.references :moderate_status, index: true, foreign_key: true
      t.references :moderable, polymorphic: true, index: true
      t.references :userable, polymorphic: true, index: true
      t.references :digital_comment, index: true, foreign_key: true, null: true
      
      t.timestamps
    end
  end
end
