class AddResourcesAccompany < ActiveRecord::Migration[5.1]
  def change
    Resource.create(description: "Tipos de servicio", active: true, main: true, name: "Service", created_at: Time.now, updated_at: Time.now)
    Resource.create(description: "Ciudadanos", active: true, main: true, name: "Citizen", created_at: Time.now, updated_at: Time.now)
    Resource.create(description: "Servicios del ciudadano", active: true, main: true, name: "CitizenService", created_at: Time.now, updated_at: Time.now)
    Resource.create(description: "Preguntas de los servicios del ciudadano", active: true, main: true, name: "ServiceQuestion", created_at: Time.now, updated_at: Time.now)
    Resource.create(description: "Respuestas de los servicios del ciudadano", active: true, main: true, name: "ServiceResponse", created_at: Time.now, updated_at: Time.now)
  end
end
