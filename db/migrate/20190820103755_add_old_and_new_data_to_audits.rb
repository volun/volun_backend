class AddOldAndNewDataToAudits < ActiveRecord::Migration[5.1]
  def change   
    add_column :audits, :old_data, :text
    add_column :audits, :new_data, :text
  end
end
