class AddColumnProjectsVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :projects_volunteers, :day, :string
    add_column :projects_volunteers, :volunteer_type, :string
  end
end
