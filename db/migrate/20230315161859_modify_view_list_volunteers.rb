class ModifyViewListVolunteers < ActiveRecord::Migration[6.1]
  def change
    execute %{
      DROP VIEW IF EXISTS view_list_volunteers;
      DROP VIEW IF EXISTS volun.view_list_volunteers;
    }

    if Rails.env.development?
      execute %{
        CREATE OR REPLACE VIEW view_list_volunteers
          AS SELECT v.id,
              v.name,
              v.last_name,
              v.last_name_alt,
              v.document_type_id AS document_type_id,
              v.id_number,
              v.birth_date,
              v.nationality_id,
              v.phone_number,
              v.phone_number_alt,
              v.email,
              v.address_id,
              v.status_id,
              v.employment_status_id,
              v.vocne,
              v.available,
              v.availability_date,
              v.academic_level_id,
              v.subscribe_date,
              v.unsubscribe_date,
              v.unsubscribe_reason_id,
              v.comments,
              v.expectations,
              v.agreement,
              v.agreement_date,
              v.search_authorization,
              v.representative_statement,
              v.has_driving_license,
              v.publish_pictures,
              v.annual_survey,
              v.subscribed_at,
              v.manager_id,
              v.info_source_id,
              v.other_academic_info,
              v.error_address,
              v.error_other,
              v.review,
              v.profession_id,
              v.active,
              v.created_at,
              v.updated_at,
              v.notes,
              v.accompany_volunteer,
              v.cession_of_personal_data,
              v.publication_image_ayto,
              v.publication_image_ooaa,
              v.publication_image_social_network,
              v.publication_image_media,
              v.criminal_certificate,
              v.retired,
              v.ec,
              v.next_point_id,
              v.code_ci360,
              v.code_cid360_autentication,
              v.validation_cid360,
              v.id_autonum,
              v.market,
              v.tester,
              v.is_adult,
              v.gender_id,
              v.is_death,
              v.padron_reason,
              date_part('year'::text, age(v.birth_date::timestamp with time zone))::integer AS age_name,
              ( SELECT g.name
                    FROM genders g
                    WHERE g.id = v.gender_id) AS gender_convert,
              ( SELECT a.town
                    FROM addresses a
                    WHERE a.id = v.address_id) AS town,
              ( SELECT a.postal_code
                    FROM addresses a
                    WHERE a.id = v.address_id) AS postal_code,
              COALESCE(( SELECT d.name
                    FROM addresses a,
                      districts d
                    WHERE a.id = v.address_id AND d.id = a.district_id), 'OTROS'::character varying) AS district_custom,
              ( SELECT count(1) AS count
                    FROM projects_volunteers pv
                    WHERE pv.active = true AND v.id = pv.volunteer_id) AS num_projects_active,
              COALESCE(( SELECT d.name
                    FROM addresses a,
                      districts d
                    WHERE d.id = a.district_id AND a.id = v.address_id), 'OTROS'::character varying) AS district,
              COALESCE(( SELECT d.code::integer AS code
                    FROM addresses a,
                      districts d
                    WHERE d.id = a.district_id AND a.id = v.address_id), 22) AS district_id,
                  CASE
                      WHEN v.active = true AND v.available = true AND v.availability_date IS NOT NULL AND v.availability_date <= (now() + '7 days'::interval)::date THEN true
                      ELSE false
                  END AS notice_available_column,
                  CASE
                      WHEN v.birth_date IS NOT NULL AND date_part('year'::text, age(v.birth_date::timestamp with time zone))::integer < 18 OR v.cession_of_personal_data = false OR v.publication_image_ayto = false OR v.publication_image_ooaa = false OR v.publication_image_social_network = false OR v.publication_image_media = false OR v.vocne = true OR COALESCE(( SELECT upper(s.name::text) = 'NO APTO'::text
                        FROM statuses s
                        WHERE s.id = v.status_id), false) THEN true
                      ELSE false
                  END AS not_autorized,
              ( SELECT string_agg(a2.name::text, ','::text) AS string_agg
                    FROM areas a2,
                      areas_volunteers av
                    WHERE av.area_id = a2.id AND av.volunteer_id = v.id) AS get_areas,
              ( SELECT string_agg(c.name::text, ','::text) AS string_agg
                    FROM collectives c,
                      collectives_volunteers cv
                    WHERE cv.collective_id = c.id AND cv.volunteer_id = v.id) AS get_collectives,
              ( SELECT string_agg(is2.name::text, ','::text) AS string_agg
                    FROM info_sources is2
                    WHERE is2.id = v.info_source_id) AS get_info_source,
              ( SELECT string_agg(vp.name::text, ','::text) AS string_agg
                    FROM volun_profiles vp,
                      volun_profiles_volunteers vpv
                    WHERE vpv.volun_profile_id = vp.id AND vpv.volunteer_id = v.id) AS get_volun_profiles,
              to_char(v.subscribe_date::timestamp with time zone, 'DD/MM/YYYY HH24:MI'::text) AS subscribe_date_format
            FROM volunteers v;
      }
    else
      execute %{
        CREATE OR REPLACE VIEW volun.view_list_volunteers
          AS SELECT v.id,
              v.name,
              v.last_name,
              v.last_name_alt,
              v.document_type_id AS document_type_id,
              v.id_number,
              v.birth_date,
              v.nationality_id,
              v.phone_number,
              v.phone_number_alt,
              v.email,
              v.address_id,
              v.status_id,
              v.employment_status_id,
              v.vocne,
              v.available,
              v.availability_date,
              v.academic_level_id,
              v.subscribe_date,
              v.unsubscribe_date,
              v.unsubscribe_reason_id,
              v.comments,
              v.expectations,
              v.agreement,
              v.agreement_date,
              v.search_authorization,
              v.representative_statement,
              v.has_driving_license,
              v.publish_pictures,
              v.annual_survey,
              v.subscribed_at,
              v.manager_id,
              v.info_source_id,
              v.other_academic_info,
              v.error_address,
              v.error_other,
              v.review,
              v.profession_id,
              v.active,
              v.created_at,
              v.updated_at,
              v.notes,
              v.accompany_volunteer,
              v.cession_of_personal_data,
              v.publication_image_ayto,
              v.publication_image_ooaa,
              v.publication_image_social_network,
              v.publication_image_media,
              v.criminal_certificate,
              v.retired,
              v.ec,
              v.next_point_id,
              v.code_ci360,
              v.code_cid360_autentication,
              v.validation_cid360,
              v.id_autonum,
              v.market,
              v.tester,
              v.is_adult,
              v.gender_id,
              v.is_death,
              v.padron_reason,
              date_part('year'::text, age(v.birth_date::timestamp with time zone))::integer AS age_name,
              ( SELECT g.name
                    FROM genders g
                    WHERE g.id = v.gender_id) AS gender_convert,
              ( SELECT a.town
                    FROM addresses a
                    WHERE a.id = v.address_id) AS town,
              ( SELECT a.postal_code
                    FROM addresses a
                    WHERE a.id = v.address_id) AS postal_code,
              COALESCE(( SELECT d.name
                    FROM addresses a,
                      districts d
                    WHERE a.id = v.address_id AND d.id = a.district_id), 'OTROS'::character varying) AS district_custom,
              ( SELECT count(1) AS count
                    FROM projects_volunteers pv
                    WHERE pv.active = true AND v.id = pv.volunteer_id) AS num_projects_active,
              COALESCE(( SELECT d.name
                    FROM addresses a,
                      districts d
                    WHERE d.id = a.district_id AND a.id = v.address_id), 'OTROS'::character varying) AS district,
              COALESCE(( SELECT d.code::integer AS code
                    FROM addresses a,
                      districts d
                    WHERE d.id = a.district_id AND a.id = v.address_id), 22) AS district_id,
                  CASE
                      WHEN v.active = true AND v.available = true AND v.availability_date IS NOT NULL AND v.availability_date <= (now() + '7 days'::interval)::date THEN true
                      ELSE false
                  END AS notice_available_column,
                  CASE
                      WHEN v.birth_date IS NOT NULL AND date_part('year'::text, age(v.birth_date::timestamp with time zone))::integer < 18 OR v.cession_of_personal_data = false OR v.publication_image_ayto = false OR v.publication_image_ooaa = false OR v.publication_image_social_network = false OR v.publication_image_media = false OR v.vocne = true OR COALESCE(( SELECT upper(s.name::text) = 'NO APTO'::text
                        FROM statuses s
                        WHERE s.id = v.status_id), false) THEN true
                      ELSE false
                  END AS not_autorized,
              ( SELECT string_agg(a2.name::text, ','::text) AS string_agg
                    FROM areas a2,
                      areas_volunteers av
                    WHERE av.area_id = a2.id AND av.volunteer_id = v.id) AS get_areas,
              ( SELECT string_agg(c.name::text, ','::text) AS string_agg
                    FROM collectives c,
                      collectives_volunteers cv
                    WHERE cv.collective_id = c.id AND cv.volunteer_id = v.id) AS get_collectives,
              ( SELECT string_agg(is2.name::text, ','::text) AS string_agg
                    FROM info_sources is2
                    WHERE is2.id = v.info_source_id) AS get_info_source,
              ( SELECT string_agg(vp.name::text, ','::text) AS string_agg
                    FROM volun_profiles vp,
                      volun_profiles_volunteers vpv
                    WHERE vpv.volun_profile_id = vp.id AND vpv.volunteer_id = v.id) AS get_volun_profiles,
              to_char(v.subscribe_date::timestamp with time zone, 'DD/MM/YYYY HH24:MI'::text) AS subscribe_date_format
            FROM volunteers v;
        }
    end

  end
end
