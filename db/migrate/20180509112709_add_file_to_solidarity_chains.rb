class AddFileToSolidarityChains < ActiveRecord::Migration[5.1]
  def up
    add_attachment :rt_solidarity_chains, :url_image
  end
 
  def down
    remove_attachment :rt_solidarity_chains, :url_image
  end
end
