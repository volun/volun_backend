class CreateVirtualCommunityComponentTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :virtual_community_component_types do |t|
      t.string :title, index: {unique: true}, null: false
      t.string :code, index: {unique: true}, null: false
      t.timestamps
    end
  end
end
