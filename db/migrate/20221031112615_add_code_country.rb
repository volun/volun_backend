class AddCodeCountry < ActiveRecord::Migration[6.1]
  def change
    add_column :addresses, :country_code, :string
    add_column :citizens, :country_code, :string
  end
end
