class AddColumnsToRtVolunteerSubscribes < ActiveRecord::Migration[5.1]
  def change
    add_column :rt_volunteer_subscribes, :district, :string
    add_column :rt_volunteer_subscribes, :postal_code, :string
  end
end
