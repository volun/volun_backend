class AddColumnsToRtVolunteersDemands < ActiveRecord::Migration[5.1]
  def up
    add_column :rt_volunteers_demands,:volunteer_functions_4,:text
    add_column :rt_volunteers_demands,:volunteer_functions_5,:text
  end

  def down
    remove_column :rt_volunteers_demands,:volunteer_functions_4,:text
    remove_column :rt_volunteers_demands,:volunteer_functions_5,:text
  end
end
