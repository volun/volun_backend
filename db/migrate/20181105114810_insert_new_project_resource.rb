class InsertNewProjectResource < ActiveRecord::Migration[5.1]
  def change
    Resource.create(description: "Proyectos para jubilados del Ayuntamiento de Madrid", active: true, main: true, name: "Pt::RetiredVolunteer", created_at: Time.now, updated_at: Time.now)
  end
end
