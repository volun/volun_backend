class AddTriggerCitizens < ActiveRecord::Migration[5.1]
  def up
    execute %{
      DROP TRIGGER IF EXISTS t_delete_citizen ON citizens;
      DROP FUNCTION IF EXISTS delete_citizen ();
      CREATE FUNCTION delete_citizen () RETURNS OPAQUE AS $$
      BEGIN
        DELETE FROM audits WHERE user_id in (SELECT id FROM users WHERE loggable_type='Citizen' AND loggable_id = OLD.id);
        DELETE FROM users WHERE loggable_type='Citizen' AND loggable_id = OLD.id;
        DELETE FROM service_votes WHERE citizen_service_id in (SELECT id FROM citizen_services WHERE citizen_id = OLD.id);
        DELETE FROM service_responses WHERE citizen_service_id in (SELECT id FROM citizen_services WHERE citizen_id = OLD.id);
        DELETE FROM citizen_services WHERE citizen_id = OLD.id;
        
      RETURN OLD;
      END;
      $$ LANGUAGE 'plpgsql';

      CREATE TRIGGER t_delete_citizen BEFORE DELETE ON citizens FOR EACH ROW
      EXECUTE PROCEDURE delete_citizen();
    }
  end

  def down
    execute %{
      DROP TRIGGER IF EXISTS t_delete_citizen ON citizens;
      DROP FUNCTION IF EXISTS delete_citizen ();
    }
  end
end
