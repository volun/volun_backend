class AddColumnsOperationTypeInAudits < ActiveRecord::Migration[5.1]
  def change
  	add_column :audits, :operation_type, :string
  end
end
