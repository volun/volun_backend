class AddColumnsVolunexports < ActiveRecord::Migration[5.1]
  def change
    add_column :volun_exports, :retired, :boolean
    add_column :volun_exports, :nationality, :boolean
    add_column :volun_exports, :country, :boolean
    add_column :volun_exports, :status, :boolean
    add_column :volun_exports, :academic_level, :boolean
    add_column :volun_exports, :degree_type, :boolean
    add_column :volun_exports, :language, :boolean
    add_column :volun_exports, :language_level, :boolean
    add_column :volun_exports, :skills, :boolean
    add_column :volun_exports, :next_point, :boolean
  end
end
