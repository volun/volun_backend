class AddTriggersToDeleteProject < ActiveRecord::Migration[5.1]

  	RT_MODELS = [
    {name: Rt::ActivityPublishing.name, function: "delete_rt_activity_publishings", trigger: "t_delete_rt_activity_publishings", table: "rt_activity_publishings"},
    {name: Rt::EntitySubscribe.name, function: "delete_rt_entity_subscribes", trigger: "t_delete_rt_entity_subscribes", table: "rt_entity_subscribes"},
    {name: Rt::EntityUnsubscribe.name, function: "delete_rt_entity_unsubscribes", trigger: "t_delete_rt_entity_unsubscribes", table: "rt_entity_unsubscribes"},
    {name: Rt::ProjectPublishing.name, function: "delete_rt_project_publishings", trigger: "t_delete_rt_project_publishings", table: "rt_project_publishings"},
    {name: Rt::ProjectUnpublishing.name, function: "delete_rt_project_unpublishings", trigger: "t_delete_rt_project_unpublishings", table: "rt_project_unpublishings"},
    {name: Rt::VolunteerAmendment.name, function: "delete_rt_volunteer_amendments", trigger: "t_delete_rt_volunteer_amendments", table: "rt_volunteer_amendments"},
    {name: Rt::VolunteerProjectSubscribe.name, function: "delete_rt_volunteer_project_subscribes", trigger: "t_delete_rt_volunteer_project_subscribes", table: "rt_volunteer_project_subscribes"},   
    {name: Rt::VolunteerSubscribe.name, function: "delete_rt_volunteer_subscribes", trigger: "t_delete_rt_volunteer_subscribes", table: "rt_volunteer_subscribes"},
    {name: Rt::VolunteerUnsubscribe.name, function: "delete_rt_volunteer_unsubscribes", trigger: "t_delete_rt_volunteer_unsubscribes", table: "rt_volunteer_unsubscribes"},
    {name: Rt::VolunteersDemand.name, function: "delete_rt_volunteers_demands", trigger: "t_delete_rt_volunteers_demands", table: "rt_volunteers_demands"}

  ]
  
	  RT_MODELS_EXTRA = [
	    {table: "rt_activity_unpublishings"},
	    {table: "rt_others"},
	    {table: "rt_solidarity_chains"},
	    {table: "rt_volunteer_appointments"}
	  ]

	 PT_MODELS = [
	 	{tab: "pt_socials"},
	    {tab: "pt_centres"},
	    {tab: "pt_permanents"},
	    {tab: "pt_punctuals"},
	    {tab: "pt_subventions"},
	    {tab: "pt_entities"},
	    {tab: "pt_others"},
	    {tab: "pt_retired_volunteers"}
	  ]

  def up
  	    
    execute %{
    		DROP TRIGGER IF EXISTS t_delete_project ON projects;
		    DROP FUNCTION IF EXISTS delete_project ();
			CREATE FUNCTION delete_project () RETURNS OPAQUE AS $$
			BEGIN
			    DELETE FROM volun_trackings WHERE project_id=OLD.id;
			    DELETE FROM volun_contacts WHERE project_id=OLD.id;
			    DELETE FROM volun_assessments_projects WHERE project_id=OLD.id;
			    DELETE FROM entities_projects WHERE project_id=OLD.id;
			    DELETE FROM documents WHERE project_id=OLD.id;
			    DELETE FROM activities WHERE project_id=OLD.id;
			    DELETE FROM areas_projects WHERE project_id=OLD.id;
			    DELETE FROM collectives_projects WHERE project_id=OLD.id;
			    DELETE FROM coordinations_projects WHERE project_id=OLD.id;
			    DELETE FROM projects_volunteers WHERE project_id=OLD.id;
			    DELETE FROM pro_trackings WHERE project_id=OLD.id;
			    DELETE FROM pro_issues WHERE project_id=OLD.id;
			    DELETE FROM volun_trackings WHERE project_id=OLD.id;
			    DELETE FROM rt_activity_publishings WHERE project_id=OLD.id;
			    DELETE FROM rt_entity_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_entity_unsubscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_project_publishings WHERE project_id=OLD.id;
			    DELETE FROM rt_project_unpublishings WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_amendments WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_project_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_unsubscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteers_demands WHERE project_id=OLD.id;
				RETURN OLD;
			END;
			$$ LANGUAGE 'plpgsql';
			
			CREATE TRIGGER t_delete_project BEFORE DELETE ON projects FOR EACH ROW
				EXECUTE PROCEDURE delete_project();
		}
		
		execute %{
    		DROP TRIGGER IF EXISTS t_delete_project_pt ON projects;
		    DROP FUNCTION IF EXISTS delete_project_pt ();
			CREATE FUNCTION delete_project_pt () RETURNS OPAQUE AS $$
			BEGIN
			    IF OLD.pt_extendable_type LIKE 'Pt::Social' THEN 
			    	DELETE FROM pt_socials WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Centre' THEN 
			    	DELETE FROM pt_centres WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Permanent' THEN 
			    	DELETE FROM pt_permanents WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Punctual' THEN 
			    	DELETE FROM pt_punctuals WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Subvention' THEN 
			    	DELETE FROM pt_subventions WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Entity' THEN 
			    	DELETE FROM pt_entities WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Other' THEN 
			    	DELETE FROM pt_others WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::RetiredVolunteer' THEN 
			    	DELETE FROM pt_retired_volunteers WHERE id=OLD.pt_extendable_id;
				END IF;
			    RETURN NEW;
			END;
			$$ LANGUAGE 'plpgsql';
			
			CREATE TRIGGER t_delete_project_pt AFTER DELETE ON projects FOR EACH ROW
				EXECUTE PROCEDURE delete_project_pt();

			DROP TRIGGER IF EXISTS t_delete_rt_project_unpublishings ON rt_project_publishings;
    	}

  	RT_MODELS.each do |rt_model|
  		execute %{
  			DROP TRIGGER IF EXISTS check_request_form_references ON #{rt_model[:table]};
  			DROP TRIGGER IF EXISTS check_project_references ON #{rt_model[:table]};
  			DROP TRIGGER IF EXISTS #{rt_model[:trigger]} ON #{rt_model[:table]};
		    DROP FUNCTION IF EXISTS #{rt_model[:function]} ();
			CREATE FUNCTION #{rt_model[:function]} () RETURNS OPAQUE AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='#{rt_model[:name]}';
			    RETURN OLD;
			END;
			$$ LANGUAGE 'plpgsql';
			
			CREATE TRIGGER #{rt_model[:trigger]} BEFORE DELETE ON #{rt_model[:table]} FOR EACH ROW
				 EXECUTE PROCEDURE #{rt_model[:function]}();
    	}
    end
    RT_MODELS_EXTRA.each do |rt_model|
  		execute %{
  			DROP TRIGGER IF EXISTS check_request_form_references ON #{rt_model[:table]};		
    	}
    end

     PT_MODELS.each do |pt_model|
  		execute %{
  			DROP TRIGGER IF EXISTS check_project_references ON #{pt_model[:tab]};			
    	}
    end

     execute %{
     		DROP FUNCTION IF EXISTS check_request_form_references ();
     		DROP FUNCTION IF EXISTS check_project_references ();
    		DROP TRIGGER IF EXISTS t_delete_request_forms ON request_forms;
		    DROP FUNCTION IF EXISTS delete_request_forms ();
			CREATE FUNCTION delete_request_forms () RETURNS OPAQUE AS '
			BEGIN
			   
			    DELETE FROM req_status_traces WHERE request_form_id=OLD.id;
			    DELETE FROM volun_trackings WHERE request_form_id=OLD.id;
			    DELETE FROM ent_trackings WHERE request_form_id=OLD.id;
			    

			    RETURN OLD;
			END;
			' LANGUAGE 'plpgsql';
			
			CREATE TRIGGER t_delete_request_forms BEFORE DELETE ON request_forms FOR EACH ROW
				EXECUTE PROCEDURE delete_request_forms();
    	}
  end

  def down
    execute %{
		DROP TRIGGER IF EXISTS t_delete_project ON projects;
		DROP FUNCTION IF EXISTS delete_project ();
	}
		
	execute %{
		DROP TRIGGER IF EXISTS t_delete_project_pt ON projects;
		DROP FUNCTION IF EXISTS delete_project_pt ();
	}

  	RT_MODELS.each do |rt_model|
  		execute %{
  			DROP TRIGGER IF EXISTS #{rt_model[:trigger]} ON #{rt_model[:table]};
		    DROP FUNCTION IF EXISTS #{rt_model[:function]} ();
    	}
   	end

   	execute %{
		DROP TRIGGER IF EXISTS t_delete_request_forms ON request_forms;
		DROP FUNCTION IF EXISTS delete_request_forms ();
	}
  end
end

