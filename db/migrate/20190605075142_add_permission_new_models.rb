class AddPermissionNewModels < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE albums_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE albums_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE albums_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE albums_id_seq TO volun_app;

        ALTER TABLE  albums
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  albums TO volun_owner;
        GRANT SELECT ON TABLE  albums TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  albums TO volun_app;

        ALTER TABLE search_forms_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE search_forms_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE search_forms_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE search_forms_id_seq TO volun_app;

        ALTER TABLE  search_forms
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  search_forms TO volun_owner;
        GRANT SELECT ON TABLE  search_forms TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  search_forms TO volun_app;

        ALTER TABLE volun_exports_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE volun_exports_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE volun_exports_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE volun_exports_id_seq TO volun_app;

        ALTER TABLE  volun_exports
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  volun_exports TO volun_owner;
        GRANT SELECT ON TABLE  volun_exports TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  volun_exports TO volun_app;

        ALTER TABLE new_campaings_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE new_campaings_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE new_campaings_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE new_campaings_id_seq TO volun_app;

        ALTER TABLE  new_campaings
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  new_campaings TO volun_owner;
        GRANT SELECT ON TABLE  new_campaings TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  new_campaings TO volun_app;

        ALTER TABLE entity_organizations_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE entity_organizations_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE entity_organizations_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE entity_organizations_id_seq TO volun_app;

        ALTER TABLE  entity_organizations
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  entity_organizations TO volun_owner;
        GRANT SELECT ON TABLE  entity_organizations TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  entity_organizations TO volun_app;
      }
    end
  end
end
