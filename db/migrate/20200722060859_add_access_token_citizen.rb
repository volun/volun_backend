class AddAccessTokenCitizen < ActiveRecord::Migration[5.1]
  def change
    add_column :citizens, :access_token, :string
  end
end
