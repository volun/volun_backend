class DropCheckPrExtensable < ActiveRecord::Migration[6.1]
  def change
    execute %{ALTER TABLE projects  DROP CONSTRAINT pt_extendable_must_be_consistent}    
  end
end
