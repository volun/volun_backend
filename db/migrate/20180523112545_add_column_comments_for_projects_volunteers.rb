class AddColumnCommentsForProjectsVolunteers < ActiveRecord::Migration[5.1]
  def up
    add_column :projects_volunteers,:comments,:string
  end

  def down
    remove_column :projects_volunteers,:comments,:string
  end
end
