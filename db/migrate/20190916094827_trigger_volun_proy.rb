class TriggerVolunProy < ActiveRecord::Migration[5.1]
  def up
    execute %{
      DROP TRIGGER IF EXISTS t_delete_projects_volunteer ON projects_volunteers;
      DROP FUNCTION IF EXISTS delete_projects_volunteer ();
      CREATE FUNCTION delete_projects_volunteer () RETURNS OPAQUE AS $$
      BEGIN
          DELETE FROM volun_proy_meetings WHERE projects_volunteer_id=OLD.id;
        RETURN OLD;
      END;
      $$ LANGUAGE 'plpgsql';
      
      CREATE TRIGGER t_delete_projects_volunteer BEFORE DELETE ON projects_volunteers FOR EACH ROW
        EXECUTE PROCEDURE delete_projects_volunteer();
    }
  end

  def down
    execute %{
      DROP TRIGGER IF EXISTS t_delete_projects_volunteer ON projects_volunteers;
      DROP FUNCTION IF EXISTS delete_projects_volunteer ();
    }
  end
end
