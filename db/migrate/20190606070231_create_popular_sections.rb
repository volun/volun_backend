class CreatePopularSections < ActiveRecord::Migration[5.1]
  def change
    create_table :popular_sections do |t|
      t.string :sections
      t.integer :n_visits
      t.timestamps null: false
    end
  end
end
