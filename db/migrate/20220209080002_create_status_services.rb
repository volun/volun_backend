class CreateStatusServices < ActiveRecord::Migration[6.1]
  def change
    create_table :status_services do |t|
      t.string :name, null: false
      t.string :code, null: false, unique: true
      t.timestamps
    end
  end
end
