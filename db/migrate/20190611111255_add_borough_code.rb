class AddBoroughCode < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :borough_code, :string
  end
end
