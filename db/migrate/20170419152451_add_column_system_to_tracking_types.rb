class AddColumnSystemToTrackingTypes < ActiveRecord::Migration[5.1]
  def change
    add_column :tracking_types, :system, :boolean, default: false
  end
end
