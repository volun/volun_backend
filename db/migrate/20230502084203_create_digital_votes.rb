class CreateDigitalVotes < ActiveRecord::Migration[6.1]
  def change
    create_table :digital_votes do |t|
      t.references :votable, polymorphic: true, index: true
      t.references :userable, polymorphic: true, index: true
      t.integer :value, null: false, inclusion: [-1,0,1]

      t.timestamps
    end
  end
end
