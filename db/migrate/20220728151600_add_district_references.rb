class AddDistrictReferences < ActiveRecord::Migration[6.1]
  def change
    add_reference :projects, :district
    add_reference :addresses, :district
    add_reference :boroughts, :district
    add_reference :citizen_services, :district
    add_reference :rt_volunteer_subscribes, :district
    add_reference :rt_project_publishings, :district
    add_reference :rt_volunteer_amendments, :district
    add_reference :rt_volunteers_demands, :district
  end
end
