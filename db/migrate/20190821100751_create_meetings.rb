class CreateMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :meetings do |t|
      t.references :project, index: true, foreign_key: true
      t.string :name
      t.date :date
      t.string :hour 
      t.string :place
      
      t.timestamps null: false
    end
  end
end
