class RemoveBoroughtsDuplicated < ActiveRecord::Migration[6.1]
  def change
    # remove_column :addresses, :borough
    # remove_column :addresses, :borough_code
    remove_column :citizen_services, :borought
    remove_column :citizen_services, :borought_code
    remove_column :rt_project_publishings, :borough
    remove_column :rt_volunteer_amendments, :borough
    remove_column :rt_volunteers_demands, :borough
  end
end
