class AddColumnPublishChartProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :publish_chart, :boolean, default: false
  end
end
