class ChangeViewManagers < ActiveRecord::Migration[6.1]
  def change
    execute %{
      drop view volun.view_list_managers;
      CREATE OR REPLACE VIEW volun.view_list_managers
AS SELECT m.id,
    m.name,
    m.last_name,
    m.last_name_alt,
    m.phone_number,
    m.login,
    m.active,
    ( SELECT
                CASE r.description
                    WHEN 'super_admin'::text THEN 'Super Administrador'::text
                    WHEN 'admin'::text THEN 'Administrador'::text
                    WHEN 'internal_staff'::text THEN 'Personal interno'::text
                    WHEN 'external_staff'::text THEN 'Personal externo'::text
                    WHEN 'special_trust'::text THEN 'Especial confianza'::text
                    WHEN 'digital_manage'::text THEN 'Gestión digital moderador'::text
                    ELSE ''::text
                END AS "case"
           FROM roles r
          WHERE r.id = m.role_id) AS role,
    m.alias_name
   FROM managers m;
    }
  end
end
