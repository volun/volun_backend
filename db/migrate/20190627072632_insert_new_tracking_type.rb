class InsertNewTrackingType < ActiveRecord::Migration[5.1]
  def change
    TrackingType.create(name: "Reunión de voluntarios", active: true, tracking_types_type: "Project")
    TrackingType.create(name: "Reunión de Coordinación", active: true, tracking_types_type: "Project")
    TrackingType.create(name: "Incidencias", active: true, tracking_types_type: "Project")
    TrackingType.create(name: "Valoración", active: true, tracking_types_type: "Project")
    TrackingType.create(name: "Otras coordinaciones", active: true, tracking_types_type: "Project")
  end
end
