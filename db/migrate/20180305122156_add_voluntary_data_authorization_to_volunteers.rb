class AddVoluntaryDataAuthorizationToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :cession_of_personal_data, :boolean, default: false
    add_column :volunteers, :publication_image_ayto, :boolean, default: false
    add_column :volunteers, :publication_image_ooaa, :boolean, default: false
    add_column :volunteers, :publication_image_social_network, :boolean, default: false
    add_column :volunteers, :publication_image_media, :boolean, default: false
  end
end
