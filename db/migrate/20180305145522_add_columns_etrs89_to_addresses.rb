class AddColumnsEtrs89ToAddresses < ActiveRecord::Migration[5.1]
  def change
    add_column :addresses, :xetrs89, :string
    add_column :addresses, :yetrs89, :string
  end
end
