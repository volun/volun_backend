class CreateVirtualCommunityComponents < ActiveRecord::Migration[6.1]
  def change
    create_table :virtual_community_components do |t|
      t.references :virtual_community_component_type, foreign_key: true,index: { name: :component_type}
      t.references :virtual_community_component,  foreign_key: true, index: {name: :component_parent}
      t.string :title, limit: 100
      t.string :subtitle, limit: 300
      t.text :content
      t.boolean :button_active, default: true
      t.string :button_title, limit: 50
      t.string :button_alt_title, limit: 150
      t.attachment :image_header
      t.integer :order, index: {unique: true}

      t.timestamps
    end
  end
end
