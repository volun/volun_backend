class CreateSearchForms < ActiveRecord::Migration[5.1]
  def change
    create_table :search_forms do |t|
      t.references :user, index: true, foreign_key: true
      t.json :content
      t.text :type
      t.timestamps null: false
    end
  end
end
