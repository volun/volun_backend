class AddPermissionShiftDefinitions < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE shift_definitions OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE shift_definitions_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE shift_definitions_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE shift_definitions_id_seq TO volun_app;

        ALTER TABLE  shift_definitions
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  shift_definitions TO volun_owner;
        GRANT SELECT ON TABLE  shift_definitions TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  shift_definitions TO volun_app;

       }
    end
  end
end
