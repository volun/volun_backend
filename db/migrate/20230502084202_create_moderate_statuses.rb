class CreateModerateStatuses < ActiveRecord::Migration[6.1]
  def change
    create_table :moderate_statuses do |t|
      t.string :title, null: false
      t.string :code, null: false
      t.boolean :active, default: true    

      t.timestamps
    end
  end
end
