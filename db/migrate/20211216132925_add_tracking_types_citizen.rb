class AddTrackingTypesCitizen < ActiveRecord::Migration[6.1]
  def change
    TrackingType.create(name: "Cancelación del servicio al ciudadano", active: true, tracking_types_type: "CitizenService", alias_name: "cancel_service")
    TrackingType.create(name: "Aceptación del servicio al ciudadano", active: true, tracking_types_type: "CitizenService", alias_name: "accept_service")
    TrackingType.create(name: "Rechazo del servicio al ciudadano", active: true, tracking_types_type: "CitizenService", alias_name: "reject_service")
    TrackingType.create(name: "Finalización del servicio al ciudadano", active: true, tracking_types_type: "CitizenService", alias_name: "finished_service")

  end
end
