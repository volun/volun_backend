class CreateChannel < ActiveRecord::Migration[6.1]
  def change
    create_table :channels do |t|
      t.string :name, null: false,unique: true
      t.string :code, null: false,unique: true
      t.boolean :active,:default=> true
      
      t.timestamps
    end
  end
end
