class DeleteIndex < ActiveRecord::Migration[5.1]
  def change
    remove_index :citizens, :code_ci360 if index_exists?(:citizens, :code_ci360)
    remove_index :volunteers, :code_ci360 if index_exists?(:volunteers, :code_ci360)
  end
end
