class AddAutoNumIdVolunteer < ActiveRecord::Migration[5.1]
  def up
    add_column :volunteers, :id_autonum, :integer

    execute %{
      DROP TRIGGER IF EXISTS t_auto_num_volunteer ON volunteers;
      DROP FUNCTION IF EXISTS auto_num_volunteer ();
      CREATE FUNCTION auto_num_volunteer () RETURNS OPAQUE AS $$
      BEGIN
        UPDATE volunteers SET id_autonum = (SELECT MAX(id_autonum)+1 FROM volunteers) WHERE id = NEW.id;
      RETURN NEW;
      END;
      $$ LANGUAGE 'plpgsql';

      CREATE TRIGGER t_auto_num_volunteer BEFORE INSERT ON volunteers FOR EACH ROW
      EXECUTE PROCEDURE auto_num_volunteer();
    }
  end

  def down
    execute %{
      DROP TRIGGER IF EXISTS t_auto_num_volunteer ON citizens;
      DROP FUNCTION IF EXISTS auto_num_volunteer ();
    }
    remove_column :volunteers, :id_autonum
  end
end
