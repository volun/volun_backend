class AddDeathField < ActiveRecord::Migration[6.1]
  def up
    add_column :volunteers, :is_death, :boolean, default: false
    add_column :volunteers, :padron_reason, :string
    add_column :citizens, :is_death, :boolean, default: false
    add_column :citizens, :padron_reason, :string
  end

  def down
    remove_column :volunteers, :is_death
    remove_column :volunteers, :padron_reason
    remove_column :citizens, :is_death
    remove_column :citizens, :padron_reason
  end
end
