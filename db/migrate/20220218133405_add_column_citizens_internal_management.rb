class AddColumnCitizensInternalManagement < ActiveRecord::Migration[6.1]
  def change
    add_column :citizens, :internal_management, :boolean, :default => false
  end
end
