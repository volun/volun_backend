class RemoveIdNumberTypeForeignKeyFromVolunteers < ActiveRecord::Migration[6.1]
  def change
    remove_foreign_key :volunteers, :id_number_types
  end
end
