class AddColsCitizens < ActiveRecord::Migration[5.1]
  def change
    add_reference :citizens, :address, index: :true, foreign_key: true
    add_column :citizens, :terms_of_service, :boolean
  end
end
