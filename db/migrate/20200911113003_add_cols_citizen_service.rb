class AddColsCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :description, :string
    add_column :citizen_services, :place, :string
    add_column :citizen_services, :additional_data, :string
    add_column :citizen_services, :borought_code, :string
  end
end
