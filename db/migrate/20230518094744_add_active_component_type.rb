class AddActiveComponentType < ActiveRecord::Migration[6.1]
  def up
    add_column :virtual_community_component_types, :active, :boolean, default: true
  end

  def down
    remove_column :virtual_community_component_types, :active
  end
end
