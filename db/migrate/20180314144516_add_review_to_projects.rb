class AddReviewToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :review_end_project, :boolean, default: false
  end
end
