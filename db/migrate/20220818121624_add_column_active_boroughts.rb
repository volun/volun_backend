class AddColumnActiveBoroughts < ActiveRecord::Migration[6.1]
  def change
    add_column :boroughts, :active, :boolean, default: true
  end
end
