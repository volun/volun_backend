class AddNumberOfActivitiesToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :number_of_activities, :integer
  end
end
