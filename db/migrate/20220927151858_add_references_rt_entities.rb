class AddReferencesRtEntities < ActiveRecord::Migration[6.1]
  def change
    add_reference :rt_activity_publishings, :entity, index: true, foreign_key: true
    add_reference :rt_others, :entity, index: true, foreign_key: true
    add_reference :rt_project_publishings, :entity, index: true, foreign_key: true
    add_reference :rt_volunteers_demands, :entity, index: true, foreign_key: true
  end
end
