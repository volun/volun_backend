class AddPermissionsMeetings < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE meetings OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE meetings_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE meetings_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE meetings_id_seq TO volun_app;

        ALTER TABLE  meetings
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  meetings TO volun_owner;
        GRANT SELECT ON TABLE  meetings TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  meetings TO volun_app;

       }
    end
  end
end
