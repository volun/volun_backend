class CreateDigitalTopics < ActiveRecord::Migration[6.1]
  def change
    create_table :digital_topics do |t|
      t.references :project, index: true, foreign_key: true
      t.string :title, null: false
      t.string :body, null: false
      t.boolean :active, default: true
      t.references :moderate_status, index: true, foreign_key: true
      t.references :moderable, polymorphic: true, index: true
      t.references :userable, polymorphic: true, index: true
      

      t.timestamps
    end
  end
end
