class AddInitialPassToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :initial_pass, :string
  end
end
