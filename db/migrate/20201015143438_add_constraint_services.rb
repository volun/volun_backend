class AddConstraintServices < ActiveRecord::Migration[5.1]
  def change
    add_index :services,:code, :unique => true
    add_index :services,:name, :unique => true
  end
end
