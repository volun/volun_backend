class AddColumnVolunteersAdult < ActiveRecord::Migration[6.1]
  def up
    add_column :volunteers, :is_adult,:boolean,:default => false
  end

  def down
    remove_column :volunteers, :is_adult
  end
end
