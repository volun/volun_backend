class RemoveProjectTypeFromProjects < ActiveRecord::Migration[6.1]
  def change
    remove_reference :projects, :project_type, index: true, foreign_key: true
  end
end
