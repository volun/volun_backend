class AddPermissionsAhoy < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        GRANT SELECT, INSERT ON TABLE  ahoy_events TO volun_internet_app;

        GRANT SELECT, INSERT ON TABLE  ahoy_visits TO volun_internet_app;
       }
    end
  end
end
