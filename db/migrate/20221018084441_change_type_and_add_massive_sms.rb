class ChangeTypeAndAddMassiveSms < ActiveRecord::Migration[6.1]
  def change
    add_column :massive_sms_historics, :specific, :string
    rename_column :massive_sms_historics, :citizen_ids, :records_ids
  end
end
