class RemoveProjectIdFromVolunAssessments < ActiveRecord::Migration[5.1]
  def change
    remove_column :volun_assessments, :project_id, :int
  end
end
