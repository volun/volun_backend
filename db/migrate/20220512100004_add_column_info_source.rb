class AddColumnInfoSource < ActiveRecord::Migration[6.1]
  def change
    add_reference :rt_volunteer_project_subscribes, :info_source, foreign_key: false
  end
end
