class AddColumnsMassiveEmail < ActiveRecord::Migration[5.1]
  def up
    add_column :massive_email_historics, :emails, :text
    add_column :massive_email_historics, :filters, :text
    add_column :massive_email_historics, :volun_ids, :text
    add_column :massive_email_historics, :title, :string
    add_column :massive_email_historics, :content, :text
  end

  def down
    remove_column :massive_email_historics, :emails
    remove_column :massive_email_historics, :filters
    remove_column :massive_email_historics, :volun_ids
    remove_column :massive_email_historics, :title
    remove_column :massive_email_historics, :content
  end
end
