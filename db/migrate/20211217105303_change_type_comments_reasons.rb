class ChangeTypeCommentsReasons < ActiveRecord::Migration[6.1]
  def change
    remove_column :citizen_services, :reason_manage_canceled
    add_column :citizen_services, :reason_manage_canceled, :string
  end
end
