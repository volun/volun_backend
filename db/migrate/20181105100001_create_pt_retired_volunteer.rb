class CreatePtRetiredVolunteer < ActiveRecord::Migration[5.1]
  def change
  	execute "DROP TABLE IF EXISTS pt_retired_volunteers"
    create_table :pt_retired_volunteers do |t|
      t.text :notes

      t.timestamps null: false
    end
  end
end
