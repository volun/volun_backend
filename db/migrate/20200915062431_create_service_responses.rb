class CreateServiceResponses < ActiveRecord::Migration[5.1]
  def change
    create_table :service_responses do |t|
      t.references :citizen_service, foreign_key: true
      t.references :service_question, foreign_key: true
      t.string :response

      t.timestamps null: false
    end
  end
end
