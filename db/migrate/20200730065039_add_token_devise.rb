class AddTokenDevise < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :authentication_token, :text
    add_column :users, :authentication_token_created_at, :datetime

    add_index :users, :authentication_token, unique: true


    add_column :citizens, :authentication_token, :text
    add_column :citizens, :authentication_token_created_at, :datetime

    add_index :citizens, :authentication_token, unique: true
  end
end
