class CreateEntityOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :entity_organizations do |t|
      t.string :name, null: false
      t.string :person_name
      t.string :phone
      
      t.timestamps null: false
    end
  end
end
