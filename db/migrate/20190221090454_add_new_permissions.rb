class AddNewPermissions < ActiveRecord::Migration[5.1]
  def change
    add_column :permissions, :can_clone_project, :boolean, default: false 
  	add_column :permissions, :can_volunteers_allowed, :boolean, default: false 
  	add_column :permissions, :can_publish, :boolean, default: false 
  	add_column :permissions, :can_order, :boolean, default: false 
  end
end
