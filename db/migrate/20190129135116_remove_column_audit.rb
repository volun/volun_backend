class RemoveColumnAudit < ActiveRecord::Migration[5.1]
  def change
    remove_reference :audits, :user, foreign_key: true
  end
end
