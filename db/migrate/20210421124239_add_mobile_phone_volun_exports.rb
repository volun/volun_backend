class AddMobilePhoneVolunExports < ActiveRecord::Migration[5.1]
  def change
    add_column :volun_exports, :mobile_phone_field, :boolean, :default => true
  end
end
