class AddPermissionsForVolunApp < ActiveRecord::Migration[6.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        GRANT UPDATE, INSERT, DELETE ON TABLE citizen_trackings TO volun_app;
        GRANT UPDATE, INSERT, DELETE ON TABLE citizen_service_trackings TO volun_app;
        GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE ws_messages TO volun_app;
       }
    end
  end
end