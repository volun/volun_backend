class RemoveColumnLoggableTypeInAudits < ActiveRecord::Migration[5.1]
  def change
  	remove_column :audits, :loggable_type, :string
  end
end
