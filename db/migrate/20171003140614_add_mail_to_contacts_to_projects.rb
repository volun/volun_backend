class AddMailToContactsToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :email1, :string
    add_column :projects, :email2, :string
  end
end
