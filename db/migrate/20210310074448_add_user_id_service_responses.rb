class AddUserIdServiceResponses < ActiveRecord::Migration[5.1]
  def change
    add_column :service_responses, :user_id, :integer
    add_column :service_responses, :user_type, :string
  end
end
