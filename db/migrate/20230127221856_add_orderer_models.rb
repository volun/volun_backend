class AddOrdererModels < ActiveRecord::Migration[6.1]
  def change
  execute %{
    CREATE OR REPLACE VIEW volun.view_list_areas
AS SELECT id, name, active from areas;

CREATE OR REPLACE VIEW volun.view_list_collectives
AS SELECT id, name, active from collectives;

CREATE OR REPLACE VIEW volun.view_list_collectives
AS SELECT id, name, active from collectives;

CREATE OR REPLACE VIEW volun.view_list_nationalities
AS SELECT id, name, active from nationalities;

CREATE OR REPLACE VIEW volun.view_list_coordinations
AS SELECT id, name, active from coordinations;

CREATE OR REPLACE VIEW volun.view_list_unsubscribe_reasons
AS SELECT id, name, active from unsubscribe_reasons;

CREATE OR REPLACE VIEW volun.view_list_degrees
AS SELECT id, (select dt."name" from degree_types dt where dt.id=d.degree_type_id) as degree_type ,name, active from "degrees" d;

CREATE OR REPLACE VIEW volun.view_list_tracking_types
AS SELECT id, name, active, tt.alias_name, tt."system", tt.tracking_types_type from tracking_types tt;

CREATE OR REPLACE VIEW volun.view_list_traits
AS SELECT id, name, active from traits;

CREATE OR REPLACE VIEW volun.view_list_req_rejection_types
AS SELECT id, name,description,  active from req_rejection_types;

CREATE OR REPLACE VIEW volun.view_list_languages
AS SELECT id, name, active from languages;

CREATE OR REPLACE VIEW volun.view_list_skills
AS SELECT id, name, active from skills;

CREATE OR REPLACE VIEW volun.view_list_professions
AS SELECT id, name, active from professions;

CREATE OR REPLACE VIEW volun.view_list_req_reasons
AS SELECT id, name,description , active from req_reasons;

CREATE OR REPLACE VIEW volun.view_list_volun_profiles
AS SELECT id, name, active from volun_profiles;

CREATE OR REPLACE VIEW volun.view_list_next_points
AS SELECT id, name, description,  active from next_points;

CREATE OR REPLACE VIEW volun.view_list_shift_definitions
AS SELECT id, name, description,  active from shift_definitions;

CREATE OR REPLACE VIEW volun.view_list_info_sources
AS SELECT id, name, active from info_sources;

CREATE OR REPLACE VIEW volun.view_list_services
AS SELECT id, name, description,  active, code from services;

CREATE OR REPLACE VIEW volun.view_list_service_questions
AS SELECT id, description,  active, question_type  from service_questions;

CREATE OR REPLACE VIEW volun.view_list_type_meetings
AS SELECT id, "name",  description,  active  from type_meetings;

CREATE OR REPLACE VIEW volun.view_list_genders
AS SELECT id, "name",   active  from genders;

CREATE OR REPLACE VIEW volun.view_list_districts
AS SELECT id, "name", code, active  from districts;

CREATE OR REPLACE VIEW volun.view_list_provinces
AS SELECT id, "name", code, active  from provinces;

CREATE OR REPLACE VIEW volun.view_list_boroughts
AS SELECT id, "name", code ,(select d.code from districts d where d.id=b.district_id) as district_code, active  from boroughts b;

CREATE OR REPLACE VIEW volun.view_list_channels
AS SELECT id, "name", code, active  from channels;

CREATE OR REPLACE VIEW volun.view_list_managers
AS SELECT id, "name", last_name , last_name_alt, phone_number, login, active,
(select case r.description 
when 'super_admin' then 'Super Administrador'
when 'admin' then 'Administrador'
when 'internal_staff' then 'Personal interno'
when 'external_staff' then 'Personal externo'
when 'special_trust' then 'Especial confianza'
else '' END
from roles r where r.id=m.role_id) as role,alias_name from managers m;

CREATE OR REPLACE VIEW volun.view_list_resources
AS SELECT id, "name",description, active,main  from resources;

CREATE OR REPLACE VIEW volun.view_list_settings
AS SELECT id, key, value  from settings;

CREATE OR REPLACE VIEW volun.view_list_new_campaings
AS SELECT id, title,body,publicable  from new_campaings;

CREATE OR REPLACE VIEW volun.view_list_citizen_services
AS SELECT cs.id,
upper(c."name") as f_citizen_name,
c.id  as f_citizen_id,
upper(c.last_name_1) as f_citizen_last_name_1,
upper(c.last_name_2) as f_citizen_last_name_2,
(select v.id from volunteers v where v.id=cs.volunteer_id) as f_volunteer_id,
(select upper(v."name") from volunteers v where v.id=cs.volunteer_id) as f_volunteer_name,
(select upper(v.last_name) from volunteers v where v.id=cs.volunteer_id) as f_volunteer_last_name_1,
(select upper(v.last_name_alt)  from volunteers v where v.id=cs.volunteer_id) as f_volunteer_last_name_2,
(select coalesce(d.name,'OTROS')  from volunteers v,addresses a, districts d  where v.id=cs.volunteer_id and a.id=v.address_id and a.district_id=d.id) as f_volunteer_district,
(select s.name from services s where s.id=cs.service_id) as type_service,
case when cs.date_request is not null THEN CAST(CONCAT(cs.date_request,' ',cs.hour_request) as timestamp) else null END  as full_date_request,
case when cs.date_accept_request is not null then CAST(CONCAT(cs.date_accept_request,' ',cs.hour_accept_request) as timestamp) else null END as full_date_accept,
(select ss."name" from status_services ss where ss.id=cs.status_service_id or ss.code=cs.status limit 1) as status_name,
(select sv.vote  from service_votes sv where sv.citizen_service_id=cs.id and sv.type_user='Citizen')  as f_citizen_vote,
(select sv.vote  from service_votes sv where sv.citizen_service_id=cs.id and sv.type_user='Volunteer')  as f_volunteer_vote,
cs.canceled_at,
cs.citizen_reason,
cs.rejected_at,
cs.volunteer_reason,
cs.place,
cs.additional_data,
(select b.name from boroughts b where b.id=cs.borought_id) as borought_name,
(select d.name from districts d where d.id=cs.district_id) as district_name,
cs.citizen_id,
cs.volunteer_id,
cs.status,
(select v.email from volunteers v where v.id=cs.volunteer_id) as f_volunteer_email,
c.email as f_citizen_email,
cs.date_accept_request,
cs.date_request,
cs.active,
cs.district_id,
cs.borought_id,
cs.service_id,
date_part('year'::text,age(cast(c.birthday as date)))::int as age_citizen,
c.gender_id,
(select g.name from genders g where g.id=c.gender_id) as gender_name,
c.enabled_app,
c.tester,
c.email_verified,
cs.generated_gi 
from citizen_services cs, citizens c where cs.citizen_id =c.id;

CREATE OR REPLACE VIEW volun.view_list_citizen_service_trackings
AS SELECT id, automatic,coments, 
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where cst.manager_id=m.id) as manager,
cst.tracked_at,
(select tt."name"  from tracking_types tt where tt.id =cst.tracking_type_id) as tracking_type,
(select concat('Servicio (',cs.id,') - ',(select s.name from services s where s.id=cs.service_id))  from citizen_services cs where cs.id=cst.citizen_service_id) as citizen_service,
(select upper(concat(c."name",' ',c.last_name_1  ,' ', c.last_name_2)) from citizens c, citizen_services cs2 where cs2.citizen_id =c.id and cst.citizen_service_id =cs2.id) as citizen,
cst.citizen_service_id
from citizen_service_trackings cst;


CREATE OR REPLACE VIEW volun.view_list_activities
AS SELECT a.id, a.name, a.description, a.active, a.start_date, a.end_date, a.transport, 
(select e."name"  from entities e where e.id=a.entity_id) as entity,
(select p."name"  from projects p where p.id=a.project_id) as project,
a."share",
(select string_agg(ac."name",' ; ')  from areas ac, activities_areas aa where aa.area_id=ac.id and aa.activity_id=a.id) as get_areas,
a.publish,
a.pdf_url,
a.review_end_activity ,
a.entity_id, 
a.area_id 
from activities a;


CREATE OR REPLACE VIEW volun.view_list_entities
AS SELECT e.id, e."name", e.vat_number, e.email, (select et."name"  from entity_types et where et.id= e.entity_type_id) as entity_type, e.active, e.entity_type_id 
from entities e;

CREATE OR REPLACE VIEW volun.view_list_ent_trackings
AS SELECT et.id, (select e."name"  from entities e where e.id=et.entity_id) as entity, et.entity_id, et.tracked_at, et.automatic, et."comments",
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where et.manager_id=m.id) as manager,
et.manager_id, et.request_form_id,
(select rf.id from request_forms rf where rf.id=et.request_form_id) as request_form,
(select tt."name"  from tracking_types tt where tt.id =et.tracking_type_id) as tracking_type,
et.tracking_type_id 
from ent_trackings et;


CREATE OR REPLACE VIEW volun.view_list_citizen_trackings
AS SELECT et.id, (select upper(concat(e."name",' ',e.last_name_1,' ',e.last_name_2))  from citizens e where e.id=et.citizen_id) as citizen, et.citizen_id , et.tracked_at, et.automatic, et.coments ,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where et.manager_id=m.id) as manager,
et.manager_id, 
(select tt."name"  from tracking_types tt where tt.id =et.tracking_type_id) as tracking_type,
et.tracking_type_id 
from citizen_trackings et;

CREATE OR REPLACE VIEW volun.view_list_volun_trackings
AS SELECT et.id, (select upper(concat(e."name",' ',e.last_name ,' ',e.last_name_alt))  from volunteers e where e.id=et.volunteer_id) as volunteer, 
et.volunteer_id, et.project_id  , et.tracked_at, et.automatic, et."comments"  ,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where et.manager_id=m.id) as manager,
(select p."name" from projects p where et.project_id =p.id) as project,
et.manager_id, 
(select tt."name" from tracking_types tt where tt.id =et.tracking_type_id) as tracking_type,
(select rf.id from request_forms rf where rf.id=et.request_form_id) as request_form,
et.request_form_id,
et.tracking_type_id 
from volun_trackings et;


CREATE OR REPLACE VIEW volun.view_list_pro_trackings
AS SELECT et.id,et.project_id  , et.tracked_at, et.automatic, et."comments"  ,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where et.manager_id=m.id) as manager,
(select p."name" from projects p where et.project_id =p.id) as project,
et.manager_id, 
(select tt."name" from tracking_types tt where tt.id =et.tracking_type_id) as tracking_type,
(select rf.id from request_forms rf where rf.id=et.request_form_id) as request_form,
et.request_form_id,
et.tracking_type_id 
from pro_trackings et;

CREATE OR REPLACE VIEW volun.view_list_rt_volunteer_subscribes
AS SELECT  vs.*, coalesce((select d.name from districts d where d.id=vs.district_id),'OTROS') as district, 
(select c.name from channels c where c.id = vs.channel_id) as channel,
(select i.name from info_sources i where i.id = vs.info_source_id) as info_source,
(select p.name from projects p where p.id=vs.project_id) as project,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when vs.phone_number ~ '[6|7]\d{8}' then vs.phone_number
else vs.phone_number_alt end as phone_number_specific,
(select to_char(u.created_at,'DD/MM/YYYY HH24:MI') from users u where u.id=rf.user_id) as volunteer_subscribe_date,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status
from rt_volunteer_subscribes vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::VolunteerSubscribe';

CREATE OR REPLACE VIEW volun.view_list_rt_others
AS SELECT  vs.*,  (select e.name from entities e where e.id=vs.entity_id) as entity,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select 
case when e.phone_number ~ '[6|7]\d{8}' then e.phone_number
else e.phone_number_alt END
from entities e where e.id=vs.entity_id) as entity_phone_number,
(select e.email from entities e where e.id=vs.entity_id) as entity_email
from rt_others vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::Other';

CREATE OR REPLACE VIEW volun.view_list_rt_activity_publishings
AS SELECT  vs.*,  (select e.name from entities e where e.id=vs.entity_id) as entity,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select 
case when e.phone_number ~ '[6|7]\d{8}' then e.phone_number
else e.phone_number_alt END
from entities e where e.id=vs.entity_id) as entity_phone_number,
(select e.email from entities e where e.id=vs.entity_id) as entity_email,
(select to_char(a.created_at,'DD/MM/YYYY HH24:MI')  from activities a where a.id=vs.activity_id) as publishing_date_activity
from rt_activity_publishings vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::ActivityPublishing';

CREATE OR REPLACE VIEW volun.view_list_rt_volunteers_demands
AS SELECT  vs.*,  (select e.name from entities e where e.id=vs.entity_id) as entity,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select 
case when e.phone_number ~ '[6|7]\d{8}' then e.phone_number
else e.phone_number_alt END
from entities e where e.id=vs.entity_id) as entity_phone_number,
(select e.email from entities e where e.id=vs.entity_id) as entity_email
from rt_volunteers_demands vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::VolunteersDemand';

CREATE OR REPLACE VIEW volun.view_list_rt_project_publishings
AS SELECT  vs.*,  (select e.name from entities e where e.id=vs.entity_id) as entity,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select 
case when e.phone_number ~ '[6|7]\d{8}' then e.phone_number
else e.phone_number_alt END
from entities e where e.id=vs.entity_id) as entity_phone_number,
(select e.email from entities e where e.id=vs.entity_id) as entity_email,
(select to_char(p.created_at,'DD/MM/YYYY HH24:MI') from projects p where p.id=vs.project_id) as publish_date_project,
(CONCAT(vs.road_type,'/',vs.road_name,', ',vs.number_type,vs.road_number,', (',
(select p.name from provinces p where p.id=vs.province_id),') ',vs.town,', ',(select b.name from boroughts b where b.id=vs.borought_id),', ',
(select d.name from districts d where d.id=vs.district_id),', ',vs.postal_code) ) as address,
(select p.name from provinces p where p.id=vs.province_id) as province
from rt_project_publishings vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::ProjectPublishing';

CREATE OR REPLACE VIEW volun.view_list_rt_volunteer_appointments
AS SELECT  vs.*,  
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select v.name from volunteers v where v.id=vs.volunteer_id) as name,
(select v.last_name  from volunteers v where v.id=vs.volunteer_id) as last_name,
(select v.last_name_alt  from volunteers v where v.id=vs.volunteer_id) as last_name_alt,
(select v.phone_number  from volunteers v where v.id=vs.volunteer_id) as phone_number,
(select v.phone_number_alt  from volunteers v where v.id=vs.volunteer_id) as phone_number_alt,
(select v.email  from volunteers v where v.id=vs.volunteer_id) as email,
(select a.postal_code  from volunteers v, addresses a  where v.id=vs.volunteer_id and a.id=v.address_id) as postal_code,
coalesce((select d.name  from volunteers v, addresses a, districts d  where v.id=vs.volunteer_id and a.id=v.address_id and d.id=a.district_id),'OTROS') as district,
(select CONCAT(v.name,' ',v.last_name,' ',v.last_name_alt) from volunteers v where v.id=vs.volunteer_id) as volunteer_name,
(select to_char(v.created_at,'DD/MM/YYYY HH24:MI') from volunteers v where v.id=vs.volunteer_id) as volunteer_subscribe_date
from rt_volunteer_appointments vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::VolunteerAppointment';

CREATE OR REPLACE VIEW volun.view_list_rt_volunteer_amendments
AS SELECT  vs.*,  
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select v.name from volunteers v where v.id=vs.volunteer_id) as name,
(select v.last_name  from volunteers v where v.id=vs.volunteer_id) as last_name,
(select v.last_name_alt  from volunteers v where v.id=vs.volunteer_id) as last_name_alt,
coalesce((select d.name  from volunteers v, addresses a, districts d  where v.id=vs.volunteer_id and a.id=v.address_id and d.id=a.district_id),'OTROS') as district,
(select CONCAT(v.name,' ',v.last_name,' ',v.last_name_alt) from volunteers v where v.id=vs.volunteer_id) as volunteer_name,
(select to_char(v.created_at,'DD/MM/YYYY HH24:MI') from volunteers v where v.id=vs.volunteer_id) as volunteer_subscribe_date
from rt_volunteer_amendments vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::VolunteerAmendment';


CREATE OR REPLACE VIEW volun.view_list_rt_volunteer_project_subscribes
AS SELECT  vs.*,
(select i.name from info_sources i where i.id = vs.info_source_id) as info_source,
(select p.name from projects p where p.id=vs.project_id) as project,
rf.manager_id, rf.status_date, rf.req_status_id,
(select rs.description from req_statuses rs where rs.id= rf.req_status_id) as req_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as manager,
rf.rt_extendable_type,
case when vs.phone_number ~ '[6|7]\d{8}' then vs.phone_number
else vs.phone_number_alt end as phone_number_specific,
(select to_char(u.created_at,'DD/MM/YYYY HH24:MI') from users u where u.id=rf.user_id) as volunteer_subscribe_date,
case when  rf.req_status_id in (3,4,2) then (rf.status_date::date-rf.created_at::date)
else (NOW()::date-rf.created_at::date) end as days_passed,
to_char(rf.status_date,'DD/MM/YYYY HH24:MI') as get_date_status,
(select upper(concat(m."name",' ',m.last_name,' ', m.last_name_alt)) from managers m where rf.manager_id=m.id) as get_manager,
(select 
case when rs.description = 'pending' then 'Pendiente'
when rs.description = 'processing' then 'En trámite'
when rs.description = 'approved' then 'Aceptada'
when rs.description ='rejected' then 'Rechazada'
when rs.description = 'appointed' then 'Citado/a'
end from req_statuses rs where rs.id= rf.req_status_id) as get_status,
(select to_char(v.created_at,'DD/MM/YYYY HH24:MI') from volunteers v where v.id=vs.volunteer_id) as volunteer_subscribe_date_form,
coalesce((select d.name  from volunteers v, addresses a, districts d  where v.id=vs.volunteer_id and a.id=v.address_id and d.id=a.district_id),'OTROS') as district_name
from rt_volunteer_project_subscribes vs, request_forms rf where vs.id=rf.rt_extendable_id and rf.rt_extendable_type ='Rt::VolunteerProjectSubscribe';

CREATE OR REPLACE VIEW volun.view_list_citizens
as select c.*,
date_part('year'::text,age(cast(c.birthday as date)))::int as age_name,
(select string_agg(distinct s.name ,' ; ')  from citizen_services cs, services s where cs.service_id =s.id and cs.citizen_id=c.id) as type_services,
(select count(1) from  citizen_services cs where cs.citizen_id=c.id) as count_services,
(select count(1) from  citizen_services cs where cs.citizen_id=c.id and cs.status_service_id in (select ss.id from status_services ss where ss.code='finished')) as count_services_finish,
(select count(1) from  citizen_services cs where cs.citizen_id=c.id and cs.status_service_id in (select ss.id from status_services ss where ss.code in ('prioritary','active'))) as count_services_pending,
(select count(1) from  citizen_services cs where cs.citizen_id=c.id and cs.status_service_id in (select ss.id from status_services ss where ss.code='accept')) as count_services_accept,
(select string_agg(distinct cast(cs.service_id as varchar),',') from citizen_services cs where  cs.citizen_id=c.id)  as service_id,
(select g.name from genders g where g.id=c.gender_id) as gender,
(select a.district_id from addresses a where a.id=c.address_id) as district_id 
from citizens c;

CREATE OR REPLACE VIEW volun.view_list_volunteers
as select v.*,
date_part('year'::text,age(cast(v.birth_date  as date)))::int as age_name,
(select g.name from genders g where g.id=v.gender_id) as gender_convert,
(select a.town from addresses a where a.id = v.address_id) as town,
(select a.postal_code from addresses a where a.id = v.address_id) as postal_code,
coalesce((select d.name  from addresses a, districts d  where a.id=v.address_id and d.id=a.district_id ),'OTROS') as district_custom,
(select count(1) from projects_volunteers pv where pv.active=true and v.id=pv.volunteer_id) as num_projects_active,
coalesce((select d."name"  from addresses a, districts d where d.id=a.district_id  and a.id = v.address_id),'OTROS') as district,
coalesce((select d.code::int from addresses a, districts d where d.id=a.district_id  and a.id = v.address_id),22) as district_id,
(case when 
v.active = true and v.available = true and (v.availability_date is not null and v.availability_date::date <= (NOW() + INTERVAL '7 day')::date)
then true
else false end) as notice_available_column,
(case when
(v.birth_date is not null and date_part('year'::text,age(cast(v.birth_date  as date)))::int < 18) or 
(v.cession_of_personal_data = false or v.publication_image_ayto = false or v.publication_image_ooaa = false or v.publication_image_social_network = false or v.publication_image_media = false or v.vocne =true or 
coalesce((select upper(s.name) = 'NO APTO' from statuses s where s.id=v.status_id), false))
then true
else false end) as not_autorized,
(select string_agg(a2."name",',') from areas a2, areas_volunteers av where av.area_id=a2.id and av.volunteer_id=v.id) as get_areas,
(select string_agg(c."name",',') from collectives c, collectives_volunteers cv where cv.collective_id =c.id and cv.volunteer_id=v.id) as get_collectives,
(select string_agg(is2."name",',') from info_sources is2 where is2.id =v.info_source_id) as get_info_source,
(select string_agg(vp."name",',') from volun_profiles vp, volun_profiles_volunteers vpv  where vpv.volun_profile_id = vp.id  and vpv.volunteer_id =v.id) as get_volun_profiles,
to_char(v.subscribe_date ,'DD/MM/YYYY HH24:MI') as subscribe_date_format
from volunteers v;

      }

  end
end
