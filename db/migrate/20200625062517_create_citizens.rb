class CreateCitizens < ActiveRecord::Migration[5.1]
  def change
    create_table :citizens do |t|
      t.string :name
      t.string :last_name_1
      t.string :last_name_2
      t.string :phone
      t.string :email
      t.string :document
      t.string :document_type
      t.string :birthday
      t.string :country
      t.string :academic_info
      t.string :gender
      t.integer :info_source_id
      t.integer :people_at_home
      t.date :start_date
      t.date :last_sign_in
      t.string :code_ci360
      t.timestamps null: false
    end
  end
end
