class AddCitizenServicesFields < ActiveRecord::Migration[6.1]
  def up
    add_column :citizens, :padron, :boolean, :default => true
    add_column :citizen_services, :send_sms_citizen, :boolean, default: false
    add_column :citizen_services, :reason_manage_canceled, :string
    add_column :citizen_services, :reason_manage_canceled_at, :datetime
    add_column :citizen_services, :reason_manage_active_at, :datetime
    add_column :citizen_services, :indications, :string
    add_column :citizen_services, :warning_message_volunteer, :string
  end

  def down
    remove_column :citizens, :padron
    remove_column :citizen_services, :send_sms_citizen
    remove_column :citizen_services, :reason_manage_canceled
    remove_column :citizen_services, :reason_manage_canceled_at
    remove_column :citizen_services, :reason_manage_active_at
    remove_column :citizen_services, :indications
    remove_column :citizen_services, :warning_message_volunteer
  end
end
