class AddReasonCitizenCitizenService < ActiveRecord::Migration[5.1]
  def change
    rename_column :citizen_services, :reason, :volunteer_reason
    add_column :citizen_services, :citizen_reason, :string
  end
end
