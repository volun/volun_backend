class AddReferencesVolunteers < ActiveRecord::Migration[6.1]
  def change
    add_reference :rt_volunteer_project_subscribes, :volunteer, index: true, foreign_key: true
  end
end
