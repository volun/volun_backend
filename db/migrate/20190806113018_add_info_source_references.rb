class AddInfoSourceReferences < ActiveRecord::Migration[5.1]
  def change
    add_reference :rt_volunteer_subscribes, :info_source, foreign_key: false
  end
end
