class AddColumnNameToResources < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :name, :string
    add_index :resources, :name, unique: true
  end
end
