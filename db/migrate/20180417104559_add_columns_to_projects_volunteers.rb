class AddColumnsToProjectsVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :projects_volunteers, :active, :boolean, :default => true
    add_column :projects_volunteers, :subscribe_date, :date
    add_column :projects_volunteers, :unsubscribe_date, :date
  end
end
