class CheckUniqueCid360 < ActiveRecord::Migration[5.1]
  def change
    add_index :citizens, :code_ci360, :unique => true
    add_index :citizens, :code_cid360_autentication, :unique => true

    execute "UPDATE volunteers set code_ci360 = null;"

    add_index :volunteers, :code_ci360, :unique => true
    add_index :volunteers, :code_cid360_autentication, :unique => true
  end
end
