class RemoveColumnsToRtVolunteerSubscribes < ActiveRecord::Migration[5.1]
  def change
    remove_column :rt_volunteer_subscribes, :publish_pictures, :boolean
    remove_column :rt_volunteer_subscribes, :annual_survey, :boolean
  end
end
