class RenameColumnDigitalFaq < ActiveRecord::Migration[6.1]
  def change
    rename_column :digital_faqs, :body, :description
  end
end
