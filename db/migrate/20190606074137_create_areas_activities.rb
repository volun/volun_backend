class CreateAreasActivities < ActiveRecord::Migration[5.1]
  def change
    create_join_table :areas,:activities  do |t|
      t.index [:area_id, :activity_id]
      t.index [:activity_id,:area_id]
    end
  end
end
