class AddColumnsCid360Autentication < ActiveRecord::Migration[5.1]
  def change
    add_column :citizens, :code_cid360_autentication, :string
    add_column :citizens, :validation_cid360, :string

    add_column :volunteers, :code_cid360_autentication, :string
    add_column :volunteers, :validation_cid360, :string
  end
end
