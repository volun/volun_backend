class ChangeNameVolunteersDistricts < ActiveRecord::Migration[5.1]
  def self.up
    rename_table :volunteers_districts, :districts_volunteers
  end

  def self.down
    rename_table :districts_volunteers, :volunteers_districts
  end
end
