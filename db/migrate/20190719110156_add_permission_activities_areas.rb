class AddPermissionActivitiesAreas < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        
        ALTER TABLE  activities_areas
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  activities_areas TO volun_owner;
        GRANT SELECT ON TABLE  activities_areas TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  activities_areas TO volun_app;

       }
    end
  end
end
