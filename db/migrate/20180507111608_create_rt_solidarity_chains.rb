class CreateRtSolidarityChains < ActiveRecord::Migration[5.1]
  def change
    execute "DROP TABLE IF EXISTS rt_solidarity_chains"
    create_table :rt_solidarity_chains do |t|
      t.string :full_name, null: false
      t.string :phone_number, null: false
      t.string :email, null: false
      t.text :action_description, null: false
      t.string :url_image
      t.string :alias
      t.boolean :data_protection, null: false, default: false
      t.datetime :created_at, null: false

      t.timestamps null: false
    end
  end
end
