class AddColumnsDebatesFaqProjects < ActiveRecord::Migration[6.1]
  def up
    add_column :projects,:permit_debate,:boolean, default: false
    add_column :projects,:permit_faq,:boolean, default: false
  end

  def down
    remove_column :projects,:permit_debate
    remove_column :projects,:permit_faq
  end
end
