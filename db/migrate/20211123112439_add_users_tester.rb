class AddUsersTester < ActiveRecord::Migration[6.1]
  def up
    add_column :citizens, :tester, :boolean, :default => false
    add_column :volunteers, :tester, :boolean, :default => false
  end

  def down
    remove_column :citizens, :tester
    remove_column :volunteers, :tester
  end
end
