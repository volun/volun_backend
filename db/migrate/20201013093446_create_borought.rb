class CreateBorought < ActiveRecord::Migration[5.1]
  def change
    create_table :boroughts do |t|
      t.string :name
      t.string :code
      t.string :district_code

      t.timestamps null: false
    end
  end
end
