class AddDefaultChannel < ActiveRecord::Migration[6.1]
  def change
    change_column_default :rt_volunteer_subscribes, :channel_id, from: nil, to: 1
  end
end
