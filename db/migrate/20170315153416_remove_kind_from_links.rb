class RemoveKindFromLinks < ActiveRecord::Migration[5.1]
  def change
    remove_column :links, :kind, :integer
  end
end
