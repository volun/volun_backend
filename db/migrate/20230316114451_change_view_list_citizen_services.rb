class ChangeViewListCitizenServices < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_citizen_services
AS SELECT cs.id,
    upper(c.name::text) AS f_citizen_name,
    c.id AS f_citizen_id,
    upper(c.last_name_1::text) AS f_citizen_last_name_1,
    upper(c.last_name_2::text) AS f_citizen_last_name_2,
    ( SELECT v.id
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_id,
    ( SELECT upper(v.name::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_name,
    ( SELECT upper(v.last_name::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_last_name_1,
    ( SELECT upper(v.last_name_alt::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_last_name_2,
    ( SELECT COALESCE(d.name, 'OTROS'::character varying) AS "coalesce"
           FROM volunteers v,
            addresses a,
            districts d
          WHERE v.id = cs.volunteer_id AND a.id = v.address_id AND a.district_id = d.id) AS f_volunteer_district,
    ( SELECT s.name
           FROM services s
          WHERE s.id = cs.service_id) AS type_service,
        CASE
            WHEN cs.date_request IS NOT NULL THEN concat(cs.date_request, ' ', cs.hour_request)::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_request,
        CASE
            WHEN cs.date_accept_request IS NOT NULL THEN concat(cs.date_accept_request, ' ', cs.hour_accept_request)::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_accept,
    ( SELECT ss.name
           FROM status_services ss
          WHERE ss.id = cs.status_service_id OR ss.code::text = cs.status::text
         LIMIT 1) AS status_name,
    ( SELECT sv.vote
           FROM service_votes sv
          WHERE sv.citizen_service_id = cs.id AND sv.type_user::text = 'Citizen'::text) AS f_citizen_vote,
    ( SELECT sv.vote
           FROM service_votes sv
          WHERE sv.citizen_service_id = cs.id AND sv.type_user::text = 'Volunteer'::text) AS f_volunteer_vote,
    cs.canceled_at,
    cs.citizen_reason,
    cs.rejected_at,
    cs.volunteer_reason,
    cs.place,
    cs.additional_data,
    ( SELECT b.name
           FROM boroughts b
          WHERE b.id = cs.borought_id) AS borought_name,
    ( SELECT d.name
           FROM districts d
          WHERE d.id = cs.district_id) AS district_name,
    cs.citizen_id,
    cs.volunteer_id,
    cs.status,
    ( SELECT v.email
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_email,
    c.email AS f_citizen_email,
    cs.date_accept_request,
    cs.date_request,
    cs.active,
    cs.district_id,
    cs.borought_id,
    cs.service_id,
    date_part('year'::text, age(c.birthday::date::timestamp with time zone))::integer AS age_citizen,
    c.gender_id,
    ( SELECT g.name
           FROM genders g
          WHERE g.id = c.gender_id) AS gender_name,
    c.enabled_app,
    c.tester,
    c.email_verified,
    cs.generated_gi,
    ( SELECT COALESCE(v.id_autonum, v.id) AS "coalesce"
           FROM volunteers v
          WHERE cs.volunteer_id = v.id) AS f_volunteer_id_autonum,
      ( SELECT s.is_digital 
           FROM services s
          WHERE s.id = cs.service_id) AS is_digital
   FROM citizen_services cs,
    citizens c
  WHERE cs.citizen_id = c.id;
    }
  end
end
