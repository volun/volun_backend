class AddColumnFatherProject < ActiveRecord::Migration[6.1]
  def up
    add_column :projects, :father_id, :integer
  end

  def down
    remove_column :projects, :father_id
  end
end
