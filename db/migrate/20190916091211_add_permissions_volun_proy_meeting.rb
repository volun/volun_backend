class AddPermissionsVolunProyMeeting < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE volun_proy_meetings OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE volun_proy_meetings_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE volun_proy_meetings_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE volun_proy_meetings_id_seq TO volun_app;

        ALTER TABLE  volun_proy_meetings
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  volun_proy_meetings TO volun_owner;
        GRANT SELECT ON TABLE  volun_proy_meetings TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  volun_proy_meetings TO volun_app;

       }
    end
  end
end
