class AddVerifiedEmailToCitizens < ActiveRecord::Migration[5.1]
  def change
    add_column :citizens, :email_verified, :boolean, default: false
  end
end
