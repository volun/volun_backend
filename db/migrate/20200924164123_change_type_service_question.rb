class ChangeTypeServiceQuestion < ActiveRecord::Migration[5.1]
  def change
    
    change_column :service_questions, :question_type, :string
  end
end
