class AddTypeAuthorMessage < ActiveRecord::Migration[6.1]
  def up
    add_column :ws_messages,:type_author,:string
  end

  def down
    remove_column :ws_messages,:type_author
  end
end
