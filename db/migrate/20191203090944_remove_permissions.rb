class RemovePermissions < ActiveRecord::Migration[5.1]
  def change
   unless column_exists? :permissions, :section
      add_column :permissions, :section, :text
    end
    unless column_exists? :permissions, :data
      add_column :permissions, :data, :jsonb, null: false, default: '{}'
      add_index  :permissions, :data, using: :gin
    end
  end
end
