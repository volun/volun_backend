class AddViewListComponentType < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_virtual_community_component_types
      AS SELECT m.id,
          m.title,
          m.code,
          m.active
         FROM virtual_community_component_types m;
    }
  end
end
