class AddMarket < ActiveRecord::Migration[5.1]
  def up
    add_column :citizens, :market, :boolean, :default => false
    add_column :volunteers, :market, :boolean, :default => false
  end

  def down
    remove_column :citizens, :market, :boolean, :default => false
    remove_column :volunteers, :market, :boolean, :default => false
  end
end
