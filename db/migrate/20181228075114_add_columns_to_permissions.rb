class AddColumnsToPermissions < ActiveRecord::Migration[5.1]
  def change
  	add_column :permissions, :can_process_request_form, :boolean, default: false 
  	add_column :permissions, :can_pre_approve_request_form, :boolean, default: false 
  	add_column :permissions, :can_reject_request_form, :boolean, default: false 
  	add_column :permissions, :can_mark_request_form_as_processing, :boolean, default: false 
  	add_column :permissions, :can_mark_request_form_as_pending, :boolean, default: false 
  	add_column :permissions, :can_mark_request_form_as_appointed, :boolean, default: false 
  end
end
