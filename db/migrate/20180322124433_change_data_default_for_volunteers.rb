class ChangeDataDefaultForVolunteers < ActiveRecord::Migration[5.1]
    def change

      change_column :volunteers, :cession_of_personal_data, :boolean, :default => true
      change_column :volunteers, :publication_image_ayto, :boolean, :default => true
      change_column :volunteers, :publication_image_ooaa, :boolean, :default => true
      change_column :volunteers, :publication_image_social_network, :boolean, :default => true
      change_column :volunteers, :publication_image_ayto, :boolean, :default => true

    end
end
