class CreateNewCampaings < ActiveRecord::Migration[5.1]
  def change
    create_table :new_campaings do |t|
      t.string :title, null: false
      t.text :body
      t.boolean :publicable, :default => false  
      t.timestamps null: false
    end
  end
end
