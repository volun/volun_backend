class AddProvinceReferences < ActiveRecord::Migration[6.1]
  def change
    add_reference :citizens, :province
    add_reference :addresses, :province
    add_reference :rt_project_publishings, :province
    add_reference :rt_volunteer_amendments, :province
    add_reference :rt_volunteers_demands, :province
  end
end
