class AddForeingKeysBoroughts < ActiveRecord::Migration[6.1]
  def change
    # add_foreign_key :addresses, :boroughts
    add_foreign_key :citizen_services, :boroughts
    add_foreign_key :rt_project_publishings, :boroughts
    add_foreign_key :rt_volunteer_amendments, :boroughts
    add_foreign_key :rt_volunteers_demands, :boroughts
  end
end
