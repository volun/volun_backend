class AddSuspendedStatusAndDatesToProjects < ActiveRecord::Migration[6.1]
  def change 
    add_column :projects, :suspended, :boolean, default: false
    add_column :projects, :change_status_at, :date
    add_column :projects, :suspended_at, :datetime
    add_column :projects, :reactivated_at, :datetime
  end
end
