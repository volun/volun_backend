class AddYearToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :year, :integer
    add_index :projects, [:year, :name], unique: true
    remove_index :projects, 'name'
  end
end