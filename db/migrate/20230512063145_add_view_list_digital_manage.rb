class AddViewListDigitalManage < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW view_list_digital_topics AS 
      SELECT dt.id,
      dt.project_id,
      dt.title,
      dt.body,
      dt.active,
      (select count(1) from digital_comments dc where dc.digital_topic_id=dt.id) as n_comments,
      (select count(1) from digital_follows df  where df.digital_topic_id=dt.id) as n_follows,
      (select sum(dv.value) from digital_votes dv  where dv.votable_id=dt.id and dv.votable_type='Digital::Topic') as votes
      FROM digital_topics dt;

      CREATE OR REPLACE VIEW view_list_digital_faqs AS 
      SELECT df.id,
      df.project_id,
      df.title,
      df.description,
      df."order",
      df.active 
      FROM digital_faqs df;
    }
  end
end
