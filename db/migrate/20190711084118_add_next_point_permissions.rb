class AddNextPointPermissions < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE next_points_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE next_points_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE next_points_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE next_points_id_seq TO volun_app;

        ALTER TABLE  next_points
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  next_points TO volun_owner;
        GRANT SELECT ON TABLE  next_points TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  next_points TO volun_app; 
      }
    end
  end
end
