class AddCodeUnsubscribeReason < ActiveRecord::Migration[6.1]
  def up
    add_column :unsubscribe_reasons,:code,:string
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_unsubscribe_reasons
AS SELECT unsubscribe_reasons.id,
    unsubscribe_reasons.name,
    unsubscribe_reasons.active,
    unsubscribe_reasons.code
   FROM unsubscribe_reasons;
    }
  end

  def down
    remove_column :unsubscribe_reasons,:code
  end
end
