class RemoveInitialPassToVolunteers < ActiveRecord::Migration[5.1]
  def change
    remove_column :volunteers, :initial_pass, :string
  end
end
