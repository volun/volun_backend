class AddReviewToActivities < ActiveRecord::Migration[5.1]
  def change
    add_column :activities, :review_end_activity, :boolean, default: false
  end
end
