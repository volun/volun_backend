class RemoveKindFromResources < ActiveRecord::Migration[5.1]
  def change
    remove_column :resources, :kind, :integer
  end
end
