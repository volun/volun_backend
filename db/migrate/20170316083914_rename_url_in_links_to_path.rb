class RenameUrlInLinksToPath < ActiveRecord::Migration[5.1]
  def change
    rename_column :links, :url, :path
  end
end
