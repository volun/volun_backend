class AddFieldEnabledAppCitizens < ActiveRecord::Migration[6.1]
  def up
    add_column :citizens, :enabled_app,:boolean,:default => false
  end

  def down
    remove_column :citizens, :enabled_app
  end
end
