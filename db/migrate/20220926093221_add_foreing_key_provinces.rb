class AddForeingKeyProvinces < ActiveRecord::Migration[6.1]
  def change
    # add_foreign_key :addresses, :provinces
    add_foreign_key :citizens, :provinces
    add_foreign_key :rt_project_publishings, :provinces
    add_foreign_key :rt_volunteer_amendments, :provinces
    add_foreign_key :rt_volunteers_demands, :provinces
  end
end
