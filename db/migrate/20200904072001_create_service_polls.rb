class CreateServicePolls < ActiveRecord::Migration[5.1]
  def change
    create_table :service_polls do |t|
      t.references :citizen_service, foreign_key: true
      t.boolean :live_alone_question
      t.boolean :go_out_question
      t.boolean :dificulties_out_question
      t.boolean :need_help_question
      t.boolean :need_alone_question
      t.string :observations

      t.timestamps null: false
    end
  end
end
