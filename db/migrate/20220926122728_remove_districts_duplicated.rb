class RemoveDistrictsDuplicated < ActiveRecord::Migration[6.1]
  def change
    remove_column :projects, :district
    # remove_column :addresses, :district
    # remove_column :addresses, :district_code
    remove_column :boroughts, :district_code
    remove_column :citizen_services, :district
    remove_column :citizen_services, :district_code
    remove_column :rt_volunteer_subscribes, :district
    remove_column :rt_project_publishings, :district
    remove_column :rt_volunteer_amendments, :district
    remove_column :rt_volunteers_demands, :district
  end
end
