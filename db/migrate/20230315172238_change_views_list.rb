class ChangeViewsList < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_rt_volunteer_amendments
AS SELECT vs.id,
    vs.road_type,
    vs.road_name,
    vs.number_type,
    vs.road_number,
    vs.postal_code,
    vs.town,
    vs.phone_number,
    vs.phone_number_alt,
    vs.email,
    vs.notes,
    vs.volunteer_id,
    vs.created_at,
    vs.updated_at,
    vs.province_id,
    vs.district_id,
    vs.borought_id,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM req_statuses rs
          WHERE rs.id = rf.req_status_id) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM managers m
          WHERE rf.manager_id = m.id) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN rf.req_status_id = ANY (ARRAY[3, 4, 2]) THEN rf.status_date::date - rf.created_at::date
            ELSE now()::date - rf.created_at::date
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM managers m
          WHERE rf.manager_id = m.id) AS get_manager,
    ( SELECT
                CASE
                    WHEN rs.description = 'pending'::text THEN 'Pendiente'::text
                    WHEN rs.description = 'processing'::text THEN 'En trámite'::text
                    WHEN rs.description = 'approved'::text THEN 'Aceptada'::text
                    WHEN rs.description = 'rejected'::text THEN 'Rechazada'::text
                    WHEN rs.description = 'appointed'::text THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM req_statuses rs
          WHERE rs.id = rf.req_status_id) AS get_status,
    ( SELECT v.name
           FROM volunteers v
          WHERE v.id = vs.volunteer_id) AS name,
    ( SELECT v.last_name
           FROM volunteers v
          WHERE v.id = vs.volunteer_id) AS last_name,
    ( SELECT v.last_name_alt
           FROM volunteers v
          WHERE v.id = vs.volunteer_id) AS last_name_alt,
    COALESCE(( SELECT d.name
           FROM volunteers v,
            addresses a,
            districts d
          WHERE v.id = vs.volunteer_id AND a.id = v.address_id AND d.id = a.district_id), 'OTROS'::character varying) AS district,
    ( SELECT concat(v.name, ' ', v.last_name, ' ', v.last_name_alt) AS concat
           FROM volunteers v
          WHERE v.id = vs.volunteer_id) AS volunteer_name,
    ( SELECT to_char(v.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volunteers v
          WHERE v.id = vs.volunteer_id) AS volunteer_subscribe_date,
     (select coalesce(v.id_autonum ,v.id) from volunteers v where vs.volunteer_id =v.id ) as id_autonum
   FROM rt_volunteer_amendments vs,
    request_forms rf
  WHERE vs.id = rf.rt_extendable_id AND rf.rt_extendable_type::text = 'Rt::VolunteerAmendment'::text;
 
 CREATE OR REPLACE VIEW volun.view_list_citizen_services
AS SELECT cs.id,
    upper(c.name::text) AS f_citizen_name,
    c.id AS f_citizen_id,
    upper(c.last_name_1::text) AS f_citizen_last_name_1,
    upper(c.last_name_2::text) AS f_citizen_last_name_2,
    ( SELECT v.id
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_id,
    ( SELECT upper(v.name::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_name,
    ( SELECT upper(v.last_name::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_last_name_1,
    ( SELECT upper(v.last_name_alt::text) AS upper
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_last_name_2,
    ( SELECT COALESCE(d.name, 'OTROS'::character varying) AS "coalesce"
           FROM volunteers v,
            addresses a,
            districts d
          WHERE v.id = cs.volunteer_id AND a.id = v.address_id AND a.district_id = d.id) AS f_volunteer_district,
    ( SELECT s.name
           FROM services s
          WHERE s.id = cs.service_id) AS type_service,
        CASE
            WHEN cs.date_request IS NOT NULL THEN concat(cs.date_request, ' ', cs.hour_request)::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_request,
        CASE
            WHEN cs.date_accept_request IS NOT NULL THEN concat(cs.date_accept_request, ' ', cs.hour_accept_request)::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_accept,
    ( SELECT ss.name
           FROM status_services ss
          WHERE ss.id = cs.status_service_id OR ss.code::text = cs.status::text
         LIMIT 1) AS status_name,
    ( SELECT sv.vote
           FROM service_votes sv
          WHERE sv.citizen_service_id = cs.id AND sv.type_user::text = 'Citizen'::text) AS f_citizen_vote,
    ( SELECT sv.vote
           FROM service_votes sv
          WHERE sv.citizen_service_id = cs.id AND sv.type_user::text = 'Volunteer'::text) AS f_volunteer_vote,
    cs.canceled_at,
    cs.citizen_reason,
    cs.rejected_at,
    cs.volunteer_reason,
    cs.place,
    cs.additional_data,
    ( SELECT b.name
           FROM boroughts b
          WHERE b.id = cs.borought_id) AS borought_name,
    ( SELECT d.name
           FROM districts d
          WHERE d.id = cs.district_id) AS district_name,
    cs.citizen_id,
    cs.volunteer_id,
    cs.status,
    ( SELECT v.email
           FROM volunteers v
          WHERE v.id = cs.volunteer_id) AS f_volunteer_email,
    c.email AS f_citizen_email,
    cs.date_accept_request,
    cs.date_request,
    cs.active,
    cs.district_id,
    cs.borought_id,
    cs.service_id,
    date_part('year'::text, age(c.birthday::date::timestamp with time zone))::integer AS age_citizen,
    c.gender_id,
    ( SELECT g.name
           FROM genders g
          WHERE g.id = c.gender_id) AS gender_name,
    c.enabled_app,
    c.tester,
    c.email_verified,
    cs.generated_gi,
    (select coalesce(v.id_autonum ,v.id) from volunteers v where cs.volunteer_id  =v.id ) as f_volunteer_id_autonum
   FROM citizen_services cs,
    citizens c
  WHERE cs.citizen_id = c.id;
    }
  end
end
