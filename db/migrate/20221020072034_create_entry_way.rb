class CreateEntryWay < ActiveRecord::Migration[6.1]
  def change
    create_table :entry_ways do |t|
      t.string :name, null: false,unique: true
      t.string :code, null: false,unique: true
      t.boolean :active,:default=> true
      t.timestamps
    end
  end
end
