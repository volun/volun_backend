class AddColumnCitizenLegal < ActiveRecord::Migration[6.1]
  def up
    add_column :citizens, :legal_msg, :boolean, default: false
  end

  def down
    remove_column :citizens, :legal_msg, :boolean
  end
end
