class InsertNewProjectType < ActiveRecord::Migration[5.1]
  def up
   ProjectType.create(id: 8, kind: 8,description: "pt_retired_volunteer", created_at: Time.now, updated_at: Time.now )
  end

  def down
    execute %{
      Delete form project_types where id=8;
    }
  end
end
