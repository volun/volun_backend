class AddColumnTypeForTrackingsTypes < ActiveRecord::Migration[5.1]
  def up
  	add_column :tracking_types,:tracking_types_type,:string
  end

  def down
  	remove_column :tracking_types,:tracking_types_type,:string
  end
end
