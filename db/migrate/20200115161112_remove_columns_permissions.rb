class RemoveColumnsPermissions < ActiveRecord::Migration[5.1]
  def up
    [
      :can_create, :can_update, :can_read, :can_destroy, :can_process_request_form, :can_pre_approve_request_form,
      :can_reject_request_form, :can_mark_request_form_as_processing, :can_mark_request_form_as_pending, 
      :can_mark_request_form_as_appointed, :can_clone_project, :can_volunteers_allowed, :can_publish,
      :can_order
    ].each {|p| remove_column :permissions, p, :boolean if column_exists? :permissions, p}

    remove_reference :permissions, :resource, foreign_key: true
  end

  def down
    [
      :can_create, :can_update, :can_read, :can_destroy, :can_process_request_form, :can_pre_approve_request_form,
      :can_reject_request_form, :can_mark_request_form_as_processing, :can_mark_request_form_as_pending, 
      :can_mark_request_form_as_appointed, :can_clone_project, :can_volunteers_allowed, :can_publish,
      :can_order
    ].each {|p| add_column :permissions, p, :boolean, default: false if !column_exists? :permissions, p}

    add_reference :permissions, :resource, foreign_key: true
  end
end
