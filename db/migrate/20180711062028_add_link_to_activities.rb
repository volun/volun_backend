class AddLinkToActivities < ActiveRecord::Migration[5.1]
  def up
    add_column :activities,:link_url,:string
  end

  def down
    remove_column :activities,:link_url,:string
  end
end
