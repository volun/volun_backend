class ChangeNameColumTypeInSearchForms < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :search_forms, :type, :model_type
  end
end
