class InsertNewReqStatus < ActiveRecord::Migration[5.1]
  def up
    Req::Status.create(id: 5, kind: 5, description: "appointed", created_at: Time.now, updated_at: Time.now)
  end

  def down
    execute %{
      Delete from req_statuses where id=5;
    }
  end
end
