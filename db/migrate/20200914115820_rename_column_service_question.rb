class RenameColumnServiceQuestion < ActiveRecord::Migration[5.1]
  def change
    rename_column :service_questions, :type, :question_type
  end
end
