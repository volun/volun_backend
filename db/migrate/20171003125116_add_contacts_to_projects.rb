class AddContactsToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :contact_name1, :string
    add_column :projects, :contact_last_name1, :string
    add_column :projects, :contact_last_name_alt1, :string
    add_column :projects, :phone_number1, :string
    add_column :projects, :phone_number_alt1, :string
    add_column :projects, :contact_name2, :string
    add_column :projects, :contact_last_name2, :string
    add_column :projects, :contact_last_name_alt2, :string
    add_column :projects, :phone_number2, :string
    add_column :projects, :phone_number_alt2, :string
  end
end
