class AddColumnCitizensUnsubscribed < ActiveRecord::Migration[5.1]
  def change
    add_column :citizens, :unsubscribed, :boolean, :default => false
  end
end
