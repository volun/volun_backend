class ChangeFieldCitizenSecurity < ActiveRecord::Migration[5.2]
  def up
    add_column :citizens, :province, :string
    remove_column :citizens, :postal_code
  end

  def down
    add_column :citizens, :postal_code, :string
    remove_column :citizens, :province
  end
end
