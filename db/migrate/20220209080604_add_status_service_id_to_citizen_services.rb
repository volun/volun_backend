class AddStatusServiceIdToCitizenServices < ActiveRecord::Migration[6.1]
  def change
    add_reference :citizen_services, :status_service, index: true, foreign_key: true
  end
end
