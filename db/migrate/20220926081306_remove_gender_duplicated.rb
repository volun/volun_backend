class RemoveGenderDuplicated < ActiveRecord::Migration[6.1]
  def change
    remove_column :volunteers, :gender
    remove_column :citizens, :gender
  end
end
