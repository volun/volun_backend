class AddGenderCode < ActiveRecord::Migration[6.1]
  def change
    add_reference :citizens, :gender
    add_reference :volunteers, :gender
  end
end
