class CreateAudits < ActiveRecord::Migration[5.1]
  def change
    create_table :audits do |t|
      t.references :manager, foreign_key: true
      t.references :resource, foreign_key: true
      t.string :operation

      t.timestamps null: false
    end
  end
end
