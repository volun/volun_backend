class AddFieldDigitalService < ActiveRecord::Migration[6.1]
  def change
    add_column :services, :is_digital, :boolean, default: false
  end
end
