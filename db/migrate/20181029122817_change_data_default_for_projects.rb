class ChangeDataDefaultForProjects < ActiveRecord::Migration[5.1]
  def change
    change_column :projects, :volunteers_allowed, :boolean, :default => false
    change_column :projects, :publish, :boolean, :default => false
  end
end
