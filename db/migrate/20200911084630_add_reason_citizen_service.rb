class AddReasonCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :reason, :string
  end
end
