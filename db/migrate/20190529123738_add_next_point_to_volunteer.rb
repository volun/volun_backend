class AddNextPointToVolunteer < ActiveRecord::Migration[5.1]
  def change
    add_reference :volunteers, :next_point, foreign_key: true
  end
end
