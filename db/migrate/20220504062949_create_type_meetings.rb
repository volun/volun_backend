class CreateTypeMeetings < ActiveRecord::Migration[6.1]
  def change
    create_table :type_meetings do |t|
      t.string :name
      t.string :description
      t.boolean :active, :default => true
      
      t.timestamps
    end
  end
end
