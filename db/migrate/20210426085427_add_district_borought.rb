class AddDistrictBorought < ActiveRecord::Migration[5.1]
  def up
    add_column :citizen_services, :district, :text
    add_column :citizen_services, :borought, :text
  end

  def remove
    remove_column :citizen_services, :district, :text
    remove_column :citizen_services, :borought, :text
  end
end
