class RemoveNoticeTypeIdNullConstraintFromUsers < ActiveRecord::Migration[5.1]
  def change
    change_column_null :users, :notice_type_id, true
  end
end
