class AddEntryWayInRtVolunteerSub < ActiveRecord::Migration[6.1]
  def change
    add_reference :rt_volunteer_subscribes, :entry_way, index: true, foreign_key: true
  end
end
