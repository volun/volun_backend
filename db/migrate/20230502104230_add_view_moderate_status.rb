class AddViewModerateStatus < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_moderate_statuses
      AS SELECT id, title,code, active from moderate_statuses;
    }  
  end
end
