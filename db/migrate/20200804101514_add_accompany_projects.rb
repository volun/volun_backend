class AddAccompanyProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :accompany, :boolean, :default => false
    remove_column :volunteers, :accompany
  end
end
