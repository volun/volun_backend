class AddGrantTypeComponents < ActiveRecord::Migration[6.1]
  def change
    if !Rails.env.staging?
      execute %{
        GRANT ALL ON TABLE  virtual_community_components TO volun_owner;
        GRANT SELECT, UPDATE, INSERT ON TABLE  virtual_community_components TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT ON TABLE  virtual_community_components TO volun_app;

        ALTER TABLE virtual_community_components_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE virtual_community_components_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE virtual_community_components_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE virtual_community_components_id_seq TO volun_app;

        GRANT ALL ON TABLE  virtual_community_component_types TO volun_owner;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  virtual_community_component_types TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  virtual_community_component_types TO volun_app;

        ALTER TABLE virtual_community_component_types_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE virtual_community_component_types_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE virtual_community_component_types_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE virtual_community_component_types_id_seq TO volun_app;

      }
    end
  end
end
