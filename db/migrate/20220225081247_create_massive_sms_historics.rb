class CreateMassiveSmsHistorics < ActiveRecord::Migration[6.1]
  def change
    create_table :massive_sms_historics do |t|
      t.string :log
      t.references :user, foreign_key: true
      t.text :sms 
      t.text :citizen_ids
      t.text :content

      t.timestamps null: false
    end
  end
end
