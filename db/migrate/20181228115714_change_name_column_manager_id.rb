class ChangeNameColumnManagerId < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :audits, :manager_id, :user_id
  end

  def self.down
    rename_column :audits, :user_id, :manager_id
  end
end
