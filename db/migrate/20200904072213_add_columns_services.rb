class AddColumnsServices < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :name, :string, :unique =>  true
    add_column :services, :active, :boolean
  end
end
