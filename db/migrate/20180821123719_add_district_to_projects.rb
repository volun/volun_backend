class AddDistrictToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :district, :string
  end
end
