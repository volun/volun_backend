class AddConfirmationUser < ActiveRecord::Migration[5.1]
  def up
    add_column :citizens, :email_confirmed, :boolean, :default => false
    add_column :citizens, :confirm_token, :string
  end

  def down
    remove_column :citizens, :email_confirmed
    remove_column :citizens, :confirm_token
  end
end
