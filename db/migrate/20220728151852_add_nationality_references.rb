class AddNationalityReferences < ActiveRecord::Migration[6.1]
  def change
    add_reference :addresses, :nationality
    add_reference :citizens, :nationality
  end
end
