class AddAttachmentFileToLinks < ActiveRecord::Migration[5.1]
  def self.up
    change_table :links do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :links, :file
  end
end
