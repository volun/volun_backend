class AddNotesToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :notes, :string
  end
end
