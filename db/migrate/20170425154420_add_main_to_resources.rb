class AddMainToResources < ActiveRecord::Migration[5.1]
  def change
    add_column :resources, :main, :boolean, default: false
  end
end
