class AddLinkTypeIdToLinks < ActiveRecord::Migration[5.1]
  def change
    add_reference :links, :link_type, index: true, foreign_key: true
  end
end
