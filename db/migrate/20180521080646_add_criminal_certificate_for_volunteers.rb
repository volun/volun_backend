class AddCriminalCertificateForVolunteers < ActiveRecord::Migration[5.1]
  def up 
    add_column :volunteers, :criminal_certificate, :boolean, default: false 
  end 

  def down 
    remove_column :volunteers, :criminal_certificate, :boolean, default: false 
  end
end
