class AddResourceNewCampaing < ActiveRecord::Migration[5.1]
  def change
    resource = Resource.new(name: NewCampaing.name , active: true, main: false)
    resource.description = resource.class_name.model_name.human
    resource.save!
  end
end
