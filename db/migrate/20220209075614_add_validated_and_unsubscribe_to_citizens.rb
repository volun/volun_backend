class AddValidatedAndUnsubscribeToCitizens < ActiveRecord::Migration[6.1]
  def change
    add_column :citizens, :validated_at, :date
    add_column :citizens, :unsubscribed_at, :date
  end
end
