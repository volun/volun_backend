class CreateVolunProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :volun_profiles do |t|
      t.string :name, null: false, unique: true
      t.boolean :active, default: true

      t.timestamps null: false
    end
  end
end
