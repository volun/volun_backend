class AddIdToDegreesVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :degrees_volunteers, :id, :primary_key
  end
end
