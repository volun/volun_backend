class AddResourceShiftDefinitions < ActiveRecord::Migration[5.1]
  def change
    exist = Resource.find_by(name: ShiftDefinition.name)
    if exist.blank?
      resource = Resource.new(name: ShiftDefinition.name , active: true, main: false)
      resource.description = resource.class_name.model_name.human
      resource.save!
    end
  end
end
