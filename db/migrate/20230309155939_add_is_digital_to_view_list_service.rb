class AddIsDigitalToViewListService < ActiveRecord::Migration[6.1]
  def change
    if Rails.env.development?
      execute %{
        CREATE OR REPLACE VIEW view_list_services
          AS SELECT services.id,
              services.name,
              services.description,
              services.active,
              services.code,
              services.is_digital
            FROM services;
      }
    else
      execute %{
        CREATE OR REPLACE VIEW volun.view_list_services
          AS SELECT services.id,
              services.name,
              services.description,
              services.active,
              services.code,
              services.is_digital
            FROM services;
      }
    end
  end
end
