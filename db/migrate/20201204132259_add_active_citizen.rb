class AddActiveCitizen < ActiveRecord::Migration[5.1]
  def change
    add_column :citizens, :active, :boolean, :default => true
    add_column :citizen_services, :active, :boolean, :default => true
  end
end
