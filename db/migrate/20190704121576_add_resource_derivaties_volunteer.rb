class AddResourceDerivatiesVolunteer < ActiveRecord::Migration[5.1]
  def change
    exist = Resource.find_by(name: DerivatiesVolunteer.name)
    if exist.blank?
      resource = Resource.new(name: DerivatiesVolunteer.name , active: true, main: false)
      resource.description = resource.class_name.model_name.human
      resource.save!
    end

    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE derivaties_volunteers_id_seq OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE derivaties_volunteers_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE derivaties_volunteers_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE derivaties_volunteers_id_seq TO volun_app;

        ALTER TABLE  derivaties_volunteers
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  derivaties_volunteers TO volun_owner;
        GRANT SELECT ON TABLE  derivaties_volunteers TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  derivaties_volunteers TO volun_app;

       }
    end
  end
end
