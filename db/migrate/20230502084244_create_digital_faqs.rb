class CreateDigitalFaqs < ActiveRecord::Migration[6.1]
  def change
    create_table :digital_faqs do |t|
      t.references :project, index: true, foreign_key: true
      t.string :title, null: false
      t.string :body, null: false
      t.integer :order, unique: true, null: false
      t.boolean :active, default: true    

      t.timestamps
    end

    execute %{
      ALTER TABLE digital_faqs ADD CONSTRAINT order_check CHECK (digital_faqs.order >= 0) NO INHERIT;
    }
  end
end
