class ChangeViewOrderList < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_services
AS SELECT services.id,
    services.name,
    services.description,
    services.active,
    services.code,
    services.is_digital,
    services.mta_order
   FROM services;
    }
  end
end
