class CreateMassiveEmailHistorics < ActiveRecord::Migration[5.1]
  def change
    create_table :massive_email_historics do |t|
      t.string :log
      t.references :user, foreign_key: true

      t.timestamps null: false
    end
  end
end
