class CreateDerivatiesVolunteers < ActiveRecord::Migration[5.1]
  def change
    create_table :derivaties_volunteers do |t|
      t.references :project, index: true, foreign_key: true
      t.references :volunteer, index: true, foreign_key: true
	    t.boolean :confirmed
      t.boolean :asist
      t.date :date
      t.string :hour
      t.string :place
      t.string :day
      t.string :turn
      t.boolean :active, default: true
      
      t.timestamps null: false
    end
  end
end
