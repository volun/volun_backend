class CreateVisitsOrigin < ActiveRecord::Migration[5.1]
  def change
    create_table :visits_origins do |t|
      t.string :country
      t.integer :n_visits

      t.timestamps null: false
    end
  end
end
