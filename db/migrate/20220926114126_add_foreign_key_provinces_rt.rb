class AddForeignKeyProvincesRt < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :rt_entity_subscribes, :provinces
  end
end
