class CreateDigitalFollows < ActiveRecord::Migration[6.1]
  def change
    create_table :digital_follows do |t|
      t.references :digital_topic, index: true, foreign_key: true
      t.references :userable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
