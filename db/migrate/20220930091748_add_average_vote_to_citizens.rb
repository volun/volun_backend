class AddAverageVoteToCitizens < ActiveRecord::Migration[6.1]
  def change
    add_column :citizens, :average_vote, :float
  end
end
