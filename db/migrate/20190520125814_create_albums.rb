class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.references :project, index: true, foreign_key: true
      t.text :description
      t.integer :year
      t.timestamps null: false
    end
  end
end
