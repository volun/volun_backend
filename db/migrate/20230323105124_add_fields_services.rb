class AddFieldsServices < ActiveRecord::Migration[6.1]
  def up
    add_column :services,:normal_flow,:boolean,:default => false
  end

  def down
    remove_column :services,:normal_flow
  end
end
