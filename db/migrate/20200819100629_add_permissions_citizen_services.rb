class AddPermissionsCitizenServices < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE citizens OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE citizens_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE citizens_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE citizens_id_seq TO volun_app;

        ALTER TABLE  citizens
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  citizens TO volun_owner;
        GRANT SELECT ON TABLE  citizens TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  citizens TO volun_app;

        ALTER TABLE citizen_services OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE citizen_services_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE citizen_services_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE citizen_services_id_seq TO volun_app;

        ALTER TABLE  citizen_services
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  citizen_services TO volun_owner;
        GRANT SELECT ON TABLE  citizen_services TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  citizen_services TO volun_app;

        ALTER TABLE services OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE services_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE services_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE services_id_seq TO volun_app;

        ALTER TABLE  services
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  services TO volun_owner;
        GRANT SELECT ON TABLE  services TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  services TO volun_app;
       }
    end
  end
end
