class AddForeignkeyProject < ActiveRecord::Migration[5.1]
  def change
    add_reference :entity_organizations, :project, index: true, foreign_key: true
  end
end
