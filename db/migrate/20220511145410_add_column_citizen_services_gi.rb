class AddColumnCitizenServicesGi < ActiveRecord::Migration[6.1]
  def up
    add_column :citizen_services, :generated_gi,:boolean,:default => false
  end

  def down
    remove_column :citizen_services, :generated_gi
  end
end
