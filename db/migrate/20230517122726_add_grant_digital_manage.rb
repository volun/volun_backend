class AddGrantDigitalManage < ActiveRecord::Migration[6.1]
  def change
    execute %{
      GRANT ALL ON TABLE  digital_topics TO volun_owner;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_topics TO volun_internet_app;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_topics TO volun_app;

      ALTER TABLE digital_topics_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE digital_topics_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE digital_topics_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE digital_topics_id_seq TO volun_app;

      GRANT ALL ON TABLE  digital_follows TO volun_owner;
      GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  digital_follows TO volun_internet_app;
      GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  digital_follows TO volun_app;

      ALTER TABLE digital_follows_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE digital_follows_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE digital_follows_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE digital_follows_id_seq TO volun_app;

      GRANT ALL ON TABLE  digital_comments TO volun_owner;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_comments TO volun_internet_app;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_comments TO volun_app;

      ALTER TABLE digital_comments_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE digital_comments_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE digital_comments_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE digital_comments_id_seq TO volun_app;

      GRANT ALL ON TABLE  digital_votes TO volun_owner;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_votes TO volun_internet_app;
      GRANT SELECT, UPDATE, INSERT ON TABLE  digital_votes TO volun_app;

      ALTER TABLE digital_votes_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE digital_votes_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE digital_votes_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE digital_votes_id_seq TO volun_app;

      GRANT ALL ON TABLE  moderate_statuses TO volun_owner;
      GRANT SELECT ON TABLE  moderate_statuses TO volun_internet_app;
      GRANT SELECT ON TABLE  moderate_statuses TO volun_app;

      ALTER TABLE moderate_statuses_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE moderate_statuses_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE moderate_statuses_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE moderate_statuses_id_seq TO volun_app;

      GRANT ALL ON TABLE  digital_faqs TO volun_owner;
      GRANT SELECT ON TABLE  digital_faqs TO volun_internet_app;
      GRANT SELECT ON TABLE  digital_faqs TO volun_app;

      ALTER TABLE digital_faqs_id_seq OWNER TO volun_owner;
      GRANT ALL ON SEQUENCE digital_faqs_id_seq TO volun_owner;
      GRANT ALL ON SEQUENCE digital_faqs_id_seq TO volun_internet_app;
      GRANT ALL ON SEQUENCE digital_faqs_id_seq TO volun_app;
    }
  end
end
