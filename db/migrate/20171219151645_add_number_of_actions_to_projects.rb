class AddNumberOfActionsToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :number_of_actions, :integer
  end
end
