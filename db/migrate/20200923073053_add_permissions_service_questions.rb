class AddPermissionsServiceQuestions < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE service_votes OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE service_votes_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE service_votes_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE service_votes_id_seq TO volun_app;

        ALTER TABLE  service_votes
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  service_votes TO volun_owner;
        GRANT SELECT ON TABLE  service_votes TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  service_votes TO volun_app;

        ALTER TABLE service_polls OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE service_polls_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE service_polls_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE service_polls_id_seq TO volun_app;

        ALTER TABLE  service_polls
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  service_polls TO volun_owner;
        GRANT SELECT ON TABLE  service_polls TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  service_polls TO volun_app;

        ALTER TABLE service_questions OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE service_questions_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE service_questions_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE service_questions_id_seq TO volun_app;

        ALTER TABLE  service_questions
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  service_questions TO volun_owner;
        GRANT SELECT ON TABLE  service_questions TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  service_questions TO volun_app;

        ALTER TABLE service_responses OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE service_responses_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE service_responses_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE service_responses_id_seq TO volun_app;

        ALTER TABLE  service_responses
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  service_responses TO volun_owner;
        GRANT SELECT ON TABLE  service_responses TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  service_responses TO volun_app;
       }
    end
  end
end
