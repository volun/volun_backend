class AddOrderServices < ActiveRecord::Migration[6.1]
  def up
    add_column :services,:mta_order, :integer,default: 0
  end
  def down
    remove_column :services,:mta_order
  end
end
