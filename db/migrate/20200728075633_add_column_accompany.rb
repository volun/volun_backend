class AddColumnAccompany < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :accompany, :boolean, :default => false
  end
end
