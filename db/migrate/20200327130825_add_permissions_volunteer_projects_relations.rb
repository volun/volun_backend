class AddPermissionsVolunteerProjectsRelations < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE volunteer_projects_relations OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE volunteer_projects_relations_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE volunteer_projects_relations_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE volunteer_projects_relations_id_seq TO volun_app;

        ALTER TABLE  volunteer_projects_relations
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  volunteer_projects_relations TO volun_owner;
        GRANT SELECT ON TABLE  volunteer_projects_relations TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  volunteer_projects_relations TO volun_app;

       }
    end
  end
end
