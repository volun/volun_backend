class AddRejectAtCitizenService < ActiveRecord::Migration[5.1]
  def change
    add_column :citizen_services, :rejected_at, :date
  end
end
