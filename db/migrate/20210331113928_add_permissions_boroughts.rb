class AddPermissionsBoroughts < ActiveRecord::Migration[5.1]
  def change
    unless (Rails.env.development? || Rails.env.test? || Rails.env.staging?)
      execute %{
        ALTER TABLE boroughts OWNER TO volun_owner;
        GRANT ALL ON SEQUENCE boroughts_id_seq TO volun_owner;
        GRANT ALL ON SEQUENCE boroughts_id_seq TO volun_internet_app;
        GRANT ALL ON SEQUENCE boroughts_id_seq TO volun_app;

        ALTER TABLE  boroughts
          OWNER TO volun_owner;
        GRANT ALL ON TABLE  boroughts TO volun_owner;
        GRANT SELECT ON TABLE  boroughts TO volun_internet_app;
        GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  boroughts TO volun_app;

       }
    end
  end
end
