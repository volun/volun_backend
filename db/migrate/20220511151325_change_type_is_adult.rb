class ChangeTypeIsAdult < ActiveRecord::Migration[6.1]
  def change
   change_column :rt_volunteer_subscribes, :is_adult,:string
   change_column :rt_volunteer_project_subscribes, :is_adult,:string
   change_column :volunteers, :is_adult,:string
  end
end
