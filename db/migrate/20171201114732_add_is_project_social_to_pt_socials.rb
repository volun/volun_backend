class AddIsProjectSocialToPtSocials < ActiveRecord::Migration[5.1]
  def change
    add_column :pt_socials, :is_project_social, :boolean, default: false
  end
end
