class AddAccompanyToVolunteers < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :accompany_volunteer, :boolean, default: false
  end
end
