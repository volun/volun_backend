class CreateWsMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :ws_messages do |t|
      t.text :body
      t.datetime :date_at, null: false
      t.attachment :attach
      t.references :citizen,index: true, foreign_key: true, null: false
      t.references :volunteer,index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end
