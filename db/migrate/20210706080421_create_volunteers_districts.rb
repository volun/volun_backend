class CreateVolunteersDistricts < ActiveRecord::Migration[5.1]
  def change
    create_table :volunteers_districts do |t|
      t.references :district, index: true, foreign_key: true
      t.references :volunteer, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
