class AddPrimaryKeyProjectsVolunteers < ActiveRecord::Migration[5.1]
  def up
    add_column :projects_volunteers, :id, :primary_key
    count = ProjectsVolunteer.all.count + 1
    execute %{
     DROP SEQUENCE IF EXISTS projects_volunteers_id_seq CASCADE;
     CREATE SEQUENCE projects_volunteers_id_seq START #{count};
     ALTER SEQUENCE projects_volunteers_id_seq OWNED BY projects_volunteers.id;
     ALTER TABLE projects_volunteers ALTER COLUMN id SET DEFAULT nextval('projects_volunteers_id_seq');
    }     
  end
end
