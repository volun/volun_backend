class CreateJoinTableProfileVolunteers < ActiveRecord::Migration[5.1]
  def change
    create_join_table :volunteers, :volun_profiles do |t|
      t.index [:volunteer_id, :volun_profile_id], :name => "volunteer_profile"
      t.index [:volun_profile_id, :volunteer_id], :name => "profile_volunteer"
    end
  end
end
