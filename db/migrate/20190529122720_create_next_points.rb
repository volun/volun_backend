class CreateNextPoints < ActiveRecord::Migration[5.1]
  def change
    create_table :next_points do |t|
      t.string :name, null: false, unique: true
      t.text :description
      t.boolean :active, default: true
      
      t.timestamps null: false
    end
  end
end
