class AddColumnsProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :pt_order, :integer
    add_column :projects, :subtype_pt, :string
    add_column :projects, :subtype_proyect, :string
  end
end
