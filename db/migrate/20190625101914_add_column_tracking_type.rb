class AddColumnTrackingType < ActiveRecord::Migration[5.1]
  def change
    add_reference :pro_trackings, :tracking_type, foreign_key: true
  end
end
