class ChangeTypeEnabledApp < ActiveRecord::Migration[6.1]
  def change
    change_column :citizens, :enabled_app,:string
  end
end
