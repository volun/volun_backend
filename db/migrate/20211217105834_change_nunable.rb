class ChangeNunable < ActiveRecord::Migration[6.1]
  def change
    change_column_null :citizen_trackings, :manager_id, true
    change_column_null :citizen_service_trackings, :manager_id, true
  end
end
