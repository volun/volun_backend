class ChangeNameRequest < ActiveRecord::Migration[6.1]
  def change
    rename_column :rt_volunteer_amendments, :project_id, :volunteer_id
  end
end
