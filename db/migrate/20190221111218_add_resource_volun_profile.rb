class AddResourceVolunProfile < ActiveRecord::Migration[5.1]
  def change
    resource = Resource.new(name: VolunProfile.name , active: true, main: false)
    resource.description = resource.class_name.model_name.human
    resource.save!
  end
end
