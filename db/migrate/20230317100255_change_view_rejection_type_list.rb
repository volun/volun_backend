class ChangeViewRejectionTypeList < ActiveRecord::Migration[6.1]
  def change
    execute %{
      CREATE OR REPLACE VIEW volun.view_list_req_rejection_types
      AS SELECT req_rejection_types.id,
          req_rejection_types.name,
          req_rejection_types.description,
          req_rejection_types.active,
          req_rejection_types.code
        FROM req_rejection_types;
    }
  end
end
