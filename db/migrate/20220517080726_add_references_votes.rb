class AddReferencesVotes < ActiveRecord::Migration[6.1]
  def up
    add_column :citizen_services, :volunteer_vote_id, :integer
    add_column :citizen_services, :citizen_vote_id, :integer
  end

  def down
    remove_column :citizen_services, :volunteer_vote_id
    remove_column :citizen_services, :citizen_vote_id
  end
end
