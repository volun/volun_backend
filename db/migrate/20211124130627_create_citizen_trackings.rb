class CreateCitizenTrackings < ActiveRecord::Migration[6.1]
  def change
    create_table :citizen_trackings do |t|
      t.references :citizen, null: false, foreign_key: true
      t.references :tracking_type, null: false, foreign_key: true
      t.references :manager, null: false, foreign_key: true
      t.datetime :tracked_at
      t.boolean :automatic, :default=>false
      t.string :coments
      t.timestamps
    end
  end
end
