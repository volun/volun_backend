class AddColumnProTrackings < ActiveRecord::Migration[5.1]
  def up
    add_reference :pro_trackings,:manager, index: true, foreign_key: true
  end

  def down
    remove_reference :pro_trackings,:manager, index: true, foreign_key: true
  end
end
