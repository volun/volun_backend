class ChangeCheckOrderComponent < ActiveRecord::Migration[6.1]
  def change
    execute %{
      drop index index_virtual_community_components_on_order;
      CREATE UNIQUE INDEX index_virtual_community_components_on_order ON volun.virtual_community_components USING btree ("order",virtual_community_component_id); 
    }
  end
end
