class ChangeColumnsRtVolunteerProjectSubscribes < ActiveRecord::Migration[5.1]
  def change
  	remove_column :rt_volunteer_project_subscribes, :publish_pictures, :boolean
    remove_column :rt_volunteer_project_subscribes, :annual_survey, :boolean
    add_column :rt_volunteer_project_subscribes, :district, :string
    add_column :rt_volunteer_project_subscribes, :postal_code, :string
  end
end
