class AddRemovalNoticeToCitizens < ActiveRecord::Migration[6.1]
  def change
    add_column :citizens, :removal_notice, :boolean, default: false
    add_column :citizens, :removal_notice_at, :date
  end
end
