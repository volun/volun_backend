class GenerateViewsMta < ActiveRecord::Migration[6.1]
  def change
    if Rails.env.production?
      execute %{
        
        CREATE OR REPLACE VIEW view_volunteers_MTA AS 
        select v.id as "ID", v."name" as "Nombre", v.last_name  as "Primer Apellido", v.last_name_alt  as "Segundo Apellido",
        v.created_at as "Fecha de creación", v.updated_at as "Fecha de última actualización", pv.subscribe_date  as "Fecha de alta",
        case pv.active when false then pv.unsubscribe_date else null end  as "Fecha de baja",
        pv.active as "Activo",
        CASE WHEN v.birth_date  is null THEN null
        ELSE date_part('year',age(cast(v.birth_date as date))) END as "Años",
        v.birth_date as "Fecha de nacimiento",
        v.gender  as "Sexo",
        (select a.country  from addresses a where a.id=v.address_id) as "País",
        (select a.province from addresses a where a.id=v.address_id)  as "Provincia",
        (select a.town  from addresses a where a.id=v.address_id)  as "Ciudad",
        (select a.borough  from addresses a where a.id=v.address_id) as "Barrio",
        (select a.district  from addresses a where a.id=v.address_id) as "Distrito",
        (select a.postal_code from addresses a where a.id=v.address_id) as "Código postal"
        from volunteers v, projects_volunteers pv where pv.volunteer_id = v.id and pv.project_id = 961 and v.market = false and v.tester = false
        and (UPPER(pv.volunteer_type) = 'ASOCIATED' or pv.volunteer_type is null);

        CREATE OR replace VIEW view_citizens_MTA AS 
        select c.id as "ID", c."name" as "Nombre", c.last_name_1 as "Primer Apellido", c.last_name_2 as "Segundo Apellido",
        c.created_at as "Fecha de creación", c.updated_at  as "Fecha de última actualización", 
        c.validated_at as "Fecha de alta",
        c.unsubscribed_at as "Fecha de baja",
        CASE WHEN c.birthday is null THEN null
        ELSE date_part('year',age(cast(c.birthday as date))) END as "Años",
        c.birthday as "Fecha de nacimiento",
        c.gender as "Sexo",
        c.country as "País",
        c.province as "Provincia",
        c.people_at_home  as "Personas en la vivienda",
        (select a.borough  from addresses a where a.id=c.address_id) as "Barrio",
        (select a.district  from addresses a where a.id=c.address_id) as "Distrito",
        (select a.postal_code from addresses a where a.id=c.address_id) as "Código postal",
        (select a.town  from addresses a where a.id=c.address_id)  as "Ciudad"
        from citizens c where email_verified = true and market = false and tester = false;

        CREATE OR REPLACE VIEW view_services_MTA AS 
        select cs.id as "ID", cs.date_request as "Fecha del servicio", cs.hour_request as "Hora del servicio",
        cs.date_accept_request as "Fecha de aceptación", cs.hour_request as "Hora de aceptación", cs.description as "Descripción del servicio",
        cs.additional_data as "Datos adicionales", cs.place as "Lugar", cs.district as "Distrito", cs.district_code as "Código de distrito",
        cs.borought as "Barrio", cs.borought_code as "Código de barrio", cs.service_id as "ID tipo de servicio", 
        (select s.description from services s where s.id=cs.service_id) as "Tipo de servicio",
        (select s.code from services s where s.id=cs.service_id) as "Código Tipo de servicio",
        cs.citizen_id as "Id del mayor",
        c."name"  as "Nombre del mayor",
        c.last_name_1 as "Primer Apellido del mayor", c.last_name_2 as "Segundo Apellido del mayor",
        cs.volunteer_id  as "Id del voluntario",
        (select v."name"  from volunteers v where v.id=cs.volunteer_id) as "Nombre del voluntario",
        (select v.last_name  from volunteers v where v.id=cs.volunteer_id) as "Primer apellido del voluntario",
        (select v.last_name_alt  from volunteers v where v.id=cs.volunteer_id) as "Segundo apellido del voluntario",
        cs.status as "Código de estado",
        cs.status_service_id as "Id de estado",
        (select ss."name" from status_services ss where ss.id=cs.status_service_id) as "Nombre del estado",
        cs.canceled_at as "Fecha de cancelación o finalización"
        from citizen_services cs, citizens c where cs.citizen_id =c.id;
      
      
       }
    end
  end
end
