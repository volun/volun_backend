class AddEcVolunteer < ActiveRecord::Migration[5.1]
  def change
    add_column :volunteers, :ec, :boolean, default: false
  end
end
