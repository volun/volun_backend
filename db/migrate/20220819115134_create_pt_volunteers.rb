class CreatePtVolunteers < ActiveRecord::Migration[6.1]
  def change
    create_table :pt_volunteers do |t|
      t.string :notes
      
      t.timestamps
    end
  end
end
