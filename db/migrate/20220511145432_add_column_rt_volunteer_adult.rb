class AddColumnRtVolunteerAdult < ActiveRecord::Migration[6.1]
  def up
    add_column :rt_volunteer_subscribes, :is_adult,:boolean,:default => false
    add_column :rt_volunteer_project_subscribes, :is_adult,:boolean,:default => false
  end

  def down
    remove_column :rt_volunteer_subscribes, :is_adult
    remove_column :rt_volunteer_project_subscribes, :is_adult
  end
end
