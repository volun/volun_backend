class AddFieldCitizenCode < ActiveRecord::Migration[6.1]
  def up
    add_column :citizens, :postal_code, :string
  end

  def down
    remove_column :citizens, :postal_code
  end
end
