class CreateCitizenServices < ActiveRecord::Migration[5.1]
  def change
    create_table :citizen_services do |t|
      t.references :citizen, foreign_key: true
      t.references :service, foreign_key: true
      t.references :volunteer, foreign_key: true
      t.date :date_request
      t.string :hour_request
      t.string :district_code
      t.date :date_accept_request
      t.string :hour_accept_request

      t.timestamps null: false
    end
  end
end
