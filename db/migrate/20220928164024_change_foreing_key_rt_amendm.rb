class ChangeForeingKeyRtAmendm < ActiveRecord::Migration[6.1]
  def change
    remove_foreign_key :rt_volunteer_amendments, :projects
    add_foreign_key :rt_volunteer_amendments, :volunteers
  end
end
