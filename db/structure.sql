SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: volun; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA volun;


--
-- Name: auto_num_volunteer(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.auto_num_volunteer() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
      BEGIN
        UPDATE volunteers SET id_autonum = (SELECT MAX(id_autonum)+1 FROM volunteers) WHERE id = NEW.id;
      RETURN NEW;
      END;
      $$;


--
-- Name: auto_order_project(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.auto_order_project() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
      BEGIN
        UPDATE projects SET pt_order = (SELECT coalesce(MAX(pt_order),0)+1 FROM projects p where p.father_id =new.father_id) WHERE id = NEW.id;
      RETURN NEW;
      END;
      $$;


--
-- Name: delete_citizen(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_citizen() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
      BEGIN
        DELETE FROM audits WHERE user_id in (SELECT id FROM users WHERE loggable_type='Citizen' AND loggable_id = OLD.id);
        DELETE FROM users WHERE loggable_type='Citizen' AND loggable_id = OLD.id;
        DELETE FROM service_votes WHERE citizen_service_id in (SELECT id FROM citizen_services WHERE citizen_id = OLD.id);        
        DELETE FROM service_responses WHERE citizen_service_id in (SELECT id FROM citizen_services WHERE citizen_id = OLD.id);
        DELETE FROM citizen_service_trackings WHERE service_id in (SELECT cs.id FROM citizen_services cs WHERE cs.citizen_id = OLD.id);
        DELETE FROM citizen_trackings WHERE citizen_id = OLD.id;
        DELETE FROM citizen_services WHERE citizen_id = OLD.id;
        
      RETURN OLD;
      END;
      $$;


--
-- Name: delete_project(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_project() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM volun_trackings WHERE project_id=OLD.id;
			    DELETE FROM volun_contacts WHERE project_id=OLD.id;
			    DELETE FROM volun_assessments_projects WHERE project_id=OLD.id;
			    DELETE FROM entities_projects WHERE project_id=OLD.id;
			    DELETE FROM documents WHERE project_id=OLD.id;
			    DELETE FROM activities WHERE project_id=OLD.id;
			    DELETE FROM areas_projects WHERE project_id=OLD.id;
			    DELETE FROM collectives_projects WHERE project_id=OLD.id;
			    DELETE FROM coordinations_projects WHERE project_id=OLD.id;
			    DELETE FROM projects_volunteers WHERE project_id=OLD.id;
			    DELETE FROM pro_trackings WHERE project_id=OLD.id;
			    DELETE FROM pro_issues WHERE project_id=OLD.id;
			    DELETE FROM volun_trackings WHERE project_id=OLD.id;
			    DELETE FROM rt_activity_publishings WHERE project_id=OLD.id;
			    DELETE FROM rt_entity_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_entity_unsubscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_project_publishings WHERE project_id=OLD.id;
			    DELETE FROM rt_project_unpublishings WHERE project_id=OLD.id;
			    DELETE FROM volunteer_projects_relations WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_project_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_subscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteer_unsubscribes WHERE project_id=OLD.id;
			    DELETE FROM rt_volunteers_demands WHERE project_id=OLD.id;
				RETURN OLD;
			END;
			$$;


--
-- Name: delete_project_pt(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_project_pt() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    IF OLD.pt_extendable_type LIKE 'Pt::Social' THEN 
			    	DELETE FROM pt_socials WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Centre' THEN 
			    	DELETE FROM pt_centres WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Permanent' THEN 
			    	DELETE FROM pt_permanents WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Punctual' THEN 
			    	DELETE FROM pt_punctuals WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Subvention' THEN 
			    	DELETE FROM pt_subventions WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Entity' THEN 
			    	DELETE FROM pt_entities WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::Other' THEN 
			    	DELETE FROM pt_others WHERE id=OLD.pt_extendable_id;
			    ELSIF OLD.pt_extendable_type LIKE 'Pt::RetiredVolunteer' THEN 
			    	DELETE FROM pt_retired_volunteers WHERE id=OLD.pt_extendable_id;
				END IF;
			    RETURN NEW;
			END;
			$$;


--
-- Name: delete_projects_volunteer(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_projects_volunteer() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
      BEGIN
          DELETE FROM volun_proy_meetings WHERE projects_volunteer_id=OLD.id;
        RETURN OLD;
      END;
      $$;


--
-- Name: delete_request_forms(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_request_forms() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			   
			    DELETE FROM req_status_traces WHERE request_form_id=OLD.id;
			    DELETE FROM volun_trackings WHERE request_form_id=OLD.id;
			    DELETE FROM ent_trackings WHERE request_form_id=OLD.id;
			    

			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_activity_publishings(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_activity_publishings() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::ActivityPublishing';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_entity_subscribes(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_entity_subscribes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::EntitySubscribe';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_entity_unsubscribes(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_entity_unsubscribes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::EntityUnsubscribe';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_project_publishings(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_project_publishings() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::ProjectPublishing';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_project_unpublishings(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_project_unpublishings() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::ProjectUnpublishing';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_volunteer_amendments(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_volunteer_amendments() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::VolunteerAmendment';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_volunteer_project_subscribes(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_volunteer_project_subscribes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::VolunteerProjectSubscribe';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_volunteer_subscribes(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_volunteer_subscribes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::VolunteerSubscribe';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_volunteer_unsubscribes(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_volunteer_unsubscribes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::VolunteerUnsubscribe';
			    RETURN OLD;
			END;
			$$;


--
-- Name: delete_rt_volunteers_demands(); Type: FUNCTION; Schema: volun; Owner: -
--

CREATE FUNCTION volun.delete_rt_volunteers_demands() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
			BEGIN
			    DELETE FROM request_forms WHERE rt_extendable_id=OLD.id AND rt_extendable_type='Rt::VolunteersDemand';
			    RETURN OLD;
			END;
			$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: academic_levels; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.academic_levels (
    id integer NOT NULL,
    name character varying NOT NULL,
    educational_type character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: academic_levels_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.academic_levels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: academic_levels_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.academic_levels_id_seq OWNED BY volun.academic_levels.id;


--
-- Name: activities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.activities (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text NOT NULL,
    start_date date NOT NULL,
    end_date date,
    transport text NOT NULL,
    pdf_url character varying,
    publish boolean DEFAULT true,
    active boolean DEFAULT true,
    entity_id integer,
    area_id integer,
    project_id integer,
    share boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    review_end_activity boolean DEFAULT false,
    organizers character varying,
    link_url character varying
);


--
-- Name: activities_areas; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.activities_areas (
    area_id integer NOT NULL,
    activity_id integer NOT NULL
);


--
-- Name: activities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: activities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.activities_id_seq OWNED BY volun.activities.id;


--
-- Name: addresses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.addresses (
    id integer NOT NULL,
    road_type character varying,
    road_name character varying,
    road_number_type character varying,
    road_number character varying,
    grader character varying,
    stairs character varying,
    floor character varying,
    door character varying,
    postal_code character varying,
    town character varying,
    country character varying,
    ndp_code character varying,
    local_code character varying,
    town_code character varying,
    class_name character varying,
    latitude character varying,
    longitude character varying,
    "normalize" boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    notes text,
    comments text,
    xetrs89 character varying,
    yetrs89 character varying,
    province_id bigint,
    district_id bigint,
    nationality_id bigint,
    borought_id bigint,
    country_code character varying
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.addresses_id_seq OWNED BY volun.addresses.id;


--
-- Name: ahoy_events; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ahoy_events (
    id integer NOT NULL,
    visit_id integer,
    user_id integer,
    name character varying,
    properties jsonb,
    "time" timestamp without time zone
);


--
-- Name: ahoy_events_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.ahoy_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ahoy_events_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.ahoy_events_id_seq OWNED BY volun.ahoy_events.id;


--
-- Name: ahoy_visits; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ahoy_visits (
    id integer NOT NULL,
    visit_token character varying,
    visitor_token character varying,
    user_id integer,
    ip character varying,
    user_agent text,
    referrer text,
    referring_domain character varying,
    landing_page text,
    browser character varying,
    os character varying,
    device_type character varying,
    country character varying,
    region character varying,
    city character varying,
    utm_source character varying,
    utm_medium character varying,
    utm_term character varying,
    utm_content character varying,
    utm_campaign character varying,
    app_version character varying,
    os_version character varying,
    platform character varying,
    started_at timestamp without time zone
);


--
-- Name: ahoy_visits_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.ahoy_visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ahoy_visits_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.ahoy_visits_id_seq OWNED BY volun.ahoy_visits.id;


--
-- Name: albums; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.albums (
    id integer NOT NULL,
    project_id integer,
    description text,
    year integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: albums_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.albums_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: albums_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.albums_id_seq OWNED BY volun.albums.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: areas; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.areas (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: areas_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.areas_id_seq OWNED BY volun.areas.id;


--
-- Name: areas_projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.areas_projects (
    area_id integer NOT NULL,
    project_id integer NOT NULL
);


--
-- Name: areas_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.areas_volunteers (
    area_id integer NOT NULL,
    volunteer_id integer NOT NULL
);


--
-- Name: audits; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.audits (
    id integer NOT NULL,
    resource_id integer,
    operation character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    operation_type character varying,
    user_id integer,
    old_data text,
    new_data text
);


--
-- Name: audits_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.audits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audits_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.audits_id_seq OWNED BY volun.audits.id;


--
-- Name: boroughts; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.boroughts (
    id integer NOT NULL,
    name character varying,
    code character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    district_id bigint,
    active boolean DEFAULT true
);


--
-- Name: boroughts_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.boroughts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: boroughts_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.boroughts_id_seq OWNED BY volun.boroughts.id;


--
-- Name: channels; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.channels (
    id bigint NOT NULL,
    name character varying NOT NULL,
    code character varying NOT NULL,
    active boolean DEFAULT true,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.channels_id_seq OWNED BY volun.channels.id;


--
-- Name: check_graphics; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.check_graphics (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: check_graphics_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.check_graphics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: check_graphics_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.check_graphics_id_seq OWNED BY volun.check_graphics.id;


--
-- Name: citizen_service_trackings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.citizen_service_trackings (
    id bigint NOT NULL,
    citizen_service_id bigint NOT NULL,
    tracking_type_id bigint NOT NULL,
    manager_id bigint,
    tracked_at timestamp without time zone,
    automatic boolean DEFAULT false,
    coments character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: citizen_service_trackings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.citizen_service_trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: citizen_service_trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.citizen_service_trackings_id_seq OWNED BY volun.citizen_service_trackings.id;


--
-- Name: citizen_services; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.citizen_services (
    id integer NOT NULL,
    citizen_id integer,
    service_id integer,
    volunteer_id integer,
    date_request date,
    hour_request character varying,
    date_accept_request date,
    hour_accept_request character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    canceled_at date,
    volunteer_reason character varying,
    citizen_reason character varying,
    rejected_at date,
    description character varying,
    place character varying,
    additional_data character varying,
    citizen_vote character varying,
    volunteer_vote character varying,
    status character varying,
    active boolean DEFAULT true,
    send_sms_citizen boolean DEFAULT false,
    reason_manage_canceled_at timestamp without time zone,
    reason_manage_active_at timestamp without time zone,
    indications character varying,
    warning_message_volunteer character varying,
    reason_manage_canceled character varying,
    status_service_id bigint,
    generated_gi boolean DEFAULT false,
    volunteer_vote_id integer,
    citizen_vote_id integer,
    district_id bigint,
    borought_id bigint
);


--
-- Name: citizen_services_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.citizen_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: citizen_services_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.citizen_services_id_seq OWNED BY volun.citizen_services.id;


--
-- Name: citizen_trackings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.citizen_trackings (
    id bigint NOT NULL,
    citizen_id bigint NOT NULL,
    tracking_type_id bigint NOT NULL,
    manager_id bigint,
    tracked_at timestamp without time zone,
    automatic boolean DEFAULT false,
    coments character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: citizen_trackings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.citizen_trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: citizen_trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.citizen_trackings_id_seq OWNED BY volun.citizen_trackings.id;


--
-- Name: citizens; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.citizens (
    id integer NOT NULL,
    name character varying,
    last_name_1 character varying,
    last_name_2 character varying,
    phone character varying,
    email character varying,
    document character varying,
    document_type_id integer,
    birthday character varying,
    country character varying,
    academic_info character varying,
    info_source_id integer,
    people_at_home integer,
    start_date date,
    last_sign_in date,
    code_ci360 character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    address_id integer,
    terms_of_service boolean,
    access_token character varying,
    authentication_token text,
    authentication_token_created_at timestamp without time zone,
    code_cid360_autentication character varying,
    validation_cid360 character varying,
    active boolean DEFAULT true,
    email_verified boolean DEFAULT false,
    unsubscribed boolean DEFAULT false,
    market boolean DEFAULT false,
    email_confirmed boolean DEFAULT false,
    confirm_token character varying,
    tester boolean DEFAULT false,
    padron boolean DEFAULT true,
    legal_msg boolean DEFAULT false,
    validated_at date,
    unsubscribed_at date,
    internal_management boolean DEFAULT false,
    enabled_app character varying DEFAULT false,
    removal_notice boolean DEFAULT false,
    removal_notice_at date,
    gender_id bigint,
    province_id bigint,
    nationality_id bigint,
    average_vote double precision,
    country_code character varying,
    is_death boolean DEFAULT false,
    padron_reason character varying,
    validate_code_sms text,
    validate_sms_intents integer DEFAULT 0,
    validate_sms_at timestamp without time zone
);


--
-- Name: citizens_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.citizens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: citizens_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.citizens_id_seq OWNED BY volun.citizens.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ckeditor_assets (
    id bigint NOT NULL,
    data_file_name character varying NOT NULL,
    data_content_type character varying,
    data_file_size integer,
    data_fingerprint character varying,
    type character varying(30),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.ckeditor_assets_id_seq OWNED BY volun.ckeditor_assets.id;


--
-- Name: collectives; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.collectives (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: collectives_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.collectives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: collectives_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.collectives_id_seq OWNED BY volun.collectives.id;


--
-- Name: collectives_projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.collectives_projects (
    collective_id integer NOT NULL,
    project_id integer NOT NULL
);


--
-- Name: collectives_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.collectives_volunteers (
    collective_id integer NOT NULL,
    volunteer_id integer NOT NULL
);


--
-- Name: contact_results; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.contact_results (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: contact_results_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.contact_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_results_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.contact_results_id_seq OWNED BY volun.contact_results.id;


--
-- Name: contact_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.contact_types (
    id integer NOT NULL,
    name character varying,
    active character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: contact_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.contact_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contact_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.contact_types_id_seq OWNED BY volun.contact_types.id;


--
-- Name: coordinations; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.coordinations (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: coordinations_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.coordinations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: coordinations_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.coordinations_id_seq OWNED BY volun.coordinations.id;


--
-- Name: coordinations_projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.coordinations_projects (
    coordination_id integer NOT NULL,
    project_id integer NOT NULL
);


--
-- Name: degree_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.degree_types (
    id integer NOT NULL,
    name character varying,
    educational_type character varying,
    active character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: degree_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.degree_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: degree_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.degree_types_id_seq OWNED BY volun.degree_types.id;


--
-- Name: degrees; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.degrees (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    degree_type_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: degrees_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.degrees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: degrees_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.degrees_id_seq OWNED BY volun.degrees.id;


--
-- Name: degrees_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.degrees_volunteers (
    degree_id integer NOT NULL,
    volunteer_id integer NOT NULL,
    id integer NOT NULL
);


--
-- Name: degrees_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.degrees_volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: degrees_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.degrees_volunteers_id_seq OWNED BY volun.degrees_volunteers.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying,
    queue character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.delayed_jobs_id_seq OWNED BY volun.delayed_jobs.id;


--
-- Name: derivaties_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.derivaties_volunteers (
    id integer NOT NULL,
    project_id integer,
    volunteer_id integer,
    confirmed boolean,
    asist boolean,
    date date,
    hour character varying,
    place character varying,
    day character varying,
    turn character varying,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: derivaties_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.derivaties_volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: derivaties_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.derivaties_volunteers_id_seq OWNED BY volun.derivaties_volunteers.id;


--
-- Name: digital_comments; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.digital_comments (
    id bigint NOT NULL,
    digital_topic_id bigint,
    active boolean DEFAULT true,
    body character varying NOT NULL,
    moderate_status_id bigint,
    moderable_type character varying,
    moderable_id bigint,
    userable_type character varying,
    userable_id bigint,
    digital_comment_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: digital_comments_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.digital_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digital_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.digital_comments_id_seq OWNED BY volun.digital_comments.id;


--
-- Name: digital_faqs; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.digital_faqs (
    id bigint NOT NULL,
    project_id bigint,
    title character varying NOT NULL,
    description character varying NOT NULL,
    "order" integer NOT NULL,
    active boolean DEFAULT true,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    CONSTRAINT order_check CHECK (("order" >= 0)) NO INHERIT
);


--
-- Name: digital_faqs_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.digital_faqs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digital_faqs_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.digital_faqs_id_seq OWNED BY volun.digital_faqs.id;


--
-- Name: digital_follows; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.digital_follows (
    id bigint NOT NULL,
    digital_topic_id bigint,
    userable_type character varying,
    userable_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: digital_follows_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.digital_follows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digital_follows_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.digital_follows_id_seq OWNED BY volun.digital_follows.id;


--
-- Name: digital_topics; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.digital_topics (
    id bigint NOT NULL,
    project_id bigint,
    title character varying NOT NULL,
    body character varying NOT NULL,
    active boolean DEFAULT true,
    moderate_status_id bigint,
    moderable_type character varying,
    moderable_id bigint,
    userable_type character varying,
    userable_id bigint,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: digital_topics_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.digital_topics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digital_topics_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.digital_topics_id_seq OWNED BY volun.digital_topics.id;


--
-- Name: digital_votes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.digital_votes (
    id bigint NOT NULL,
    votable_type character varying,
    votable_id bigint,
    userable_type character varying,
    userable_id bigint,
    value integer NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: digital_votes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.digital_votes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: digital_votes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.digital_votes_id_seq OWNED BY volun.digital_votes.id;


--
-- Name: districts; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.districts (
    id integer NOT NULL,
    name character varying,
    code character varying,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: districts_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.districts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: districts_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.districts_id_seq OWNED BY volun.districts.id;


--
-- Name: districts_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.districts_volunteers (
    id bigint NOT NULL,
    district_id bigint,
    volunteer_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: districts_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.districts_volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: districts_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.districts_volunteers_id_seq OWNED BY volun.districts_volunteers.id;


--
-- Name: document_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.document_types (
    id bigint NOT NULL,
    name character varying NOT NULL,
    code integer NOT NULL,
    active boolean DEFAULT true,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: document_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.document_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: document_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.document_types_id_seq OWNED BY volun.document_types.id;


--
-- Name: documents; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.documents (
    id integer NOT NULL,
    name character varying,
    description text,
    extension character varying,
    csv character varying,
    doc_class character varying,
    documentum_id character varying,
    project_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.documents_id_seq OWNED BY volun.documents.id;


--
-- Name: employment_statuses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.employment_statuses (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: employment_statuses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.employment_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employment_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.employment_statuses_id_seq OWNED BY volun.employment_statuses.id;


--
-- Name: ent_trackings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ent_trackings (
    id integer NOT NULL,
    tracking_type_id integer NOT NULL,
    entity_id integer NOT NULL,
    manager_id integer,
    request_form_id integer,
    tracked_at timestamp without time zone NOT NULL,
    automatic boolean DEFAULT false,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: ent_trackings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.ent_trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ent_trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.ent_trackings_id_seq OWNED BY volun.ent_trackings.id;


--
-- Name: entities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.entities (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    vat_number character varying,
    email character varying NOT NULL,
    representative_name character varying NOT NULL,
    representative_last_name character varying NOT NULL,
    representative_last_name_alt character varying,
    contact_name character varying NOT NULL,
    contact_last_name character varying NOT NULL,
    contact_last_name_alt character varying,
    phone_number character varying,
    phone_number_alt character varying,
    publish_pictures boolean DEFAULT true,
    annual_survey boolean DEFAULT false,
    req_reason_id integer,
    entity_type_id integer NOT NULL,
    comments text,
    other_subscribe_reason text,
    address_id integer NOT NULL,
    active boolean DEFAULT true,
    subscribed_at timestamp without time zone,
    unsubscribed_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: entities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.entities_id_seq OWNED BY volun.entities.id;


--
-- Name: entities_projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.entities_projects (
    entity_id integer NOT NULL,
    project_id integer NOT NULL
);


--
-- Name: entity_organizations; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.entity_organizations (
    id integer NOT NULL,
    name character varying NOT NULL,
    person_name character varying,
    phone character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    project_id integer
);


--
-- Name: entity_organizations_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.entity_organizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entity_organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.entity_organizations_id_seq OWNED BY volun.entity_organizations.id;


--
-- Name: entity_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.entity_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: entity_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.entity_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entity_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.entity_types_id_seq OWNED BY volun.entity_types.id;


--
-- Name: event_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.event_types (
    id integer NOT NULL,
    kind integer NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT kind_and_id_must_be_equal CHECK ((((id = 1) AND (kind = 1)) OR ((id = 2) AND (kind = 2))))
);


--
-- Name: event_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.event_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.event_types_id_seq OWNED BY volun.event_types.id;


--
-- Name: events; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.events (
    id integer NOT NULL,
    publish boolean DEFAULT true,
    eventable_id integer NOT NULL,
    eventable_type character varying NOT NULL,
    event_type_id integer,
    address_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    notes text,
    comments text,
    CONSTRAINT eventable_must_be_consistent CHECK ((((event_type_id = 1) AND ((eventable_type)::text = 'Activity'::text)) OR ((event_type_id = 2) AND ((eventable_type)::text = 'Project'::text))))
);


--
-- Name: events_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.events_id_seq OWNED BY volun.events.id;


--
-- Name: frontpage_elements; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.frontpage_elements (
    id integer NOT NULL,
    frontpage_position_id integer NOT NULL,
    text_panel text,
    text_button text,
    image_url text,
    link_url text,
    logo_url text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    created_by integer NOT NULL,
    updated_by integer
);


--
-- Name: frontpage_elements_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.frontpage_elements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: frontpage_elements_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.frontpage_elements_id_seq OWNED BY volun.frontpage_elements.id;


--
-- Name: frontpage_positions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.frontpage_positions (
    id integer NOT NULL,
    "position" integer,
    description text,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: frontpage_positions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.frontpage_positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: frontpage_positions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.frontpage_positions_id_seq OWNED BY volun.frontpage_positions.id;


--
-- Name: genders; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.genders (
    id bigint NOT NULL,
    name character varying,
    active boolean DEFAULT false,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: genders_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.genders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: genders_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.genders_id_seq OWNED BY volun.genders.id;


--
-- Name: id_number_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.id_number_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    code integer
);


--
-- Name: id_number_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.id_number_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: id_number_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.id_number_types_id_seq OWNED BY volun.id_number_types.id;


--
-- Name: info_sources; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.info_sources (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: info_sources_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.info_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: info_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.info_sources_id_seq OWNED BY volun.info_sources.id;


--
-- Name: language_levels; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.language_levels (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: language_levels_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.language_levels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: language_levels_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.language_levels_id_seq OWNED BY volun.language_levels.id;


--
-- Name: languages; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.languages (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.languages_id_seq OWNED BY volun.languages.id;


--
-- Name: link_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.link_types (
    id integer NOT NULL,
    kind integer,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: link_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.link_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: link_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.link_types_id_seq OWNED BY volun.link_types.id;


--
-- Name: links; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.links (
    id integer NOT NULL,
    path character varying,
    description text,
    linkable_id integer,
    linkable_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    link_type_id integer,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone
);


--
-- Name: links_cpy; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.links_cpy (
    id integer,
    path character varying,
    description text,
    linkable_id integer,
    linkable_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    link_type_id integer,
    file_file_name character varying,
    file_content_type character varying,
    file_file_size integer,
    file_updated_at timestamp without time zone
);


--
-- Name: links_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.links_id_seq OWNED BY volun.links.id;


--
-- Name: managers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.managers (
    id integer NOT NULL,
    name character varying NOT NULL,
    last_name character varying,
    last_name_alt character varying,
    alias_name character varying,
    role_id integer,
    profile_id integer,
    phone_number character varying,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    login character varying,
    uweb_id character varying,
    document character varying,
    email character varying,
    official_position character varying,
    personal_number character varying
);


--
-- Name: managers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.managers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: managers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.managers_id_seq OWNED BY volun.managers.id;


--
-- Name: massive_email_historics; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.massive_email_historics (
    id integer NOT NULL,
    log character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    emails text,
    filters text,
    volun_ids text,
    title character varying,
    content text
);


--
-- Name: massive_email_historics_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.massive_email_historics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: massive_email_historics_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.massive_email_historics_id_seq OWNED BY volun.massive_email_historics.id;


--
-- Name: massive_sms_historics; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.massive_sms_historics (
    id bigint NOT NULL,
    log character varying,
    user_id bigint,
    sms text,
    records_ids text,
    content text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    specific character varying
);


--
-- Name: massive_sms_historics_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.massive_sms_historics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: massive_sms_historics_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.massive_sms_historics_id_seq OWNED BY volun.massive_sms_historics.id;


--
-- Name: meetings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.meetings (
    id integer NOT NULL,
    project_id integer,
    name character varying,
    date date,
    hour character varying,
    place character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    type_meeting_id bigint
);


--
-- Name: meetings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.meetings_id_seq OWNED BY volun.meetings.id;


--
-- Name: moderate_statuses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.moderate_statuses (
    id bigint NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    active boolean DEFAULT true,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: moderate_statuses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.moderate_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: moderate_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.moderate_statuses_id_seq OWNED BY volun.moderate_statuses.id;


--
-- Name: motivations; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.motivations (
    id integer NOT NULL,
    name character varying,
    active character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: motivations_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.motivations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: motivations_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.motivations_id_seq OWNED BY volun.motivations.id;


--
-- Name: nationalities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.nationalities (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: nationalities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.nationalities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: nationalities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.nationalities_id_seq OWNED BY volun.nationalities.id;


--
-- Name: new_campaings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.new_campaings (
    id integer NOT NULL,
    title character varying NOT NULL,
    body text,
    publicable boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: new_campaings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.new_campaings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: new_campaings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.new_campaings_id_seq OWNED BY volun.new_campaings.id;


--
-- Name: next_points; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.next_points (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: next_points_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.next_points_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: next_points_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.next_points_id_seq OWNED BY volun.next_points.id;


--
-- Name: notice_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.notice_types (
    id integer NOT NULL,
    kind integer,
    description text,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: notice_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.notice_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notice_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.notice_types_id_seq OWNED BY volun.notice_types.id;


--
-- Name: permissions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.permissions (
    id integer NOT NULL,
    manager_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    section text,
    data jsonb DEFAULT '{}'::jsonb NOT NULL
);


--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.permissions_id_seq OWNED BY volun.permissions.id;


--
-- Name: popular_sections; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.popular_sections (
    id integer NOT NULL,
    sections character varying,
    n_visits integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: popular_sections_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.popular_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: popular_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.popular_sections_id_seq OWNED BY volun.popular_sections.id;


--
-- Name: pro_issues; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pro_issues (
    id integer NOT NULL,
    comments text,
    start_date timestamp without time zone,
    project_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pro_issues_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pro_issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pro_issues_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pro_issues_id_seq OWNED BY volun.pro_issues.id;


--
-- Name: pro_trackings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pro_trackings (
    id integer NOT NULL,
    project_id integer,
    request_form_id integer,
    tracked_at timestamp without time zone,
    automatic boolean DEFAULT false,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    manager_id integer,
    tracking_type_id integer
);


--
-- Name: pro_trackings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pro_trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pro_trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pro_trackings_id_seq OWNED BY volun.pro_trackings.id;


--
-- Name: professions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.professions (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: professions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.professions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: professions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.professions_id_seq OWNED BY volun.professions.id;


--
-- Name: profiles; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.profiles (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.profiles_id_seq OWNED BY volun.profiles.id;


--
-- Name: project_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.project_types (
    id integer NOT NULL,
    kind integer NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: project_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.project_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: project_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.project_types_id_seq OWNED BY volun.project_types.id;


--
-- Name: projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.projects (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    description text NOT NULL,
    functions character varying,
    execution_start_date date NOT NULL,
    execution_end_date date,
    contact_name character varying NOT NULL,
    contact_last_name character varying NOT NULL,
    contact_last_name_alt character varying,
    phone_number character varying NOT NULL,
    phone_number_alt character varying,
    email character varying NOT NULL,
    participants_num integer,
    beneficiaries_num integer,
    volunteers_num integer,
    volunteers_allowed boolean DEFAULT false,
    publish boolean DEFAULT false,
    outstanding boolean DEFAULT false,
    insurance_date date,
    comments text,
    insured boolean DEFAULT false,
    pt_extendable_id integer NOT NULL,
    pt_extendable_type character varying NOT NULL,
    entity_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    urgent boolean DEFAULT false,
    contact_name1 character varying,
    contact_last_name1 character varying,
    contact_last_name_alt1 character varying,
    phone_number1 character varying,
    phone_number_alt1 character varying,
    contact_name2 character varying,
    contact_last_name2 character varying,
    contact_last_name_alt2 character varying,
    phone_number2 character varying,
    phone_number_alt2 character varying,
    email1 character varying,
    email2 character varying,
    area_departament character varying,
    number_of_activities integer,
    number_of_actions integer,
    year integer,
    review_end_project boolean DEFAULT false,
    cloned boolean,
    origin_id integer,
    "order" integer DEFAULT 1000,
    publish_chart boolean DEFAULT false,
    accompany boolean DEFAULT false,
    father_id integer,
    suspended boolean DEFAULT false,
    change_status_at date,
    suspended_at timestamp without time zone,
    reactivated_at timestamp without time zone,
    district_id bigint,
    pt_order integer,
    subtype_pt character varying,
    subtype_proyect character varying,
    permit_debate boolean DEFAULT false,
    permit_faq boolean DEFAULT false
);


--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.projects_id_seq OWNED BY volun.projects.id;


--
-- Name: projects_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.projects_volunteers (
    project_id integer NOT NULL,
    volunteer_id integer NOT NULL,
    active boolean DEFAULT true,
    subscribe_date date,
    unsubscribe_date date,
    comments character varying,
    id integer NOT NULL,
    day character varying,
    volunteer_type character varying,
    shift_definition_id integer,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: projects_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.projects_volunteers_id_seq
    START WITH 46
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: projects_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.projects_volunteers_id_seq OWNED BY volun.projects_volunteers.id;


--
-- Name: proposals; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.proposals (
    id integer NOT NULL,
    name character varying,
    description text,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: proposals_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.proposals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: proposals_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.proposals_id_seq OWNED BY volun.proposals.id;


--
-- Name: provinces; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.provinces (
    id integer NOT NULL,
    name character varying,
    code character varying,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: provinces_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.provinces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: provinces_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.provinces_id_seq OWNED BY volun.provinces.id;


--
-- Name: pt_centres; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_centres (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_centres_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_centres_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_centres_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_centres_id_seq OWNED BY volun.pt_centres.id;


--
-- Name: pt_entities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_entities (
    id integer NOT NULL,
    request_date date,
    request_description text,
    volunteers_profile character varying,
    activities text,
    sav_date date,
    derived_volunteers_num integer,
    added_volunteers_num integer,
    agreement_signed boolean DEFAULT false,
    agreement_date date,
    prevailing boolean DEFAULT false,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_entities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_entities_id_seq OWNED BY volun.pt_entities.id;


--
-- Name: pt_municipals; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_municipals (
    id bigint NOT NULL,
    notes character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: pt_municipals_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_municipals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_municipals_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_municipals_id_seq OWNED BY volun.pt_municipals.id;


--
-- Name: pt_others; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_others (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_others_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_others_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_others_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_others_id_seq OWNED BY volun.pt_others.id;


--
-- Name: pt_permanents; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_permanents (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_permanents_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_permanents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_permanents_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_permanents_id_seq OWNED BY volun.pt_permanents.id;


--
-- Name: pt_punctuals; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_punctuals (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_punctuals_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_punctuals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_punctuals_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_punctuals_id_seq OWNED BY volun.pt_punctuals.id;


--
-- Name: pt_retired_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_retired_volunteers (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_retired_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_retired_volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_retired_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_retired_volunteers_id_seq OWNED BY volun.pt_retired_volunteers.id;


--
-- Name: pt_socials; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_socials (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_project_social boolean DEFAULT false
);


--
-- Name: pt_socials_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_socials_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_socials_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_socials_id_seq OWNED BY volun.pt_socials.id;


--
-- Name: pt_subventions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_subventions (
    id integer NOT NULL,
    representative_name character varying,
    representative_last_name character varying,
    representative_last_name_alt character varying,
    id_num character varying,
    vat_number character varying,
    entity_registry boolean DEFAULT false,
    cost double precision,
    requested_amount double precision,
    subsidized_amount double precision,
    initial_volunteers_num integer,
    has_quality_evaluation boolean DEFAULT false,
    proposal_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pt_subventions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_subventions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_subventions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_subventions_id_seq OWNED BY volun.pt_subventions.id;


--
-- Name: pt_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.pt_volunteers (
    id bigint NOT NULL,
    notes character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: pt_volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.pt_volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pt_volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.pt_volunteers_id_seq OWNED BY volun.pt_volunteers.id;


--
-- Name: record_histories; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.record_histories (
    id integer NOT NULL,
    user_id integer NOT NULL,
    recordable_id integer NOT NULL,
    recordable_type character varying NOT NULL,
    recordable_changed_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: record_histories_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.record_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: record_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.record_histories_id_seq OWNED BY volun.record_histories.id;


--
-- Name: req_reasons; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.req_reasons (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: req_reasons_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.req_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: req_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.req_reasons_id_seq OWNED BY volun.req_reasons.id;


--
-- Name: req_rejection_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.req_rejection_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    code character varying
);


--
-- Name: req_rejection_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.req_rejection_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: req_rejection_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.req_rejection_types_id_seq OWNED BY volun.req_rejection_types.id;


--
-- Name: req_status_traces; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.req_status_traces (
    id integer NOT NULL,
    req_status_id integer NOT NULL,
    request_form_id integer NOT NULL,
    manager_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: req_status_traces_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.req_status_traces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: req_status_traces_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.req_status_traces_id_seq OWNED BY volun.req_status_traces.id;


--
-- Name: req_statuses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.req_statuses (
    id integer NOT NULL,
    kind integer NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: req_statuses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.req_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: req_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.req_statuses_id_seq OWNED BY volun.req_statuses.id;


--
-- Name: request_forms; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.request_forms (
    id integer NOT NULL,
    request_type_id integer,
    rt_extendable_id integer NOT NULL,
    rt_extendable_type character varying NOT NULL,
    user_id integer,
    req_status_id integer NOT NULL,
    status_date timestamp without time zone NOT NULL,
    req_rejection_type_id integer,
    req_reason_id integer,
    manager_id integer,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT rt_extendable_must_be_consistent CHECK ((((request_type_id = 1) AND ((rt_extendable_type)::text = 'Rt::VolunteerSubscribe'::text)) OR ((request_type_id = 2) AND ((rt_extendable_type)::text = 'Rt::VolunteerUnsubscribe'::text)) OR ((request_type_id = 3) AND ((rt_extendable_type)::text = 'Rt::VolunteerAmendment'::text)) OR ((request_type_id = 4) AND ((rt_extendable_type)::text = 'Rt::VolunteerAppointment'::text)) OR ((request_type_id = 5) AND ((rt_extendable_type)::text = 'Rt::EntitySubscribe'::text)) OR ((request_type_id = 6) AND ((rt_extendable_type)::text = 'Rt::EntityUnsubscribe'::text)) OR ((request_type_id = 7) AND ((rt_extendable_type)::text = 'Rt::VolunteersDemand'::text)) OR ((request_type_id = 8) AND ((rt_extendable_type)::text = 'Rt::ProjectPublishing'::text)) OR ((request_type_id = 9) AND ((rt_extendable_type)::text = 'Rt::ProjectUnpublishing'::text)) OR ((request_type_id = 10) AND ((rt_extendable_type)::text = 'Rt::ActivityPublishing'::text)) OR ((request_type_id = 11) AND ((rt_extendable_type)::text = 'Rt::ActivityUnpublishing'::text)) OR ((request_type_id = 12) AND ((rt_extendable_type)::text = 'Rt::Other'::text)) OR ((request_type_id = 13) AND ((rt_extendable_type)::text = 'Rt::SolidarityChain'::text)) OR ((request_type_id = 14) AND ((rt_extendable_type)::text = 'Rt::VolunteerProjectSubscribe'::text))))
);


--
-- Name: request_forms_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.request_forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: request_forms_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.request_forms_id_seq OWNED BY volun.request_forms.id;


--
-- Name: request_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.request_types (
    id integer NOT NULL,
    kind integer NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    CONSTRAINT kind_and_id_must_be_equal CHECK ((((id = 1) AND (kind = 1)) OR ((id = 2) AND (kind = 2)) OR ((id = 3) AND (kind = 3)) OR ((id = 4) AND (kind = 4)) OR ((id = 5) AND (kind = 5)) OR ((id = 6) AND (kind = 6)) OR ((id = 7) AND (kind = 7)) OR ((id = 8) AND (kind = 8)) OR ((id = 9) AND (kind = 9)) OR ((id = 10) AND (kind = 10)) OR ((id = 11) AND (kind = 11)) OR ((id = 12) AND (kind = 12)) OR ((id = 13) AND (kind = 13)) OR ((id = 14) AND (kind = 14))))
);


--
-- Name: request_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.request_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: request_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.request_types_id_seq OWNED BY volun.request_types.id;


--
-- Name: resources; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.resources (
    id integer NOT NULL,
    description character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying,
    active boolean DEFAULT true,
    main boolean DEFAULT false
);


--
-- Name: resources_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.resources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: resources_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.resources_id_seq OWNED BY volun.resources.id;


--
-- Name: road_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.road_types (
    id integer NOT NULL,
    name character varying,
    code character varying,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: road_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.road_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: road_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.road_types_id_seq OWNED BY volun.road_types.id;


--
-- Name: roles; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.roles (
    id integer NOT NULL,
    kind integer,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.roles_id_seq OWNED BY volun.roles.id;


--
-- Name: rt_activity_publishings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_activity_publishings (
    id integer NOT NULL,
    name character varying,
    organizer character varying,
    description text,
    execution_date date,
    execution_hour character varying,
    road_type character varying,
    road_name character varying,
    number_type character varying,
    road_number character varying,
    postal_code character varying,
    borough character varying,
    district character varying,
    town character varying,
    province character varying,
    project_id integer,
    notes text,
    activity_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    dates_text_free character varying,
    hours_text_free character varying,
    places_text_free character varying,
    observations_text_free character varying,
    entity_id bigint
);


--
-- Name: rt_activity_publishings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_activity_publishings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_activity_publishings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_activity_publishings_id_seq OWNED BY volun.rt_activity_publishings.id;


--
-- Name: rt_activity_unpublishings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_activity_unpublishings (
    id integer NOT NULL,
    activity_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: rt_activity_unpublishings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_activity_unpublishings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_activity_unpublishings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_activity_unpublishings_id_seq OWNED BY volun.rt_activity_unpublishings.id;


--
-- Name: rt_entity_subscribes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_entity_subscribes (
    id integer NOT NULL,
    name character varying,
    description text,
    vat_number character varying,
    email character varying,
    representative_name character varying,
    representative_last_name character varying,
    representative_last_name_alt character varying,
    contact_name character varying,
    contact_last_name character varying,
    contact_last_name_alt character varying,
    phone_number character varying,
    phone_number_alt character varying,
    publish_pictures boolean DEFAULT true,
    annual_survey boolean DEFAULT false,
    entity_type_id integer,
    comments text,
    other_subscribe_reason text,
    road_type character varying,
    road_name character varying,
    road_number_type character varying,
    road_number character varying,
    postal_code character varying,
    borough character varying,
    district character varying,
    town character varying,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    province_id integer
);


--
-- Name: rt_entity_subscribes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_entity_subscribes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_entity_subscribes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_entity_subscribes_id_seq OWNED BY volun.rt_entity_subscribes.id;


--
-- Name: rt_entity_unsubscribes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_entity_unsubscribes (
    id integer NOT NULL,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: rt_entity_unsubscribes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_entity_unsubscribes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_entity_unsubscribes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_entity_unsubscribes_id_seq OWNED BY volun.rt_entity_unsubscribes.id;


--
-- Name: rt_others; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_others (
    id integer NOT NULL,
    description text,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    entity_id bigint
);


--
-- Name: rt_others_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_others_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_others_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_others_id_seq OWNED BY volun.rt_others.id;


--
-- Name: rt_project_publishings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_project_publishings (
    id integer NOT NULL,
    description text,
    road_type character varying,
    road_name character varying,
    number_type character varying,
    road_number character varying,
    postal_code character varying,
    town character varying,
    notes text,
    project_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    province_id bigint,
    district_id bigint,
    borought_id bigint,
    entity_id bigint
);


--
-- Name: rt_project_publishings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_project_publishings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_project_publishings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_project_publishings_id_seq OWNED BY volun.rt_project_publishings.id;


--
-- Name: rt_project_unpublishings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_project_unpublishings (
    id integer NOT NULL,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: rt_project_unpublishings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_project_unpublishings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_project_unpublishings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_project_unpublishings_id_seq OWNED BY volun.rt_project_unpublishings.id;


--
-- Name: rt_solidarity_chains; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_solidarity_chains (
    id integer NOT NULL,
    full_name character varying NOT NULL,
    phone_number character varying NOT NULL,
    email character varying NOT NULL,
    action_description text NOT NULL,
    url_image character varying,
    alias character varying,
    data_protection boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    url_image_file_name character varying,
    url_image_content_type character varying,
    url_image_file_size integer,
    url_image_updated_at timestamp without time zone
);


--
-- Name: rt_solidarity_chains_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_solidarity_chains_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_solidarity_chains_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_solidarity_chains_id_seq OWNED BY volun.rt_solidarity_chains.id;


--
-- Name: rt_volunteer_amendments; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteer_amendments (
    id integer NOT NULL,
    road_type character varying,
    road_name character varying,
    number_type character varying,
    road_number character varying,
    postal_code character varying,
    town character varying,
    phone_number character varying,
    phone_number_alt character varying,
    email character varying,
    notes text,
    volunteer_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    province_id bigint,
    district_id bigint,
    borought_id bigint
);


--
-- Name: rt_volunteer_amendments_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteer_amendments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteer_amendments_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteer_amendments_id_seq OWNED BY volun.rt_volunteer_amendments.id;


--
-- Name: rt_volunteer_appointments; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteer_appointments (
    id integer NOT NULL,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    volunteer_id bigint
);


--
-- Name: rt_volunteer_appointments_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteer_appointments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteer_appointments_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteer_appointments_id_seq OWNED BY volun.rt_volunteer_appointments.id;


--
-- Name: rt_volunteer_project_subscribes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteer_project_subscribes (
    id integer NOT NULL,
    name character varying,
    last_name character varying,
    last_name_alt character varying,
    phone_number character varying,
    phone_number_alt character varying,
    email character varying,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    district character varying,
    postal_code character varying,
    appointment_at timestamp without time zone,
    contact_person character varying,
    is_adult character varying DEFAULT false,
    info_source_id bigint,
    district_id integer,
    volunteer_id bigint
);


--
-- Name: rt_volunteer_project_subscribes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteer_project_subscribes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteer_project_subscribes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteer_project_subscribes_id_seq OWNED BY volun.rt_volunteer_project_subscribes.id;


--
-- Name: rt_volunteer_subscribes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteer_subscribes (
    id integer NOT NULL,
    name character varying,
    last_name character varying,
    last_name_alt character varying,
    phone_number character varying,
    phone_number_alt character varying,
    email character varying,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    postal_code integer,
    info_source_id integer,
    "Learn_service" character varying,
    appointment_at timestamp without time zone,
    contact_person character varying,
    is_adult character varying DEFAULT false,
    district_id bigint,
    channel_id bigint DEFAULT 1
);


--
-- Name: rt_volunteer_subscribes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteer_subscribes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteer_subscribes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteer_subscribes_id_seq OWNED BY volun.rt_volunteer_subscribes.id;


--
-- Name: rt_volunteer_unsubscribes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteer_unsubscribes (
    id integer NOT NULL,
    unsubscribe_level_id integer,
    project_id integer,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: rt_volunteer_unsubscribes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteer_unsubscribes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteer_unsubscribes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteer_unsubscribes_id_seq OWNED BY volun.rt_volunteer_unsubscribes.id;


--
-- Name: rt_volunteers_demands; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.rt_volunteers_demands (
    id integer NOT NULL,
    description text,
    execution_start_date date,
    execution_end_date date,
    road_type character varying,
    road_name character varying,
    number_type character varying,
    road_number character varying,
    postal_code character varying,
    town character varying,
    requested_volunteers_num character varying,
    volunteers_profile text,
    volunteer_functions_1 text,
    volunteer_functions_2 text,
    volunteer_functions_3 text,
    notes text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    project_id integer,
    volunteer_functions_4 text,
    volunteer_functions_5 text,
    province_id bigint,
    district_id bigint,
    borought_id bigint,
    entity_id bigint
);


--
-- Name: rt_volunteers_demands_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.rt_volunteers_demands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rt_volunteers_demands_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.rt_volunteers_demands_id_seq OWNED BY volun.rt_volunteers_demands.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: search_forms; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.search_forms (
    id integer NOT NULL,
    user_id integer,
    content json,
    model_type text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: search_forms_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.search_forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_forms_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.search_forms_id_seq OWNED BY volun.search_forms.id;


--
-- Name: sectors; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.sectors (
    id integer NOT NULL,
    name character varying,
    active character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: sectors_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.sectors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sectors_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.sectors_id_seq OWNED BY volun.sectors.id;


--
-- Name: service_polls; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.service_polls (
    id integer NOT NULL,
    citizen_service_id integer,
    live_alone_question boolean,
    go_out_question boolean,
    dificulties_out_question boolean,
    need_help_question boolean,
    need_alone_question boolean,
    observations character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: service_polls_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.service_polls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_polls_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.service_polls_id_seq OWNED BY volun.service_polls.id;


--
-- Name: service_questions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.service_questions (
    id integer NOT NULL,
    description character varying,
    question_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    active boolean DEFAULT false
);


--
-- Name: service_questions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.service_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.service_questions_id_seq OWNED BY volun.service_questions.id;


--
-- Name: service_responses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.service_responses (
    id integer NOT NULL,
    citizen_service_id integer,
    service_question_id integer,
    response character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer,
    user_type character varying
);


--
-- Name: service_responses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.service_responses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_responses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.service_responses_id_seq OWNED BY volun.service_responses.id;


--
-- Name: service_votes; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.service_votes (
    id integer NOT NULL,
    vote integer,
    citizen_service_id integer,
    type_user character varying,
    comments character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: service_votes_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.service_votes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: service_votes_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.service_votes_id_seq OWNED BY volun.service_votes.id;


--
-- Name: services; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.services (
    id integer NOT NULL,
    code character varying,
    description character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name character varying,
    active boolean,
    is_digital boolean DEFAULT false,
    mta_order integer DEFAULT 0,
    normal_flow boolean DEFAULT false
);


--
-- Name: services_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.services_id_seq OWNED BY volun.services.id;


--
-- Name: settings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.settings (
    id integer NOT NULL,
    key character varying,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    active boolean DEFAULT true
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.settings_id_seq OWNED BY volun.settings.id;


--
-- Name: shift_definitions; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.shift_definitions (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    active boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    projects_volunteer_id integer
);


--
-- Name: shift_definitions_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.shift_definitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shift_definitions_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.shift_definitions_id_seq OWNED BY volun.shift_definitions.id;


--
-- Name: skills; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.skills (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: skills_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.skills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: skills_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.skills_id_seq OWNED BY volun.skills.id;


--
-- Name: skills_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.skills_volunteers (
    skill_id integer NOT NULL,
    volunteer_id integer NOT NULL
);


--
-- Name: status_services; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.status_services (
    id bigint NOT NULL,
    name character varying NOT NULL,
    code character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: status_services_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.status_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: status_services_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.status_services_id_seq OWNED BY volun.status_services.id;


--
-- Name: statuses; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.statuses (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: statuses_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.statuses_id_seq OWNED BY volun.statuses.id;


--
-- Name: timetables; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.timetables (
    id integer NOT NULL,
    event_id integer NOT NULL,
    execution_date date NOT NULL,
    start_hour character varying NOT NULL,
    end_hour character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: timetables_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.timetables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: timetables_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.timetables_id_seq OWNED BY volun.timetables.id;


--
-- Name: tracking_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.tracking_types (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    alias_name character varying,
    system boolean DEFAULT false,
    tracking_types_type character varying
);


--
-- Name: tracking_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.tracking_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tracking_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.tracking_types_id_seq OWNED BY volun.tracking_types.id;


--
-- Name: traits; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.traits (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: traits_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.traits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: traits_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.traits_id_seq OWNED BY volun.traits.id;


--
-- Name: type_meetings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.type_meetings (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    active boolean DEFAULT true,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: type_meetings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.type_meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: type_meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.type_meetings_id_seq OWNED BY volun.type_meetings.id;


--
-- Name: unsubscribe_levels; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.unsubscribe_levels (
    id integer NOT NULL,
    kind integer NOT NULL,
    description text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: unsubscribe_levels_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.unsubscribe_levels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unsubscribe_levels_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.unsubscribe_levels_id_seq OWNED BY volun.unsubscribe_levels.id;


--
-- Name: unsubscribe_reasons; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.unsubscribe_reasons (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    code character varying
);


--
-- Name: unsubscribe_reasons_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.unsubscribe_reasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unsubscribe_reasons_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.unsubscribe_reasons_id_seq OWNED BY volun.unsubscribe_reasons.id;


--
-- Name: users; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.users (
    id integer NOT NULL,
    login character varying,
    locale character varying,
    notice_type_id integer,
    loggable_id integer,
    loggable_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    email character varying DEFAULT ''::character varying,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    terms_of_service boolean DEFAULT false,
    mobile_phone character varying,
    authentication_token text,
    authentication_token_created_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.users_id_seq OWNED BY volun.users.id;


--
-- Name: view_list_activities; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_activities AS
 SELECT a.id,
    a.name,
    a.description,
    a.active,
    a.start_date,
    a.end_date,
    a.transport,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = a.entity_id)) AS entity,
    ( SELECT p.name
           FROM volun.projects p
          WHERE (p.id = a.project_id)) AS project,
    a.share,
    ( SELECT string_agg((ac.name)::text, ' ; '::text) AS string_agg
           FROM volun.areas ac,
            volun.activities_areas aa
          WHERE ((aa.area_id = ac.id) AND (aa.activity_id = a.id))) AS get_areas,
    a.publish,
    a.pdf_url,
    a.review_end_activity,
    a.entity_id,
    a.area_id
   FROM volun.activities a;


--
-- Name: view_list_areas; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_areas AS
 SELECT areas.id,
    areas.name,
    areas.active
   FROM volun.areas;


--
-- Name: view_list_boroughts; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_boroughts AS
 SELECT b.id,
    b.name,
    b.code,
    ( SELECT d.code
           FROM volun.districts d
          WHERE (d.id = b.district_id)) AS district_code,
    b.active
   FROM volun.boroughts b;


--
-- Name: view_list_channels; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_channels AS
 SELECT channels.id,
    channels.name,
    channels.code,
    channels.active
   FROM volun.channels;


--
-- Name: view_list_citizen_service_trackings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_citizen_service_trackings AS
 SELECT cst.id,
    cst.automatic,
    cst.coments,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (cst.manager_id = m.id)) AS manager,
    cst.tracked_at,
    ( SELECT tt.name
           FROM volun.tracking_types tt
          WHERE (tt.id = cst.tracking_type_id)) AS tracking_type,
    ( SELECT concat('Servicio (', cs.id, ') - ', ( SELECT s.name
                   FROM volun.services s
                  WHERE (s.id = cs.service_id))) AS concat
           FROM volun.citizen_services cs
          WHERE (cs.id = cst.citizen_service_id)) AS citizen_service,
    ( SELECT upper(concat(c.name, ' ', c.last_name_1, ' ', c.last_name_2)) AS upper
           FROM volun.citizens c,
            volun.citizen_services cs2
          WHERE ((cs2.citizen_id = c.id) AND (cst.citizen_service_id = cs2.id))) AS citizen,
    cst.citizen_service_id
   FROM volun.citizen_service_trackings cst;


--
-- Name: volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volunteers (
    id integer NOT NULL,
    name character varying NOT NULL,
    last_name character varying NOT NULL,
    last_name_alt character varying,
    document_type_id integer,
    id_number character varying,
    birth_date date,
    nationality_id integer,
    phone_number character varying,
    phone_number_alt character varying,
    email character varying,
    address_id integer,
    status_id integer,
    employment_status_id integer,
    vocne boolean DEFAULT false,
    available boolean DEFAULT false,
    availability_date date,
    academic_level_id integer,
    subscribe_date date,
    unsubscribe_date date,
    unsubscribe_reason_id integer,
    comments text,
    expectations text,
    agreement boolean DEFAULT false,
    agreement_date timestamp without time zone,
    search_authorization boolean DEFAULT false,
    representative_statement boolean DEFAULT false,
    has_driving_license boolean DEFAULT false,
    publish_pictures boolean DEFAULT true,
    annual_survey boolean DEFAULT false,
    subscribed_at timestamp without time zone,
    manager_id integer,
    info_source_id integer,
    other_academic_info text,
    error_address text,
    error_other text,
    review integer,
    profession_id integer,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    notes character varying,
    accompany_volunteer boolean DEFAULT false,
    cession_of_personal_data boolean DEFAULT true,
    publication_image_ayto boolean DEFAULT true,
    publication_image_ooaa boolean DEFAULT true,
    publication_image_social_network boolean DEFAULT true,
    publication_image_media boolean DEFAULT false,
    criminal_certificate boolean DEFAULT false,
    retired boolean DEFAULT false,
    ec boolean DEFAULT false,
    next_point_id integer,
    code_ci360 integer,
    code_cid360_autentication character varying,
    validation_cid360 character varying,
    id_autonum integer,
    market boolean DEFAULT false,
    tester boolean DEFAULT false,
    is_adult character varying DEFAULT false,
    gender_id bigint,
    is_death boolean DEFAULT false,
    padron_reason character varying
);


--
-- Name: view_list_citizen_services; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_citizen_services AS
 SELECT cs.id,
    upper((c.name)::text) AS f_citizen_name,
    c.id AS f_citizen_id,
    upper((c.last_name_1)::text) AS f_citizen_last_name_1,
    upper((c.last_name_2)::text) AS f_citizen_last_name_2,
    ( SELECT v.id
           FROM volun.volunteers v
          WHERE (v.id = cs.volunteer_id)) AS f_volunteer_id,
    ( SELECT upper((v.name)::text) AS upper
           FROM volun.volunteers v
          WHERE (v.id = cs.volunteer_id)) AS f_volunteer_name,
    ( SELECT upper((v.last_name)::text) AS upper
           FROM volun.volunteers v
          WHERE (v.id = cs.volunteer_id)) AS f_volunteer_last_name_1,
    ( SELECT upper((v.last_name_alt)::text) AS upper
           FROM volun.volunteers v
          WHERE (v.id = cs.volunteer_id)) AS f_volunteer_last_name_2,
    ( SELECT COALESCE(d.name, 'OTROS'::character varying) AS "coalesce"
           FROM volun.volunteers v,
            volun.addresses a,
            volun.districts d
          WHERE ((v.id = cs.volunteer_id) AND (a.id = v.address_id) AND (a.district_id = d.id))) AS f_volunteer_district,
    ( SELECT s.name
           FROM volun.services s
          WHERE (s.id = cs.service_id)) AS type_service,
        CASE
            WHEN (cs.date_request IS NOT NULL) THEN (concat(cs.date_request, ' ', cs.hour_request))::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_request,
        CASE
            WHEN (cs.date_accept_request IS NOT NULL) THEN (concat(cs.date_accept_request, ' ', cs.hour_accept_request))::timestamp without time zone
            ELSE NULL::timestamp without time zone
        END AS full_date_accept,
    ( SELECT ss.name
           FROM volun.status_services ss
          WHERE ((ss.id = cs.status_service_id) OR ((ss.code)::text = (cs.status)::text))
         LIMIT 1) AS status_name,
    ( SELECT sv.vote
           FROM volun.service_votes sv
          WHERE ((sv.citizen_service_id = cs.id) AND ((sv.type_user)::text = 'Citizen'::text))) AS f_citizen_vote,
    ( SELECT sv.vote
           FROM volun.service_votes sv
          WHERE ((sv.citizen_service_id = cs.id) AND ((sv.type_user)::text = 'Volunteer'::text))) AS f_volunteer_vote,
    cs.canceled_at,
    cs.citizen_reason,
    cs.rejected_at,
    cs.volunteer_reason,
    cs.place,
    cs.additional_data,
    ( SELECT b.name
           FROM volun.boroughts b
          WHERE (b.id = cs.borought_id)) AS borought_name,
    ( SELECT d.name
           FROM volun.districts d
          WHERE (d.id = cs.district_id)) AS district_name,
    cs.citizen_id,
    cs.volunteer_id,
    cs.status,
    ( SELECT v.email
           FROM volun.volunteers v
          WHERE (v.id = cs.volunteer_id)) AS f_volunteer_email,
    c.email AS f_citizen_email,
    cs.date_accept_request,
    cs.date_request,
    cs.active,
    cs.district_id,
    cs.borought_id,
    cs.service_id,
    (date_part('year'::text, age(((c.birthday)::date)::timestamp with time zone)))::integer AS age_citizen,
    c.gender_id,
    ( SELECT g.name
           FROM volun.genders g
          WHERE (g.id = c.gender_id)) AS gender_name,
    c.enabled_app,
    c.tester,
    c.email_verified,
    cs.generated_gi,
    ( SELECT COALESCE(v.id_autonum, v.id) AS "coalesce"
           FROM volun.volunteers v
          WHERE (cs.volunteer_id = v.id)) AS f_volunteer_id_autonum,
    ( SELECT s.is_digital
           FROM volun.services s
          WHERE (s.id = cs.service_id)) AS is_digital
   FROM volun.citizen_services cs,
    volun.citizens c
  WHERE (cs.citizen_id = c.id);


--
-- Name: view_list_citizen_trackings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_citizen_trackings AS
 SELECT et.id,
    ( SELECT upper(concat(e.name, ' ', e.last_name_1, ' ', e.last_name_2)) AS upper
           FROM volun.citizens e
          WHERE (e.id = et.citizen_id)) AS citizen,
    et.citizen_id,
    et.tracked_at,
    et.automatic,
    et.coments,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (et.manager_id = m.id)) AS manager,
    et.manager_id,
    ( SELECT tt.name
           FROM volun.tracking_types tt
          WHERE (tt.id = et.tracking_type_id)) AS tracking_type,
    et.tracking_type_id
   FROM volun.citizen_trackings et;


--
-- Name: view_list_citizens; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_citizens AS
 SELECT c.id,
    c.name,
    c.last_name_1,
    c.last_name_2,
    c.phone,
    c.email,
    c.document,
    c.document_type_id,
    c.birthday,
    c.country,
    c.academic_info,
    c.info_source_id,
    c.people_at_home,
    c.start_date,
    c.last_sign_in,
    c.code_ci360,
    c.created_at,
    c.updated_at,
    c.address_id,
    c.terms_of_service,
    c.access_token,
    c.authentication_token,
    c.authentication_token_created_at,
    c.code_cid360_autentication,
    c.validation_cid360,
    c.active,
    c.email_verified,
    c.unsubscribed,
    c.market,
    c.email_confirmed,
    c.confirm_token,
    c.tester,
    c.padron,
    c.legal_msg,
    c.validated_at,
    c.unsubscribed_at,
    c.internal_management,
    c.enabled_app,
    c.removal_notice,
    c.removal_notice_at,
    c.gender_id,
    c.province_id,
    c.nationality_id,
    c.average_vote,
    c.country_code,
    c.is_death,
    c.padron_reason,
    (date_part('year'::text, age(((c.birthday)::date)::timestamp with time zone)))::integer AS age_name,
    ( SELECT string_agg(DISTINCT (s.name)::text, ' ; '::text) AS string_agg
           FROM volun.citizen_services cs,
            volun.services s
          WHERE ((cs.service_id = s.id) AND (cs.citizen_id = c.id))) AS type_services,
    ( SELECT count(1) AS count
           FROM volun.citizen_services cs
          WHERE (cs.citizen_id = c.id)) AS count_services,
    ( SELECT count(1) AS count
           FROM volun.citizen_services cs
          WHERE ((cs.citizen_id = c.id) AND (cs.status_service_id IN ( SELECT ss.id
                   FROM volun.status_services ss
                  WHERE ((ss.code)::text = 'finished'::text))))) AS count_services_finish,
    ( SELECT count(1) AS count
           FROM volun.citizen_services cs
          WHERE ((cs.citizen_id = c.id) AND (cs.status_service_id IN ( SELECT ss.id
                   FROM volun.status_services ss
                  WHERE ((ss.code)::text = ANY (ARRAY[('prioritary'::character varying)::text, ('active'::character varying)::text])))))) AS count_services_pending,
    ( SELECT count(1) AS count
           FROM volun.citizen_services cs
          WHERE ((cs.citizen_id = c.id) AND (cs.status_service_id IN ( SELECT ss.id
                   FROM volun.status_services ss
                  WHERE ((ss.code)::text = 'accept'::text))))) AS count_services_accept,
    ( SELECT string_agg(DISTINCT ((cs.service_id)::character varying)::text, ','::text) AS string_agg
           FROM volun.citizen_services cs
          WHERE (cs.citizen_id = c.id)) AS service_id,
    ( SELECT g.name
           FROM volun.genders g
          WHERE (g.id = c.gender_id)) AS gender,
    ( SELECT a.district_id
           FROM volun.addresses a
          WHERE (a.id = c.address_id)) AS district_id
   FROM volun.citizens c;


--
-- Name: view_list_collectives; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_collectives AS
 SELECT collectives.id,
    collectives.name,
    collectives.active
   FROM volun.collectives;


--
-- Name: view_list_coordinations; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_coordinations AS
 SELECT coordinations.id,
    coordinations.name,
    coordinations.active
   FROM volun.coordinations;


--
-- Name: view_list_degrees; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_degrees AS
 SELECT d.id,
    ( SELECT dt.name
           FROM volun.degree_types dt
          WHERE (dt.id = d.degree_type_id)) AS degree_type,
    d.name,
    d.active
   FROM volun.degrees d;


--
-- Name: volun_profiles; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_profiles (
    id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_profiles_volunteers; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_profiles_volunteers (
    volunteer_id integer NOT NULL,
    volun_profile_id integer NOT NULL
);


--
-- Name: view_list_derivaties; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_derivaties AS
 SELECT pv.project_id,
    pv.active AS pv_active,
    pv.volunteer_type,
    pv.volunteer_id,
    v.name,
    v.last_name,
    v.last_name_alt,
    v.document_type_id,
    v.id_number,
    v.birth_date,
    v.nationality_id,
    v.phone_number,
    v.phone_number_alt,
    v.email,
    v.address_id,
    v.status_id,
    v.employment_status_id,
    v.vocne,
    v.available,
    v.availability_date,
    v.academic_level_id,
    v.subscribe_date,
    v.unsubscribe_date,
    v.unsubscribe_reason_id,
    v.comments,
    v.expectations,
    v.agreement,
    v.agreement_date,
    v.search_authorization,
    v.representative_statement,
    v.has_driving_license,
    v.publish_pictures,
    v.annual_survey,
    v.subscribed_at,
    v.manager_id,
    v.info_source_id,
    v.other_academic_info,
    v.error_address,
    v.error_other,
    v.review,
    v.profession_id,
    v.active,
    v.created_at,
    v.updated_at,
    v.notes,
    v.accompany_volunteer,
    v.cession_of_personal_data,
    v.publication_image_ayto,
    v.publication_image_ooaa,
    v.publication_image_social_network,
    v.publication_image_media,
    v.criminal_certificate,
    v.retired,
    v.ec,
    v.next_point_id,
    v.code_ci360,
    v.code_cid360_autentication,
    v.validation_cid360,
    v.id_autonum,
    v.market,
    v.tester,
    v.is_adult,
    v.gender_id,
    v.is_death,
    v.padron_reason,
    (date_part('year'::text, age((v.birth_date)::timestamp with time zone)))::integer AS age_name,
    ( SELECT g.name
           FROM volun.genders g
          WHERE (g.id = v.gender_id)) AS gender_convert,
    ( SELECT a.town
           FROM volun.addresses a
          WHERE (a.id = v.address_id)) AS town,
    ( SELECT a.postal_code
           FROM volun.addresses a
          WHERE (a.id = v.address_id)) AS postal_code,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.districts d
          WHERE ((a.id = v.address_id) AND (d.id = a.district_id))), 'OTROS'::character varying) AS district_custom,
    ( SELECT count(1) AS count
           FROM volun.projects_volunteers pv_1
          WHERE ((pv_1.active = true) AND (v.id = pv_1.volunteer_id))) AS num_projects_active,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.districts d
          WHERE ((d.id = a.district_id) AND (a.id = v.address_id))), 'OTROS'::character varying) AS district,
    COALESCE(( SELECT (d.code)::integer AS code
           FROM volun.addresses a,
            volun.districts d
          WHERE ((d.id = a.district_id) AND (a.id = v.address_id))), 22) AS district_id,
        CASE
            WHEN ((v.active = true) AND (v.available = true) AND (v.availability_date IS NOT NULL) AND (v.availability_date <= ((now() + '7 days'::interval))::date)) THEN true
            ELSE false
        END AS notice_available_column,
        CASE
            WHEN (((v.birth_date IS NOT NULL) AND ((date_part('year'::text, age((v.birth_date)::timestamp with time zone)))::integer < 18)) OR (v.cession_of_personal_data = false) OR (v.publication_image_ayto = false) OR (v.publication_image_ooaa = false) OR (v.publication_image_social_network = false) OR (v.publication_image_media = false) OR (v.vocne = true) OR COALESCE(( SELECT (upper((s.name)::text) = 'NO APTO'::text)
               FROM volun.statuses s
              WHERE (s.id = v.status_id)), false)) THEN true
            ELSE false
        END AS not_autorized,
    ( SELECT string_agg((a2.name)::text, ','::text) AS string_agg
           FROM volun.areas a2,
            volun.areas_volunteers av
          WHERE ((av.area_id = a2.id) AND (av.volunteer_id = v.id))) AS get_areas,
    ( SELECT string_agg((c.name)::text, ','::text) AS string_agg
           FROM volun.collectives c,
            volun.collectives_volunteers cv
          WHERE ((cv.collective_id = c.id) AND (cv.volunteer_id = v.id))) AS get_collectives,
    ( SELECT string_agg((is2.name)::text, ','::text) AS string_agg
           FROM volun.info_sources is2
          WHERE (is2.id = v.info_source_id)) AS get_info_source,
    ( SELECT string_agg((vp.name)::text, ','::text) AS string_agg
           FROM volun.volun_profiles vp,
            volun.volun_profiles_volunteers vpv
          WHERE ((vpv.volun_profile_id = vp.id) AND (vpv.volunteer_id = v.id))) AS get_volun_profiles,
    to_char((v.subscribe_date)::timestamp with time zone, 'DD/MM/YYYY HH24:MI'::text) AS subscribe_date_format,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.boroughts d
          WHERE ((d.id = a.borought_id) AND (a.id = v.address_id))), 'OTROS'::character varying) AS borought,
    COALESCE(( SELECT (d.code)::integer AS code
           FROM volun.addresses a,
            volun.boroughts d
          WHERE ((d.id = a.borought_id) AND (a.id = v.address_id))), 22) AS borought_id,
    pv.day,
    pv.id,
    pv.shift_definition_id
   FROM volun.projects_volunteers pv,
    volun.volunteers v
  WHERE (pv.volunteer_id = v.id);


--
-- Name: view_list_digital_faqs; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_digital_faqs AS
 SELECT df.id,
    df.project_id,
    df.title,
    df.description,
    df."order",
    df.active
   FROM volun.digital_faqs df;


--
-- Name: view_list_digital_topics; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_digital_topics AS
 SELECT dt.id,
    dt.project_id,
    dt.title,
    dt.body,
    dt.active,
    ( SELECT count(1) AS count
           FROM volun.digital_comments dc
          WHERE (dc.digital_topic_id = dt.id)) AS n_comments,
    ( SELECT count(1) AS count
           FROM volun.digital_follows df
          WHERE (df.digital_topic_id = dt.id)) AS n_follows,
    ( SELECT sum(dv.value) AS sum
           FROM volun.digital_votes dv
          WHERE ((dv.votable_id = dt.id) AND ((dv.votable_type)::text = 'Digital::Topic'::text))) AS votes
   FROM volun.digital_topics dt;


--
-- Name: view_list_districts; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_districts AS
 SELECT districts.id,
    districts.name,
    districts.code,
    districts.active
   FROM volun.districts;


--
-- Name: view_list_ent_trackings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_ent_trackings AS
 SELECT et.id,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = et.entity_id)) AS entity,
    et.entity_id,
    et.tracked_at,
    et.automatic,
    et.comments,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (et.manager_id = m.id)) AS manager,
    et.manager_id,
    et.request_form_id,
    ( SELECT rf.id
           FROM volun.request_forms rf
          WHERE (rf.id = et.request_form_id)) AS request_form,
    ( SELECT tt.name
           FROM volun.tracking_types tt
          WHERE (tt.id = et.tracking_type_id)) AS tracking_type,
    et.tracking_type_id
   FROM volun.ent_trackings et;


--
-- Name: view_list_entities; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_entities AS
 SELECT e.id,
    e.name,
    e.vat_number,
    e.email,
    ( SELECT et.name
           FROM volun.entity_types et
          WHERE (et.id = e.entity_type_id)) AS entity_type,
    e.active,
    e.entity_type_id
   FROM volun.entities e;


--
-- Name: view_list_genders; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_genders AS
 SELECT genders.id,
    genders.name,
    genders.active
   FROM volun.genders;


--
-- Name: view_list_info_sources; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_info_sources AS
 SELECT info_sources.id,
    info_sources.name,
    info_sources.active
   FROM volun.info_sources;


--
-- Name: view_list_languages; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_languages AS
 SELECT languages.id,
    languages.name,
    languages.active
   FROM volun.languages;


--
-- Name: view_list_managers; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_managers AS
 SELECT m.id,
    m.name,
    m.last_name,
    m.last_name_alt,
    m.phone_number,
    m.login,
    m.active,
    ( SELECT
                CASE r.description
                    WHEN 'super_admin'::text THEN 'Super Administrador'::text
                    WHEN 'admin'::text THEN 'Administrador'::text
                    WHEN 'internal_staff'::text THEN 'Personal interno'::text
                    WHEN 'external_staff'::text THEN 'Personal externo'::text
                    WHEN 'special_trust'::text THEN 'Especial confianza'::text
                    WHEN 'digital_manage'::text THEN 'Gestión digital moderador'::text
                    ELSE ''::text
                END AS "case"
           FROM volun.roles r
          WHERE (r.id = m.role_id)) AS role,
    m.alias_name
   FROM volun.managers m;


--
-- Name: view_list_moderate_statuses; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_moderate_statuses AS
 SELECT moderate_statuses.id,
    moderate_statuses.title,
    moderate_statuses.code,
    moderate_statuses.active
   FROM volun.moderate_statuses;


--
-- Name: view_list_nationalities; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_nationalities AS
 SELECT nationalities.id,
    nationalities.name,
    nationalities.active
   FROM volun.nationalities;


--
-- Name: view_list_new_campaings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_new_campaings AS
 SELECT new_campaings.id,
    new_campaings.title,
    new_campaings.body,
    new_campaings.publicable
   FROM volun.new_campaings;


--
-- Name: view_list_next_points; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_next_points AS
 SELECT next_points.id,
    next_points.name,
    next_points.description,
    next_points.active
   FROM volun.next_points;


--
-- Name: view_list_pro_trackings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_pro_trackings AS
 SELECT et.id,
    et.project_id,
    et.tracked_at,
    et.automatic,
    et.comments,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (et.manager_id = m.id)) AS manager,
    ( SELECT p.name
           FROM volun.projects p
          WHERE (et.project_id = p.id)) AS project,
    et.manager_id,
    ( SELECT tt.name
           FROM volun.tracking_types tt
          WHERE (tt.id = et.tracking_type_id)) AS tracking_type,
    ( SELECT rf.id
           FROM volun.request_forms rf
          WHERE (rf.id = et.request_form_id)) AS request_form,
    et.request_form_id,
    et.tracking_type_id
   FROM volun.pro_trackings et;


--
-- Name: view_list_professions; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_professions AS
 SELECT professions.id,
    professions.name,
    professions.active
   FROM volun.professions;


--
-- Name: view_list_provinces; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_provinces AS
 SELECT provinces.id,
    provinces.name,
    provinces.code,
    provinces.active
   FROM volun.provinces;


--
-- Name: view_list_req_reasons; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_req_reasons AS
 SELECT req_reasons.id,
    req_reasons.name,
    req_reasons.description,
    req_reasons.active
   FROM volun.req_reasons;


--
-- Name: view_list_req_rejection_types; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_req_rejection_types AS
 SELECT req_rejection_types.id,
    req_rejection_types.name,
    req_rejection_types.description,
    req_rejection_types.active,
    req_rejection_types.code
   FROM volun.req_rejection_types;


--
-- Name: view_list_resources; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_resources AS
 SELECT resources.id,
    resources.name,
    resources.description,
    resources.active,
    resources.main
   FROM volun.resources;


--
-- Name: view_list_rt_activity_publishings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_activity_publishings AS
 SELECT vs.id,
    vs.name,
    vs.organizer,
    vs.description,
    vs.execution_date,
    vs.execution_hour,
    vs.road_type,
    vs.road_name,
    vs.number_type,
    vs.road_number,
    vs.postal_code,
    vs.borough,
    vs.district,
    vs.town,
    vs.province,
    vs.project_id,
    vs.notes,
    vs.activity_id,
    vs.created_at,
    vs.updated_at,
    vs.dates_text_free,
    vs.hours_text_free,
    vs.places_text_free,
    vs.observations_text_free,
    vs.entity_id,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT
                CASE
                    WHEN ((e.phone_number)::text ~ '[6|7]\d{8}'::text) THEN e.phone_number
                    ELSE e.phone_number_alt
                END AS phone_number_alt
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_phone_number,
    ( SELECT e.email
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_email,
    ( SELECT to_char(a.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.activities a
          WHERE (a.id = vs.activity_id)) AS publishing_date_activity
   FROM volun.rt_activity_publishings vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::ActivityPublishing'::text));


--
-- Name: view_list_rt_others; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_others AS
 SELECT vs.id,
    vs.description,
    vs.notes,
    vs.created_at,
    vs.updated_at,
    vs.entity_id,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT
                CASE
                    WHEN ((e.phone_number)::text ~ '[6|7]\d{8}'::text) THEN e.phone_number
                    ELSE e.phone_number_alt
                END AS phone_number_alt
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_phone_number,
    ( SELECT e.email
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_email
   FROM volun.rt_others vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::Other'::text));


--
-- Name: view_list_rt_project_publishings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_project_publishings AS
 SELECT vs.id,
    vs.description,
    vs.road_type,
    vs.road_name,
    vs.number_type,
    vs.road_number,
    vs.postal_code,
    vs.town,
    vs.notes,
    vs.project_id,
    vs.created_at,
    vs.updated_at,
    vs.province_id,
    vs.district_id,
    vs.borought_id,
    vs.entity_id,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT
                CASE
                    WHEN ((e.phone_number)::text ~ '[6|7]\d{8}'::text) THEN e.phone_number
                    ELSE e.phone_number_alt
                END AS phone_number_alt
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_phone_number,
    ( SELECT e.email
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_email,
    ( SELECT to_char(p.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.projects p
          WHERE (p.id = vs.project_id)) AS publish_date_project,
    concat(vs.road_type, '/', vs.road_name, ', ', vs.number_type, vs.road_number, ', (', ( SELECT p.name
           FROM volun.provinces p
          WHERE (p.id = vs.province_id)), ') ', vs.town, ', ', ( SELECT b.name
           FROM volun.boroughts b
          WHERE (b.id = vs.borought_id)), ', ', ( SELECT d.name
           FROM volun.districts d
          WHERE (d.id = vs.district_id)), ', ', vs.postal_code) AS address,
    ( SELECT p.name
           FROM volun.provinces p
          WHERE (p.id = vs.province_id)) AS province
   FROM volun.rt_project_publishings vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::ProjectPublishing'::text));


--
-- Name: view_list_rt_volunteer_amendments; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_volunteer_amendments AS
 SELECT vs.id,
    vs.road_type,
    vs.road_name,
    vs.number_type,
    vs.road_number,
    vs.postal_code,
    vs.town,
    vs.phone_number,
    vs.phone_number_alt,
    vs.email,
    vs.notes,
    vs.volunteer_id,
    vs.created_at,
    vs.updated_at,
    vs.province_id,
    vs.district_id,
    vs.borought_id,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT v.name
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS name,
    ( SELECT v.last_name
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS last_name,
    ( SELECT v.last_name_alt
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS last_name_alt,
    COALESCE(( SELECT d.name
           FROM volun.volunteers v,
            volun.addresses a,
            volun.districts d
          WHERE ((v.id = vs.volunteer_id) AND (a.id = v.address_id) AND (d.id = a.district_id))), 'OTROS'::character varying) AS district,
    ( SELECT concat(v.name, ' ', v.last_name, ' ', v.last_name_alt) AS concat
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS volunteer_name,
    ( SELECT to_char(v.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS volunteer_subscribe_date,
    ( SELECT COALESCE(v.id_autonum, v.id) AS "coalesce"
           FROM volun.volunteers v
          WHERE (vs.volunteer_id = v.id)) AS id_autonum
   FROM volun.rt_volunteer_amendments vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::VolunteerAmendment'::text));


--
-- Name: view_list_rt_volunteer_appointments; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_volunteer_appointments AS
 SELECT vs.id,
    vs.notes,
    vs.created_at,
    vs.updated_at,
    vs.volunteer_id,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT v.name
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS name,
    ( SELECT v.last_name
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS last_name,
    ( SELECT v.last_name_alt
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS last_name_alt,
    ( SELECT v.phone_number
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS phone_number,
    ( SELECT v.phone_number_alt
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS phone_number_alt,
    ( SELECT v.email
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS email,
    ( SELECT a.postal_code
           FROM volun.volunteers v,
            volun.addresses a
          WHERE ((v.id = vs.volunteer_id) AND (a.id = v.address_id))) AS postal_code,
    COALESCE(( SELECT d.name
           FROM volun.volunteers v,
            volun.addresses a,
            volun.districts d
          WHERE ((v.id = vs.volunteer_id) AND (a.id = v.address_id) AND (d.id = a.district_id))), 'OTROS'::character varying) AS district,
    ( SELECT concat(v.name, ' ', v.last_name, ' ', v.last_name_alt) AS concat
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS volunteer_name,
    ( SELECT to_char(v.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS volunteer_subscribe_date
   FROM volun.rt_volunteer_appointments vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::VolunteerAppointment'::text));


--
-- Name: view_list_rt_volunteer_project_subscribes; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_volunteer_project_subscribes AS
 SELECT vs.id,
    vs.name,
    vs.last_name,
    vs.last_name_alt,
    vs.phone_number,
    vs.phone_number_alt,
    vs.email,
    vs.project_id,
    vs.notes,
    vs.created_at,
    vs.updated_at,
    vs.district,
    vs.postal_code,
    vs.appointment_at,
    vs.contact_person,
    vs.is_adult,
    vs.info_source_id,
    vs.district_id,
    vs.volunteer_id,
    ( SELECT i.name
           FROM volun.info_sources i
          WHERE (i.id = vs.info_source_id)) AS info_source,
    ( SELECT p.name
           FROM volun.projects p
          WHERE (p.id = vs.project_id)) AS project,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN ((vs.phone_number)::text ~ '[6|7]\d{8}'::text) THEN vs.phone_number
            ELSE vs.phone_number_alt
        END AS phone_number_specific,
    ( SELECT to_char(u.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.users u
          WHERE (u.id = rf.user_id)) AS volunteer_subscribe_date,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT to_char(v.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.volunteers v
          WHERE (v.id = vs.volunteer_id)) AS volunteer_subscribe_date_form,
    COALESCE(( SELECT d.name
           FROM volun.volunteers v,
            volun.addresses a,
            volun.districts d
          WHERE ((v.id = vs.volunteer_id) AND (a.id = v.address_id) AND (d.id = a.district_id))), 'OTROS'::character varying) AS district_name
   FROM volun.rt_volunteer_project_subscribes vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::VolunteerProjectSubscribe'::text));


--
-- Name: view_list_rt_volunteer_subscribes; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_volunteer_subscribes AS
 SELECT vs.id,
    vs.name,
    vs.last_name,
    vs.last_name_alt,
    vs.phone_number,
    vs.phone_number_alt,
    vs.email,
    vs.project_id,
    vs.notes,
    vs.created_at,
    vs.updated_at,
    vs.postal_code,
    vs.info_source_id,
    vs."Learn_service",
    vs.appointment_at,
    vs.contact_person,
    vs.is_adult,
    vs.district_id,
    vs.channel_id,
    COALESCE(( SELECT d.name
           FROM volun.districts d
          WHERE (d.id = vs.district_id)), 'OTROS'::character varying) AS district,
    ( SELECT c.name
           FROM volun.channels c
          WHERE (c.id = vs.channel_id)) AS channel,
    ( SELECT i.name
           FROM volun.info_sources i
          WHERE (i.id = vs.info_source_id)) AS info_source,
    ( SELECT p.name
           FROM volun.projects p
          WHERE (p.id = vs.project_id)) AS project,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN ((vs.phone_number)::text ~ '[6|7]\d{8}'::text) THEN vs.phone_number
            ELSE vs.phone_number_alt
        END AS phone_number_specific,
    ( SELECT to_char(u.created_at, 'DD/MM/YYYY HH24:MI'::text) AS to_char
           FROM volun.users u
          WHERE (u.id = rf.user_id)) AS volunteer_subscribe_date,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status
   FROM volun.rt_volunteer_subscribes vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::VolunteerSubscribe'::text));


--
-- Name: view_list_rt_volunteers_demands; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_rt_volunteers_demands AS
 SELECT vs.id,
    vs.description,
    vs.execution_start_date,
    vs.execution_end_date,
    vs.road_type,
    vs.road_name,
    vs.number_type,
    vs.road_number,
    vs.postal_code,
    vs.town,
    vs.requested_volunteers_num,
    vs.volunteers_profile,
    vs.volunteer_functions_1,
    vs.volunteer_functions_2,
    vs.volunteer_functions_3,
    vs.notes,
    vs.created_at,
    vs.updated_at,
    vs.project_id,
    vs.volunteer_functions_4,
    vs.volunteer_functions_5,
    vs.province_id,
    vs.district_id,
    vs.borought_id,
    vs.entity_id,
    ( SELECT e.name
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity,
    rf.manager_id,
    rf.status_date,
    rf.req_status_id,
    ( SELECT rs.description
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS req_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS manager,
    rf.rt_extendable_type,
        CASE
            WHEN (rf.req_status_id = ANY (ARRAY[3, 4, 2])) THEN ((rf.status_date)::date - (rf.created_at)::date)
            ELSE ((now())::date - (rf.created_at)::date)
        END AS days_passed,
    to_char(rf.status_date, 'DD/MM/YYYY HH24:MI'::text) AS get_date_status,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (rf.manager_id = m.id)) AS get_manager,
    ( SELECT
                CASE
                    WHEN (rs.description = 'pending'::text) THEN 'Pendiente'::text
                    WHEN (rs.description = 'processing'::text) THEN 'En trámite'::text
                    WHEN (rs.description = 'approved'::text) THEN 'Aceptada'::text
                    WHEN (rs.description = 'rejected'::text) THEN 'Rechazada'::text
                    WHEN (rs.description = 'appointed'::text) THEN 'Citado/a'::text
                    ELSE NULL::text
                END AS "case"
           FROM volun.req_statuses rs
          WHERE (rs.id = rf.req_status_id)) AS get_status,
    ( SELECT
                CASE
                    WHEN ((e.phone_number)::text ~ '[6|7]\d{8}'::text) THEN e.phone_number
                    ELSE e.phone_number_alt
                END AS phone_number_alt
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_phone_number,
    ( SELECT e.email
           FROM volun.entities e
          WHERE (e.id = vs.entity_id)) AS entity_email
   FROM volun.rt_volunteers_demands vs,
    volun.request_forms rf
  WHERE ((vs.id = rf.rt_extendable_id) AND ((rf.rt_extendable_type)::text = 'Rt::VolunteersDemand'::text));


--
-- Name: view_list_service_questions; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_service_questions AS
 SELECT service_questions.id,
    service_questions.description,
    service_questions.active,
    service_questions.question_type
   FROM volun.service_questions;


--
-- Name: view_list_services; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_services AS
 SELECT services.id,
    services.name,
    services.description,
    services.active,
    services.code,
    services.is_digital,
    services.mta_order,
    services.normal_flow
   FROM volun.services;


--
-- Name: view_list_settings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_settings AS
 SELECT settings.id,
    settings.key,
    settings.value,
    settings.active
   FROM volun.settings;


--
-- Name: view_list_shift_definitions; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_shift_definitions AS
 SELECT shift_definitions.id,
    shift_definitions.name,
    shift_definitions.description,
    shift_definitions.active
   FROM volun.shift_definitions;


--
-- Name: view_list_skills; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_skills AS
 SELECT skills.id,
    skills.name,
    skills.active
   FROM volun.skills;


--
-- Name: view_list_tracking_types; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_tracking_types AS
 SELECT tt.id,
    tt.name,
    tt.active,
    tt.alias_name,
    tt.system,
    tt.tracking_types_type
   FROM volun.tracking_types tt;


--
-- Name: view_list_traits; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_traits AS
 SELECT traits.id,
    traits.name,
    traits.active
   FROM volun.traits;


--
-- Name: view_list_type_meetings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_type_meetings AS
 SELECT type_meetings.id,
    type_meetings.name,
    type_meetings.description,
    type_meetings.active
   FROM volun.type_meetings;


--
-- Name: view_list_unsubscribe_reasons; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_unsubscribe_reasons AS
 SELECT unsubscribe_reasons.id,
    unsubscribe_reasons.name,
    unsubscribe_reasons.active,
    unsubscribe_reasons.code
   FROM volun.unsubscribe_reasons;


--
-- Name: virtual_community_component_types; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.virtual_community_component_types (
    id bigint NOT NULL,
    title character varying NOT NULL,
    code character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    active boolean DEFAULT true
);


--
-- Name: view_list_virtual_community_component_types; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_virtual_community_component_types AS
 SELECT m.id,
    m.title,
    m.code,
    m.active
   FROM volun.virtual_community_component_types m;


--
-- Name: view_list_volun_profiles; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_volun_profiles AS
 SELECT volun_profiles.id,
    volun_profiles.name,
    volun_profiles.active
   FROM volun.volun_profiles;


--
-- Name: volun_trackings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_trackings (
    id integer NOT NULL,
    volunteer_id integer NOT NULL,
    tracking_type_id integer NOT NULL,
    project_id integer,
    manager_id integer,
    request_form_id integer,
    tracked_at timestamp without time zone NOT NULL,
    automatic boolean DEFAULT false,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: view_list_volun_trackings; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_volun_trackings AS
 SELECT et.id,
    ( SELECT upper(concat(e.name, ' ', e.last_name, ' ', e.last_name_alt)) AS upper
           FROM volun.volunteers e
          WHERE (e.id = et.volunteer_id)) AS volunteer,
    et.volunteer_id,
    et.project_id,
    et.tracked_at,
    et.automatic,
    et.comments,
    ( SELECT upper(concat(m.name, ' ', m.last_name, ' ', m.last_name_alt)) AS upper
           FROM volun.managers m
          WHERE (et.manager_id = m.id)) AS manager,
    ( SELECT p.name
           FROM volun.projects p
          WHERE (et.project_id = p.id)) AS project,
    et.manager_id,
    ( SELECT tt.name
           FROM volun.tracking_types tt
          WHERE (tt.id = et.tracking_type_id)) AS tracking_type,
    ( SELECT rf.id
           FROM volun.request_forms rf
          WHERE (rf.id = et.request_form_id)) AS request_form,
    et.request_form_id,
    et.tracking_type_id
   FROM volun.volun_trackings et;


--
-- Name: view_list_volunteers; Type: VIEW; Schema: volun; Owner: -
--

CREATE VIEW volun.view_list_volunteers AS
 SELECT v.id,
    v.name,
    v.last_name,
    v.last_name_alt,
    v.document_type_id,
    v.id_number,
    v.birth_date,
    v.nationality_id,
    v.phone_number,
    v.phone_number_alt,
    v.email,
    v.address_id,
    v.status_id,
    v.employment_status_id,
    v.vocne,
    v.available,
    v.availability_date,
    v.academic_level_id,
    v.subscribe_date,
    v.unsubscribe_date,
    v.unsubscribe_reason_id,
    v.comments,
    v.expectations,
    v.agreement,
    v.agreement_date,
    v.search_authorization,
    v.representative_statement,
    v.has_driving_license,
    v.publish_pictures,
    v.annual_survey,
    v.subscribed_at,
    v.manager_id,
    v.info_source_id,
    v.other_academic_info,
    v.error_address,
    v.error_other,
    v.review,
    v.profession_id,
    v.active,
    v.created_at,
    v.updated_at,
    v.notes,
    v.accompany_volunteer,
    v.cession_of_personal_data,
    v.publication_image_ayto,
    v.publication_image_ooaa,
    v.publication_image_social_network,
    v.publication_image_media,
    v.criminal_certificate,
    v.retired,
    v.ec,
    v.next_point_id,
    v.code_ci360,
    v.code_cid360_autentication,
    v.validation_cid360,
    v.id_autonum,
    v.market,
    v.tester,
    v.is_adult,
    v.gender_id,
    v.is_death,
    v.padron_reason,
    (date_part('year'::text, age((v.birth_date)::timestamp with time zone)))::integer AS age_name,
    ( SELECT g.name
           FROM volun.genders g
          WHERE (g.id = v.gender_id)) AS gender_convert,
    ( SELECT a.town
           FROM volun.addresses a
          WHERE (a.id = v.address_id)) AS town,
    ( SELECT a.postal_code
           FROM volun.addresses a
          WHERE (a.id = v.address_id)) AS postal_code,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.districts d
          WHERE ((a.id = v.address_id) AND (d.id = a.district_id))), 'OTROS'::character varying) AS district_custom,
    ( SELECT count(1) AS count
           FROM volun.projects_volunteers pv
          WHERE ((pv.active = true) AND (v.id = pv.volunteer_id))) AS num_projects_active,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.districts d
          WHERE ((d.id = a.district_id) AND (a.id = v.address_id))), 'OTROS'::character varying) AS district,
    COALESCE(( SELECT (d.code)::integer AS code
           FROM volun.addresses a,
            volun.districts d
          WHERE ((d.id = a.district_id) AND (a.id = v.address_id))), 22) AS district_id,
        CASE
            WHEN ((v.active = true) AND (v.available = true) AND (v.availability_date IS NOT NULL) AND (v.availability_date <= ((now() + '7 days'::interval))::date)) THEN true
            ELSE false
        END AS notice_available_column,
        CASE
            WHEN (((v.birth_date IS NOT NULL) AND ((date_part('year'::text, age((v.birth_date)::timestamp with time zone)))::integer < 18)) OR (v.cession_of_personal_data = false) OR (v.publication_image_ayto = false) OR (v.publication_image_ooaa = false) OR (v.publication_image_social_network = false) OR (v.publication_image_media = false) OR (v.vocne = true) OR COALESCE(( SELECT (upper((s.name)::text) = 'NO APTO'::text)
               FROM volun.statuses s
              WHERE (s.id = v.status_id)), false)) THEN true
            ELSE false
        END AS not_autorized,
    ( SELECT string_agg((a2.name)::text, ','::text) AS string_agg
           FROM volun.areas a2,
            volun.areas_volunteers av
          WHERE ((av.area_id = a2.id) AND (av.volunteer_id = v.id))) AS get_areas,
    ( SELECT string_agg((c.name)::text, ','::text) AS string_agg
           FROM volun.collectives c,
            volun.collectives_volunteers cv
          WHERE ((cv.collective_id = c.id) AND (cv.volunteer_id = v.id))) AS get_collectives,
    ( SELECT string_agg((is2.name)::text, ','::text) AS string_agg
           FROM volun.info_sources is2
          WHERE (is2.id = v.info_source_id)) AS get_info_source,
    ( SELECT string_agg((vp.name)::text, ','::text) AS string_agg
           FROM volun.volun_profiles vp,
            volun.volun_profiles_volunteers vpv
          WHERE ((vpv.volun_profile_id = vp.id) AND (vpv.volunteer_id = v.id))) AS get_volun_profiles,
    to_char((v.subscribe_date)::timestamp with time zone, 'DD/MM/YYYY HH24:MI'::text) AS subscribe_date_format,
    COALESCE(( SELECT d.name
           FROM volun.addresses a,
            volun.boroughts d
          WHERE ((d.id = a.borought_id) AND (a.id = v.address_id))), 'OTROS'::character varying) AS borought,
    COALESCE(( SELECT (d.code)::integer AS code
           FROM volun.addresses a,
            volun.boroughts d
          WHERE ((d.id = a.borought_id) AND (a.id = v.address_id))), 22) AS borought_id
   FROM volun.volunteers v;


--
-- Name: virtual_communities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.virtual_communities (
    id integer NOT NULL,
    visits_num integer,
    num_share_projects integer,
    num_login integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: virtual_communities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.virtual_communities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_communities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.virtual_communities_id_seq OWNED BY volun.virtual_communities.id;


--
-- Name: virtual_community_component_types_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.virtual_community_component_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_community_component_types_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.virtual_community_component_types_id_seq OWNED BY volun.virtual_community_component_types.id;


--
-- Name: virtual_community_components; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.virtual_community_components (
    id bigint NOT NULL,
    virtual_community_component_type_id bigint,
    virtual_community_component_id bigint,
    title character varying(100),
    subtitle character varying(300),
    content text,
    button_active boolean DEFAULT true,
    button_title character varying(50),
    button_alt_title character varying(150),
    image_header_file_name character varying,
    image_header_content_type character varying,
    image_header_file_size integer,
    image_header_updated_at timestamp without time zone,
    "order" integer,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    denomination character varying NOT NULL
);


--
-- Name: virtual_community_components_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.virtual_community_components_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_community_components_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.virtual_community_components_id_seq OWNED BY volun.virtual_community_components.id;


--
-- Name: virtual_comunities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.virtual_comunities (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: virtual_comunities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.virtual_comunities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: virtual_comunities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.virtual_comunities_id_seq OWNED BY volun.virtual_comunities.id;


--
-- Name: visits_origins; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.visits_origins (
    id integer NOT NULL,
    country character varying,
    n_visits integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: visits_origins_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.visits_origins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visits_origins_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.visits_origins_id_seq OWNED BY volun.visits_origins.id;


--
-- Name: volun_assessments; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_assessments (
    id integer NOT NULL,
    volunteer_id integer NOT NULL,
    trait_id integer NOT NULL,
    trait_other character varying,
    assessment boolean DEFAULT false NOT NULL,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_assessments_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_assessments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_assessments_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_assessments_id_seq OWNED BY volun.volun_assessments.id;


--
-- Name: volun_assessments_projects; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_assessments_projects (
    id integer NOT NULL,
    volunteer_id integer,
    trait_id integer,
    project_id integer,
    trait_other character varying,
    assessment boolean DEFAULT false,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_assessments_projects_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_assessments_projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_assessments_projects_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_assessments_projects_id_seq OWNED BY volun.volun_assessments_projects.id;


--
-- Name: volun_availabilities; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_availabilities (
    id integer NOT NULL,
    volunteer_id integer NOT NULL,
    day integer NOT NULL,
    start_hour character varying,
    end_hour character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_availabilities_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_availabilities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_availabilities_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_availabilities_id_seq OWNED BY volun.volun_availabilities.id;


--
-- Name: volun_contacts; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_contacts (
    id integer NOT NULL,
    volunteer_id integer NOT NULL,
    contact_result_id integer NOT NULL,
    project_id integer NOT NULL,
    manager_id integer NOT NULL,
    contact_type_id integer,
    contact_date timestamp without time zone NOT NULL,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_contacts_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_contacts_id_seq OWNED BY volun.volun_contacts.id;


--
-- Name: volun_exports; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_exports (
    id integer NOT NULL,
    id_field boolean DEFAULT true,
    name_field boolean DEFAULT true,
    first_last_name_field boolean DEFAULT true,
    second_last_name_field boolean DEFAULT true,
    phone_field boolean DEFAULT true,
    mail_field boolean DEFAULT true,
    district_field boolean DEFAULT true,
    subscribe_date_field boolean DEFAULT false,
    unsubscribe_date_field boolean DEFAULT false,
    sex_field boolean DEFAULT false,
    birth_date_field boolean DEFAULT false,
    age_field boolean DEFAULT false,
    document_type_field boolean DEFAULT false,
    document_number_field boolean DEFAULT false,
    province_field boolean DEFAULT false,
    town_field boolean DEFAULT false,
    road_name_field boolean DEFAULT false,
    postal_code_field boolean DEFAULT false,
    academic_level_field boolean DEFAULT false,
    languages_field boolean DEFAULT false,
    project_culities_field boolean DEFAULT false,
    vocne_field boolean DEFAULT false,
    can_criminal_field boolean DEFAULT false,
    criminal_certificate_field boolean DEFAULT false,
    has_diving_license_field boolean DEFAULT false,
    profession_field boolean DEFAULT false,
    how_know_cv_field boolean DEFAULT false,
    is_export boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    retired boolean,
    nationality boolean,
    country boolean,
    status boolean,
    academic_level boolean,
    degree_type boolean,
    language boolean,
    language_level boolean,
    skills boolean,
    next_point boolean,
    mobile_phone_field boolean DEFAULT true
);


--
-- Name: volun_exports_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_exports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_exports_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_exports_id_seq OWNED BY volun.volun_exports.id;


--
-- Name: volun_known_languages; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_known_languages (
    id integer NOT NULL,
    volunteer_id integer NOT NULL,
    language_id integer NOT NULL,
    language_level_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_known_languages_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_known_languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_known_languages_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_known_languages_id_seq OWNED BY volun.volun_known_languages.id;


--
-- Name: volun_profiles_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_profiles_id_seq OWNED BY volun.volun_profiles.id;


--
-- Name: volun_proy_meetings; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volun_proy_meetings (
    id integer NOT NULL,
    projects_volunteer_id integer,
    meeting_id integer,
    asist boolean,
    confirm boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volun_proy_meetings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_proy_meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_proy_meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_proy_meetings_id_seq OWNED BY volun.volun_proy_meetings.id;


--
-- Name: volun_trackings_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volun_trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volun_trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volun_trackings_id_seq OWNED BY volun.volun_trackings.id;


--
-- Name: volunteer_projects_relations; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volunteer_projects_relations (
    id integer NOT NULL,
    project_id integer,
    volunteer_id integer,
    type_relation character varying,
    start_date date,
    end_date date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volunteer_projects_relations_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volunteer_projects_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volunteer_projects_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volunteer_projects_relations_id_seq OWNED BY volun.volunteer_projects_relations.id;


--
-- Name: volunteer_turns; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.volunteer_turns (
    id integer NOT NULL,
    day character varying,
    turn character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: volunteer_turns_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volunteer_turns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volunteer_turns_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volunteer_turns_id_seq OWNED BY volun.volunteer_turns.id;


--
-- Name: volunteers_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.volunteers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: volunteers_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.volunteers_id_seq OWNED BY volun.volunteers.id;


--
-- Name: ws_messages; Type: TABLE; Schema: volun; Owner: -
--

CREATE TABLE volun.ws_messages (
    id bigint NOT NULL,
    body text,
    date_at timestamp without time zone NOT NULL,
    attach_file_name character varying,
    attach_content_type character varying,
    attach_file_size integer,
    attach_updated_at timestamp without time zone,
    citizen_id bigint NOT NULL,
    volunteer_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    type_author character varying
);


--
-- Name: ws_messages_id_seq; Type: SEQUENCE; Schema: volun; Owner: -
--

CREATE SEQUENCE volun.ws_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ws_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: volun; Owner: -
--

ALTER SEQUENCE volun.ws_messages_id_seq OWNED BY volun.ws_messages.id;


--
-- Name: academic_levels id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.academic_levels ALTER COLUMN id SET DEFAULT nextval('volun.academic_levels_id_seq'::regclass);


--
-- Name: activities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.activities ALTER COLUMN id SET DEFAULT nextval('volun.activities_id_seq'::regclass);


--
-- Name: addresses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.addresses ALTER COLUMN id SET DEFAULT nextval('volun.addresses_id_seq'::regclass);


--
-- Name: ahoy_events id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ahoy_events ALTER COLUMN id SET DEFAULT nextval('volun.ahoy_events_id_seq'::regclass);


--
-- Name: ahoy_visits id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ahoy_visits ALTER COLUMN id SET DEFAULT nextval('volun.ahoy_visits_id_seq'::regclass);


--
-- Name: albums id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.albums ALTER COLUMN id SET DEFAULT nextval('volun.albums_id_seq'::regclass);


--
-- Name: areas id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.areas ALTER COLUMN id SET DEFAULT nextval('volun.areas_id_seq'::regclass);


--
-- Name: audits id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.audits ALTER COLUMN id SET DEFAULT nextval('volun.audits_id_seq'::regclass);


--
-- Name: boroughts id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.boroughts ALTER COLUMN id SET DEFAULT nextval('volun.boroughts_id_seq'::regclass);


--
-- Name: channels id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.channels ALTER COLUMN id SET DEFAULT nextval('volun.channels_id_seq'::regclass);


--
-- Name: check_graphics id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.check_graphics ALTER COLUMN id SET DEFAULT nextval('volun.check_graphics_id_seq'::regclass);


--
-- Name: citizen_service_trackings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_service_trackings ALTER COLUMN id SET DEFAULT nextval('volun.citizen_service_trackings_id_seq'::regclass);


--
-- Name: citizen_services id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services ALTER COLUMN id SET DEFAULT nextval('volun.citizen_services_id_seq'::regclass);


--
-- Name: citizen_trackings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_trackings ALTER COLUMN id SET DEFAULT nextval('volun.citizen_trackings_id_seq'::regclass);


--
-- Name: citizens id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens ALTER COLUMN id SET DEFAULT nextval('volun.citizens_id_seq'::regclass);


--
-- Name: ckeditor_assets id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('volun.ckeditor_assets_id_seq'::regclass);


--
-- Name: collectives id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.collectives ALTER COLUMN id SET DEFAULT nextval('volun.collectives_id_seq'::regclass);


--
-- Name: contact_results id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.contact_results ALTER COLUMN id SET DEFAULT nextval('volun.contact_results_id_seq'::regclass);


--
-- Name: contact_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.contact_types ALTER COLUMN id SET DEFAULT nextval('volun.contact_types_id_seq'::regclass);


--
-- Name: coordinations id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.coordinations ALTER COLUMN id SET DEFAULT nextval('volun.coordinations_id_seq'::regclass);


--
-- Name: degree_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degree_types ALTER COLUMN id SET DEFAULT nextval('volun.degree_types_id_seq'::regclass);


--
-- Name: degrees id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degrees ALTER COLUMN id SET DEFAULT nextval('volun.degrees_id_seq'::regclass);


--
-- Name: degrees_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degrees_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.degrees_volunteers_id_seq'::regclass);


--
-- Name: delayed_jobs id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.delayed_jobs ALTER COLUMN id SET DEFAULT nextval('volun.delayed_jobs_id_seq'::regclass);


--
-- Name: derivaties_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.derivaties_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.derivaties_volunteers_id_seq'::regclass);


--
-- Name: digital_comments id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_comments ALTER COLUMN id SET DEFAULT nextval('volun.digital_comments_id_seq'::regclass);


--
-- Name: digital_faqs id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_faqs ALTER COLUMN id SET DEFAULT nextval('volun.digital_faqs_id_seq'::regclass);


--
-- Name: digital_follows id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_follows ALTER COLUMN id SET DEFAULT nextval('volun.digital_follows_id_seq'::regclass);


--
-- Name: digital_topics id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_topics ALTER COLUMN id SET DEFAULT nextval('volun.digital_topics_id_seq'::regclass);


--
-- Name: digital_votes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_votes ALTER COLUMN id SET DEFAULT nextval('volun.digital_votes_id_seq'::regclass);


--
-- Name: districts id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts ALTER COLUMN id SET DEFAULT nextval('volun.districts_id_seq'::regclass);


--
-- Name: districts_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.districts_volunteers_id_seq'::regclass);


--
-- Name: document_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.document_types ALTER COLUMN id SET DEFAULT nextval('volun.document_types_id_seq'::regclass);


--
-- Name: documents id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.documents ALTER COLUMN id SET DEFAULT nextval('volun.documents_id_seq'::regclass);


--
-- Name: employment_statuses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.employment_statuses ALTER COLUMN id SET DEFAULT nextval('volun.employment_statuses_id_seq'::regclass);


--
-- Name: ent_trackings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings ALTER COLUMN id SET DEFAULT nextval('volun.ent_trackings_id_seq'::regclass);


--
-- Name: entities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entities ALTER COLUMN id SET DEFAULT nextval('volun.entities_id_seq'::regclass);


--
-- Name: entity_organizations id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entity_organizations ALTER COLUMN id SET DEFAULT nextval('volun.entity_organizations_id_seq'::regclass);


--
-- Name: entity_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entity_types ALTER COLUMN id SET DEFAULT nextval('volun.entity_types_id_seq'::regclass);


--
-- Name: event_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.event_types ALTER COLUMN id SET DEFAULT nextval('volun.event_types_id_seq'::regclass);


--
-- Name: events id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.events ALTER COLUMN id SET DEFAULT nextval('volun.events_id_seq'::regclass);


--
-- Name: frontpage_elements id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_elements ALTER COLUMN id SET DEFAULT nextval('volun.frontpage_elements_id_seq'::regclass);


--
-- Name: frontpage_positions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_positions ALTER COLUMN id SET DEFAULT nextval('volun.frontpage_positions_id_seq'::regclass);


--
-- Name: genders id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.genders ALTER COLUMN id SET DEFAULT nextval('volun.genders_id_seq'::regclass);


--
-- Name: id_number_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.id_number_types ALTER COLUMN id SET DEFAULT nextval('volun.id_number_types_id_seq'::regclass);


--
-- Name: info_sources id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.info_sources ALTER COLUMN id SET DEFAULT nextval('volun.info_sources_id_seq'::regclass);


--
-- Name: language_levels id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.language_levels ALTER COLUMN id SET DEFAULT nextval('volun.language_levels_id_seq'::regclass);


--
-- Name: languages id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.languages ALTER COLUMN id SET DEFAULT nextval('volun.languages_id_seq'::regclass);


--
-- Name: link_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.link_types ALTER COLUMN id SET DEFAULT nextval('volun.link_types_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.links ALTER COLUMN id SET DEFAULT nextval('volun.links_id_seq'::regclass);


--
-- Name: managers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.managers ALTER COLUMN id SET DEFAULT nextval('volun.managers_id_seq'::regclass);


--
-- Name: massive_email_historics id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_email_historics ALTER COLUMN id SET DEFAULT nextval('volun.massive_email_historics_id_seq'::regclass);


--
-- Name: massive_sms_historics id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_sms_historics ALTER COLUMN id SET DEFAULT nextval('volun.massive_sms_historics_id_seq'::regclass);


--
-- Name: meetings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.meetings ALTER COLUMN id SET DEFAULT nextval('volun.meetings_id_seq'::regclass);


--
-- Name: moderate_statuses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.moderate_statuses ALTER COLUMN id SET DEFAULT nextval('volun.moderate_statuses_id_seq'::regclass);


--
-- Name: motivations id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.motivations ALTER COLUMN id SET DEFAULT nextval('volun.motivations_id_seq'::regclass);


--
-- Name: nationalities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.nationalities ALTER COLUMN id SET DEFAULT nextval('volun.nationalities_id_seq'::regclass);


--
-- Name: new_campaings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.new_campaings ALTER COLUMN id SET DEFAULT nextval('volun.new_campaings_id_seq'::regclass);


--
-- Name: next_points id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.next_points ALTER COLUMN id SET DEFAULT nextval('volun.next_points_id_seq'::regclass);


--
-- Name: notice_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.notice_types ALTER COLUMN id SET DEFAULT nextval('volun.notice_types_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.permissions ALTER COLUMN id SET DEFAULT nextval('volun.permissions_id_seq'::regclass);


--
-- Name: popular_sections id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.popular_sections ALTER COLUMN id SET DEFAULT nextval('volun.popular_sections_id_seq'::regclass);


--
-- Name: pro_issues id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_issues ALTER COLUMN id SET DEFAULT nextval('volun.pro_issues_id_seq'::regclass);


--
-- Name: pro_trackings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings ALTER COLUMN id SET DEFAULT nextval('volun.pro_trackings_id_seq'::regclass);


--
-- Name: professions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.professions ALTER COLUMN id SET DEFAULT nextval('volun.professions_id_seq'::regclass);


--
-- Name: profiles id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.profiles ALTER COLUMN id SET DEFAULT nextval('volun.profiles_id_seq'::regclass);


--
-- Name: project_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.project_types ALTER COLUMN id SET DEFAULT nextval('volun.project_types_id_seq'::regclass);


--
-- Name: projects id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects ALTER COLUMN id SET DEFAULT nextval('volun.projects_id_seq'::regclass);


--
-- Name: projects_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.projects_volunteers_id_seq'::regclass);


--
-- Name: proposals id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.proposals ALTER COLUMN id SET DEFAULT nextval('volun.proposals_id_seq'::regclass);


--
-- Name: provinces id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.provinces ALTER COLUMN id SET DEFAULT nextval('volun.provinces_id_seq'::regclass);


--
-- Name: pt_centres id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_centres ALTER COLUMN id SET DEFAULT nextval('volun.pt_centres_id_seq'::regclass);


--
-- Name: pt_entities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_entities ALTER COLUMN id SET DEFAULT nextval('volun.pt_entities_id_seq'::regclass);


--
-- Name: pt_municipals id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_municipals ALTER COLUMN id SET DEFAULT nextval('volun.pt_municipals_id_seq'::regclass);


--
-- Name: pt_others id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_others ALTER COLUMN id SET DEFAULT nextval('volun.pt_others_id_seq'::regclass);


--
-- Name: pt_permanents id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_permanents ALTER COLUMN id SET DEFAULT nextval('volun.pt_permanents_id_seq'::regclass);


--
-- Name: pt_punctuals id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_punctuals ALTER COLUMN id SET DEFAULT nextval('volun.pt_punctuals_id_seq'::regclass);


--
-- Name: pt_retired_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_retired_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.pt_retired_volunteers_id_seq'::regclass);


--
-- Name: pt_socials id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_socials ALTER COLUMN id SET DEFAULT nextval('volun.pt_socials_id_seq'::regclass);


--
-- Name: pt_subventions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_subventions ALTER COLUMN id SET DEFAULT nextval('volun.pt_subventions_id_seq'::regclass);


--
-- Name: pt_volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_volunteers ALTER COLUMN id SET DEFAULT nextval('volun.pt_volunteers_id_seq'::regclass);


--
-- Name: record_histories id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.record_histories ALTER COLUMN id SET DEFAULT nextval('volun.record_histories_id_seq'::regclass);


--
-- Name: req_reasons id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_reasons ALTER COLUMN id SET DEFAULT nextval('volun.req_reasons_id_seq'::regclass);


--
-- Name: req_rejection_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_rejection_types ALTER COLUMN id SET DEFAULT nextval('volun.req_rejection_types_id_seq'::regclass);


--
-- Name: req_status_traces id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_status_traces ALTER COLUMN id SET DEFAULT nextval('volun.req_status_traces_id_seq'::regclass);


--
-- Name: req_statuses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_statuses ALTER COLUMN id SET DEFAULT nextval('volun.req_statuses_id_seq'::regclass);


--
-- Name: request_forms id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms ALTER COLUMN id SET DEFAULT nextval('volun.request_forms_id_seq'::regclass);


--
-- Name: request_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_types ALTER COLUMN id SET DEFAULT nextval('volun.request_types_id_seq'::regclass);


--
-- Name: resources id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.resources ALTER COLUMN id SET DEFAULT nextval('volun.resources_id_seq'::regclass);


--
-- Name: road_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.road_types ALTER COLUMN id SET DEFAULT nextval('volun.road_types_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.roles ALTER COLUMN id SET DEFAULT nextval('volun.roles_id_seq'::regclass);


--
-- Name: rt_activity_publishings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_publishings ALTER COLUMN id SET DEFAULT nextval('volun.rt_activity_publishings_id_seq'::regclass);


--
-- Name: rt_activity_unpublishings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_unpublishings ALTER COLUMN id SET DEFAULT nextval('volun.rt_activity_unpublishings_id_seq'::regclass);


--
-- Name: rt_entity_subscribes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_subscribes ALTER COLUMN id SET DEFAULT nextval('volun.rt_entity_subscribes_id_seq'::regclass);


--
-- Name: rt_entity_unsubscribes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_unsubscribes ALTER COLUMN id SET DEFAULT nextval('volun.rt_entity_unsubscribes_id_seq'::regclass);


--
-- Name: rt_others id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_others ALTER COLUMN id SET DEFAULT nextval('volun.rt_others_id_seq'::regclass);


--
-- Name: rt_project_publishings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings ALTER COLUMN id SET DEFAULT nextval('volun.rt_project_publishings_id_seq'::regclass);


--
-- Name: rt_project_unpublishings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_unpublishings ALTER COLUMN id SET DEFAULT nextval('volun.rt_project_unpublishings_id_seq'::regclass);


--
-- Name: rt_solidarity_chains id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_solidarity_chains ALTER COLUMN id SET DEFAULT nextval('volun.rt_solidarity_chains_id_seq'::regclass);


--
-- Name: rt_volunteer_amendments id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteer_amendments_id_seq'::regclass);


--
-- Name: rt_volunteer_appointments id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_appointments ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteer_appointments_id_seq'::regclass);


--
-- Name: rt_volunteer_project_subscribes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_project_subscribes ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteer_project_subscribes_id_seq'::regclass);


--
-- Name: rt_volunteer_subscribes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_subscribes ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteer_subscribes_id_seq'::regclass);


--
-- Name: rt_volunteer_unsubscribes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_unsubscribes ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteer_unsubscribes_id_seq'::regclass);


--
-- Name: rt_volunteers_demands id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands ALTER COLUMN id SET DEFAULT nextval('volun.rt_volunteers_demands_id_seq'::regclass);


--
-- Name: search_forms id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.search_forms ALTER COLUMN id SET DEFAULT nextval('volun.search_forms_id_seq'::regclass);


--
-- Name: sectors id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.sectors ALTER COLUMN id SET DEFAULT nextval('volun.sectors_id_seq'::regclass);


--
-- Name: service_polls id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_polls ALTER COLUMN id SET DEFAULT nextval('volun.service_polls_id_seq'::regclass);


--
-- Name: service_questions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_questions ALTER COLUMN id SET DEFAULT nextval('volun.service_questions_id_seq'::regclass);


--
-- Name: service_responses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_responses ALTER COLUMN id SET DEFAULT nextval('volun.service_responses_id_seq'::regclass);


--
-- Name: service_votes id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_votes ALTER COLUMN id SET DEFAULT nextval('volun.service_votes_id_seq'::regclass);


--
-- Name: services id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.services ALTER COLUMN id SET DEFAULT nextval('volun.services_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.settings ALTER COLUMN id SET DEFAULT nextval('volun.settings_id_seq'::regclass);


--
-- Name: shift_definitions id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.shift_definitions ALTER COLUMN id SET DEFAULT nextval('volun.shift_definitions_id_seq'::regclass);


--
-- Name: skills id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.skills ALTER COLUMN id SET DEFAULT nextval('volun.skills_id_seq'::regclass);


--
-- Name: status_services id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.status_services ALTER COLUMN id SET DEFAULT nextval('volun.status_services_id_seq'::regclass);


--
-- Name: statuses id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.statuses ALTER COLUMN id SET DEFAULT nextval('volun.statuses_id_seq'::regclass);


--
-- Name: timetables id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.timetables ALTER COLUMN id SET DEFAULT nextval('volun.timetables_id_seq'::regclass);


--
-- Name: tracking_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.tracking_types ALTER COLUMN id SET DEFAULT nextval('volun.tracking_types_id_seq'::regclass);


--
-- Name: traits id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.traits ALTER COLUMN id SET DEFAULT nextval('volun.traits_id_seq'::regclass);


--
-- Name: type_meetings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.type_meetings ALTER COLUMN id SET DEFAULT nextval('volun.type_meetings_id_seq'::regclass);


--
-- Name: unsubscribe_levels id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.unsubscribe_levels ALTER COLUMN id SET DEFAULT nextval('volun.unsubscribe_levels_id_seq'::regclass);


--
-- Name: unsubscribe_reasons id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.unsubscribe_reasons ALTER COLUMN id SET DEFAULT nextval('volun.unsubscribe_reasons_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.users ALTER COLUMN id SET DEFAULT nextval('volun.users_id_seq'::regclass);


--
-- Name: virtual_communities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_communities ALTER COLUMN id SET DEFAULT nextval('volun.virtual_communities_id_seq'::regclass);


--
-- Name: virtual_community_component_types id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_component_types ALTER COLUMN id SET DEFAULT nextval('volun.virtual_community_component_types_id_seq'::regclass);


--
-- Name: virtual_community_components id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_components ALTER COLUMN id SET DEFAULT nextval('volun.virtual_community_components_id_seq'::regclass);


--
-- Name: virtual_comunities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_comunities ALTER COLUMN id SET DEFAULT nextval('volun.virtual_comunities_id_seq'::regclass);


--
-- Name: visits_origins id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.visits_origins ALTER COLUMN id SET DEFAULT nextval('volun.visits_origins_id_seq'::regclass);


--
-- Name: volun_assessments id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments ALTER COLUMN id SET DEFAULT nextval('volun.volun_assessments_id_seq'::regclass);


--
-- Name: volun_assessments_projects id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments_projects ALTER COLUMN id SET DEFAULT nextval('volun.volun_assessments_projects_id_seq'::regclass);


--
-- Name: volun_availabilities id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_availabilities ALTER COLUMN id SET DEFAULT nextval('volun.volun_availabilities_id_seq'::regclass);


--
-- Name: volun_contacts id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts ALTER COLUMN id SET DEFAULT nextval('volun.volun_contacts_id_seq'::regclass);


--
-- Name: volun_exports id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_exports ALTER COLUMN id SET DEFAULT nextval('volun.volun_exports_id_seq'::regclass);


--
-- Name: volun_known_languages id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_known_languages ALTER COLUMN id SET DEFAULT nextval('volun.volun_known_languages_id_seq'::regclass);


--
-- Name: volun_profiles id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_profiles ALTER COLUMN id SET DEFAULT nextval('volun.volun_profiles_id_seq'::regclass);


--
-- Name: volun_proy_meetings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_proy_meetings ALTER COLUMN id SET DEFAULT nextval('volun.volun_proy_meetings_id_seq'::regclass);


--
-- Name: volun_trackings id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings ALTER COLUMN id SET DEFAULT nextval('volun.volun_trackings_id_seq'::regclass);


--
-- Name: volunteer_projects_relations id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_projects_relations ALTER COLUMN id SET DEFAULT nextval('volun.volunteer_projects_relations_id_seq'::regclass);


--
-- Name: volunteer_turns id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_turns ALTER COLUMN id SET DEFAULT nextval('volun.volunteer_turns_id_seq'::regclass);


--
-- Name: volunteers id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers ALTER COLUMN id SET DEFAULT nextval('volun.volunteers_id_seq'::regclass);


--
-- Name: ws_messages id; Type: DEFAULT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ws_messages ALTER COLUMN id SET DEFAULT nextval('volun.ws_messages_id_seq'::regclass);


--
-- Name: academic_levels academic_levels_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.academic_levels
    ADD CONSTRAINT academic_levels_pkey PRIMARY KEY (id);


--
-- Name: activities activities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.activities
    ADD CONSTRAINT activities_pkey PRIMARY KEY (id);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: ahoy_events ahoy_events_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ahoy_events
    ADD CONSTRAINT ahoy_events_pkey PRIMARY KEY (id);


--
-- Name: ahoy_visits ahoy_visits_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ahoy_visits
    ADD CONSTRAINT ahoy_visits_pkey PRIMARY KEY (id);


--
-- Name: albums albums_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.albums
    ADD CONSTRAINT albums_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: areas areas_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.areas
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);


--
-- Name: audits audits_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.audits
    ADD CONSTRAINT audits_pkey PRIMARY KEY (id);


--
-- Name: boroughts boroughts_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.boroughts
    ADD CONSTRAINT boroughts_pkey PRIMARY KEY (id);


--
-- Name: channels channels_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: check_graphics check_graphics_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.check_graphics
    ADD CONSTRAINT check_graphics_pkey PRIMARY KEY (id);


--
-- Name: citizen_service_trackings citizen_service_trackings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_service_trackings
    ADD CONSTRAINT citizen_service_trackings_pkey PRIMARY KEY (id);


--
-- Name: citizen_services citizen_services_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT citizen_services_pkey PRIMARY KEY (id);


--
-- Name: citizen_trackings citizen_trackings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_trackings
    ADD CONSTRAINT citizen_trackings_pkey PRIMARY KEY (id);


--
-- Name: citizens citizens_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens
    ADD CONSTRAINT citizens_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets ckeditor_assets_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: collectives collectives_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.collectives
    ADD CONSTRAINT collectives_pkey PRIMARY KEY (id);


--
-- Name: contact_results contact_results_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.contact_results
    ADD CONSTRAINT contact_results_pkey PRIMARY KEY (id);


--
-- Name: contact_types contact_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.contact_types
    ADD CONSTRAINT contact_types_pkey PRIMARY KEY (id);


--
-- Name: coordinations coordinations_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.coordinations
    ADD CONSTRAINT coordinations_pkey PRIMARY KEY (id);


--
-- Name: degree_types degree_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degree_types
    ADD CONSTRAINT degree_types_pkey PRIMARY KEY (id);


--
-- Name: degrees degrees_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degrees
    ADD CONSTRAINT degrees_pkey PRIMARY KEY (id);


--
-- Name: degrees_volunteers degrees_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degrees_volunteers
    ADD CONSTRAINT degrees_volunteers_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs delayed_jobs_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: derivaties_volunteers derivaties_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.derivaties_volunteers
    ADD CONSTRAINT derivaties_volunteers_pkey PRIMARY KEY (id);


--
-- Name: digital_comments digital_comments_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_comments
    ADD CONSTRAINT digital_comments_pkey PRIMARY KEY (id);


--
-- Name: digital_faqs digital_faqs_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_faqs
    ADD CONSTRAINT digital_faqs_pkey PRIMARY KEY (id);


--
-- Name: digital_follows digital_follows_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_follows
    ADD CONSTRAINT digital_follows_pkey PRIMARY KEY (id);


--
-- Name: digital_topics digital_topics_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_topics
    ADD CONSTRAINT digital_topics_pkey PRIMARY KEY (id);


--
-- Name: digital_votes digital_votes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_votes
    ADD CONSTRAINT digital_votes_pkey PRIMARY KEY (id);


--
-- Name: districts districts_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts
    ADD CONSTRAINT districts_pkey PRIMARY KEY (id);


--
-- Name: districts_volunteers districts_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts_volunteers
    ADD CONSTRAINT districts_volunteers_pkey PRIMARY KEY (id);


--
-- Name: document_types document_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.document_types
    ADD CONSTRAINT document_types_pkey PRIMARY KEY (id);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: employment_statuses employment_statuses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.employment_statuses
    ADD CONSTRAINT employment_statuses_pkey PRIMARY KEY (id);


--
-- Name: ent_trackings ent_trackings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings
    ADD CONSTRAINT ent_trackings_pkey PRIMARY KEY (id);


--
-- Name: entities entities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entities
    ADD CONSTRAINT entities_pkey PRIMARY KEY (id);


--
-- Name: entity_organizations entity_organizations_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entity_organizations
    ADD CONSTRAINT entity_organizations_pkey PRIMARY KEY (id);


--
-- Name: entity_types entity_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entity_types
    ADD CONSTRAINT entity_types_pkey PRIMARY KEY (id);


--
-- Name: event_types event_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.event_types
    ADD CONSTRAINT event_types_pkey PRIMARY KEY (id);


--
-- Name: events events_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- Name: frontpage_elements frontpage_elements_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_elements
    ADD CONSTRAINT frontpage_elements_pkey PRIMARY KEY (id);


--
-- Name: frontpage_positions frontpage_positions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_positions
    ADD CONSTRAINT frontpage_positions_pkey PRIMARY KEY (id);


--
-- Name: genders genders_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.genders
    ADD CONSTRAINT genders_pkey PRIMARY KEY (id);


--
-- Name: id_number_types id_number_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.id_number_types
    ADD CONSTRAINT id_number_types_pkey PRIMARY KEY (id);


--
-- Name: info_sources info_sources_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.info_sources
    ADD CONSTRAINT info_sources_pkey PRIMARY KEY (id);


--
-- Name: language_levels language_levels_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.language_levels
    ADD CONSTRAINT language_levels_pkey PRIMARY KEY (id);


--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: link_types link_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.link_types
    ADD CONSTRAINT link_types_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: managers managers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.managers
    ADD CONSTRAINT managers_pkey PRIMARY KEY (id);


--
-- Name: massive_email_historics massive_email_historics_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_email_historics
    ADD CONSTRAINT massive_email_historics_pkey PRIMARY KEY (id);


--
-- Name: massive_sms_historics massive_sms_historics_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_sms_historics
    ADD CONSTRAINT massive_sms_historics_pkey PRIMARY KEY (id);


--
-- Name: meetings meetings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.meetings
    ADD CONSTRAINT meetings_pkey PRIMARY KEY (id);


--
-- Name: moderate_statuses moderate_statuses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.moderate_statuses
    ADD CONSTRAINT moderate_statuses_pkey PRIMARY KEY (id);


--
-- Name: motivations motivations_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.motivations
    ADD CONSTRAINT motivations_pkey PRIMARY KEY (id);


--
-- Name: nationalities nationalities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.nationalities
    ADD CONSTRAINT nationalities_pkey PRIMARY KEY (id);


--
-- Name: new_campaings new_campaings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.new_campaings
    ADD CONSTRAINT new_campaings_pkey PRIMARY KEY (id);


--
-- Name: next_points next_points_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.next_points
    ADD CONSTRAINT next_points_pkey PRIMARY KEY (id);


--
-- Name: notice_types notice_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.notice_types
    ADD CONSTRAINT notice_types_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: popular_sections popular_sections_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.popular_sections
    ADD CONSTRAINT popular_sections_pkey PRIMARY KEY (id);


--
-- Name: pro_issues pro_issues_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_issues
    ADD CONSTRAINT pro_issues_pkey PRIMARY KEY (id);


--
-- Name: pro_trackings pro_trackings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings
    ADD CONSTRAINT pro_trackings_pkey PRIMARY KEY (id);


--
-- Name: professions professions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.professions
    ADD CONSTRAINT professions_pkey PRIMARY KEY (id);


--
-- Name: profiles profiles_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: project_types project_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.project_types
    ADD CONSTRAINT project_types_pkey PRIMARY KEY (id);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: projects_volunteers projects_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects_volunteers
    ADD CONSTRAINT projects_volunteers_pkey PRIMARY KEY (id);


--
-- Name: proposals proposals_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.proposals
    ADD CONSTRAINT proposals_pkey PRIMARY KEY (id);


--
-- Name: provinces provinces_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.provinces
    ADD CONSTRAINT provinces_pkey PRIMARY KEY (id);


--
-- Name: pt_centres pt_centres_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_centres
    ADD CONSTRAINT pt_centres_pkey PRIMARY KEY (id);


--
-- Name: pt_entities pt_entities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_entities
    ADD CONSTRAINT pt_entities_pkey PRIMARY KEY (id);


--
-- Name: pt_municipals pt_municipals_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_municipals
    ADD CONSTRAINT pt_municipals_pkey PRIMARY KEY (id);


--
-- Name: pt_others pt_others_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_others
    ADD CONSTRAINT pt_others_pkey PRIMARY KEY (id);


--
-- Name: pt_permanents pt_permanents_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_permanents
    ADD CONSTRAINT pt_permanents_pkey PRIMARY KEY (id);


--
-- Name: pt_punctuals pt_punctuals_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_punctuals
    ADD CONSTRAINT pt_punctuals_pkey PRIMARY KEY (id);


--
-- Name: pt_retired_volunteers pt_retired_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_retired_volunteers
    ADD CONSTRAINT pt_retired_volunteers_pkey PRIMARY KEY (id);


--
-- Name: pt_socials pt_socials_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_socials
    ADD CONSTRAINT pt_socials_pkey PRIMARY KEY (id);


--
-- Name: pt_subventions pt_subventions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_subventions
    ADD CONSTRAINT pt_subventions_pkey PRIMARY KEY (id);


--
-- Name: pt_volunteers pt_volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_volunteers
    ADD CONSTRAINT pt_volunteers_pkey PRIMARY KEY (id);


--
-- Name: record_histories record_histories_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.record_histories
    ADD CONSTRAINT record_histories_pkey PRIMARY KEY (id);


--
-- Name: req_reasons req_reasons_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_reasons
    ADD CONSTRAINT req_reasons_pkey PRIMARY KEY (id);


--
-- Name: req_rejection_types req_rejection_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_rejection_types
    ADD CONSTRAINT req_rejection_types_pkey PRIMARY KEY (id);


--
-- Name: req_status_traces req_status_traces_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_status_traces
    ADD CONSTRAINT req_status_traces_pkey PRIMARY KEY (id);


--
-- Name: req_statuses req_statuses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_statuses
    ADD CONSTRAINT req_statuses_pkey PRIMARY KEY (id);


--
-- Name: request_forms request_forms_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT request_forms_pkey PRIMARY KEY (id);


--
-- Name: request_types request_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_types
    ADD CONSTRAINT request_types_pkey PRIMARY KEY (id);


--
-- Name: resources resources_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (id);


--
-- Name: road_types road_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.road_types
    ADD CONSTRAINT road_types_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: rt_activity_publishings rt_activity_publishings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_publishings
    ADD CONSTRAINT rt_activity_publishings_pkey PRIMARY KEY (id);


--
-- Name: rt_activity_unpublishings rt_activity_unpublishings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_unpublishings
    ADD CONSTRAINT rt_activity_unpublishings_pkey PRIMARY KEY (id);


--
-- Name: rt_entity_subscribes rt_entity_subscribes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_subscribes
    ADD CONSTRAINT rt_entity_subscribes_pkey PRIMARY KEY (id);


--
-- Name: rt_entity_unsubscribes rt_entity_unsubscribes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_unsubscribes
    ADD CONSTRAINT rt_entity_unsubscribes_pkey PRIMARY KEY (id);


--
-- Name: rt_others rt_others_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_others
    ADD CONSTRAINT rt_others_pkey PRIMARY KEY (id);


--
-- Name: rt_project_publishings rt_project_publishings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT rt_project_publishings_pkey PRIMARY KEY (id);


--
-- Name: rt_project_unpublishings rt_project_unpublishings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_unpublishings
    ADD CONSTRAINT rt_project_unpublishings_pkey PRIMARY KEY (id);


--
-- Name: rt_solidarity_chains rt_solidarity_chains_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_solidarity_chains
    ADD CONSTRAINT rt_solidarity_chains_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteer_amendments rt_volunteer_amendments_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments
    ADD CONSTRAINT rt_volunteer_amendments_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteer_appointments rt_volunteer_appointments_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_appointments
    ADD CONSTRAINT rt_volunteer_appointments_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteer_project_subscribes rt_volunteer_project_subscribes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_project_subscribes
    ADD CONSTRAINT rt_volunteer_project_subscribes_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteer_subscribes rt_volunteer_subscribes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_subscribes
    ADD CONSTRAINT rt_volunteer_subscribes_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteer_unsubscribes rt_volunteer_unsubscribes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_unsubscribes
    ADD CONSTRAINT rt_volunteer_unsubscribes_pkey PRIMARY KEY (id);


--
-- Name: rt_volunteers_demands rt_volunteers_demands_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT rt_volunteers_demands_pkey PRIMARY KEY (id);


--
-- Name: search_forms search_forms_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.search_forms
    ADD CONSTRAINT search_forms_pkey PRIMARY KEY (id);


--
-- Name: sectors sectors_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (id);


--
-- Name: service_polls service_polls_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_polls
    ADD CONSTRAINT service_polls_pkey PRIMARY KEY (id);


--
-- Name: service_questions service_questions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_questions
    ADD CONSTRAINT service_questions_pkey PRIMARY KEY (id);


--
-- Name: service_responses service_responses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_responses
    ADD CONSTRAINT service_responses_pkey PRIMARY KEY (id);


--
-- Name: service_votes service_votes_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_votes
    ADD CONSTRAINT service_votes_pkey PRIMARY KEY (id);


--
-- Name: services services_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: shift_definitions shift_definitions_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.shift_definitions
    ADD CONSTRAINT shift_definitions_pkey PRIMARY KEY (id);


--
-- Name: skills skills_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);


--
-- Name: status_services status_services_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.status_services
    ADD CONSTRAINT status_services_pkey PRIMARY KEY (id);


--
-- Name: statuses statuses_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.statuses
    ADD CONSTRAINT statuses_pkey PRIMARY KEY (id);


--
-- Name: timetables timetables_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.timetables
    ADD CONSTRAINT timetables_pkey PRIMARY KEY (id);


--
-- Name: tracking_types tracking_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.tracking_types
    ADD CONSTRAINT tracking_types_pkey PRIMARY KEY (id);


--
-- Name: traits traits_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.traits
    ADD CONSTRAINT traits_pkey PRIMARY KEY (id);


--
-- Name: type_meetings type_meetings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.type_meetings
    ADD CONSTRAINT type_meetings_pkey PRIMARY KEY (id);


--
-- Name: unsubscribe_levels unsubscribe_levels_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.unsubscribe_levels
    ADD CONSTRAINT unsubscribe_levels_pkey PRIMARY KEY (id);


--
-- Name: unsubscribe_reasons unsubscribe_reasons_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.unsubscribe_reasons
    ADD CONSTRAINT unsubscribe_reasons_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: virtual_communities virtual_communities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_communities
    ADD CONSTRAINT virtual_communities_pkey PRIMARY KEY (id);


--
-- Name: virtual_community_component_types virtual_community_component_types_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_component_types
    ADD CONSTRAINT virtual_community_component_types_pkey PRIMARY KEY (id);


--
-- Name: virtual_community_components virtual_community_components_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_components
    ADD CONSTRAINT virtual_community_components_pkey PRIMARY KEY (id);


--
-- Name: virtual_comunities virtual_comunities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_comunities
    ADD CONSTRAINT virtual_comunities_pkey PRIMARY KEY (id);


--
-- Name: visits_origins visits_origins_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.visits_origins
    ADD CONSTRAINT visits_origins_pkey PRIMARY KEY (id);


--
-- Name: volun_assessments volun_assessments_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments
    ADD CONSTRAINT volun_assessments_pkey PRIMARY KEY (id);


--
-- Name: volun_assessments_projects volun_assessments_projects_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments_projects
    ADD CONSTRAINT volun_assessments_projects_pkey PRIMARY KEY (id);


--
-- Name: volun_availabilities volun_availabilities_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_availabilities
    ADD CONSTRAINT volun_availabilities_pkey PRIMARY KEY (id);


--
-- Name: volun_contacts volun_contacts_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT volun_contacts_pkey PRIMARY KEY (id);


--
-- Name: volun_exports volun_exports_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_exports
    ADD CONSTRAINT volun_exports_pkey PRIMARY KEY (id);


--
-- Name: volun_known_languages volun_known_languages_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_known_languages
    ADD CONSTRAINT volun_known_languages_pkey PRIMARY KEY (id);


--
-- Name: volun_profiles volun_profiles_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_profiles
    ADD CONSTRAINT volun_profiles_pkey PRIMARY KEY (id);


--
-- Name: volun_proy_meetings volun_proy_meetings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_proy_meetings
    ADD CONSTRAINT volun_proy_meetings_pkey PRIMARY KEY (id);


--
-- Name: volun_trackings volun_trackings_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT volun_trackings_pkey PRIMARY KEY (id);


--
-- Name: volunteer_projects_relations volunteer_projects_relations_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_projects_relations
    ADD CONSTRAINT volunteer_projects_relations_pkey PRIMARY KEY (id);


--
-- Name: volunteer_turns volunteer_turns_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_turns
    ADD CONSTRAINT volunteer_turns_pkey PRIMARY KEY (id);


--
-- Name: volunteers volunteers_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT volunteers_pkey PRIMARY KEY (id);


--
-- Name: ws_messages ws_messages_pkey; Type: CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ws_messages
    ADD CONSTRAINT ws_messages_pkey PRIMARY KEY (id);


--
-- Name: component_parent; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX component_parent ON volun.virtual_community_components USING btree (virtual_community_component_id);


--
-- Name: component_type; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX component_type ON volun.virtual_community_components USING btree (virtual_community_component_type_id);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX delayed_jobs_priority ON volun.delayed_jobs USING btree (priority, run_at);


--
-- Name: index_academic_levels_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_academic_levels_on_name ON volun.academic_levels USING btree (name);


--
-- Name: index_activities_areas_on_activity_id_and_area_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_activities_areas_on_activity_id_and_area_id ON volun.activities_areas USING btree (activity_id, area_id);


--
-- Name: index_activities_areas_on_area_id_and_activity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_activities_areas_on_area_id_and_activity_id ON volun.activities_areas USING btree (area_id, activity_id);


--
-- Name: index_activities_on_area_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_activities_on_area_id ON volun.activities USING btree (area_id);


--
-- Name: index_activities_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_activities_on_entity_id ON volun.activities USING btree (entity_id);


--
-- Name: index_activities_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_activities_on_name ON volun.activities USING btree (name);


--
-- Name: index_activities_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_activities_on_project_id ON volun.activities USING btree (project_id);


--
-- Name: index_addresses_on_borought_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_addresses_on_borought_id ON volun.addresses USING btree (borought_id);


--
-- Name: index_addresses_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_addresses_on_district_id ON volun.addresses USING btree (district_id);


--
-- Name: index_addresses_on_nationality_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_addresses_on_nationality_id ON volun.addresses USING btree (nationality_id);


--
-- Name: index_addresses_on_province_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_addresses_on_province_id ON volun.addresses USING btree (province_id);


--
-- Name: index_ahoy_events_on_name_and_time; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ahoy_events_on_name_and_time ON volun.ahoy_events USING btree (name, "time");


--
-- Name: index_ahoy_visits_on_visit_token; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_ahoy_visits_on_visit_token ON volun.ahoy_visits USING btree (visit_token);


--
-- Name: index_albums_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_albums_on_project_id ON volun.albums USING btree (project_id);


--
-- Name: index_areas_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_areas_on_name ON volun.areas USING btree (name);


--
-- Name: index_areas_projects_on_area_id_and_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_areas_projects_on_area_id_and_project_id ON volun.areas_projects USING btree (area_id, project_id);


--
-- Name: index_areas_projects_on_project_id_and_area_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_areas_projects_on_project_id_and_area_id ON volun.areas_projects USING btree (project_id, area_id);


--
-- Name: index_areas_volunteers_on_area_id_and_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_areas_volunteers_on_area_id_and_volunteer_id ON volun.areas_volunteers USING btree (area_id, volunteer_id);


--
-- Name: index_areas_volunteers_on_volunteer_id_and_area_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_areas_volunteers_on_volunteer_id_and_area_id ON volun.areas_volunteers USING btree (volunteer_id, area_id);


--
-- Name: index_boroughts_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_boroughts_on_district_id ON volun.boroughts USING btree (district_id);


--
-- Name: index_citizen_service_trackings_on_citizen_service_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_service_trackings_on_citizen_service_id ON volun.citizen_service_trackings USING btree (citizen_service_id);


--
-- Name: index_citizen_service_trackings_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_service_trackings_on_manager_id ON volun.citizen_service_trackings USING btree (manager_id);


--
-- Name: index_citizen_service_trackings_on_tracking_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_service_trackings_on_tracking_type_id ON volun.citizen_service_trackings USING btree (tracking_type_id);


--
-- Name: index_citizen_services_on_borought_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_services_on_borought_id ON volun.citizen_services USING btree (borought_id);


--
-- Name: index_citizen_services_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_services_on_district_id ON volun.citizen_services USING btree (district_id);


--
-- Name: index_citizen_services_on_status_service_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_services_on_status_service_id ON volun.citizen_services USING btree (status_service_id);


--
-- Name: index_citizen_trackings_on_citizen_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_trackings_on_citizen_id ON volun.citizen_trackings USING btree (citizen_id);


--
-- Name: index_citizen_trackings_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_trackings_on_manager_id ON volun.citizen_trackings USING btree (manager_id);


--
-- Name: index_citizen_trackings_on_tracking_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizen_trackings_on_tracking_type_id ON volun.citizen_trackings USING btree (tracking_type_id);


--
-- Name: index_citizens_on_address_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizens_on_address_id ON volun.citizens USING btree (address_id);


--
-- Name: index_citizens_on_authentication_token; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_citizens_on_authentication_token ON volun.citizens USING btree (authentication_token);


--
-- Name: index_citizens_on_code_cid360_autentication; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_citizens_on_code_cid360_autentication ON volun.citizens USING btree (code_cid360_autentication);


--
-- Name: index_citizens_on_gender_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizens_on_gender_id ON volun.citizens USING btree (gender_id);


--
-- Name: index_citizens_on_nationality_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizens_on_nationality_id ON volun.citizens USING btree (nationality_id);


--
-- Name: index_citizens_on_province_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_citizens_on_province_id ON volun.citizens USING btree (province_id);


--
-- Name: index_ckeditor_assets_on_type; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ckeditor_assets_on_type ON volun.ckeditor_assets USING btree (type);


--
-- Name: index_collectives_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_collectives_on_name ON volun.collectives USING btree (name);


--
-- Name: index_collectives_projects_on_collective_id_and_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_collectives_projects_on_collective_id_and_project_id ON volun.collectives_projects USING btree (collective_id, project_id);


--
-- Name: index_collectives_projects_on_project_id_and_collective_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_collectives_projects_on_project_id_and_collective_id ON volun.collectives_projects USING btree (project_id, collective_id);


--
-- Name: index_collectives_volunteers_on_collective_id_and_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_collectives_volunteers_on_collective_id_and_volunteer_id ON volun.collectives_volunteers USING btree (collective_id, volunteer_id);


--
-- Name: index_collectives_volunteers_on_volunteer_id_and_collective_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_collectives_volunteers_on_volunteer_id_and_collective_id ON volun.collectives_volunteers USING btree (volunteer_id, collective_id);


--
-- Name: index_contact_results_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_contact_results_on_name ON volun.contact_results USING btree (name);


--
-- Name: index_contact_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_contact_types_on_name ON volun.contact_types USING btree (name);


--
-- Name: index_coordinations_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_coordinations_on_name ON volun.coordinations USING btree (name);


--
-- Name: index_coordinations_projects_on_coordination_id_and_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_coordinations_projects_on_coordination_id_and_project_id ON volun.coordinations_projects USING btree (coordination_id, project_id);


--
-- Name: index_coordinations_projects_on_project_id_and_coordination_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_coordinations_projects_on_project_id_and_coordination_id ON volun.coordinations_projects USING btree (project_id, coordination_id);


--
-- Name: index_degree_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_degree_types_on_name ON volun.degree_types USING btree (name);


--
-- Name: index_degrees_on_degree_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_degrees_on_degree_type_id ON volun.degrees USING btree (degree_type_id);


--
-- Name: index_degrees_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_degrees_on_name ON volun.degrees USING btree (name);


--
-- Name: index_degrees_volunteers_on_degree_id_and_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_degrees_volunteers_on_degree_id_and_volunteer_id ON volun.degrees_volunteers USING btree (degree_id, volunteer_id);


--
-- Name: index_degrees_volunteers_on_volunteer_id_and_degree_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_degrees_volunteers_on_volunteer_id_and_degree_id ON volun.degrees_volunteers USING btree (volunteer_id, degree_id);


--
-- Name: index_derivaties_volunteers_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_derivaties_volunteers_on_project_id ON volun.derivaties_volunteers USING btree (project_id);


--
-- Name: index_derivaties_volunteers_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_derivaties_volunteers_on_volunteer_id ON volun.derivaties_volunteers USING btree (volunteer_id);


--
-- Name: index_digital_comments_on_digital_comment_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_comments_on_digital_comment_id ON volun.digital_comments USING btree (digital_comment_id);


--
-- Name: index_digital_comments_on_digital_topic_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_comments_on_digital_topic_id ON volun.digital_comments USING btree (digital_topic_id);


--
-- Name: index_digital_comments_on_moderable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_comments_on_moderable ON volun.digital_comments USING btree (moderable_type, moderable_id);


--
-- Name: index_digital_comments_on_moderate_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_comments_on_moderate_status_id ON volun.digital_comments USING btree (moderate_status_id);


--
-- Name: index_digital_comments_on_userable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_comments_on_userable ON volun.digital_comments USING btree (userable_type, userable_id);


--
-- Name: index_digital_faqs_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_faqs_on_project_id ON volun.digital_faqs USING btree (project_id);


--
-- Name: index_digital_follows_on_digital_topic_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_follows_on_digital_topic_id ON volun.digital_follows USING btree (digital_topic_id);


--
-- Name: index_digital_follows_on_userable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_follows_on_userable ON volun.digital_follows USING btree (userable_type, userable_id);


--
-- Name: index_digital_topics_on_moderable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_topics_on_moderable ON volun.digital_topics USING btree (moderable_type, moderable_id);


--
-- Name: index_digital_topics_on_moderate_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_topics_on_moderate_status_id ON volun.digital_topics USING btree (moderate_status_id);


--
-- Name: index_digital_topics_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_topics_on_project_id ON volun.digital_topics USING btree (project_id);


--
-- Name: index_digital_topics_on_userable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_topics_on_userable ON volun.digital_topics USING btree (userable_type, userable_id);


--
-- Name: index_digital_votes_on_userable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_votes_on_userable ON volun.digital_votes USING btree (userable_type, userable_id);


--
-- Name: index_digital_votes_on_votable; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_digital_votes_on_votable ON volun.digital_votes USING btree (votable_type, votable_id);


--
-- Name: index_districts_on_code; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_districts_on_code ON volun.districts USING btree (code);


--
-- Name: index_districts_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_districts_on_name ON volun.districts USING btree (name);


--
-- Name: index_districts_volunteers_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_districts_volunteers_on_district_id ON volun.districts_volunteers USING btree (district_id);


--
-- Name: index_districts_volunteers_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_districts_volunteers_on_volunteer_id ON volun.districts_volunteers USING btree (volunteer_id);


--
-- Name: index_documents_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_documents_on_name ON volun.documents USING btree (name);


--
-- Name: index_documents_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_documents_on_project_id ON volun.documents USING btree (project_id);


--
-- Name: index_employment_statuses_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_employment_statuses_on_name ON volun.employment_statuses USING btree (name);


--
-- Name: index_ent_trackings_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ent_trackings_on_entity_id ON volun.ent_trackings USING btree (entity_id);


--
-- Name: index_ent_trackings_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ent_trackings_on_manager_id ON volun.ent_trackings USING btree (manager_id);


--
-- Name: index_ent_trackings_on_request_form_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ent_trackings_on_request_form_id ON volun.ent_trackings USING btree (request_form_id);


--
-- Name: index_ent_trackings_on_tracking_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ent_trackings_on_tracking_type_id ON volun.ent_trackings USING btree (tracking_type_id);


--
-- Name: index_entities_on_address_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entities_on_address_id ON volun.entities USING btree (address_id);


--
-- Name: index_entities_on_entity_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entities_on_entity_type_id ON volun.entities USING btree (entity_type_id);


--
-- Name: index_entities_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_entities_on_name ON volun.entities USING btree (name);


--
-- Name: index_entities_on_req_reason_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entities_on_req_reason_id ON volun.entities USING btree (req_reason_id);


--
-- Name: index_entities_projects_on_entity_id_and_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entities_projects_on_entity_id_and_project_id ON volun.entities_projects USING btree (entity_id, project_id);


--
-- Name: index_entities_projects_on_project_id_and_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entities_projects_on_project_id_and_entity_id ON volun.entities_projects USING btree (project_id, entity_id);


--
-- Name: index_entity_organizations_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_entity_organizations_on_project_id ON volun.entity_organizations USING btree (project_id);


--
-- Name: index_entity_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_entity_types_on_name ON volun.entity_types USING btree (name);


--
-- Name: index_event_types_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_event_types_on_kind ON volun.event_types USING btree (kind);


--
-- Name: index_events_on_address_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_events_on_address_id ON volun.events USING btree (address_id);


--
-- Name: index_events_on_event_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_events_on_event_type_id ON volun.events USING btree (event_type_id);


--
-- Name: index_events_on_eventable_type_and_eventable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_events_on_eventable_type_and_eventable_id ON volun.events USING btree (eventable_type, eventable_id);


--
-- Name: index_frontpage_elements_on_frontpage_position_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_frontpage_elements_on_frontpage_position_id ON volun.frontpage_elements USING btree (frontpage_position_id);


--
-- Name: index_frontpage_positions_on_position; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_frontpage_positions_on_position ON volun.frontpage_positions USING btree ("position");


--
-- Name: index_id_number_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_id_number_types_on_name ON volun.id_number_types USING btree (name);


--
-- Name: index_info_sources_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_info_sources_on_name ON volun.info_sources USING btree (name);


--
-- Name: index_language_levels_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_language_levels_on_name ON volun.language_levels USING btree (name);


--
-- Name: index_languages_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_languages_on_name ON volun.languages USING btree (name);


--
-- Name: index_link_types_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_link_types_on_kind ON volun.link_types USING btree (kind);


--
-- Name: index_links_on_link_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_links_on_link_type_id ON volun.links USING btree (link_type_id);


--
-- Name: index_links_on_linkable_type_and_linkable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_links_on_linkable_type_and_linkable_id ON volun.links USING btree (linkable_type, linkable_id);


--
-- Name: index_managers_on_role_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_managers_on_role_id ON volun.managers USING btree (role_id);


--
-- Name: index_massive_sms_historics_on_user_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_massive_sms_historics_on_user_id ON volun.massive_sms_historics USING btree (user_id);


--
-- Name: index_meetings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_meetings_on_project_id ON volun.meetings USING btree (project_id);


--
-- Name: index_meetings_on_type_meeting_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_meetings_on_type_meeting_id ON volun.meetings USING btree (type_meeting_id);


--
-- Name: index_nationalities_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_nationalities_on_name ON volun.nationalities USING btree (name);


--
-- Name: index_notice_types_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_notice_types_on_kind ON volun.notice_types USING btree (kind);


--
-- Name: index_permissions_on_data; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_permissions_on_data ON volun.permissions USING gin (data);


--
-- Name: index_permissions_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_permissions_on_manager_id ON volun.permissions USING btree (manager_id);


--
-- Name: index_pro_issues_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_pro_issues_on_project_id ON volun.pro_issues USING btree (project_id);


--
-- Name: index_pro_trackings_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_pro_trackings_on_manager_id ON volun.pro_trackings USING btree (manager_id);


--
-- Name: index_pro_trackings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_pro_trackings_on_project_id ON volun.pro_trackings USING btree (project_id);


--
-- Name: index_pro_trackings_on_request_form_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_pro_trackings_on_request_form_id ON volun.pro_trackings USING btree (request_form_id);


--
-- Name: index_professions_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_professions_on_name ON volun.professions USING btree (name);


--
-- Name: index_profiles_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_profiles_on_name ON volun.profiles USING btree (name);


--
-- Name: index_project_types_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_project_types_on_kind ON volun.project_types USING btree (kind);


--
-- Name: index_projects_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_projects_on_district_id ON volun.projects USING btree (district_id);


--
-- Name: index_projects_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_projects_on_entity_id ON volun.projects USING btree (entity_id);


--
-- Name: index_projects_on_pt_extendable_type_and_pt_extendable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_projects_on_pt_extendable_type_and_pt_extendable_id ON volun.projects USING btree (pt_extendable_type, pt_extendable_id);


--
-- Name: index_projects_on_year_and_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_projects_on_year_and_name ON volun.projects USING btree (year, name);


--
-- Name: index_projects_volunteers_on_project_id_and_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_projects_volunteers_on_project_id_and_volunteer_id ON volun.projects_volunteers USING btree (project_id, volunteer_id);


--
-- Name: index_projects_volunteers_on_volunteer_id_and_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_projects_volunteers_on_volunteer_id_and_project_id ON volun.projects_volunteers USING btree (volunteer_id, project_id);


--
-- Name: index_proposals_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_proposals_on_name ON volun.proposals USING btree (name);


--
-- Name: index_provinces_on_code; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_provinces_on_code ON volun.provinces USING btree (code);


--
-- Name: index_provinces_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_provinces_on_name ON volun.provinces USING btree (name);


--
-- Name: index_pt_subventions_on_proposal_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_pt_subventions_on_proposal_id ON volun.pt_subventions USING btree (proposal_id);


--
-- Name: index_record_histories_on_recordable_type_and_recordable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_record_histories_on_recordable_type_and_recordable_id ON volun.record_histories USING btree (recordable_type, recordable_id);


--
-- Name: index_record_histories_on_user_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_record_histories_on_user_id ON volun.record_histories USING btree (user_id);


--
-- Name: index_req_rejection_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_req_rejection_types_on_name ON volun.req_rejection_types USING btree (name);


--
-- Name: index_req_status_traces_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_req_status_traces_on_manager_id ON volun.req_status_traces USING btree (manager_id);


--
-- Name: index_req_status_traces_on_req_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_req_status_traces_on_req_status_id ON volun.req_status_traces USING btree (req_status_id);


--
-- Name: index_req_status_traces_on_request_form_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_req_status_traces_on_request_form_id ON volun.req_status_traces USING btree (request_form_id);


--
-- Name: index_req_statuses_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_req_statuses_on_kind ON volun.req_statuses USING btree (kind);


--
-- Name: index_request_forms_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_manager_id ON volun.request_forms USING btree (manager_id);


--
-- Name: index_request_forms_on_req_reason_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_req_reason_id ON volun.request_forms USING btree (req_reason_id);


--
-- Name: index_request_forms_on_req_rejection_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_req_rejection_type_id ON volun.request_forms USING btree (req_rejection_type_id);


--
-- Name: index_request_forms_on_req_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_req_status_id ON volun.request_forms USING btree (req_status_id);


--
-- Name: index_request_forms_on_request_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_request_type_id ON volun.request_forms USING btree (request_type_id);


--
-- Name: index_request_forms_on_rt_extendable_type_and_rt_extendable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_rt_extendable_type_and_rt_extendable_id ON volun.request_forms USING btree (rt_extendable_type, rt_extendable_id);


--
-- Name: index_request_forms_on_user_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_request_forms_on_user_id ON volun.request_forms USING btree (user_id);


--
-- Name: index_request_types_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_request_types_on_kind ON volun.request_types USING btree (kind);


--
-- Name: index_resources_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_resources_on_name ON volun.resources USING btree (name);


--
-- Name: index_road_types_on_code; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_road_types_on_code ON volun.road_types USING btree (code);


--
-- Name: index_road_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_road_types_on_name ON volun.road_types USING btree (name);


--
-- Name: index_roles_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_roles_on_kind ON volun.roles USING btree (kind);


--
-- Name: index_rt_activity_publishings_on_activity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_activity_publishings_on_activity_id ON volun.rt_activity_publishings USING btree (activity_id);


--
-- Name: index_rt_activity_publishings_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_activity_publishings_on_entity_id ON volun.rt_activity_publishings USING btree (entity_id);


--
-- Name: index_rt_activity_publishings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_activity_publishings_on_project_id ON volun.rt_activity_publishings USING btree (project_id);


--
-- Name: index_rt_activity_unpublishings_on_activity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_activity_unpublishings_on_activity_id ON volun.rt_activity_unpublishings USING btree (activity_id);


--
-- Name: index_rt_entity_subscribes_on_entity_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_entity_subscribes_on_entity_type_id ON volun.rt_entity_subscribes USING btree (entity_type_id);


--
-- Name: index_rt_entity_subscribes_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_entity_subscribes_on_project_id ON volun.rt_entity_subscribes USING btree (project_id);


--
-- Name: index_rt_entity_unsubscribes_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_entity_unsubscribes_on_project_id ON volun.rt_entity_unsubscribes USING btree (project_id);


--
-- Name: index_rt_others_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_others_on_entity_id ON volun.rt_others USING btree (entity_id);


--
-- Name: index_rt_project_publishings_on_borought_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_publishings_on_borought_id ON volun.rt_project_publishings USING btree (borought_id);


--
-- Name: index_rt_project_publishings_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_publishings_on_district_id ON volun.rt_project_publishings USING btree (district_id);


--
-- Name: index_rt_project_publishings_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_publishings_on_entity_id ON volun.rt_project_publishings USING btree (entity_id);


--
-- Name: index_rt_project_publishings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_publishings_on_project_id ON volun.rt_project_publishings USING btree (project_id);


--
-- Name: index_rt_project_publishings_on_province_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_publishings_on_province_id ON volun.rt_project_publishings USING btree (province_id);


--
-- Name: index_rt_project_unpublishings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_project_unpublishings_on_project_id ON volun.rt_project_unpublishings USING btree (project_id);


--
-- Name: index_rt_volunteer_amendments_on_borought_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_amendments_on_borought_id ON volun.rt_volunteer_amendments USING btree (borought_id);


--
-- Name: index_rt_volunteer_amendments_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_amendments_on_district_id ON volun.rt_volunteer_amendments USING btree (district_id);


--
-- Name: index_rt_volunteer_amendments_on_province_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_amendments_on_province_id ON volun.rt_volunteer_amendments USING btree (province_id);


--
-- Name: index_rt_volunteer_amendments_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_amendments_on_volunteer_id ON volun.rt_volunteer_amendments USING btree (volunteer_id);


--
-- Name: index_rt_volunteer_appointments_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_appointments_on_volunteer_id ON volun.rt_volunteer_appointments USING btree (volunteer_id);


--
-- Name: index_rt_volunteer_project_subscribes_on_info_source_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_project_subscribes_on_info_source_id ON volun.rt_volunteer_project_subscribes USING btree (info_source_id);


--
-- Name: index_rt_volunteer_project_subscribes_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_project_subscribes_on_project_id ON volun.rt_volunteer_project_subscribes USING btree (project_id);


--
-- Name: index_rt_volunteer_project_subscribes_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_project_subscribes_on_volunteer_id ON volun.rt_volunteer_project_subscribes USING btree (volunteer_id);


--
-- Name: index_rt_volunteer_subscribes_on_channel_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_subscribes_on_channel_id ON volun.rt_volunteer_subscribes USING btree (channel_id);


--
-- Name: index_rt_volunteer_subscribes_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_subscribes_on_district_id ON volun.rt_volunteer_subscribes USING btree (district_id);


--
-- Name: index_rt_volunteer_subscribes_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_subscribes_on_project_id ON volun.rt_volunteer_subscribes USING btree (project_id);


--
-- Name: index_rt_volunteer_unsubscribes_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_unsubscribes_on_project_id ON volun.rt_volunteer_unsubscribes USING btree (project_id);


--
-- Name: index_rt_volunteer_unsubscribes_on_unsubscribe_level_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteer_unsubscribes_on_unsubscribe_level_id ON volun.rt_volunteer_unsubscribes USING btree (unsubscribe_level_id);


--
-- Name: index_rt_volunteers_demands_on_borought_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteers_demands_on_borought_id ON volun.rt_volunteers_demands USING btree (borought_id);


--
-- Name: index_rt_volunteers_demands_on_district_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteers_demands_on_district_id ON volun.rt_volunteers_demands USING btree (district_id);


--
-- Name: index_rt_volunteers_demands_on_entity_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteers_demands_on_entity_id ON volun.rt_volunteers_demands USING btree (entity_id);


--
-- Name: index_rt_volunteers_demands_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteers_demands_on_project_id ON volun.rt_volunteers_demands USING btree (project_id);


--
-- Name: index_rt_volunteers_demands_on_province_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_rt_volunteers_demands_on_province_id ON volun.rt_volunteers_demands USING btree (province_id);


--
-- Name: index_search_forms_on_user_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_search_forms_on_user_id ON volun.search_forms USING btree (user_id);


--
-- Name: index_services_on_code; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_services_on_code ON volun.services USING btree (code);


--
-- Name: index_services_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_services_on_name ON volun.services USING btree (name);


--
-- Name: index_skills_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_skills_on_name ON volun.skills USING btree (name);


--
-- Name: index_skills_volunteers_on_skill_id_and_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_skills_volunteers_on_skill_id_and_volunteer_id ON volun.skills_volunteers USING btree (skill_id, volunteer_id);


--
-- Name: index_skills_volunteers_on_volunteer_id_and_skill_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_skills_volunteers_on_volunteer_id_and_skill_id ON volun.skills_volunteers USING btree (volunteer_id, skill_id);


--
-- Name: index_statuses_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_statuses_on_name ON volun.statuses USING btree (name);


--
-- Name: index_timetables_on_event_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_timetables_on_event_id ON volun.timetables USING btree (event_id);


--
-- Name: index_tracking_types_on_alias_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_tracking_types_on_alias_name ON volun.tracking_types USING btree (alias_name);


--
-- Name: index_tracking_types_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_tracking_types_on_name ON volun.tracking_types USING btree (name);


--
-- Name: index_traits_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_traits_on_name ON volun.traits USING btree (name);


--
-- Name: index_unsubscribe_levels_on_kind; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_unsubscribe_levels_on_kind ON volun.unsubscribe_levels USING btree (kind);


--
-- Name: index_unsubscribe_reasons_on_name; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_unsubscribe_reasons_on_name ON volun.unsubscribe_reasons USING btree (name);


--
-- Name: index_users_on_authentication_token; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_users_on_authentication_token ON volun.users USING btree (authentication_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON volun.users USING btree (email);


--
-- Name: index_users_on_loggable_type_and_loggable_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_users_on_loggable_type_and_loggable_id ON volun.users USING btree (loggable_type, loggable_id);


--
-- Name: index_users_on_notice_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_users_on_notice_type_id ON volun.users USING btree (notice_type_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON volun.users USING btree (reset_password_token);


--
-- Name: index_virtual_community_component_types_on_code; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_virtual_community_component_types_on_code ON volun.virtual_community_component_types USING btree (code);


--
-- Name: index_virtual_community_component_types_on_title; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_virtual_community_component_types_on_title ON volun.virtual_community_component_types USING btree (title);


--
-- Name: index_virtual_community_components_on_order; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_virtual_community_components_on_order ON volun.virtual_community_components USING btree ("order");


--
-- Name: index_volun_assessments_on_trait_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_assessments_on_trait_id ON volun.volun_assessments USING btree (trait_id);


--
-- Name: index_volun_assessments_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_assessments_on_volunteer_id ON volun.volun_assessments USING btree (volunteer_id);


--
-- Name: index_volun_assessments_projects_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_assessments_projects_on_project_id ON volun.volun_assessments_projects USING btree (project_id);


--
-- Name: index_volun_assessments_projects_on_trait_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_assessments_projects_on_trait_id ON volun.volun_assessments_projects USING btree (trait_id);


--
-- Name: index_volun_assessments_projects_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_assessments_projects_on_volunteer_id ON volun.volun_assessments_projects USING btree (volunteer_id);


--
-- Name: index_volun_availabilities_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_availabilities_on_volunteer_id ON volun.volun_availabilities USING btree (volunteer_id);


--
-- Name: index_volun_contacts_on_contact_result_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_contacts_on_contact_result_id ON volun.volun_contacts USING btree (contact_result_id);


--
-- Name: index_volun_contacts_on_contact_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_contacts_on_contact_type_id ON volun.volun_contacts USING btree (contact_type_id);


--
-- Name: index_volun_contacts_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_contacts_on_manager_id ON volun.volun_contacts USING btree (manager_id);


--
-- Name: index_volun_contacts_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_contacts_on_project_id ON volun.volun_contacts USING btree (project_id);


--
-- Name: index_volun_contacts_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_contacts_on_volunteer_id ON volun.volun_contacts USING btree (volunteer_id);


--
-- Name: index_volun_known_languages_on_language_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_known_languages_on_language_id ON volun.volun_known_languages USING btree (language_id);


--
-- Name: index_volun_known_languages_on_language_level_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_known_languages_on_language_level_id ON volun.volun_known_languages USING btree (language_level_id);


--
-- Name: index_volun_known_languages_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_known_languages_on_volunteer_id ON volun.volun_known_languages USING btree (volunteer_id);


--
-- Name: index_volun_proy_meetings_on_meeting_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_proy_meetings_on_meeting_id ON volun.volun_proy_meetings USING btree (meeting_id);


--
-- Name: index_volun_proy_meetings_on_projects_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_proy_meetings_on_projects_volunteer_id ON volun.volun_proy_meetings USING btree (projects_volunteer_id);


--
-- Name: index_volun_trackings_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_trackings_on_manager_id ON volun.volun_trackings USING btree (manager_id);


--
-- Name: index_volun_trackings_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_trackings_on_project_id ON volun.volun_trackings USING btree (project_id);


--
-- Name: index_volun_trackings_on_request_form_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_trackings_on_request_form_id ON volun.volun_trackings USING btree (request_form_id);


--
-- Name: index_volun_trackings_on_tracking_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_trackings_on_tracking_type_id ON volun.volun_trackings USING btree (tracking_type_id);


--
-- Name: index_volun_trackings_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volun_trackings_on_volunteer_id ON volun.volun_trackings USING btree (volunteer_id);


--
-- Name: index_volunteer_projects_relations_on_project_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteer_projects_relations_on_project_id ON volun.volunteer_projects_relations USING btree (project_id);


--
-- Name: index_volunteer_projects_relations_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteer_projects_relations_on_volunteer_id ON volun.volunteer_projects_relations USING btree (volunteer_id);


--
-- Name: index_volunteers_on_academic_level_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_academic_level_id ON volun.volunteers USING btree (academic_level_id);


--
-- Name: index_volunteers_on_address_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_address_id ON volun.volunteers USING btree (address_id);


--
-- Name: index_volunteers_on_code_cid360_autentication; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX index_volunteers_on_code_cid360_autentication ON volun.volunteers USING btree (code_cid360_autentication);


--
-- Name: index_volunteers_on_document_type_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_document_type_id ON volun.volunteers USING btree (document_type_id);


--
-- Name: index_volunteers_on_employment_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_employment_status_id ON volun.volunteers USING btree (employment_status_id);


--
-- Name: index_volunteers_on_gender_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_gender_id ON volun.volunteers USING btree (gender_id);


--
-- Name: index_volunteers_on_info_source_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_info_source_id ON volun.volunteers USING btree (info_source_id);


--
-- Name: index_volunteers_on_manager_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_manager_id ON volun.volunteers USING btree (manager_id);


--
-- Name: index_volunteers_on_nationality_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_nationality_id ON volun.volunteers USING btree (nationality_id);


--
-- Name: index_volunteers_on_profession_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_profession_id ON volun.volunteers USING btree (profession_id);


--
-- Name: index_volunteers_on_status_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_status_id ON volun.volunteers USING btree (status_id);


--
-- Name: index_volunteers_on_unsubscribe_reason_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_volunteers_on_unsubscribe_reason_id ON volun.volunteers USING btree (unsubscribe_reason_id);


--
-- Name: index_ws_messages_on_citizen_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ws_messages_on_citizen_id ON volun.ws_messages USING btree (citizen_id);


--
-- Name: index_ws_messages_on_volunteer_id; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX index_ws_messages_on_volunteer_id ON volun.ws_messages USING btree (volunteer_id);


--
-- Name: profile_volunteer; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX profile_volunteer ON volun.volun_profiles_volunteers USING btree (volun_profile_id, volunteer_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: volun; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON volun.schema_migrations USING btree (version);


--
-- Name: volunteer_profile; Type: INDEX; Schema: volun; Owner: -
--

CREATE INDEX volunteer_profile ON volun.volun_profiles_volunteers USING btree (volunteer_id, volun_profile_id);


--
-- Name: volunteers t_auto_num_volunteer; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_auto_num_volunteer BEFORE INSERT ON volun.volunteers FOR EACH ROW EXECUTE FUNCTION volun.auto_num_volunteer();


--
-- Name: projects t_auto_order_project; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_auto_order_project BEFORE INSERT ON volun.projects FOR EACH ROW EXECUTE FUNCTION volun.auto_order_project();


--
-- Name: citizens t_delete_citizen; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_citizen BEFORE DELETE ON volun.citizens FOR EACH ROW EXECUTE FUNCTION volun.delete_citizen();


--
-- Name: projects t_delete_project; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_project BEFORE DELETE ON volun.projects FOR EACH ROW EXECUTE FUNCTION volun.delete_project();


--
-- Name: projects t_delete_project_pt; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_project_pt AFTER DELETE ON volun.projects FOR EACH ROW EXECUTE FUNCTION volun.delete_project_pt();


--
-- Name: projects_volunteers t_delete_projects_volunteer; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_projects_volunteer BEFORE DELETE ON volun.projects_volunteers FOR EACH ROW EXECUTE FUNCTION volun.delete_projects_volunteer();


--
-- Name: request_forms t_delete_request_forms; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_request_forms BEFORE DELETE ON volun.request_forms FOR EACH ROW EXECUTE FUNCTION volun.delete_request_forms();


--
-- Name: rt_activity_publishings t_delete_rt_activity_publishings; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_activity_publishings BEFORE DELETE ON volun.rt_activity_publishings FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_activity_publishings();


--
-- Name: rt_entity_subscribes t_delete_rt_entity_subscribes; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_entity_subscribes BEFORE DELETE ON volun.rt_entity_subscribes FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_entity_subscribes();


--
-- Name: rt_entity_unsubscribes t_delete_rt_entity_unsubscribes; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_entity_unsubscribes BEFORE DELETE ON volun.rt_entity_unsubscribes FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_entity_unsubscribes();


--
-- Name: rt_project_publishings t_delete_rt_project_publishings; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_project_publishings BEFORE DELETE ON volun.rt_project_publishings FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_project_publishings();


--
-- Name: rt_project_unpublishings t_delete_rt_project_unpublishings; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_project_unpublishings BEFORE DELETE ON volun.rt_project_unpublishings FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_project_unpublishings();


--
-- Name: rt_volunteer_amendments t_delete_rt_volunteer_amendments; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_volunteer_amendments BEFORE DELETE ON volun.rt_volunteer_amendments FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_volunteer_amendments();


--
-- Name: rt_volunteer_project_subscribes t_delete_rt_volunteer_project_subscribes; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_volunteer_project_subscribes BEFORE DELETE ON volun.rt_volunteer_project_subscribes FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_volunteer_project_subscribes();


--
-- Name: rt_volunteer_subscribes t_delete_rt_volunteer_subscribes; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_volunteer_subscribes BEFORE DELETE ON volun.rt_volunteer_subscribes FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_volunteer_subscribes();


--
-- Name: rt_volunteer_unsubscribes t_delete_rt_volunteer_unsubscribes; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_volunteer_unsubscribes BEFORE DELETE ON volun.rt_volunteer_unsubscribes FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_volunteer_unsubscribes();


--
-- Name: rt_volunteers_demands t_delete_rt_volunteers_demands; Type: TRIGGER; Schema: volun; Owner: -
--

CREATE TRIGGER t_delete_rt_volunteers_demands BEFORE DELETE ON volun.rt_volunteers_demands FOR EACH ROW EXECUTE FUNCTION volun.delete_rt_volunteers_demands();


--
-- Name: addresses fk_rails_0286d8b237; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.addresses
    ADD CONSTRAINT fk_rails_0286d8b237 FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: rt_volunteer_project_subscribes fk_rails_043e9d4436; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_project_subscribes
    ADD CONSTRAINT fk_rails_043e9d4436 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: pro_trackings fk_rails_060cb9329c; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings
    ADD CONSTRAINT fk_rails_060cb9329c FOREIGN KEY (tracking_type_id) REFERENCES volun.tracking_types(id);


--
-- Name: rt_activity_publishings fk_rails_0660269916; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_publishings
    ADD CONSTRAINT fk_rails_0660269916 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: service_polls fk_rails_067e6430ce; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_polls
    ADD CONSTRAINT fk_rails_067e6430ce FOREIGN KEY (citizen_service_id) REFERENCES volun.citizen_services(id);


--
-- Name: volunteers fk_rails_076cef1fdc; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_076cef1fdc FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: rt_project_unpublishings fk_rails_078fb0c271; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_unpublishings
    ADD CONSTRAINT fk_rails_078fb0c271 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: citizen_service_trackings fk_rails_0812121828; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_service_trackings
    ADD CONSTRAINT fk_rails_0812121828 FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: projects_volunteers fk_rails_10b7dc3c73; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects_volunteers
    ADD CONSTRAINT fk_rails_10b7dc3c73 FOREIGN KEY (shift_definition_id) REFERENCES volun.shift_definitions(id);


--
-- Name: pt_subventions fk_rails_11a4b85b23; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pt_subventions
    ADD CONSTRAINT fk_rails_11a4b85b23 FOREIGN KEY (proposal_id) REFERENCES volun.proposals(id);


--
-- Name: citizens fk_rails_11c4a2a562; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens
    ADD CONSTRAINT fk_rails_11c4a2a562 FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: timetables fk_rails_15c4c560b1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.timetables
    ADD CONSTRAINT fk_rails_15c4c560b1 FOREIGN KEY (event_id) REFERENCES volun.events(id);


--
-- Name: rt_activity_publishings fk_rails_18766af8ad; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_publishings
    ADD CONSTRAINT fk_rails_18766af8ad FOREIGN KEY (activity_id) REFERENCES volun.activities(id);


--
-- Name: volunteers fk_rails_1b6a6db21c; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_1b6a6db21c FOREIGN KEY (address_id) REFERENCES volun.addresses(id);


--
-- Name: addresses fk_rails_1b98d66e19; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.addresses
    ADD CONSTRAINT fk_rails_1b98d66e19 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: ent_trackings fk_rails_1c63970ba6; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings
    ADD CONSTRAINT fk_rails_1c63970ba6 FOREIGN KEY (request_form_id) REFERENCES volun.request_forms(id);


--
-- Name: rt_volunteer_appointments fk_rails_1cc9427642; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_appointments
    ADD CONSTRAINT fk_rails_1cc9427642 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: frontpage_elements fk_rails_1eb0844745; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_elements
    ADD CONSTRAINT fk_rails_1eb0844745 FOREIGN KEY (frontpage_position_id) REFERENCES volun.frontpage_positions(id);


--
-- Name: volun_assessments_projects fk_rails_1f94e4af32; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments_projects
    ADD CONSTRAINT fk_rails_1f94e4af32 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: volun_proy_meetings fk_rails_20182559f2; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_proy_meetings
    ADD CONSTRAINT fk_rails_20182559f2 FOREIGN KEY (projects_volunteer_id) REFERENCES volun.projects_volunteers(id);


--
-- Name: req_status_traces fk_rails_218216dd2a; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_status_traces
    ADD CONSTRAINT fk_rails_218216dd2a FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: volun_trackings fk_rails_238a7f0b81; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT fk_rails_238a7f0b81 FOREIGN KEY (tracking_type_id) REFERENCES volun.tracking_types(id);


--
-- Name: volun_contacts fk_rails_23944d719f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT fk_rails_23944d719f FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: rt_volunteer_amendments fk_rails_25bfa3a243; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments
    ADD CONSTRAINT fk_rails_25bfa3a243 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: rt_volunteer_project_subscribes fk_rails_29c1ce4943; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_project_subscribes
    ADD CONSTRAINT fk_rails_29c1ce4943 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: service_responses fk_rails_2a04eb7e87; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_responses
    ADD CONSTRAINT fk_rails_2a04eb7e87 FOREIGN KEY (service_question_id) REFERENCES volun.service_questions(id);


--
-- Name: events fk_rails_2a2c9250e8; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.events
    ADD CONSTRAINT fk_rails_2a2c9250e8 FOREIGN KEY (address_id) REFERENCES volun.addresses(id);


--
-- Name: activities fk_rails_2a8dd8e5f4; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.activities
    ADD CONSTRAINT fk_rails_2a8dd8e5f4 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: rt_entity_unsubscribes fk_rails_2e4768f7ae; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_unsubscribes
    ADD CONSTRAINT fk_rails_2e4768f7ae FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: rt_volunteer_amendments fk_rails_2ec518b2c1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments
    ADD CONSTRAINT fk_rails_2ec518b2c1 FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: volun_trackings fk_rails_30ddac6918; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT fk_rails_30ddac6918 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: record_histories fk_rails_3184e37ad1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.record_histories
    ADD CONSTRAINT fk_rails_3184e37ad1 FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: pro_trackings fk_rails_3196973a02; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings
    ADD CONSTRAINT fk_rails_3196973a02 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: activities fk_rails_319db762f5; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.activities
    ADD CONSTRAINT fk_rails_319db762f5 FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: volun_proy_meetings fk_rails_31f7f11847; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_proy_meetings
    ADD CONSTRAINT fk_rails_31f7f11847 FOREIGN KEY (meeting_id) REFERENCES volun.meetings(id);


--
-- Name: citizens fk_rails_32d0559513; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens
    ADD CONSTRAINT fk_rails_32d0559513 FOREIGN KEY (document_type_id) REFERENCES volun.document_types(id);


--
-- Name: rt_project_publishings fk_rails_332a8e8cba; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT fk_rails_332a8e8cba FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: districts_volunteers fk_rails_34ba85bb73; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts_volunteers
    ADD CONSTRAINT fk_rails_34ba85bb73 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: volunteers fk_rails_34e98e56ba; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_34e98e56ba FOREIGN KEY (academic_level_id) REFERENCES volun.academic_levels(id);


--
-- Name: citizen_services fk_rails_3540214057; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_3540214057 FOREIGN KEY (citizen_id) REFERENCES volun.citizens(id);


--
-- Name: rt_project_publishings fk_rails_365de92f46; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT fk_rails_365de92f46 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: shift_definitions fk_rails_3b79c69e9f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.shift_definitions
    ADD CONSTRAINT fk_rails_3b79c69e9f FOREIGN KEY (projects_volunteer_id) REFERENCES volun.projects_volunteers(id);


--
-- Name: citizens fk_rails_3ce699a0cb; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens
    ADD CONSTRAINT fk_rails_3ce699a0cb FOREIGN KEY (address_id) REFERENCES volun.addresses(id);


--
-- Name: citizen_services fk_rails_3fcfe9e875; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_3fcfe9e875 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: addresses fk_rails_427031b28f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.addresses
    ADD CONSTRAINT fk_rails_427031b28f FOREIGN KEY (borought_id) REFERENCES volun.boroughts(id);


--
-- Name: pro_issues fk_rails_42c5cc9a36; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_issues
    ADD CONSTRAINT fk_rails_42c5cc9a36 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: service_responses fk_rails_4346ee0eed; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_responses
    ADD CONSTRAINT fk_rails_4346ee0eed FOREIGN KEY (citizen_service_id) REFERENCES volun.citizen_services(id);


--
-- Name: rt_activity_unpublishings fk_rails_442ab3e172; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_unpublishings
    ADD CONSTRAINT fk_rails_442ab3e172 FOREIGN KEY (activity_id) REFERENCES volun.activities(id);


--
-- Name: citizen_services fk_rails_448e664ef1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_448e664ef1 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: albums fk_rails_46b2cb25d7; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.albums
    ADD CONSTRAINT fk_rails_46b2cb25d7 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: frontpage_elements fk_rails_48cc4fb8e0; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_elements
    ADD CONSTRAINT fk_rails_48cc4fb8e0 FOREIGN KEY (updated_by) REFERENCES volun.users(id);


--
-- Name: degrees fk_rails_4a5d797686; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.degrees
    ADD CONSTRAINT fk_rails_4a5d797686 FOREIGN KEY (degree_type_id) REFERENCES volun.degree_types(id);


--
-- Name: rt_volunteer_amendments fk_rails_4ad249f539; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments
    ADD CONSTRAINT fk_rails_4ad249f539 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: volun_trackings fk_rails_4e614b6d26; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT fk_rails_4e614b6d26 FOREIGN KEY (request_form_id) REFERENCES volun.request_forms(id);


--
-- Name: volunteers fk_rails_500ca5be40; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_500ca5be40 FOREIGN KEY (profession_id) REFERENCES volun.professions(id);


--
-- Name: volun_trackings fk_rails_5048823c24; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT fk_rails_5048823c24 FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: rt_volunteer_subscribes fk_rails_537c7138f3; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_subscribes
    ADD CONSTRAINT fk_rails_537c7138f3 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: meetings fk_rails_541cff8da4; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.meetings
    ADD CONSTRAINT fk_rails_541cff8da4 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: virtual_community_components fk_rails_551447f60b; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_components
    ADD CONSTRAINT fk_rails_551447f60b FOREIGN KEY (virtual_community_component_type_id) REFERENCES volun.virtual_community_component_types(id);


--
-- Name: documents fk_rails_55cfc1b0e0; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.documents
    ADD CONSTRAINT fk_rails_55cfc1b0e0 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: entities fk_rails_5607227932; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entities
    ADD CONSTRAINT fk_rails_5607227932 FOREIGN KEY (req_reason_id) REFERENCES volun.req_reasons(id);


--
-- Name: audits fk_rails_56c76c8b78; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.audits
    ADD CONSTRAINT fk_rails_56c76c8b78 FOREIGN KEY (resource_id) REFERENCES volun.resources(id);


--
-- Name: digital_topics fk_rails_574bdd1836; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_topics
    ADD CONSTRAINT fk_rails_574bdd1836 FOREIGN KEY (moderate_status_id) REFERENCES volun.moderate_statuses(id);


--
-- Name: rt_project_publishings fk_rails_57ce8fa08b; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT fk_rails_57ce8fa08b FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: volunteers fk_rails_58ead69265; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_58ead69265 FOREIGN KEY (unsubscribe_reason_id) REFERENCES volun.unsubscribe_reasons(id);


--
-- Name: virtual_community_components fk_rails_5b0c4c33b9; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.virtual_community_components
    ADD CONSTRAINT fk_rails_5b0c4c33b9 FOREIGN KEY (virtual_community_component_id) REFERENCES volun.virtual_community_components(id);


--
-- Name: request_forms fk_rails_5c72243f21; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_5c72243f21 FOREIGN KEY (req_reason_id) REFERENCES volun.req_reasons(id);


--
-- Name: service_votes fk_rails_5d45634cfd; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.service_votes
    ADD CONSTRAINT fk_rails_5d45634cfd FOREIGN KEY (citizen_service_id) REFERENCES volun.citizen_services(id);


--
-- Name: volunteers fk_rails_5e1021f516; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_5e1021f516 FOREIGN KEY (document_type_id) REFERENCES volun.document_types(id);


--
-- Name: volun_contacts fk_rails_5e29a4093f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT fk_rails_5e29a4093f FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: ent_trackings fk_rails_5fda6378ac; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings
    ADD CONSTRAINT fk_rails_5fda6378ac FOREIGN KEY (tracking_type_id) REFERENCES volun.tracking_types(id);


--
-- Name: rt_volunteer_unsubscribes fk_rails_61c7d4f0f7; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_unsubscribes
    ADD CONSTRAINT fk_rails_61c7d4f0f7 FOREIGN KEY (unsubscribe_level_id) REFERENCES volun.unsubscribe_levels(id);


--
-- Name: digital_follows fk_rails_620dfeb480; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_follows
    ADD CONSTRAINT fk_rails_620dfeb480 FOREIGN KEY (digital_topic_id) REFERENCES volun.digital_topics(id);


--
-- Name: rt_volunteers_demands fk_rails_6406ed0e98; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT fk_rails_6406ed0e98 FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: citizen_services fk_rails_64aa202196; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_64aa202196 FOREIGN KEY (service_id) REFERENCES volun.services(id);


--
-- Name: rt_others fk_rails_676005d820; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_others
    ADD CONSTRAINT fk_rails_676005d820 FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: ws_messages fk_rails_682b832382; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ws_messages
    ADD CONSTRAINT fk_rails_682b832382 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: volun_assessments fk_rails_6b7b9a59a1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments
    ADD CONSTRAINT fk_rails_6b7b9a59a1 FOREIGN KEY (trait_id) REFERENCES volun.traits(id);


--
-- Name: volun_assessments_projects fk_rails_6bfcef8235; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments_projects
    ADD CONSTRAINT fk_rails_6bfcef8235 FOREIGN KEY (trait_id) REFERENCES volun.traits(id);


--
-- Name: volunteers fk_rails_6c193dac04; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_6c193dac04 FOREIGN KEY (info_source_id) REFERENCES volun.info_sources(id);


--
-- Name: citizen_trackings fk_rails_6fd337a75c; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_trackings
    ADD CONSTRAINT fk_rails_6fd337a75c FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: rt_activity_publishings fk_rails_738ca69b5b; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_activity_publishings
    ADD CONSTRAINT fk_rails_738ca69b5b FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: citizen_trackings fk_rails_73c3b5d30d; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_trackings
    ADD CONSTRAINT fk_rails_73c3b5d30d FOREIGN KEY (tracking_type_id) REFERENCES volun.tracking_types(id);


--
-- Name: ent_trackings fk_rails_752e02b6ad; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings
    ADD CONSTRAINT fk_rails_752e02b6ad FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: volunteers fk_rails_757c811ef3; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_757c811ef3 FOREIGN KEY (status_id) REFERENCES volun.statuses(id);


--
-- Name: events fk_rails_75f14fef31; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.events
    ADD CONSTRAINT fk_rails_75f14fef31 FOREIGN KEY (event_type_id) REFERENCES volun.event_types(id);


--
-- Name: meetings fk_rails_7680d10454; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.meetings
    ADD CONSTRAINT fk_rails_7680d10454 FOREIGN KEY (type_meeting_id) REFERENCES volun.type_meetings(id);


--
-- Name: massive_email_historics fk_rails_780d95da9e; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_email_historics
    ADD CONSTRAINT fk_rails_780d95da9e FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: frontpage_elements fk_rails_7c5d7eb5f0; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.frontpage_elements
    ADD CONSTRAINT fk_rails_7c5d7eb5f0 FOREIGN KEY (created_by) REFERENCES volun.users(id);


--
-- Name: digital_comments fk_rails_82db8fe7c3; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_comments
    ADD CONSTRAINT fk_rails_82db8fe7c3 FOREIGN KEY (moderate_status_id) REFERENCES volun.moderate_statuses(id);


--
-- Name: derivaties_volunteers fk_rails_86582f5183; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.derivaties_volunteers
    ADD CONSTRAINT fk_rails_86582f5183 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: derivaties_volunteers fk_rails_8c465460a7; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.derivaties_volunteers
    ADD CONSTRAINT fk_rails_8c465460a7 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: rt_entity_subscribes fk_rails_908a35f934; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_subscribes
    ADD CONSTRAINT fk_rails_908a35f934 FOREIGN KEY (entity_type_id) REFERENCES volun.entity_types(id);


--
-- Name: rt_volunteers_demands fk_rails_90b919b5dd; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT fk_rails_90b919b5dd FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: entities fk_rails_931c3c76ef; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entities
    ADD CONSTRAINT fk_rails_931c3c76ef FOREIGN KEY (address_id) REFERENCES volun.addresses(id);


--
-- Name: digital_comments fk_rails_944bb6c896; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_comments
    ADD CONSTRAINT fk_rails_944bb6c896 FOREIGN KEY (digital_comment_id) REFERENCES volun.digital_comments(id);


--
-- Name: volun_contacts fk_rails_9aae347038; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT fk_rails_9aae347038 FOREIGN KEY (contact_type_id) REFERENCES volun.contact_types(id);


--
-- Name: volun_known_languages fk_rails_9d90949dfa; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_known_languages
    ADD CONSTRAINT fk_rails_9d90949dfa FOREIGN KEY (language_level_id) REFERENCES volun.language_levels(id);


--
-- Name: digital_faqs fk_rails_9ec414637e; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_faqs
    ADD CONSTRAINT fk_rails_9ec414637e FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: citizen_trackings fk_rails_9f20914ccb; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_trackings
    ADD CONSTRAINT fk_rails_9f20914ccb FOREIGN KEY (citizen_id) REFERENCES volun.citizens(id);


--
-- Name: req_status_traces fk_rails_a0518bdb63; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_status_traces
    ADD CONSTRAINT fk_rails_a0518bdb63 FOREIGN KEY (req_status_id) REFERENCES volun.req_statuses(id);


--
-- Name: permissions fk_rails_a1fc3c028f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.permissions
    ADD CONSTRAINT fk_rails_a1fc3c028f FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: rt_volunteer_subscribes fk_rails_a280a09a27; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_subscribes
    ADD CONSTRAINT fk_rails_a280a09a27 FOREIGN KEY (channel_id) REFERENCES volun.channels(id);


--
-- Name: links fk_rails_a578a39c28; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.links
    ADD CONSTRAINT fk_rails_a578a39c28 FOREIGN KEY (link_type_id) REFERENCES volun.link_types(id);


--
-- Name: search_forms fk_rails_a59c37d9fb; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.search_forms
    ADD CONSTRAINT fk_rails_a59c37d9fb FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: managers fk_rails_a6f023dc94; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.managers
    ADD CONSTRAINT fk_rails_a6f023dc94 FOREIGN KEY (role_id) REFERENCES volun.roles(id);


--
-- Name: citizen_service_trackings fk_rails_a70ad13676; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_service_trackings
    ADD CONSTRAINT fk_rails_a70ad13676 FOREIGN KEY (citizen_service_id) REFERENCES volun.citizen_services(id);


--
-- Name: volun_availabilities fk_rails_a7af8553e5; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_availabilities
    ADD CONSTRAINT fk_rails_a7af8553e5 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: rt_volunteers_demands fk_rails_a96f5dbb87; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT fk_rails_a96f5dbb87 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: rt_volunteer_unsubscribes fk_rails_a9ecf0a1e5; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_unsubscribes
    ADD CONSTRAINT fk_rails_a9ecf0a1e5 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: rt_volunteers_demands fk_rails_abaca6d161; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT fk_rails_abaca6d161 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: volunteer_projects_relations fk_rails_abd1500acb; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_projects_relations
    ADD CONSTRAINT fk_rails_abd1500acb FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: volun_contacts fk_rails_b0091044ff; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT fk_rails_b0091044ff FOREIGN KEY (contact_result_id) REFERENCES volun.contact_results(id);


--
-- Name: volun_assessments_projects fk_rails_b167571955; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments_projects
    ADD CONSTRAINT fk_rails_b167571955 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: citizen_service_trackings fk_rails_b424cd82d3; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_service_trackings
    ADD CONSTRAINT fk_rails_b424cd82d3 FOREIGN KEY (tracking_type_id) REFERENCES volun.tracking_types(id);


--
-- Name: digital_topics fk_rails_b4445a9401; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_topics
    ADD CONSTRAINT fk_rails_b4445a9401 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: req_status_traces fk_rails_b590151cfe; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.req_status_traces
    ADD CONSTRAINT fk_rails_b590151cfe FOREIGN KEY (request_form_id) REFERENCES volun.request_forms(id);


--
-- Name: citizens fk_rails_b5bbf76ab4; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizens
    ADD CONSTRAINT fk_rails_b5bbf76ab4 FOREIGN KEY (gender_id) REFERENCES volun.genders(id);


--
-- Name: users fk_rails_b6d9e93c75; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.users
    ADD CONSTRAINT fk_rails_b6d9e93c75 FOREIGN KEY (notice_type_id) REFERENCES volun.notice_types(id);


--
-- Name: request_forms fk_rails_b7b1b87a80; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_b7b1b87a80 FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: volunteer_projects_relations fk_rails_b9232f371f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteer_projects_relations
    ADD CONSTRAINT fk_rails_b9232f371f FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: ent_trackings fk_rails_b949e9dc3f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ent_trackings
    ADD CONSTRAINT fk_rails_b949e9dc3f FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: massive_sms_historics fk_rails_bd4ed927db; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.massive_sms_historics
    ADD CONSTRAINT fk_rails_bd4ed927db FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: activities fk_rails_bddba12401; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.activities
    ADD CONSTRAINT fk_rails_bddba12401 FOREIGN KEY (area_id) REFERENCES volun.areas(id);


--
-- Name: volun_contacts fk_rails_bfb3770a7f; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_contacts
    ADD CONSTRAINT fk_rails_bfb3770a7f FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: volunteers fk_rails_c14510fd72; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_c14510fd72 FOREIGN KEY (next_point_id) REFERENCES volun.next_points(id);


--
-- Name: projects fk_rails_c18bcd8b34; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects
    ADD CONSTRAINT fk_rails_c18bcd8b34 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: rt_project_publishings fk_rails_c26fe692f2; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT fk_rails_c26fe692f2 FOREIGN KEY (borought_id) REFERENCES volun.boroughts(id);


--
-- Name: digital_comments fk_rails_c3dbb8f35c; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.digital_comments
    ADD CONSTRAINT fk_rails_c3dbb8f35c FOREIGN KEY (digital_topic_id) REFERENCES volun.digital_topics(id);


--
-- Name: ws_messages fk_rails_c4590a950a; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.ws_messages
    ADD CONSTRAINT fk_rails_c4590a950a FOREIGN KEY (citizen_id) REFERENCES volun.citizens(id);


--
-- Name: volun_assessments fk_rails_c5cc40e3a0; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_assessments
    ADD CONSTRAINT fk_rails_c5cc40e3a0 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: volun_trackings fk_rails_c6094089f7; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_trackings
    ADD CONSTRAINT fk_rails_c6094089f7 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: pro_trackings fk_rails_c62e2af513; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings
    ADD CONSTRAINT fk_rails_c62e2af513 FOREIGN KEY (manager_id) REFERENCES volun.managers(id);


--
-- Name: districts_volunteers fk_rails_c6975a0a3d; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.districts_volunteers
    ADD CONSTRAINT fk_rails_c6975a0a3d FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: entity_organizations fk_rails_c7f911e163; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entity_organizations
    ADD CONSTRAINT fk_rails_c7f911e163 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: citizen_services fk_rails_c9cc5e0b12; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_c9cc5e0b12 FOREIGN KEY (status_service_id) REFERENCES volun.status_services(id);


--
-- Name: pro_trackings fk_rails_cba16eb429; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.pro_trackings
    ADD CONSTRAINT fk_rails_cba16eb429 FOREIGN KEY (request_form_id) REFERENCES volun.request_forms(id);


--
-- Name: volun_known_languages fk_rails_cc2b7ccd21; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_known_languages
    ADD CONSTRAINT fk_rails_cc2b7ccd21 FOREIGN KEY (volunteer_id) REFERENCES volun.volunteers(id);


--
-- Name: volun_known_languages fk_rails_d2aadf7da1; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volun_known_languages
    ADD CONSTRAINT fk_rails_d2aadf7da1 FOREIGN KEY (language_id) REFERENCES volun.languages(id);


--
-- Name: volunteers fk_rails_d3eae920e5; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_d3eae920e5 FOREIGN KEY (employment_status_id) REFERENCES volun.employment_statuses(id);


--
-- Name: rt_project_publishings fk_rails_d7571941e7; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_project_publishings
    ADD CONSTRAINT fk_rails_d7571941e7 FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- Name: request_forms fk_rails_d97274598d; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_d97274598d FOREIGN KEY (request_type_id) REFERENCES volun.request_types(id);


--
-- Name: boroughts fk_rails_e334104308; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.boroughts
    ADD CONSTRAINT fk_rails_e334104308 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: citizen_services fk_rails_e3cae136be; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.citizen_services
    ADD CONSTRAINT fk_rails_e3cae136be FOREIGN KEY (borought_id) REFERENCES volun.boroughts(id);


--
-- Name: rt_volunteer_subscribes fk_rails_e4656aa818; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_subscribes
    ADD CONSTRAINT fk_rails_e4656aa818 FOREIGN KEY (district_id) REFERENCES volun.districts(id);


--
-- Name: audits fk_rails_e6d7b3fb68; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.audits
    ADD CONSTRAINT fk_rails_e6d7b3fb68 FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: rt_entity_subscribes fk_rails_e8163aa1d5; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_subscribes
    ADD CONSTRAINT fk_rails_e8163aa1d5 FOREIGN KEY (project_id) REFERENCES volun.projects(id);


--
-- Name: request_forms fk_rails_e988d26401; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_e988d26401 FOREIGN KEY (req_rejection_type_id) REFERENCES volun.req_rejection_types(id);


--
-- Name: rt_entity_subscribes fk_rails_ed2268fc5c; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_entity_subscribes
    ADD CONSTRAINT fk_rails_ed2268fc5c FOREIGN KEY (province_id) REFERENCES volun.provinces(id);


--
-- Name: rt_volunteers_demands fk_rails_f0cbf030b6; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteers_demands
    ADD CONSTRAINT fk_rails_f0cbf030b6 FOREIGN KEY (borought_id) REFERENCES volun.boroughts(id);


--
-- Name: request_forms fk_rails_f4b5a45eed; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_f4b5a45eed FOREIGN KEY (user_id) REFERENCES volun.users(id);


--
-- Name: volunteers fk_rails_f771039889; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_f771039889 FOREIGN KEY (nationality_id) REFERENCES volun.nationalities(id);


--
-- Name: entities fk_rails_f8a44fe9bf; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.entities
    ADD CONSTRAINT fk_rails_f8a44fe9bf FOREIGN KEY (entity_type_id) REFERENCES volun.entity_types(id);


--
-- Name: volunteers fk_rails_f91bcc4362; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.volunteers
    ADD CONSTRAINT fk_rails_f91bcc4362 FOREIGN KEY (gender_id) REFERENCES volun.genders(id);


--
-- Name: request_forms fk_rails_f9a88e8ebd; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.request_forms
    ADD CONSTRAINT fk_rails_f9a88e8ebd FOREIGN KEY (req_status_id) REFERENCES volun.req_statuses(id);


--
-- Name: rt_volunteer_amendments fk_rails_fe3e202092; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.rt_volunteer_amendments
    ADD CONSTRAINT fk_rails_fe3e202092 FOREIGN KEY (borought_id) REFERENCES volun.boroughts(id);


--
-- Name: projects fk_rails_ffd1fb1016; Type: FK CONSTRAINT; Schema: volun; Owner: -
--

ALTER TABLE ONLY volun.projects
    ADD CONSTRAINT fk_rails_ffd1fb1016 FOREIGN KEY (entity_id) REFERENCES volun.entities(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO volun;

INSERT INTO "schema_migrations" (version) VALUES
('20170302091316'),
('20170302091318'),
('20170302091319'),
('20170302091321'),
('20170302091322'),
('20170302091324'),
('20170302091325'),
('20170302091327'),
('20170302091329'),
('20170302091330'),
('20170302091332'),
('20170302091333'),
('20170302091335'),
('20170302091336'),
('20170302091338'),
('20170302091340'),
('20170302091341'),
('20170302091343'),
('20170302091344'),
('20170302091346'),
('20170302091348'),
('20170302091349'),
('20170302091351'),
('20170302091352'),
('20170302091354'),
('20170302091356'),
('20170302091357'),
('20170302091359'),
('20170302091401'),
('20170302091402'),
('20170302091404'),
('20170302091406'),
('20170302091407'),
('20170302091409'),
('20170302091411'),
('20170302091412'),
('20170302091414'),
('20170302091416'),
('20170302091417'),
('20170302091419'),
('20170302091421'),
('20170302091422'),
('20170302091424'),
('20170302091426'),
('20170302091428'),
('20170302091429'),
('20170302091431'),
('20170302091433'),
('20170302091434'),
('20170302091436'),
('20170302091438'),
('20170302091439'),
('20170302091441'),
('20170302091443'),
('20170302091445'),
('20170302091446'),
('20170302091448'),
('20170302091450'),
('20170302091452'),
('20170302091453'),
('20170302091455'),
('20170302091457'),
('20170302091459'),
('20170302091500'),
('20170302091502'),
('20170302091504'),
('20170302091506'),
('20170302091507'),
('20170302091509'),
('20170302091511'),
('20170302091513'),
('20170302091515'),
('20170302091517'),
('20170302091518'),
('20170302091520'),
('20170302091522'),
('20170302091524'),
('20170302091526'),
('20170302091527'),
('20170302091529'),
('20170302091532'),
('20170302091533'),
('20170302091535'),
('20170302091537'),
('20170302091538'),
('20170302091539'),
('20170302091541'),
('20170302091542'),
('20170302091543'),
('20170302091545'),
('20170302091546'),
('20170302091547'),
('20170302091548'),
('20170302091550'),
('20170302091551'),
('20170302091552'),
('20170302091554'),
('20170302091555'),
('20170302091556'),
('20170302091558'),
('20170306131946'),
('20170313091610'),
('20170315152345'),
('20170315153416'),
('20170315153639'),
('20170316083914'),
('20170321160845'),
('20170403094612'),
('20170405163435'),
('20170405163822'),
('20170406105006'),
('20170406105317'),
('20170406113450'),
('20170406140900'),
('20170406173450'),
('20170416174003'),
('20170416175829'),
('20170416181032'),
('20170416183328'),
('20170419152355'),
('20170419152451'),
('20170425124700'),
('20170425154420'),
('20170426070241'),
('20170426070253'),
('20170426070322'),
('20170426070330'),
('20170426070533'),
('20170427104323'),
('20170428073441'),
('20170428074708'),
('20170511083434'),
('20171003125116'),
('20171003140614'),
('20171009095533'),
('20171011132508'),
('20171013091357'),
('20171129115622'),
('20171129120606'),
('20171201114732'),
('20171204092124'),
('20171204092327'),
('20171205100345'),
('20171219151528'),
('20171219151645'),
('20171220110937'),
('20171220110957'),
('20180305102725'),
('20180305122156'),
('20180305145522'),
('20180314144508'),
('20180314144516'),
('20180322124433'),
('20180412080500'),
('20180412081023'),
('20180413125706'),
('20180413130801'),
('20180417104559'),
('20180507111608'),
('20180508115736'),
('20180509112709'),
('20180521080646'),
('20180521082853'),
('20180521102259'),
('20180523112545'),
('20180531115118'),
('20180605071929'),
('20180711062028'),
('20180802111347'),
('20180821123719'),
('20181016085749'),
('20181017140515'),
('20181017141702'),
('20181025083847'),
('20181029122817'),
('20181105100001'),
('20181105100021'),
('20181105100054'),
('20181105111250'),
('20181105113651'),
('20181105114810'),
('20181105123356'),
('20181115083944'),
('20181130124124'),
('20181228075114'),
('20181228115714'),
('20181228120028'),
('20190109070504'),
('20190109105200'),
('20190115091635'),
('20190117152711'),
('20190124093010'),
('20190129124309'),
('20190129135116'),
('20190129140649'),
('20190221090401'),
('20190221090425'),
('20190221090454'),
('20190221090710'),
('20190221091025'),
('20190221111218'),
('20190225094948'),
('20190314083256'),
('20190520071330'),
('20190520122857'),
('20190520124042'),
('20190520125814'),
('20190529062010'),
('20190529122720'),
('20190529123046'),
('20190529123047'),
('20190529123738'),
('20190529131748'),
('20190530115556'),
('20190531074042'),
('20190531111932'),
('20190531115211'),
('20190605075142'),
('20190606064629'),
('20190606070213'),
('20190606070231'),
('20190606074137'),
('20190606074846'),
('20190607092536'),
('20190607124433'),
('20190611111255'),
('20190624111153'),
('20190625101914'),
('20190627072632'),
('20190701064905'),
('20190703123454'),
('20190703123470'),
('20190703123475'),
('20190704121541'),
('20190704121571'),
('20190704121576'),
('20190711084118'),
('20190719100423'),
('20190719110156'),
('20190723103054'),
('20190806113018'),
('20190812085203'),
('20190812085720'),
('20190814073826'),
('20190814080519'),
('20190820081146'),
('20190820103755'),
('20190821082703'),
('20190821100751'),
('20190821101358'),
('20190826104349'),
('20190829082903'),
('20190912131729'),
('20190916091211'),
('20190916091345'),
('20190916094827'),
('20190917075544'),
('20190917121659'),
('20190918105944'),
('20190918114933'),
('20190927100908'),
('20191203090944'),
('20200115161112'),
('20200310150730'),
('20200327130825'),
('20200608071341'),
('20200625062517'),
('20200625062658'),
('20200715060420'),
('20200722060859'),
('20200728075328'),
('20200728075633'),
('20200730065039'),
('20200804101514'),
('20200819100629'),
('20200904070007'),
('20200904072001'),
('20200904072213'),
('20200911082636'),
('20200911084630'),
('20200911085145'),
('20200911102455'),
('20200911113003'),
('20200911124457'),
('20200914073242'),
('20200914091141'),
('20200914115820'),
('20200915062431'),
('20200923073053'),
('20200924140016'),
('20200924164123'),
('20200925070239'),
('20201013093446'),
('20201013093746'),
('20201015112421'),
('20201015143438'),
('20201105070948'),
('20201203100749'),
('20201203115456'),
('20201204132259'),
('20201215072940'),
('20201222074907'),
('20201223092919'),
('20210222092152'),
('20210228165015'),
('20210310074448'),
('20210314162726'),
('20210314172200'),
('20210331113928'),
('20210421124239'),
('20210426085427'),
('20210614093915'),
('20210706080409'),
('20210706080421'),
('20210708090807'),
('20210709064553'),
('20211114162021'),
('20211123075227'),
('20211123112439'),
('20211124112901'),
('20211124130627'),
('20211124131119'),
('20211205183256'),
('20211209155331'),
('20211216132925'),
('20211216142354'),
('20211217103927'),
('20211217105303'),
('20211217105834'),
('20220209075614'),
('20220209080002'),
('20220209080604'),
('20220211111722'),
('20220218133405'),
('20220225081247'),
('20220324113723'),
('20220504062949'),
('20220504063410'),
('20220511133531'),
('20220511145410'),
('20220511145421'),
('20220511145432'),
('20220511151325'),
('20220512100004'),
('20220512103207'),
('20220517080726'),
('20220520082511'),
('20220630140429'),
('20220707151240'),
('20220727113937'),
('20220728095428'),
('20220728151108'),
('20220728151600'),
('20220728151852'),
('20220728152017'),
('20220818121624'),
('20220819114632'),
('20220819114642'),
('20220819115134'),
('20220824131302'),
('20220828163921'),
('20220926081306'),
('20220926082009'),
('20220926092719'),
('20220926093221'),
('20220926111153'),
('20220926114126'),
('20220926122721'),
('20220926122728'),
('20220926122746'),
('20220926122753'),
('20220927091437'),
('20220927095528'),
('20220927111516'),
('20220927151858'),
('20220928164024'),
('20220929101551'),
('20220930091748'),
('20221018084441'),
('20221018100434'),
('20221018100451'),
('20221020072034'),
('20221020072049'),
('20221024131033'),
('20221031111804'),
('20221031112615'),
('20221202074125'),
('20230126114226'),
('20230126170934'),
('20230127221856'),
('20230216111649'),
('20230307105439'),
('20230309155939'),
('20230310122339'),
('20230315133901'),
('20230315155256'),
('20230315161859'),
('20230315172238'),
('20230315175414'),
('20230315175854'),
('20230316114451'),
('20230317095917'),
('20230317100255'),
('20230317101222'),
('20230323105124'),
('20230323110136'),
('20230411100956'),
('20230412072001'),
('20230424123126'),
('20230502084202'),
('20230502084203'),
('20230502084204'),
('20230502084212'),
('20230502084233'),
('20230502084244'),
('20230502104230'),
('20230502131708'),
('20230502151455'),
('20230510072932'),
('20230510135349'),
('20230512063145'),
('20230517074858'),
('20230517122726'),
('20230518075309'),
('20230518075330'),
('20230518094744'),
('20230518111904'),
('20230518130215'),
('20230522131126');


