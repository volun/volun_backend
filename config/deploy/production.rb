set :deploy_to, deploysecret(:deploy_to)
set :server_name, deploysecret(:server_name)
set :server_name1, deploysecret(:server_name1)
set :db_server, deploysecret(:db_server)
set :branch, :stable
set :ssh_options, port: deploysecret(:ssh_port)
set :stage, :production
set :rails_env, :production
set :application, 'volun_backend'
set :default_env, {
    PATH: '$HOME/.npm-packages/bin/:$PATH',
    NODE_ENVIRONMENT: 'production'
 }

server deploysecret(:server), user: deploysecret(:user), roles: %w(web app db importer)
server deploysecret(:server1), user: deploysecret(:user), roles: %w(web app importer)
# server deploysecret(:server2), user: deploysecret(:user), roles: %w(web app db importer)
# server deploysecret(:server3), user: deploysecret(:user), roles: %w(web app db importer)
