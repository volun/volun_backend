class SpanishVatValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value.blank?
      unless validate_vat(value)
        record.errors[attribute] <<
          (options[:message] ||
            I18n.t('activerecord.errors.messages.invalid_document_number', default: 'Invalid document number'))
      end
    end
  end

  def validate_vat(passed_value)
    return false if passed_value.nil?
    value = passed_value.clone
      case
      when value.match(/[0-9]{8}[a-z]/i)
        validate_nif(value)
      when value.match(/[a-w][0-9]{7}[0-9a-z]/i)
        validate_cif(value)
      when value.match(/[xyz][0-9]{7,8}[a-z]/i)
        validate_nie(value)
      else
        false
      end
  end

  # Validates NIF
  def validate_nif(value)
    letters = "TRWAGMYFPDXBNJZSQVHLCKE"
    check = value.slice!(value.length - 1..value.length - 1).upcase
    calculated_letter = letters[value.to_i % 23].chr
    check === calculated_letter
  end

  def validate_cif(value)
    even = 0
    odd = 0
    uletter = ["J", "A", "B", "C", "D", "E", "F", "G", "H", "I"]
    text = value.upcase
    last = text[8,1]

    [1,3,5,7].collect do |cont|
      xxx = (2 * text[cont,1].to_i).to_s + "0"
      odd += xxx[0,1].to_i + xxx[1,1].to_i
      even += text[cont+1,1].to_i if cont < 7
    end

    sum = (even + odd).to_s
    unumber = sum[-1].to_i
    unumber = (10 - unumber).to_s
    unumber = 0 if(unumber.to_i == 10)
    ((last.to_i == unumber.to_i) || (last == uletter[unumber.to_i]))
  end

  # Validates NIE, in fact is a fake, a NIE is really a NIF with first number changed to capital 'X' letter, so we change the first X to a 0 and then try to
  # pass the nif validator
  def validate_nie(value)
    case
    when value[0].upcase == 'X'
      value[0] = '0'
    when value[0].upcase == 'Y'
      value[0] = '1'
    when value[0].upcase == 'Z'
      value[0] = '2'
    end
    value.slice(0) if value.size > 9
    validate_nif(value)
  end
end
