require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module VolunBackend
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.i18n.default_locale = :es

    config.time_zone = 'Madrid'

    config.active_record.default_timezone = :local
    config.active_record.schema_format = :sql

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    config.active_job.queue_adapter = :delayed_job

    #config.active_record.raise_in_transactional_callbacks = true

    config.generators do |g|
      g.test_framework :rspec, routing_specs: false, request_specs: false
      g.helper            false
      g.assets            false
      g.view_specs        false
      g.jbuilder          false
    end

    config.autoloader = :classic
    config.paths["config/routes"] = Dir[Rails.root.join('config/routes/*.rb')]
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
    config.assets.precompile += ['ckeditor/*']
  end
end
