# config valid only for current version of Capistrano
lock '3.5.0'

# TODO Remove this line when cap -T works
#set :stage, 'preproduction'

def deploysecret(key)
  @deploy_secrets_yml ||= YAML.load_file('config/deploy_secrets.yml')[fetch(:stage).to_s]
  @deploy_secrets_yml[key.to_s]
end

# set :default_env, {
#   PATH: '$HOME/.npm-packages/bin/:$PATH',
#   NODE_ENVIRONMENT: 'production'
# }

$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
 
set :rvm_bin_path, "/aytomad/app/volun/.rvm/bin"
set :rvm_ruby_version, '2.7.2'
set :rvm_type, :user

set :application, 'volun_backend'
set :server_name, deploysecret(:server_name)
set :full_app_name, fetch(:application)
# If ssh access is restricted, probably you need to use https access
set :repo_url, 'https://bitbucket.org/volun/volun_backend.git'

set :scm, :git
set :revision, `git rev-parse --short #{fetch(:branch)}`.strip

set :log_level, :info
set :pty, true
set :use_sudo, false

set :linked_files, %w{config/database.yml config/secrets.yml config/unicorn.rb config/elastic_apm.yml}
set :linked_dirs, %w{log tmp public/system public/assets}

set :keep_releases, 5

set :local_user, ENV['USER']

set :delayed_job_workers, 4
#set :delayed_job_roles, [:app, :background]

# Run test before deploy
set :tests, ["spec"]

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }


# Config files should be copied by deploy:setup_config
set(:config_files, %w(
  log_rotation
  database.yml
  secrets.yml
  unicorn.rb
))


namespace :deploy do
  # Check right version of deploy branch
  before :deploy, "deploy:check_revision"
  # Run test aund continue only if passed
  # before :deploy, "deploy:run_tests"
  # Compile assets locally and then rsync
  # after 'deploy:symlink:shared', 'deploy:compile_assets_locally'
  after :finishing, 'deploy:cleanup'
  # Restart unicorn
  after :published, 'mapeo_nas'
  after 'deploy:publishing', 'deploy:restart'
  # after 'deploy:restart', 'sidekiq:restart'
end

task :mapeo_nas do
  on roles(:app) do
    execute "ln -ds /aytomad/datos/datosabiertos /aytomad/app/volun/volun_backend/current/public/ficherosdatosabiertos"
    execute "ln -ds /aytomad/app/volun/volun_backend/shared/private/SolidaryChain /aytomad/app/volun/volun_backend/current/public/SolidaryChain"
    # Solo para instalación inicial en entornos
    # execute "ln -ds /aytomad/app/volun/volun_backend/shared/private /aytomad/app/volun/volun_backend/shared/public"
  end
end