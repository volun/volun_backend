# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

require "./"+ File.dirname(__FILE__) + "/environment.rb"
set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

#value es el valor que tiene asignada la variable "PeriodicidadPublicacionDatosAbiertos"
value=(Setting.find_by(key: "PeriodicidadPublicacionDatosAbiertos").value).to_i 

every 1.day, at: '9:00'  do
  rake "announcements:unsubscribe_activities"
  rake "announcements:unsubscribe_projects"
  rake "send:congratulations"
  rake "csv_list_volunteers:list_volunteers"
end

every 1.day, at: '01:00' do
  rake "destroy_search:clean"
  rake "padron:get"
end

every 1.day, at: '10:00' do
  rake "send:rt_appointment"
end

every 1.hours do
  rake "services:update_services_names"
  rake "services:update_status"
end 

every value.months do  #La tarea se ejecutara cada n meses
  # rake "csv_generate:nationality" 
  rake "csv_generate:volunteer" 
  # rake "csv_generate:volunteer_by_district" 
end

every 1.month, at: 'start of the month at 2am' do  #La tarea se ejecutara el primer día del mes
  rake "padron:get" 
end

every '0 14 * * 5' do
  rake "send:citizen_services"
end

every 5.minutes do 
  rake "citizens:send_sms"
end

every '0 22 * * 1' do 
  rake "citizens:citizens_removal"
end