get 'digital_manage' => 'digital/manage#index', as: 'digital_manage'  
get 'digital_moderate' => 'digital/manage#moderate', as: 'digital_moderate'
get 'digital_edit_moderate/:id/:type' => 'digital/manage#edit_moderate', as: 'digital_edit_moderate'
put 'digital_update_moderate/:id/:type' => 'digital/manage#update_moderate', as: 'digital_update_moderate'
get 'digital_accept_moderate/:id/:type' => 'digital/manage#accept_moderate', as: 'digital_accept_moderate'
get 'digital_reject_moderate/:id/:type' => 'digital/manage#reject_moderate', as: 'digital_reject_moderate'
namespace :digital do
    resources :topics, concerns: :recoverable 
    resources :comments, concerns: :recoverable do
      get :comment_respond, on: :member
      post :send_respond, on: :member
    end
    resources :faqs, concerns: :recoverable    
    resources :follows, concerns: :recoverable 
end
