 # Project related routes
 resources :projects, concerns: :recoverable do
   post :clone_project, on: :member
   get :remove_project, on: :member
   get 'pt_extensions_menu', on: :collection
   get :pre_clone, as: 'pre_clone', on: :member
   get :volun_unlink, as: 'volun_unlink', on: :member
   resources :volunteers do
      get :derivatie, on: :collection
      get :massive_email, on: :collection
      post :send_massive_email, on: :collection
      get 'export_all', on: :collection
      get :set_project_volunteer,as: "set_project_volunteer", on: :member
      get :show_project_volunteer, as: "show_project_volunteer", on: :member
      get :show_project_derivaty, as: "show_project_derivaty", on: :member
      get :set_project_derivaty, as: "set_project_derivaty", on: :member
      get :show_unsubscribe, as: 'show_unsubscribe', on: :member
      get :volun_password, on: :member
      get :unlink_project_volunteer, as: "unlink_project_volunteer", on: :member
      get :create_meeting, on: :member
      post :process_create_meeting, on: :member
      get :assign_turn, on: :member
      get :associate_derivatie, on: :member
      get :post_associate_derivatie, on: :collection  
      post :process_assign_turn, on: :member
      get :new_password, on: :member
      get :update_password, on: :member
   end
 end