namespace :pt do
    resources :socials 
    resources :centres 
    resources :permanents 
    resources :punctuals 
    resources :entities
    resources :subventions 
    resources :others 
    resources :retired_volunteers
    resources :municipals
    resources :volunteers
end