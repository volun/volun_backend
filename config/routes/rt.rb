concern :rt_routes do
    get :process_request_form, on: :member
    get :pre_approve_request_form, on: :member
    get :pre_reject_request_form, on: :member
    patch :reject_request_form, on: :member
    get :mark_request_form_as_pending, on: :member
    get :mark_request_form_as_processing, on: :member
    get :download_request, on: :collection
    post :process_dowload_request, on: :collection
end

# RequestForm related routes
resources :request_types
resources :request_forms, concerns: :recoverable do
  get 'rt_extensions_menu', on: :collection
end


namespace :rt do
    resources :solidarity_chains, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'solidarity_chains#mark_request_form_as_pending',
          on: :member
      get :destroy_image, on: :member
    end
    resources :volunteer_subscribes, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'volunteer_subscribes#mark_request_form_as_pending',
          on: :member
      get :mark_request_form_as_appointed, on: :member
      get :send_appointment, on: :member
     
    end
    resources :volunteer_unsubscribes, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'volunteer_unsubscribes#mark_request_form_as_pending',
          on: :member
    end
    resources :volunteer_amendments, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'volunteer_amendments#mark_request_form_as_pending',
          on: :member
    end
    resources :volunteer_appointments, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'volunteer_appointments#mark_request_form_as_pending',
          on: :member
    end
    resources :entity_subscribes, concerns: :rt_routes
    resources :entity_unsubscribes, concerns: :rt_routes
    resources :volunteers_demands, concerns: :rt_routes do 
      get :undo_rejection_request_form, 
          to: 'volunteers_demands#mark_request_form_as_pending', 
          on: :member 
    end
    resources :activity_unpublishings, concerns: :rt_routes
    resources :activity_publishings, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'activity_publishings#mark_request_form_as_pending',
          on: :member
    end
    resources :project_unpublishings, concerns: :rt_routes
    resources :project_publishings, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'project_publishings#mark_request_form_as_pending',
          on: :member
    end
    resources :volunteer_project_subscribes, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'volunteer_project_subscribes#mark_request_form_as_pending',
          on: :member
    end
    resources :others, concerns: :rt_routes do
      get :undo_rejection_request_form,
          to: 'others#mark_request_form_as_pending',
          on: :member
    end
  end