resources :derivaties_volunteers, concerns: :recoverable do
    get :unlink, on: :member
    get :create_meeting, on: :member
    post :process_create_meeting, on: :member
    get :associate_derivatie, on: :member
  end

  resources :volunteers, concerns: :recoverable do #MGC
    get :derivatie, on: :collection
    get :massive_email, on: :collection
    post :send_massive_email, on: :collection
    get 'export_all', on: :collection
    get :set_project_volunteer,as: "set_project_volunteer", on: :member
    get :show_project_volunteer, as: "show_project_volunteer", on: :member
    get :show_project_derivaty, as: "show_project_derivaty", on: :member
    get :set_project_derivaty, as: "set_project_derivaty", on: :member
    get :show_unsubscribe, as: 'show_unsubscribe', on: :member
    get :volun_password, on: :member
    get :unlink_project_volunteer, as: "unlink_project_volunteer", on: :member
    get :create_meeting, on: :member
    post :process_create_meeting, on: :member
    get :assign_turn, on: :member
    get :associate_derivatie, on: :member  
    get :post_associate_derivatie, on: :collection  
    post :process_assign_turn, on: :member
    get :new_password, on: :member
    get :update_password, on: :member
    get :derivatie_projects, on: :member
    get :send_massive_sms, on: :collection
    post :post_send_massive_sms, on: :collection
    get :historic_massive_sms, on: :collection
  end

  