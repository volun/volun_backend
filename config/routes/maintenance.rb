namespace :administration do
    namespace :catalogue do
        resources :areas, concerns: :recoverable
        resources :channels, concerns: :recoverable
        resources :boroughts, concerns: :recoverable
        resources :moderate_statuses, concerns: :recoverable
        resources :collectives, concerns: :recoverable
        resources :coordinations, concerns: :recoverable
        resources :degrees, concerns: :recoverable
        resources :districts, concerns: :recoverable
        resources :genders, concerns: :recoverable
        resources :info_sources, concerns: :recoverable
        resources :languages, concerns: :recoverable
        resources :nationalities, concerns: :recoverable
        resources :next_points, concerns: :recoverable
        resources :professions, concerns: :recoverable
        resources :provinces, concerns: :recoverable
        namespace :req do
            resources :reasons, concerns: :recoverable
            resources :rejection_types, concerns: :recoverable
        end
        namespace :virtual_community do
          resources :component_types, concerns: :recoverable
        end
        resources :service_questions, concerns: :recoverable
        resources :services, concerns: :recoverable
        resources :shift_definitions, concerns: :recoverable
        resources :skills, concerns: :recoverable
        resources :tracking_types, concerns: :recoverable
        resources :traits, concerns: :recoverable
        resources :type_meetings, concerns: :recoverable
        resources :unsubscribe_reasons, concerns: :recoverable
        resources :volun_profiles, concerns: :recoverable
    end 

    resources :resources, concerns: :recoverable
    resources :settings
    resources :audits
    resources :new_campaings do
        get :change_public, on: :member
    end
    resources :virtual_communities do
        get :cv_statistics, on: :collection
        get :ws_statistics, on: :collection
        get :show_origin, on: :collection
        get :show_popular, on: :collection
    end
    resources :managers, concerns: :recoverable
    
end

get 'administration_order_projects' => 'administration/order_projects#index', as: 'administration_order_projects' 
post 'administration_order_projects_update' => 'administration/order_projects#update', as: 'administration_order_projects_update' 
get 'virtual_community' => 'administration/virtual_community#index', as: 'virtual_community' 
get 'virtual_community/component/:id' => 'administration/virtual_community#component', as: 'virtual_community_component' 
put 'virtual_community/component/:id' => 'administration/virtual_community#update_component', as: 'virtual_community_update_component' 