Rails.application.routes.draw do
  
 
 
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end

  get "/", to: redirect("/volun")

  scope 'volun' do
    
    concern :recoverable do
      post :recover, on: :member
    end
  
    resources :massive_email_historics
    resources :massive_sms_historics
    resources :volun_exports do
      get :delete_modal, on: :member
      get :download, on: :member
    end

    resources :meetings, concerns: :recoverable
    resources :activities , concerns: :recoverable
    
    devise_for :users
    resources :links do
      post :target, on: :collection
    end
    
    resources :addresses do
      get 'bdc_search_towns', on: :collection
      get 'bdc_search_roads', on: :collection
      get 'bdc_search_road_numbers', on: :collection
      get 'bdc_get_district_borought', on: :collection
    end
    resources :entities, concerns: :recoverable do
      get :ent_password, as: 'ent_password', on: :member
      get :new_password, on: :member
      get :update_password, on: :member
    end
    resources :citizens, concerns: :recoverable do
      get :citizen_password, on: :member
      get :send_massive_sms, on: :collection
      post :post_send_massive_sms, on: :collection
      get :historic_massive_sms, on: :collection
      get :new_password, on: :member
      get :update_password, on: :member
      resources :citizen_services do
        get :question, on: :member
      end
      resources :citizen_trackings
    end
    resources :citizen_services, concerns: :recoverable do
      get :export_questions, on: :collection
      get :question, on: :member
      get :search, on: :collection
      get :search_district, on: :collection
      resources :citizen_service_trackings
    end

    resources :citizen_trackings, concerns: :recoverable
    resources :citizen_service_trackings, concerns: :recoverable
    
    
    draw :volunteer
    draw :project
    draw :maintenance
    draw :rt
    draw :pt
    draw :tracking
    draw :manage_digital
    
    get 'accept_unsubscribe' => 'volunteers#accept_unsubscribe', as: 'accept_unsubscribe'  
    get 'show_sms' => 'volunteers#show_sms', as: 'show_sms'
    get 'send_sms' => 'volunteers#send_sms', as: 'send_sms'
    get 'show_mail' => 'volunteers#show_mail', as: 'show_mail'
    get 'send_mail' => 'volunteers#send_mail', as: 'send_mail'
    get 'send_mail_dataset' => 'administrators#send_mail_dataset', as: 'send_mail_dataset'
    get 'show_questions_mail_path' => 'citizen_services#show_mail', as: 'show_questions_mail'
    post 'send_questions_mail_path' => 'citizen_services#send_mail', as: 'send_questions_mail'
    get 'export_all'=> 'volunteers#export_all', as: 'export_all' #MGC
    get 'maintenance' => 'welcome#maintenance', as: 'maintenance'
    get 'administration' => 'welcome#administration', as: 'administration'
    root 'welcome#index'
  end


  get '/ckeditor/pictures' => 'ckeditor/rails/pictures#index'
  post '/ckeditor/pictures' => 'ckeditor/rails/pictures#create'
  delete '/ckeditor/pictures/:id' => 'ckeditor/rails/pictures#destroy'
  get '/ckeditor/attachment_files' => 'ckeditor/rails/attachment_files#index'
  post '/ckeditor/attachment_files' => 'ckeditor/rails/attachment_files#create'
  delete '/ckeditor/attachment_files' => 'ckeditor/rails/attachment_files#create'

  Ckeditor::Rails::Engine.routes.draw do
    resources :pictures, only: [:index, :create, :destroy]
    resources :attachment_files, only: [:index, :create, :destroy]
  end
  
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
end
